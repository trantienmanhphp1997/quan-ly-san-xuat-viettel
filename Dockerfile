FROM phpdockerio/php74-fpm

# Fix debconf warnings upon build
ARG DEBIAN_FRONTEND=noninteractive
# Set working directory
WORKDIR /var/www

# Install selected extensions and other stuff
RUN apt-get update \
    && apt-get -y --no-install-recommends install  php7.4-mysql php7.4-xdebug php7.4-gd \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

RUN curl -sL https://deb.nodesource.com/setup_14.x  | bash - \
    && apt-get -y install nodejs

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy existing application directory contents
COPY . /var/www
COPY ./phpdocker/php-fpm/xdebug.ini /etc/php/7.4/mods-available/xdebug.ini

# Copy existing application directory permissions
COPY --chown=www:www . /var/www

#Permission cache Laravel
RUN chgrp -R www-data storage bootstrap/cache && chmod -R ug+rwx storage bootstrap/cache

#clear cache
RUN php artisan config:cache && php artisan config:clear

#another config
#RUN composer install && composer dump-autoload && php artisan key:generate && npm install && npm run dev
