<?php

namespace App\Console\Commands;

use App\Models\RecordPlanMemberXref;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SyncRecPlanStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:recordPlanStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync recordplan status to record_plan_member_xrefs table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::transaction
            (function () {
                $rpxrefs = RecordPlanMemberXref::with(["record_plan"])->get();

                foreach ($rpxrefs as $rpxref) {
                    $rpxref->update(["record_plan_status" => $rpxref->record_plan->status]);
                }

                $this->info('Syncing rp status was successful!');
            });
        } catch (\Exception $error) {
            Log::error("Error synchronizing  rp status to record_plan", $error->getTrace());
        }
    }
}
