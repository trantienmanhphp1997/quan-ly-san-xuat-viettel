<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SyncRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:roles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize roles for all users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::transaction(function () {
                $users = User::all();

                foreach ($users as $user) {
                    $roles = $user->getRoleNames()->filter(function ($role) use ($user) {
                        return strpos($role, "department") !== 0 || $role === "department $user->department_code";
                    });
                    if (count($roles) === 0) {
                        $roles[] = "department $user->department_code";
                    }
                    $user->syncRoles($roles);
                }

                $this->info('Syncing roles was successful!');
            });
        } catch (\Exception $error) {
            Log::error("Error synchronizing roles to users", $error->getTrace());
        }
    }
}
