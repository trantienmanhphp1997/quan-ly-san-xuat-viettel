<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->call('App\Http\Controllers\Admin\RecordPlanController@remindInComingRecordPlans')->everyFiveMinutes();
        $schedule->call('App\Http\Controllers\Admin\RecordPlanController@autoStartReportPlan')->everyFiveMinutes();
        $schedule->call('App\Http\Controllers\Admin\RecordPlanController@autoWrapReportPlan')->everyFiveMinutes();
        $schedule->call('App\Http\Controllers\Admin\MailController@sendDailyPlannedReportRecordPlans')->dailyAt('12:30');
        $schedule->call('App\Http\Controllers\Admin\MailController@sendDailyExecutedReportRecordPlans')->dailyAt('13:30');
//        $schedule->call('App\Http\Controllers\Admin\MailController@sendDailyReportRecordPlans')->everyMinute();
        $schedule->call('App\Http\Controllers\Admin\RecordPlanController@autoCreateRecordPlanRecurring')->dailyAt('00:30'); //7 giờ sáng hàng ngày
//        $schedule->call('App\Http\Controllers\Admin\RecordPlanController@autoCreateRecordPlanRecurring')->everyFifteenMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
