<?php


namespace App\Helpers;


use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class PaginateForArray
{
    private $items;
    private $perPage;
    private $page;
    private $path;
    private $request;
    private $pageName;
    private $otherPageName;

    public function __construct(
        $items,
        $perPage,
        $page,
        $path,
        Request $request,
        $pageName,
        $otherPageName
    ){
        $this->items = $items;
        $this->perPage = $perPage;
        $this->page = $page;
        $this->path = $path;
        $this->request = $request;
        $this->pageName = $pageName;
        $this->otherPageName = $otherPageName;
    }

    public static function paginate($items, $perPage, $path, $request, $pageName='page', $page = null)
    {
        $page = $page ?: (Paginator::resolveCurrentPage($pageName) ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        $options = [
            "path" => $path ,
            "pageName" => $pageName
        ];
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

}
