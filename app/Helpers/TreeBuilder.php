<?php
namespace App\Helpers;

class TreeBuilder
{
    public static function buildTree($list, $parentKey = "parent_id") {
        //Tạo mảng trung gian chứa danh sách con
        //key: parentIds
        //value: Danh sách các con
        $referList = array();
        foreach ($list as $item){
            $referList[$item[$parentKey]][] = $item;
        }
        if (count($referList) === 0) return $referList;
        $tree = self::createTree($referList, $referList[0]);
        return $tree;
    }

    private static function createTree(& $list, $parents){
        $tree = array();
        foreach ($parents as $parent){
            if(isset($list[$parent['id']])){
                $parent['children'] = self::createTree($list, $list[$parent['id']]);
            }
            $tree[] = $parent;
        }
        return $tree;
    }
}
