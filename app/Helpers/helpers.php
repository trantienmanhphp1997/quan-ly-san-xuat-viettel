<?php

// For add'active' class for activated route nav-item
function active_class($path, $active = 'active', $param_name='', $param_value='') {
  if (call_user_func_array('Request::is', (array)$path)) {
    if ($param_name !== '' && $param_value !== '' && request()->has('department_code'))
        return request()->get('department_code') == $param_value ? $active : '';
    return $active;
  }
  return '';
}

// For checking activated route
function is_active_route($path) {
  return call_user_func_array('Request::is', (array)$path) ? 'true' : 'false';
}

function reFormatDate($datetime, $format='d-m-Y'){
    return (isset($datetime) && ($datetime != '0000-00-00 00:00:00') && ($datetime != '0000-00-00'))? date($format, strtotime($datetime)) : '';
}
if (! function_exists('app')) {
    /**
     * Get the available container instance.
     *
     * @param  string|null  $abstract
     * @param  array  $parameters
     * @return mixed|\Illuminate\Contracts\Foundation\Application
     */
    function app($abstract = null, array $parameters = [])
    {
        if (is_null($abstract)) {
            return Container::getInstance();
        }

        return Container::getInstance()->make($abstract, $parameters);
    }
}
