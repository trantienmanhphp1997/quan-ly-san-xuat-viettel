<?php

namespace App\Http\Controllers\API;

use App\Exceptions\ValidateException;
use App\Repositories\CarRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\RecordPlanMemberXrefRepository;
use App\ResponseHandler\HandleResponse;
use App\Services\AssignmentService;
use App\Services\DeviceService;
use App\Services\Interfaces\IRecordPlanService;
use App\Services\MemberService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AssignmentAPIController extends BaseAPIController
{
    public $assignmentService;
    public $memberService;
    public $recordPlanService;
    public $deviceService;
    public $deviceRepository;
    public $carRepository;
    public $recordPlanMemberXrefRepository;

    public function __construct(
        AssignmentService $assignmentService,
        MemberService $memberService,
        IRecordPlanService $recordPlanService,
        DeviceService $deviceService,
        DeviceRepository $deviceRepository,
        CarRepository $carRepository,
        RecordPlanMemberXrefRepository $recordPlanMemberXrefRepository
    )
    {
        $this->assignmentService = $assignmentService;
        $this->memberService = $memberService;
        $this->recordPlanService = $recordPlanService;
        $this->deviceService = $deviceService;
        $this->deviceRepository = $deviceRepository;
        $this->carRepository = $carRepository;
        $this->recordPlanMemberXrefRepository = $recordPlanMemberXrefRepository;
    }

    /**
     *Lấy về danh sách lịch sản xuất có đề uất theo phòng ban
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getProposals(Request $request)
    {
        $response = HandleResponse::builder('/api/assignments/get-proposals')
            ->code(config('api_message.assignments.get_proposals'))
            ->status(200);
        try {
            $department_code = $request->department_code;
            $query = [
                's' => $request->query("s", ""),
                'record_plan_id' => $request->query("record_plan_id", ""),
                'time_filter' => $request->query("time_filter", ""),
                'address_type' => $request->query("address_type", "")
            ];

            //Nếu có điền id thì bỏ qua các filter
            if (!empty($query["record_plan_id"])) {
                unset($query["time_filter"]);
                unset($query["address_type"]);
            }

            $allocate_status = $request->allocate_status;
            $proposals = $this->recordPlanService->getProposalsByDepartmentCode($department_code, $query, $allocate_status);
            return $response->data($proposals)->build();
        } catch (\Exception $e) {
            Log::error('/api/assignment');
            Log::error($e);
            $response->status(422)->code(config('api_message.assignments.get_proposals'))->error($e->getMessage());
            if ($e instanceof ValidateException) {
                $response->code($e->getMessage());
            }
            return $response->build();
        }
    }

    /**
     *Lấy về danh sách lịch sản xuất đã đề xuất theo phòng ban
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAssignedProposals(Request $request)
    {
        try {
            $departmentCode = $request->department_code;
            $key_search = ["s" => $request->s ?? ""];
            $proposals = $this->recordPlanService->getProposalsByDepartmentCode($departmentCode, $key_search, config('common.status.proposals.assigned'));
            return HandleResponse::handle(
                config('api_message.assignments.get_assigned_proposals'),
                200,
                null,
                $proposals,
                '/api/assignments/get-assigned-proposals'
            );
        } catch (\Exception $e) {
            Log::error('/api/assignment');
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.assignments.get_assigned_proposals'),
                422,
                $e->getMessage(),
                null,
                '/api/assignments/get-assigned-proposals'
            );
        }
    }

    public function getRecordPlansHasDeliveredDevices(Request $request)
    {
        try {
            $record_plans = $this->assignmentService->getRecordPlansHasDeliveredDevices();
            return HandleResponse::handle(
                config('api_message.assignments.get_delivered_devices'),
                200,
                null,
                $record_plans,
                '/api/assignments/get-delivered-devices'
            );
        } catch (\Exception $e) {
            Log::error('/api/assignment');
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.assignments.get_delivered_devices'),
                422,
                $e->getMessage(),
                null,
                '/api/assignments/get-delivered-devices'
            );
        }
    }

    /**
     *Lấy về danh sách available members cho 1 record plan theo phòng ban
     *
     * @OA\Get(
     *      path="/api/find-by-department",
     *      tags={"Plan info"},
     *      description="Trả về plan info.",
     *      @OA\Parameter(
     *          name="record_plan_id",
     *          description="department id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="department_code",
     *          description="phòng ban của member, có 3 nhóm: camera, reporter, transportation",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="GET_PLAN_INFO_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/PlanInfoResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_PLAN_INFO_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAvailableMemberForRecordPlanByDepartmentCode(Request $request)
    {
        $response = HandleResponse::builder("/api/assignments/get-available-members")
            ->status(200)->code(config('api_message.assignments.get-available-members'));

        Log::info("/api/assignments/get-available-members", [
            "departmentCode" => $request->department_code,
            "s" => $request->s,
            "start_time" => $request->start_time,
            "end_time" => $request->end_time,
        ]);
        try {
            $departmentCode = $request->department_code;
            $availableMembers = $this->memberService->getAvailableMembers($departmentCode, $request->record_plan_id, ["s" => $request->s], $request->start_time, $request->end_time);
            return $response->data($availableMembers)->build();
        } catch (\Exception $e) {
            Log::error('/api/assignment');
            Log::error($e);
            $response->code(config('api_message.assignments.get-available-members'))->status(422)->error($e->getMessage());
            if ($e instanceof ValidateException) {
                $response->code($e->getMessage());
            }
            return $response->build();
        }

    }

    public function getAssignedMemberForRecordPlanByDepartmentCode(Request $request, $record_plan_id)
    {
        try {
            $department_code = $request->department_code;
            if ($department_code == 'transportation') {
                $assigned_members = collect($this->memberService->getAssignedMembers($department_code, $record_plan_id, ['car'], ["s" => $request->s]));
            } else {
                $assigned_members = collect($this->memberService->getAssignedMembers($department_code, $record_plan_id,  [], ["s" => $request->s]));
            }
            return HandleResponse::handle(
                config('api_message.assignments.get_assigned_members_success'),
                200,
                null,
                $assigned_members,
                "/api/assignments/$record_plan_id/get-assigned-members"
            );
        } catch (\Exception $e) {
            Log::error("/api/assignments/$record_plan_id/get-assigned-members");
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.assignments.get-assigned-members'),
                422,
                $e->getMessage(),
                null,
                "/api/assignments/$record_plan_id/get-assigned-members"
            );
        }

    }

    /**
     *Phân member vào 1 record_plan
     *
     * @OA\Get(
     *      path="/api/find-by-department",
     *      tags={"Plan info"},
     *      description="Trả về plan info.",
     *      @OA\Parameter(
     *          name="record_plan_id",
     *          description="department id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="member_ids",
     *          description="mảng member_ids = [1, 2, 3]",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="department_code",
     *          description="phòng ban của member, có 3 nhóm: camera, reporter, transportation",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="GET_PLAN_INFO_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/PlanInfoResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_PLAN_INFO_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function assignMembersToRecordPlan(Request $request, $record_plan_id)
    {
        $response = HandleResponse::builder("/api/assignments/$record_plan_id/assign-members")
            ->status(200);
        try {
            $memberIds = $request->member_ids;
            $departmentCode = $request->department_code;
            //nếu departmentCode = reporter | driver | camera | technical_camera => xóa tất cả members đã được phân công trước đó vào lịch
            //if (in_array($departmentCode, ["reporter", config("common.departments.transportation"), config("common.departments.camera"), config("common.departments.technical_camera")])) {
            if (in_array($departmentCode, ["reporter", config("common.departments.transportation")])) {
                $this->assignmentService->deleteMembersByRecordPlanId($record_plan_id, $memberIds);
            }
            $rs = "";
            $code_message = "ASSIGN_MEMBER_SUCCESS";
            if (!count($memberIds) && (in_array($departmentCode, [config("common.departments.transportation"), config("common.departments.camera"), config("common.departments.technical_camera")]))) {
                $code_message = "UNASSIGN_MEMBER_SUCCESS";
                // nếu là lái xe và k có nhân sự nào được phân công => bỏ qua hàm phân công
               // do nothing
            } else {
                $rs = $this->assignmentService->assignMembersToRecordPlan($record_plan_id, $departmentCode, $memberIds);
            }
            //check allocate status to change allocate and status record plan
            $this->assignmentService->checkAllocateStatus($departmentCode, $record_plan_id);
            $response->code($code_message);
            return $response->data($rs)->build();

        } catch (\Exception $e) {
            Log::error('/api/assignment');
            Log::error($e);
            $response->code(config('api_message.assignments.assign_member_failed'))
                ->status(422)
                ->error($e->getMessage());
            if ($e instanceof ValidateException) {
                $response->code($e->getMessage());
            }
            return $response->build();
        }
    }

    public function assignDeviceForRecordPlan($record_plan_id, Request $request)
    {
        $response = HandleResponse::builder("/api/assignments/$record_plan_id/assign-devices")->status(400);
        try {
            $device_ids = $request->device_ids;
            // kiểm tra số lượng device đã được assign so với số lượng đề xuất
            if ((count($device_ids) + (int)$request->devices) > (int)$request->proposal_number) {
                return $response->code(config('api_message.assignments.error_assign_device_number_higher_suggest'))->build();
            }

            if (!$request->device_ids) {
                return $response->code(config('api_message.assignments.no_data_select'))->status(422)->build();
            }
            $status = 1;
            if (auth()->user()->hasRole("super admin") || (auth()->user()->department_code == config("common.departments.device") && auth()->user()->is_manager == 1)) {
                $status = 2;
            }
            if ($request->status) {
                $status = $request->status;
            }
            $rs = $this->assignmentService->assignDevicesToRecordPlan($record_plan_id, $device_ids, $status);
            if ($status == 2) {
                $this->assignmentService->handleProposedDevicesOnOtherRecordPlan($record_plan_id, $device_ids);
            }

            $this->assignmentService->checkAllocateStatus("device", $record_plan_id);
            if (!$rs) {
                return $response->code(config("api_message.assignments.assign_device_failed"))->build();
            }
        } catch (\Exception $e) {
            Log::error('/api/assignment');
            Log::error($e);
            return $response->code(config('api_message.assignments.assign_device_failed'))
                ->status(422)
                ->error($e->getMessage())->build();
        }
        //suss
        return $response->code(config("api_message.assignments.assign_device_success"))->status(200)->build();
    }


    public function cancelDeviceForRecordPlan($record_plan_id, Request $request)
    {
        $response = HandleResponse::builder("/api/assignments/$record_plan_id/cancel-devices")->status(400);
        try {
            $device_ids = $request->device_ids;

            $this->assignmentService->handleProposedDevicesOnOtherRecordPlan($request->record_plan_id, $device_ids, config("common.status.propose_device.pending"));
            $result = $this->recordPlanService->detachDevicesByRecordPlan($request->record_plan_id, $device_ids);
            $this->assignmentService->checkAllocateStatus(__("common.departments.device"), $request->record_plan_id);
            if ($result) {
                $response->data($result);
            } else {
                return $response->code(config('api_message.assignments.cancel_devices_failed'))->build();
            }
        } catch (\Exception $e) {
            Log::error("/api/assignment/$record_plan_id/cancel-devices");
            Log::error($e);
            return $response->code(config('api_message.assignments.cancel_devices_failed'))
                ->status(422)
                ->error($e->getMessage())->build();
        }
        //suss
        return $response->status(200)->code(config('api_message.assignments.cancel_device_success'))->build();
    }


    public function updateDeviceStatus($record_plan_id, Request $request)
    {
        $response = HandleResponse::builder("/api/assignments/$record_plan_id/update-status-devices")->status(400);
        try {
            $device_ids = $request->device_ids;
            if (blank($device_ids) || !$device_ids[0]) {
                return $response->code(config("api_message.assignments.no_data_select"))->build();
            }
            $status = $request->status;
            $borrowOne = $request->borrow;
            //validate status
            if (blank($status) || !in_array($status, config('common.status.propose_device'))) {
                return $response->code(config("api_message.assignments.lending_device_failed"))->build();
            }

            // nếu mượn 1 thiết bị
            if ($borrowOne) {
                $this->deviceRepository->confirmBorrowOne($record_plan_id, $device_ids[0]);
            }

            $result = $this->assignmentService->updateDeviceStatus($request->record_plan_id, $device_ids, $status);
            $this->assignmentService->checkAllocateStatus(config("common.departments.device"), $request->record_plan_id);
            if ($status == config("common.status.propose_device.delivered")) {
                $response->code(config("api_message.assignments.lending_device_success"));
            } elseif ($status == config("common.status.propose_device.returned")) {
                $response->code(config("api_message.devices.return_success"));
            }
        } catch (\Exception $e) {
            Log::error("/api/assignment/$record_plan_id/update-status-devices");
            Log::error($e->getMessage());
            return $response->code(config('api_message.assignments.something_was_wrong'))
                ->status(422)
                ->error($e->getMessage())->build();
        }
        //suss
        return $response->status(200)->build();

    }


    public function cancelMembersForRecordPlan($record_plan_id, Request $request)
    {
        $response = HandleResponse::builder("/api/assignments/$record_plan_id/cancel-members")
            ->code(config('api_message.assignments.cancel_member_success'))
            ->status(200);
        Log::info("/api/assignments/$record_plan_id/cancel-members", $request->all());
        try {
            $memberIds = $request->member_ids;
            $rs = $this->recordPlanService->detachMembersByRecordPlan($record_plan_id, $memberIds);
             //check allocate status to change allocate and status record plan
            $this->assignmentService->checkAllocateStatus($request->department_code, $record_plan_id);
            $this->assignmentService->checkAllocateStatus("device", $record_plan_id);
            return $response->data($rs)->build();
        } catch (\Exception $e) {
            Log::error('/api/assignment');
            Log::error($e);
            $response->code(config('api_message.assignments.cancel_member_failed'))
                ->status(422)
                ->error($e->getMessage());

            if ($e instanceof ValidateException) {
                $response->code($e->getMessage());
            }
            return $response->build();
        }
    }

    public function getAvailableDevicesForRecordPlan(Request $request)
    {
        try {
            $record_plan_id = $request->record_plan_id;
            $keyword = $request->query("s", "");

            $available_devices = $this->deviceService->getAvailableDevicesByRecordPlanId($record_plan_id, $keyword);
            //        Danh sách thiết bị bị đề xuất trùng
            $proposal_device_ids = $this->deviceService->getProposalDeviceIds($record_plan_id);

            $proposal_devices = $this->deviceRepository->allQuery()->whereIn("id", $proposal_device_ids)->get();
            $returned_device_ids = $this->deviceService->getReturnedDeviceIds($record_plan_id);
            $returned_devices = $this->deviceRepository->allQuery()->whereIn("id", $returned_device_ids)->get();

            // Danh sách thiết bị sẵn sàng cho mượn (k có thiết bị được đề xuất đã hoàn trả)
            $available_devices = $available_devices->diff($returned_devices);
            //        Danh sách thiết bị sẵn sàng cho mượn (k có thiết bị được đề xuất nhưng chưa phê duyệt)
            $available_devices = $available_devices->diff($proposal_devices);

            return HandleResponse::handle(
                config('api_message.assignments.get-available-devices'),
                200,
                true,
                $available_devices,
                "/api/assignments/$record_plan_id/get-available-devices"
            );

        } catch (\Exception $e) {
            Log::error('/api/assignment');
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.assignments.get-available-devices'),
                422,
                $e->getMessage(),
                null,
                "/api/assignments/$record_plan_id/get-available-devices"
            );
        }
    }

    /**
     * Cập nhật trạng thái công việc của quay phim, lái xe theo lịch
     *
     * @param $record_plan_id
     * @param Request $request { status(1: bắt đầu | 2: kết thúc) }
     * @return JsonResponse
     */
    public function updateWorkStatusOfMember($record_plan_id, Request $request) {
        try {
            $workStatus = $request->status;
            $rs = $this->assignmentService->updateWorkStatusOfMember($record_plan_id, $workStatus);
            return HandleResponse::handle(
                $workStatus === 1 ? config('api_message.assignments.start_work_success') : config('api_message.assignments.end_work_success'),
                200,
                true,
                true,
                "/api/assignments/$record_plan_id/update-work-status"
            );

        } catch (\Exception $e) {
            Log::error('/api/update-work-status');
            Log::error($e);
            return HandleResponse::handle(
                $workStatus === 1 ? config('api_message.assignments.start_work_error') : config('api_message.assignments.end_work_error'),
                422,
                $e->getMessage(),
                null,
                "/api/assignments/$record_plan_id/update-work-status"
            );
        }
    }


}
