<?php

namespace App\Http\Controllers\API;

use App\Services\AuthService;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthAPIController extends BaseAPIController
{
    public $authService;

    public function __construct(
        AuthService $authService
    )
    {
        $this->authService = $authService;
    }

    /**
     * Xác thực người dùng qua uuid.
     *
     * @param Request $request
     * @return JsonResponse
     * * @OA\Post(
     *     path="/api/authenticate",
     *     tags={"Authenticate"},
     *     description="Xác thực người dùng qua uuid.",
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/RequestBodyAuthenticateUser")
     * ),
     *     @OA\Response(
     *     response=200,
     *     description="Get Auth Success",
     *     @OA\JsonContent(ref="#/components/schemas/ResponseSuccessAuthenticateUser")
     * ),
     *     @OA\Response(
     *     response=401,
     *     description="Unauthorized"
     * ),
     * )
     */
    public function authenticateUser(Request $request)
    {
        return $this->authService->authenticate($request);
    }

    public function mockUuid(Request $request)
    {
        return $this->authService->mockUuid($request);
    }

    /**
     * @return Authenticatable|null
     */
    public function getUser()
    {
        return auth("api")->user();
    }

}
