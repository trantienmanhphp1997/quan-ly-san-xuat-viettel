<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Traits\isValue;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * @OA\Info(
 *      version="1.0.1",
 *      title="DOCS API SX-WAP",
 *      description="this is documentation API",
 *     @OA\Contact(
 *          email="tien.nguyentat@evotek.vn"
 *      ),
 *     @OA\License(
 *          name="Server API - Laravel 8.0",
 *          url="https://laravel.com/docs/8.x"
 *      ),
 * )
 */
class BaseAPIController extends Controller
{
    use isValue;

    /**trả về thông tin người login api
     * @return Authenticatable|null
     */
    public function getUser()
    {
        return auth('api')->user();
    }
}
