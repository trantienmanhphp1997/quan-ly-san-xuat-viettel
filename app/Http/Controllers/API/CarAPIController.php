<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\CarResource;
use App\Repositories\CarRepository;
use App\ResponseHandler\HandleResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class CarAPIController extends BaseAPIController
{

    public $carRepository;

    public function __construct(CarRepository $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    /**
     * Trả về tất cả car
     *
     * @OA\Get(
     *      path="/api/cars",
     *      tags={"Cars"},
     *      description="Trả về tất cả cars.",
     *      @OA\Response(
     *          response=200,
     *          description="get car success",
     *          @OA\JsonContent(ref="#/components/schemas/ResponseGetListCarSuccess")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="get car failed",
     *          @OA\JsonContent(ref="#/components/schemas/ResponseGetListCarFailed")
     *       ),
     * )
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        try {
            $key_search = ['s' => $request->s];
            $cars = $this->carRepository->allQuery($key_search)->get();
            return HandleResponse::handle(
                config('api_message.cars.get_success'),
                200,
                null,
                CarResource::collection($cars),
                "api/cars"
            );
        } catch (\Exception $e) {
            Log::error("api/cars");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.get_failed'),
                422,
                $e->getMessage(),
                null,
                "api/cars"
            );
        }
    }

    /**
     * trả về chi tiết 1 car
     *
     * @OA\Get(
     *      path="/api/cars/find-by-id/{id}",
     *      tags={"Cars"},
     *      description="Trả về chi tiết 1 cars.",
     *      @OA\Response(
     *          response=200,
     *          description="GET_CAR_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/CarResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_CAR_FAILED",
     *          @OA\JsonContent(ref="#/components/schemas/CarResourceSchema")
     *       ),
     * )
     *
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        try {
            $cars = $this->carRepository->findOrFail($id);
            return HandleResponse::handle(
                config('api_message.cars.get_detail_success'),
                200,
                null,
                CarResource::make($cars),
                "api/cars/find-by-id"
            );
        } catch (\Exception $e) {
            Log::error("api/cars/find-by-id");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.get_detail_filed'),
                422,
                $e->getMessage(),
                null,
                "api/cars/find-by-id"
            );
        }
    }

    /**
     *
     * Tạo mới hoặc cập nhật car.
     *
     * @OA\Post(
     *      path="/api/cars/store/{id}",
     *      tags={"Cars"},
     *      description="Tạo mới hoặc cập nhật car.",
     *      @OA\Parameter(
     *          name="id",
     *          description="id car",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/CarResourceSchema")
     * ),
     *      @OA\Response(
     *          response=200,
     *          description="STORE_CAR_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="STORE_CAR_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function store(Request $request, $id)
    {
        try {
            $this->carRepository->store($request, $id);
            return HandleResponse::handle(
                config('api_message.cars.store_success'),
                200,
                null, null,
                "api/cars/store/$id"
            );
        } catch (\Exception $e) {
            Log::error("api/cars/store/$id");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.cars.store_failed'),
                422,
                $e->getMessage(),
                null,
                "api/cars/store/$id"
            );
        }
    }
}
