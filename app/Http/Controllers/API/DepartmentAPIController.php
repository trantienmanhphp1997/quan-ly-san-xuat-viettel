<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\DepartmentResource;
use App\Repositories\DepartmentRepository;
use App\Repositories\RecordPlanRepository;
use App\ResponseHandler\HandleResponse;
use App\Services\API\DepartmentAPIService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DepartmentAPIController extends BaseAPIController
{
    public $departmentRepository;
    public $recordPlanRepository;
    public $departmentAPIService;

    public function __construct(DepartmentRepository $departmentRepository, RecordPlanRepository $recordPlanRepository, DepartmentAPIService $departmentAPIService)
    {
        $this->departmentRepository = $departmentRepository;
        $this->recordPlanRepository = $recordPlanRepository;
        $this->departmentAPIService = $departmentAPIService;
    }

    /**
     * Trả về danh sách tất cả department
     *
     * @OA\Get(
     *      path="/api/departments",
     *      tags={"Departments"},
     *      description="Trả về tất cả department.",
     *      @OA\Response(
     *          response=200,
     *          description="GET_DEPARTMENT_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/DepartmentResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_DEPARTMENT_FAILED",
     *       ),
     * )
     *
     * @return JsonResponse
     */
    public function index()
    {
        try {
            $departments = $this->departmentRepository->all();
            return HandleResponse::handle(
                config('api_message.departments.get_success'),
                200,
                null,
                DepartmentResource::collection($departments),
                '/api/departments'
            );
        } catch (\Exception $e) {
            Log::error('/api/departments');
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.departments.get_failed'),
                422,
                $e->getMessage(),
                null,
                '/api/departments'
            );
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getIncludeChild(Request $request)
    {
        try {
            $departments = $this->departmentRepository->getIncludeChild($request->department_code);
            return HandleResponse::handle(
                config('api_message.departments.get_success'),
                200,
                null,
                DepartmentResource::collection($departments),
                '/api/departments/include-child'
            );
        } catch (\Exception $e) {
            Log::error('/api/departments/include-child');
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.departments.get_failed'),
                422,
                $e->getMessage(),
                null,
                '/api/departments/include-child'
            );
        }
    }

    /**
     *
     * Tìm department theo department code.
     *
     * @OA\Get(
     *      path="/api/departments/find-by-code",
     *      tags={"Departments"},
     *      description="Tìm department theo department code.",
     *     *      @OA\Parameter(
     *          name="department_code",
     *          description="department code",
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *              example="FLSD34G",
     *          )
     *      )
     *      @OA\Response(
     *          response=200,
     *          description="GET_DEPARTMENT_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/DepartmentResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_DEPARTMENT_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function findByCode(Request $request)
    {
        try {
            $department = $this->departmentRepository->findByCode($request->get('department_code'));
            return HandleResponse::handle(
                config('api_message.departments.get_detail_success'),
                200,
                null,
                DepartmentResource::make($department),
                '/api/departments/find-by-code'
            );
        } catch (\Exception $e) {
            Log::error('/api/departments/find-by-code');
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.departments.get_detail_failed'),
                422,
                $e->getMessage(),
                null,
                '/api/department/find-by-code'
            );
        }
    }

    /**
     *
     * Tìm department theo record plan id.
     *
     * @OA\Get(
     *      path="/api/departments/find-by-record-plan-id",
     *      tags={"Departments"},
     *      description="Tìm department theo department code.",
     *     *      @OA\Parameter(
     *          name="department_code",
     *          description="department code",
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *              example="FLSD34G",
     *          )
     *      )
     *      @OA\Response(
     *          response=200,
     *          description="GET_DEPARTMENT_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/DepartmentResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_DEPARTMENT_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function findByRecordPlanId(Request $request)
    {
        try {
            $department_code = $this->departmentAPIService->findCodeByRecordPlanId($request);
            $department = $this->departmentRepository->findByCode($department_code);
            return HandleResponse::handle(
                config('api_message.departments.get_detail_success'),
                200,
                null,
                DepartmentResource::make($department),
                '/api/departments/find-by-record-plan-id'
            );
        } catch (\Exception $e) {
            Log::error('/api/departments/find-by-record-plan-id');
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.departments.get_detail_failed'),
                422,
                $e->getMessage(),
                null,
                '/api/department/find-by-record-plan-id'
            );
        }
    }
}
