<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\DeviceAPIRequest;
use App\Http\Resources\DeviceBorrowingResources;
use App\Http\Resources\DeviceDetailHaveRecPlanResource;
use App\Http\Resources\DeviceResource;
use App\Http\Resources\DeviceReturnedResources;
use App\Http\Resources\RecordPlanResource;
use App\Models\DeviceCategory;
use App\Repositories\DeviceRepository;
use App\Repositories\RecordPlanDeviceXrefRepository;
use App\Repositories\RecordPlanRepository;
use App\ResponseHandler\HandleResponse;
use App\Services\API\AssignmentService;
use App\Services\API\RecordPlanAPIService;
use App\Services\DeviceService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DeviceAPIController extends BaseAPIController
{
    public $deviceRepository;
    public $recordPlanRepository;
    public $deviceService;
    public $assignmentService;
    public $recordPlanAPIService;
    public $recordPlanDeviceXrefRepository;

    /**
     * DeviceAPIController constructor.
     * @param DeviceRepository $deviceRepository
     * @param RecordPlanRepository $recordPlanRepository
     * @param DeviceService $deviceService
     * @param AssignmentService $assignmentService
     * @param RecordPlanAPIService $recordPlanAPIService
     * @param RecordPlanDeviceXrefRepository $recordPlanDeviceXrefRepository
     */
    public function __construct(
        DeviceRepository $deviceRepository,
        RecordPlanRepository $recordPlanRepository,
        DeviceService $deviceService,
        AssignmentService $assignmentService,
        RecordPlanAPIService $recordPlanAPIService,
        RecordPlanDeviceXrefRepository $recordPlanDeviceXrefRepository
    )
    {
        $this->deviceRepository = $deviceRepository;
        $this->recordPlanRepository = $recordPlanRepository;
        $this->deviceService = $deviceService;
        $this->assignmentService = $assignmentService;
        $this->recordPlanAPIService = $recordPlanAPIService;
        $this->recordPlanDeviceXrefRepository = $recordPlanDeviceXrefRepository;
    }


    /**
     * Trả về tất cả device.
     *
     * @OA\Get(
     *      path="/api/devices",
     *      tags={"Devices"},
     *      description="Trả về tất cả device.",
     *      @OA\Response(
     *          response=200,
     *          description="GET_DEVICE_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/DeviceResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_DEVICE_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        try {
            $search = [
                's' => $request->s,
                'status' => config("common.status.device.active")
            ];
            $device = $this->deviceRepository->getDevices($search);
            return HandleResponse::handle(
                config('api_message.devices.get_success'),
                200,
                null,
                DeviceResource::collection($device),
                "api/devices"
            );
        } catch (\Exception $e) {
            Log::error("api/devices");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.get_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices"
            );
        }
    }

    /**
     * Tìm tất cả device theo search keyword, start time, end time
     *
     * @OA\Post(
     *      path="/api/devices/search",
     *      tags={"Devices"},
     *      description="Tìm kiến thiết bị.",
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/RequestBodySearchDevice")
     * ),
     *      @OA\Response(
     *          response=200,
     *          description="RETURN_DEVICE_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/DeviceResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="RETURN_DEVICE_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function search(Request $request)
    {
        try {
            $start_time = $request->start_time;
            $end_time = $request->end_time;
            $keyword = $request->keyword;
            $status = $request->status;
            $devices = $this->deviceService->searchDevicesByMany($start_time, $end_time, '', $keyword, $status);
            return HandleResponse::handle(
                config('api_message.devices.search_success'),
                200,
                null,
                DeviceResource::collection($devices),
                "api/devices/search"
            );
        } catch (\Exception $e) {
            Log::error("api/devices/search");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.search_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/search"
            );
        }
    }

    /**
     * Tìm kiếm thiết bị theo id.
     *
     * @OA\Get(
     *      path="/api/devices/find-by-id/{id}",
     *      tags={"Devices"},
     *      description="Tìm kiếm thiết bị theo id.",
     *      @OA\Parameter(
     *          name="id",
     *          description="id thiết bị",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="GET_DEVICE_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/DeviceResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_DEVICE_FAILED",
     *       ),
     * )
     *
     * @param int $id
     * @return JsonResponse
     */
    public function findById($id)
    {
        try {
            $device = $this->deviceRepository->findById($id);
            return HandleResponse::handle(
                config('api_message.devices.get_detail_success'),
                200,
                null,
                DeviceResource::make($device),
                "api/devices/find-by-id/$id"
            );
        } catch (\Exception $e) {
            Log::error("api/devices/find-by-id/$id");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.get_detail_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/find-by-id/$id"
            );
        }
    }

    /**
     * Tìm kiếm chi tiết 1 thiết bị theo kế hoạch sản xuất.
     *
     * @OA\Get(
     *      path="/api/devices/detail-have-record-plan/{id}",
     *      tags={"Devices"},
     *      description="Tìm kiếm chi tiết 1 thiết bị theo kế hoạch sản xuất.",
     *      @OA\Parameter(
     *          name="id",
     *          description="id thiết bị",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="GET_DEVICE_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/DeviceResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_DEVICE_FAILED",
     *       ),
     * )
     *
     * @param int $id
     * @return JsonResponse
     */
    public function getDetailHaveRecordPlan($id)
    {
        try {
            $recordPlan = $this->deviceService->getRecordPlansForDevices($id);
            $device = $this->deviceRepository->findOrFail($id);
            $device['record_plan'] = $recordPlan;
            return HandleResponse::handle(
                config('api_message.devices.get_detail_success'),
                200,
                null,
                DeviceDetailHaveRecPlanResource::make($device),
                "api/devices/detail-have-record-plan/$id"
            );
        } catch (\Exception $e) {
            Log::error("api/devices/detail-have-record-plan/$id");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.get_detail_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/detail-have-record-plan/$id"
            );
        }
    }

    /**
     * Đề xuất thiết bị
     *
     * @OA\Post(
     *      path="/api/devices/suggest",
     *      tags={"Devices"},
     *      description="Đề xuất thiết bị.",
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/RequestBodySuggestDevice")
     * ),
     *      @OA\Response(
     *          response=200,
     *          description="SUGGEST_DEVICE_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="SUGGEST_DEVICE_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function suggest(Request $request)
    {
        try {
            $data = $request->only('offer_device_note', 'offer_device_number');
            $this->recordPlanRepository->update($data, $request->record_plan_id);
            return HandleResponse::handle(
                config('api_message.devices.suggest_success'),
                200,
                null, null,
                "api/devices/suggest"
            );
        } catch (\Exception $e) {
            Log::error("api/devices/suggest");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.suggest_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/suggest"
            );
        }
    }

    /**
     *
     * Xác nhận mượn 1 thiết bị
     *
     * @OA\Post(
     *      path="/api/devices/confirm-borrow-one",
     *      tags={"Devices"},
     *      description="Xác nhận cho mượn một thiết bị.",
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/RequestBodyConfirmBorrowDevice")
     * ),
     *      @OA\Response(
     *          response=200,
     *          description="BORROW_ONE_DEVICE_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="BORROW_ONE_DEVICE_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function confirmBorrowOne(Request $request)
    {
        try {
            $this->deviceRepository->confirmBorrowOne($request);
            return HandleResponse::handle(
                config('api_message.devices.borrow_success'),
                200,
                null, null,
                "api/devices/confirm-borrow-one"
            );
        } catch (\Exception $e) {
            Log::error("api/devices/confirm-borrow-one");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.borrow_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/confirm-borrow-one"
            );
        }
    }

    /**
     *Xác nhận nhận lại thiết bị.
     *
     * @OA\Post(
     *      path="/api/devices/confirm-return",
     *      tags={"Devices"},
     *      description="Xác nhận nhận lại thiết bị.",
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/RequestBodyConfirmBorrowDevice")
     * ),
     *      @OA\Response(
     *          response=200,
     *          description="RETURN_DEVICE_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="RETURN_DEVICE_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function confirmReturnOne(Request $request)
    {
        try {
            $this->deviceRepository->confirmReturnOne($request);
            return HandleResponse::handle(
                config('api_message.devices.return_success'),
                200,
                null, null,
                "api/devices/confirm-return"
            );
        } catch (\Exception $e) {
            Log::error("Error confirm return");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.return_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/confirm-return"
            );
        }
    }

    /**
     * Báo hỏng thiết bị.
     *
     * *  @OA\Post(
     *      path="/api/devices/report-broken",
     *      tags={"Devices"},
     *      description="Báo hỏng thiết bị.",
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/RequestBodyReportBrokenDevice")
     * ),
     *      @OA\Response(
     *          response=200,
     *          description="REPORT_BROKEN_DEVICE_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="REPORT_BROKEN_DEVICE_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function reportBroken(Request $request)
    {
        try {
            //report broken device
            $this->deviceRepository->reportBroken($request);
            return HandleResponse::handle(
                config('api_message.devices.report_broken_success'),
                200,
                null, null,
                "api/devices/report-broken"
            );
        } catch (\Exception $e) {
            Log::error("api/devices/report-broken");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.report_broken_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/report-broken"
            );
        }
    }

    /**
     * Khôi phục thiết bị hỏng.
     *
     * @OA\Post(
     *      path="/api/devices/restore-device",
     *      tags={"Devices"},
     *      description="Khôi phục thiết bị hỏng.",
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/RequestBodyReportBrokenDevice")
     * ),
     *      @OA\Response(
     *          response=200,
     *          description="RESTORE_DEVICE_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="RESTORE_DEVICE_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function restoreDevice(Request $request)
    {
        try {
            //lấy tất cả device theo search keyword
            $this->deviceRepository->restoreDevice($request);
            return HandleResponse::handle(
                config('api_message.devices.restore_success'),
                200,
                null, null,
                "api/devices/restore-device"
            );
        } catch (\Exception $e) {
            Log::error("api/devices/restore-device");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.restore_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/restore-device"
            );
        }
    }

    /**
     *
     * Tạo mới hoặc cập nhật thiết bị.
     *
     * @OA\Post(
     *      path="/api/devices/update-or-create/{id}",
     *      tags={"Devices"},
     *      description="Tạo mới hoặc cập nhật thiết bị.",
     *      @OA\Parameter(
     *          name="id",
     *          description="id thiết bị",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/RequestBodyUpdateOrCreateDevice")
     * ),
     *      @OA\Response(
     *          response=200,
     *          description="UPDATE_OR_CREATE_DEVICE_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="UPDATE_OR_CREATE_DEVICE_FAILED",
     *       ),
     * )
     *
     * @param DeviceAPIRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function updateOrCreateDevice(DeviceAPIRequest $request, $id)
    {
        $code = $this->deviceService->handleStatusCode($id);
        try {
            $this->deviceRepository->updateOrCreateDevice($request, $id);
            return HandleResponse::handle(
                $code['success'],
                200,
                null, null,
                "api/devices/update-or-create"
            );
        } catch (\Exception $e) {
            Log::error("api/devices/update-or-create");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                $code['failed'],
                422,
                $e->getMessage(),
                null,
                "api/devices/update-or-create"
            );
        }
    }

    /**
     * xóa 1 thiết bị.
     *
     * @OA\Delete(
     *      path="/api/devices/delete/{id}",
     *      tags={"Devices"},
     *      description="xóa 1 thiết bị.",
     *      @OA\Parameter(
     *          name="id",
     *          description="id thiết bị",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="DELETE_DEVICE_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="DELETE_DEVICE_FAILED",
     *       ),
     * )
     *
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy($id)
    {
        try {
            $device = $this->deviceRepository->findOrFail($id);
            $device->delete();
            return HandleResponse::handle(
                config('api_message.record_plan.delete_success'),
                200, null, null,
                "api/record-plans/delete/$id"
            );
        } catch (\Exception $e) {
            Log::error("api/device/delete/$id");
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.record_plan.delete_failed'),
                422,
                $e->getMessage(),
                null,
                "api/device/delete/$id"
            );
        }
    }

    /**
     * Danh sách record plan có đề xuất thiết bị.
     *
     * @OA\Get(
     *      path="/api/devices/list-record-plan-suggest",
     *      tags={"Devices"},
     *      description="Danh sách record plan có đề xuất thiết bị.",
     *      @OA\Response(
     *          response=200,
     *          description="GET_LIST_RECORD_PLAN_SUGGEST_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/RecordPlanResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_LIST_RECORD_PLAN_SUGGEST_FAILED",
     *       ),
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function listRecordPlanSuggest(Request $request)
    {
        try {
            $status = $request->query("status", 1);
            $query = ['s' => $request->query("s", "")];
            $recordPlans = $this->assignmentService->getSuggestByDepartmentCode(config("common.departments.device"), $query, $status);
            return HandleResponse::handle(
                config('api_message.devices.get_list_record_plan_suggest_success'),
                200, null,
                RecordPlanResource::collection($recordPlans),
                "api/device/list-record-plan-suggest"
            );
        } catch (\Exception $e) {
            Log::error("api/device/list-record-plan-suggest");
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.devices.get_list_record_plan_suggest_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/list-record-plan-suggest"
            );
        }
    }

    //TODO add docblock and log
    public function findBorrowingDevicesByRecPlan(Request $request)
    {
        $response = HandleResponse::builder('api/devices/borrowing')->status(400);
        try {
            $recordPlan = $request->input('record_plan', null);
            $borrowStatus = $request->input('status', null);
            if (!$recordPlan || !$borrowStatus || !in_array($borrowStatus, config('common.status.propose_device'))) {
                throw new \Error("Invalid request");
            }

            $keyword = $request->input('s', '');
            $data = $this->recordPlanDeviceXrefRepository->getDevicesForRecordPlan($recordPlan, [$borrowStatus], ['s' => $keyword]);
            return $response->code("Success finding borrowing devices")->data($data)->status(200)->build();
        } catch (\Exception $exception) {
            Log::error("api/devices/borrowing");
            Log::error($exception->getMessage());
            return $response->code("Fail to find borrowing devices")->status(422)->error($exception->getMessage())->build();
        }
    }

    //TODO add docblock and log
    public function findRecPlansBorrowingDevice(Request $request) {
        $response = HandleResponse::builder('api/devices/record-plan-borrowing')->status(400);
        try {
            $keyword = $request->input('s', '');
            $data = $this->recordPlanAPIService->getRecordPlanBorrowingDevices(['s' => $keyword]);

            return $response->code("Success finding borrowing devices")->data($data)->status(200)->build();
        } catch (\Exception $exception) {
            Log::error("api/devices/record-plan-borrowing");
            Log::error($exception->getMessage());
            return $response->code("Fail to find borrowing devices")->status(422)->error($exception->getMessage())->build();
        }
    }

    public function getOneDeviceHaveBorrowingOrNot(Request $request, $id)
    {
        try {
            $device = DeviceReturnedResources::make($this->deviceRepository->findOrFail($id));
            $device['status_record_plan_device_xref'] = config("common.status.propose_device.returned");
            $record_plan_xref = $this->recordPlanDeviceXrefRepository->allQuery(['device_id'=>$id, 'status'=>config("common.status.propose_device.delivered")])->get();
            $isBorrowing = collect(DeviceBorrowingResources::collection($record_plan_xref))->pluck('id')->contains($id);
            if($isBorrowing == true) {
                $device =  collect(DeviceBorrowingResources::collection($record_plan_xref))->firstWhere('id',$id);
                $device_category_name = DeviceCategory::find($device["device_category_id"])->name;
                $device['category_name']  = $device_category_name;
                $device['status_record_plan_device_xref'] = config("common.status.propose_device.delivered");
            }
            return HandleResponse::handle(
                config('api_message.devices.get_devices_borrowing_success'),
                200, null,
                $device,
                "api/device/one-borrowing"
            );
        } catch (\Exception $e) {
            Log::error("api/device/one-borrowing");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.get_devices_borrowing_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/one-borrowing"
            );
        }
    }

    public function getListDeviceBorrowing(Request $request)
    {
        try {
            $record_plan_xref = $this->deviceService->getLendingDevicesForAPI($request);
            return HandleResponse::handle(
                config('api_message.devices.get_devices_borrowing_success'),
                200, null,
                DeviceBorrowingResources::collection($record_plan_xref),
                "api/device/list-borrowing"
            );
        } catch (\Exception $e) {
            Log::error("api/device/list-borrowing");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.get_devices_borrowing_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/list-borrowing"
            );
        }
    }

    public function getListBorrowingDeviceByUserId(Request $request)
    {
        //@TODO:Check permission
        $userId = isset($request->userId) ? $request->userId : auth('api')->id();
        try {
            $devices = $this->deviceService->getListBorrowingDeviceByUserId($userId, $request->s);
            return HandleResponse::handle(
                config('api_message.devices.search_success'),
                200,
                null,
                DeviceBorrowingResources::collection($devices),
                "api/devices/borrowing-device"
            );
        } catch (\Exception $e) {
            Log::error("api/devices/search");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.search_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/borrowing-device"
            );
        }
    }

    public function requestReturnDevice(Request $request)
    {
        //@TODO:Check permission
        try {
            $this->recordPlanDeviceXrefRepository->requestToReturnDevice($request);
            return HandleResponse::handle(
                config('api_message.devices.request_return_device_success'),
                200,
                null, null,
                "api/devices/request-return-device"
            );
        } catch (\Exception $e) {
            Log::error("api/devices/request-return-device");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.devices.request_return_device_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/request-return-device"
            );
        }
    }
}
