<?php

namespace App\Http\Controllers\API;

use App\Repositories\DeviceCategoryRepository;
use App\ResponseHandler\HandleResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class DeviceCategoryAPIController extends BaseAPIController
{
    //
    public $deviceCategoryRepository;

    public function __construct(DeviceCategoryRepository $deviceCategoryRepository)
    {
        $this->deviceCategoryRepository = $deviceCategoryRepository;
    }

    /**
     *
     * Trả về tất cả category.
     *
     * @OA\Get(
     *      path="/api/categories",
     *      tags={"Categories"},
     *      description="Trả về tất cả device category.",
     *      @OA\Response(
     *          response=200,
     *          description="GET_CATEGORIES_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/DeviceCategoryResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_CATEGORIES_fAILED",
     *       ),
     * )
     *
     * Lấy tất cả thiết bị cha
     * @return JsonResponse
     */
    public function index()
    {
        try {
            $categories = $this->deviceCategoryRepository->getParentCategories();
            return HandleResponse::handle(
                config('api_message.categories.get_categories_success'),
                200,
                null,
                $categories,
                "api/devices/report-broken"
            );
        } catch (\Exception $e) {
            Log::error("api/devices/report-broken");
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.categories.get_categories_failed'),
                422,
                $e->getMessage(),
                null,
                "api/devices/report-broken"
            );
        }
    }
}
