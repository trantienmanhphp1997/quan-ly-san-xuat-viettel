<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\CarResource;
use App\Http\Resources\ExampleNoteResource;
use App\Repositories\CarRepository;
use App\Repositories\ExampleNoteRepository;
use App\ResponseHandler\HandleResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class ExampleNoteAPIController extends BaseAPIController
{

    public $exampleNoteRepository;

    public function __construct(ExampleNoteRepository $exampleNoteRepository)
    {
        $this->exampleNoteRepository = $exampleNoteRepository;
    }

    /**
     * Trả về tất cả ExampleNote
     *
     * @OA\Get(
     *      path="/api/example-notes",
     *      tags={"Example-notes"},
     *      description="Trả về tất cả notes.",
     *      @OA\Response(
     *          response=200,
     *          description="get car success",
     *          @OA\JsonContent(ref="#/components/schemas/ResponseGetListExampleNoteSuccess")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="get car failed",
     *          @OA\JsonContent(ref="#/components/schemas/ResponseGetListExampleNoteFailed")
     *       ),
     * )
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        try {
            $key_search = ['s' => $request->s];
            $notes = $this->exampleNoteRepository->allQuery($key_search)->get();
            return HandleResponse::handle(
                config('api_message.example-notes.get_success'),
                200,
                null,
                ExampleNoteResource::collection($notes),
                "api/example-notes"
            );
        } catch (\Exception $e) {
            Log::error("api/example-notes");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.example-notes.get_failed'),
                422,
                $e->getMessage(),
                null,
                "api/example-notes"
            );
        }
    }
}
