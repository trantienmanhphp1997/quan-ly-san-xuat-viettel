<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\BusyMemberResource;
use App\Http\Resources\MemberResource;
use App\Repositories\MemberRepository;
use App\Repositories\RecordPlanMemberXrefRepository;
use App\ResponseHandler\HandleResponse;
use App\Services\Interfaces\IMemberService;
use App\ViewModel\PaginateModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MemberAPIController extends BaseAPIController
{
    public $memberRepository;
    public $recordPlanMemberXrefRepository;
    public $memberService;

    /**
     * MemberAPIController constructor.
     *
     * @param MemberRepository $memberRepository
     * @param RecordPlanMemberXrefRepository $recordPlanMemberXrefRepository
     */
    public function __construct(MemberRepository $memberRepository, RecordPlanMemberXrefRepository $recordPlanMemberXrefRepository, IMemberService $memberService)
    {
        $this->memberRepository = $memberRepository;
        $this->recordPlanMemberXrefRepository = $recordPlanMemberXrefRepository;
        $this->memberService = $memberService;
    }


    /**
     *Trả về tất cả members.
     *
     * @OA\Get(
     *      path="/api/members",
     *      tags={"Members"},
     *      description="Trả về tất cả members.",
     *      @OA\Response(
     *          response=200,
     *          description="GET_MEMBERS_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/MemberResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_MEMBERS_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getMember(Request $request)
    {
        try {
            $members = $this->memberRepository->getMember($request);
            return HandleResponse::handle(
                config('api_message.member.get_members_success'),
                200,
                null,
                MemberResource::collection($members),
                '/api/members'
            );
        } catch (\Exception $e) {
            Log::error("Error get members");
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.member.get_members_failed'),
                422,
                $e->getMessage(),
                null,
                '/api/members'
            );
        }
    }

    /**
     * Tìm thành viên theo uuid.
     *
     * @OA\Post(
     *      path="/api/members/find-by-uuid",
     *      tags={"Members"},
     *      description="Tìm thành viên theo uuid.",
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/RequestBodyUpdateOrCreateMember")
     * ),
     *      @OA\Response(
     *          response=200,
     *          description="FIND_BY_UUID_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/MemberResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="FIND_BY_UUID_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function findByUuid(Request $request)
    {
        try {
            $member = $this->memberRepository->findByUuid($request);
            return HandleResponse::handle(
                config('api_message.member.get_members_detail_success'),
                200,
                null,
                $member,
                '/api/members/detail'
            );
        } catch (\Exception $e) {
            Log::error('/api/members/detail');
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.member.get_members_detail_failed'),
                422,
                $e->getMessage(),
                null,
                '/api/members/detail'
            );
        }
    }

    /**
     * Get user by id
     *
     * @param Request $request {department_code, id}
     * @return JsonResponse
     */
    public function findById(Request $request)
    {
        try {
            switch ($request->department_code) {
                case config('common.departments.transportation'): {
                    $member = $this->memberRepository->findDriverById($request->id);
                    break;
                }
                case config('common.departments.camera'): {
                    break;
                }
                case config('common.departments.device'): {
                    break;
                }
                case config('common.departments.production'): {
                    break;
                }
            }
            return HandleResponse::handle(
                config('api_message.member.get_members_detail_success'),
                200,
                null,
                $member,
                '/api/members/find-by-id'
            );
        } catch (\Exception $e) {
            Log::error('/api/members/detail');
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.member.get_members_detail_failed'),
                422,
                $e->getMessage(),
                null,
                '/api/members/find-by-id'
            );
        }
    }

    public function findBusyMembers(Request $request)
    {
        $response = HandleResponse::builder('/api/members/find-busy-by-department')->status(400);
        try {
            $record_plans = $this->memberService->getAssignmentsByDepartmentCode($request, $request->department_code);
            return $response->status(200)
                ->code(config('api_message.member.get_busy_members_success'))
                ->data(BusyMemberResource::collection($record_plans))->build();
        } catch (\Exception $e) {
            Log::error('/api/members/find-busy-by-department');
            Log::error($e->getMessage());
            return $response->status(422)->code(config('api_message.member.get_busy_members_failed'))->error($e->getMessage())->build();
        }
    }

    public function getReportersForRecordPlan(Request $request) {
        $response = HandleResponse::builder('/api/members/get-reporters-for-record-plan')->status(400);
        try {
            $record_plans = $this->memberService->getReportersByDepartmentCode($request->department_code, $request->s ?? '');
//            $record_plans = $this->memberService->getReportersForRecPlan($request);
            return $response->status(200)
                ->code(config('api_message.member.get-reporters-for-record-plan'))
                ->data(MemberResource::collection($record_plans))->build();
        } catch (\Exception $e) {
            Log::error('/api/members/find-busy-by-department');
            Log::error($e->getMessage());
            return $response->status(422)->code(config('api_message.member.get-reporters-for-record-plan'))->error($e->getMessage())->build();
        }
    }

    public function getMembersForRecordPlan(Request $request) {
        $response = HandleResponse::builder('/api/members/get-members-for-record-plan')->status(400);
        try {
            $members = $this->memberService->getMembersForRecPlan($request);
            return $response->status(200)
                ->code(config('api_message.member.get-drivers-for-record-plan'))
                ->data($members)->build();
        } catch (\Exception $e) {
            Log::error('/api/members/get-members-for-record-plan');
            Log::error($e->getMessage());
            return $response->status(422)->code(config('api_message.member.get-drivers-for-record-plan'))->error($e->getMessage())->build();
        }
    }

}
