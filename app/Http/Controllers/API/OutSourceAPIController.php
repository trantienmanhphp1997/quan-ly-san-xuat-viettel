<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\OutsourceResource;
use App\Repositories\OutsourceRepository;
use App\ResponseHandler\HandleResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OutSourceAPIController extends BaseAPIController
{
    public $outsourceRepository;

    public function __construct(OutsourceRepository $outsourceRepository)
    {
        $this->outsourceRepository = $outsourceRepository;
    }

    public function index(Request $request)
    {
        try {
            $outsources = $this->outsourceRepository->allQuery($request->input())->get();
            return HandleResponse::handle(
                config('api_message.outsources.get_success'),
                200,
                null,
                OutsourceResource::collection($outsources),
                "api/outsources"
            );
        } catch (\Exception $e) {
            Log::error("api/outsources");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.outsources.get_failed'),
                422,
                $e->getMessage(),
                null,
                "api/outsources"
            );
        }
    }
}
