<?php

namespace App\Http\Controllers\API;

use App\Repositories\PlanInfoRepository;
use App\ResponseHandler\HandleResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PlanInfoAPIController extends BaseAPIController
{
    public $planInfoRepository;

    public function __construct(PlanInfoRepository $planInfoRepository)
    {
        $this->planInfoRepository = $planInfoRepository;
    }

    /**
     * Trả về tất cả plan info
     *
     * @OA\Get(
     *      path="/api/plan-info",
     *      tags={"Plan info"},
     *      description="Trả về tất cả plan info.",
     *      @OA\Response(
     *          response=200,
     *          description="GET_PLAN_INFO_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/PlanInfoResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_PLAN_INFO_FAILED",
     *       ),
     * )
     *
     * @return JsonResponse
     */
    public function index()
    {
        try {
            $planInfo = $this->planInfoRepository->allQuery()->get();
            return HandleResponse::handle(
                config('api_message.plan-info.get_plan_info_success'),
                200,
                null,
                $planInfo,
                '/api/plan-info'
            );
        } catch (\Exception $e) {
            Log::error('/api/plan-info');
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.plan-info.get_plan_info_failed'),
                422,
                $e->getMessage(),
                null,
                '/api/plan-info'
            );
        }
    }

    /**
     *Tìm kiếm plan info theo department
     *
     * @OA\Get(
     *      path="/api/find-by-department",
     *      tags={"Plan info"},
     *      description="Trả về plan info.",
     *      @OA\Parameter(
     *          name="department_id",
     *          description="department id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="GET_PLAN_INFO_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/PlanInfoResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_PLAN_INFO_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function findByDepartment(Request $request)
    {
        try {
            $plansInfo = $this->planInfoRepository->allQuery()->where("department_id", $request->department_id)->get();
            return HandleResponse::handle(
                config('api_message.plan-info.find_plan_success'),
                200,
                null,
                $plansInfo,
                '/api/plan-info/find-by-department'
            );
        } catch (\Exception $e) {
            Log::error('/api/plan-info');
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.plan-info.find_plan_failed'),
                422,
                $e->getMessage(),
                null,
                '/api/plan-info/find-by-department'
            );
        }
    }
}
