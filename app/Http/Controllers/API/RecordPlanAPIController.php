<?php

namespace App\Http\Controllers\API;

use App\Exceptions\ValidateException;
use App\Http\Requests\API\RecordPlanAPIRequest;
use App\Http\Resources\RecordPlanResource;
use App\Models\RecordPlanMemberXref;
use App\Repositories\DepartmentRepository;
use App\Repositories\MemberRepository;
use App\Repositories\RecordPlanRepository;
use App\ResponseHandler\HandleResponse;
use App\Services\API\RecordPlanAPIService;
use App\Services\Interfaces\IRecordPlanService;
use App\Services\MemberService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RecordPlanAPIController extends BaseAPIController
{
    public $recordPlanRepository;
    public $memberRepository;
    public $recordPlanAPIService;
    public $recordPlanService;
    public $departmentRepository;
    public $memberService;

    public function __construct
    (
        RecordPlanRepository $recordPlanRepository,
        MemberRepository $memberRepository,
        RecordPlanAPIService $recordPlanAPIService,
        IRecordPlanService $recordPlanService,
        DepartmentRepository $departmentRepository,
        MemberService $memberService
    )
    {
        $this->recordPlanRepository = $recordPlanRepository;
        $this->memberRepository = $memberRepository;
        $this->recordPlanAPIService = $recordPlanAPIService;
        $this->departmentRepository = $departmentRepository;
        $this->recordPlanService = $recordPlanService;
        $this->memberService = $memberService;
    }

    /**
     * Trả về lịch làm việc được phân công của user theo start time và end time.
     *
     * @OA\Get(
     *      path="/api/my-assignments?start_time={start_time}&end_time={end_time}&time_assign={time_assign}",
     *      tags={"Assignments"},
     *      description="Trả về lịch làm việc được phân công của user theo start time và end time.",
     *      @OA\Parameter(
     *          name="start_time",
     *          description="Thời gian đang diễn ra chương trình mong muốn",
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *              format="datetime",
     *               example="2020-12-30 07:53:09",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="end_time",
     *          description="Thời gian kết thúc chương trình",
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *              format="datetime",
     *              example="2020-12-30 07:53:09",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="time_assign",
     *          description="Loại assignment mong muốn (today, tomorrow, week)",
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *              example="week",
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="GET_ASSIGNMENT_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/RecordPlanResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_ASSIGNMENT_FAILED"
     *      )
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getAssignmentList(Request $request)
    {
        $response = HandleResponse::builder("/api/assignments")
            ->code(config('api_message.record_plan.get_assignment_success'))
            ->status(200);
        try {
            $assignmentList = $this->recordPlanRepository->assignmentList($request);
            return $response->data($assignmentList)->build();
        } catch (Exception $e) {
            Log::error('/api/assignments');
            Log::error($e);
            return $response->code(config('api_message.record_plan.get_assignment_failed'))
                ->error($e->getMessage())->status(422)->build();
        }
    }

    /**
     * Trả về lịch làm việc được phân công của Login User theo start time và end time.
     *
     * @OA\Get(
     *      path="/api/my-assignments?start_time={start_time}&end_time={end_time}&time_assign={time_assign}",
     *      tags={"Assignments"},
     *      description="Trả về lịch làm việc được phân công của user theo start time và end time.",
     *      @OA\Parameter(
     *          name="start_time",
     *          description="Thời gian đang diễn ra chương trình mong muốn",
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *              format="datetime",
     *               example="2020-12-30 07:53:09",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="end_time",
     *          description="Thời gian kết thúc chương trình",
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *              format="datetime",
     *              example="2020-12-30 07:53:09",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="time_assign",
     *          description="Loại assignment mong muốn (today, tomorrow, week)",
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *              example="week",
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="GET_ASSIGNMENT_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/RecordPlanResourceSchema")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_ASSIGNMENT_FAILED"
     *      )
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getMyAssignmentList(Request $request)
    {
        try {
            $memberId = auth("api")->user()->id;
            $assignmentList = $this->memberService->getAssignmentsByMember($request, $memberId);
            $assignments = [];

            $loginUser = auth()->user();
            $driverStatus = RecordPlanMemberXref::where(["member_id" => $loginUser->id])->whereIn("record_plan_id", $assignmentList->pluck("record_plan_id")->toArray())->get();
            $driverStatusMap = [];
            foreach ($driverStatus as $driver) {
                $driverStatusMap[$driver->record_plan_id] = $driver->transportation_status;
            }
            foreach ($assignmentList as $assignment) {
                $transportation_status = $driverStatusMap[$assignment->record_plan_id];
                $assignment->record_plan->{"transportation_status"} = $transportation_status;
                $assignments[] = $assignment->record_plan;
            }
            return HandleResponse::handle(
                config('api_message.record_plan.get_assignment_success'),
                200,
                null,
                $assignments,
                '/api/my-assignments'
            );
        } catch (Exception $e) {
            Log::error('/api/my-assignments');
            Log::error($e);
            return HandleResponse::handle(
                config('api_message.record_plan.get_assignment_failed'),
                422,
                $e->getMessage(),
                null,
                '/api/my-assignments'
            );
        }
    }

    /**
     *
     * Trả về toàn bộ record plan theo start time và end time.
     *
     * @OA\Get(
     *      path="/api/record-plans?start_time={start_time}&end_time={end_time}&s={keyword}",
     *      tags={"Record Plans"},
     *      description="Trả về toàn bộ record plan theo start time và end time.",
     *      @OA\Parameter(
     *          name="start_time",
     *          description="Thời gian bắt đầu mong muốn",
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *              format="datetime",
     *              example="2020-12-30 07:53:09",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="end_time",
     *          description="Thời gian kết thúc mong muốn",
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *              format="datetime",
     *              example="2020-12-30 07:53:09",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="s",
     *          description="keyword tìm kiếm tên chương trình",
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *              example="căng chải",
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="GET_RECORD_PLAN_SUCCESS",
     *          @OA\JsonContent(ref="#/components/schemas/ResponseSuccessIndex")
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="GET_RECORD_PLAN_FAILED",
     *       ),
     * )
     *
     * Trả về toàn bộ record plan theo start time và end time
     * Display a listing of the resource.
     * return all records plan in specific time
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $response = HandleResponse::builder("/api/record-plans")->status(200);
        Log::info("/api/record-plans", $request->all());
        $query = [
            "s" => $request->s,
            "status" => $request->status,
            "department_code" => $request->department_code
        ];

        // nếu loggin user là quản lý thì lấy ra danh sách lịch sx của phòng ban mình và phòng ban trực thuộc
        // nếu loggin user không phải là quản lý thì lấy ra danh sách lịch sản xuất mà user đó tạo
        $creator = auth("api")->user()->is_manager == 0 ? auth("api")->id() : null;
        Log::info("/api/record-plans", $query);
        try {
            $data = $this->recordPlanService->searchRecordPlan($query, null, null, $creator);
            return $response->data($data)->build();
        } catch (Exception $e) {
            Log::error('/api/record-plans', [$e]);
            $response->status(422)->code(config('api_message.record_plan.get_record_plan_failed'))->error($e->getMessage());

            if ($e instanceof ValidateException) {
                $response->code($e->getMessage());
            }
            return $response->build();
        }
    }


    /**
     * Tạo mới hoặc cập nhật kế hoạch sản xuất.
     *
     * @OA\Post(
     *      path="/api/record-plans/update-or-create/{id}",
     *      tags={"Record Plans"},
     *      description="Tạo mới hoặc cập nhật kế hoạch sản xuất.",
     *      @OA\Parameter(
     *          name="id",
     *          description="id record plans",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/RequestBodyUpdateOrCreate")
     * ),
     *      @OA\Response(
     *          response=200,
     *          description="UPDATE_OR_CREATE_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="UPDATE_OR_CREATE_FAILED",
     *       ),
     * )
     *
     *
     * Store a newly created resource in storage.
     *
     * @param RecordPlanAPIRequest $request
     * @return JsonResponse
     */
    public function updateOrCreate(RecordPlanAPIRequest $request, $id)
    {
        $code = $this->recordPlanAPIService->handleStatusCode($id);
        // validate data
        $request->validated();
        try {
            $department = $this->departmentRepository->findOrFail($request->departmentId);
            $data = $request->except('planInfoId', 'departmentId');
            if ($data['address_type'] == config("common.address_type.useStage")) {
                // kiểm tra lịch sản xuất đã được đặt lịch ở phòng quay
                $checkRecordPlanInSatge = $this->recordPlanService->checkRecordPlanExistsInStage($data['address'], $data['start_time'], $id);
                if ($checkRecordPlanInSatge["status"] === "FOUND") {
                    return HandleResponse::handle(
                        config('api_message.record_plan.room_busy'),
                        422,
                        "Trường quay ".$checkRecordPlanInSatge["stage"]." đã được lịch ". $checkRecordPlanInSatge["record_plan"]." đăng ký tại khung giờ trên",
                        null,
                        "/api/record-plans/update-or-create"
                    );
                }
            }
            $data['is_allday'] = $request->is_allday ? 1 : 0;
            $data['plan_id'] = $request->planInfoId;
            $data['department_code'] = $department->code;

            if(trim($request->offer_device_note)){
                $data["offer_device_number"] = 1;
            }

            //set status if create record plan
            if ($id == 0) {
                $data["reporter_allocate_status"] = 1;
                $data["camera_allocate_status"] = 1;
                $data["technical_camera_allocate_status"] = 1;
                $data["device_allocate_status"] = 1;
                $data["transportation_allocate_status"] = 1;

                if (!$request->offer_reporter_number) {
                    $data["reporter_allocate_status"] = 2;
                }

                if (!$request->offer_camera_number) {
                    $data["camera_allocate_status"] = 2;
                }
                if (!$request->offer_technical_camera_number) {
                    $data["technical_camera_allocate_status"] = 2;
                }

                if (!$request->offer_transportation_number) {
                    $data["transportation_allocate_status"] = 2;
                }

                if (!$request->offer_device_number) {
                    $data["device_allocate_status"] = 2;
                }
            } else {
                //if update record plan
                $record_plan = $this->recordPlanRepository->allQuery()->findOrFail($id);
                /* vì thay đổi logic gán phóng viên khi tạo mới lịch nên tạm thời bỏ check số lượng đề xuất phóng viên và set thẳng reporter_allocate_status = 2 */
//                $data["reporter_allocate_status"] = $this->recordPlanService->checkResourceAllocateStatusToUpdate($record_plan, "reporter", $request);
                $data["camera_allocate_status"] = $this->recordPlanService->checkResourceAllocateStatusToUpdate($record_plan, "camera", $request);
                $data["technical_camera_allocate_status"] = $this->recordPlanService->checkResourceAllocateStatusToUpdate($record_plan, "technical_camera", $request);
                $data["transportation_allocate_status"] = $this->recordPlanService->checkResourceAllocateStatusToUpdate($record_plan, "transportation", $request);
                $data["device_allocate_status"] = $this->recordPlanService->checkResourceAllocateStatusToUpdate($record_plan, "device", $request);
            }

            $record_plan = $this->recordPlanRepository->updateOrCreate($data, $id);
            $this->recordPlanService->checkRecordPlanGeneralStatus($record_plan->id);

            return HandleResponse::handle(
                $code["success"],
                200,
                null,
                RecordPlanResource::make($record_plan),
                '/api/record-plans/update-or-create'
            );
        } catch (Exception $e) {
            Log::error('/api/record-plans/update-or-create');
            Log::error($e->getMessage());
            return HandleResponse::handle(
                $code["failed"],
                422,
                $e->getMessage(),
                null,
                '/api/record-plans/update-or-create'
            );
        }
    }

    /**
     * Tìm chương trình theo id.
     *
     * @OA\Get(
     *      path="/api/record-plans/find-by-id/{id}",
     *      tags={"Record Plans"},
     *      description="Tìm chương trình theo id.",
     *      @OA\Parameter(
     *          name="id",
     *          description="id record plans",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="FIND_RECORD_PLAN_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="FIND_RECORD_PLAN_FAILED",
     *       ),
     * )
     *
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function findById($id)
    {
        try {
            $record_plan = $this->recordPlanRepository->findOrFail($id);
            return HandleResponse::handle(
                config('api_message.record_plan.get_detail_success'),
                200,
                null,
                RecordPlanResource::make($record_plan),
                "api/record-plans/find-by-id/$id"
            );
        } catch (\Exception $e) {
            Log::error("api/record-plans/find-by-id/$id");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.record_plan.get_detail_failed'),
                422,
                $e->getMessage(),
                null,
                "api/record-plans/find-by-id/$id"
            );
        }
    }

    /**
     *Xóa chương trình theo id.
     *
     * @OA\Delete(
     *      path="/api/record-plans/delete/{id}",
     *      tags={"Record Plans"},
     *      description="Xóa chương trình theo id.",
     *      @OA\Parameter(
     *          name="id",
     *          description="id record plans",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="DELETE_DEVICE_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="DELETE_DEVICE_FAILED",
     *       ),
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy($id)
    {
        try {
            $recordPlanIds = [$id];
            $this->recordPlanService->destroy($recordPlanIds);
            return HandleResponse::handle(
                config('api_message.record_plan.delete_success'),
                200, null, null,
                "api/record-plans/delete/$id"
            );
        } catch (\Exception $e) {
            Log::error("api/record-plans/delete/$id");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.record_plan.delete_failed'),
                422,
                $e->getMessage(),
                null,
                "api/record-plans/delete/$id"
            );
        }
    }

    /**
     * Đề xuất thành viên.
     *
     * @OA\Post(
     *      path="/api/record-plans/suggest-member",
     *      tags={"Record Plans"},
     *      description="Đề xuất thành viên.",
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/RequestBodySuggestMember")
     * ),
     *      @OA\Response(
     *          response=200,
     *          description="SUGGEST_MEMBER_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="SUGGEST_MEMBER_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function suggestMember(Request $request)
    {
        try {
            $this->recordPlanAPIService->handleSuggestMember($request);
            return HandleResponse::handle(
                config('api_message.record_plan.suggest_success'),
                200,
                null, null,
                "api/record-plans/suggest-member"
            );
        } catch (\Exception $e) {
            Log::error("api/record-plans/suggest-member");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.record_plan.suggest_failed'),
                422,
                $e->getMessage(),
                null,
                "api/record-plans/suggest-member"
            );
        }
    }

    /**
     *Xác nhận cho mượn tất cả thiết bị của 1 chương trình.
     *
     * @OA\Post(
     *      path="/api/record-plans/confirm-borrow-all/{id}",
     *      tags={"Record Plans"},
     *      description="Xác nhận cho mượn tất cả thiết bị của 1 chương trình.",
     *      @OA\Parameter(
     *          name="id",
     *          description="id record plans",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/RequestBodyConfirmBorrowAll")
     * ),
     *      @OA\Response(
     *          response=200,
     *          description="CONFIRM_BORROW_ALL_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="CONFIRM_BORROW_ALL_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function confirmBorrowAll(Request $request, $id)
    {
        try {
            $this->recordPlanRepository->confirmBorrowAll($request, $id);
            return HandleResponse::handle(
                config('api_message.record_plan.borrow_success'),
                200,
                null, null,
                "api/record-plans/confirm-borrow-all"
            );
        } catch (\Exception $e) {
            Log::error("api/record-plans/confirm-borrow-all");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.record_plan.borrow_failed'),
                422,
                $e->getMessage(),
                null,
                "api/record-plans/confirm-borrow-all"
            );
        }
    }

    /**
     *Xác nhận trả lại tất cả thiết bị của 1 chương trình.
     *
     * @OA\Post(
     *      path="/api/record-plans/confirm-return-all/{id}",
     *      tags={"Record Plans"},
     *      description="Xác nhận trả lại tất cả thiết bị của 1 chương trình.",
     *      @OA\Parameter(
     *          name="id",
     *          description="id record plans",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *     @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/RequestBodyConfirmBorrowAll")
     * ),
     *      @OA\Response(
     *          response=200,
     *          description="CONFIRM_RETURN_ALL_SUCCESS",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="CONFIRM_RETURN_ALL_FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function confirmReturnAll(Request $request, $id)
    {
        try {
            $this->recordPlanRepository->confirmReturnAll($request, $id);
            return HandleResponse::handle(
                config('api_message.record_plan.return_all_success'),
                200,
                null, null,
                "api/record-plans/confirm-return-all/$id"
            );
        } catch (\Exception $e) {
            Log::error("api/record-plans/confirm-return-all/$id");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.record_plan.return_all_failed'),
                422,
                $e->getMessage(),
                null,
                "api/record-plans/confirm-return-all/$id"
            );
        }
    }

    /**
     *Kiểm tra xem đã nhận được thiết bị hay chưa.
     *
     * @OA\Post(
     *      path="/api/record-plans/check-received-device/{id}",
     *      tags={"Record Plans"},
     *      description="Kiểm tra xem đã nhận được thiết bị hay chưa.",
     *      @OA\Parameter(
     *          name="id",
     *          description="id record plans",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="int",
     *              example="1",
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="RECEIVED OR NO RECEIVED",
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="CHECK RECEIVED FAILED",
     *       ),
     * )
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function checkReceivedDevice(Request $request, $id)
    {
        try {
            $checkReceived = $this->recordPlanRepository->checkReceivedDevice($request, $id);
            if ($checkReceived === true) {
                return HandleResponse::handle(
                    config('api_message.record_plan.received'),
                    200,
                    null, $checkReceived,
                    "api/record-plans/check-received-device/$id"
                );
            } else {
                return HandleResponse::handle(
                    config('api_message.record_plan.no_received'),
                    200,
                    null, $checkReceived,
                    "api/record-plans/check-received-device/$id"
                );
            }
        } catch (\Exception $e) {
            Log::error("api/record-plans/check-received-device/$id");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.record_plan.check_received_failed'),
                422,
                $e->getMessage(),
                null,
                "api/record-plans/check-received-device/$id"
            );
        }
    }

    public function getAssignComing(Request $request)
    {
        $response = HandleResponse::builder("/api/record-plans/assign-coming")->status(200);
        Log::info("/api/record-plans/assign-coming", $request->all());
        $query = [
            "s" => $request->s,
            "status" => $request->status,
            "department_code" => $request->department_code
        ];
        Log::info("/api/record-plans/assign-coming", $query);
        try {
            $data = $this->recordPlanService->getAssignComing($request);
            return $response->data($data)->build();
        } catch (Exception $e) {
            Log::error('/api/record-plans/assign-coming', [$e]);
            $response->status(422)->code(config('api_message.record_plan.get_record_plan_failed'))->error($e->getMessage());

            if ($e instanceof ValidateException) {
                $response->code($e->getMessage());
            }
            return $response->build();
        }
    }

    public function updateGeneralStatus($id, Request $request){
        $response = HandleResponse::builder("/api/record-plans/$id/update-status")->status(200);
        Log::info("/api/record-plans/$id/update-status", $request->all());
        try {
            $data = $this->recordPlanService->updateGeneralStatus($id, $request);
            return $response->data($data)->build();
        } catch (Exception $e) {
            Log::error("/api/record-plans/$id/update-status", [$e]);
            $response->status(422)->code(config('api_message.record_plan.update_status'))->error($e->getMessage());

            if ($e instanceof ValidateException) {
                $response->code($e->getMessage());
            }
            return $response->build();
        }
    }
}
