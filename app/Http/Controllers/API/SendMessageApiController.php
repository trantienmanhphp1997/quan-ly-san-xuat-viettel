<?php

namespace App\Http\Controllers\API;

use App\Services\MochaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SendMessageApiController extends BaseAPIController
{
    public function sendMessage(Request $request){
        Log::info("Send redirect message", [$request->all()]);
        MochaService::sendMessageToUser($request->service_id, $request["content"], $request->msisdns, $request->label, $request->deeplink);
    }
}

