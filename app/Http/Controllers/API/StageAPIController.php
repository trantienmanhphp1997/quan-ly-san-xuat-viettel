<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\CarResource;
use App\Repositories\StageRepository;
use App\ResponseHandler\HandleResponse;
use App\Services\Interfaces\IStageService;
use App\Services\StageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StageAPIController extends BaseAPIController
{

    public $stageRepository;
    public $stageService;

    public function __construct(StageRepository $stageRepository, StageService $stageService)
    {
        $this->stageRepository = $stageRepository;
        $this->stageService = $stageService;
    }
    public function index(Request $request)
    {
        try {
            $stages = $this->stageService->getAllStages($request);
            return HandleResponse::handle(
                config('api_message.stages.success'),
                200,
                null,
                $stages,
                "api/stages"
            );
        } catch (\Exception $e) {
            Log::error("api/cars");
            Log::error($e->getMessage());
            return HandleResponse::handle(
                config('api_message.stage.get_failed'),
                422,
                $e->getMessage(),
                null,
                "api/stages"
            );
        }
    }

}
