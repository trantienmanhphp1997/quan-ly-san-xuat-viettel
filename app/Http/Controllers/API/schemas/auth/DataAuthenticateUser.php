<?php

/**
 * @OA\Schema(
 *      title="Data Authenticate",
 *      description="Data Authenticate",
 *      type="object",
 * )
 */
class DataAuthenticateUser
{
    /**
     * @OA\Property(
     *     title="TOKEN",
     *     description="token sau khi xác thực",
     *     format="string",
     *     example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9hdXRoZW50aWNhdGUiLCJpYXQiOjE2MDg5NTE4MzQsImV4cCI6MTYwOTU1NjYzNCwibmJmIjoxNjA4OTUxODM0LCJqdGkiOiJ0dHdWMDZkM09KM1JNMlhrIiwic3ViIjoxMCwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyIsImlkIjoxMCwidXVpZCI6IjQ5YjU1ZWIyNTI1NmEzZjFhZTQ4NzE4N2JjZjdhMjhmIiwicGhvbmVfbnVtYmVyIjoiMDk3NzE4OTk0NiIsImlzX21hbmFnZXIiOjEsImRlcGFydG1lbnRfY29kZSI6InRyYW5zcG9ydGF0aW9uIn0.eUkXscosjjEbQIdWHLfk6PRq9dNqvt-PRMxAaJrjYEw"
     * )
     *
     * @var string
     */
    public $access_token;

    /**
     * @OA\Property(
     *     title="TOKEN_TYPE",
     *     description="loại token",
     *     format="string",
     *     example="bearer"
     * )
     *
     * @var string
     */
    public $token_type;
}
