<?php

namespace App\Http\Requests\API;

/**
 * @OA\Schema(
 *      title="Request Body Auth API",
 *      description="Auth API Request",
 *      type="object",
 *      required={"uuid"}
 * )
 */

class RequestBodyAuthenticateUser
{
    /**
     * @OA\Property(
     *      description="uuid của người dùng",
     *      example="49b55eb25256a3f1ae487187bcf7a28f"
     * )
     *
     * @var string
     */
    public $uuid;
}
