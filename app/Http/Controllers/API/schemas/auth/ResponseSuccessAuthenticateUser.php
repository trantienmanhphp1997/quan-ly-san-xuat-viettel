<?php

namespace App\Http\Requests\API;

use DataAuthenticateUser;

/**
 * @OA\Schema(
 *      title="Response Success Auth API",
 *      description="Get token success with status 200",
 *      type="object",
 * )
 */
class ResponseSuccessAuthenticateUser
{
    /**
     * @OA\Property(
     *     title="code",
     *     description="mã code",
     *     example="GET_AUTH_SUCCESS"
     * )
     *
     * @var string
     */
    public $code;

    /**
     * @OA\Property(
     *     title="status",
     *     description="status code",
     *     example=200
     * )
     *
     * @var integer
     */
    public $status;

    /**
     * @OA\Property(
     *     title="data",
     *     description="data",
     * )
     *
     * @var DataAuthenticateUser[]
     */
    public $data;

    /**
     * @OA\Property(
     *     title="path api",
     *     description="path api",
     *     example="api/authenticate"
     * )
     *
     * @var string
     */
    public $path;

    /**
     * @OA\Property(
     *     title="path api",
     *     description="path api",
     *     format="timestamp",
     *     example="2021-01-04T03:52:39.003251Z"
     * )
     *
     * @var string
     */
    public $timestamp;
}
