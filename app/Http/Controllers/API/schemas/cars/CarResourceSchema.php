<?php

/**
 * @OA\Schema(
 *      title="Data Car",
 *      description="DataCar.php",
 *      type="object",
 * )
 */
class CarResourceSchema
{
    /**
     * @OA\Property(
     *      description="id car",
     *      example="1"
     * )
     *
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *      description="id của người lái",
     *      example="1"
     * )
     *
     * @var integer
     */
    public $driver_id;

    /**
     * @OA\Property(
     *      description="Tên xe ",
     *      example="Xe 4 đầu rồng"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      description="Model ",
     *      example="Delpha Lang"
     * )
     *
     * @var string
     */
    public $model;

    /**
     * @OA\Property(
     *      description="Brand ",
     *      example="Ford"
     * )
     *
     * @var string
     */
    public $brand;

    /**
     * @OA\Property(
     *      description="Màu xe ",
     *      example="red"
     * )
     *
     * @var string
     */
    public $color;

    /**
     * @OA\Property(
     *      description="Số lượng ghế ",
     *      example="10"
     * )
     *
     * @var integer
     */
    public $number_of_seats;

    /**
     * @OA\Property(
     *      description="Biển số xe ",
     *      example="10SDFS23"
     * )
     *
     * @var string
     */
    public $license_plate;

    /**
     * @OA\Property(
     *      description="Trạng thái ",
     *      example="1"
     * )
     *
     * @var integer
     */
    public $status;

    /**
     * @OA\Property(
     *      description="Mô tả ",
     *      example="đây là xe"
     * )
     *
     * @var string
     */
    public $description;

    /**
     * @OA\Property(
     *      description="ghi chú ",
     *      example="đây là ghi chú"
     * )
     *
     * @var string
     */
    public $note;

    /**
     * @OA\Property(
     *      description="người lái xe ",
     *      example={}
     * )
     *
     * @var object
     */
    public $transportation;
}
