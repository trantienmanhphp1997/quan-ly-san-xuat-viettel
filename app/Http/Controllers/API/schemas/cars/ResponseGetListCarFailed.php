<?php

namespace App\Http\Requests\API;

use CarResourceSchema;

/**
 * @OA\Schema(
 *      title="Car Resource",
 *      description="Car Resource",
 *      type="object",
 * )
 */
class ResponseGetListCarFailed
{
    /**
     * @OA\Property(
     *     title="code",
     *     description="mã code",
     *     example="GET_CARS_FAILED"
     * )
     *
     * @var string
     */
    public $code;

    /**
     * @OA\Property(
     *     title="status",
     *     description="status code",
     *     example=422
     * )
     *
     * @var integer
     */
    public $status;

    /**
     * @OA\Property(
     *     title="error message",
     *     description="error message",
     *     example="Lỗi cù lừn khi lấy danh sách xe"
     * )
     *
     * @var string
     */
    public $error;

    /**
     * @OA\Property(
     *     title="path api",
     *     description="path api",
     *     example="api/cars"
     * )
     *
     * @var string
     */
    public $path;

    /**
     * @OA\Property(
     *     title="path api",
     *     description="path api",
     *     format="timestamp",
     *     example="2021-01-04T03:52:39.003251Z"
     * )
     *
     * @var string
     */
    public $timestamp;
}
