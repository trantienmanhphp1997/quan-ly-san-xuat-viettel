<?php

namespace App\Http\Requests\API;

use CarResourceSchema;

/**
 * @OA\Schema(
 *      title="Car Resource",
 *      description="Car Resource",
 *      type="object",
 * )`
 */
class ResponseGetListCarSuccess
{
    /**
     * @OA\Property(
     *     title="code",
     *     description="mã code",
     *     example="GET_CARS_SUCCESS"
     * )
     *
     * @var string
     */
    public $code;

    /**
     * @OA\Property(
     *     title="status",
     *     description="status code",
     *     example=200
     * )
     *
     * @var integer
     */
    public $status;

    /**
     * @OA\Property(
     *     title="data",
     *     description="data",
     * )
     *
     * @var CarResourceSchema[]
     */
    public $data;

    /**
     * @OA\Property(
     *     title="path api",
     *     description="path api",
     *     example="api/cars"
     * )
     *
     * @var string
     */
    public $path;

    /**
     * @OA\Property(
     *     title="path api",
     *     description="path api",
     *     format="timestamp",
     *     example="2021-01-04T03:52:39.003251Z"
     * )
     *
     * @var string
     */
    public $timestamp;
}
