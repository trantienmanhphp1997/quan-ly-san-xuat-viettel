<?php

/**
 * @OA\Schema(
 *      title="Department Resource",
 *      description="Department Resource",
 *      type="object",
 * )
 */
class DepartmentResourceSchema
{

    /**
     * @OA\Property(
     *      description="id của department",
     *      example="2"
     * )
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *      description="tên của department",
     *      example="Phòng sản xuất"
     * )
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      description="mô tả department",
     *      example="Phòng sản xuất"
     * )
     * @var string
     */
    public $description;

    /**
     * @OA\Property(
     *      description="parent id",
     *      example="0"
     * )
     * @var integer
     */
    public $parent_id;

    /**
     * @OA\Property(
     *      description="code",
     *      example="production"
     * )
     * @var string
     */
    public $code;

    /**
     * @OA\Property(
     *      description="code",
     *      example="1"
     * )
     * @var integer
     */
    public $status;

    /**
     * @OA\Property(
     *      description="ghi chú",
     *      example="ghi chú"
     * )
     * @var string
     */
    public $note;
}
