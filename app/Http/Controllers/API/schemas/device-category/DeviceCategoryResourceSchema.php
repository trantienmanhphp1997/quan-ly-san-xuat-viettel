<?php

/**
 * @OA\Schema(
 *      title="Device Category Resource",
 *      description="Device Category Resource",
 *      type="object",
 * )
 */
class DeviceCategoryResourceSchema
{
    /**
     * @OA\Property(
     *      description="id device category",
     *      example="1"
     * )
     *
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *      description="device category name",
     *      example="camera"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      description="device category code",
     *      example="5fea9a03ddd59"
     * )
     *
     * @var string
     */
    public $code;

    /**
     * @OA\Property(
     *      description="device category display code",
     *      example="5fea9a03ddd59"
     * )
     *
     * @var string
     */
    public $display_code;

    /**
     * @OA\Property(
     *      description="parent id device category",
     *      example="0"
     * )
     *
     * @var string
     */
    public $parent_id;

    /**
     * @OA\Property(
     *      description="status device category",
     *      example="1"
     * )
     *
     * @var string
     */
    public $status;

    /**
     * @OA\Property(
     *      description="description device category",
     *      example="đây là mô tả category"
     * )
     *
     * @var string
     */
    public $description;
}
