<?php

/**
 * @OA\Schema(
 *      title="Devices Resource",
 *      description="Devices Resource",
 *      type="object",
 * )
 */
class DeviceResourceSchema
{
    /**
     * @OA\Property(
     *      description="id của thiết bị",
     *      example="2"
     * )
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *      description="tên thiết bị",
     *      example="Camera 150809759"
     * )
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      description="tên danh mục",
     *      example="Camera XYZT"
     * )
     * @var string
     */
    public $category_name;

    /**
     * @OA\Property(
     *      description="id danh mục",
     *      example="123"
     * )
     * @var integer
     */
    public $category_id;

    /**
     * @OA\Property(
     *      description="serial",
     *      example="33503633310"
     * )
     * @var integer
     */
    public $serial;

    /**
     * @OA\Property(
     *      description="description",
     *      example="đây là mô tả thiết bị"
     * )
     * @var string
     */
    public $description;

    /**
     * @OA\Property(
     *      description="quantity",
     *      example="số thiết bị"
     * )
     * @var integer
     */
    public $quantity;

    /**
     * @OA\Property(
     *      description="trạng thái thiết bị",
     *      example="1"
     * )
     * @var integer
     */
    public $status;


    /**
     * @OA\Property(
     *      description="kế hoạch đã mượn sản phẩm",
     *     example={}
     * )
     * @var object
     */
    public $record_plan;
}
