<?php


/**
 * @OA\Schema(
 *      title="Request Body Confirm Borrow Device",
 *      description="Request Body Confirm Borrow Device",
 *      type="object",
 *      required={"record_plan_id", "device_id"}
 * )
 */
class RequestBodyConfirmBorrowDevice
{
    /**
     * @OA\Property(
     *      description="id của record plan",
     *      example="2"
     * )
     * @var integer
     */
    public $record_plan_id;

    /**
     * @OA\Property(
     *      description="id của device",
     *      example="2"
     * )
     * @var integer
     */
    public $device_id;
}
