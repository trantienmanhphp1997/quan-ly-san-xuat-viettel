<?php

/**
 * @OA\Schema(
 *      title="Request Body REport Broken Device",
 *      description="Request Body REport Broken Device",
 *      type="object",
 *      required={"device_id"}
 * )
 */
class RequestBodyReportBrokenDevice
{
    /**
     * @OA\Property(
     *      description="id của device",
     *      example="2"
     * )
     * @var integer
     */
    public $device_id;
}
