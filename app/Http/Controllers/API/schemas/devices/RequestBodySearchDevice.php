<?php


/**
 * @OA\Schema(
 *      title="Request Body Search Device",
 *      description="Request Body Search Device",
 *      type="object",
 *      required={"record_plan_id", "device_id"}
 * )
 */
class RequestBodySearchDevice
{
    /**
     * @OA\Property(
     *      description="keyword",
     *      example="key"
     * )
     * @var string
     */
    public $s;

    /**
     * @OA\Property(
     *      description="Thời gian bắt đầu",
     *      format="datetime",
     *      example="2020-12-30 07:53:09"
     * )
     * @var string
     */
    public $start_time;

    /**
     * @OA\Property(
     *      description="Thời gian kết thúc",
     *      format="datetime",
     *      example="2020-12-30 07:53:09"
     * )
     * @var string
     */
    public $end_time;
}
