<?php

/**
 * @OA\Schema(
 *      title="Request Body Suggest Device",
 *      description="Request Body Suggest Device",
 *      type="object",
 *      required={"record_plan_id", "offer_device_note", "offer_device_number"}
 * )
 */
class RequestBodySuggestDevice
{
    /**
     * @OA\Property(
     *      description="record plan id",
     *      example="2"
     * )
     * @var integer
     */
    public $record_plan_id;

    /**
     * @OA\Property(
     *      description="những thiết bị được đề xuất",
     *      example="cần 10 camera"
     * )
     * @var string
     */
    public $offer_device_note;

    /**
     * @OA\Property(
     *      description="số thiết bị được đề xuất",
     *      example="100"
     * )
     * @var integer
     */
    public $offer_device_number;
}
