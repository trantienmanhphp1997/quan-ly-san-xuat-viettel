<?php

/**
 * @OA\Schema(
 *      title="Request Body Update or Create Device",
 *      description="Request Body Update or Create Device",
 *      type="object",
 *      required={"device_id", "name", "device_category_id", "serial"}
 * )
 */
class RequestBodyUpdateOrCreateDevice
{
    /**
     * @OA\Property(
     *      description="id của device",
     *      example="2"
     * )
     * @var integer
     */
    public $device_id;

    /**
     * @OA\Property(
     *      description="tên của device",
     *      example="camera"
     * )
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      description="id danh mục thiết bị",
     *      example="10"
     * )
     * @var integer
     */
    public $device_category_id;

    /**
     * @OA\Property(
     *      description="mã thiết bị",
     *      example="123JSDHF3"
     * )
     * @var string
     */
    public $serial;

    /**
     * @OA\Property(
     *      description="mô tả thiết bị",
     *      example="Đây là thiết bị"
     * )
     * @var string
     */
    public $description;
}
