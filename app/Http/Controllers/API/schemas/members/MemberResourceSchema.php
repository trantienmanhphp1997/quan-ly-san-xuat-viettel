<?php

/**
 * @OA\Schema(
 *      title="Members Resource",
 *      description="Members Resource",
 *      type="object",
 * )
 */
class MemberResourceSchema
{
    /**
     * @OA\Property(
     *      description="id của member",
     *      example="2"
     * )
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *      description="username",
     *      example="kubikubo"
     * )
     * @var string
     */
    public $username;

    /**
     * @OA\Property(
     *      description="Họ tên đẩy đủ",
     *      example="Nguyễn Tất Tiến"
     * )
     * @var string
     */
    public $full_name;

    /**
     * @OA\Property(
     *      description="số điện thoại",
     *      example="09019976474"
     * )
     * @var string
     */
    public $phone_number;

    /**
     * @OA\Property(
     *      description="số điện thoại khác",
     *      example="09019976474"
     * )
     * @var string
     */
    public $other_phone_number;

    /**
     * @OA\Property(
     *      description="Địa chỉ nhà, trọ của thành viên",
     *      example="Người Việt Nam"
     * )
     * @var string
     */
    public $address;

    /**
     * @OA\Property(
     *      description="Địa chỉ email",
     *      example="tien.nguyentat@evotek.vn"
     * )
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      description="department code",
     *      example="production"
     * )
     * @var string
     */
    public $department_code;

    /**
     * @OA\Property(
     *      description="uuid",
     *      example="49b55eb25256a3f1ae487187bcf7a28f"
     * )
     * @var string
     */
    public $uuid;

    /**
     * @OA\Property(
     *      description="uuid",
     *      example="1"
     * )
     * @var integer
     */
    public $is_manager;

    /**
     * @OA\Property(
     *      description="trạng thái của member",
     *      example="1"
     * )
     * @var integer
     */
    public $status;
}
