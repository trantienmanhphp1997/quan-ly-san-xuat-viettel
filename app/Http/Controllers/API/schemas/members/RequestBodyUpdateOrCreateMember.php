<?php

/**
 * @OA\Schema(
 *      title="Request Body Update or create members",
 *      description="Request Body Update or create members",
 *      type="object",
 *      required={"uuid"}
 * )
 */
class RequestBodyUpdateOrCreateMember
{
    /**
     * @OA\Property(
     *      description="uuid",
     *      example="49b55eb25256a3f1ae487187bcf7a28f"
     * )
     * @var string
     */
    public $uuid;
}
