<?php

/**
 * @OA\Schema(
 *      title="Plan info Resource",
 *      description="Plan info Resource",
 *      type="object",
 * )
 */
class PlanInfoResourceSchema
{
    /**
     * @OA\Property(
     *      description="id của plan info",
     *      example="2"
     * )
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *      description="code của plan info",
     *      example="6011421688636572"
     * )
     * @var string
     */
    public $code;

    /**
     * @OA\Property(
     *      description="năm plan info",
     *      example="2021"
     * )
     * @var string
     */
    public $year;

    /**
     * @OA\Property(
     *      description="title plan info",
     *      example="Kế hoạch năm 2021"
     * )
     * @var string
     */
    public $title;

    /**
     * @OA\Property(
     *      description="mô tả plan info",
     *      example="Đây là kế hoạch bình thường năm 2021"
     * )
     * @var string
     */
    public $description;

    /**
     * @OA\Property(
     *      description="department id",
     *      example="1"
     * )
     * @var integer
     */
    public $department_id;

    /**
     * @OA\Property(
     *      description="plan category id",
     *      example="1"
     * )
     * @var integer
     */
    public $plan_category_id;

    /**
     * @OA\Property(
     *      description="Trạng thái",
     *      example="1"
     * )
     * @var integer
     */
    public $status;
}
