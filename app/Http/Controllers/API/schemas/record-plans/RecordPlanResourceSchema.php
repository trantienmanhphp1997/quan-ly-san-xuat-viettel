<?php

/**
 * @OA\Schema(
 *      title="Record Plans Resource",
 *      description="Record Plans Resource",
 *      type="object",
 * )
 */
class RecordPlanResourceSchema
{
    /**
     * @OA\Property(
     *      description="id của chương trình",
     *      example="2"
     * )
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *      description="tên chương trình",
     *      example="Chương trình chạy bộ"
     * )
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      description="mô tả chương trình",
     *      example="Muốn có sức khỏe thì phải năng tập thể dục"
     * )
     * @var string
     */
    public $description;

    /**
     * @OA\Property(
     *      description="thời gian bắt đầu chương trình",
     *      example="2020-12-28 16:30:37"
     * )
     * @var string
     */
    public $start_time;

    /**
     * @OA\Property(
     *      description="thời gian kết thúc chương trình",
     *      format="datetime",
     *      example="2020-12-28 16:30:37"
     * )
     * @var string
     */
    public $end_time;

    /**
     * @OA\Property(
     *      description="địa chỉ chương trình",
     *      format="datetime",
     *      example="Xóm 6, xã Lăng Thành, Yên Thành, Nghệ An"
     * )
     * @var string
     */
    public $address;

    /**
     * @OA\Property(
     *      description="id của plan",
     *      example="1"
     * )
     * @var integer
     */
    public $plan_id;

    /**
     * @OA\Property(
     *      description="department code",
     *      example="production"
     * )
     * @var string
     */
    public $department_code;

    /**
     * @OA\Property(
     *      description="trạng thái phân bổ phóng viên",
     *      example="1"
     * )
     * @var integer
     */
    public $reporter_allocate_status;

    /**
     * @OA\Property(
     *      description="trạng thái phân bổ quay phim",
     *      example="1"
     * )
     * @var integer
     */
    public $cameraman_allocate_status;

    /**
     * @OA\Property(
     *      description="trạng thái phân bổ lái xe",
     *      example="1"
     * )
     * @var integer
     */
    public $transportation_allocate_status;

    /**
     * @OA\Property(
     *      description="trạng thái phân bổ thiết bị",
     *      example="1"
     * )
     * @var integer
     */
    public $device_allocate_status;

    /**
     * @OA\Property(
     *      description="đề xuất reporter",
     *      example="cần 10 reporter"
     * )
     * @var string
     */
    public $offer_reporter_note;

    /**
     * @OA\Property(
     *      description="đề xuất người quay phim",
     *      example="cần 10 quay phim"
     * )
     * @var string
     */
    public $offer_camera_note;

    /**
     * @OA\Property(
     *      description="đề xuất người lái xe",
     *      example="cần 10 lái xe"
     * )
     * @var string
     */
    public $offer_transportation_note;

    /**
     * @OA\Property(
     *      description="đề xuất thiết bị",
     *      example="cần 10 máy quay 4K"
     * )
     * @var string
     */
    public $offer_device_note;

    /**
     * @OA\Property(
     *      description="số thiết bị",
     *      example="10"
     * )
     * @var string
     */
    public $offer_device_number;

    /**
     * @OA\Property(
     *      description="số người quay phim được đề xuất",
     *      example="10"
     * )
     * @var integer
     */
    public $offer_camera_number;

    /**
     * @OA\Property(
     *      description="số người phóng viên được đề xuất",
     *      example="10"
     * )
     * @var integer
     */
    public $offer_reporter_number;

    /**
     * @OA\Property(
     *      description="số người lái xe được đề xuất",
     *      example="10"
     * )
     * @var integer
     */
    public $offer_transportation_number;

    /**
     * @OA\Property(
     *      description="id của người tạo",
     *      example="10"
     * )
     * @var integer
     */
    public $created_by;

    /**
     * @OA\Property(
     *      description="ghi chú kế hoạch",
     *      example="kế hoạch bá đạo"
     * )
     * @var string
     */
    public $notes;

    /**
     * @OA\Property(
     *      description="Trạng thái của kế hoạch",
     *      example="1"
     * )
     * @var integer
     */
    public $status;

    /**
     * @OA\Property(
     *      description="tất cả thành viên tham gia",
     * )
     * @var MemberResourceSchema[]
     */
    public $members;

    /**
     * @OA\Property(
     *      description="tất cả thiết bị được mượn",
     * )
     * @var DeviceResourceSchema[]
     */
    public $devices;
}
