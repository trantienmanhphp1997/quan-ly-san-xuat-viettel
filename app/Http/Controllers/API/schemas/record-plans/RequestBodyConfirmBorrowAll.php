<?php

/**
 * @OA\Schema(
 *      title="Request Body Confirm borrow all",
 *      description="Request Body Confirm borrow all",
 *      type="object",
 *      required={"list_device_id", "record_plan_id"}
 * )
 */
class RequestBodyConfirmBorrowAll
{
    /**
     * @OA\Property(
     *      description="Danh sách id của thiết bị",
     *      example={1, 2, 3}
     * )
     * @var object
     */
    public $list_device_id;

    /**
     * @OA\Property(
     *      description="id của chương trình",
     *      example="1"
     * )
     * @var object
     */
    public $record_plan_id;
}
