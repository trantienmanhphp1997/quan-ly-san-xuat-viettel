<?php

namespace App\Http\Requests\API;

use RecordPlanResourceSchema;

/**
 * @OA\Schema(
 *      title="Request Body Search Record Plans by Name API",
 *      description="Request Body Search Record Plans by Name API",
 *      type="object",
 * )
 */
class RequestBodySearchByName
{
    /**
     * @OA\Property(
     *      description="Danh sách record plans",
     * )
     *
     * @var RecordPlanResourceSchema[]
     */
    public $ListRecordPlan;
}
