<?php

/**
 * @OA\Schema(
 *      title="Request Body Suggest Member",
 *      description="Request Body Suggest Member",
 *      type="object",
 *      required={"record_plan_id", "group_type", "offer_note", "offer_number"}
 * )
 */
class RequestBodySuggestMember
{
    /**
     * @OA\Property(
     *      description="id của chương trình",
     *      example="2"
     * )
     * @var integer
     */
    public $record_plan_id;

    /**
     * @OA\Property(
     *      description="1 bộ phận cụ thể",
     *      example="reporter"
     * )
     * @var string
     */
    public $group_type;

    /**
     * @OA\Property(
     *      description="danh sách member đề xuất",
     *      example="cần 10 camera"
     * )
     * @var string
     */
    public $offer_note;

    /**
     * @OA\Property(
     *      description="số member được đề xuất",
     *      example="40"
     * )
     * @var string
     */
    public $offer_number;
}

