<?php

/**
 * @OA\Schema(
 *      title="Request Body Update or Create API",
 *      description="Request Body Update or Create API",
 *      type="object",
 *     required={"id", "name", "start_time", "end_time", "department_code", "address"}
 * )
 */
class RequestBodyUpdateOrCreate
{
    /**
     * @OA\Property(
     *      description="id record plan",
     *      example="1",
     * )
     *
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *      description="tên chương trình",
     *      example="Chương trình mù căng chải"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      description="Thời gian bắt đầu chương trình",
     *      example="2020-12-29 02:52:47",
     *      format="datetime",
     * )
     *
     * @var string
     */
    public $start_time;

    /**
     * @OA\Property(
     *      description="Thời gian kết thúc chương trình",
     *      example="2020-12-29 02:52:47",
     *      format="datetime",
     * )
     *
     * @var string
     */
    public $end_time;

    /**
     * @OA\Property(
     *      description="mã phòng ban",
     *      example="234GDJH3"
     * )
     *
     * @var string
     */
    public $department_code;

    /**
     * @OA\Property(
     *      description="Địa chỉ diễn ra chương trình",
     *      example="quán thỏ nướng, Trung tâm mặt trăng"
     * )
     *
     * @var string
     */
    public $address;

    /**
     * @OA\Property(
     *      description="mô tả chương trình",
     *      example="chương trình quay về cuộc sống trên mặt trăng"
     * )
     *
     * @var string
     */
    public $description;
}
