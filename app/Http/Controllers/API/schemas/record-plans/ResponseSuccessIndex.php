<?php

namespace App\Http\Requests\API;

use RecordPlanResourceSchema;

/**
 * @OA\Schema(
 *      title="Request Body Index Record Plan API",
 *      description="Request Body Index Record Plan API",
 *      type="object",
 * )
 */

class ResponseSuccessIndex
{
    /**
     * @OA\Property(
     *      description="Danh sách record plans",
     * )
     *
     * @var RecordPlanResourceSchema[]
     */
    public $ListRecordPlan;
}
