<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\ValidateException;
use App\Helpers\PaginateForArray;
use App\Models\RecordPlan;
use App\Models\Member;
use App\Repositories\DeviceRepository;
use App\Repositories\MemberRepository;
use App\Repositories\NoteRepository;
use App\Repositories\RecordPlanDeviceXrefRepository;
use App\Repositories\RecordPlanMemberXrefRepository;
use App\Repositories\RecordPlanRepository;
use App\Services\AssignmentService;
use App\Services\DeviceService;
use App\Services\Interfaces\IRecordPlanService;
use App\Services\MemberService;
use App\Services\RecordPlanService;
use App\ViewModel\PaginateModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use DB;
class AssignController extends Controller
{
    protected $prefix = 'assignment';
    protected $modelDisplayName = 'Assignment';
    protected $modelClass = "App\Models\RecordPlanMemberXref";
    public $recordPlanRepository;
    public $recordPlanMemberXrefRepository;
    public $noteRepository;
    public $memberRepository;
    public $memberService;
    public $assignmentService;
    public $recordPlanService;
    public $deviceService;
    public $deviceRepository;
    public $recordPlanDeviceXrefRepository;

    public function __construct(
        RecordPlanRepository $recordPlanRepository,
        RecordPlanMemberXrefRepository $recordPlanMemberXrefRepository,
        RecordPlanDeviceXrefRepository $recordPlanDeviceXrefRepository,
        MemberRepository $memberRepository,
        NoteRepository $noteRepository,
        DeviceRepository $deviceRepository,
        MemberService $memberService,
        AssignmentService $assignmentService,
        IRecordPlanService $recordPlanService,
        DeviceService $deviceService
    )
    {
        parent::__construct();
        $this->recordPlanRepository = $recordPlanRepository;
        $this->recordPlanMemberXrefRepository = $recordPlanMemberXrefRepository;
        $this->recordPlanDeviceXrefRepository = $recordPlanDeviceXrefRepository;
        $this->memberRepository = $memberRepository;
        $this->noteRepository = $noteRepository;
        $this->deviceRepository = $deviceRepository;
        $this->memberService = $memberService;
        $this->assignmentService = $assignmentService;
        $this->recordPlanService = $recordPlanService;
        $this->deviceService = $deviceService;
    }

    public function getAssignmentByDepartmentCode($record_plan_id, Request $request)
    {
        $record_plan = $this->recordPlanRepository->find($record_plan_id);
        $department = $request->segment(2);
        if(!$record_plan){
            abort(404);
        }

//        $checkTime = RecordPlanService::checkTimeToUpdateRecordPlan($record_plan->start_time);
//        if(!$checkTime){
//            abort(403, "THIS ACTION IS UNAUTHORIZED.");
//        }

        $data = [];
        $available_members = [];
        $assigned_members = [];
        try {
            $assigned_members = $this->memberService->getAssignedMembers($department, $record_plan_id);
            $available_members = $this->memberService->getAvailableMembers($department, $record_plan_id, [], $record_plan->start_time, $record_plan->end_time);
        } catch (\Exception $e) {
            if($e instanceof ValidateException){
                session()->flash("error", $e->getMessage());
            }else{
                session()->flash("error",  __("notification.common.fail.no-data"));
            }
            Log::error($e);
        }

        $data['available_members'] =  $available_members;
        $data['assigned_members'] =  $assigned_members;
        //return getAssignmentByDepartmentCode
        return $data;
    }

    public function assignMembersForRecordPlan($record_plan_id, Request $request)
    {
        // kiểm tra thời gian được phép cập nhật nguồn lực
        $start_time = $this->recordPlanRepository->allQuery(['id'=>$record_plan_id])->pluck('start_time')->first();
        if (!$this->recordPlanService->checkTimeToUpdateRecordPlan($start_time)){
            session()->flash("error", __("notification.assignment.fail.add.member"));
            return false;
        }

        if (blank($request->member_ids)) {
            session()->flash("error", __("notification.common.fail.no-data-selected"));
            return false;
        }

        $member_ids = [$request->member_ids];
        if (strpos($request->member_ids, ",")) {
            $member_ids = explode(',', $request->member_ids);
        }

        $department_code = $request->department_code;

        try {
            $rs = $this->assignmentService->assignMembersToRecordPlan($record_plan_id, $department_code, $member_ids);
        } catch (\Exception $e) {
            if ($e instanceof ValidateException) {
                session()->flash("error", $e->getMessage());
            } else {
                session()->flash("error", __("notification.assignment.fail.add.member"));
            }
            Log::error($e->getMessage());
            return false;
        }
        $this->assignmentService->checkAllocateStatus($department_code, $record_plan_id);
        session()->flash("success", __("notification.assignment.success.add.member"));
        return true;

    }
    public function assignMembers($record_plan_id, Request $request) {
        DB::beginTransaction();
        try {
            if(isset($request->assign_reporter["ids"]) && count($request->assign_reporter["ids"]) > 0){
                $assignReporter = $this->assignmentService->assignMembersToRecordPlan($record_plan_id, $request->assign_reporter["department_code"], $request->assign_reporter["ids"]);
            }
            $reporterStatus = $this->assignmentService->checkAllocateStatus($request->assign_reporter["department_code"], $record_plan_id, false);

            if(isset($request->assign_camera["ids"]) && count($request->assign_camera["ids"]) > 0){
                $assignCamera = $this->assignmentService->assignMembersToRecordPlan($record_plan_id, $request->assign_camera["department_code"], $request->assign_camera["ids"]);
            }
            $cameraStatus = $this->assignmentService->checkAllocateStatus($request->assign_camera["department_code"], $record_plan_id, false);

            if(isset($request->assign_technical_camera["ids"]) && count($request->assign_technical_camera["ids"]) > 0){
                $assignTechnicalCamera = $this->assignmentService->assignMembersToRecordPlan($record_plan_id, $request->assign_technical_camera["department_code"], $request->assign_technical_camera["ids"]);
            }
            $technicalCameraStatus = $this->assignmentService->checkAllocateStatus($request->assign_technical_camera["department_code"], $record_plan_id, false);

            if(isset($request->assign_transportation["ids"]) && count($request->assign_transportation["ids"]) > 0){
                $assignTransportation = $this->assignmentService->assignMembersToRecordPlan($record_plan_id, $request->assign_transportation["department_code"], $request->assign_transportation["ids"]);
            }
            $transportationStatus = $this->assignmentService->checkAllocateStatus($request->assign_transportation["department_code"], $record_plan_id);
            $record_plan = $this->recordPlanRepository->find($record_plan_id);
            $record_plan->update([
                "reporter_allocate_status" => $reporterStatus,
                "camera_allocate_status" => $cameraStatus,
                "transportation_allocate_status" => $transportationStatus,
                "technical_camera_allocate_status" => $technicalCameraStatus
            ]);
            $this->recordPlanService->checkRecordPlanGeneralStatus($record_plan_id);
            DB::commit();
        } catch (\Exception $e) {
            if ($e instanceof ValidateException) {
                session()->flash("error", $e->getMessage());
            } else {
                session()->flash("error", __("notification.assignment.fail.add.member"));
            }
            Log::error($e->getMessage());
            DB::rollBack();
            return false;
        }
        session()->flash("success", __("notification.assignment.success.add.member"));
        return true;
    }
    public function assignDeviceForRecordPlan($record_plan_id, Request $request)
    {
        $device_ids = $request->device_ids;

        // kiểm tra thời gian được phép cập nhật nguồn lực
        $start_time = $this->recordPlanRepository->allQuery(['id'=>$record_plan_id])->pluck('start_time')->first();
        if (!$this->recordPlanService->checkTimeToUpdateRecordPlan($start_time)){
            session()->flash("error", __("notification.assignment.fail.add.member"));
            return false;
        }
        if (!$request->device_ids) {
            session()->flash("error", __("notification.common.fail.no-data-selected"));
            return false;
        }

        $status = 1;
        if (auth()->user()->hasRole("super admin") || (auth()->user()->department_code == config("common.departments.device") && auth()->user()->is_manager == 1)) {
            $status = 2;
        }
        if ($request->status) {
            $status = $request->status;
        }

        $rs = $this->assignmentService->assignDevicesToRecordPlan($record_plan_id, $device_ids, $status);
        if ($status == 2) {
            $this->assignmentService->handleProposedDevicesOnOtherRecordPlan($record_plan_id, $device_ids);
        }

        $this->assignmentService->checkAllocateStatus("device", $record_plan_id);
        if ($rs) {
            session()->flash("success", __("notification.assignment.success.add.device"));
        } else {
            session()->flash("error", __("notification.assignment.fail.add.device"));
        }
        return true;
    }

    public function deleteMembersForRecordPlan(Request $request)
    {
        $memberIds = [];
        if (!blank($request->member_ids)) {
            $memberIds = explode(',', $request->member_ids);
        }

        try {
            $rs = $this->recordPlanService->detachMembersByRecordPlan($request->record_plan_id, $memberIds);
        } catch (\Exception $e) {
            if ($e instanceof ValidateException) {
                session()->flash("error", $e->getMessage());
            } else {
                session()->flash("error", __("notification.assignment.fail.delete"));
            }
            Log::error($e->getMessage());
            return redirect()->back();
            return isset($request->currentPage) ? redirect()->back() : false;
        }

        $this->assignmentService->checkAllocateStatus($request->department_code, $request->record_plan_id);
        session()->flash("success", __("notification.assignment.success.delete.member"));
        return redirect()->back();
        return isset($request->currentPage) ? redirect()->back() : true;
    }

    public function addNote($record_plan_id, Request $request)
    {
        $department_code = $request->departmentCode;
        DB::beginTransaction();
        try{
            $note = $this->assignmentService->addNoteForRecordPlan($record_plan_id, $department_code, $request["content"]);
            $rs = true;
            if($department_code != "device"){// phân công members cho record
                $rs = $this->assignMembers($record_plan_id, $request);
            }
            if($department_code == "device" && $request->device_ids){
                $rs = $this->assignDeviceForRecordPlan($record_plan_id, $request);
            }

            if($rs){
                DB::commit();
                session()->flash("success", __("notification.assignment.success.add-note"));
                return true;
            }else{
                session()->flash("error", __("notification.assignment.fail.add-note"));
                return false;
            }
        }catch (\Exception $exception){
            session()->flash("error", __("notification.assignment.fail.add-note"));
            return false;
        }
    }

    public function getDeviceAssignment($record_plan_id, Request $request)
    {
        $record_plan = $this->recordPlanRepository->find($record_plan_id);
        $keyword = $request->keyword;
        $data= [];
        if(!$record_plan){
            abort(404);
        }

        $checkTime = RecordPlanService::checkTimeToUpdateRecordPlan($record_plan->end_time);
        if(!$checkTime){
            abort(403, "THIS ACTION IS UNAUTHORIZED.");
        }

        // Danh sách thiết bị được đề xuất
        $proposal_device_ids = $this->deviceService->getProposalDeviceIds($record_plan_id);
        $proposal_devices = $this->recordPlanDeviceXrefRepository->geDataByRecordPlanIdAndDeviceIds($record_plan_id, $proposal_device_ids);

        // Danh sách thiết bị được phân công
        $assigned_devices = $this->deviceService->getAssignedDevices($record_plan_id);

        // Danh sách thiết bị sẵn sàng cho mượn (có cả thiết bị đã được đề xuất nhưng chưa phê duyệt)
        $available_devices = $this->deviceService->getAvailableDevicesByRecordPlanId($record_plan_id, $keyword, $request->category_id);

        // Danh sách thiết bị bị đề xuất trùng
        $duplicated_proposal_device_ids = $this->deviceService->getDuplicatedProposalDeviceIds($record_plan_id, $available_devices->pluck("id")->toArray());

        // Danh sách thiết bị được phân công và được đề xuất (nhưng chưa phê duyệt)
        $assigned_and_proposed_devices = $assigned_devices->merge($proposal_devices);
        $assigned_and_proposed_devices = PaginateForArray::paginate($assigned_and_proposed_devices, 10, "/assignment/device/$record_plan_id/show", $request, 'assignedPage');

        $proposal_devices = $this->deviceRepository->allQuery()->whereIn("id", $proposal_device_ids)->get();
        $returned_device_ids = $this->deviceService->getReturnedDeviceIds($record_plan_id);
        $returned_devices = $this->deviceRepository->allQuery()->whereIn("id", $returned_device_ids)->get();

        $note = $this->noteRepository->findNoteByRecordPlanIdAndDepartmentCode($record_plan_id, config("common.departments.device"));

        $data["available_devices"] = $available_devices;
        return $data;
        //return_devive_assinment_data
    }

    public function deleteDeviceForRecordPlan(Request $request)
    {
        if (!$request->device_ids) {
            session()->flash("error", __("notification.common.fail.no-data-selected"));
            return redirect()->back();
        }
        $device_ids = [$request->device_ids];
        if (strpos($request->device_ids, ",")) {
            $device_ids = explode(',', $request->device_ids);
        }
        $this->assignmentService->handleProposedDevicesOnOtherRecordPlan($request->record_plan_id, $device_ids, config("common.status.propose_device.pending"));
        $result = $this->recordPlanService->detachDevicesByRecordPlan($request->record_plan_id, $device_ids);
        $this->assignmentService->checkAllocateStatus(config("common.departments.device"), $request->record_plan_id);
        if ($result) {
            session()->flash("success", __("notification.assignment.success.delete.device"));
        } else {
            session()->flash("error", __("notification.assignment.fail.delete.device"));
        }
        return redirect()->back();
    }

    public function acceptDeviceForRecordPlan(Request $request)
    {
        $device_ids = [$request->device_ids];
        $result = $this->assignmentService->updateDeviceStatus($request->record_plan_id, $device_ids, 2);
        $this->assignmentService->handleProposedDevicesOnOtherRecordPlan($request->record_plan_id, $device_ids);

        $this->assignmentService->checkAllocateStatus(config("common.departments.device"), $request->record_plan_id);
        if ($result) {
            session()->flash("success", __("notification.assignment.success.accept.device"));
        } else {
            session()->flash("error", __("notification.assignment.fail.accept.device"));
        }
        return redirect()->back();
    }

    public function updateDeviceStatus(Request $request)
    {
        $device_ids = $request->device_ids;
        if (blank($device_ids) || !$device_ids[0]) {
            session()->flash("error", __("notification.common.fail.no-device-selected"));
            return false;
        }
        $status = $request->status;
        $result = $this->assignmentService->updateDeviceStatus($request->record_plan_id, $device_ids, $status, $request->user_id);
        if (!$result["result"]) {
            session()->flash("error", $result["message"]);
            return false;
        }
        $this->assignmentService->checkAllocateStatus(config("common.departments.device"), $request->record_plan_id);
        if ($status == config("common.status.propose_device.delivered")) {
            session()->flash("success", __("notification.assignment.success.propose_device.delivered"));
        } elseif ($status == config("common.status.propose_device.returned")) {
            session()->flash("success", __("notification.assignment.success.propose_device.returned"));
        }
        return true;
    }

    public function getReporterProposals(Request $request)
    {
        $status = $request->query("status", config('common.status.proposals.unassigned'));
        if(!$status){
            $status = "";
        }

        $query = [
            's' => $request->query("s", ""),
            'time_filter' => $request->query("time_filter", "")
        ];

        $data = $this->recordPlanService->getProposalsBydepartmentCode("reporter", $query, $status, new PaginateModel());
        $total = [
            's' => $request->query("s", ""),
            'time_filter' => $request->query("time_filter", ""),
            "status" => $status
        ];
        return view($this->viewNamespace . $this->prefix . ".proposals", [
            "data" => $data,
            "department_code" => config("common.departments.production"),
            "total" =>$this->recordPlanService->countProposalsByDepartmentCode("reporter", $total),
            "unassigned" => $this->recordPlanService->countProposalsByDepartmentCode("reporter", $query, config('common.status.proposals.unassigned')),
            "assigned" =>  $this->recordPlanService->countProposalsByDepartmentCode("reporter", $query, config('common.status.proposals.assigned')),
            "allParams" => $total,
        ]);
    }

    public function getCamProposals(Request $request)
    {
        $status = $request->query("status", config('common.status.proposals.unassigned'));
        if(!$status){
            $status = "";
        }
        $query = [
            's' => $request->query("s", ""),
            'time_filter' => $request->query("time_filter", "")
        ];

        $data = $this->recordPlanService->getProposalsByDepartmentCode(config("common.departments.camera"), $query, $status, new PaginateModel());

        $total = [
            's' => $request->query("s", ""),
            'time_filter' => $request->query("time_filter", ""),
            "status" => $status
        ];

        return view($this->viewNamespace . $this->prefix . ".proposals", [
            "data" => $data,
            "department_code" => config("common.departments.camera"),
            "total" =>$this->recordPlanService->countProposalsByDepartmentCode(config("common.departments.camera"), $total),
            "unassigned" => $this->recordPlanService->countProposalsByDepartmentCode(config("common.departments.camera"), $query, config('common.status.proposals.unassigned')),
            "assigned" =>  $this->recordPlanService->countProposalsByDepartmentCode(config("common.departments.camera"), $query, config('common.status.proposals.assigned')),
            "allParams" => $total,
        ]);
    }
    public function getTechnicalCamProposals(Request $request)
    {
        $status = $request->query("status", config('common.status.proposals.unassigned'));
        if(!$status){
            $status = "";
        }
        $query = [
            's' => $request->query("s", ""),
            'time_filter' => $request->query("time_filter", "")
        ];

        $data = $this->recordPlanService->getProposalsByDepartmentCode(config("common.departments.technical_camera"), $query, $status, new PaginateModel());
        $total = [
            's' => $request->query("s", ""),
            'time_filter' => $request->query("time_filter", ""),
            "status" => $status
        ];
        return view($this->viewNamespace . $this->prefix . ".proposals", [
            "data" => $data,
            "department_code" => config("common.departments.technical_camera"),
            "total" =>$this->recordPlanService->countProposalsByDepartmentCode(config("common.departments.technical_camera"), $total),
            "unassigned" => $this->recordPlanService->countProposalsByDepartmentCode(config("common.departments.technical_camera"), $query, config('common.status.proposals.unassigned')),
            "assigned" =>  $this->recordPlanService->countProposalsByDepartmentCode(config("common.departments.technical_camera"), $query, config('common.status.proposals.assigned')),
            "allParams" => $total,
        ]);
    }

    public function getDriverProposals(Request $request)
    {
        $status = $request->query("status", config('common.status.proposals.unassigned'));
        if(!$status){
            $status = "";
        }
        $query = [
            's' => $request->query("s", ""),
            'time_filter' => $request->query("time_filter", ""),
            'address_type' => $request->query("address_type", "")
        ];
        $data = $this->recordPlanService->getProposalsByDepartmentCode(config("common.departments.transportation"), $query, $status, new PaginateModel());
        $total = [
            's' => $request->query("s", ""),
            'time_filter' => $request->query("time_filter", ""),
            "status" => $status,
            'address_type' => $request->query("address_type", "")
        ];
        return view($this->viewNamespace . $this->prefix . ".proposals", [
            "data" => $data,
            "department_code" => config("common.departments.transportation"),
            "total" =>$this->recordPlanService->countProposalsByDepartmentCode(config("common.departments.transportation"), $query),
            "unassigned" => $this->recordPlanService->countProposalsByDepartmentCode(config("common.departments.transportation"), $query, config('common.status.proposals.unassigned')),
            "assigned" =>  $this->recordPlanService->countProposalsByDepartmentCode(config("common.departments.transportation"), $query, config('common.status.proposals.assigned')),
            "allParams" => $total,
        ]);
    }

    public function getDeviceProposals(Request $request)
    {
        $status = $request->query("status");
        if(!$status){
            $status = "";
        }
        $query = [
            's' => $request->query("s", ""),
            'lendingDevice' =>  $request->query("lendingDevice", ""),
            'time_filter' => $request->query("time_filter", "")
        ];
        $data = $this->recordPlanService->getProposalsByDepartmentCode(config("common.departments.device"), $query, $status, new PaginateModel());
        $total = [
            's' => $request->query("s", ""),
            'time_filter' => $request->query("time_filter", ""),
            'lendingDevice' =>  $request->query("lendingDevice", ""),
            "status" => $status
        ];
        return view($this->viewNamespace . $this->prefix . ".proposals", [
            "data" => $data,
            "department_code" => config("common.departments.device"),
            "total" =>$this->recordPlanService->countProposalsByDepartmentCode(config("common.departments.device"), $total),
            "unassigned" => $this->recordPlanService->countProposalsByDepartmentCode(config("common.departments.device"), $query, config('common.status.proposals.unassigned')),
            "assigned" =>  $this->recordPlanService->countProposalsByDepartmentCode(config("common.departments.device"), $query, config('common.status.proposals.assigned')),
            "notGive" => $this->recordPlanService->countProposalsByLendingStatus(config("common.departments.device"), $query, '', "notGive"),
            "lendingDevice" => $this->recordPlanService->countProposalsByLendingStatus(config("common.departments.device"), $query, '', "lendingDevices"),
//            "lendingOnTime" => $this->recordPlanService->countProposalsByLendingStatus(config("common.departments.device"), $query, config('common.status.proposals.unassigned'),0),
            "lendingOutTime" => $this->recordPlanService->countProposalsByLendingStatus(config("common.departments.device"), $query, '', 'lendingOutTime'),
            "allParams" => $total,
        ]);
    }

    /**
     * Danh sách RecordPlan đã xác nhận nhưng chưa phân bổ thiết bị
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function lendingDevices(Request $request)
    {
        $allParams = $request->input();
        $device_xref_status = $request->query("status", 2);
        $allocate_status = config("common.status.propose.approved");
        $data = $this->assignmentService->getRecordPlansForDevices($device_xref_status, $allParams, $allocate_status);
        return view($this->viewNamespace . $this->prefix . ".lending", [
            "data" => $data,
            "department_code" => config("common.departments.device"),
            "allParams" => $allParams,
            "is_lending" => true,
            "status" => $device_xref_status
        ]);
    }

    /**
     * Danh sách RecordPlan đã giao nhưng chưa hoàn trả
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function returningDevices(Request $request)
    {
        $allParams = $request->input();
        $device_xref_status = $request->query("status", 3);
        $allocate_status = config("common.status.propose.approved");
        $data = $this->assignmentService->getRecordPlansForDevices($device_xref_status, $allParams, $allocate_status);
        $data_return = [];
        if ($device_xref_status == 4) {
            $general_status = [
                config("common.status.record_plan.pending"),
                config("common.status.record_plan.approved"),
                config("common.status.record_plan.in_progress"),
                config("common.status.record_plan.wrap"),
                config("common.status.record_plan.done")
            ];
            $data_return = $this->recordPlanRepository->allQuery(["s" => ''])->where([
                "device_allocate_status" => $allocate_status,
            ])->whereIn("status", $general_status)->whereHas("devices", function ($query) {
                $query->where(["record_plan_device_xrefs.status" => 3]);
            })->get()->pluck('id')->toArray();
        }
        return view($this->viewNamespace . $this->prefix . ".lending", [
            "data" => $data,
            "department_code" => config("common.departments.device"),
            "allParams" => $request->input(),
            "is_returning" => true,
            "data_return" => $data_return
        ]);
    }

    /**
     *  Trả dữ liệu cho màn hình giao thiết bị cho lịch sản xuất
     *  data: Danh sách thiết bị đã xác nhận phân bổ và chưa giao
     *  delivered_devices: Danh sách thiết bị đã giao cho lịch sản xuất
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getDevicesForLending($record_plan_id)
    {
        $data= [];
        $devices = $this->recordPlanDeviceXrefRepository->paginateDevicesForRecordPlan($record_plan_id, [config("common.status.propose_device.approved")], "approved");
        $delivered_devices = $this->recordPlanDeviceXrefRepository->paginateDevicesForRecordPlan($record_plan_id, [config("common.status.propose_device.delivered"), config("common.status.propose_device.returned")], "delivered");
        $record_plan = $this->recordPlanRepository->findOrFail($record_plan_id);
        $data['delivery_devices'] = $devices;
        return $data;
    }

    public function getDevicesForReturning($record_plan_id)
    {
        $data= [];
        $devices = $this->recordPlanDeviceXrefRepository->paginateDevicesForRecordPlan($record_plan_id, [config("common.status.propose_device.approved")], "approved");
        $delivered_devices = $this->recordPlanDeviceXrefRepository->paginateDevicesForRecordPlan($record_plan_id, [config("common.status.propose_device.delivered")], "delivered");
        $record_plan = $this->recordPlanRepository->findOrFail($record_plan_id);
        $data['return_devices'] = $delivered_devices;
        return $data;
    }

    public function getMemberCanLending($record_plan_id){
        $assigned_member_ids = $this->recordPlanMemberXrefRepository->allQuery(["record_plan_id" => $record_plan_id])->pluck("member_id")->toArray();
        $creator = $this->recordPlanRepository->allQuery(["id" => $record_plan_id])->pluck("created_by")->first();
        array_push($assigned_member_ids, $creator);
        $assigned_members = Member::whereIn("id",$assigned_member_ids)->where("department_code",'!=',config("common.departments.transportation"))->orderBy("is_manager", "desc")->get();
        return $assigned_members;

    }

    public function getNoteByDepartmentCode($record_plan_id, Request $request){
        $departmentCode = $request->departmentCode;
        $note = $this->noteRepository->findNoteByRecordPlanIdAndDepartmentCode($record_plan_id, $departmentCode);

        if($note){
            return response()->json(["note" => $note->content]) ;
        }
        return response()->json(["note" => $note]) ;
    }
}
