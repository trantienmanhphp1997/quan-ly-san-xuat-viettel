<?php


namespace App\Http\Controllers\Admin;

class CameraController extends MemberController
{
    protected $modelDisplayName = 'camera';
    protected $prefix = 'camera';
    protected $departmentCode = 'camera';
}
