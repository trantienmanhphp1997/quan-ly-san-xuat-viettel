<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CarImportRequest;
use App\Http\Requests\CarRequest;
use App\Imports\CarImport;
use App\Models\Car;
use App\Repositories\CarRepository;
use App\Repositories\MemberRepository;
use App\Repositories\RecordPlanMemberXrefRepository;
use App\Services\CarService;
use App\Utils\ExcelUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class CarController extends Controller
{
    protected $modelDisplayName = 'cars';
    protected $modelClass = "App\Models\Car";
    protected $prefix = 'cars';
    protected $filterOptions = [];
    protected $memberRepository;
    private $excelUtil;
    protected $carService;
    protected $recordPlanMemberXrefRepository;

    public function __construct(
        CarRepository $carRepository,
        MemberRepository $memberRepository,
        ExcelUtil $excelUtil,
        CarService $carService,
        RecordPlanMemberXrefRepository $recordPlanMemberXrefRepository
    )
    {
        parent::__construct();
        $this->selfRepository = $carRepository;
        $this->memberRepository = $memberRepository;
        $this->excelUtil = $excelUtil;
        $this->recordPlanMemberXrefRepository = $recordPlanMemberXrefRepository;
    }

    protected function additionalData($requestParams, $data) {
        $statusCount = Car::select('status', DB::raw('count(*) as total'))->groupBy('status')->get();
        $countInfo = $statusCount->mapWithKeys(function($item) {
            return [$item->status => $item->total];
        });
        $countInfo['total'] = $countInfo->sum();
        return [
            'countInfo' => $countInfo,
        ];
    }

    public function create(Request $request)
    {
        $editable = true;

        $is_active = $request->query("is_active", 1);

        $assigned_driver_ids = $this->selfRepository->allQuery()->where("driver_id","<>",null)->pluck("driver_id");

        $available_drivers = $this->memberRepository
            ->allQuery(["department_code" => config("common.departments.transportation")])
            ->whereNotIn("id", $assigned_driver_ids)
            ->get();

        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "editable" => $editable,
            "drivers" => $available_drivers,
            "is_active" => $is_active
        ]);
    }

    public function show($id)
    {
        $data = $this->selfRepository->allQuery()->where('id',$id)->first();
        $this->authorize("view", $data);
        $editable = false;
        return view($this->viewNamespace . $this->prefix . '.detail', compact(
            'editable',
            'data'
        ));
    }

    public function edit($id)
    {
        $editable = true;
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("update", $data);
        $driver_id = [$data->driver_id];

        $assigned_driver_ids = $this->selfRepository->allQuery()->where("driver_id","<>",null)->pluck("driver_id")->toArray();

        if(!empty($driver_id)){
            $assigned_driver_ids = array_diff($assigned_driver_ids, $driver_id);
        }

        $availble_drivers = $this->memberRepository
            ->allQuery(["department_code" => config("common.departments.transportation")])
            ->whereNotIn("id", $assigned_driver_ids)
            ->get();

        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "data" => $data,
            "editable" => $editable,
            "drivers" => $availble_drivers
        ]);
    }

    public function store(CarRequest $request)
    {
        $newData = $request->all();
        $newData["status"] = array_key_exists("status", $request->all()) ? config("common.status.car.active") : config("common.status.car.inactive");

        try {
            $car = $this->selfRepository->create($newData);

            session()->flash("success", __("notification.common.success.add"));
        } catch (\Exception $exception) {
            session()->flash("error", __("notification.common.fail.add"));
        }

        return redirect()->route($this->prefix . ".index");
    }

    public function update(CarRequest $request, $id)
    {
        $car = $this->selfRepository->withTrashed()->find($id);
        $updatedData = $request->all();
        $updatedData["status"] = array_key_exists("status", $request->all()) ? config("common.status.car.active") : config("common.status.car.inactive");

        try {
            $car->update($updatedData);
            session()->flash("success", __("notification.common.success.update"));
        } catch (\Exception $exception) {
            session()->flash("error", __("notification.common.fail.update"));
        }
        return redirect($request->link);
    }

    public function export(Request $request)
    {
        $allParams = $request->query();

        $cars = $this->selfRepository
            ->allQuery($this->makeQueryParams($allParams))->with('transportation')
            ->get();
        $index = 1;
        foreach ($cars as $car) {
            $car->{'no'} = $index++;
            $car->{'driver'} = $car->transportation ? $car->transportation->full_name : '';
        }
        $titles = [
            "no", "brand", "model", "color", "driver", "number_of_seats", "license_plate", "note"
        ];

        $data = [];
        $data["data"] = $cars;
        $data["titles"] = $titles;
        $this->excelUtil->exportExcel($data, "DSXe", "templates/car-export.xlsx", 6);
    }

    public function import()
    {
        return view("pages.cars.import");
    }

    public function importStore(CarImportRequest $request)
    {
        if ($request->hasFile("cars")) {
            $file_path = $request->file("cars")->storeAs('cars', $request->file('cars')->getClientOriginalName() . '.' . $request->file('cars')->extension());
            try {
                $import = Excel::import(new CarImport(), $file_path);
            } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                $failures = $e->failures();
                return view("$this->viewNamespace$this->prefix.import")->with([
                    "failures" => $failures
                ]);
            }
            session()->flash("success", __("notification.common.success.import")); //?
            return redirect()->route("$this->prefix.index");
        }
    }

    public function reportBrokenCars(Request $request, $id)
    {
        // Init array of IDs
        $car_ids = [];
        $reason_report_broken = $request->input('reason_report_broken','');
        if (empty($id) || $id == -1) {
            // Bulk restore, get IDs from POST
            $car_ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URLc
            $car_ids[] = $id;
        }

        $rs = $this->selfRepository->updateCarStatus($car_ids, config("common.status.car.broken"), $reason_report_broken);
        if ($rs) {
            session()->flash("success", __("notification.common.success.report-broken"));
        } else {
            session()->flash("error", __("notification.common.fail.report-broken"));
        }
        return redirect()->back();
    }

    public function restore(Request $request, $id)
    {
        // Init array of IDs
        $car_ids = [];
        if (empty($id) || $id == -1) {
            // Bulk delete, get IDs from POST
            $car_ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $car_ids[] = $id;
        }
        $rs = $this->selfRepository->updateCarStatus($car_ids, config("common.status.car.active"));
        if($rs){
            session()->flash("success", __("notification.common.success.restore"));
        }else{
           session()->flash("error", __("notification.common.fail.restore"));
        }
        return redirect()->back();
    }

    public function exportAssignment(Request $request)
    {
        $allParams = [];
        $car_ids = $this->selfRepository->allQuery($allParams)->pluck("id")->toArray();
        $driver_status = config("common.status.member.active");
        $driver_ids = $this->memberRepository->getDriversByCarIdsAndStatus($car_ids, $driver_status)->pluck("id")->toArray();
        $status = [
            config("common.status.propose.approved"),
            config("common.status.propose.pending"),
            config("common.status.propose.record_plan_done"),
        ];
        $data = [];
        $data["titles"] = [
           "no", "car_name", "license_plate", "number_of_seats", "driver_name", "phone_number", "record_plan_name", "start_time", "end_time", "address", "status"
        ];

        $items = $this->recordPlanMemberXrefRepository->getDataByMemberIdsAndStatus($driver_ids, $status, $allParams);
        $rs = [];
        foreach ($items as $index => $item){
            $record_plan_status = "";
            foreach (config('common.status.record_plan') as $key => $value) {
                if ($value == $item->record_plan->status) {
                    $record_plan_status = __('status.record_plan.' . $key);
                }
            }

            $rs[] = [
                "no" => $index + 1,
                "car_name" => optional(optional($item->member)->car)->brand . " " . optional(optional($item->member)->car)->model,
                "license_plate" => optional(optional($item->member)->car)->license_plate,
                "number_of_seats" => optional(optional($item->member)->car)->number_of_seats,
                "driver_name" => optional($item->member)->full_name,
                "phone_number" => optional($item->member)->phone_number,
                "record_plan_name" => optional($item->record_plan)->name,
                "start_time" => optional($item->record_plan)->start_time ? date("d-m-y H:i:s", strtotime(optional($item->record_plan)->start_time)) : "",
                "end_time" => optional($item->record_plan)->end_time ? date("d-m-y H:i:s", strtotime(optional($item->record_plan)->end_time)) : "",
                "address" => optional($item->record_plan)->address,
                "status" => $record_plan_status,
            ];
        }

        $data["data"] =  $rs;
        if(!count($rs)){
            session()->flash("warning", __("common.warning.no_data"));
            return redirect()->back();
        }

        $this->excelUtil->exportExcel($data, "DSLichSuDungXe", "templates/assignment_car-export.xlsx", 5);
    }
}
