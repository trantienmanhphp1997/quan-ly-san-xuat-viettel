<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\MemberRepository;
use App\Services\MemberService;
use Illuminate\Http\Request;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    protected $memberRepository;
    protected $memberService;

    public function __construct(
        MemberRepository $memberRepository,
        MemberService $memberService
    )
    {
        parent::__construct();
        $this->middleware('auth');
        $this->selfRepository = $memberRepository;
        $this->memberService = $memberService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('pages.change-password.detail');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChangePasswordRequest $request)
    {
        if (!(Hash::check($request->get('current_password'), auth()->user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Mật khẩu hiện tại không đúng !");
        }

        if (strcmp($request->get('current_password'), $request->get('new_password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "Mật khẩu mới giống mật khẩu cũ !");
        }


        auth()->user()->password = bcrypt($request->get('new_password'));
        auth()->user()->save();

        return redirect()->back()->with("success", "Đổi mật khẩu thành công !");
    }

}
