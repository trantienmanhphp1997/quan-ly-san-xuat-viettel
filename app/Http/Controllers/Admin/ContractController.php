<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ContractRequest;
use App\Imports\ContractImport;
use App\Models\Contract;
use App\Repositories\ContractRepository;
use App\Repositories\DepartmentRepository;
use App\Repositories\StatementRepository;
use App\Utils\ExcelUtil;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ContractController extends Controller
{
    protected $modelDisplayName = 'contracts';
    protected $modelClass = Contract::class;
    protected $prefix = 'contracts';
    protected $filterOptions = [];

    protected $departmentRepository;
    protected $statementRepository;
    protected $excelUtil;


    public function __construct(
        ContractRepository $contractRepository,
        StatementRepository $statementRepository,
        DepartmentRepository $departmentRepository,
        ExcelUtil $excelUtil
    )
    {
        parent::__construct();
        $this->selfRepository = $contractRepository;
        $this->departmentRepository = $departmentRepository;
        $this->statementRepository = $statementRepository;
        $this->excelUtil = $excelUtil;
    }

    public function create(Request $request)
    {
        $editable = true;

        $is_active = $request->query("is_active", 1);

        $departments = $this->departmentRepository->allQuery(["code" => $this->getDepartmentCode()])->where(["status" => 1])->get();

        $statements = $this->statementRepository->allQuery()->get();

        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "editable" => $editable,
            "departments" => $departments,
            "statements" => $statements,
            "is_active" => $is_active
        ]);
    }

    public function show($id)
    {
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("view", $data);
        $editable = false;
        return view($this->viewNamespace . $this->prefix . '.detail', compact(
            'editable',
            'data'
        ));
    }

    public function edit($id)
    {
        $editable = true;
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("update", $data);

        $departments = $this->departmentRepository->allQuery(["code" => $this->getDepartmentCode()])->where(["status" => 1])->get();
        $statements = $this->statementRepository->allQuery()->where(["status" => 1])->get();

        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "data" => $data,
            "editable" => $editable,
            "departments" => $departments,
            "statements" => $statements,
        ]);
    }

    public function store(ContractRequest $request)
    {
        $newData = $request->all();
        $newData["status"] = 1;
        $newData["partner_id"] = 1;

        try {
            $this->selfRepository->create($newData);
            session()->flash("success", __("notification.common.success.add"));
        } catch (\Exception $exception) {
            session()->flash("error", __("notification.common.fail.add"));
        }

        return redirect()->route($this->prefix . ".index");

    }

    public function update(ContractRequest $request, $id)
    {
        $car = $this->selfRepository->withTrashed()->find($id);
        $updatedData = $request->except(["code","link"]);

        try {
            $car->update($updatedData);
            session()->flash("success", __("notification.common.success.update"));
        } catch (\Exception $exception) {
            session()->flash("error", __("notification.common.fail.update"));
        }
        return redirect($request->link);
    }

    public function import(){
        return view("$this->viewNamespace.$this->prefix.import");
    }

    public function importStore(Request $request)
    {
        if ($request->hasFile("contracts")) {
            $file_path = $request->file("contracts")->storeAs('contracts', $request->file('contracts')->getClientOriginalName() . '.' . $request->file('contracts')->extension());
            try {
                $import = Excel::import(new ContractImport(), $file_path);
            } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                $failures = $e->failures();
                return view($this->viewNamespace . $this->prefix . ".import", with([
                    "failures" => $failures,
                ]));
            }
            session()->flash("success", "Import successfully"); //?
            return redirect()->route("$this->prefix.index");
        }
    }

    public function export(Request $request){
        $allParams = $request->input();
        $contracts = $this->selfRepository
            ->allQuery($allParams)->with(['department', "statement"])
            ->get();
        $index = 1;
        foreach($contracts as $contract){
            $contract->{'no'} = $index++;
            $contract->{'statement_code'} = $contract->statement ? $contract->statement->code : '';
            $contract->{'statement_signed_date'} = '';
            $contract->{'contract_code'} = $contract->code;
            $contract->{'title'} = $contract->title;
        }
        $titles = [
            "no","statement_code","statement_signed_date","contract_code","title",
        ];

        $data = [];
        $data["data"] = $contracts;
        $data["titles"] = $titles;
        $this->excelUtil->exportExcel($data, "DSHopDong", "templates/contract-export.xlsx", 6);
    }

}
