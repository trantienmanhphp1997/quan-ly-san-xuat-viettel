<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PaginateForArray;
use App\Traits\isValue;
use App\Utils\CommonUtil;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, isValue;

    protected $selfRepository;
    protected $showView = '';
    protected $editView = '';
    protected $modelDisplayName = 'element';
    protected $modelClass = '';
    protected $prefix = '';
    protected $viewNamespace = 'pages.';
    protected $filterOptions = [
        'is_active' => [
            'label' => 'Status',
            'type' => 'checkBox',
            'options' => [
                '1' => 'Active',
                '-1' => 'Inactive'
            ]
        ],
    ];

//    protected function makeFilterQuery($baseQuery, $filterParameters) {
//        $resultQuery = $baseQuery;
//        foreach ($filterParameters as $key => $value) {
//            if ($value != "" && array_key_exists($key, $this->filterOptions)){
//                $resultQuery = $resultQuery->where($key, $value);
//            }
//        }
//        return $resultQuery;
//    }


    public function __construct()
    {
        // Fetch the Site Settings object
        View::share('routePrefix', $this->prefix);
        View::share('modelDisplayName', $this->modelDisplayName);
        View::share('modelClass', $this->modelClass);
        View::share('filterOptions', $this->filterOptions);
    }

    protected function makeQueryParams($requestParams)
    {
        return $requestParams;
    }

    /**
     * Trả về dạng array key=> value
     * Những tham số muốn bổ sung vào trong hàm index
     * @param $requestParams
     * @return array
     */
    protected function additionalData($requestParams, $data) {
        return [
            'total' => $this->selfRepository->all()->count()
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allParams = $request->except('page');

        if ($request->is_active == -1){
            $data = $this->selfRepository->allQuery($this->makeQueryParams($allParams))->onlyTrashed()->paginate(10);
        } else {
            $data = $this->selfRepository->paginate(10, $this->makeQueryParams($allParams));
        }

        return view($this->viewNamespace . $this->prefix . '.list')->with([
            "data" => $data,
            "allParams" => $request->except('page')
        ]+$this->additionalData($allParams, $data));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("view", $data);
        $editable = false;
        return view($this->viewNamespace . $this->prefix . '.detail', compact(
            'editable',
            'data'
        ));
    }

    public function create(Request $request)
    {
        $editable = true;
        return view($this->viewNamespace . $this->prefix . '.detail', compact(
            "editable"
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->selfRepository->withTrashed()->find($id);

        $this->authorize("update", $data);
        $editable = true;
        return view($this->viewNamespace . $this->prefix . '.detail', compact(
            'data', 'editable'
        ));
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Init array of IDs
        $ids = [];
        if (empty($id) || $id == -1) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        $this->selfRepository->delete($ids);
        $data = [
            'message' => __('common.message.delete_success', ['modelName' => $this->modelDisplayName]) . " {$this->modelDisplayName}",
            'alert-type' => 'success',
        ];
        session()->flash("success", __("notification.common.success.delete"));

        $url = CommonUtil::backPage($request->total,$request->currentPage,$request->perPage,$request->lastPage,$ids,$this->prefix,true);

        return redirect($url)->with($data);
    }

    public function restore(Request $request, $id)
    {

        // Init array of IDs
        $ids = [];
        if (empty($id) || $id == -1) {
            // Bulk restore, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        $this->selfRepository->restoreMany($ids);
        $data = [
            'message' => __('common.message.restore_success', ['modelName' => $this->modelDisplayName]) . " {$this->modelDisplayName}",
            'alert-type' => 'success',
        ];
        session()->flash("success", __("notification.common.success.delete"));
        return redirect()->back()->with($data);
    }

    public function addOtherInfoToMembers($record_plan, $member_ids, $status = 1)
    {
        $members = [];
        foreach ($member_ids as $id) {
            $members[$id] = [
                "record_plan_id" => $record_plan->id,
                "member_id" => $id,
                "status" => $status,
                "start_time" => $record_plan->start_time,
                "end_time" => $record_plan->end_time,
                "send_message_type" => config("common.sendMessageType.invite"),
                "record_plan_status" => $record_plan->status
            ];
        }
        return $members;
    }
    public function addOtherInfoToDevice($record_plan, $device_ids, $status = 1)
    {
        $devices = [];
        foreach ($device_ids as $id) {
            $devices[$id] = [
                "status" => $status,
                "start_time" => $record_plan->start_time,
                "end_time" => $record_plan->end_time
            ];
        }
        return $devices;
    }

    public function downloadFile($filename)
    {
        return response()->download(storage_path("import-template/{$filename}"));
    }

    public function getDepartmentCode(){
        if(auth()->user()->hasRole("super admin")){
            $department_code = config("common.departments.production");
        }else{
            if(strpos(auth()->user()->department_code, config("common.departments.production")) !== false){
                $department_code = auth()->user()->department_code;
            }else{
                $department_code = "";
            }
        }
        return $department_code;
    }
}
