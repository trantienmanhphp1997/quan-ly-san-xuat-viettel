<?php

namespace App\Http\Controllers\Admin\CustomValidation;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

/**
 * Class DeviceCategoryRules custom validation
 *
 * @package App\Http\Controllers\Admin\CustomValidation
 */
class DeviceCategoryRules implements Rule
{
    private $table;
    private $message;
    private $operator;

    /**
     * Create a new rule instance.
     *
     * @param $table
     * @param $message
     * @param array $operator array where query
     */
    public function __construct($table, $message, $operator = [])
    {
        $this->table = $table;
        $this->message = $message;
        $this->operator = $operator;
    }

    /**
     * Check field duplicate on $table
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !DB::table($this->table)->where(array_merge([[$attribute, '=', $value]], $this->operator))->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
