<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PaginateForArray;
use App\Helpers\TreeBuilder;
use App\Http\Resources\DepartmentResource;
use App\Repositories\DepartmentRepository;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    protected $modelDisplayName = 'department';
    protected $prefix = 'departments';
    protected $modelClass = "App\Models\Department";

    public function __construct(DepartmentRepository $departmentRepository)
    {
        parent::__construct();
        $this->selfRepository = $departmentRepository;
    }

    /**
     * Tạo cấu trúc dữ liệu phòng ban dạng cây
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allParams = $request->except('page');

        $data = $this->selfRepository->allQuery($this->makeQueryParams($allParams))->get();
        if ($request->is_active == -1) {
            $data = $this->selfRepository->allQuery($this->makeQueryParams($allParams))->onlyTrashed()->get();
        }
        $canBuildTree = $this->checkIfDataCanBuildTree($data);

        if($canBuildTree){
            $tree = TreeBuilder::buildTree($data);
        }else{
            $tree = $data;
        }

        return view($this->viewNamespace . $this->prefix . '.list')->with([
            "data" => $data,
            "tree" => $tree,
            "allParams" => $allParams
        ]);
    }

    public function checkIfDataCanBuildTree($list, $parentKey = "parent_id"){
        //xử lý data xem có cần build tree hay k
        $referList = array();
        foreach ($list as $item){
            $referList[$item[$parentKey]][] = $item;
        }
        if(array_key_exists(0, $referList)){
            return true;
        }
        return false;
    }
}
