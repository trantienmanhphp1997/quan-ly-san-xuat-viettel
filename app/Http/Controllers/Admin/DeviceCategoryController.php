<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\CustomValidation\DeviceCategoryRules;
use App\Models\DeviceCategory;
use App\Repositories\DeviceCategoryRepository;
use App\Repositories\DeviceRepository;
use App\Utils\CommonUtil;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DeviceCategoryController extends Controller
{
    protected $modelDisplayName = 'device-categories';
    protected $modelClass = DeviceCategory::class;
    protected $prefix = 'device-categories';
    protected $filterOptions = [];
    private $tableName = 'device_categories';

    public function __construct(DeviceCategoryRepository $deviceCategoryRepository, DeviceRepository $deviceRepository)
    {
        parent::__construct();
        $this->selfRepository = $deviceCategoryRepository;
        $this->deviceRepository = $deviceRepository;
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function create(Request $request)
    {
        $editable = true;
        $hideCategory = false;
        $is_active = $request->query("is_active", 1);

        $categories = $this->selfRepository->allQuery()->with('parent')->get();
        foreach ($categories as $category) {
            if ($category->parent) {
                $category->parent->name = CommonUtil::Categ($category->parent);
            }
            $category->name = CommonUtil::Categ($category);
        }
        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "editable" => $editable,
            "categories" => $categories,
            "is_active" => $is_active,
            "hideCategory" => $hideCategory
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => [
                'required',
                'max:191',
                new DeviceCategoryRules($this->tableName, __('server_validation.device_category.name.unique'), [['deleted_at', '=', null]])
            ],
            "display_code" => [
                'required',
                new DeviceCategoryRules($this->tableName, __('server_validation.device_category.display_code.unique'), [['deleted_at', '=', null]])],
        ]);
        $newData = $request->except('parent_id');
        if ($request->input('parent_id') == 0) {
            $newData['code'] = $newData['display_code'];
            $newData['parent_id'] = 0;
        } else {
            $category = $this->selfRepository->allQuery()->where('id', $request->parent_id)->first();
            if ($category == null) {
                $newData['code'] = $newData['display_code'];
                $newData['parent_id'] = 0;
            } else {
                $newData['parent_id'] = $category->id;
                $newData['code'] = $category->display_code . '_' . $newData['display_code'];
            }
        }
        $newData['status'] = 1;
        try {
            $this->selfRepository->create($newData);
            session()->flash("success", __("notification.common.success.add"));
            return redirect()->route($this->prefix . ".index");
        } catch (Exception $e) {
            session()->flash("error", __("notification.common.fail.add"));
            return redirect()->back()->withInput();
        }
    }

    /**
     * @param int $id
     * @return Factory|View|Response
     * @throws AuthorizationException
     */
    public function show($id)
    {
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("view", $data);
        $editable = false;
        $parent_name = "None";
        $hideCategory = true;
        if ($data->parent_id !== 0) {
            $parent_category = $this->selfRepository->allQuery()->find($data->parent_id);
            $parent_name = $parent_category->name;
        }
        return view($this->viewNamespace . $this->prefix . '.detail', compact('editable', 'data', 'parent_name', 'hideCategory'));
    }

    /**
     * @param int $id
     * @return Factory|View|Response
     * @throws AuthorizationException
     */
    public function edit($id)
    {
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("update", $data);
        $editable = true;
        $hideCategory = false;
        $categories = $this->selfRepository->allQuery()->where('id', '<>', $id)->with('parent')->get();
        // $categories = $this->selfRepository->allQuery()->with('parent')->get();
        foreach ($categories as $category) {
            if ($category->parent) {
                $category->parent->name = CommonUtil::Categ($category->parent);
            }
            $category->name = CommonUtil::Categ($category);
        }

        return view($this->viewNamespace . $this->prefix . '.detail', compact('data', 'editable', 'categories', 'hideCategory'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => [
                'required',
                'max:191',
                new DeviceCategoryRules($this->tableName, __('server_validation.device_category.name.unique'), [['deleted_at', '=', null], ['id', '<>', $id]])
            ],
            "display_code" => [
                'required',
                new DeviceCategoryRules($this->tableName, __('server_validation.device_category.display_code.unique'), [['deleted_at', '=', null], ['id', '<>', $id]])],
        ]);
        $deviceCategory = $this->selfRepository->withTrashed()->find($id);
        $updatedData = $request->except('link');

        try {
            $deviceCategory->update($updatedData);
            session()->flash("success", __("notification.common.success.update"));
            return redirect($request->link);
        } catch (Exception $e) {
            session()->flash("error", __("notification.common.fail.update"));
            return redirect()->back()->withInput();
        }
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function destroy(Request $request, $id)
    {
        $deviceCategories = $this->selfRepository->allQuery()->pluck('parent_id')->toArray();

        // Init array of IDs
        $ids = [];
        if (empty($id) || $id == -1) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        //kiểm tra xem danh mục thiết bị có chứa danh mục con hay không ??,
        // nếu không có thì kiểm tra điều kiện tiếp theo !
        if (count(array_diff($ids, $deviceCategories)) == count($ids)) {
            //kiểm tra danh mục có thiết bị hay không ?
            $device = $this->deviceRepository->allQuery()->whereIn("device_category_id", $ids)->pluck('id');
            if (count($device) == 0) {
                // Nếu danh mục rỗng thì mới xóa
                $this->selfRepository->delete($ids);
                session()->flash("success", __("notification.common.success.delete"));
            } else {
                session()->flash("error", __("notification.device_category.hasItem"));
            }

            return redirect()->back();
        } else {
            session()->flash("error", __("notification.common.fail.delete_parent"));
            return redirect()->back();
        }
    }

    public function getListCategory()
    {
        return $device_categories = $this->selfRepository->allQuery()->get();
    }
}
