<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DeviceRequest;
use App\Models\Device;
use App\Imports\DeviceImport;
use App\Repositories\DeviceCategoryRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\RecordPlanDeviceXrefRepository;
use App\Repositories\RecordPlanRepository;
use App\Services\DeviceService;
use App\Services\RecordPlanService;
use App\Utils\ExcelUtil;
use App\ViewModel\PaginateModel;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use App\Utils\CommonUtil;

class DeviceController extends Controller
{
    protected $modelDisplayName = 'devices';
    protected $modelClass = Device::class;
    protected $prefix = 'devices';
    protected $filterOptions = [];
    public $deviceCategoryRepository;
    public $deviceService;
    private $excelUtil;
    protected $recordPlanRepository;
    protected $recordPlanDeviceXrefRepository;
    public $recordPlanService;

    /**
     * DeviceController constructor.
     * @param DeviceRepository $deviceRepository
     * @param DeviceCategoryRepository $deviceCategoryRepository
     */
    public function __construct(
        DeviceRepository $deviceRepository, DeviceCategoryRepository $deviceCategoryRepository, DeviceService $deviceService,ExcelUtil $excelUtil,
        RecordPlanRepository $recordPlanRepository,
        RecordPlanDeviceXrefRepository $recordPlanDeviceXrefRepository,
        RecordPlanService $recordPlanService
    )
    {
        parent::__construct();
        $this->selfRepository = $deviceRepository;
        $this->deviceCategoryRepository = $deviceCategoryRepository;
        $this->deviceService = $deviceService;
        $this->excelUtil = $excelUtil;
        $this->recordPlanRepository = $recordPlanRepository;
        $this->recordPlanDeviceXrefRepository = $recordPlanDeviceXrefRepository;
        $this->recordPlanService = $recordPlanService;
    }

    protected function additionalData($requestParams, $data) {
        $categories = $this->deviceCategoryRepository->allQuery()->get();
        $statusCount = Device::select('status', DB::raw('count(*) as total'))->groupBy('status')->get();
        $countInfo = $statusCount->mapWithKeys(function($item) {
            return [$item->status => $item->total];
        });
        $countInfo['total'] = $countInfo->sum();
        return [
            'countInfo' => $countInfo,
            'lendingCount' => Device::has('lendingPlans')->count(),
            'requestReturn' => Device::has('requestReturn')->count(),
            'categories' => $categories
        ];
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function create(Request $request)
    {
        $editable = true;
        $is_active = $request->query("is_active", 1);

        $categories = $this->deviceCategoryRepository->allQuery()->get();

        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "editable" => $editable,
            "categories" => $categories,
            "is_active" => $is_active
        ]);
    }

    /**
     * @param DeviceRequest $request
     * @return RedirectResponse
     */
    public function store(DeviceRequest $request)
    {
        $newData = $request->all();
        $device_catetory = $this->deviceCategoryRepository->findOrFail($request->device_category_id);
        $newData['device_category_code'] = $device_catetory->code;
        $newData["quantity"] = 1;
        $newData["status"] = array_key_exists("status",  $request->all()) ? config("common.status.device.active") : config("common.status.device.inactive");

        try {
            $this->selfRepository->create($newData);
            session()->flash("success", __("notification.common.success.add"));
            return redirect()->route($this->prefix . ".index");
        } catch (\Exception $e) {
            session()->flash("error", __("notification.common.fail.add"));
            return redirect()->back()->withInput();
        }
    }

    public function show($id)
    {
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("view", $data);
        $editable = 0;
        $categories = $this->deviceCategoryRepository->allQuery()->get();
        return view($this->viewNamespace . $this->prefix . '.detail', compact('editable', 'data', 'categories'));
    }

    public function edit($id)
    {
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("update", $data);
        $editable = true;
        $categories = $this->deviceCategoryRepository->allQuery()->get();

        return view($this->viewNamespace . $this->prefix . '.detail', compact('data', 'editable', 'categories'));
    }

    public function update(DeviceRequest $request, $id)
    {
        $device = $this->selfRepository->withTrashed()->find($id);
        $updatedData = $request->except('link');
        $updatedData["status"] = array_key_exists("status",  $request->all()) ? config("common.status.device.active") : config("common.status.device.inactive");
        try {
            $device->update($updatedData);
            session()->flash("success", __("notification.common.success.update"));
            return redirect($request->link);
        }catch (\Exception $exception){
            session()->flash("error", __("notification.common.fail.update"));
            return redirect()->back()->withInput();
        }
    }

    public function getAvailableDevicesByTime(Request $request){
        $start_time = $request->start_time;
        $end_time = $request->end_time;
        $data = $this->deviceService->getAvailableDevicesByTime($start_time, $end_time);
        return $data;
    }

    public function reportBrokenDevices(Request $request,$id){
        // Init array of IDs
        $device_ids = [];
        $reason_report_broken = $request->input('reason_report_broken','');
        if (empty($id) || $id == -1) {
            // Bulk restore, get IDs from POST
            $device_ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $device_ids[] = $id;
        }

        $rs = $this->selfRepository->updateDeviceStatus($device_ids, config("common.status.device.broken"), $reason_report_broken);

        if($rs){
            session()->flash("success", __("notification.common.success.report-broken"));
        }else{
           session()->flash("error", __("notification.common.fail.report-broken"));
        }
        return redirect()->back();
    }

    public function getLendingDevices(Request $request){
        $allParams = [
            's' => $request->query("s", ""),
            'status' =>  $request->query("status", 1)
        ];

        $record_plans = $this->deviceService->getLendingDevices($allParams);
        return view($this->viewNamespace . $this->prefix . ".lending-list", with([
            "allParams" => $allParams,
            "data" => $record_plans,
            "isPivotData" => true,
            "prefix" => "device"
        ]));
    }

    public function destroy(Request $request, $id)
    {
        // Init array of IDs
        $device_ids = [];
        if (empty($id) || $id == -1) {
            // Bulk delete, get IDs from POST
            $device_ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $device_ids[] = $id;
        }
        $deleted = false;
        $result = $this->deviceService->checkIfDevicesAssignedToRecordPlan($device_ids);

        $cannt_delete_rps = $result["cannt_delete_rps"];
        $can_delete_rps = $result["can_delete_rps"];
        if(!count($cannt_delete_rps)){

            $record_plan_ids = count($can_delete_rps) ? $can_delete_rps->pluck("id")->toArray() : [];
            $this->deviceService->handleDeletedDevices($device_ids, $record_plan_ids);
            $this->selfRepository->delete($device_ids);
            $deleted = true;
            if(!count($can_delete_rps)){
                session()->flash("success", __("notification.common.success.delete"));
            }else{
                $record_plan_names = join(", ", $can_delete_rps->pluck("name")->toArray());
                session()->flash("warning", __("notification.member.warning.reallocate_device", ["record_plan_name" => $record_plan_names]));
            }

        }else{
            if(count($device_ids) == 1){
                $device = $this->selfRepository->find($device_ids[0]);
                $record_plan_names = join(", ", $cannt_delete_rps->pluck("name")->toArray());
                session()->flash("warning", __("notification.member.warning.delete_device", ["device_name" => $device->name, "record_plan_name" => $record_plan_names]));
            }else{
                session()->flash("warning", __("notification.member.warning.delete_list_devices"));
            }
        }
        $url = CommonUtil::backPage($request->total,$request->currentPage,$request->perPage,$request->lastPage,$device_ids,$this->prefix,$deleted);
        return redirect($url);
    }

    public function restore(Request $request, $id)
    {
        // Init array of IDs
        $device_ids = [];
        if (empty($id) || $id == -1) {
            // Bulk delete, get IDs from POST
            $device_ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $device_ids[] = $id;
        }
        $rs = $this->selfRepository->updateDeviceStatus($device_ids, config("common.status.device.active"));
        if($rs){
            session()->flash("success", __("notification.common.success.restore"));
        }else{
           session()->flash("error", __("notification.common.fail.restore"));
        }
        return redirect()->back();
    }

    public function export(Request $request){
        $allParams = $request->query();
        $devices = $this->selfRepository
            ->allQuery($this->makeQueryParams($allParams))->with('category')
            ->get();
        $index = 1;
        foreach($devices as $device){
            $device->{'no'} = $index++;
            $device->{'category'} = $device->category ? $device->category->name : '';
        }
        $titles = [
            "no","device_category_code", "name","category","serial","note"
        ];

        $data = [];
        $data["data"] = $devices;
        $data["titles"] = $titles;
        $this->excelUtil->exportExcel($data, "DSThietBi", "templates/device-export.xlsx", 6);
    }

    public function exportDeviceAssignment(Request $request){
        $allParams = $request->input();
        $search = [];
        if(array_key_exists("s", $allParams)){
            $search = [
                "s" => $allParams['s']
            ];
        }

        $items = $this->recordPlanDeviceXrefRepository->allQuery($search)->whereHas("record_plan", function ($query){
            $query->where([
                "record_plans.device_allocate_status" => config("common.status.propose_device.approved"),
            ]);
        })->with(["record_plan", "device"])->get();

        $items_map = [];
        $index = 1;
        foreach ($items as $item){
            if(array_key_exists($item->record_plan_id, $items_map)){
                $items_map[$item->record_plan_id]["devices"] .= ", "."\n".optional($item->device)->name;
            }else{
                $record_plan_status = "";
                foreach (config('common.status.record_plan') as $key => $value) {
                    if ($value == $item->record_plan->status) {
                        $record_plan_status = __('status.record_plan.' . $key);
                    }
                }
                $items_map[$item->record_plan_id] = [
                    "no" => $index,
                    "record_plan_name" => optional($item->record_plan)->name,
                    "start_time" => optional($item->record_plan)->start_time ? date("d-m-y H:i:s", strtotime(optional($item->record_plan)->start_time)) : "",
                    "end_time" =>optional($item->record_plan)->end_time ? date("d-m-y H:i:s", strtotime(optional($item->record_plan)->end_time)) : "",
                    "address" => optional($item->record_plan)->address,
                    "offer_device_number" => optional($item->record_plan)->offer_device_number,
                    "offer_device_note" => optional($item->record_plan)->offer_device_note,
                    "status" => $record_plan_status,
                    "devices" => optional($item->device)->name,
                ];
                $index++;
            }
        }
        $result["data"] = array_values($items_map);
        $result["titles"] = ["no", "record_plan_name", "start_time", "end_time", "address", "offer_device_number", "offer_device_note", "status", "devices"];

        $this->excelUtil->exportExcel($result, "DSLichSuDungThietBi", "templates/assignment_device-export.xlsx", 5);
    }

    public function exportDataExpiredLendingDayDevices(Request $request)
    {
        $allParams = $request->query();
        //lấy ra những thiết bị đang cho mượn
        // -quá hạn: lịch sản xuất đã kết thúc những vẫn chưa trả tức là trạng thái vẫn bằng 3
        $data = $this->recordPlanDeviceXrefRepository->allQuery(['status'=>config('common.status.propose_device.delivered')])->where('end_time','<', date('Y-m-d H:i:s',time()))->with(["record_plan", "device"])->get();
        $items_map = [];
        $index = 1;
        foreach ($data as $item) {
            if(array_key_exists($item->record_plan_id, $items_map)){
                $items_map[$item->record_plan_id]["devices"] .= ", ".optional($item->device)->name;
            }else{
                $items_map[$item->record_plan_id] = [
                    "no" => $index,
                    "record_plan_name" => optional($item->record_plan)->name,
                    "start_time" => optional($item->record_plan)->start_time ? Date::PHPToExcel(date("d/m/Y h:i", strtotime(optional($item->record_plan)->start_time))) : "",
                    "end_time" =>optional($item->record_plan)->end_time ? Date::PHPToExcel(date("d/m/Y h:i", strtotime(optional($item->record_plan)->end_time))) : "",
                    "address" => optional($item->record_plan)->address,
                    "devices" => optional($item->device)->name,
                ];
                $index++;
            }
        }
        $result["data"] = array_values($items_map);
        $result["titles"] = ["no", "record_plan_name", "start_time", "end_time", "address", "devices"];

        $this->excelUtil->exportExcel($result, "DSLichMuonThietBiQuaHan", "templates/expired_lending_day_devices_export.xlsx", 5);

    }
    public function import(Request $request)
    {
        return view($this->viewNamespace . $this->prefix . '.import', [
            "routePrefix" => $this->prefix,
        ]);
    }

    public function storeImport (Request $request)
    {
        if ($request->hasFile("devices")) {
            $file_path = $request->file("devices")->storeAs('devices', $request->file('devices')->getClientOriginalName() . '.' . $request->file('devices')->extension());
            try {
                $import = Excel::import(new DeviceImport(), $file_path);
            } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                $failures = $e->failures();
                return view($this->viewNamespace . $this->prefix . ".import", with([
                    "failures" => $failures,
                ]));
            }
            session()->flash("success", __("notification.common.success.import")); //?
            return redirect()->route("$this->prefix.index");
        }
    }

    public function getListDeviceWaitConf(Request $request)
    {
        return $this->selfRepository->allQuery()->whereIn('id',$request->ids)->get();
    }
}
