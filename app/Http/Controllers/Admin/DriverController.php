<?php


namespace App\Http\Controllers\Admin;


class DriverController extends MemberController
{
    protected $modelDisplayName = 'transportation';
    protected $prefix = 'transportation';
    protected $departmentCode = 'transportation';

}
