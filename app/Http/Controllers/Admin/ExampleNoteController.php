<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PaginateForArray;
use App\Helpers\TreeBuilder;
use App\Http\Requests\PlanInfoRequest;
use App\Http\Resources\DepartmentResource;
use App\Repositories\DepartmentRepository;
use App\Repositories\ExampleNoteRepository;
use Illuminate\Http\Request;

class ExampleNoteController extends Controller
{
    protected $modelDisplayName = 'example-note';
    protected $prefix = 'example-notes';
    protected $modelClass = "App\Models\ExampleNote";

    public function __construct(ExampleNoteRepository $exampleNoteRepository)
    {
        parent::__construct();
        $this->selfRepository = $exampleNoteRepository;
    }

    public function store(Request $request){
        $newData = $request->all();
        $newData["status"] = 1;

        try {
            $this->selfRepository->create($newData);
            session()->flash("success", __("notification.common.success.add"));
        }catch (\Exception $exception){
            session()->flash("error", __("notification.common.fail.add"));
        }

        return redirect()->route($this->prefix.".index");

    }

    public function update(Request $request, $id){
        $data = $this->selfRepository->withTrashed()->find($id);
        $updatedData = $request->all();

        try {
            $data->update($updatedData);
            session()->flash("success", __("notification.common.success.update"));
        }catch (\Exception $exception){
            session()->flash("error", __("notification.common.fail.update"));
        }
        return redirect($request->link);
    }
}
