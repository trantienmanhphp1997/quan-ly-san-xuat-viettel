<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\GroupRepository;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    protected $modelDisplayName = 'groups';
    protected $prefix = 'groups';
    protected $modelClass = "App\Models\Group";

    protected $filterOptions = [
        'number_of_seats' => [
            'label' => 'Number of Seat',
            'type' => 'checkBox',
            'options' => [
                "" => 'All',
                4 => '4 Seat',
                7 => '7 Seat'
            ]
        ],
        'car_status' => [
            'label' => 'Car Status',
            'tooltip' => 'Trạng thái của xe',
            'type' => 'checkBox',
            'options' => [
                "" => 'All',
                -2 => 'Inactive',
                -1 => 'Refuse',
                1 => 'Pending',
                2 => 'Active',
            ]
        ],
        'transmission_type' => [
            'label' => 'Transmission type',
            'tooltip' => 'Loại truyền tải của xe',
            'type' => 'checkBox',
            'options' => [
                "" => 'All',
                'manual' => 'Manual',
                'auto' => 'Auto',
            ]
        ],
    ];

    /**
     * Create a new controller instance.
     * @param GroupRepository $groupRepository
     * @return void
     */
    public function __construct(GroupRepository $groupRepository)
    {
        parent::__construct();
        $this->middleware('auth');
        $this->middleware('admin');
        $this->selfRepository = $groupRepository;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'       => 'required|min:1|max:256',
            'code'      => 'required|email|max:256'
        ]);
        $group = $this->selfRepository->find($id);
        $group->name       = $request->input('name');
        $group->code      = $request->input('code');
        $group->save();
        $request->session()->flash('message', 'Successfully updated user');
        return redirect()->route($this->prefix .'.index');
    }
}
