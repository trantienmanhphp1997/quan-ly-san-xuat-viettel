<?php
namespace App\Http\Controllers\Admin;

use App\Jobs\SendDailyIncomingRecPlans;
use App\Mail\ReportDailyRecordPlansEmail;
use App\Models\User;
use App\Repositories\RecordPlanRepository;
use App\Repositories\UserRepository;
use App\Services\MochaService;
use App\Services\RecordPlanService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public $userRepository;
    public $recordPlanRepository;
    public $recordPlanService;
    public function __construct(
        UserRepository $userRepository,
        RecordPlanRepository $recordPlanRepository,
        RecordPlanService $recordPlanService
    )
    {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->recordPlanRepository = $recordPlanRepository;
        $this->recordPlanService = $recordPlanService;

    }

    public function sendDailyPlannedReportRecordPlans(){
        //gửi tin nhắn
        $tomorrow = Carbon::tomorrow();
        $startOfDay = $tomorrow->copy()->startOfDay();
        $endOfDay = $tomorrow->copy()->endOfDay();
        $status = [
            config("common.status.record_plan.pending"),
            config("common.status.record_plan.approved"),
            config("common.status.record_plan.in_progress"),
            config("common.status.record_plan.wrap"),
            config("common.status.record_plan.done"),
        ];
        $attachmentLink = $this->recordPlanService->getDailyRecordPlans($startOfDay, $endOfDay, $status);
        $oa_id = config("mocha.oa_id");
        $label = "Xem file";
        $deeplink = "mocha://survey?ref=" . urlencode($attachmentLink);
        $reporter_managers = User::where(["status" => 1])->whereNotNull("phone_number")->get();
        MochaService::sendMessageToUser($oa_id, 'Danh sách công việc ngày mai ' . $tomorrow->format('d/m/Y'), $reporter_managers, $label, $deeplink);

        //gửi email
        Log::info("start to send mail to users");
        Log::info("send mail ahihi ");
//        $userRepository = app("App\Repositories\UserRepository");
//        $recordPlanService = app("App\Services\RecordPlanService");
        $content = [
            "title" => "Báo cáo lịch sản xuất sắp diễn ra",
            "body" => "Danh sách lịch sản xuất"
        ];
        $recipients = $this->userRepository->allQuery()->whereNotNull(["email"])->pluck("email")->toArray();

        Log::info("mail recipients" ,[$recipients]);
        Mail::to($recipients)->send(new ReportDailyRecordPlansEmail($content, $attachmentLink));
        Log::info("send mail successfully");
        return true;
    }

    public function sendDailyExecutedReportRecordPlans(){
        //gửi tin nhắn
        $now = Carbon::now();
        $startOfDay = $now->copy()->startOfDay();
        $endOfDay = $now->copy()->endOfDay();
        $status = [
            config("common.status.record_plan.pending"),
            config("common.status.record_plan.approved"),
            config("common.status.record_plan.in_progress"),
            config("common.status.record_plan.wrap"),
            config("common.status.record_plan.done"),
        ];
        $attachmentLink = $this->recordPlanService->getDailyRecordPlans($startOfDay, $endOfDay, $status);
        $oa_id = config("mocha.oa_id");
        $label = "Xem file";
        $deeplink = "mocha://survey?ref=" . urlencode($attachmentLink);
        $reporter_managers = User::where(["status" => 1])->whereNotNull("phone_number")->get();
        MochaService::sendMessageToUser($oa_id, "Báo cáo lịch sản xuất đã diễn ra trong ngày " . $now->format('d/m/Y'), $reporter_managers, $label, $deeplink);

        //gửi email
        Log::info("start to send mail to users");
        $content = [
            "title" => "Báo cáo lịch sản xuất đã diễn ra trong ngày " . $now->format('d/m/Y'),
            "body" => "Danh sách lịch sản xuất đã diễn ra trong ngày"
        ];
        $recipients = $this->userRepository->allQuery()->whereNotNull(["email"])->pluck("email")->toArray();

        Log::info("mail recipients" ,[$recipients]);
        Mail::to($recipients)->send(new ReportDailyRecordPlansEmail($content, $attachmentLink));
        Log::info("send mail successfully");
        return true;
    }
}
