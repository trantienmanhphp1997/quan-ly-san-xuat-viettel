<?php

namespace App\Http\Controllers\Admin;

use App\Models\Menu;
use App\Repositories\MenuRepository;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    protected $modelDisplayName = 'menu';
    protected $prefix = 'menus';
    protected $modelClass = Menu::class;

    public function __construct(MenuRepository $menuRepository)
    {
        parent::__construct();
        $this->selfRepository = $menuRepository;
    }
}
