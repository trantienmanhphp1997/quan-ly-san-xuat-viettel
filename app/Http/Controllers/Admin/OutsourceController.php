<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OutsourceRequest;
use App\Repositories\OutsourceRepository;
use App\Repositories\OutsourceTypeRepository;
use App\Services\OutsourceService;
use Illuminate\Http\Request;

class OutsourceController extends Controller
{
    protected $modelDisplayName = 'outsources';
    protected $modelClass = "App\Models\Outsource";
    protected $prefix = 'outsources';
    protected $filterOptions = [];
    protected $outsourceTypeRepository;
    public $outsourceService;

    public function __construct(
        OutsourceRepository $outsourceRepository,
        OutsourceTypeRepository $outsourceTypeRepository,
        OutsourceService $outsourceService
    )
    {
        parent::__construct();
        $this->selfRepository = $outsourceRepository;
        $this->outsourceTypeRepository = $outsourceTypeRepository;
        $this->outsourceService = $outsourceService;
    }
    public function index(Request $request)
    {
        $allParams = $request->except('page');

        if(auth()->user()->hasRole("super admin")){
            $outsource_type_ids = $this->outsourceTypeRepository->allQuery([ "status" => 1])->pluck("id")->toArray();
        }else{
            $outsource_type_ids = $this->outsourceTypeRepository->allQuery(["department_code" => auth()->user()->department_code, "status" => 1])->pluck("id")->toArray();
        }

        $allParams["outsource_type_id"] = $outsource_type_ids;

        if ($request->is_active == -1){
            $data = $this->selfRepository->allQuery($this->makeQueryParams($allParams))->onlyTrashed()->paginate(10);
        } else {
            $data = $this->selfRepository->paginate(10, $this->makeQueryParams($allParams));
        }

        return view($this->viewNamespace . $this->prefix . '.list')->with([
            "data" => $data,
            "allParams" => $request->except('page')
        ]);
    }

    public function create(Request $request)
    {
        $editable = true;

        $is_active = $request->query("is_active", 1);

        $outsource_types = $this->outsourceTypeRepository->allQuery()->where(["status" => 1])->get();

        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "editable" => $editable,
            "outsource_types" => $outsource_types,
            "is_active" => $is_active
        ]);
    }

    public function show($id)
    {
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("view", $data);
        $editable = false;
        return view($this->viewNamespace . $this->prefix . '.detail', compact(
            'editable',
            'data'
        ));
    }

    public function edit($id)
    {
        $editable = true;
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("update", $data);
        $outsource_types = $this->outsourceTypeRepository->allQuery()->where(["status" => 1])->get();

        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "data" => $data,
            "editable" => $editable,
            "outsource_types" => $outsource_types
        ]);
    }

    public function store(OutsourceRequest $request)
    {
        $newData = $request->all();
        $newData["status"] = 1;

        try {
            $outsource = $this->selfRepository->create($newData);
            session()->flash("success", __("notification.common.success.add"));
        } catch (\Exception $exception) {
            session()->flash("error", __("notification.common.fail.add"));
        }

        return redirect()->route($this->prefix . ".index");

    }

    public function update(OutsourceRequest $request, $id)
    {
        $outsource = $this->selfRepository->withTrashed()->find($id);
        $updatedData = $request->except('link');

//        try {
        $outsource->update($updatedData);
        session()->flash("success", __("notification.common.success.update"));
//        }catch (\Exception $exception){
//            session()->flash("error", __("notification.common.fail.update"));
//        }
        return redirect($request->link);
    }

}
