<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\TreeBuilder;
use App\Models\Department;
use App\Models\Menu;
use App\Repositories\DepartmentRepository;
use App\Repositories\MenuRepository;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    protected $modelDisplayName = 'permissions';
    protected $prefix = 'permissions';
    protected $modelClass = Permission::class;

    private $roleRepository;
    private $menuRepository;
    private $departmentRepository;

    /**
     * Create a new controller instance.
     * @param PermissionRepository $permissionRepository
     * @param RoleRepository $roleRepository
     * @param MenuRepository $menuRepository
     * @param DepartmentRepository $departmentRepository
     * @return void
     */
    public function __construct(
        PermissionRepository $permissionRepository,
        RoleRepository $roleRepository,
        MenuRepository $menuRepository,
        DepartmentRepository $departmentRepository
    )
    {
        parent::__construct();
        $this->middleware('auth');
        $this->middleware('admin');
        $this->selfRepository = $permissionRepository;
        $this->roleRepository = $roleRepository;
        $this->menuRepository = $menuRepository;
        $this->departmentRepository = $departmentRepository;
    }

    /**
     * Recursive function to check if one menu does not have,
     * or containing children menu that does not have certain role
     * @param Menu $menu
     * @param Role $role
     * @return bool
     */
    private function childMenuNotHavingPermission(Menu $menu, Role $role)
    {
        if (count($menu->children) === 0) {
            return !$role->hasPermissionTo($menu->permissions->first()->name);
        }
        return $menu->children->contains(function ($submenu) use ($role) {
            return $this->childMenuNotHavingPermission($submenu, $role);
        });
    }

    /**
     * Recursive function to check if one menu has,
     * or containing children menu that has certain role
     * @param Menu $menu
     * @param Role $role
     * @return bool
     */
    private function childMenuHavingPermission(Menu $menu, Role $role)
    {
        if (count($menu->children) === 0) {
            return $role->hasPermissionTo($menu->permissions->first()->name);
        }
        return $menu->children->contains(function ($submenu) use ($role) {
            return $this->childMenuHavingPermission($submenu, $role);
        });
    }

    /**
     * Recursive function to check if one menu has,
     * or containing children menu that has id in array of ids
     * @param Menu $menu
     * @param array $ids
     * @return bool
     */
    private function childMenuHavingIds(Menu $menu, array $ids)
    {
        if (count($menu->children) === 0) {
            return in_array($menu->id, $ids);
        }
        return $menu->children->contains(function ($submenu) use ($ids) {
            return $this->childMenuHavingIds($submenu, $ids);
        });
    }

    public function access(Request $request)
    {
        $departmentId = $request->get("department");
        $department = $this->departmentRepository->findWith($departmentId, ["roles"]);
        $departmentRole = $department->roles->first(); // Tạm thời 1 department chỉ có 1 role, lấy role đầu tiên

        // If department is a root department (parent_id = 0), get all menus available
        // and if department is a child department, get all menus accessible by direct parent department
        if ($department->parent_id === 0) {
            $menus = $this->menuRepository->getAllMenuWithAccessPermissions();
        } else {
            $parentMenu = $this->departmentRepository->find($department->parent_id);
            $parentPermissionIds = $parentMenu->getPermissionsViaRoles()->map(function ($p) {
                return $p->menu_id;
            })->toArray();
            if (count($parentPermissionIds) === 0) {
                $menus = collect([]);
            } else {
                $menus = $this->menuRepository->getAllMenuWithAccessPermissions($parentPermissionIds);
            }
        }
        /**
         * Lọc danh sách menu có thể thêm quyền truy cập, theo role của department hiện tại,
         * lấy menu theo 1 trong 2 điều kiện:
         * - Role không có permission access tương ứng với menu
         * - Hoặc role không có permission access với ít nhất 1 menu con trong menu đang xét
         */
        $availableMenus = $menus->filter(function ($menu) use ($departmentRole) {
            return !$departmentRole->hasPermissionTo($menu->permissions->first()->name) ||
                $this->childMenuNotHavingPermission($menu, $departmentRole);
        });

        /**
         * Lọc danh sách menu có quyền truy cập, theo role của department hiện tại,
         * lấy menu theo 1 trong 2 điều kiện:
         * - Role có permission access tương ứng với menu
         * - Hoặc role có permission access với ít nhất 1 menu con trong menu đang xét
         */
        $selectedMenus = $menus->filter(function ($menu) use ($departmentRole) {
            return $departmentRole->hasPermissionTo($menu->permissions->first()->name) ||
                $this->childMenuHavingPermission($menu, $departmentRole);
        })->map(function ($item) {
            // Map thành 1 object mới, tránh bị trùng với danh sách availableMenus
            return clone $item;
        });

        // Tạo cây menu tương ứng
        $availableMenuTree = TreeBuilder::buildTree($availableMenus);
        $selectedMenuTree = TreeBuilder::buildTree($selectedMenus);

        return view($this->viewNamespace . $this->prefix . '.access', [
            "availableMenus" => $availableMenuTree,
            "selectedMenus" => $selectedMenuTree,
            "role" => $departmentRole,
            "departmentId" => $departmentId
        ]);
    }

    /**
     * Save access permissions from one department
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveAccessPermissions(Request $request)
    {
        if(!$request->menu){
            session()->flash("error", __("notification.common.fail.no_menu"));
            return redirect()->back();
        }
        $roleId = $request->get("roleId");
        $role = $this->roleRepository->findOrFail($roleId);

        // Find all menus that has ids from requested data
        // and menus containing submenus that has ids from requested data
        $menus = $this->menuRepository->getAllMenuWithAccessPermissions();
        $menuIds = $request->get("menu");
        $selectedMenus = $menus
            ->filter(function ($menu) use ($menuIds) {
                return $this->childMenuHavingIds($menu, $menuIds);
            });

        $permissions = $selectedMenus->map(function ($menu) {
            return $menu->permissions->first();
        });

        foreach ($permissions as $permission) {
            if (!$role->hasPermissionTo($permission)) {
                $role->givePermissionTo($permission);
            }
        }
        return redirect()->route("permissions.access", [
            'department' => $request->get("departmentId")
        ]);
    }

    /**
     * Remove access permissions from one department
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeAccessPermissions(Request $request)
    {
        if(!$request->menu){
            session()->flash("error", __("notification.common.fail.no_menu"));
            return redirect()->back();
        }

        $roleId = $request->get("roleId");
        $role = $this->roleRepository->findOrFail($roleId);

        $permissions = $this->menuRepository
            ->getAllMenuWithAccessPermissions($request->get("menu"))
            ->map(function ($menu) {
                return $menu->permissions->first();
            });
        foreach ($permissions as $permission) {
            if ($role->hasPermissionTo($permission)) {
                $role->revokePermissionTo($permission);
            }
        }
        return redirect()->route("permissions.access", [
            'department' => $request->get("departmentId")
        ]);
    }

    public function features(Request $request)
    {
        $departmentId = $request->get("department");
        $department = $this->departmentRepository->findWith($departmentId, ["roles"]);
        $permissions = $department->getPermissionsViaRoles();

        // Get all menu with access permission
        $accessibleMenuIds = $permissions
            ->filter(function ($permission) {
                return substr($permission->name, 0, strlen("access")) === "access";
            })
            ->map(function ($permission) {
                return $permission->menu_id;
            });
        $accessibleMenus = $this->menuRepository
            ->getAllMenusWithPermissionsExceptAccess($accessibleMenuIds->toArray());

        // Get all current permission array
        $currentPermissions = $permissions->map(function ($permission) {
            return $permission->name;
        })->toArray();

        // If there is no permissions or accessible menus,
        // return with empty array for menus and currentPermissions
        if (count($permissions) === 0 || count($accessibleMenus) === 0) {
            return view("pages.permissions.features", [
                "menus" => [],
                "currentPermissions" => [],
                "departmentId" => $departmentId
            ]);
        }

        return view("pages.permissions.features", [
            "menus" => $accessibleMenus,
            "currentPermissions" => $currentPermissions,
            "departmentId" => $departmentId
        ]);
    }

    public function saveFeaturePermissions(Request $request)
    {
        $departmentId = $request->get("departmentId");
        $department = $this->departmentRepository->findWith($departmentId, ["roles"]);
        $departmentRole = $department->roles->first();

        $permissions = collect($request->get("permissions"))->flatten()->toArray();

        // Append access permissions into permission array
        // Get all access permissions
        $departmentCurrentPermissions = $department->getPermissionsViaRoles();
        $accessibleMenuNames = $departmentCurrentPermissions
            ->filter(function ($permission) {
                return substr($permission->name, 0, strlen("access")) === "access";
            })
            ->map(function ($permission) {
                return $permission->name;
            })
            ->toArray();

        // Sync permissions to role
        $departmentRole->syncPermissions(array_merge($permissions, $accessibleMenuNames));

        return redirect()->route("permissions.features", [
            "department" => $departmentId
        ]);
    }
}
