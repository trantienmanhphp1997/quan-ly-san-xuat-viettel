<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PlanInfoRequest;
use App\Imports\PlanImport;
use App\Utils\ExcelUtil;
use App\Repositories\DepartmentRepository;
use App\Repositories\PlanInfoRepository;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class PlanController extends Controller
{
    protected $modelDisplayName = 'plans';
    protected $modelClass = "App\Models\PlanInfo";
    protected $prefix = 'plans';
    protected $filterOptions = [];

    public $departmentRepository;

    public function __construct(
        PlanInfoRepository $planInfoRepository,
        DepartmentRepository $departmentRepository,
        ExcelUtil $excelUtil
    )
    {
        parent::__construct();
        $this->selfRepository = $planInfoRepository;
        $this->departmentRepository = $departmentRepository;
        $this->excelUtil = $excelUtil;
    }

    public function create(Request $request)
    {
        $editable = true;

        $is_active = $request->query("is_active", 1);
        $department = $this->departmentRepository->allQuery(["code" => $this->getDepartmentCode()])->where(["status" => 1])->get();
        $department_tree = [];
        foreach( $department as $prd ){
            $parentname = array_filter($department_tree,function($value) use($prd){
                if($value['id'] == $prd->parent_id){
                    return $value;
                }
            });
            if (count($parentname)>0) {
                foreach( $parentname as $item ) {
                    $name = $item['name'] . ' / ' . $prd->name;
                }
            } else {
                $name = $prd->name;
            }
            $department_tree[] = ['id'=>$prd->id, 'name'=>$name];
        };
        $department = $department_tree;
        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "editable" => $editable,
            "departments" => $department,
            "is_active" => $is_active
        ]);
    }

    public function show($id)
    {
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("view", $data);
        $editable=false;
        return view($this->viewNamespace .$this->prefix.'.detail', compact(
            'editable',
            'data'
        ));
    }

    public function edit($id)
    {
        $editable=true;
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("update", $data);
        $department = $this->departmentRepository->allQuery(["code" => $this->getDepartmentCode()])->where(["status" => 1])->get();
        $department_tree = [];
        foreach( $department as $prd ){
            $parentname = array_filter($department_tree,function($value) use($prd){
                if($value['id'] == $prd->parent_id){
                    return $value;
                }
            });
            if (count($parentname)>0) {
                foreach( $parentname as $item ) {
                    $name = $item['name'] . ' / ' . $prd->name;
                }
            } else {
                $name = $prd->name;
            }
            $department_tree[] = ['id'=>$prd->id, 'name'=>$name];
        };
        $department = $department_tree;
        return view($this->viewNamespace .$this->prefix.'.detail')->with([
            "data" => $data,
            "editable" => $editable,
            "departments" => $department
        ]);
    }

    public function store(PlanInfoRequest $request){
        $newData = $request->all();
        $newData["status"] = 1;
        $newData["plan_category_id"] = 1;

        try {
            $this->selfRepository->create($newData);
            session()->flash("success", __("notification.common.success.add"));
        }catch (\Exception $exception){
            session()->flash("error", __("notification.common.fail.add"));
        }

        return redirect()->route($this->prefix.".index");

    }

    public function update(PlanInfoRequest $request, $id){
        $data = $this->selfRepository->withTrashed()->find($id);
        $updatedData = $request->except(["code","link"]);

        try {
            $data->update($updatedData);
            session()->flash("success", __("notification.common.success.update"));
        }catch (\Exception $exception){
            session()->flash("error", __("notification.common.fail.update"));
        }
        return redirect($request->link);
    }

    public function import(){
        return view("$this->viewNamespace.$this->prefix.import");
    }

    public function importStore(Request $request)
    {
        if ($request->hasFile("plans")) {
            $file_path = $request->file("plans")->storeAs('plans', $request->file('plans')->getClientOriginalName() . '.' . $request->file('plans')->extension());
            try {
                $import = Excel::import(new PlanImport(), $file_path);
            } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                $failures = $e->failures();
                return view($this->viewNamespace . $this->prefix . ".import", with([
                    "failures" => $failures,
                ]));
            }
            session()->flash("success", __("notification.common.success.import")); //?
            return redirect()->route("$this->prefix.index");
        }
    }

    public function getPlansByDepartmentId(Request $request){
        $department_id = $request->department_id;
        $plans = $this->selfRepository->getDataByDepartmentId($department_id);
        return $plans;
    }

    public function export(Request $request){
        $allParams = $request->input();
        $plans = $this->selfRepository
            ->allQuery($allParams)->with('department','category')
            ->get();
        $index = 1;
        foreach($plans as $plan){
            $plan->{'no'} = $index++;
            $plan->{'department'} = $plan->department ? $plan->department->name : '';
//            $plan->{'category'} = $plan->category ? $plan->category->name : '';
            $plan->{'planyear'} = '';
        }
        $titles = [
            "no","department","title","code","description","planyear","year"
        ];

        $data = [];
        $data["data"] = $plans;
        $data["titles"] = $titles;
        $this->excelUtil->exportExcel($data, "DSKeHoach", "templates/plan-export.xlsx", 6);
    }

}
