<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\ValidateException;
use App\Http\Requests\RecordPlanImportRequest;
use App\Http\Requests\RecordPlanRequest;
use App\Http\Resources\RecordPlanEventResource;
use App\Imports\RecordPlanImport;
use App\Models\RecordPlan;
use App\Repositories\DepartmentRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\MemberRepository;
use App\Repositories\NoteRepository;
use App\Repositories\PlanInfoRepository;
use App\Repositories\RecordPlanMemberXrefRepository;
use App\Repositories\RecordPlanRepository;
use App\Repositories\StageRepository;
use App\Repositories\ExampleNoteRepository;
use App\Services\AssignmentService;
use App\Services\DeviceService;
use App\Services\Interfaces\IRecordPlanService;
use App\Services\MemberService;
use App\Utils\CommonUtil;
use App\Utils\ExcelUtil;
use App\View\Components\DateTime;
use App\ViewModel\PaginateModel;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class RecordPlanController extends Controller
{
    protected $modelDisplayName = 'record-plans';
    protected $modelClass = "App\Models\RecordPlan";
    protected $prefix = 'record-plans';
    public $memberRepository;
    public $recordPlanMemberXrefRepository;
    public $noteRepository;
    public $memberService;
    public $deviceService;
    public $deviceRepository;
    public $assignmentService;
    public $recordPlanService;
    public $departmentRepository;
    public $planRepository;
    public $stageRepository;
    public $exampleNoteRepository;
    public $excelUtil;
    public $messageBag;

    protected $filterOptions = [
        'is_active' => [
            'label' => 'Status',
            'type' => 'checkBox',
            'options' => [
                '1' => 'Active',
                '-1' => 'Inactive'
            ]
        ],
    ];

    public function __construct(
        RecordPlanRepository $recordPlanRepository,
        MemberRepository $memberRepository,
        RecordPlanMemberXrefRepository $recordPlanMemberXrefRepository,
        NoteRepository $noteRepository,
        MemberService $memberService,
        DeviceService $deviceService,
        DeviceRepository $deviceRepository,
        DepartmentRepository $departmentRepository,
        AssignmentService $assignmentService,
        IRecordPlanService $recordPlanService,
        PlanInfoRepository $planRepository,
        StageRepository $stageRepository,
        ExampleNoteRepository $exampleNoteRepository,
        ExcelUtil $excelUtil,
        MessageBag $messageBag
    )
    {
        parent::__construct();
        $this->middleware('auth');
        $this->middleware('admin');
        $this->selfRepository = $recordPlanRepository;
        $this->memberRepository = $memberRepository;
        $this->recordPlanMemberXrefRepository = $recordPlanMemberXrefRepository;
        $this->memberService = $memberService;
        $this->noteRepository = $noteRepository;
        $this->deviceService = $deviceService;
        $this->deviceRepository = $deviceRepository;
        $this->assignmentService = $assignmentService;
        $this->recordPlanService = $recordPlanService;
        $this->departmentRepository = $departmentRepository;
        $this->planRepository = $planRepository;
        $this->stageRepository = $stageRepository;
        $this->exampleNoteRepository = $exampleNoteRepository;
        $this->excelUtil = $excelUtil;
        $this->messageBag = $messageBag;
    }

    public function getRecordPlans(Request $request, PaginateModel $paginateModel = null){
        $allParams = $request->except('page');
        $allParams['from_date'] = !empty($request->from_date) ?  date('Y-m-d H:i:s', strtotime($request->from_date)) : null;
        $allParams['to_date'] = !empty($request->to_date) ? date('Y-m-d H:i:s', strtotime($request->to_date))  : null;

        $allParams["department_code"] = $this->getDepartmentCode();
        if(!$paginateModel){
            $data = $this->recordPlanService->searchRecordPlan($this->makeQueryParams($allParams), null, null);
        }else{
            $data = $this->recordPlanService->searchRecordPlan($this->makeQueryParams($allParams), null, new PaginateModel(10, $request["page"]));
        }
        return $data;
    }

    public function index(Request $request)
    {
        $allParams = $request->except('page');
        $allParams['from_date'] = !empty($request->from_date) ?  date('Y-m-d H:i:s', strtotime($request->from_date)) : null;
        $allParams['to_date'] = !empty($request->to_date) ? date('Y-m-d H:i:s', strtotime($request->to_date))  : null;
        $reporters = $this->memberService->getReportersForRecPlan($request);
//        $allParams["department_code"] = $this->getDepartmentCode();

        $data = new LengthAwarePaginator(collect([]), 0, 10);
        try {
            $data = $this->getRecordPlans($request, new PaginateModel(10, $request["page"]));
//            $data = $this->recordPlanService->searchRecordPlan($this->makeQueryParams($allParams), null, new PaginateModel(10, $request["page"]));
        } catch (\Exception $e) {
            Log::error("recordplan list", [$e]);
        }
        //thống kê số lượng
        // @todo dùng config
        $newRequest = new Request();
        $newRequest["status"] = 1;
        $num_of_pendings = count($this->getRecordPlans($newRequest));
        $newRequest["status"] = 2;
        $num_of_approved = count($this->getRecordPlans($newRequest));
        $newRequest["status"] = 3;
        $num_of_in_progress = count($this->getRecordPlans($newRequest));
        $newRequest["status"] = 4;
        $num_of_wrap = count($this->getRecordPlans($newRequest));

        $departments = $this->departmentRepository->allQuery(["code" => $this->getDepartmentCode()])->where(["status" => config("common.status.plan.active")])->get();
        $stages = $this->stageRepository->allQuery()->where(["status" => 1])->get();
        $exampleNotes = $this->exampleNoteRepository->allQuery()->get();
        $search = (object)['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        return view($this->viewNamespace . $this->prefix . '.list', ([
            "is_active" => $request->is_active ?? 1,
            "data" => $data,
            "search" => $search,
            "allParams" => $allParams,
            "num_of_pendings" => $num_of_pendings,
            "num_of_approved" => $num_of_approved,
            "num_of_in_progress" => $num_of_in_progress,
            "num_of_wrap" => $num_of_wrap,
            "departments"=> $departments,
            "reporters" => $reporters,
            "stages" => $stages,
            "exampleNotes" => $exampleNotes
        ]));
    }

    public function create(Request $request)
    {
        $editable = true;
        $routePrefix = $this->prefix;
        $reporters = $this->memberRepository->allQuery(["department_code" => "reporter"])->get();
        $departments = $this->departmentRepository->allQuery(["code" => $this->getDepartmentCode()])->get();

        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "routePrefix" => $routePrefix,
            "editable" => $editable,
            "departments" => $departments,
        ]);
    }

    public function showRecordPlan($id, Request $request)
    {
        $data = $this->selfRepository->find($id);
        $this->authorize("view", $data);
        $modelDisplayName = $this->modelDisplayName;
        $routePrefix = $this->prefix;
        $editable = true;
        $departments = $this->departmentRepository->allQuery(["code" => $this->getDepartmentCode()])->where(["status" => config("common.status.plan.active")])->get();
        $assigned_reporter = $this->memberService->getAssignedMembers('reporter', $id);
        $assigned_camera = $this->memberService->getAssignedMembers('camera', $id);
        $assigned_transportation = $this->memberService->getAssignedMembers('transportation', $id, ['record_plan_member_xref' => function ($query) use ($id) {
            $query->where(['record_plan_id' => $id]);
        }]);
        $assigned_devices = $this->deviceService->getAssignedDevices($id);
        $assigned_technical_camera = $this->memberService->getAssignedMembers('technical_camera', $id);

        $reporter_note = $this->noteRepository->findNoteByRecordPlanIdAndDepartmentCode($id, "reporter");
        $camera_note = $this->noteRepository->findNoteByRecordPlanIdAndDepartmentCode($id, config("common.departments.camera"));
        $transportation_note = $this->noteRepository->findNoteByRecordPlanIdAndDepartmentCode($id, config("common.departments.transportation"));
        $device_note = $this->noteRepository->findNoteByRecordPlanIdAndDepartmentCode($id, config("common.departments.device"));
        $stages = $this->stageRepository->allQuery()->where(["status" => 1])->get();
        $exampleNotes = $this->exampleNoteRepository->allQuery()->get();

        //show_record_plan

        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "modelDisplayName" => $modelDisplayName,
            "routePrefix" => $routePrefix,
            "editable" => $editable,
            "data" => $data,
            "departments" => $departments,
            "assigned_reporter"=>$assigned_reporter,
            "assigned_camera"=>$assigned_camera,
            "assigned_transportation"=>$assigned_transportation,
            "assigned_technical_camera" => $assigned_technical_camera,
            "assigned_devices"=>$assigned_devices,
            "reporter_note" => $reporter_note ? $reporter_note->content : "",
            "camera_note" => $camera_note  ? $camera_note->content : "",
            "device_note" => $device_note  ? $device_note->content : "",
            "transportation_note" => $transportation_note  ? $transportation_note->content : "",
            "stages" => $stages,
            "exampleNotes" => $exampleNotes
        ]);
    }

    public function store(RecordPlanRequest $request)
    {
        $this->authorize("create", RecordPlan::class);
        $newData = $request->except(["reporter_allocate_status", "reporters", "devices"]);
        if (empty($newData['address']) && empty($newData['address2'])) {
            session()->flash("create_error", __("server_validation.record_plan.address.required"));
            return redirect()->back()->withInput();
        }

        // lịch lặp lại
        $newData['is_recurring'] = +isset($newData['is_recurring']);
        $newData['on_monday'] = +isset($newData['on_monday']);
        $newData['on_tuesday'] = +isset($newData['on_tuesday']);
        $newData['on_wednesday'] = +isset($newData['on_wednesday']);
        $newData['on_thursday'] = +isset($newData['on_thursday']);
        $newData['on_friday'] = +isset($newData['on_friday']);
        $newData['on_saturday'] = +isset($newData['on_saturday']);
        $newData['on_sunday'] = +isset($newData['on_sunday']);

        $start_time_timestamp = strtotime($request->start_time);
        $start_time = date('Y-m-d H:i:s', $start_time_timestamp);
        $newData["start_time"] = $start_time;
        $newData["created_by"] = auth()->user()->id;

        $end_time_timestamp = strtotime($request->end_time);
        $end_time = date('Y-m-d H:i:s', $end_time_timestamp);
        $newData["end_time"] = $end_time;
        if ($newData['is_recurring'] == 1 && ($newData['on_monday'] == 0 && $newData['on_tuesday'] == 0 && $newData['on_wednesday'] == 0 && $newData['on_thursday'] == 0
                && $newData['on_friday'] == 0 && $newData['on_saturday'] == 0 && $newData['on_sunday'] == 0)) {
            session()->flash("create_error", __("server_validation.record_plan.recurring.required"));
            return redirect()->back()->withInput();
        }

        if ($request->address_type_check == 3) {
            //kiểm tra thời gian lịch trong phòng quay
            if (date('Y-m-d', strtotime($request->start_time)) != date('Y-m-d', strtotime($request->end_time))) {
                session()->flash("create_error", "Lịch quay trong trường quay chỉ diễn ra trong ngày, vui lòng chọn thời gian hợp lệ");
                return redirect()->back()->withInput();
            }

            // kiểm tra lịch sản xuất đã được đặt lịch ở phòng quay
            $checkRecordPlanInSatge = $this->recordPlanService->checkRecordPlanExistsInStage($newData['address2'], $newData['start_time']);
            if ($checkRecordPlanInSatge["status"] === "FOUND") {
                session()->flash("create_error", "Trường quay ".$checkRecordPlanInSatge["stage"]." đã được lịch ". $checkRecordPlanInSatge["record_plan"]." đăng ký tại khung giờ trên");
                return redirect()->back()->withInput();
            }

            $newData["address_type"] = config("common.address_type.useStage");
            $newData['address'] = $newData['address2'];
            unset($newData['address2']);
        }

        $newData["is_allday"] = array_key_exists('is_allday', $newData);
        $newData["reporter_allocate_status"] = 1;
        $newData["camera_allocate_status"] = 1;
        $newData["device_allocate_status"] = 1;
        $newData["transportation_allocate_status"] = 1;
        $newData["technical_camera_allocate_status"] = 1;
        $newData["status"] = config("common.status.record_plan.pending"); /*array_key_exists("status", $request->all()) ? 1 : 1*/

        if($request->device_used == 1) {
            $newData["offer_device_number"] = 1;
        }

        if (!$request["offer_reporter_number"]) {
            $newData["reporter_allocate_status"] = 2;
        }

        if (!$request["offer_camera_number"]) {
            $newData["camera_allocate_status"] = 2;
        }

        if (!$request["offer_transportation_number"]) {
            $newData["transportation_allocate_status"] = 2;
        }

        if (!$request["offer_device_number"]) {
            $newData["device_allocate_status"] = 2;
        }

        if (!$request["offer_technical_camera_number"]) {
            $newData["technical_camera_allocate_status"] = 2;
        }


        $newData["plan_id"] = $request->plan_id;
        $department = $this->departmentRepository->find($request->department_id);
        $newData["department_code"] = $department->code;

        // dd($newData);
        $record_plan = $this->selfRepository->create($newData);
        $this->recordPlanService->checkRecordPlanGeneralStatus($record_plan->id);

        $rs = $this->assignmentService->assignMembersToRecordPlan($record_plan->id, "reporter", $request->reporter_ids);

        session()->flash("success", __("notification.common.success.add"));

        return redirect()->route($this->prefix . ".show", ["id" => $record_plan->id])->withInput();

    }

    public function editRecordPlan($id, Request $request)
    {
        $record_plan = $this->selfRepository->find($id);
        $this->authorize("update", $record_plan);

        $departments = $this->departmentRepository->allQuery(["code" => $this->getDepartmentCode()])->where(["status" => config("common.status.plan.active")])->get();
        $assigned_reporter = $this->memberService->getAssignedMembers('reporter', $id);
        $assigned_camera = $this->memberService->getAssignedMembers('camera', $id);
        $assigned_transportation = $this->memberService->getAssignedMembers('transportation', $id);

        // edit_record_plan

        $routePrefix = $this->prefix;
        $data = $this->selfRepository->find($id);
        $editable = true;
        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "routePrefix" => $routePrefix,
            "editable" => $editable,
            "data" => $data,
            "departments" => $departments,
            "assigned_reporter"=>$assigned_reporter,
            "assigned_camera"=>$assigned_camera,
            "assigned_transportation"=>$assigned_transportation
        ]);
    }

    public function update(RecordPlanRequest $request, $id, MessageBag $messageBag)
    {
        $updateData = $request->except("reporters");

        // lịch lặp lại
        $updateData['is_recurring'] = +isset($updateData['is_recurring']);
        $updateData['on_monday'] = +isset($updateData['on_monday']);
        $updateData['on_tuesday'] = +isset($updateData['on_tuesday']);
        $updateData['on_wednesday'] = +isset($updateData['on_wednesday']);
        $updateData['on_thursday'] = +isset($updateData['on_thursday']);
        $updateData['on_friday'] = +isset($updateData['on_friday']);
        $updateData['on_saturday'] = +isset($updateData['on_saturday']);
        $updateData['on_sunday'] = +isset($updateData['on_sunday']);

        if ($updateData['is_recurring'] == 1 && ($updateData['on_monday'] == 0 && $updateData['on_tuesday'] == 0 && $updateData['on_wednesday'] == 0 && $updateData['on_thursday'] == 0
                && $updateData['on_friday'] == 0 && $updateData['on_saturday'] == 0 && $updateData['on_sunday'] == 0)) {
            session()->flash("error", __("server_validation.record_plan.recurring.required"));
            session()->flash("field", __($updateData['field']));
            return redirect()->back()->withInput();
        }

        if ($updateData['address_type'] == config("common.address_type.useStage")) {
            // kiểm tra lịch sản xuất đã được đặt lịch ở phòng quay
            $checkRecordPlanInSatge = $this->recordPlanService->checkRecordPlanExistsInStage($updateData['address'], $updateData['start_time'], $id);
            if ($checkRecordPlanInSatge["status"] === "FOUND") {
                session()->flash("error", "Trường quay ".$checkRecordPlanInSatge["stage"]." đã được lịch ". $checkRecordPlanInSatge["record_plan"]." đăng ký tại khung giờ trên");
                return redirect()->back()->withInput();
            }
        }

        if ($updateData['is_recurring'] == 0) {
            $updateData['on_monday'] = $updateData['on_tuesday'] = $updateData['on_wednesday'] = $updateData['on_thursday'] = $updateData['on_friday'] = $updateData['on_saturday'] = $updateData['on_sunday'] = 0;
        }

        unset($updateData['field']);

        $record_plan = $this->selfRepository->find($id);

        // update_data_record_plan

        $updateData["is_allday"] = array_key_exists('is_allday', $updateData);
        $updateData["plan_id"] = $request->plan_id;
        $department = $this->departmentRepository->find($request->department_id);
        $updateData["department_code"] = $department->code;

        // convert time format H:i:s d-m-Y to format Y-m-d H:i:s theo format datetime của MySql quy định
        $start_time_timestamp = strtotime($request->start_time);
        $start_time = date('Y-m-d H:i:s', $start_time_timestamp);
        $updateData["start_time"] = $start_time;

        $end_time_timestamp = strtotime($request->end_time);
        $end_time = date('Y-m-d H:i:s', $end_time_timestamp);
        $updateData["end_time"] = $end_time;

        $this->authorize("update", $record_plan);
        $record_plan->update($updateData);

        $this->recordPlanService->checkRecordPlanGeneralStatus($id);

        session()->flash("success", __("notification.common.success.update"));

        return redirect()->back();
    }

    public function import(Request $request)
    {
        return view($this->viewNamespace . $this->prefix . '.import', [
            "routePrefix" => $this->prefix,
        ]);
    }

    public function importStore(RecordPlanImportRequest $request)
    {
        if ($request->hasFile("record-plans")) {
            $file_path = $request->file("record-plans")->storeAs('record-plans', $request->file('record-plans')->getClientOriginalName() . '.' . $request->file('record-plans')->extension());
            try {
                $import = Excel::import(new RecordPlanImport(), $file_path);
            } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
//                if ($e instanceof \Exception) {
//                    session()->flash("error", __("notification.common.fail.import"));
//                    return view($this->viewNamespace . $this->prefix . ".import", with([
//                        "department_code" => $department_code
//                    ]));
//                }
                $failures = $e->failures();
                return view($this->viewNamespace . $this->prefix . ".import", with([
                    "failures" => $failures,
                ]));
            }
            session()->flash("success", __("notification.common.success.import"));
            return redirect()->route("$this->prefix.index");
        }
    }

    public function export(Request $request)
    {
        $result = [];

        $data = $this->getRecordPlans($request);
        $data = $this->recordPlanService->addXrefsInfoToRecordPlans($data);

        // export_record_plan

        $rs = $this->recordPlanService->prepareRecordPlansForExport($data);
        $this->excelUtil->exportExcel($rs["result"], "DSLichSanXuat", "templates/" . $rs["fileName"], 5);
        return true;
    }

    public function getEvents(Request $request)
    {
        $start = $request->input('start');
        $end = $request->input('end');
        $record_plans = RecordPlan::where(function ($q) use ($start, $end) {
            $q->where('start_time', '>=', $start)
                ->where('start_time', '<=', $end);
        })
            ->orwhere(function ($q) use ($start, $end) {
                $q->where('end_time', '>=', $start)
                    ->where('end_time', '<=', $end);
            })->get();
        return RecordPlanEventResource::collection($record_plans)->all();
    }

    /**
     * return calendar view as dashboard
     * param Request $request
     * return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCalendar(Request $request)
    {
        $data = RecordPlan::all();
        return view('layouts.calendar', compact('data'));
    }

    public function updateGeneralStatus($id, Request $request)
    {
        try {
            $this->recordPlanService->updateGeneralStatus($id, $request);
            session()->flash("success", __("notification.update_status_record_plan.$request->status.success"));
        } catch (\Exception $exception) {
            if($exception instanceof ValidateException){
                session()->flash("error", $exception->getMessage());
            }
            Log::error($exception);
            session()->flash("error", __("notification.update_status_record_plan.$request->status.fail"));
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Init array of IDs
        $ids = [];
        if (empty($id) || $id == -1) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        try {
            $this->recordPlanService->destroy($ids);
            $data = [
                'message' => __('common.message.delete_success', ['modelName' => $this->modelDisplayName]) . " {$this->modelDisplayName}",
                'alert-type' => 'success',
            ];

            session()->flash("success", __("notification.common.success.delete"));

            $url = CommonUtil::backPage($request->total, $request->currentPage, $request->perPage, $request->lastPage, $ids, $this->prefix, true);
            return redirect($url)->with($data);

        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            if ($exception instanceof ValidateException) {
                session()->flash("error", $exception->getMessage());
            } else{
                session()->flash("error", __("notification.common.fail.delete"));
            }
            return redirect()->back();
        }
    }

    public function updateOfferMember($id, Request $request){
        try {
            $recordPlan = $this->selfRepository->find($id);

            $offers = $request->all();
            $data = [];
            foreach ($offers as $key => $items ){
                foreach ($items as $field => $value){
                    $data["offer_$key"."_$field"] = $value;
                }
            }
            $recordPlan->update($data);

            foreach ($offers as $key => $items ){
                $this->assignmentService->checkAllocateStatus($key, $id);
            }

            session()->flash("success", "Cập nhật đề xuất thành công");
            return true;
        }catch (\Exception $exception){
            Log::info($exception);
            session()->flash("error", "Cập nhật đề xuất thất bại");
            return false;
        }
    }

    public function remindInComingRecordPlans()
    {
        return true;
        return $this->recordPlanService->remindInComingRecordPlans();
    }

    public function remindWrapRecordPlans()
    {
        return true;
        return $this->recordPlanService->remindWrapRecordPlans();
    }

    public function autoStartReportPlan(){
        return $this->recordPlanService->autoStartReportPlan();
    }

    public function autoWrapReportPlan(){
        return $this->recordPlanService->autoWrapReportPlan();
    }

    public function autoCreateRecordPlanRecurring() {
        Log::info("autoCreateRecordPlanRecurring");
        return $this->recordPlanService->autoCreateRecordPlanRecurring();
    }
}
