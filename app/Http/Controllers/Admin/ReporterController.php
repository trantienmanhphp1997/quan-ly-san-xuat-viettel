<?php


namespace App\Http\Controllers\Admin;


class ReporterController extends MemberController
{
    protected $modelDisplayName = 'reporter';
    protected $prefix = 'reporter';
    protected $departmentCode = 'production';

}
