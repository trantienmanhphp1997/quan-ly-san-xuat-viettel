<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CarRequest;
use App\Http\Requests\StageRequest;
use App\Repositories\StageRepository;
use App\Utils\ExcelUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StageController extends Controller
{
    protected $modelDisplayName = 'stages';
    protected $modelClass = "App\Models\Stage";
    protected $prefix = 'stages';
    protected $filterOptions = [];
    private $excelUtil;

    public function __construct(
        StageRepository $stageRepository,
        ExcelUtil $excelUtil
    )
    {
        parent::__construct();
        $this->selfRepository = $stageRepository;
        $this->excelUtil = $excelUtil;
    }

    public function create(Request $request)
    {
        $editable = true;

        $is_active = $request->query("is_active", 1);

        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "editable" => $editable,
            "is_active" => $is_active
        ]);
    }

    public function show($id)
    {
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("view", $data);
        $editable = false;
        $hideCode = true;
        return view($this->viewNamespace . $this->prefix . '.detail', compact(
            'editable',
            'data',
            "hideCode"
        ));
    }

    public function edit($id)
    {
        $editable = true;
        $data = $this->selfRepository->withTrashed()->find($id);
        $hideCode = true;

        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "data" => $data,
            "editable" => $editable,
            "hideCode" => $hideCode
        ]);
    }

    public function store(StageRequest $request)
    {
        $newData = $request->all();
        $newData["status"] = array_key_exists("status", $request->all()) ? config("common.status.stage.active") : config("common.status.stage.inactive");

        try {
            $stage = $this->selfRepository->create($newData);

            session()->flash("success", __("notification.common.success.add"));
        } catch (\Exception $exception) {
            Log::info($exception);
            session()->flash("error", __("notification.common.fail.add"));
        }

        return redirect()->route($this->prefix . ".index");
    }

    public function update(StageRequest $request, $id)
    {
        $car = $this->selfRepository->withTrashed()->find($id);
        $updatedData = $request->all();
        $updatedData["status"] = array_key_exists("status", $request->all()) ? config("common.status.stage.active") : config("common.status.stage.inactive");

        try {
            $car->update($updatedData);
            session()->flash("success", __("notification.common.success.update"));
        } catch (\Exception $exception) {
            Log::info($exception);
            session()->flash("error", __("notification.common.fail.update"));
        }
        return redirect()->back();
    }
}
