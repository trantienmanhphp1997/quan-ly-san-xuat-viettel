<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StatementRequest;
use App\Imports\PlanImport;
use App\Imports\StatementImport;
use App\Models\Statement;
use App\Utils\ExcelUtil;
use App\Repositories\DepartmentRepository;
use App\Repositories\StatementRepository;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class StatementController extends Controller
{
    protected $modelDisplayName = 'statements';
    protected $modelClass = Statement::class;
    protected $prefix = 'statements';
    protected $filterOptions = [];

    protected $departmentRepository;


    public function __construct(
        StatementRepository $statementRepository,
        DepartmentRepository $departmentRepository,
        ExcelUtil $excelUtil
    )
    {
        parent::__construct();
        $this->selfRepository = $statementRepository;
        $this->departmentRepository = $departmentRepository;
        $this->excelUtil = $excelUtil;
    }

    public function create(Request $request)
    {
        $editable = true;

        $is_active = $request->query("is_active", 1);

        $departments = $this->departmentRepository->allQuery(["code" => $this->getDepartmentCode()])->where(["status" => 1])->get();

        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "editable" => $editable,
            "departments" => $departments,
            "is_active" => $is_active
        ]);
    }

    public function show($id)
    {
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("view", $data);
        $editable = false;
        return view($this->viewNamespace . $this->prefix . '.detail', compact(
            'editable',
            'data'
        ));
    }

    public function edit($id)
    {
        $editable = true;
        $data = $this->selfRepository->withTrashed()->find($id);
        $this->authorize("update", $data);

        $departments = $this->departmentRepository->allQuery(["code" => $this->getDepartmentCode()])->where(["status" => 1])->get();


        return view($this->viewNamespace . $this->prefix . '.detail')->with([
            "data" => $data,
            "editable" => $editable,
            "departments" => $departments
        ]);
    }

    public function store(StatementRequest $request)
    {
        $newData = $request->all();
        $newData["status"] = 1;

        try {
            $this->selfRepository->create($newData);
            session()->flash("success", __("notification.common.success.add"));
        } catch (\Exception $exception) {
            session()->flash("error", __("notification.common.fail.add"));
        }

        return redirect()->route($this->prefix . ".index");

    }

    public function update(StatementRequest $request, $id)
    {
        $car = $this->selfRepository->withTrashed()->find($id);
        $updatedData = $request->except(["code","link"]);

        try {
            $car->update($updatedData);
            session()->flash("success", __("notification.common.success.update"));
        } catch (\Exception $exception) {
            session()->flash("error", __("notification.common.fail.update"));
        }
        return redirect($request->link);
    }
    public function getStatementsByDepartmentId(Request $request){
        $department_id = $request->department_id;
        return $this->selfRepository->getDataByDepartmentId($department_id);
    }

    public function import(){
        return view("$this->viewNamespace.$this->prefix.import");
    }

    public function importStore(Request $request)
    {
        if ($request->hasFile("statements")) {
            $file_path = $request->file("statements")->storeAs('statements', $request->file('statements')->getClientOriginalName() . '.' . $request->file('statements')->extension());
            try {
                $import = Excel::import(new StatementImport(), $file_path);
            } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                $failures = $e->failures();
                return view($this->viewNamespace . $this->prefix . ".import", with([
                    "failures" => $failures,
                ]));
            }
            session()->flash("success", __("notification.common.success.import")); //?
            return redirect()->route("$this->prefix.index");
        }
    }

    public function export(Request $request){
        $allParams = $request->input();
        $statements = $this->selfRepository
            ->allQuery($allParams)->with('department')
            ->get();
        $index = 1;
        foreach($statements as $statement){
            $statement->{'no'} = $index++;
            $statement->{'codeold'} = '';
            $statement->{'department'} = $statement->department ? $statement->department->name : '';
        }
        $titles = [
            "no","code", "title","description","codeold","department"
        ];

        $data = [];
        $data["data"] = $statements;
        $data["titles"] = $titles;
        $this->excelUtil->exportExcel($data, "DSTotrinh", "templates/statement-export.xlsx", 6);
    }

}
