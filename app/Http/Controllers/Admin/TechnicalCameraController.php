<?php


namespace App\Http\Controllers\Admin;


class TechnicalCameraController extends MemberController
{
    protected $modelDisplayName = 'technical_camera';
    protected $prefix = 'technical_camera';
    protected $departmentCode = 'technical_camera';

}
