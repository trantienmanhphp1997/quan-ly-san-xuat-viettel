<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Admin\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $maxAttempts = 5;
    protected $decayMinutes = 30;

    public function username()
    {
        return 'username';
    }

    protected function throttleKey(Request $request)
    {
        return Str::lower($request->ip());
    }

    protected function validateLogin(Request $request)
    {

        $this->validate(
            $request,
            [
                $this->username() => 'required',
                'password' => 'required', //|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])
            ],
            [
                'username.required' => 'Tên đăng nhập là bắt buộc',
                'password.required' => 'Mật khẩu là bắt buộc',
                'password.min' => 'Mật khẩu chứa tối thiểu 8 ký tự',
                'password.regex' => 'Mật khẩu cần chứa tối thiểu 8 ký tự và ít nhất 1 chữ in hoa',
            ]
        );

    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
