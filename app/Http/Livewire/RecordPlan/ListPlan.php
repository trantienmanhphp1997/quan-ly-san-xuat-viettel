<?php

namespace App\Http\Livewire\RecordPlan;
use App\Http\Livewire\Base\BaseLive;
use App\Exceptions\ValidateException;
use App\Http\Requests\RecordPlanImportRequest;
use App\Http\Requests\RecordPlanRequest;
use App\Http\Resources\RecordPlanEventResource;
use App\Imports\RecordPlanImport;
use App\Models\RecordPlan;
use App\Repositories\DepartmentRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\MemberRepository;
use App\Repositories\NoteRepository;
use App\Repositories\PlanInfoRepository;
use App\Repositories\RecordPlanMemberXrefRepository;
use App\Repositories\RecordPlanRepository;
use App\Repositories\StageRepository;
use App\Repositories\ExampleNoteRepository;
use App\Services\AssignmentService;
use App\Services\DeviceService;
use App\Services\Interfaces\IRecordPlanService;
use App\Services\MemberService;
use App\Utils\CommonUtil;
use App\Utils\ExcelUtil;
use App\View\Components\DateTime;
use App\ViewModel\PaginateModel;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class ListPlan extends BaseLive{
    public $routePrefix = 'record-plans';
    public $modelDisplayName = 'record-plans';
    protected $recordPlanService;
    protected $memberService;
    protected $departmentRepository;
    protected $exampleNoteRepository;
    protected $stageRepository;
    public $searchTerm;
    public $searchStatus;
    public $searchRecurring;
    public $fromDate;
    public $toDate;
    public function mount(
        // MemberService $memberService,
        // DepartmentRepository $departmentRepository,
        // ExampleNoteRepository $exampleNoteRepository,
        // StageRepository $stageRepository
    ){
        // $this->memberService  = $memberService;
        // $this->departmentRepository  = $departmentRepository;
        // $this->exampleNoteRepository  = $exampleNoteRepository;
        // $this->stageRepository  = $stageRepository;
    }
    public function render(){
        $data = [];
        $newRequest = new Request();
        $newRequest["status"] = 1;
        $num_of_pendings = count($this->getRecordPlans($newRequest));
        $newRequest["status"] = 2;
        $num_of_approved = count($this->getRecordPlans($newRequest));
        $newRequest["status"] = 3;
        $num_of_in_progress = count($this->getRecordPlans($newRequest));
        $newRequest["status"] = 4;
        $num_of_wrap = count($this->getRecordPlans($newRequest));

        // $reporters = $this->memberService->getReportersForRecPlan($newRequest);
        // $departments = $this->departmentRepository->allQuery(["code" => $this->getDepartmentCode()])->where(["status" => config("common.status.plan.active")])->get();
        // $exampleNotes = $this->exampleNoteRepository->allQuery()->get();
        // $stages = $this->stageRepository->allQuery()->where(["status" => 1])->get();
        try {
            $newRequest["status"] = $this->searchStatus;
            $data = $this->getRecordPlans($newRequest, new PaginateModel(10));
        } catch (\Exception $e) {
            Log::error("recordplan list", [$e]);
        }
        return view('livewire.recordPlan.list-plan',compact(
            'data',
            'num_of_pendings',
            'num_of_approved',
            'num_of_in_progress',
            'num_of_wrap',
            // 'reporters',
            // 'departments',
            // 'exampleNotes',
            // 'stages'
        ));
    }
    public function getRecordPlans($request, PaginateModel $paginateModel = null){
        $allParams = $request->except('page');
        if(!$paginateModel){
            $data = $this->searchRecordPlan($allParams, null, null);
        }else{
            $data = $this->searchRecordPlan($allParams, null, new PaginateModel(10));
        }
        return $data;
    }
    function searchRecordPlan($query, $isActive, PaginateModel $paginateModel = null, $creator = null)
    {
        // dd($query, $isActive);

        if ($paginateModel) {
            $paginateModel = new PaginateModel();
        }

        $data = $this->makeQuery($query);
        if ($isActive == -1) {
            $data = $data->onlyTrashed()->orderBy("created_at", "desc");
        } else {
            $data = $data->orderBy("start_time", "desc")
                ->orderByRaw("
                CASE WHEN DATEDIFF(start_time, CURRENT_TIME()) = 0 THEN DATEDIFF(start_time, CURRENT_TIME()) END DESC,
                CASE WHEN DATEDIFF(start_time, CURRENT_TIME()) > 0 THEN 1 END DESC,
                CASE WHEN DATEDIFF(start_time, CURRENT_TIME()) > 0 THEN DATEDIFF(start_time, CURRENT_TIME()) END ASC,
                DATEDIFF(start_time, CURRENT_TIME()) DESC,
                CASE
                    WHEN  status = ? THEN 1
                    WHEN  status = ? THEN 2
                    WHEN  status = ? THEN 3
                    WHEN  status = ? THEN 4
                    WHEN  status = ? THEN 5
                ELSE 6 END",
                [
                    config('common.status.record_plan.pending'),
                    config('common.status.record_plan.approved'),
                    config('common.status.record_plan.in_progress'),
                    config('common.status.record_plan.wrap'),
                    config('common.status.record_plan.done'),
                ]);
        }
        if ($paginateModel) {
            return $paginateModel->toPaging($data);
        }
        return PaginateModel::All()->toPaging($data)->items() ?? [];
    }
    public function makeQuery($query){
        $data = RecordPlan::query();
        if($query['status']){
            $data = $data->where('status',$query['status']);
        }
        $department_code = $this->getDepartmentCode();
        if($department_code){
            $data = $data->where('department_code','Like','%'.$department_code.'%');
        }
        // dd('vào');
        if($this->searchTerm){
            $data = $data->where(function($query) {
                $query->Where('name','LIKE','%'.$this->searchTerm.'%');
                $query->orWhere('address','LIKE','%'.$this->searchTerm.'%');
                $query->orWhere('address_type','LIKE','%'.$this->searchTerm.'%');
            });
        }
        if($this->searchRecurring){
            $data = $data->where('is_recurring',$this->searchRecurring);
        }
        //Lọc theo thời gian
        if($this->fromDate){
            $data = $data->where('end_time','>=', reFormatDate($this->fromDate,'Y-m-d H:i:s'));
            // dd($data);
        }
        if($this->toDate){
            $data = $data->where('start_time','<=', reFormatDate($this->toDate,'Y-m-d H:i:s'));
        }
        return $data;
        // //lọc theo người tạo lịch
        // if (!empty($creator)) {
        //     $query['created_by'] = $creator;
        // }
    }
    public function setSearchStatus($index){
        $this->searchStatus = $index;
    }
}
