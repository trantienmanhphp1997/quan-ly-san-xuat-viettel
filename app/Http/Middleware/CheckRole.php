<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{

    public function handle($request, Closure $next, $departmentCode)
    {
        if ($request->user()->hasRole('super admin') || ($request->user()->is_manager == 1 && str_contains($request->user()->department_code, $departmentCode))) {
            return $next($request);
        }
        abort(404);
    }
}
