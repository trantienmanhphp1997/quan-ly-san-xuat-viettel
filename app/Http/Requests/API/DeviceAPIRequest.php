<?php

namespace App\Http\Requests\API;

use App\ResponseHandler\HandleResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Services\DeviceService;

class DeviceAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|max:191",
            "category" => "required",
            "serial" => "required|unique:devices,serial,".$this->id,
        ];
    }

    public function messages()
    {
        return [
            "name.required" => __("server_validation.device.name.required"),
            "category.required" => __("server_validation.device.category.required"),
            "serial.required" => __("server_validation.device.serial.required"),
            "serial.unique" => __("server_validation.device.serial.unique"),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            HandleResponse::handle(
                "CREATE_OR_UPDATE_FAILED",
                422,
                $validator->errors(),
                null,
                '/api/devices/update-or-create'));
    }
}
