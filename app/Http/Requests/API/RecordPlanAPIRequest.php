<?php

namespace App\Http\Requests\API;

use App\ResponseHandler\HandleResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RecordPlanAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            "name" => "required|max:191|unique:record_plans,name," . $this->route('id'),
            "name" => "required|max:191",
            "start_time" => "required",
            "end_time" => "required",
            "address" => "required"
        ];
    }

    public function messages()
    {
        return [
            "name.required" => __("server_validation.record_plan.name.required"),
            "name.unique" => __("server_validation.record_plan.name.unique"),
            "name.max" => __("server_validation.record_plan.name.max"),
            "start_time" => __("server_validation.record_plan.start_time.required"),
            "end_time" => __("server_validation.record_plan.end_time.required"),
            "address.required" => __("server_validation.record_plan.address.required"),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            HandleResponse::handle(
                config('api_message.record_plan.create_failed'),
                422,
                $validator->errors(),
                '',
                '/api/record-plans/store'));
    }
}
