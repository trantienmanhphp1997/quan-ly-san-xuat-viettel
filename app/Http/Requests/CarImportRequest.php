<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "cars" => "required|mimes:xlsx,xls"
        ];
    }

    public function messages()
    {
        return [
            "cars.required" => __("server_validation.file.required"),
            "cars.mimes" => __("server_validation.file.mimes"),

        ];
    }
}
