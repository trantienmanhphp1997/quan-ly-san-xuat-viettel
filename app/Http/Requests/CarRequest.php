<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // "name" => ["required", "max:191" ],
            "model" => ["required", "max:191" ],
            "number_of_seats" => ["required", "max:191" ],
            "license_plate" => ["required", "max:191" ,"unique:cars,license_plate,{$this->id}"],
//            "driver_id" => ["required" ],
        ];
    }

    public function messages()
    {
        return [
            // "name.required" => __("server_validation.car.name.required"),
            // "name.max" => __("server_validation.car.name.maxlength"),
            "model.required" => __("server_validation.car.model.required"),
            "model.max" => __("server_validation.car.model.maxlength"),
            "number_of_seats.required" => __("server_validation.car.number_of_seats.required"),
            "number_of_seats.max" => __("server_validation.car.number_of_seats.maxlength"),
            "license_plate.required" => __("server_validation.car.license_plate.required"),
            "license_plate.max" => __("server_validation.car.license_plate.maxlength"),
            "license_plate.unique" => __("server_validation.car.license_plate.unique"),
//            "driver_id.required" => __("server_validation.car.driver_id.required"),
        ];
    }

}
