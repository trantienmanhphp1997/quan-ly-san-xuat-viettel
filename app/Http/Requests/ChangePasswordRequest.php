<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => 'required',
            'new_password' => 'required|regex:/^(?=.*?[A-Z]).{8,}$/',
            'confirm_password' => 'same:new_password',
        ];
    }

    public function messages()
    {
        return [
            "current_password.required" => 'Mật khẩu hiện tại là bắt buộc',
            "current_password.regex" => 'required',
            "new_password.required" => 'Mật khẩu mới là bắt buộc',
            "new_password.regex" =>'Mật khẩu mới cần chứa tối thiểu 8 ký tự và ít nhất 1 chữ in hoa',
            "confirm_password.same" => "Mật khẩu nhập lại chưa đúng"
        ];
    }

}
