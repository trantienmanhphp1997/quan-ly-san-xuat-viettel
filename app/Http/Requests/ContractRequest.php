<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|max:191",
            "code" => "required|max:191|unique:contracts,code,".$this->id,
            "department_id" => "required",
            "statement_id" => "required",
        ];
    }

    public function messages()
    {
        return [
            "title.required" => __("server_validation.contract.title.required"),
            "title.max" => __("server_validation.contract.title.maxlength"),
            "code.required" => __("server_validation.contract.code.required"),
            "code.max" => __("server_validation.contract.code.maxlength"),
            "code.unique" => __("server_validation.contract.code.unique"),
            "department_id.required" => __("server_validation.contract.department_id.required"),
            "statement_id.required" => __("server_validation.contract.statement_id.required"),
        ];
    }
}
