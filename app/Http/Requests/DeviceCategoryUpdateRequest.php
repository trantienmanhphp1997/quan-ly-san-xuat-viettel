<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeviceCategoryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => ["required", "max:191","unique:device_categories,name,".$this->id],
            "display_code" => ["required","unique:device_categories,display_code,".$this->id],
        ];
    }

    public function messages()
    {
        return [
            "name.required" => __("server_validation.device_category.name.required"),
            "name.max" => __("server_validation.device_category.name.maxlength"),
            "name.unique" => __("server_validation.device_category.name.unique"),
            "display_code.required" => __("server_validation.device_category.display_code.required"),
            "display_code.unique" => __("server_validation.device_category.display_code.unique"),
        ];
    }
}
