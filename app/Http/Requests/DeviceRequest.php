<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeviceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => ["required", "max:191" ],
            "device_category_id" => ["required"],
            "serial" => ["required","unique:devices,serial,".$this->id],
        ];
    }

    public function messages()
    {
        return [
            "name.required" => __("server_validation.device.name.required"),
            "name.max" => __("server_validation.device.name.maxlength"),
            "device_category_id.required" => __("server_validation.device.device_category_id.required"),
            "serial.required" => __("server_validation.device.serial.required"),
            "serial.unique" => __("server_validation.device.serial.unique"),
        ];
    }
}
