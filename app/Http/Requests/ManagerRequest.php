<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ManagerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->phone_number = str_replace("+84","0",$this->phone_number);
        return [
            "username" => "required",
//            "password" => "required",
            "phone_number" => "required",
            "full_name" => "required",
            "department_code" => "required",
            "email" => "nullable|email"

        ];
    }

    public function messages()
    {
        return [
            "username.required" => __("server_validation.manager.username.required"),
            "username.unique" => __("server_validation.manager.username.unique"),
//            "password.required" => __("server_validation.manager.password.required"),
            "phone_number.required" => __("server_validation.manager.phone_number.required"),
            // "phone_number.unique" => __("server_validation.manager.phone_number.unique"),
            "full_name.required" => __("server_validation.manager.full_name.required"),
            "department_code.required" => __("server_validation.manager.department_code.required"),
            "email.email" =>  __("server_validation.email.format"),
        ];
    }

    //xử lý input trước khi thực hiện validate
    protected function getValidatorInstance()
    {
        $this->phone_number = str_replace("+84","0",$this->phone_number);
        $this->merge([
            'phone_number' => $this->phone_number
        ]);

        return parent::getValidatorInstance();
    }
}
