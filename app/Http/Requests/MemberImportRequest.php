<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "members" => "required|mimes:xlsx,xls"
        ];
    }

    public function messages()
    {
        return [
            "members.required" => __("server_validation.file.required"),
            "members.mimes" => __("server_validation.file.mimes"),

        ];
    }
}
