<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Member;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "full_name" => "required",
            "phone_number" => "required",
            "email" => "nullable|email"
        ];
    }
    public function messages()
    {
        // $departmentCode = Member::where('phone_number',$this->phone_number)->pluck('department_code')->first();
        return [
            "full_name.required" => __("server_validation.member.full_name.required"),
            "phone_number.required" => __("server_validation.member.phone_number.required"),
            // "phone_number.unique" => __("server_validation.member.phone_number.unique",['department'=> __("department.".$departmentCode)]),
            "department_code.required" => __("server_validation.member.department_code.required"),
            "email.email" => __("server_validation.email.format"),
        ];
    }

    //xử lý input trước khi thực hiện validate
    protected function getValidatorInstance()
    {
        $this->phone_number = str_replace("+84","0",$this->phone_number);
        $this->merge([
            'phone_number' => $this->phone_number
        ]);

        return parent::getValidatorInstance();
    }

}
