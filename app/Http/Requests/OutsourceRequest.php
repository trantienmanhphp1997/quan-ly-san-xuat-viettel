<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OutsourceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "outsource_type_id" => "required",
            "full_name" => "required|max:191",
            "phone_number" => "required|unique:outsources,phone_number,".$this->id,
            "email" => "nullable|email"
        ];
    }

    public function messages()
    {
        return [
            "outsource_type_id.required" => __("server_validation.outsource.outsource_type_id.required"),
            "full_name.required" => __("server_validation.outsource.full_name.required"),
            "full_name.max" => __("server_validation.outsource.full_name.maxlength"),
            "phone_number.required" => __("server_validation.outsource.phone_number.required"),
            "phone_number.unique" => __("server_validation.outsource.phone_number.unique"),
            "email.email" => __("server_validation.email.format"),
        ];
    }

     //xử lý input trước khi thực hiện validate
     protected function getValidatorInstance()
     {
         $this->phone_number = str_replace("+84","0",$this->phone_number);
         $this->merge([
             'phone_number' => $this->phone_number
         ]);

         return parent::getValidatorInstance();
     }
}
