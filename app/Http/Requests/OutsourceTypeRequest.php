<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OutsourceTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => ["required", "max:191"],
            "code" => "required|unique:outsource_types,code,".$this->id,
        ];
    }

    public function messages()
    {
        return [
            "name.required" => __("server_validation.outsource_type.name.required"),
            "name.max" => __("server_validation.outsource_type.name.maxlength"),
            "code.required" => __("server_validation.outsource_type.code.required"),
            "code.unique" => __("server_validation.outsource_type.code.unique"),
        ];
    }
}
