<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlanInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|max:191",
            "code" => "required|max:191|unique:plan_info,code,".$this->id,
            "department_id" => "required",
            "year" => "required"
        ];
    }

    public function messages()
    {
        return [
            "title.required" => __("server_validation.plan_info.title.required"),
            "title.max" => __("server_validation.plan_info.title.maxlength"),
            "code.required" => __("server_validation.plan_info.code.required"),
            "code.max" => __("server_validation.plan_info.code.maxlength"),
            "code.unique" => __("server_validation.plan_info.code.unique"),
            "department_id.required" => __("server_validation.plan_info.department_id.required"),
            "year.required" => __("server_validation.plan_info.department_id.required"),
        ];
    }
}
