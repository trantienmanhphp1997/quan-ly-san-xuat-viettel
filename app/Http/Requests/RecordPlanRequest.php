<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecordPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|max:191",
//            "description" => "required",
            "start_time" => "required",
            "end_time" => "required",
//            "address" => "required",
//            "plan_id" => "required"
        ];
    }

    public function messages()
    {
        return [
            "name.required" => __("server_validation.record_plan.name.required"),
//            "description.required" => __("server_validation.record_plan.description.required"),
            "name.max" => __("server_validation.record_plan.name.max"),
            "name.unique" => __("server_validation.record_plan.name.unique"),
            "start_time" => __("server_validation.record_plan.start_time.required"),
            "end_time" => __("server_validation.record_plan.end_time.required"),
//            "address.required" => __("server_validation.record_plan.address.required"),
            "plan_id.required" => __("server_validation.record_plan.plan_id.required"),
        ];
    }
}
