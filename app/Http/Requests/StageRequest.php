<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // "name" => ["required", "max:191" ],
            "name" => ["required", "max:191" ],
            "code" => ["required", "max:191" , "unique:cinematic_stages,code,{$this->id}"],
            "address" => ["required", "max:191"],
//            "driver_id" => ["required" ],
        ];
    }

    public function messages()
    {
        return [
            "name.required" => __("server_validation.stage.name.required"),
            "name.max" => __("server_validation.stage.name.max"),
            "code.required" => __("server_validation.stage.code.required"),
            "code.max" => __("server_validation.stage.code.max"),
            "code.unique" => __("server_validation.stage.code.unique"),
            "address.required" => __("server_validation.stage.address.required"),
            "address.max" => __("server_validation.stage.address.max"),
        ];
    }

}
