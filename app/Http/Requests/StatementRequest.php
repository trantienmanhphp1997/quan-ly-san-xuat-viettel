<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StatementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|max:191",
            "code" => "required|max:191|unique:statements,code,".$this->id,
            "department_id" => "required",
        ];
    }

   public function messages()
   {
       return [
           "title.required" => __("server_validation.statement.title.required"),
           "title.max" => __("server_validation.statement.title.maxlength"),
           "code.required" => __("server_validation.statement.code.required"),
           "code.max" => __("server_validation.statement.code.maxlength"),
           "code.unique" => __("server_validation.statement.code.unique"),
           "department_id.required" => __("server_validation.statement.department_id.required"),
       ];
   }
}
