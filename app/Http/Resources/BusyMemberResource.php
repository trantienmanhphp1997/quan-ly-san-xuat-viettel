<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BusyMemberResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->member->id,
            "username" => $this->member->username,
            "uuid" => $this->member->uuid,
            "is_manager" => $this->member->is_manager,
            "full_name" => $this->member->full_name,
            "phone_number" => $this->member->phone_number,
            "other_phone_number" => $this->member->other_phone_number,
            "department_code" => $this->member->department_code,
            "address" => $this->member->address,
            "status" => $this->member->status,
            "record_plan" => $this->record_plan,

        ];
    }
}
