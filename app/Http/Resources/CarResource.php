<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "driver_id" => $this->driver_id,
            "name" => $this->name,
            "model" => $this->model,
            "brand" => $this->brand,
            "color" => $this->color,
            "number_of_seats" => $this->number_of_seats,
            "license_plate" => $this->license_plate,
            "status" => $this->status,
            "description" => $this->description,
            "note" => $this->note,
            "transportation" => $this->transportation,
        ];
    }
}
