<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DeviceBorrowingResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->device->id,
            "device_category_id" => $this->device->device_category_id,
            "device_category_code" => $this->device->device_category_code,
            "name" => $this->device->name,
            "serial" => $this->device->serial,
            "status" => $this->device->status,
            "description" => $this->device->description,
            "quantity" => $this->device->quantity,
            "note" => $this->device->note,
            "record_plan" => $this->record_plan,
            "status_record_plan_device_xref" => $this->status_record_plan_device_xref,
        ];
    }
}
