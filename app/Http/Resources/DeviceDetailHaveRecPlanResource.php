<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DeviceDetailHaveRecPlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "category_id" => $this->device_category_id,
            "category_name" => $this->category->name,
            "serial" => $this->serial,
            "description" => $this->description,
            "quantity" => $this->quantity,
            "status" => $this->status,
            "record_plan" => $this->record_plan
        ];
    }
}
