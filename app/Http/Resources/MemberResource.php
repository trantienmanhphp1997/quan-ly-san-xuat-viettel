<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MemberResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "username" => $this->username,
            "full_name" => $this->full_name,
            "phone_number" => $this->phone_number,
            "other_phone_number" => $this->other_phone_number,
            "address" => $this->address,
            "email" => $this->email,
            "department_code" => $this->department_code,
            "uuid" => $this->uuid,
            "is_manager" => $this->is_manager,
            "status" => $this->status,
        ];
    }
}
