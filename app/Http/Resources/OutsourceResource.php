<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OutsourceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "outsource_type_id" => $this->outsource_type_id,
            "outsource_type_name" => $this->outsource_type->name,
            "name" => $this->name,
            "description" => $this->description,
            "full_name" => $this->full_name,
            "phone_number" => $this->phone_number,
            "email" => $this->email,
            "address" => $this->address,
            "status" => $this->status,
            "note" => $this->note,
            "created_by" => $this->created_by,
        ];
    }
}
