<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PlanInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "code" => $this->code,
            "year" => $this->year,
            "title" => $this->title,
            "description" => $this->description,
            "department_id" => $this->department_id,
            "plan_category_id" => $this->plan_category_id,
            "status" => $this->status,
        ];
    }
}
