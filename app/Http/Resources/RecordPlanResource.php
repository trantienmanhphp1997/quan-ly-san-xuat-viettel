<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RecordPlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
            "start_time" => $this->start_time,
            "end_time" => $this->end_time,
            "address" => $this->address,
            "actual_address" => $this->actual_address,
            "plan_id" => $this->plan_id,
            "department_code" => $this->department_code,
            "reporter_allocate_status" => $this->reporter_allocate_status,
            "cameraman_allocate_status" => $this->cameraman_allocate_status,
            "transportation_allocate_status" => $this->transportation_allocate_status,
            "device_allocate_status" => $this->device_allocate_status,
            "offer_reporter_note" => $this->offer_reporter_note,
            "offer_camera_note" => $this->offer_camera_note,
            "offer_transportation_note" => $this->offer_transportation_note,
            "offer_device_note" => $this->offer_device_note,
            "offer_device_number" => $this->offer_device_number,
            "offer_camera_number" => $this->offer_camera_number,
            "offer_reporter_number" => $this->offer_reporter_number,
            "offer_transportation_number" => $this->offer_transportation_number,
            "created_by" => $this->created_by,
            "notes" => $this->notes,
            "status" => $this->status,
            "address_type" => $this->address_type,
            "is_allday" => $this->is_allday,
            "is_recurring" => $this->is_recurring,
            "on_monday" => $this->on_monday,
            "on_tuesday" => $this->on_tuesday,
            "on_wednesday" => $this->on_wednesday,
            "on_thursday" => $this->on_thursday,
            "on_friday" => $this->on_friday,
            "on_saturday" => $this->on_saturday,
            "on_sunday" => $this->on_sunday,
            "offer_technical_camera_number" => $this->offer_technical_camera_number,
            "offer_technical_camera_note" => $this->offer_technical_camera_note,
            "can_update_record_plan" => $this->can_update_record_plan,
            "transportation_status" => $this->transportation_status ,
            "members" => MemberResource::collection($this->members),
            "devices" => DeviceResource::collection($this->devices)
        ];
    }
}
