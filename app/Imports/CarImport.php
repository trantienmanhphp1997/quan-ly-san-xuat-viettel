<?php


namespace App\Imports;

use App\Models\Member;
use App\Models\Car;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithLimit;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class CarImport implements ToCollection, WithValidation, WithStartRow,SkipsEmptyRows
{
    use Importable;

    public $mappingHeader = [
        "brand" => 1,
        "model" => 2,
        "color" => 3,
        // "driver" => 4,
        "number_of_seats" => 4,
        "license_plate" => 5,
        "note" => 6
    ];

    public function __construct()
    {

    }

    public function collection(Collection $rows)
    {
        $data = collect($rows)->map(function ($el){
            return $this->collectData($el);
        });

        $colUpdate = ["brand", "model","color","number_of_seats", "status","note"];

        Car::upsert($data->toArray(), ['license_plate'], $colUpdate);
    }

    public function collectData($row)
    {
        $data = [];
        $keys = array_keys($this->mappingHeader);
        foreach($keys as $key){
            $data[$key] = $row[$this->mappingHeader[$key]];
            $data["status"] = 1;
        }
        return $data;

    }

    public function withValidator($validator) {
        $validator->after(function($validator) {
            $data = $validator->getData();
            if(count($data) == 0) { 
                $validator->errors()->add('-1','Import thất bại, File rỗng');  
                }
        });
    }


    public function rules(): array
    {
        $roles = [
            '*.1' => "required",
            '*.2' => "required",
            '*.3' => "required",
            '*.4' => "required|numeric",
            '*.5' => "required",
        ];

        return $roles;
    }

    public function customValidationMessages()
    {
        $result = [
            '*.1.required' => __("server_validation.car.brand.required"),
            '*.2.required' => __("server_validation.car.model.required"),
            '*.3.required' =>__("server_validation.car.color.required"),
            '*.4.required' => __("server_validation.car.number_of_seats.required"),
            '*.5.required' => __("server_validation.car.license_plate.required"),
            '*.4.numeric' =>  __("server_validation.car.number_of_seats.numberic"),
        ];

        return $result;
    }

    public function headingRow(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 6;
    }

}
