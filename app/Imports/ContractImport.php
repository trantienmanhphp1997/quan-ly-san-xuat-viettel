<?php


namespace App\Imports;

use App\Models\Contract;
use App\Models\Member;
use App\Models\Car;
use App\Repositories\StatementRepository;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithLimit;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ContractImport implements ToCollection, WithValidation, WithStartRow,SkipsEmptyRows
{
    use Importable;

    public $mappingHeader = [
        "statement_code" => 1,
        "contract_code" => 3,
        "title" => 4,
    ];

    public function __construct()
    {

    }

    public function collection(Collection $rows)
    {
        $data = collect($rows)->map(function ($el){
            return $this->collectData($el);
        });

        $colUpdate = ["title", "code", "statement_id", "partner_id", "department_id"];

        Contract::upsert($data->toArray(), ['code'], $colUpdate);
    }

    public function collectData($row)
    {
        $data = [];
        $keys = array_keys($this->mappingHeader);
        foreach($keys as $key){
            $data["title"] = $row[$this->mappingHeader["title"]];
            $data["code"] = $row[$this->mappingHeader["contract_code"]];
        }
        $statementRepo = app(StatementRepository::class);
        $statement = $statementRepo->allQuery()->where(["code" => $row[$this->mappingHeader["statement_code"]]])->first();
        $data["statement_id"] = $statement->id;
        $data["department_id"] = optional($statement->department)->id;
        $data["partner_id"] = 1;
        return $data;

    }

    public function withValidator($validator) {
        $validator->after(function($validator) {
            $data = $validator->getData();
            if(count($data) == 0) { 
                $validator->errors()->add('-1','Import thất bại, File rỗng');  
                }
        });
    }


    public function rules(): array
    {
        $roles = [
            '*.1' => 'required',
            '*.1' => function ($attribute, $value, $onFailure) {
                $statementRepo = app(StatementRepository::class);
                $value = preg_replace('/\s+/', '', $value);
                if(!empty($value)){
                    $checkStatementCode = $statementRepo->allQuery()->where(["code" => $value])->exists();
                    if (!$checkStatementCode) {
                        $onFailure('Tờ trình không tồn tại');
                    }
                }
            },
        ];

        return $roles;
    }

    public function customValidationMessages()
    {
        $result = [

        ];

        return $result;
    }

    public function headingRow(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 6;
    }

}
