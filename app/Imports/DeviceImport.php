<?php
namespace App\Imports;

use App\Models\Member;
use App\Models\Device;
use App\Models\DeviceCategory;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithLimit;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Facades\Log;

class DeviceImport implements ToCollection, WithValidation, WithStartRow,SkipsEmptyRows
{
    use Importable;

    public $mappingHeader = [
        "device_category_code" => 1,
        "name" => 2,
        "serial" => 3,
        "description" => 4,
    ];

    public function __construct()
    {

    }

    public function collection(Collection $rows)
    {
        Log::info("chạy collection");
        $data = collect($rows)->map(function ($el){
            return $this->collectData($el);
        });

        $colUpdate = ["device_category_code", "name","description"];

        Device::upsert($data->toArray(), ['serial'], $colUpdate);
    }

    public function collectData($row)
    {
        $data = [
            "device_category_code" => $row[$this->mappingHeader["device_category_code"]],
            "name" => $row[$this->mappingHeader["name"]],
            "serial" => $row[$this->mappingHeader["serial"]],
            "description" => $row[$this->mappingHeader["description"]],
        ];
        $data["device_category_code"] = preg_replace('/\s+/', '', $data["device_category_code"]);
        $data["serial"] = preg_replace('/\s+/', '', $data["serial"]);
        $deletedDivice = Device::where(['serial'=>$data["serial"]])->withTrashed()->first();
        if ($deletedDivice) {
            $deletedDivice->restore();
        }
        $data["device_category_id"] = DeviceCategory::where(['display_code'=>$data["device_category_code"]])->pluck("id")->first();
        $data["status"] = config("common.status.device.active");
        $data["quantity"] = 1;
        return $data;

    }

    public function withValidator($validator) {
        $validator->after(function($validator) {
            $data = $validator->getData();
            if(count($data) == 0) { 
                $validator->errors()->add('-1','Import thất bại, File rỗng');  
                }
        });
    }


    public function rules(): array
    {
        Log::info("chạy rules");
        $roles = [
            '*.1' => function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if(!empty($value)){
                    $checkCategoryCode = DeviceCategory::where(['display_code'=>$value])->exists();
                    if (!$checkCategoryCode) {
                        $onFailure('Mã danh mục không tồn tại');
                    }
                }
                else {
                    $onFailure('Mã danh mục không được để trống');
                }
            },
            '*.2' => "required",
            '*.3' => function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if(!empty($value)){
                    $checkCategoryCode = Device::where(['serial'=>$value])->exists();
                    if ($checkCategoryCode) {
                        $onFailure('Số serial đã tồn tại');
                    }
                }
                else {
                    $onFailure('Số serial không được để trống');
                }
            }
        ];

        return $roles;
    }

    public function customValidationMessages()
    {
        $result = [
            '*.2.required' => "Tên thiết bị không được để trống",
        ];

        return $result;
    }

    public function headingRow(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 6;
    }

}
