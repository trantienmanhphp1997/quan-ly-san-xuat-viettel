<?php


namespace App\Imports;

use App\Models\Car;
use App\Models\Member;
use App\Repositories\CarRepository;
use App\Repositories\MemberRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class MemberImport implements ToCollection, WithValidation, WithStartRow,SkipsEmptyRows
{
    private $isDriver = false;

    use Importable;

    public $department_code;
    public $mappingHeader = [
        "full_name" => 1,
        "phone_number" => 2,
        // "other_phone_number" => 3,
        "email" => 3,
        "address" => 4,
        "note" => 5,
        "license_plate" => 6
    ];

    public function __construct($department_code, $isDriver)
    {
        $this->department_code = $department_code;
        $this->isDriver = config('common.departments.transportation') == $this->department_code;
        if ($this->isDriver) {
            $mappingHeader['license_plate'] = 6;
        }
        $this->isDriver = $isDriver;
    }

    public function collection(Collection $rows)
    {
        $memberRepository = app(MemberRepository::class);
        $result = collect($rows)->filter(function ($el) {
            return !blank($el[$this->mappingHeader["phone_number"]]);
        })->map(function ($el){
            return $this->collectData($el);
        });
        $data = $result->map(function ($item){
            return $item["data"];
        });

        $colUpdate = ['full_name', 'email','address','note'];
        Member::upsert($data->toArray(), ['phone_number'], $colUpdate);

        //restore deleted members
        $phone_numbers = array_column($data->toArray(), "phone_number");
        $member_ids = Member::withTrashed()->where(["department_code" => $this->department_code, "status" => 1])->whereIn("phone_number", $phone_numbers)->pluck("id")->toArray();
        $memberRepository->restoreMany($member_ids);

        //map car
        if ($this->isDriver) {
            $data_driver = $result->map(function ($item){
                return $item["data_driver"];
            });
            $members = Member::whereIn('phone_number', $data_driver->map(function ($el){
                return $el["phone_number"];
            }))->get();
            $mapLicensePlate = [];
            foreach ($members as $member) {
                $dataSave = $data_driver->first(function ($el) use ($member){
                    return $el['phone_number'] === $member['phone_number'];
                });
                if ($dataSave) {
                    $mapLicensePlate[$dataSave['license_plate']] = $member['id'];
                }

            }
            $licence_plates = $data_driver->map(function ($el){
                return $el['license_plate'];
            })->toArray();
            $licence_plates = array_filter($licence_plates);
            $cars = Car::whereIn('license_plate', $licence_plates)->get()->toArray();
            $updated_cars = [];
            foreach ($cars as $car) {
                $memberId = $mapLicensePlate[$car['license_plate']];
                if ($memberId) {
                    $car['driver_id'] = $memberId;
                    $car["created_at"] = new \DateTime($car['created_at'], new \DateTimeZone('Asia/Ho_Chi_Minh'));
                    $car["updated_at"] = new \DateTime($car['updated_at'], new \DateTimeZone('Asia/Ho_Chi_Minh'));
                }
                $updated_cars[] = $car;
            }
            Car::upsert($updated_cars, "license_plate", ["driver_id"]);
        }
    }

    public function collectData($row)
    {
        $data = [
            "full_name" => $row[$this->mappingHeader["full_name"]],
            "phone_number" => $row[$this->mappingHeader["phone_number"]],
            "email" => $row[$this->mappingHeader["email"]],
            "address" => $row[$this->mappingHeader["address"]],
            "note" => $row[$this->mappingHeader["note"]],
            // "other_phone_number" => $row[$this->mappingHeader["other_phone_number"]],
            "department_code" => $this->department_code
        ];

        // $data['phone_number'] = str_replace(" ", "", $data['phone_number']);
        $data['phone_number']  = preg_replace('/\s+/', '', $data['phone_number'] );
        if(substr($data['phone_number'] ,0,1) != 0){
            $data['phone_number']  = str_replace("+84","",$data['phone_number'] );
            $data['phone_number']  = "0".$data['phone_number'] ;
        }
        // $data['other_phone_number'] = str_replace(" ", "", $data['other_phone_number']);
        $data_driver = $data;
        if ($this->isDriver) {
            $data_driver['license_plate'] = $row[$this->mappingHeader["license_plate"]];
//            $data_driver['license_plate'] = str_replace(" ", "", $data_driver['license_plate']);
            $data_driver['license_plate'] = trim( $data_driver['license_plate']);
        }

        // dd ($data);
        return [
            "data" => $data,
            "data_driver" => $data_driver
        ];

    }

    public function withValidator($validator) {
        $validator->after(function($validator) {
            $data = $validator->getData();
            if(count($data) == 0) { 
                $validator->errors()->add('-1','Import thất bại, File rỗng');  
                }
        });
    }


    public function rules(): array
    {
        $roles = [
            '*.1' => "required_with:*.2",
            '2' =>  function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if(substr($value,0,1) != 0){
                    $value = str_replace("+84","",$value);
                    $value = "0".$value;
                }
                if(!empty($value)){
                    $check = preg_match('/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/', $value);
                    if (!$check && strlen($value) != 10 ) {
                        $onFailure('Số điện thoại không đúng định dạng');
                    }else{
                        /*
                            Kiểm tra số điện thoại có đang giữ vai trò mannager hay không?
                            - get user dựa vào số điện thoại
                            - Kiểm tra is_manager == 1 ? thông báo lỗi : pass
                        */
                        $existedUser = Member::withTrashed()->where(['phone_number' => $value])->first();
                        // dd($existedUser);
                        if (!empty($existedUser ) && $existedUser->is_manager == 1) {
                            $onFailure("Số điện thoại này đang giữ vai trò quản lý phòng ban ". __("department.".$existedUser->department_code));
                        }
                    }
                }
            }
        ];
        if ($this->isDriver) {
            $roles["*.6"] = function($attribute, $value, $onFailure){
                if($value && $value!=''){
                    $value = trim($value);
                    $car = Car::where(['license_plate' => $value])->first();
                    if($car){
                        $car = $car->toArray();
                        if($car['driver_id']){
                            $onFailure(__("server_validation.car.license_plate.has_driver"));
                        }
                    }else{
                        $onFailure(__("server_validation.car.license_plate.not_exist"));
                    }
                }
            };
        }

        return $roles;
    }

    public function customValidationMessages()
    {
        $result = [
            '*.1.required_with' => __("server_validation.member.full_name.required"),
        ];

//        if ($this->isDriver) {
//            $result["*.4.required"] = 'license_plate is required';
//        }
        return $result;
    }

    public function headingRow(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 6;
    }
//    public function onFailure(Failure ...$failures)
//    {
//        return $failures;
//    }


}
