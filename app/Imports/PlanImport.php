<?php


namespace App\Imports;

use App\Models\Department;
use App\Models\PlanInfo;
use App\Models\PlanCategory;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithLimit;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class PlanImport implements ToCollection, WithValidation, WithStartRow,WithMultipleSheets,SkipsEmptyRows
{
    use Importable;

    public $mappingHeader = [
        "department_id" => 1,
//        "plan_category_id" => 2,
        "title" => 2,
        "code" => 3,
        "description" => 4,
        "year" => 6,
        // "number_of_seats" => 5
    ];

    public function __construct()
    {

    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }

    public function collection(Collection $rows)
    {

        $data = [];
        foreach ($rows as $row){
            $data[] = $this->collectData($row);
        }

        $colUpdate = ["title", "description","year"];

        PlanInfo::upsert($data, ['code'], $colUpdate);
    }

    public function collectData($row)
    {
        $data = [];
        $keys = array_keys($this->mappingHeader);
        foreach($keys as $key){
            $data[$key] = $row[$this->mappingHeader[$key]];
        }
        $data["department_id"] = Department::where(["code" => $data["department_id"]])->pluck('id')->first();
        $data["plan_category_id"] = 1;

        return $data;

    }

    public function withValidator($validator) {
        $validator->after(function($validator) {
            $data = $validator->getData();
            if(count($data) == 0) { 
                $validator->errors()->add('-1','Import thất bại, File rỗng');  
                }
        });
    }


    public function rules(): array
    {
        $roles = [
           '*.1' => "required",
//            '*.2' => "required",
           '*.2' => "required",
           '*.3' => "required",
           '*.6' => "required",
           '*.1' => function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if(!empty($value)){
                    $checkDepartmentCode = Department::where(['code'=>$value])->exists();
                    if (!$checkDepartmentCode) {
                        $onFailure('Mã phòng ban không tồn tại');
                    }
                }
                else {
                    $onFailure('Mã phòng ban không được để trống');
                }
            },
        ];

        return $roles;
    }

    public function customValidationMessages()
    {
        $result = [

        ];

        return $result;
    }

    public function headingRow(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 6;
    }


}
