<?php


namespace App\Imports;

use Illuminate\Support\Facades\Auth;
use App\Repositories\DepartmentRepository;
use App\Repositories\PlanInfoRepository;
use App\Repositories\RecordPlanMemberXrefRepository;
use App\Repositories\RecordPlanRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Events\BeforeSheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class RecordPlanImport implements ToCollection, WithValidation, WithStartRow,SkipsEmptyRows
{

    use Importable;
    public $recordPlanRepository;
    public $planInfoRepository;
    public $departmentRepository;
    public $recordPlanMemberXrefRepository;

    public $mappingHeader = [
        "department_code" => 1,
        "plan_code" => 2,
        "name" => 3,
//        "offer_reporter_number" => 4,
        "offer_reporter_note" => 4,
        "offer_technical_camera_number" => 5,
        "offer_technical_camera_note" => 6,
        "offer_camera_number" => 7,
        "offer_camera_note" => 8,
        "start_date" => 9,
        "start_time" => 10,
        "end_date" => 11,
        "end_time" => 12,
        // "is_allDay" => 13,
//        "recurring_frequency" => 11,
        "address" => 13,
        "offer_transportation_number" => 14,
        "offer_transportation_note" => 15,
        "offer_device_number" => 16,
        "offer_device_note" => 17,
        "description" => 18,
        "note" => 19,
    ];

    public function __construct()
    {
        $this->recordPlanRepository = app(RecordPlanRepository::class);
        $this->planInfoRepository = app(PlanInfoRepository::class);
        $this->departmentRepository = app(DepartmentRepository::class);
        $this->recordPlanMemberXrefRepository = app(RecordPlanMemberXrefRepository::class);
    }

    public function collection(Collection $rows)
    {
        $result = [];
        $excelImportedTime = time();
        foreach ($rows as $row){
            $data = $this->collectData($row);
            if(!blank($data)){
                $data["excel_imported"] = $excelImportedTime;
                $result[] = $data;
            }
        }

        $rs = $this->recordPlanRepository->insert($result);
        if($rs){
            $importedRecPlans = $this->recordPlanRepository->allQuery()->where(["excel_imported" => $excelImportedTime])->get();
            $assignedReporter = [];
            foreach ($importedRecPlans as $importedRecPlan){
                $assignedReporter[] = [
                    "record_plan_id" => $importedRecPlan->id,
                    "member_id" => auth()->user()->id,
                    "start_time" => $importedRecPlan->start_time,
                    "end_time" => $importedRecPlan->end_time,
                    "status" => config("common.status.assignments.members.accept"),
                    "record_plan_status" => $importedRecPlan->status
                ];
            }
            $this->recordPlanMemberXrefRepository->insert($assignedReporter);

        }
    }

    public function collectData($row)
    {
        $data = [];
//        start_time
        $start_time = $row[$this->mappingHeader['start_date']] + $row[$this->mappingHeader['start_time']];
        $start_time = Date::excelToDateTimeObject($start_time);
//        end_time
        $end_time = $row[$this->mappingHeader['end_date']] + $row[$this->mappingHeader['end_time']];
        $end_time = Date::excelToDateTimeObject($end_time);

        //handle plan code
        $plan_code = $row[$this->mappingHeader["plan_code"]];
        if($plan_code){
            $plan = $this->planInfoRepository->allQuery()->where(["code" => $plan_code])->first();
            $data["plan_id"] = $plan->id;
        }

        $data["name"] = $row[$this->mappingHeader["name"]];
        $data["department_code"] = $row[$this->mappingHeader["department_code"]];
        $data["address"] = $row[$this->mappingHeader["address"]];
//        $data["offer_reporter_number"] = $row[$this->mappingHeader["offer_reporter_number"]];
        $data["offer_reporter_note"] = $row[$this->mappingHeader["offer_reporter_note"]];
        $data["offer_technical_camera_number"] = $row[$this->mappingHeader["offer_technical_camera_number"]];
        $data["offer_technical_camera_note"] = $row[$this->mappingHeader["offer_technical_camera_note"]];
        $data["offer_camera_number"] = $row[$this->mappingHeader["offer_camera_number"]];
        $data["offer_camera_note"] = $row[$this->mappingHeader["offer_camera_note"]];
        $data["offer_device_number"] = $row[$this->mappingHeader["offer_device_number"]];
        $data["offer_device_note"] = $row[$this->mappingHeader["offer_device_note"]];
        $data["offer_transportation_number"] = $row[$this->mappingHeader["offer_transportation_number"]];
        $data["offer_transportation_note"] = $row[$this->mappingHeader["offer_transportation_note"]];
        $data["notes"] = $row[$this->mappingHeader["note"]];
        $data["description"] = $row[$this->mappingHeader["description"]];

        //add other info
        $data["reporter_allocate_status"] = 1;
        $data["technical_camera_allocate_status"] = 1;
        $data["camera_allocate_status"] = 1;
        $data["technical_camera_allocate_status"] = 1;
        $data["transportation_allocate_status"] = 1;
        $data["device_allocate_status"] = 1;
        $data["status"]  = 1;

        $data["created_at"] = now();
        $data["created_by"] = auth()->user()->id;

        $data["start_time"] = $start_time;
        $data["end_time"] = $end_time;

//        if(!$data["offer_reporter_number"]){
//            $data["reporter_allocate_status"] = config("common.status.propose.approved");
//        }
        if(!$data["offer_technical_camera_number"]){
            $data["technical_camera_allocate_status"] = config("common.status.propose.approved");
        }
        if(!$data["offer_camera_number"]){
            $data["camera_allocate_status"] = config("common.status.propose.approved");
        }
        if(!$data["offer_technical_camera_number"]){
            $data["technical_camera_allocate_status"] = config("common.status.propose.approved");
        }
        if(!$data["offer_transportation_number"]){
            $data["transportation_allocate_status"] = config("common.status.propose.approved");
        }

        if(!$data["offer_device_number"]){
            $data["device_allocate_status"] = config("common.status.propose.approved");
        }
        if($data["reporter_allocate_status"] == 2 &&  $data["camera_allocate_status"] == 2 && $data["transportation_allocate_status"] == 2 && $data["device_allocate_status"] == 2){
            $data["status"]  = 2;
        }

        return $data;
    }

    public function withValidator($validator) {
        $validator->after(function($validator) {
            $data = $validator->getData();
            if(count($data) == 0) { 
                $validator->errors()->add('-1','Import thất bại, File rỗng');  
                }
        });
    }


    public function rules(): array
    {
        $rules = [
            '*.1' => "required_with:*.3",
            '1' =>  function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if($value != ''){
                    $checkDepartmentCode = Auth::user()->department_code;
                    if($value!==$checkDepartmentCode)
                    {
                        $onFailure('Không thể import lịch sản xuất cho phòng ban khác');
                    }
                }
            },
            '2' =>  function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);

                if($value != ''){
                    $checkPlanCode = $this->planInfoRepository->allQuery()->where(["code" => $value])->exists();
                    if(!$checkPlanCode)
                    {
                        $onFailure('Mã kế hoạch không tồn tại ');
                    }
                }
            },
            '*.3' => "required",
            '5' => function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if((!empty($value) && !is_numeric($value)) || (is_numeric($value) && $value <= 0) ){
                    $onFailure('Số lượng không đúng định dạng');
                }
            },
            '7' => function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if((!empty($value) && !is_numeric($value)) || (is_numeric($value) && $value <= 0) ){
                    $onFailure('Số lượng không đúng định dạng');
                }
            },
            '14'  => function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if((!empty($value) && !is_numeric($value)) || (is_numeric($value) && $value <= 0) ){
                    $onFailure('Số lượng không đúng định dạng');
                }
            },
            '16' => function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if((!empty($value) && !is_numeric($value)) || (is_numeric($value) && $value <= 0) ){
                    $onFailure('Số lượng không đúng định dạng');
                }
            },
            '*.9' => "required_with:*.3",
            '9' =>  function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if(!is_numeric($value)){
                    $onFailure('Nhập định dạng ngày/tháng/năm chưa đúng');
                }
            },
            '*.10' => "required_with:*.3",
            '10' =>  function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if(!is_numeric($value)){
                    $onFailure('Nhập định dạng giờ/phút chưa đúng');
                }
            },
            '*.11' => "required_with:*.3",
            '11' =>  function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if(!is_numeric($value)){
                    $onFailure('Nhập định dạng ngày/tháng/năm chưa đúng');
                }
            },
            '*.12' => "required_with:*.3",
            '12' =>  function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if(!is_numeric($value)){
                    $onFailure('Nhập định dạng giờ/phút chưa đúng');
                }
            },
            '*.13' => "required_with:*.3",
        ];

        return $rules;
    }

    public function customValidationMessages()
    {
        $result = [
            '*.1.required_with' => __("server_validation.record_plan.department_code.required"),
            '*.3.required' => __("server_validation.record_plan.name.required"),
            '*.9.required_with' => __("server_validation.record_plan.start_date.required"),
            '*.10.required_with' => __("server_validation.record_plan.start_time.required"),
            '*.11.required_with' => __("server_validation.record_plan.end_date.required"),
            '*.12.required_with' => __("server_validation.record_plan.end_time.required"),
            '*.13.required_with' => __("server_validation.record_plan.address.required"),
        ];
        return $result;
    }

    public function headingRow(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 5;
    }


}
