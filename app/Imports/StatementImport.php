<?php


namespace App\Imports;

use App\Models\Member;
use App\Models\Statement;
use App\Models\Department;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithLimit;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class StatementImport implements ToCollection, WithValidation, WithStartRow, WithMultipleSheets,SkipsEmptyRows
{
    use Importable;

    public $mappingHeader = [
        "code" => 1,
        "title" => 2,
        "description" => 3,
        "department_id" => 5,
    ];

    public function __construct()
    {

    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }

    public function collection(Collection $rows)
    {
        $data = collect($rows)->map(function ($el){
            return $this->collectData($el);
        });

        $colUpdate = ["title", "description"];

        Statement::upsert($data->toArray(), ['code'], $colUpdate);
    }

    public function collectData($row)
    {
        $data = [];
        $keys = array_keys($this->mappingHeader);
        foreach($keys as $key){
            $data[$key] = $row[$this->mappingHeader[$key]];
        }
        $data["department_id"] = Department::where(["code" => $data["department_id"]])->pluck('id')->first();
        return $data;

    }

    public function withValidator($validator) {
        $validator->after(function($validator) {
            $data = $validator->getData();
            if(count($data) == 0) { 
                $validator->errors()->add('-1','Import thất bại, File rỗng');  
                }
        });
    }

    public function rules(): array
    {
        $roles = [
            '*.5' => function ($attribute, $value, $onFailure) {
                $value = preg_replace('/\s+/', '', $value);
                if(!empty($value)){
                    $checkDepartmentCode = Department::where(['code'=>$value])->exists();
                    if (!$checkDepartmentCode) {
                        $onFailure('Mã phòng ban không tồn tại');
                    }
                }
                else {
                    $onFailure('Mã phòng ban không hợp lệ');
                }
            },
        ];

        return $roles;
    }

    public function customValidationMessages()
    {
        $result = [

        ];

        return $result;
    }

    public function headingRow(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 6;
    }

}
