<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use SoftDeletes;

    protected $table = "cars";
    protected $fillable = ["id", "driver_id", "name", "model", "brand", "color", "number_of_seats", "license_plate", "status", "description", "note", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];

    public function transportation(){
        return $this->belongsTo(Member::class, "driver_id", "id");
    }
}
