<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChangeHistory extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "change_histories";
    protected $fillable = ["id", "tracking_table", "relation_table", "tracking_id", "relation_key", "change_type", "version",
        "change_field", "original_data", "new_data", "status", "note", "description", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];
}
