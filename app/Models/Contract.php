<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "contracts";
    protected $fillable = ["id", "code", "title", "description", "department_id", "statement_id", "partner_id", "urgent_level", "status", "note", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];

    public function department(){
        return $this->belongsTo(Department::class, "department_id", "id");
    }

    public function statement(){
        return $this->belongsTo(Statement::class, "statement_id", "id");
    }
}
