<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class Department extends Model
{
    use HasFactory;
    use SoftDeletes;
    use HasRoles;

    protected $table = "departments";
    protected $fillable = ["id", "name", "description", "parent_id", "code", "status", "note", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];
    // Define default guard name for laravel-permission, prevent error when creating roles/permissions
    protected $guard_name = "web";

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
}
