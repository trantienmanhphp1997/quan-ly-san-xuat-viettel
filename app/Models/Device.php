<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{
    use SoftDeletes;

    protected $table = "devices";
    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_category_id', 'device_category_code', 'name', "status", "serial", "description",
        "quantity"
    ];

    public function category()
    {
        return $this->belongsTo(DeviceCategory::class, "device_category_id", "id");
    }

    public function recordPlans()
    {
        return $this->belongsToMany(RecordPlan::class, 'record_plan_device_xrefs','device_id','record_plan_id')->withTimestamps();
    }

    public function lendingPlans(){
        return $this->belongsToMany(RecordPlan::class, "record_plan_device_xrefs", "device_id", "record_plan_id")
            ->wherePivot('status', config("common.status.propose_device.delivered"));
    }
    public function requestReturn(){
        return $this->belongsToMany(RecordPlan::class, "record_plan_device_xrefs", "device_id", "record_plan_id")
            ->wherePivot('status', config("common.status.propose_device.pending_return"));
    }

}
