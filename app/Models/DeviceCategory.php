<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeviceCategory extends Model
{
    use SoftDeletes;

    protected $table = "device_categories";
    protected $fillable = ["id", "name", "code", "display_code", "parent_id", "status", "description", "note", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];

    public function parent()
    {
        return $this->belongsTo(DeviceCategory::class, "parent_id", "id");
    }

    public function devices()
    {
        return $this->hasMany(Device::class, "device_category_id", "id");
    }
    public function child()
    {
        return $this->hasMany(DeviceCategory::class, "parent_id", "id");
    }


}
