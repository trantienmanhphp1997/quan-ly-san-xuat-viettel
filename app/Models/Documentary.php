<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documentary extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "documentaries";
    protected $fillable = ["id", "original_name", "file_name", "mime_type", "href", "related_document_type", "related_document_id", "sub_type", "status", "note", "description", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];
}
