<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExampleNote extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "example_notes";
    protected $fillable = ["id", "name", "type", "content", "code", "status", "note", "description", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];
}
