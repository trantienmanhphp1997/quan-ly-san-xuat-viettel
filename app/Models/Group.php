<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    protected $table = "groups";
    protected $fillable = ["id", "name", "code", "status", "note", "description", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];
}
