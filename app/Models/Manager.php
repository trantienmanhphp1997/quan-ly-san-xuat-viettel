<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class Manager extends Model
{
    use Notifiable;
    use SoftDeletes;
//    use HasRoles;

    protected $table = 'users';

    protected $guard_name = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', "phone_number", "full_name", "address",
        "department_code", "status", "created_by", "updated_by", "is_manager", "note"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'deleted_at'
    ];

    public function department(){
        return $this->hasOne(Department::class, "code", "department_code");
    }
    public function record_plans(){
        return $this->belongsToMany(RecordPlan::class, "record_plan_member_xrefs", "member_id", "record_plan_id" );
    }

    public function car(){
        return $this->hasOne(Car::class, "driver_id", "id");
    }

}
