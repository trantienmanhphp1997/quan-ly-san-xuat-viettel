<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Note extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "notes";
    protected $fillable = ["id", "record_plan_id", "from", "to", "content", "status", "note", "description", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];
}
