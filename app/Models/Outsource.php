<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Outsource extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "outsources";
    protected $fillable = ["id", "outsource_type_id", "name", "password", "description", "uuid", "full_name", "phone_number", "email", "address", "status", "note", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];

    public function outsource_type(){
        return $this->hasOne(OutsourceType::class, "id", "outsource_type_id");
    }
}
