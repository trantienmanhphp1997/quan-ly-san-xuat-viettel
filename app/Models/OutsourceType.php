<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutsourceType extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "outsource_types";
    protected $fillable = ["id", "name", "code", "status", "note", "description", "created_at", "updated_at", "created_by", "updated_by", "deleted_at", "department_code"];

    public function outsource(){
        return $this->belongsTo(Outsource::class);
    }

    public function department(){
        return $this->belongsTo(Department::class, "department_code", "code");
    }
}
