<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partner extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "partners";
    protected $fillable = ["id", "name", "email", "phone_number", "address", "field", "website", "status", "note", "description", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];
}
