<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanCategory extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "plan_categories";
    protected $fillable = ["id", "name", "code", "description", "parent_id", "status", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];
}
