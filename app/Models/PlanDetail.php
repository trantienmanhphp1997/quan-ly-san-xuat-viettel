<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanDetail extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "plan_details";
    protected $fillable = ["id", "plan_id", "month", "content", "revenue", "num_of_episodes", "status", "note", "description", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];
}
