<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanInfo extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "plan_info";
    protected $fillable = ["id", "code", "year", "title", "description", "department_id", "plan_category_id", "status", "note", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];

    public function department(){
        return $this->belongsTo(Department::class, "department_id", "id");
    }

    public function category(){
        return $this->belongsTo(PlanCategory::class, "plan_category_id", "id");
    }
}
