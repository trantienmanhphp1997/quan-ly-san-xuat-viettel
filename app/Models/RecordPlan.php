<?php

namespace App\Models;

use App\Services\MochaService;
use App\Services\RecordPlanService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\This;
use DateTime;

class RecordPlan extends Model
{
    use SoftDeletes;

    protected $table = "record_plans";
    protected $fillable = ["id", "name", "description", "plan_id", "plan_category_id", "version",
        "start_time", "end_time", "is_recurring", "is_allday", "on_monday", "on_tuesday", "on_wednesday", "on_thursday", "on_friday", "on_saturday", "on_sunday",
        "address", "reporter_allocate_status", "camera_allocate_status","technical_camera_allocate_status", "transportation_allocate_status", "device_allocate_status", "status", "department_code",
        "notes", "offer_reporter_note", "offer_camera_note", "offer_transportation_note", "offer_device_note",
        "offer_reporter_number", "offer_camera_number", "offer_transportation_number", "offer_device_number",
        "created_at", "updated_at", "created_by", "updated_by", "deleted_at","address_type", "offer_technical_camera_note", "offer_technical_camera_number",
        "excel_imported"
    ];
    protected $appends = ['can_update_record_plan', "actual_address"];

    public function members()
    {
        return $this->belongsToMany(User::class, "record_plan_member_xrefs", "record_plan_id", "member_id")->withPivot("involve_type", "transportation_status");
    }

    public function devices()
    {
        return $this->belongsToMany(Device::class, "record_plan_device_xrefs", "record_plan_id", "device_id");
    }

    public function notGivenDevice()
    {
        return $this->belongsToMany(Device::class, "record_plan_device_xrefs", "record_plan_id", "device_id")
            ->wherePivot('status', config("common.status.propose_device.approved"));
    }

    public function lendingDevices()
    {
        return $this->belongsToMany(Device::class, "record_plan_device_xrefs", "record_plan_id", "device_id")
            ->wherePivot('status', config("common.status.propose_device.delivered"));
    }

    public function lendingDevicesOnTime()
    {
        return $this->belongsToMany(Device::class, "record_plan_device_xrefs", "record_plan_id", "device_id")
            ->wherePivot('status', config("common.status.propose_device.delivered"))
            ->wherePivot('end_time', '>=', date('Y-m-d H:i:s', time()));
    }

    public function lendingDevicesOutTime()
    {
        return $this->belongsToMany(Device::class, "record_plan_device_xrefs", "record_plan_id", "device_id")
            ->wherePivot('status', config("common.status.propose_device.delivered"))
            ->wherePivot('end_time', '<', date('Y-m-d H:i:s', time()));
    }

    public function plan_category()
    {
        return $this->hasOne(PlanCategory::class, "id", "plan_category_id");
    }

    public function department()
    {
        return $this->belongsTo(Department::class, "department_code", "code");
    }

    public function plan()
    {
        return $this->belongsTo(PlanInfo::class, "plan_id", "id");
    }

    public function stage()
    {
        return $this->belongsTo(Stage::class, "address", "code");
    }

    public function creator()
    {
        return $this->belongsTo(User::class, "created_by", "id");
    }
    public function getCanUpdateRecordPlanAttribute()
    {
        return RecordPlanService::checkTimeToUpdateRecordPlan($this->end_time);

    }

    public function getActualAddressAttribute(){

        if($this->address_type == config("common.address_type.useStage") ){
            $actual_address = Stage::where(["code" => $this->address])->first();
            if($actual_address){
                return $actual_address->name;
            }
        }

        return $this->address;
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        /**
         * Gửi tin nhắn cho leader của từng phòng ban, yêu cầu phân công nhân sự cho lịch
         */
        static::created(function ($recordPlan) {
            $recordPlan->sendMessageToLeadersWhenCreateRecPlan();
        });

        /**
         * Gửi tin nhắn nếu chuyển trạng thái từ chờ duyệt sang đã chốt
         * */
        static::updating(function ($recordPlan) {
            //Tính toán message type
            $recordPlan->updateSendMessageType();
            //Gửi tin nhắn cho các quản lý liên quan
            $recordPlan->sendMessageToLeader();
            //Gửi tin nhắn cho member khi chốt lịch
            $recordPlan->sendMessageToMember();
            $recordPlan->sendMessagesToAssignedResources();
            $recordPlan->updateTimeInPivotTable();
        });
        static::updated(function ($recordPlan) {
            $recordPlan->updateStatusInMemberXref();
        });
    }

    //---------------------------------------------------Gửi thông báo đến quản lý khi mới tạo lịch ----------------------------------------
    private function formateDate($inputDate) {
        $date = new DateTime($inputDate);
        return $date->format('H:i d/m/Y');
    }

    public function sendMessageToLeadersWhenCreateRecPlan()
    {
        $recordPlan = $this;
        $label = "Xem chi tiết";
        $oa_id = config("mocha.oa_id");
        $creater = auth()->user() ? auth()->user() :  User::find($this->created_by);
        // với phóng viên, nếu có đề xuất nhân sự
        if ($recordPlan->offer_reporter_number || !empty($this->offer_reporter_note)) {
            $detailLink = "/record-plans/update/$recordPlan->id?record-plan-id=$recordPlan->id";
            $deeplink = "mocha://survey?ref=" . urlencode(config("mocha.wap_url") . $detailLink);
            $department_code = [$recordPlan->department_code];
            $reporter_managers = User::whereIn("department_code", $department_code)->where(["is_manager" => 1, "status" => 1])->whereNotNull("phone_number")->get();
            MochaService::sendMessageToUser($oa_id,
                'Đề xuất lịch sản xuất mới' ."\n"
            . 'Lịch sản xuất: ' . $this->name . "\n"
            . 'Người tạo: ' . $creater->full_name . "\n"
//            . 'Nội dung: ' . $this->description ."\n"
            . 'Khởi hành: ' . $this->formateDate($this->start_time) ."\n"
            . 'Kết thúc: ' . $this->formateDate($this->end_time) ."\n"
            . 'Nội dung đề xuất: ' . $this->offer_reporter_note
                , $reporter_managers, $label, $deeplink);
        }

        // phân công quay phim
        if ($recordPlan->offer_camera_number || !empty($this->offer_camera_note)) {
            //$detailLink = "/cameraman/list-suggest?record-plan-name=$recordPlan->name";
            $detailLink = "/cameraman/list-suggest";
            $deeplink = "mocha://survey?ref=" . urlencode(config("mocha.wap_url") . $detailLink);
            $camera_managers = User::whereIn("department_code", ["camera"])->where(["is_manager" => 1, "status" => 1])->whereNotNull("phone_number")->get();
            MochaService::sendMessageToUser($oa_id,
                'Đề xuất lịch sản xuất mới' ."\n"
                . 'Lịch sản xuất: ' . $this->name . "\n"
                . 'Người tạo: ' . $creater->full_name . "\n"
//                . 'Nội dung: ' . $this->description ."\n"
                . 'Khởi hành: ' . $this->formateDate($this->start_time) ."\n"
                . 'Kết thúc: ' . $this->formateDate($this->end_time) ."\n"
                . 'Nội dung đề xuất: ' . $this->offer_camera_note
                , $camera_managers, $label, $deeplink);
        }

        // phân công kỹ thuật phòng quay
        if ($recordPlan->offer_technical_camera_number || !empty($this->offer_technical_camera_note)) {
            //$detailLink = "/technical_cameraman/list-suggest?record-plan-name=$recordPlan->name";
            $detailLink = "/technical_cameraman/list-suggest";
            $deeplink = "mocha://survey?ref=" . urlencode(config("mocha.wap_url") . $detailLink);
            $technical_camera_managers = User::whereIn("department_code", ["technical_camera"])->where(["is_manager" => 1, "status" => 1])->whereNotNull("phone_number")->get();
            MochaService::sendMessageToUser($oa_id,
                'Đề xuất lịch sản xuất mới' ."\n"
                . 'Lịch sản xuất: ' . $this->name . "\n"
                . 'Người tạo: ' . $creater->full_name . "\n"
//                . 'Nội dung: ' . $this->description ."\n"
                . 'Khởi hành: ' . $this->formateDate($this->start_time) ."\n"
                . 'Kết thúc: ' . $this->formateDate($this->end_time) ."\n"
                . 'Địa điểm: ' . $this->actual_address ."\n"
                . 'Nội dung đề xuất: ' . $this->offer_technical_camera_note
                , $technical_camera_managers, $label, $deeplink);
        }

        // phân công lái xe
        if ($recordPlan->offer_transportation_number || !empty($this->offer_transportation_note)) {
            //$detailLink = "/drivers/list-suggest?record-plan-name=$recordPlan->name";
            $detailLink = "/drivers/list-suggest";
            $deeplink = "mocha://survey?ref=" . urlencode(config("mocha.wap_url") . $detailLink);
            $transportation_managers = User::whereIn("department_code", ["transportation"])->where(["is_manager" => 1, "status" => 1])->whereNotNull("phone_number")->get();
            MochaService::sendMessageToUser($oa_id,
                'Đề xuất lịch sản xuất mới' ."\n"
                . 'Lịch sản xuất: ' . $this->name . "\n"
                . 'Người tạo: ' . $creater->full_name . "\n"
//                . 'Nội dung: ' . $this->description ."\n"
                . 'Khởi hành: ' . $this->formateDate($this->start_time) ."\n"
                . 'Kết thúc: ' . $this->formateDate($this->end_time) ."\n"
                . 'Địa điểm: ' . $this->actual_address ."\n"
                . 'Nội dung đề xuất: ' . $this->offer_transportation_note
                , $transportation_managers, $label, $deeplink);
        }

        // phân công thiết bị
        if ($recordPlan->offer_device_number || !empty($this->offer_device_note)) {
            $device_managers = User::whereIn("department_code", ["device"])->where(["is_manager" => 1, "status" => 1])->whereNotNull("phone_number")->get();
            $deeplink = "mocha://survey?ref=".urlencode(config("mocha.wap_url")."/record-plans/detail/$this->id?record-plan-id=$this->id");
            MochaService::sendMessageToUser($oa_id,
                'Đề xuất lịch sản xuất mới' ."\n"
                . 'Lịch sản xuất: ' . $this->name . "\n"
                . 'Người tạo: ' . $creater->full_name . "\n"
//                . 'Nội dung: ' . $this->description ."\n"
                . 'Khởi hành: ' . $this->formateDate($this->start_time) ."\n"
                . 'Kết thúc: ' . $this->formateDate($this->end_time) ."\n"
                . 'Nội dung đề xuất: ' . $this->offer_device_note
                , $device_managers, $label, $deeplink);
        }

    }



//---------------------------------------------------Gửi thông báo đến nhân viên khi lịch được chốt ----------------------------------------

    /**
     * Lắng nghe sự kiện cập nhật của lịch sản xuất để chuẩn bị dữ liệu cho việc gửi tin nhắn
     * Nếu cột  thay đổi là thời gian --> duyệt cập nhật lại tất cả relation đến member, device --> update_time
     * Nếu cột thay đổi là địa điểm --> duyệt thay đổi các relation đến member -> update_place
     * Nếu cột thay đổi là đề xuất --> gửi đến quản lý của phòng ban tương ứng
     * */
    public function updateSendMessageType()
    {
        $originalValue = $this->original;
        $sendingType = array_key_exists("send_message_type", $originalValue) ? $originalValue['send_message_type'] : "";
        //Kiểm tra sự thay đổi của thời gian
        //Do định dạng của thời gian có thể bị client thay đổi nên phải ép kiểu về dateTime để so sánh để đảm bảo chính xác
        $startTimeNew = new \DateTime($this->start_time);
        $startTimeOriginal = new \DateTime($originalValue['start_time']);

        $endTimeNew = new \DateTime($this->end_time);
        $endTimeOriginal = new \DateTime($originalValue['end_time']);

        if ($startTimeNew != $startTimeOriginal || $endTimeNew != $endTimeOriginal) {
            if (empty($sendingType))
                $sendingType = config('common.sendMessageType.update_time');
//            if ($sendingType == config('common.sendMessageType.update_place'))
//                $sendingType = config('common.sendMessageType.update_time_place');
        }

        //Kiểm tra sự thay đổi của địa điểm
//        if ($this->address != $originalValue['address']) {
//            if (empty($sendingType))
//                $sendingType = config('common.sendMessageType.update_place');
//            if ($sendingType == config('common.sendMessageType.update_time'))
//                $sendingType = config('common.sendMessageType.update_time_place');
//        }

        /* //Kiểm tra sự thay đổi của đề xuất
         if(($this->offer_reporter_number != $originalValue["offer_reporter_number"]) || ($this->offer_reporter_number != $originalValue["offer_reporter_number"])
             || ($this->offer_reporter_number != $originalValue["offer_reporter_number"]) || ($this->offer_reporter_number != $originalValue["offer_reporter_number"])){
             $sendingType = config("common.sendMessageType.update_offer");
         }*/

        $this->send_message_type = $sendingType;

        //Cập nhật lại các relation liên quan
        //@todo Chỉ update sendMessage type với lịch đã từng chốt. Nếu không, sendMessageType = invite

        if ($sendingType == config('common.sendMessageType.update_time')) {
            RecordPlanMemberXref::where('record_plan_id', $this->id)
                ->where(function ($query) {
                    $query->where('send_message_type', '=', '')->orWhereNull('send_message_type');
                })
                ->update(['send_message_type' => config('common.sendMessageType.update_time')]);

        } elseif ($sendingType == config('common.sendMessageType.update_place')) {
            RecordPlanMemberXref::where('record_plan_id', $this->id)
                ->where(function ($query) {
                    $query->where('send_message_type', '=', '')->orWhereNull('send_message_type');
                })
                ->update(['send_message_type' => config('common.sendMessageType.update_place')]);

        } elseif ($sendingType == config('common.sendMessageType.update_time_place')) {
            RecordPlanMemberXref::where('record_plan_id', $this->id)
                ->where(function ($query) {
                    $query->whereIn('send_message_type', ['', config('common.sendMessageType.update_time'), config('common.sendMessageType.update_place')])->orWhereNull('send_message_type');
                })
                ->update(['send_message_type' => config('common.sendMessageType.update_time_place')]);
        }
    }

    /**
     * Nếu cập nhật liên quan đến thời gian, gửi cho tất cả các leader
     * Nếu cập nhật liên quan đến đề xuất, gửi thông báo cho quản lý tương ứng
     */
    public function sendMessageToLeader()
    {
        $originalValue = $this->original;
        //update đề xuất
        $keepOffer = [];
        $changeOfferr = [];
        Log::info("gửi tin nhắn cho quản lý");

        if ($this->offer_reporter_number != $originalValue['offer_reporter_number'] || $this->offer_reporter_note != $originalValue['offer_reporter_note'])
            $changeOfferr[] = 'reporter';
        else if ($this->offer_reporter_number != null && $this->offer_reporter_number > 0)
            $keepOffer[] = 'reporter';

        if ($this->offer_camera_number != $originalValue['offer_camera_number'] || $this->offer_camera_note != $originalValue['offer_camera_note'])
            $changeOfferr[] = 'camera';
        else if ($this->offer_camera_number != null && $this->offer_camera_number > 0)
            $keepOffer[] = 'camera';

        if ($this->offer_technical_camera_number != $originalValue['offer_technical_camera_number'] || $this->offer_technical_camera_note != $originalValue['offer_technical_camera_note'])
            $changeOfferr[] = 'technical_camera';
        else if ($this->offer_technical_camera_number != null && $this->offer_technical_camera_number > 0)
            $keepOffer[] = 'technical_camera';

        if ($this->offer_transportation_number != $originalValue['offer_transportation_number'] || $this->offer_transportation_note != $originalValue['offer_transportation_note'])
            $changeOfferr[] = 'transportation';
        else if ($this->offer_transportation_number != null && $this->offer_transportation_number > 0)
            $keepOffer[] = 'transportation';
// tuy nhiên, hiện tại bên wap k có quản lý thiết bị => k gửi tin nhắn cho quản lý thiết bị
        if ($this->offer_device_number != $originalValue['offer_device_number'] || $this->offer_device_note != $originalValue['offer_device_note'])
            $changeOfferr[] = 'device';
        else if ($this->offer_device_number != null && $this->offer_device_number > 0)
            $keepOffer[] = 'device';

        Log::info("keep and change offer", [$keepOffer, $changeOfferr]);

        //Kiểm tra xem thời gian có thay đổi không
        //Do định dạng của thời gian có thể bị client thay đổi nên phải ép kiểu về dateTime để so sánh để đảm bảo chính xác
        $startTimeNew = new \DateTime($this->start_time);
        $startTimeOriginal = new \DateTime($originalValue['start_time']);

        $endTimeNew = new \DateTime($this->end_time);
        $endTimeOriginal = new \DateTime($originalValue['end_time']);

        $changeTime = ($startTimeNew != $startTimeOriginal || $endTimeNew != $endTimeOriginal);
        $changeAddress = ($this->address != $originalValue["address"]);
        $oa_id = config("mocha.oa_id");

        $label = "Xem chi tiết";
        $deeplink = "mocha://survey?ref=".urlencode(config("mocha.wap_url")."/record-plans/detail/$this->id?record-plan-id=$this->id");
        $detailLinkCamera = "/cameraman/list-suggest";
        $deepLinkCamera = "mocha://survey?ref=" . urlencode(config("mocha.wap_url") . $detailLinkCamera);
        $detailLinkTechnicalCamera = "/technical_cameraman/list-suggest";
        $deepLinkTechnicalCamera = "mocha://survey?ref=" . urlencode(config("mocha.wap_url") . $detailLinkTechnicalCamera);
        $detailLinkDriver = "/drivers/list-suggest";
        $deepLinkDriver = "mocha://survey?ref=" . urlencode(config("mocha.wap_url") . $detailLinkDriver);

        if ($changeTime && !in_array($this->status, [config("common.status.record_plan.wrap"), config("common.status.record_plan.cancel")])) {
            if (count($keepOffer) > 0) {
                $users = User::where('is_manager', 1)->whereIn('department_code', $keepOffer)->where("department_code", "<>", config("common.departments.device"))->whereNotNull('phone_number')->get();
                $content = 'Lịch ' . $this->name . ' vừa thay đổi thời gian bạn cần xác nhận lại';
                $reporters = $users->filter(function ($user) {
                    return $user->department_code == config("common.departments.production");
                });
                $cameras = $users->filter(function ($user) {
                    return $user->department_code == config("common.departments.camera");
                });
                $technicalCameras = $users->filter(function ($user) {
                    return $user->department_code == config("common.departments.technical_camera");
                });
                $drives = $users->filter(function ($user) {
                    return $user->department_code == config("common.departments.transportation");
                });
                MochaService::sendMessageToUser($oa_id, $content, $reporters, $label, $deeplink);
                MochaService::sendMessageToUser($oa_id, $content, $cameras, $label, $deepLinkCamera);
                MochaService::sendMessageToUser($oa_id, $content, $technicalCameras, $label, $deepLinkTechnicalCamera);
                MochaService::sendMessageToUser($oa_id, $content, $drives, $label, $deepLinkDriver);
                if(in_array("device", $keepOffer)){
                    $this->sendMessageToDeviceLeaders($oa_id, $content, $this->name);
                }
            }
            if (count($changeOfferr) > 0) {
                $users = User::where('is_manager', 1)->whereIn('department_code', $changeOfferr)->where("department_code", "<>", config("common.departments.device"))->whereNotNull('phone_number')->get();
                $content = 'Lịch ' . $this->name . ' vừa thay đổi thời gian và số lượng đề xuất bạn cần xác nhận lại';
                $reporters = $users->filter(function ($user) {
                    return $user->department_code == config("common.departments.production");
                });
                $cameras = $users->filter(function ($user) {
                    return $user->department_code == config("common.departments.camera");
                });
                $technicalCameras = $users->filter(function ($user) {
                    return $user->department_code == config("common.departments.technical_camera");
                });
                $drives = $users->filter(function ($user) {
                    return $user->department_code == config("common.departments.transportation");
                });
                MochaService::sendMessageToUser($oa_id, $content, $reporters, $label, $deeplink);
                MochaService::sendMessageToUser($oa_id, $content, $cameras, $label, $deepLinkCamera);
                MochaService::sendMessageToUser($oa_id, $content, $technicalCameras, $label, $deepLinkTechnicalCamera);
                MochaService::sendMessageToUser($oa_id, $content, $drives, $label, $deepLinkDriver);
                if(in_array("device", $keepOffer)){
                    $this->sendMessageToDeviceLeaders($oa_id, $content, $this->name);
                }
            }
        } else {
            if (count($changeOfferr) > 0 && !in_array($this->status, [config("common.status.record_plan.wrap"), config("common.status.record_plan.cancel")])) {
                $messageContent = 'Lịch sản xuất ' . $this->name . ' vừa cập nhật đề xuất đề xuất bạn cần xác nhận lại';
                $users = User::where('is_manager', 1)->whereIn('department_code', $changeOfferr)->where("department_code", "<>", config("common.departments.device"))->whereNotNull('phone_number')->get();
                $reporters = $users->filter(function ($user) {
                    return $user->department_code == config("common.departments.production");
                });
                $cameras = $users->filter(function ($user) {
                    return $user->department_code == config("common.departments.camera");
                });
                $technicalCameras = $users->filter(function ($user) {
                    return $user->department_code == config("common.departments.technical_camera");
                });
                $drives = $users->filter(function ($user) {
                    return $user->department_code == config("common.departments.transportation");
                });
                MochaService::sendMessageToUser($oa_id, $messageContent, $reporters, $label, $deeplink);
                MochaService::sendMessageToUser($oa_id, $messageContent, $cameras, $label, $deepLinkCamera);
                MochaService::sendMessageToUser($oa_id, $messageContent, $technicalCameras, $label, $deepLinkTechnicalCamera);
                MochaService::sendMessageToUser($oa_id, $messageContent, $drives, $label, $deepLinkDriver);

                if(in_array("device", $keepOffer)){
                    $this->sendMessageToDeviceLeaders($oa_id, $messageContent, $this->name);
                }
            }
        }
    }

    /**
     * Dựa vào status của recordPlan, quyết định có gửi tin nhắn hay không
     * Dựa vào sendMessageType để gửi tin nhắn phù hợp
     */
    public function sendMessageToMember()
    {
        $originalValue = $this->original;
        $oa_id = config("mocha.oa_id");

        $label = "Xem chi tiết";
        $deeplink = "mocha://survey?ref=".urlencode(config("mocha.wap_url")."/record-plans/detail/$this->id?record-plan-id=$this->id");
        Log::info("gửi tin nhắn cho members ");
        if ($this->status == config('common.status.record_plan.approved') && $this->status != $originalValue['status']) {
            $invites = RecordPlanMemberXref::with(["member"])->where('record_plan_id', $this->id)->where('send_message_type', config('common.sendMessageType.invite'))->get();
            $invitePhones = $this->getMemberPhoneNumbers($invites);

            if (!blank($invitePhones)) {//k gửi tin nhắn khi lịch chốt
//                MochaService::sendMessageToUser($oa_id, 'Lịch sản xuất ' . $this->name . " đã chốt. Bạn hãy sắp xếp thời gian để tham gia lịch đầy đủ nhé.", $invitePhones, $label, $deeplink);
                $this->removeMessageTypeAfterSendMessage($this->id);
            }

            $updateTimes = RecordPlanMemberXref::with(["member"])->where('record_plan_id', $this->id)->where('send_message_type', config('common.sendMessageType.update_time'))->get();
            $updateTimePhones = $this->getMemberPhoneNumbers($updateTimes);
            if (!blank($updateTimePhones)) {
                $updateTimeMembers = [];
                foreach ($updateTimes as $xref) {
                    $updateTimeMembers[] = $xref->member;
                }
                MochaService::sendMessageToUser($oa_id, 'Lịch sản xuất ' . $this->name . ' Vừa cập nhật thời gian', $updateTimeMembers, $label, $deeplink);
                $this->removeMessageTypeAfterSendMessage($this->id);
            }

            $updatePlacePhones = RecordPlanMemberXref::with(["member"])->where('record_plan_id', $this->id)->where('send_message_type', config('common.sendMessageType.update_place'))->get();
//            $updatePlacePhones = $this->getMemberPhoneNumbers($updatePlacePhones);
            $updatePlaceMembers = [];
            foreach ($updatePlacePhones as $xref) {
                $updatePlaceMembers[] = $xref->member;
            }

            if (!blank($updatePlacePhones)) {
                MochaService::sendMessageToUser($oa_id, 'Lịch sản xuất ' . $this->name . ' vừa cập nhật địa điểm diễn ra', $updatePlaceMembers, $label, $deeplink);
                $this->removeMessageTypeAfterSendMessage($this->id);
            }

            $updateTimeAndPlatePhones = RecordPlanMemberXref::with(["member"])->where('record_plan_id', $this->id)->where('send_message_type', config('common.sendMessageType.update_time_place'))->get();
//            $updateTimeAndPlatePhones = $this->getMemberPhoneNumbers($updateTimeAndPlatePhones);
            $updateTimeAndPlaceMembers = [];
            foreach ($updateTimeAndPlatePhones as $xref) {
                $updateTimeAndPlaceMembers[] = $xref->member;
            }
            if (!blank($updateTimeAndPlatePhones)) {
                MochaService::sendMessageToUser($oa_id, 'Lịch sản xuất ' . $this->name . ' vừa cập nhật thời gian và địa điểm', $updateTimeAndPlaceMembers, $label, $deeplink);
                $this->removeMessageTypeAfterSendMessage($this->id);
            }
        }
    }

    public function sendMessagesToAssignedResources()
    {
        $recordPlan = $this;
        $originalValue = $this->original;
        $label = "Xem chi tiết";
        $deeplink = "mocha://survey?ref=".urlencode(config("mocha.wap_url")."/record-plans/detail/$this->id?record-plan-id=$this->id");
//        $label = "";
//        $deeplink = "";
        $oa_id = config("mocha.oa_id");
        if ($this->status == config("common.status.record_plan.cancel") && $this->status != $originalValue["status"]) {
            //<------Gửi cho quản lý-------->
            Log::info("gửi tin nhắn khi xóa lịch");
            //với phóng viên, nếu có đề xuất nhân sự
            if ($recordPlan->offer_reporter_number) {

                $department_code = [$recordPlan->department_code];
                $reporter_managers = User::whereIn("department_code", $department_code)->where(["is_manager" => 1, "status" => 1])->whereNotNull("phone_number")->get();
                MochaService::sendMessageToUser($oa_id, 'Lịch ' . $this->name . ' đã bị hủy.', $reporter_managers, $label, $deeplink);
            }

            //phân công quay phim
            if ($recordPlan->offer_camera_number) {
                $camera_managers = User::whereIn("department_code", ["camera"])->where(["is_manager" => 1, "status" => 1])->whereNotNull("phone_number")->get();
                MochaService::sendMessageToUser($oa_id, 'Lịch ' . $this->name . ' đã bị hủy.', $camera_managers, $label, $deeplink);
            }

            // phân công kỹ thuật phòng quay
            if ($recordPlan->offer_technical_camera_number) {
                $technical_camera_managers = User::whereIn("department_code", ["technical_camera"])->where(["is_manager" => 1, "status" => 1])->whereNotNull("phone_number")->get();
                MochaService::sendMessageToUser($oa_id, 'Lịch ' . $this->name . ' đã bị hủy.', $technical_camera_managers, $label, $deeplink);
            }

            //phân công lái xe
            if ($recordPlan->offer_transportation_number) {
                $transportation_managers = User::whereIn("department_code", ["transportation"])->where(["is_manager" => 1, "status" => 1])->whereNotNull("phone_number")->get();
                MochaService::sendMessageToUser($oa_id, 'Lịch ' . $this->name . ' đã bị hủy. ', $transportation_managers, $label, $deeplink);
            }

            //phân công thiết bị
            if ($recordPlan->offer_device_number) {
                $device_managers = User::whereIn("department_code", ["device"])->where(["is_manager" => 1, "status" => 1])->whereNotNull("phone_number")->get();
                MochaService::sendMessageToUser($oa_id, 'Lịch ' . $this->name . '  đã bị hủy. ', $device_managers, "", "");
            }

            //<------Gửi cho nhân viên-------->
            $members = RecordPlanMemberXref::with(["member"])->where('record_plan_id', $this->id)
                ->where(function ($query) {
//                    $query->where('send_message_type', '=', '')->orWhereNull('send_message_type');
                })->get();
//            $phone_numbers = $this->getMemberPhoneNumbers($members);
            $phone_numbers = [];
            foreach ($members as $xref) {
                $phone_numbers[] = ($xref->member);
            }

            Log::info("gửi cho nhân viên của lịch bị hủy");
            MochaService::sendMessageToUser($oa_id, 'Lịch sản xuất ' . $this->name . ' đã bị hủy', $phone_numbers, $label, $deeplink);

            $this->removeMessageTypeAfterSendMessage($this->id);
        }
    }

    public function removeMessageTypeAfterSendMessage($recordPlanId)
    {
        RecordPlanMemberXref::where(["record_plan_id" => $recordPlanId, "status" => config("common.status.proposals.assigned")])->update(["send_message_type" => null]);
        return true;
    }

    public function getMemberPhoneNumbers($member_xrefs)
    {
        $phone_numbers = [];

        foreach ($member_xrefs as $xref) {
            $phone_numbers[] = optional($xref->member)->phone_number;
        }
        return $phone_numbers;
    }

    public function updateStatusInMemberXref()
    {
        return RecordPlanMemberXref::where(["record_plan_id" => $this->id])->update(["record_plan_status" => $this->status]);
    }

    public function sendMessageToDeviceLeaders($oa_id, $content, $recordPlanName){
        $phoneNumbers = User::where('is_manager', 1)->where("department_code", config("common.departments.device"))->whereNotNull('phone_number')->get();
        MochaService::sendMessageToUser($oa_id, $content, $phoneNumbers, "Xem chi tiết",  $deeplink = "mocha://survey?ref=" . urlencode(config("mocha.wap_url") . "/my-assignment") );//"mocha://survey?ref=" . urlencode(config("mocha.wap_url") . "/my-assignment")
    }

    public function updateTimeInPivotTable(){
        $originalValue = $this->original;
        if($this["start_time"] != $originalValue["start_time"] || $this["end_time"] != $originalValue["end_time"]){
            //update start_time && end_time trong member_xrefs and device_xrefs;
            $memberIds = $this->members->pluck("id")->toArray();
            $deviceIds = $this->devices->pluck("id")->toArray();
            $data = [
              "start_time" => $this->start_time,
              "end_time" => $this->end_time,
            ];
            RecordPlanMemberXref::where(["record_plan_id" => $this->id])->whereIn("member_id", $memberIds)->update($data);
            RecordPlanDeviceXref::where(["record_plan_id" => $this->id])->whereIn("device_id", $deviceIds)->update($data);
        }
    }

}
