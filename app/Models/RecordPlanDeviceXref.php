<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecordPlanDeviceXref extends Model
{

    protected $table = "record_plan_device_xrefs";
    protected $fillable = ["id", "record_plan_id", "device_id", "start_time", "end_time", "note", "status", "description", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];

    public function device(){
        return $this->belongsTo(Device::class, "device_id", "id");
    }

    public function record_plan(){
        return $this->belongsTo(RecordPlan::class, "record_plan_id", "id");
    }
}
