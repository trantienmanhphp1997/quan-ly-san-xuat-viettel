<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class RecordPlanMemberXref extends Model
{

    protected $table = "record_plan_member_xrefs";
    protected $fillable = ["id", "record_plan_id", "member_id", "start_time", "end_time", "status", "note", "description", "created_at", "updated_at", "created_by", "updated_by", "deleted_at", "record_plan_status"];

    public function member()
    {
        return $this->belongsTo(Member::class, "member_id", "id");
    }

    public function record_plan()
    {
        return $this->belongsTo(RecordPlan::class, "record_plan_id", "id");
    }

    protected static function booted()
    {
        static::deleted(function ($recordPlanMemberXref) {
            $recordPlanMemberXref->addMemberToRemoving();
        });
        static::created(function ($recordPlanMemberXref) {
            $recordPlanMemberXref->addSendMessageType();
        });
    }

    //---------------Thêm sdt của member vào ds số điện thoại trong removing members------------------
    public function addMemberToRemoving()
    {

    }

    //---------------Thêm sdt của member vào ds số điện thoại trong removing members------------------
    public function addSendMessageType($type)
    {
        Log::info("member xref", [$this]);
    }
}
