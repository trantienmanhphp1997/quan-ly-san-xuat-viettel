<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecordPlanOutsourceXref extends Model
{
    use HasFactory;

    protected $table = "record_plan_outsource_xrefs";
    protected $fillable = ["id", "outsource_id", "contract_id", "price", "start_time", "end_time", "status", "note", "description", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];
}
