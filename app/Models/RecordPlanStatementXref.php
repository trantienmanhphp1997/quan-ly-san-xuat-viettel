<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecordPlanStatementXref extends Model
{
    use HasFactory;

    protected $table = "record_plan_statement_xrefs";
    protected $fillable = ["id", "record_plan_id", "statement_id", "status", "note", "description", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];
}
