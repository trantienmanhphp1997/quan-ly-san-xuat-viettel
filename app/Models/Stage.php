<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stage extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "cinematic_stages";
    protected $fillable = ["id", "code", "type","name", "address", "description", "status", "note", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];

}
