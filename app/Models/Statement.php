<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Statement extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "statements";
    protected $fillable = ["id", "code", "title", "description", "department_id", "original_statement_id", "security_level", "urgent_level", "status", "note", "created_at", "updated_at", "created_by", "updated_by", "deleted_at"];

    public function department(){
        return $this->belongsTo(Department::class, "department_id", "id");
    }
}
