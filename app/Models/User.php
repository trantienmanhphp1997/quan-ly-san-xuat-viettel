<?php

namespace App\Models;

use App\Services\MochaService;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', "phone_number", "other_phone_number", "full_name", "address",
        "department_code", "status", "created_by", "updated_by", "note", "is_manager", "language", "recieve_notice_phone_number"
    ];
    protected static function booted()
    {
        static::created(function($user){
            $user->sendMessageToUser();
        });

        static::restored(function ($user){
            //@todo recheck
            Log::info("restored user");
            $user->sendMessageToUser();
        });
    }

    public function sendMessageToUser(){
        $label = "Tham gia ngay";
        $deeplink = "mocha://survey?ref=".urlencode(config("mocha.wap_url")."/my-assignment");
        $oa_id = config("mocha.oa_id");
        if($this->is_manager == 1){
            $content = 'Xin chào '.$this->full_name . "!". "\n".  "Bạn vừa được phân làm quản lý " . __("department.$this->department_code") ;
        }else{
            $content = 'Xin chào '.$this->full_name . "!". "\n".  "Bạn vừa thêm làm thành viên của " . __("department.$this->department_code") ;
        }

        MochaService::sendMessageToUser($oa_id, $content, $this, $label, $deeplink);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $with = [
        "department"
    ];

    public function department(){
        return $this->hasOne(Department::class, "code", "department_code");
    }

    public function group(){
        return $this->hasOne(Group::class, "code", "department_code");
    }
    public function record_plans(){
        return $this->belongsToMany(RecordPlan::class, "record_plan_member_xrefs", "member_id", "record_plan_id" );
    }

    public function car(){
        return $this->hasOne(Car::class, "driver_id", "id");
    }
    public function record_plan_member_xref() // record_plan_id
    {
        return $this->hasMany(RecordPlanMemberXref::class, "member_id", "id");
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            "phone_number" => $this->phone_number,
            'is_manager' => $this->is_manager,
            'department_code' => $this->department_code
        ];
    }

}
