<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BasePolicy
{
    use HandlesAuthorization;

    protected $modelPermissionName = "";
    protected $managerOnly = true;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view any instance.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        if ($this->managerOnly) {
            return $user->hasPermissionTo("view_any_" . $this->modelPermissionName) && $user->is_manager === 1;
        } else {
            return $user->hasPermissionTo("view_any_" . $this->modelPermissionName);
        }
    }

    /**
     * Determine whether the user can view the specific instance.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function view(User $user, $instance)
    {
        return $user->hasPermissionTo("view_" . $this->modelPermissionName) && $user->id === $instance->created_by;
    }

    /**
     * Determine whether the user can create instance.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if ($this->managerOnly) {
            return $user->hasPermissionTo("create_" . $this->modelPermissionName) && $user->is_manager === 1;
        } else {
            return $user->hasPermissionTo("create_" . $this->modelPermissionName);
        }
    }

    /**
     * Determine whether the user can update the specific instance.
     *
     * @param  \App\Models\User  $user
     *
     * @return mixed
     */
    public function update(User $user, $instance)
    {
        return $user->hasPermissionTo("update_" . $this->modelPermissionName) && $user->id === $instance->created_by;
    }

    /**
     * Determine whether the user can update any instance.
     *
     * @param  \App\Models\User  $user
     *
     * @return mixed
     */
    public function updateAny(User $user)
    {
        if ($this->managerOnly) {
            return $user->hasPermissionTo("update_any_" . $this->modelPermissionName) && $user->is_manager === 1;
        } else {
            return $user->hasPermissionTo("update_any_" . $this->modelPermissionName);
        }
    }

    /**
     * Determine whether the user can delete the specific instance.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function delete(User $user, $instance)
    {
        return $user->hasPermissionTo("delete_any_" . $this->modelPermissionName) && $user->id === $instance->created_by;
    }

    /**
     *
     * Determine whether the user can delete any instance
     *
     * @param User $user
     * @param $instance
     */
    public function deleteAny(User $user)
    {
        if ($this->managerOnly) {
            return $user->hasPermissionTo("delete_any_" . $this->modelPermissionName) && $user->is_manager === 1;
        } else {
            return $user->hasPermissionTo("delete_any_" . $this->modelPermissionName);
        }
    }

    /**
     * Determine whether the user can restore the instance.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function restore(User $user)
    {
        if ($this->managerOnly) {
            return $user->hasPermissionTo("restore_" . $this->modelPermissionName) && $user->is_manager === 1;
        } else {
            return $user->hasPermissionTo("restore_" . $this->modelPermissionName);
        }
    }
}
