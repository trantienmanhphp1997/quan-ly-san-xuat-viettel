<?php

namespace App\Policies;

use App\Models\Car;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CarPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Car  $car
     * @return mixed
     */
    public function view(User $user, Car $car)
    {
        return ($user->department_code == config("common.departments.transportation") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to view this car");
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return ($user->department_code == config("common.departments.transportation") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to create a new car");
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Car  $car
     * @return mixed
     */
    public function update(User $user, Car $car)
    {
        return ($user->department_code == config("common.departments.transportation") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to update this car");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Car  $car
     * @return mixed
     */
    public function delete(User $user, Car $car)
    {
        return ($user->department_code == config("common.departments.transportation") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to delete this car");
    }

    public function deleteAny(User $user, Car $car){
        return ($user->department_code == config("common.departments.transportation") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to delete this car");
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Car  $car
     * @return mixed
     */
    public function restore(User $user, Car $car)
    {
        return ($user->department_code == config("common.departments.transportation") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to delete this car");
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Car  $car
     * @return mixed
     */
    public function forceDelete(User $user, Car $car)
    {
        //
    }

    public function export(){
        return true;
    }
    public function import(){
        return true;
    }
}
