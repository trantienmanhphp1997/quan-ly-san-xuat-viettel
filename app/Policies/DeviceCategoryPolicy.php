<?php

namespace App\Policies;

use App\Models\Device;
use App\Models\DeviceCategory;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class DeviceCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param DeviceCategory $deviceCategory
     * @return mixed
     */
    public function view(User $user, DeviceCategory $deviceCategory)
    {
        return ($user->department_code === config("common.departments.device") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to view this device category");
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return ($user->department_code === config("common.departments.device") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to create a new device category");
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param DeviceCategory $deviceCategory
     * @return mixed
     */
    public function update(User $user, DeviceCategory $deviceCategory)
    {
        return ($user->department_code === config("common.departments.device") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to update this device category");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param DeviceCategory $deviceCategory
     * @return mixed
     */
    public function delete(User $user, DeviceCategory $deviceCategory)
    {
        return ($user->department_code === config("common.departments.device") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to delete this device category");
    }

    /**
     * @param User $user
     * @param DeviceCategory $deviceCategory
     * @return Response
     */
    public function deleteAny(User $user, DeviceCategory $deviceCategory){
        return ($user->department_code === config("common.departments.device") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to delete this device category");
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User $user
     * @param DeviceCategory $deviceCategory
     * @return mixed
     */
    public function restore(User $user, DeviceCategory $deviceCategory)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User $user
     * @param DeviceCategory $device
     * @return mixed
     */
    public function forceDelete(User $user, DeviceCategory $device)
    {
        //
    }
}
