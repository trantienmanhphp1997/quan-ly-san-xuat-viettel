<?php

namespace App\Policies;

use App\Models\Device;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class DevicePolicy
{
    protected $modelPermissionName = "device_list";

    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Device $device
     * @return mixed
     */
    public function view(User $user, Device $device)
    {
        return ($user->department_code == config("common.departments.device") && $user->is_manager == 1) || ($user->hasRole('super admin'))
            ? Response::allow()
            : Response::deny("You are not allowed to view this device");
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return ($user->department_code == config("common.departments.device") && $user->is_manager == 1) || ($user->hasRole('super admin'))
            ? Response::allow()
            : Response::deny("You are not allowed to create a new device");
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Device $device
     * @return mixed
     */
    public function update(User $user, Device $device)
    {
        return ($user->department_code == config("common.departments.device") && $user->is_manager == 1) || ($user->hasRole('super admin'))
            ? Response::allow()
            : Response::deny("You are not allowed to update this device");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Device $device
     * @return mixed
     */
    public function delete(User $user, Device $device)
    {
        return ($user->department_code == config("common.departments.device") && $user->is_manager == 1) || ($user->hasRole('super admin'))
            ? Response::allow()
            : Response::deny("You are not allowed to delete this device");
    }

    public function deleteAny(User $user, Device $device){
        return ($user->department_code == config("common.departments.device") && $user->is_manager == 1) || ($user->hasRole('super admin'))
            ? Response::allow()
            : Response::deny("You are not allowed to delete this device");
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User $user
     * @param Device $device
     * @return mixed
     */
    public function restore(User $user, Device $device)
    {
        return ($user->department_code == config("common.departments.device") && $user->is_manager == 1) || ($user->hasRole('super admin'))
            ? Response::allow()
            : Response::deny("You are not allowed to delete this device");
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User $user
     * @param Device $device
     * @return mixed
     */
    public function forceDelete(User $user, Device $device)
    {
        //
    }
    public function import(User $user)
    {
        return $user->is_manager == 1;
    }
    public function export(){
        return true;
    }

    public function exportDataExpiredLendingDayDevices(User $user, Device $device){
        return ($user->department_code == config("common.departments.device") && $user->is_manager == 1) || ($user->hasRole('super admin'))
            ? Response::allow()
            : Response::deny("You are not allowed to export");
    }
}
