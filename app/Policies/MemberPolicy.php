<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class MemberPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User $User
     * @return mixed
     */
    public function view(User $user)
    {
        return ($user->is_manager == 1 )  || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to view this member");
    }

    /**
     * Determine whether the user can create models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->is_manager == 1
            ? Response::allow()
            : Response::deny("You are not allowed to create new member");
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User $User
     * @return mixed
     */
    public function update(User $user)
    {
        return ($user->is_manager == 1 ) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You can not update this member ");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User $User
     * @return mixed
     */
    public function delete(User $user)
    {
        return ($user->is_manager == 1 )  || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You can not delete this member ");
    }

    public function deleteAny(User $user)
    {
        return ($user->is_manager == 1 || $user->hasRole('super admin'))
                   ? Response::allow()
                   : Response::deny("You can not delete this member ");
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User $User
     * @return mixed
     */
    public function restore(User $user)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User $User
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        //
    }

    public function import(User $user)
    {
        return $user->is_manager == 1;
    }

    public function export(User $user)
    {
        return $user->is_manager == 1;
    }



}
