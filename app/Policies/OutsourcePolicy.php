<?php

namespace App\Policies;

use App\Models\Outsource;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class OutsourcePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->is_manager == 1
            ? Response::allow()
            : Response::deny("You are not allowed to view outsources");
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Outsource  $outsource
     * @return mixed
     */
    public function view(User $user, Outsource $outsource)
    {
        return $user->is_manager == 1
            ? Response::allow()
            : Response::deny("You are not allowed to view this outsource");
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->is_manager == 1
            ? Response::allow()
            : Response::deny("You are not allowed to create this outsource");
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Outsource  $outsource
     * @return mixed
     */
    public function update(User $user, Outsource $outsource)
    {
        return $user->is_manager == 1
            ? Response::allow()
            : Response::deny("You are not allowed to update this outsource");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Outsource  $outsource
     * @return mixed
     */
    public function delete(User $user, Outsource $outsource)
    {
        return $user->is_manager == 1
            ? Response::allow()
            : Response::deny("You are not allowed to delete this outsource");
    }

    public function deleteAny(User $user, Outsource $outsource)
    {
        return $user->is_manager == 1
            ? Response::allow()
            : Response::deny("You are not allowed to delete this outsource");
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Outsource  $outsource
     * @return mixed
     */
    public function restore(User $user, Outsource $outsource)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Outsource  $outsource
     * @return mixed
     */
    public function forceDelete(User $user, Outsource $outsource)
    {
        //
    }
}
