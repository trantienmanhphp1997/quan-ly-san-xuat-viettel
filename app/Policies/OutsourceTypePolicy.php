<?php

namespace App\Policies;

use App\Models\OutsourceType;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class OutsourceTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OutsourceType  $outsourceType
     * @return mixed
     */
    public function view(User $user, OutsourceType $outsourceType)
    {
        return $user->is_manager == 1
            ? Response::allow()
            : Response::deny("You are not allowed to view this outsource type");
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->is_manager == 1
            ? Response::allow()
            : Response::deny("You are not allowed to create a new outsource type");
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OutsourceType  $outsourceType
     * @return mixed
     */
    public function update(User $user, OutsourceType $outsourceType)
    {
        return $user->is_manager == 1
            ? Response::allow()
            : Response::deny("You are not allowed to update this outsource type");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OutsourceType  $outsourceType
     * @return mixed
     */
    public function delete(User $user, OutsourceType $outsourceType)
    {
        return $user->is_manager == 1
            ? Response::allow()
            : Response::deny("You are not allowed to delete this outsource type");
    }

    public function deleteAny(User $user, OutsourceType $outsourceType)
    {
        return $user->is_manager == 1
            ? Response::allow()
            : Response::deny("You are not allowed to delete this outsource type");
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OutsourceType  $outsourceType
     * @return mixed
     */
    public function restore(User $user, OutsourceType $outsourceType)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OutsourceType  $outsourceType
     * @return mixed
     */
    public function forceDelete(User $user, OutsourceType $outsourceType)
    {
        //
    }
}
