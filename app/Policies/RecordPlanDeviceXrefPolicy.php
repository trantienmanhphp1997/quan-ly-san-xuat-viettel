<?php

namespace App\Policies;

use App\Models\RecordPlanDeviceXref;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RecordPlanDeviceXrefPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\RecordPlanDeviceXref  $recordPlanDeviceXref
     * @return mixed
     */
    public function view(User $user, RecordPlanDeviceXref $recordPlanDeviceXref)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\RecordPlanDeviceXref  $recordPlanDeviceXref
     * @return mixed
     */
    public function update(User $user, RecordPlanDeviceXref $recordPlanDeviceXref)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\RecordPlanDeviceXref  $recordPlanDeviceXref
     * @return mixed
     */
    public function delete(User $user, RecordPlanDeviceXref $recordPlanDeviceXref)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\RecordPlanDeviceXref  $recordPlanDeviceXref
     * @return mixed
     */
    public function restore(User $user, RecordPlanDeviceXref $recordPlanDeviceXref)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\RecordPlanDeviceXref  $recordPlanDeviceXref
     * @return mixed
     */
    public function forceDelete(User $user, RecordPlanDeviceXref $recordPlanDeviceXref)
    {
        //
    }

    public function export(User $user, RecordPlanDeviceXref $recordPlanDeviceXref){
        return true;
    }

    public function canUpdateApprovedRecordPlan(User $user, RecordPlanDeviceXref $recordPlanDeviceXref){
        $diff = $this->getDayDiff($recordPlanDeviceXref->record_plan);
        $recordPlan = $recordPlanDeviceXref->record_plan;
        return  ((str_contains(auth()->user()->department_code, "production") && $user->id == $recordPlan->created_by) || $user->hasRole('super admin'))
            && $diff ;
    }

    public function getDayDiff($record_plan){
        $start_time =  new \DateTime($record_plan->start_time, new \DateTimeZone('Asia/Ho_Chi_Minh'));
        $now = new \DateTime(now());

        $diff = $start_time->getTimestamp() - $now->getTimestamp();
        if($diff < 86400){
            return false;
        }
        return true;

    }
}
