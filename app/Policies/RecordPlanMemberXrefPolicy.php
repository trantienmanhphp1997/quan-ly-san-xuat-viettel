<?php

namespace App\Policies;

use App\Models\RecordPlanDeviceXref;
use App\Models\RecordPlanMemberXref;
use App\Models\User;
use App\Services\RecordPlanService;
use Illuminate\Auth\Access\HandlesAuthorization;

class RecordPlanMemberXrefPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\RecordPlanMemberXref  $RecordPlanMemberXref
     * @return mixed
     */
    public function view(User $user, RecordPlanMemberXref $RecordPlanMemberXref)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\RecordPlanMemberXref  $RecordPlanMemberXref
     * @return mixed
     */
    public function update(User $user, RecordPlanMemberXref $RecordPlanMemberXref)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\RecordPlanMemberXref  $RecordPlanMemberXref
     * @return mixed
     */
    public function delete(User $user, RecordPlanMemberXref $RecordPlanMemberXref)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\RecordPlanMemberXref  $RecordPlanMemberXref
     * @return mixed
     */
    public function restore(User $user, RecordPlanMemberXref $RecordPlanMemberXref)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\RecordPlanMemberXref  $RecordPlanMemberXref
     * @return mixed
     */
    public function forceDelete(User $user, RecordPlanMemberXref $RecordPlanMemberXref)
    {
        //
    }

    public function export(User $user, RecordPlanDeviceXref $RecordPlanMemberXref){
        return true;
    }

    public function canAssignResource(User $user, RecordPlanMemberXref $recordPlanMemberXref){
        return  RecordPlanService::checkTimeToUpdateRecordPlan($recordPlanMemberXref->record_plan->start_time);
    }
}
