<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RecordPlan;
use App\Services\RecordPlanService;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class RecordPlanPolicy
{
    protected $modelPermissionName = "record_plans";
    protected $managerOnly = false;

    /**
     * Determine whether the user can view any record plans.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the record plan.
     *
     * @param \App\Models\User $user
     * @param \App\Models\RecordPlan $recordPlan
     * @return mixed
     */
    public function view(User $user, RecordPlan $recordPlan)
    {
        return true;
    }

    /**
     * Determine whether the user can create record plans.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return ($user->is_manager == 1 && (str_contains(auth()->user()->department_code, "production")))|| $user->hasRole('super admin')
            ? Response::allow() : Response::deny('You are not allowed to create new record plan.');
    }

    /**
     * Determine whether the user can update the record plan.
     *
     * @param \App\Models\User $user
     * @param \App\Models\RecordPlan $recordPlan
     * @return mixed
     */
    /*public function update(User $user, RecordPlan $recordPlan)
    {
        return $user->id == $recordPlan->created_by || $user->hasRole('super admin
            ? Response::allow()
            : Response::deny('You do not own this record plan.');
    }

    /**
     * Determine whether the user can delete the record plan.
     *
     * @param \App\Models\User $user
     * @param \App\Models\RecordPlan $recordPlan
     * @return mixed
     */
    /*public function delete(User $user, RecordPlan $recordPlan)
    {
        return $user->id == $recordPlan->created_by || $user->hasRole('super admin
            ? Response::allow() : Response::deny('You do not own this record plan.');
    }*/

    public function deleteAny(User $user, RecordPlan $recordPlan)
    {
        return $user->id == $recordPlan->created_by || $user->hasRole('super admin')
            ? Response::allow() : Response::deny('You do not own this record plan.');
    }

    /**
     * Determine whether the user can restore the record plan.
     *
     * @param \App\Models\User $user
     * @param \App\Models\RecordPlan $recordPlan
     * @return mixed
     */
    public function restore(User $user, RecordPlan $recordPlan)
    {
        return $user->id == $recordPlan->created_by || $user->hasRole('super admin')
            ? Response::allow() : Response::deny('You do not own this record plan.');
    }

    /**
     * Determine whether the user can permanently delete the record plan.
     *
     * @param \App\Models\Manager $user
     * @param \App\Models\RecordPlan $recordPlan
     * @return mixed
     */
    public function forceDelete(User $user, RecordPlan $recordPlan)
    {

    }

    public function export(User $user, RecordPlan $recordPlan)
    {
//        return false;
        return $user->is_manager == 1;
    }

    public function import(User $user, RecordPlan $recordPlan)
    {
//        return false;
        return ($user->is_manager == 1 && str_contains(auth()->user()->department_code, "production")) || $user->hasRole("super admin");
    }

    public function is_author(User $user, RecordPlan $recordPlan)
    {
        return (($user->is_manager == 1 && str_contains(auth()->user()->department_code, "production") && $user->id == $recordPlan->created_by) || $user->hasRole('super admin'));
//            && RecordPlanService::checkTimeToUpdateRecordPlan($recordPlan->end_time);
    }


    public function can_assign_reporter(User $user, RecordPlan $recordPlan)
    {
        return (($user->is_manager == 1 && str_contains(auth()->user()->department_code, "production") && $user->id == $recordPlan->created_by) || $user->hasRole('super admin'))
            && $recordPlan->status != config("common.status.record_plan.cancel")
            && $recordPlan->status != config("common.status.record_plan.done")
            && $recordPlan->status != config("common.status.record_plan.wrap")
            && $recordPlan->status != config("common.status.record_plan.inactive")
        ;
    }

    public function delete(User $user, RecordPlan $recordPlan)
    {
        return  (($user->is_manager == 1 && str_contains(auth()->user()->department_code, "production") && $user->id == $recordPlan->created_by) || $user->hasRole('super admin'))
            && $recordPlan->status != config("common.status.record_plan.done")
            && $recordPlan->status != config("common.status.record_plan.wrap");
    }

    public function update(User $user, RecordPlan $recordPlan)
    {
        return  ((str_contains(auth()->user()->department_code, "production") && $user->id == $recordPlan->created_by) || $user->hasRole('super admin'))
            && ($recordPlan->is_recurring == 1 || (($recordPlan->status == config("common.status.record_plan.inactive")
                || $recordPlan->status == config("common.status.record_plan.pending")
                || $recordPlan->status == config("common.status.record_plan.approved")
                || $recordPlan->status == config("common.status.record_plan.in_progress")
            ) && RecordPlanService::checkTimeToUpdateRecordPlan($recordPlan->end_time)));
    }

    public function cancel(User $user, RecordPlan $recordPlan){
        return  ((str_contains(auth()->user()->department_code, "production") && $user->id == $recordPlan->created_by) || $user->hasRole('super admin'))
            && (
                $recordPlan->status == config("common.status.record_plan.pending")
                || $recordPlan->status == config("common.status.record_plan.approved")
                || $recordPlan->status == config("common.status.record_plan.in_progress")
            );
    }

    public function canUpdateApprovedRecordPlan(User $user, RecordPlan $recordPlan){
        return RecordPlanService::checkTimeToUpdateRecordPlan($recordPlan->end_time);
    }

    public function assignResource(User $user, RecordPlan $recordPlan){
        $canAssignMember = false;
        if(!str_contains($user->department_code, "production")){
            if($recordPlan["offer_".$user->department_code."_number"] > 0 && $user->department_code !=config("common.departments.device")){
                $canAssignMember = true;
            }
        }else{
            $canAssignMember = true;
        }
        return ($user->hasRole("super admin") || ($canAssignMember == true && $user->is_manager == 1))
            && RecordPlanService::checkTimeToUpdateRecordPlan($recordPlan->end_time)
            && RecordPlanService::checkGeneralStatusCanUpdate($recordPlan->status) ;
    }

    public function assignDevice(User $user, RecordPlan $recordPlan){
        return ($user->hasRole("super admin") || ($user->department_code == config("common.departments.device") && $user->is_manager == 1))
            && RecordPlanService::checkTimeToUpdateRecordPlan($recordPlan->end_time)
            && RecordPlanService::checkGeneralStatusCanUpdate($recordPlan->status) ;
    }

    public function proposalDevice(User $user, RecordPlan $recordPlan)
    {
        return (($user->is_manager == 1 && in_array($user->department_code, [
            config("common.departments.production"),
            config("common.departments.general_news"),
            config("common.departments.news"),
            config("common.departments.documentary"),
            config("common.departments.entertainment"),
            config("common.departments.music"),
            config("common.departments.film")
        ]) && $user->id == $recordPlan->created_by) || $user->hasRole('super admin')) || ($user->is_manager == 1 && $user->department_code == config("common.departments.camera") || ($user->is_manager == 1 && $user->department_code == config("common.departments.technical_camera")));
    }
}
