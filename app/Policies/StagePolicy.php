<?php

namespace App\Policies;

use App\Models\Stage;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class StagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Stage  $stage
     * @return mixed
     */
    public function view(User $user, Stage $stage)
    {
        return ($user->department_code == config("common.departments.transportation") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to view this stage");
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return ($user->department_code == config("common.departments.transportation") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to create a new stage");
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Stage  $stage
     * @return mixed
     */
    public function update(User $user, Stage $stage)
    {
        return ($user->department_code == config("common.departments.transportation") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to update this stage");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Stage  $stage
     * @return mixed
     */
    public function delete(User $user, Stage $stage)
    {
        return ($user->department_code == config("common.departments.transportation") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to delete this stage");
    }

    public function deleteAny(User $user, Stage $stage){
        return ($user->department_code == config("common.departments.transportation") && $user->is_manager == 1) || $user->hasRole('super admin')
            ? Response::allow()
            : Response::deny("You are not allowed to delete this stage");
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Stage  $stage
     * @return mixed
     */
//    public function restore(User $user, Stage $stage)
//    {
//        return ($user->department_code == config("common.departments.transportation") && $user->is_manager == 1) || $user->hasRole('super admin')
//            ? Response::allow()
//            : Response::deny("You are not allowed to delete this stage");
//    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Stage  $stage
     * @return mixed
     */
    public function forceDelete(User $user, Stage $stage)
    {
        //
    }

    public function export(){
        return true;
    }
    public function import(){
        return false;
    }
}
