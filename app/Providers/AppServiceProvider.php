<?php

namespace App\Providers;

use App\Http\Controllers\Admin\AssignController;
use App\Services\Interfaces\IAssignmentService;
use App\Services\Interfaces\IMemberService;
use App\Services\Interfaces\IRecordPlanService;
use App\Services\MemberService;
use App\Services\RecordPlanService;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        Schema::defaultStringLength(191);
        $this->app->bind(IRecordPlanService::class, RecordPlanService::class);
        $this->app->bind(IMemberService::class, MemberService::class);
        $this->app->bind(IAssignmentService::class, AssignController::class);
//       if(env('APP_DEBUG') == "true") {
//           DB::listen(function($query) {
//               File::append(
//                   storage_path('/logs/query.log'),
//                   $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL
//               );
//           });
//       }
    }
}
