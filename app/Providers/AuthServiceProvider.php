<?php

namespace App\Providers;

use App\Models\Car;
use App\Models\Device;
use App\Models\DeviceCategory;
use App\Models\Outsource;
use App\Models\OutsourceType;
use App\Models\RecordPlan;
use App\Models\RecordPlanDeviceXref;
use App\Models\RecordPlanMemberXref;
use App\Models\Stage;
use App\Models\User;
use App\Policies\CarPolicy;
use App\Policies\DeviceCategoryPolicy;
use App\Policies\DevicePolicy;
use App\Policies\ManagerPolicy;
use App\Policies\MemberPolicy;
use App\Policies\OutsourcePolicy;
use App\Policies\OutsourceTypePolicy;
use App\Policies\RecordPlanDeviceXrefPolicy;
use App\Policies\RecordPlanMemberXrefPolicy;
use App\Policies\RecordPlanPolicy;
use App\Policies\StagePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [

        User::class => ManagerPolicy::class,
        User::class => MemberPolicy::class,
        RecordPlan::class => RecordPlanPolicy::class,
        Car::class => CarPolicy::class,
        OutsourceType::class => OutsourceTypePolicy::class,
        Outsource::class => OutsourcePolicy::class,
        Device::class => DevicePolicy::class,
        DeviceCategory::class => DeviceCategoryPolicy::class,
        RecordPlanMemberXref::class => RecordPlanMemberXrefPolicy::class,
        RecordPlanDeviceXref::class => RecordPlanDeviceXrefPolicy::class,
        Stage::class => StagePolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define("access_routes", function ($user, $route) {
            return auth()->user()->hasPermissionTo('access_' . $route);
        });

        Gate::define("manager_driver", function ($user){
            return ($user->hasRole("department transportation") && $user->is_manager === 1) || $user->hasRole("super admin");
        });

        Gate::define("removeAssignedMember", function ($user, $departmentCode){
            if($departmentCode == "reporter"){
                return (str_contains($user->department_code, "production") && $user->is_manager == 1) || $user->hasRole("super admin");
            }else{
                return ($user->department_code == $departmentCode && $user->is_manager == 1 ) || $user->hasRole("super admin");
            }
        });
        // Define super admin role
//        Gate::before(function ($user, $ability) {
//            if ($user->hasRole('super admin')) {
//                return true;
//            }
//        });
    }
}
