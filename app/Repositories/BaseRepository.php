<?php

namespace App\Repositories;

use App\Traits\isValue;
use Illuminate\Container\Container as Application;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    use isValue;
    /**
     * @var array $fieldSearchable Danh sách các trường cho phép client search like
     * tương tự quickSearchField nhưng không dùng cho relationship
     * là tập con trong quickSearchField
     */
    protected $fieldSearchable = [];

    /**
     * @var array Danh sách các trường sử dụng trong quick search.
     * Nếu sử dụng arrow để trỏ đến bảng liên kết
     * Ex: ['name', 'color', 'description',  'carOwner->users.name', 'energySource->energy_sources.name'];
     * carOwner là relationship giữa cars và users
     * users là tên bảng
     * name là trường trong bảng users
     */
    protected $quickSearchField = [];
    /**
     * @var array Danh sách các trường cho phép client filter equal
     * Ex: dùng cho danh sách chọn xổ xuống
     */
    protected $fieldFilterable = [];
    /**
     * @var Model
     */
    protected $model;

    /**
     * Configure the Model
     *
     * @return string
     */
    abstract public function model();

    /**
     * @var Application
     */
    protected $app;

    /**
     * @param Application $app
     *
     * @throws \Exception
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->makeModel();
//        log query laravel
//        DB::connection()->enableQueryLog();
    }

    /**
     * Get searchable fields array
     * search like
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Get filterable fields array
     * equal search
     * @return array
     */
    public function getFieldsFilterable()
    {
        return $this->fieldFilterable;
    }

    /**
     * Make Model instance
     *
     * @return Model
     * @throws \Exception
     *
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    /**
     * Paginate records for scaffold.
     *
     * @param int $perPage
     * @param array $columns
     * @return LengthAwarePaginator
     */
    public function paginate($perPage, $search = [], $columns = ['*'])
    {
        $query = $this->allQuery($search);

        return $query->paginate($perPage, $columns);
    }

    protected function expandQuery($baseQuery, $search)
    {
        return $baseQuery;
    }

    protected function makeSearchQuery($baseQuery, $value)
    {
        $resultQuery = $baseQuery;
        $search_value = '%' . $value . '%';
        foreach ($this->quickSearchField as $quickSearchField) {
            $splitField = explode("->", $quickSearchField);
            if (count($splitField) == 1) {
                $baseQuery = $baseQuery->orWhere($quickSearchField, 'LIKE', $search_value);
            } else if (count($splitField) == 2) {
                $baseQuery = $baseQuery->orWhereHas($splitField[0], function ($query) use ($splitField, $search_value) {
                    $query->Where($splitField[1], 'LIKE', $search_value);
                });
            } else {
                $baseQuery = $baseQuery->orWhereHas($splitField[0], function ($query) use ($splitField, $search_value) {
                    $query->whereHas($splitField[1], function ($query) use ($splitField, $search_value) {
                        $query->Where($splitField[2], 'LIKE', $search_value);
                    });
                });
            }
        }
        return $resultQuery;
    }

    /**
     * Build a query for retrieving all records.
     *
     * @param array $search
     * @param int|null $skip
     * @param int|null $limit
     * @return Builder
     */
    public function allQuery($search = [], $skip = null, $limit = null)
    {
        $query = $this->model->newQuery();
        if (count($search)) {
            foreach ($search as $key => $value) {
                if (in_array($key, $this->getFieldsFilterable())) {
                    if (is_array($value) || $value instanceof Arrayable) {
                        $query = $query->whereIn($key, $value);
                    } else if ($value == 'null') { //Chỉ query null khi client chủ động truyền null --> giải quyết vấn đề muốn bỏ qua filter
                        $query = $query->whereNull($key);
                    } else if ($value !== "" && is_null($value) == false) { //Khi không truyền dữ liệu --> không apply filter
                        $query->where($key, $value);
                    }
                }
                //filter
                if (in_array($key, $this->getFieldsSearchable())) {
                    if(strpos($value, "%") !== false) {
                        $query->where($key, 'like', $value);
                    }else{
                        $query->where($key, 'like', '%' . $value . '%');
                    }
                }
                //Search
                if ($key == 's') {

                    $query->where(function ($query) use ($value) {
                        $query = $this->makeSearchQuery($query, $value);
                    });
                }
            }

            if (!is_null($skip)) {
                $query->skip($skip);
            }

            if (!is_null($limit)) {
                $query->limit($limit);
            }
        }
        return $this->expandQuery($query, $search);
    }

    /**
     * Retrieve all records with given filter criteria
     *
     * @param array $search
     * @param int|null $skip
     * @param int|null $limit
     * @param array $columns
     *
     * @return LengthAwarePaginator|Builder[]|Collection
     */
    public function all($search = [], $skip = null, $limit = null, $columns = ['*'])
    {
        $query = $this->allQuery($search, $skip, $limit);

        return $query->get($columns);
    }

    /**
     * Create model record
     *
     * @param array $input
     *
     * @return Model
     */
    public function create($input)
    {
        $model = $this->model->newInstance($input);

        $model->save();

        return $model;
    }

    public function insert($data){
        $query = $this->model->newQuery();

        return $query->insert($data);
    }

    /**
     * Find model record for given id
     *
     * @param int $id
     * @param array $columns
     *
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function find($id, $columns = ['*'])
    {
        $query = $this->model->newQuery();

        return $query->find($id, $columns);
    }

    /**
     * @param $id
     * @param array $columns
     * @return Builder|Builder[]|Collection|Model
     */
    public function findOrFail($id, $columns = ['*'])
    {
        $query = $this->model->newQuery();

        return $query->findOrFail($id, $columns);
    }

    /**
     * Update model record for given id
     *
     * @param array $input
     * @param int $id
     *
     * @return Builder|Builder[]|Collection|Model
     */
    public function update($input, $id)
    {
        $query = $this->model->newQuery();

        $model = $query->findOrFail($id);

        $model->fill($input);

        $model->save();

        return $model;
    }


    /**
     * @param $input
     * @param $id
     * @return mixed
     */
    public function updateOrCreate($input, $id)
    {
        return $this->model->updateOrCreate(['id' => $id], $input);
    }

    /**
     * @param mixed $id
     *
     * @return bool|mixed|null
     * @throws \Exception
     *
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function restoreMany(array $id)
    {
        return $this->model->onlyTrashed()->whereIn('id', $id)->restore();
    }

    public function onlyTrashed()
    {
        return $this->model->onlyTrashed();
    }

    public function withTrashed()
    {
        return $this->model->withTrashed();
    }
}
