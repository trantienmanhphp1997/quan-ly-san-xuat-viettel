<?php


namespace App\Repositories;

use App\Models\Car;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class CarRepository extends BaseRepository
{
    protected $fieldSearchable = ['name'];

    protected $quickSearchField = ["name", "brand", "model", "number_of_seats", 'license_plate', "transportation->users.id"];

    protected $fieldFilterable = ["model", "number_of_seats", 'license_plate', "status"];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Car::class;
    }

    protected function expandQuery($baseQuery, $search)
    {
        return $baseQuery->with(["transportation"])->orderBy("created_at", "desc");
    }

    /**
     * @param $car_ids
     * @param $status
     * @return bool
     */
    public function updateCarStatus($car_ids, $status, $reason_report_broken = null)
    {
        try {
            $this->allQuery()->whereIn("id", $car_ids)->update(["status" => $status, "reason_report_broken" => $reason_report_broken]);
            return true;
        } catch (\Exception $exception) {
            Log::error($exception);
            return [];
        }
    }

    /**
     * @param $id
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function findById($id)
    {
        return $this->allQuery()->find($id);
    }

    /**
     * @param $request
     * @param $id
     */
    public function store($request, $id)
    {
        $this->updateOrCreate($request->all(), $id);
    }

    /**
     * search car
     *
     * @param $request
     * @return Builder[]|Collection
     */
    public function search($request)
    {
        return $this->all(['name' => $request->keyword]);
    }


}
