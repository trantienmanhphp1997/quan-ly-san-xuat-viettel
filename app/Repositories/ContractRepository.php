<?php


namespace App\Repositories;

use App\Models\Contract;
use App\Models\Statement;

class ContractRepository extends BaseRepository
{
    protected $fieldSearchable = [
        "code", "title",
    ];

    protected $quickSearchField = [ "code", "title", "department->departments.name"];

    protected $fieldFilterable = [
        "code", "title",
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Contract::class;
    }

    protected function expandQuery($baseQuery, $search)
    {
        return $baseQuery->with(["department", "statement"])->orderBy("created_at", "desc");
    }
}
