<?php


namespace App\Repositories;

use App\Models\Department;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class DepartmentRepository extends BaseRepository
{
    protected $fieldSearchable = ['name', 'code'];
    protected $quickSearchField = ["name", "description"];
    protected $fieldFilterable = [];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Department::class;
    }

    public function expandQuery($baseQuery, $search)
    {
        return $baseQuery->with(["parent", "children", "roles"]);
    }

    /**
     * Find model record for given id, with relationships
     *
     * @param int $id
     * @param array $relationships
     * @param array $columns
     *
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function findWith($id, $relationships, $columns = ['*'])
    {
        $query = $this->model->newQuery();

        return $query->with($relationships)->findOrFail($id, $columns);
    }

    public function findByCode($departmentCode)
    {
        return Department::firstWhere('code', $departmentCode);
    }

    public function getIncludeChild($departmentCode)
    {
        return $this->allQuery()->where("code", "like", "%$departmentCode%")->get();
    }
}
