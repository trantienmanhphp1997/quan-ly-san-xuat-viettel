<?php


namespace App\Repositories;

use App\Models\DeviceCategory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class DeviceCategoryRepository extends BaseRepository
{
    protected $quickSearchField = ['name', 'code', 'display_code'];
    protected $fieldFilterable = ['status'];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DeviceCategory::class;
    }

    /**
     * @param $baseQuery
     * @param $search
     * @return mixed
     */
    protected function expandQuery($baseQuery, $search)
    {
        return $baseQuery->orderBy("created_at", "desc");
    }

    /**
     * @return Builder[]|Collection
     */
    public function getParentCategories()
    {
        return $this->allQuery()->where('parent_id', '=', 0)->get();
    }
}
