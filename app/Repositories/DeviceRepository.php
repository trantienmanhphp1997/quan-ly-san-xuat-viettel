<?php


namespace App\Repositories;

use App\Models\Device;
use App\Models\DeviceCategory;
use App\Models\RecordPlan;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class DeviceRepository extends BaseRepository
{
    protected $fieldSearchable = ['name', 'status'];
    protected $quickSearchField = ['name', 'status', 'serial', 'device_category_code', 'category->device_categories.name'];
    protected $fieldFilterable = ['status'];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Device::class;
    }

    /**
     * @param $baseQuery
     * @param $search
     * @return mixed
     */
    protected function expandQuery($baseQuery, $search)
    {
        if (isset($search['borrowed']) && $search['borrowed'] == "borrowed")
            $baseQuery->whereHas('lendingPlans');
        if (isset($search['borrowed']) && $search['borrowed'] == "instock")
            $baseQuery->whereDoesntHave('lendingPlans');
        if (isset($search['requestReturn']) && $search['requestReturn'] == "requestReturn")
            $baseQuery->whereHas('requestReturn')->with('requestReturn');
        if (isset($search['device_category_id']))
            // dd($search['device_category_id']);
            $baseQuery->whereIn('device_category_id',explode(",",$search['device_category_id']));

        return $baseQuery->with(["category"])->orderBy("created_at", "desc");
    }

    /**
     * @param array $search
     * @return Builder[]|Collection
     */
    public function getDevices($search = [])
    {
        return $this->allQuery($search)->get();
    }

    /**
     * Xác nhận mượn 1 thiết bị
     *
     * @param $record_plan_id
     * @param $device_id
     */
    public function confirmBorrowOne($record_plan_id, $device_id)
    {
        $recordPlan = RecordPlan::find($record_plan_id);
        $device = $this->find($device_id);

        $device->recordPlans()->sync([$record_plan_id => [
            'status' => config("common.status.propose_device.delivered"),
            'start_time' => $recordPlan->start_time,
            'end_time' => $recordPlan->end_time]
        ]);
    }

    /**
     * xác nhận trả thiết bị
     *
     * @param $request
     */
    public function confirmReturnOne($request)
    {
        $device = $this->find($request->device_id);
        // thay đổi status của bảng trung gian record_plan_device_xreft
        $device->recordPlans()->sync([$request->record_plan_id => [
            'status' => config("common.status.propose_device.returned")
        ]]);
    }

    /**
     * @param $request
     */
    public function reportBroken($request)
    {
        $device = $this->find($request->device_id);
        $device->status = config('common.status.device.broken');
        $device->save();
    }

    /**
     * @param $id
     * @return Builder|Builder[]|Collection|Model
     */
    public function findById($id)
    {
        return $this->findOrFail($id);
    }

    /**
     * khôi phục thiết bị hỏng về trạng thái sẵn sàng
     *
     * @param $request
     */
    public function restoreDevice($request)
    {
        $device = $this->find($request->device_id);
        $device->status = config("common.status.device.active");
        $device->save();
    }

    /**
     * lấy các thiết bị có sẵn sàng cho mượn
     *
     * @param $assigned_device_ids
     * @return Builder[]|Collection
     */
    public function getAvailableDevices($assigned_device_ids, $keyword = null, $category = null)
    {
        $condition = [];
        if(!empty($category)){
            $condition = array_merge($condition,["device_category_id"=>$category]);
        };
        return $this->allQuery(["status" => config("common.status.device.active"), "s" => $keyword,])->where($condition)
            ->whereNotIn("id", $assigned_device_ids)->get();
    }

    /**
     * Cập nhật trạng thái thiết bị
     *
     * @param $device_ids
     * @param $status
     * @return bool
     */
    public function updateDeviceStatus($device_ids, $status, $reason_report_broken = null)
    {
        try {
            $this->allQuery()->whereIn("id", $device_ids)->update(["status" => $status, "reason_report_broken" => $reason_report_broken]);
            return true;
        } catch (\Exception $exception) {
            Log::error($exception);
            return [];
        }
    }

    /**
     * @param array $request
     * @return Model|void
     */
    public function updateOrCreateDevice($request, $id)
    {
        $device = $request->except('category');
        $device = $device;
        $device_catetory = DeviceCategory::findOrFail($request->category);
        $device['device_category_code'] = $device_catetory->code;
        $device['device_category_id'] = $device_catetory->id;
        $device['quantity'] = 1;
        $this->updateOrCreate($device, $id);
    }
}
