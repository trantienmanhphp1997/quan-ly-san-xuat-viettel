<?php


namespace App\Repositories;

use App\Models\ExampleNote;

class ExampleNoteRepository extends BaseRepository
{
    protected $fieldSearchable = [

    ];

    protected $quickSearchField = ["name"];

    protected $fieldFilterable = [
        "name", "type", "content"
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ExampleNote::class;
    }

//    public function findNoteByRecordPlanIdAndDepartmentCode($record_plan_id, $department_code){
//        return $this->allQuery()->where(["record_plan_id" => $record_plan_id, "from" => $department_code])->first();
//    }

}
