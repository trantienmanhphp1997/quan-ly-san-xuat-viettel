<?php


namespace App\Repositories;

use App\Models\Group;

class GroupRepository extends BaseRepository
{
    protected $fieldSearchable = ['name'];
    protected $quickSearchField = ['name', 'code'];
    protected $fieldFilterable = ['id'];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Group::class;
    }
    protected function expandQuery($baseQuery, $search)
    {
        return $baseQuery->orderBy("created_at", "desc");
    }
}
