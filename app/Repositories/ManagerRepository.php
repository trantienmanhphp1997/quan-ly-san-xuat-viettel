<?php


namespace App\Repositories;

use App\Models\User;

class ManagerRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name'
    ];

    protected $quickSearchField = ['username', 'full_name', 'email', 'phone_number', 'department_code'];

    protected $fieldFilterable = [
        'id', "department_code", "uuid", "phone_number"
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    protected function expandQuery($baseQuery, $search)
    {
//        return $baseQuery->where(["is_manager" => 1, ["department_code", "!=", config("common.groups.admin.code")]])->orderBy("created_at", "desc");
        return $baseQuery->where(["is_manager" => 1])->orderBy("created_at", "desc");
    }
}
