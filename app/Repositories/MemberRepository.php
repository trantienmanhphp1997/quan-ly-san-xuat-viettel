<?php


namespace App\Repositories;

use App\Models\User;
use Illuminate\Container\Container as Application;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class MemberRepository extends BaseRepository
{
    public $recordPlanRepository;
    public $recordPlanMemberXrefRepository;
    protected $fieldSearchable = ['name', 'department_code'];
    protected $quickSearchField = ['full_name', 'email', 'phone_number', "department->departments.name"];
    protected $fieldFilterable = [
        'id', "uuid", "phone_number", "recieve_notice_phone_number"
    ];

    public function __construct(Application $app, RecordPlanRepository $recordPlanRepository, RecordPlanMemberXrefRepository $recordPlanMemberXrefRepository)
    {
        parent::__construct($app);
        $this->recordPlanRepository = $recordPlanRepository;
        $this->recordPlanMemberXrefRepository = $recordPlanMemberXrefRepository;

    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    /**
     * @param $baseQuery
     * @param $search
     * @return mixed
     */
    public function handleDepartmentCode($department_code)
    {
        if (!auth()->user()) {
            return $department_code;
        }
        if ($department_code == "production") {
            if (auth()->user()->hasRole("super admin")) {
                return config("common.departments.production");
            } else {
                return auth()->user()->department_code;
            }
        } else {
            return $department_code;
        }
    }

    public function getMemberByIdAndDepartmentCode($department_code, $assigned_member_ids, $relationship = [], $query = [])
    {
        if($department_code == "camera" || $department_code == "technical_camera")
        {
            $query = $this->allQuery(array_merge(["id" => $assigned_member_ids], $query))->where("department_code" , config("common.departments.$department_code"));
        }
        else
        {
            $query = $this->allQuery(array_merge(["id" => $assigned_member_ids, "department_code" => config("common.departments.$department_code")], $query));
        }

        if (count($relationship) > 0) {
            $query->with($relationship);
        }
        return $query->get();
    }

    public function getAvailableMembers($departmentCode, $assigned_member_ids, $query = [])
    {

        $query["department_code"] = $departmentCode.'%';
        $data = $this->allQuery($query)
            ->where(["status" => 1])
            ->whereNotIn("id", $assigned_member_ids);

        if ($departmentCode == config("common.departments.transportation")) {
            $data = $data->with(["car"])->whereHas("car", function ($query) {
                $query->where(["cars.status" => 1]);
            });
        }
        return $data->get();
    }

    public function getMemberIdsByDepartmentCode($department_code)
    {
        if ($department_code == "production") {
            if (auth()->user()->hasRole("super admin")) {
                $department_code = config("common.departments.production");
            } else {
                $department_code = auth()->user()->department_code;
            }
        }
        return $this->allQuery(["department_code" => $department_code])->pluck("id");
    }

    public function getDriversByCarIdsAndStatus($car_ids, $status)
    {
        return $this->allQuery(["status" => $status])->where(["department_code" => config("common.departments.transportation")])
            ->whereHas("car", function ($query) use ($car_ids) {
                $query->whereIn("cars.id", $car_ids);
            })->get();
    }

    /**
     * List danh sách member theo status và mã phòng ban
     *
     * @param $status
     * @param $department_code
     * @param string $search
     * @return Builder[]|Collection
     */
    public function getMembersByDepartmentCodeAndStatus($status, $department_code, $search = '')
    {
        $_query = $this->allQuery(["status" => $status])->where(function ($query) use ($department_code, $search) {
            $query->where([["department_code", "=", $department_code], ["full_name", "like", "%$search%"]]);
            $query->orWhere([["department_code", "=", $department_code], ["phone_number", "like", "%$search%"]]);
        });
        if ($department_code == config("common.departments.transportation")) {
            return $_query->with("car")->whereHas("car", function ($query) {
                $query->where("cars.status", 1);
            })->get();
        }
        return $_query->get();
    }

    /**
     *  Trả về member theo điều kiện
     *
     * @param $request
     * @return mixed
     */
    public function getMember($request)
    {
        $department_code = $request->department_code;
        $start_time = $request->start_time;
        $end_time = $request->start_time;
        $search = ['s' => $request->s];

        /**
         *lấy các member id đã được phân công kế hoạch
         */
        if ($this->isValue($start_time) && $this->isValue($end_time)) {
            $member_ids = $this->allQuery()
                ->with('record_plans')
                ->whereHas("record_plans", function (Builder $query) use ($start_time, $end_time) {
                    $query->where(
                        [
                            ["record_plan_member_xrefs.start_time", "<=", $start_time],
                            ["record_plan_member_xrefs.end_time", ">=", $end_time],
                        ]);
                })
                ->pluck("id");
        } else {
            $member_ids = [];
        }

        $members = $this->allQuery($request->all())->whereNotIn('id', $member_ids);
        /**
         * lấy các members theo điều kiện
         */
        if ($this->isValue($department_code)) {
            if (str_contains($department_code, 'production')) {
                /**
                 * Lấy members theo department code và child của department code đó
                 */
                $members = $members->where("department_code", "like", "%$department_code%")->get();
            } else if ($department_code == config("common.departments.camera") || $department_code == config("common.departments.technical_camera")) {
                $members = $members->where("department_code", "=", $department_code)->get();
            } else {
                $members = $members->get();
            }
        }

        return $members;
    }

    /**
     * @param $request
     * @return Builder|Model|object|null
     */
    public function findByUuid($request)
    {
        return $this->allQuery()->where('uuid', $request->uuid)->first();
    }

    /**
     * @param $request
     * @return Builder|Model|object|null
     */
    public function findById($request)
    {
        return $this->allQuery()->where('id', $request->id)->first();
    }

    /**
     * Find driver by id
     *
     * @param $id
     * @return Builder|Model|object|null
     */
    public function findDriverById($id)
    {
        return $this->allQuery()->where([["department_code", "=", config("common.departments.transportation")], ["id", "=", $id]])->with("car")->first();
    }

    protected function expandQuery($baseQuery, $search)
    {
        $department_code = auth()->user()->department_code;
        if (array_key_exists("department_code", $search)) {
            $department_code = $search["department_code"];
        }

        $userAdminIds = User::role('super admin')->pluck('id')->toArray();

        return auth()->user()->hasRole('super admin')
            ? $baseQuery->whereNotIn("id", $userAdminIds)->orderBy("created_at", "desc")
            : $baseQuery->whereNotIn("id", $userAdminIds)->orderBy("created_at", "desc");
            // : $baseQuery->whereNotIn("id", $userAdminIds)->where([["department_code", "like", "%$department_code%"]])->orderBy("created_at", "desc");

    }
}
