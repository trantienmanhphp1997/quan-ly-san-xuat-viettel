<?php


namespace App\Repositories;

use App\Models\Menu;
use Illuminate\Database\Eloquent\Builder;

class MenuRepository extends BaseRepository
{
    protected $fieldSearchable = [];

    protected $quickSearchField = [];

    protected $fieldFilterable = ["id"];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Menu::class;
    }

    /**
     * Find a list of menu based on an array of menu ids (optional),
     * with only access permissions
     * @param array $menuIds
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllMenuWithAccessPermissions(array $menuIds = [])
    {
        $query = $this->model
            ->newQuery()
            ->with([
                "parent",
                "children",
                "permissions" => function ($q) {
                    $q->where('name', 'like', 'access%')->get()->first();
                }
            ]);
        if (count($menuIds) !== 0) {
            $query->whereIn('id', $menuIds);
        }
        return $query->get();
    }

    public function getAllMenusWithPermissionsExceptAccess(array $menuIds = []) {
        $query = $this->model
            ->newQuery()
            ->with([
                "permissions" => function ($q) {
                    $q->where('name', 'not like', 'access%')->get();
                }
            ])
            ->where('menu_type', 'not like', 'dropdown');
        if (count($menuIds) !== 0) {
            $query->whereIn('id', $menuIds);
        }
        return $query->get();
    }
}
