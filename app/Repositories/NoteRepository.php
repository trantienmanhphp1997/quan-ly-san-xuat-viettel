<?php


namespace App\Repositories;

use App\Models\Note;

class NoteRepository extends BaseRepository
{
    protected $fieldSearchable = [

    ];

    protected $quickSearchField = ["content"];

    protected $fieldFilterable = [
        "record_plan_id", "from", "to", "status"
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Note::class;
    }

    public function findNoteByRecordPlanIdAndDepartmentCode($record_plan_id, $department_code){
        return $this->allQuery()->where(["record_plan_id" => $record_plan_id, "from" => $department_code])->first();
    }

}
