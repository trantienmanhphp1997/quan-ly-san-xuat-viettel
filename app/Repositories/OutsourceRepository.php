<?php


namespace App\Repositories;

use App\Models\Outsource;

class OutsourceRepository extends BaseRepository
{
    protected $fieldSearchable = ['name'];

    protected $quickSearchField = ["name", "full_name", "phone_number", "outsource_type->outsource_types.id"];

    protected $fieldFilterable = [ "status", "outsource_type_id"];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Outsource::class;
    }

    protected function expandQuery($baseQuery, $search)
    {
        return $baseQuery->with(["outsource_type"])->orderBy("created_by", "desc");
    }
}
