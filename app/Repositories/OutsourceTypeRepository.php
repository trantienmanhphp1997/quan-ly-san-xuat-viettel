<?php


namespace App\Repositories;

use App\Models\Car;
use App\Models\OutsourceType;

class OutsourceTypeRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name', "department_code"
    ];

    protected $quickSearchField = ["name", "code"];

    protected $fieldFilterable = [
        "name", "code", "status"
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OutsourceType::class;
    }

    protected function expandQuery($baseQuery, $search)
    {
        $department_code = auth()->user()->department_code;
        return auth()->user()->hasRole('super admin')
            ? $baseQuery->orderBy("created_at", "desc")
            : $baseQuery->where( [["department_code", "like", "%$department_code%"]])->orderBy("created_at", "desc");
    }
}
