<?php


namespace App\Repositories;

use Spatie\Permission\Models\Permission;

class PermissionRepository extends BaseRepository
{
    protected $fieldSearchable = ["name"];

    protected $quickSearchField = [];

    protected $fieldFilterable = [];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Permission::class;
    }
}
