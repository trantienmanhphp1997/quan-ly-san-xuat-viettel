<?php


namespace App\Repositories;

use App\Models\Car;
use App\Models\Outsource;
use App\Models\OutsourceType;
use App\Models\PlanInfo;

class PlanInfoRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name'
    ];

    protected $quickSearchField = ["code", "year", "title", "department->departments.name"];

    protected $fieldFilterable = [
        "full_name", "phone_number", "status"
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PlanInfo::class;
    }

    protected function expandQuery($baseQuery, $search)
    {
        return $baseQuery->with(["department"])->orderBy("created_at", "desc");
    }
    public function getDataByDepartmentId($department_id){
        return $this->allQuery()->where(["department_id" => $department_id])->get();
    }
}
