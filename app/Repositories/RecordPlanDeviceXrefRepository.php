<?php


namespace App\Repositories;

use App\Models\Device;
use App\Models\RecordPlanDeviceXref;
use Illuminate\Support\Facades\Log;

class RecordPlanDeviceXrefRepository extends BaseRepository
{
    protected $fieldSearchable = [

    ];
    protected $quickSearchField = [
        'record_plan->record_plans.name', 'device->devices.name', 'device->devices.serial'
    ];
    protected $fieldFilterable = [
        'record_plan_id', "device_id", "start_time", "end_time", "status",
    ];
    public $recordPlanRepository;


    /**
     * Configure the Model
     **/
    public function model()
    {
        return RecordPlanDeviceXref::class;
    }


    public function getColumnsByRecordPlanIdAndStatus($record_plan_id, $status = null, $column = "device_id")
    {
        if (is_null($status)) {
            return $this->allQuery(["record_plan_id" => $record_plan_id])->pluck($column);
        } else {
            return $this->allQuery(["record_plan_id" => $record_plan_id, "status" => $status])->pluck($column);
        }
    }

    public function geDataByRecordPlanIdAndDeviceIds($record_plan_id, $device_ids)
    {
        return $this->allQuery(["record_plan_id" => $record_plan_id, "device" => $device_ids])->get();
    }

    public function updateDataByStatus($device_ids, $record_plan_id, $status, $user_id = null)
    {
        if ($status == config('common.status.propose_device.delivered')) {
            return $this->allQuery(["device_id" => $device_ids, "record_plan_id" => $record_plan_id])->update(["status" => $status, "hold_by"=> $user_id]);
        }
        return $this->allQuery(["device_id" => $device_ids, "record_plan_id" => $record_plan_id])->update(["status" => $status]);
    }

    public function getDataByStatusAnDeviceIdsNoPaginate($device_ids, $status, $allParams)
    {
        return $this->handleDataByStatusAnDeviceIds($device_ids, $status, $allParams)->with(["device", "record_plan"])->get();
    }


    public function getDataByStatusAnDeviceIds($device_ids, $status, $allParams = [])
    {
        return $this->handleDataByStatusAnDeviceIds($device_ids, $status, $allParams)->paginate(10);
    }

    public function handleDataByStatusAnDeviceIds($device_ids, $status, $allParams)
    {
        $search = '';
        $condition = [];
        if (isset($allParams['s'])) {
            $search = $allParams['s'];
        }
        if (!empty($allParams['status']) && $allParams['status'] == 2) {
            array_push($condition, ['end_time','<', date('Y-m-d H:i:s',time())]);
        }
        else {
            array_push($condition, ['end_time','>=', date('Y-m-d H:i:s',time())]);
        }
        return $this->allQuery(["status" => $status, "device_id" => $device_ids, "s" => $search])->whereHas("record_plan", function ($query) {
            $query->whereNotIn("record_plans.status", [config("common.status.record_plan.cancel"),/* config("common.status.record_plan.inactive")*/]);
        })->where($condition);
    }

    public function paginateDevicesForRecordPlan($record_plan_id, $status, $pageName = "page")
    {
        return $this->allQuery()->with(["device", "record_plan","device.category"])->where([
            "record_plan_device_xrefs.record_plan_id" => $record_plan_id
        ])->whereIn("record_plan_device_xrefs.status", $status)->paginate(10, "*", $pageName);
    }

    public function getDevicesForRecordPlan($record_plan_id, $status, $extraCriteria = [])
    {
        return $this->allQuery($extraCriteria)->with(["device", "record_plan"])->where([
            "record_plan_device_xrefs.record_plan_id" => $record_plan_id
        ])->whereIn("record_plan_device_xrefs.status", $status)->get()->pluck("device");
    }

    public function getDataStartTimeAndEndTime($start_time, $end_time, $status)
    {
        try {
            return $this->allQuery(["status" => $status])->where([
                ["end_time", ">=", $start_time],
                ["start_time", "<=", $end_time],
            ])->get();
        } catch (\Exception $exception) {
            Log::error($exception);
            return [];
        }
    }

    public function getDataDeviceIdsAndRecordPlanStatus($device_ids, $record_plan_ids, $status)
    {
        return $this->allQuery(["record_plan_id" => $record_plan_ids])
            ->whereIn("device_id", $device_ids)
            ->whereHas("record_plan", function ($query) use ($status) {
                $query->whereIn("record_plans.status", $status);
            })
//            ->where([["end_time", ">", now()]])
            ->get();
    }

    public function getDuplicatedDataByDeviceIds($start_time, $end_time, $device_ids)
    {
        try {
            return $this->allQuery(["device_id" => $device_ids])->where([
                ["end_time", ">=", $start_time],
                ["start_time", "<=", $end_time],
            ])->pluck("device_id");

        } catch (\Exception $exception) {
            Log::error($exception);
            return [];
        }
    }

    public function getDataByRecordPlanIdAndStatus($record_plan_id, $status)
    {
        try {
            return $this->allQuery(["status" => $status])->where([
                "record_plan_id" => $record_plan_id,
            ])->get();
        } catch (\Exception $exception) {
            Log::error($exception);
            return [];
        }
    }

    /**
     * yêu cầu trả thiết bị từ wap
     *
     * @param $request
     */
    public function requestToReturnDevice($request)
    {
        $device = $this->allQuery(["device_id" => $request->device_id,"status"=>config("common.status.propose_device.delivered")])->first();
        $device->status = config("common.status.propose_device.pending_return");
        $device->save();
    }

}
