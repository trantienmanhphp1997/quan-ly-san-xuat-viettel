<?php


namespace App\Repositories;

use App\Http\Resources\RecordPlanResource;
use App\Models\Device;
use App\Models\RecordPlan;
use App\Models\RecordPlanDeviceXref;
use App\Models\RecordPlanMemberXref;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class RecordPlanRepository extends BaseRepository
{
    protected $fieldSearchable = ['name', "department_code", "created_by", "address_type"];
    protected $quickSearchField = ['name', 'address', 'address_type'];
    protected $fieldFilterable = ['id', 'status', "address_type", "reporter_allocate_status", "camera_allocate_status", "technical_camera_allocate_status", "transportation_allocate_status", "device_allocate_status","is_recurring", "excel_imported"];

    /**Configure the Model
     * @return string
     */
    public function model()
    {
        return RecordPlan::class;
    }

    /**
     * @param $status
     * @return Builder[]|Collection
     */
    public function getRecordPlansByStatus($status)
    {
        return $this->allQuery(["status" => $status])->get();
    }

    /**
     * Trả về lịch làm việc theo start time, end time và user
     * @param $request
     * @return mixed
     */
    public function assignmentList($request)
    {
        $user_id = $request->input('user_id');
        $start_time = $request->input('start_time');
        $end_time = $request->input('end_time');
        return $this->getAssignmentListByUserAndTime($start_time, $end_time, $user_id);
    }

    /**
     * Trả về lịch làm việc theo start time, end time và user
     * Nếu không truyền user, lấy assignment của tất cả user nằm trong phòng ban
     * @param $startTime
     * @param $endTime
     * @param $timeAssign
     * @param null $userId
     * @param $search
     * @return mixed
     */
    private function getAssignmentListByUserAndTime($startTime, $endTime, $timeAssign, $search, $userId = null)
    {
        $criteria = [];
        if ($this->isValue($startTime) && !$this->isValue($endTime)) {
            $criteria = [
                ["record_plan_member_xrefs.start_time", "<=", $startTime],
                ["record_plan_member_xrefs.end_time", ">=", $startTime]
            ];
        } else if (!$this->isValue($startTime) && $this->isValue($endTime)) {
            $criteria = [
                ["record_plan_member_xrefs.end_time", ">=", $endTime]
            ];
        } else if ($this->isValue($startTime) && $this->isValue($endTime)) {
            if ($timeAssign == "week") {
                $criteria = [
                    ["record_plan_member_xrefs.start_time", "<=", $endTime],
                    ["record_plan_member_xrefs.end_time", ">=", $startTime]
                ];
            } else {
                $criteria = [
                    ["record_plan_member_xrefs.start_time", "<=", $endTime],
                    ["record_plan_member_xrefs.end_time", ">=", $startTime]
                ];
            }
        }

        //@todo check quyền xem dữ liệu của userId theo loginUser
        //Lấy deparmnt code
        $departmentCode = auth("api")->user()->department_code;
        $userIds = User::where([["department_code", 'like', "%$departmentCode%"]])->pluck('id')->toArray();
        $users = [];
        if ($userId !== null)
            $users = [$userId];
        else $users = $userIds;

        $assignments = $this->allQuery($search)
            ->whereHas('members', function ($query) use ($criteria, $users) {
                $query->where($criteria)->whereIn('record_plan_member_xrefs.member_id', $users);
            })->whereNotIn("record_plans.status", [config("common.status.record_plan.cancel"), config("common.status.record_plan.pending")])->get();
        $loginUser = auth()->user();
        $driverStatus = RecordPlanMemberXref::where(["member_id" => $loginUser->id])->whereIn("record_plan_id", $assignments->pluck("id")->toArray())->get();
        $driverStatusMap = [];
        foreach ($driverStatus as $driver) {
            $driverStatusMap[$driver->record_plan_id] = $driver->transportation_status;
        }
        foreach ($assignments as $assignment) {
            $transportation_status = $driverStatusMap[$assignment->id];
            $assignment->{"transportation_status"} = $transportation_status;
        }

        return RecordPlanResource::collection($assignments)->all();
    }

    /**
     * Trả về lịch làm việc của login user theo start time và end time
     * @param $request
     * @return mixed
     */
    public function myAssignmentList($request)
    {
        $start_time = $request->input('start_time');
        $end_time = $request->input('end_time');
        $time_assign = $request->input('time_assign');
        $search = ['s' => $request->s];
        // $userIds = auth("api")->user()->is_manager == 0 ? auth('api')->id() : null;
        return $this->getAssignmentListByUserAndTime($start_time, $end_time, $time_assign, $search, auth('api')->id());
    }

    /**
     * @param string $departmentCode
     * @param string $status
     * @param array $search
     * @return mixed
     */
    public function getRecordPlans($departmentCode = "", $status = "", $search = [])
    {
        $criteria = [];

        if ($this->isValue($departmentCode)) {
            //Nếu là phòng ban hỗ trợ --> Xem được tất cả
            $supportedDepartMent = ["camera", "device", "transportation", "accounting"];
            if (!in_array($departmentCode, $supportedDepartMent))
                $criteria[] = ["department_code", "like", "%$departmentCode%"];
        }

        if ($this->isValue($status) || (int)($status) === 0 && $status != "undefined" && $status != null) {
            $criteria[] = ["status", "=", $status];
        }

        return $record_plans = $this->allQuery($search)->where($criteria)->orderBy("start_time", "desc")->get();
    }

    /**
     * @param $record_plan
     * @param $member_ids
     * @return bool
     */
    public function detachMembersByRecordPlan($record_plan, $member_ids)
    {
        try {
            $record_plan->members()->detach($member_ids);
            return true;
        } catch (Exception $exception) {
            Log::error($exception);
            return false;
        }
    }

    /**
     * @param $record_plan
     * @param $device_ids
     * @return bool
     */
    public function detachDevicesByRecordPlan($record_plan, $device_ids)
    {
        try {
            $record_plan->devices()->detach($device_ids);
            return true;
        } catch (Exception $exception) {
            Log::error($exception);
            return false;
        }

    }

    /**
     * Đề xuất thiết bị
     *
     * @param $request
     */
    public function suggestDevice($request)
    {
        if ($request->group_type == "device") {
            $data = $request->only('offer_device_note', 'offer_device_number');
            $this->update($data, $request->record_plan_id);
        }
    }

    /**
     * @param $field
     * @param $status
     * @param $record_plan_id
     * @return bool
     */
    public function updateRecordPlanStatus($field, $status, $record_plan_id)
    {
        try {
            $record_plan = $this->find($record_plan_id);
            $record_plan->update([$field => $status]);
            return true;
        } catch (Exception $exception) {
            Log::error($exception);
            return false;
        }

    }

    public function getDataByStatusAndDeviceIdsAndXrefStatus($status, $device_ids, $record_plan_device_status = [1, 2, 3])
    {
        return $this->allQuery()
            ->whereIn("status", $status)
            ->whereHas("devices", function ($query) use ($device_ids, $record_plan_device_status) {
                $query->whereIn("record_plan_device_xrefs.device_id", $device_ids)
                    ->whereIn("record_plan_device_xrefs.status", $record_plan_device_status);
            })->get();
    }

    public function getDataByStatusAndMemberIdsAndXrefStatus($status, $device_ids, $record_plan_member_status = [1, 2])
    {
        return $this->allQuery()
            ->whereIn("status", $status)
            ->whereHas("members", function ($query) use ($device_ids, $record_plan_member_status) {
                $query->whereIn("record_plan_member_xrefs.member_id", $device_ids)
                    ->whereIn("record_plan_member_xrefs.status", $record_plan_member_status);
            })->get();
    }

    /**
     * @param $request
     * @param $id
     */
    public function confirmBorrowAll($request, $id)
    {
        $recordPlan = $this->findOrFail($id);
        foreach ($request->all() as $data) {
            $device = Device::findOrFail($data['id']);
            // thay đổi status của bảng trung gian record_plan_device_xreft
            $device->recordPlans()->sync([$id => [
                'status' => config("common.status.record_plan_change_status.in_progress"),
                'start_time' => $recordPlan->start_time,
                'end_time' => $recordPlan->end_time,
            ]]);
        }
    }

    /**
     * @param $request
     * @param $id
     */
    public function confirmReturnAll($request, $id)
    {
        foreach ($request->all() as $data) {
            $device = Device::findOrFail($data['id']);
            // thay đổi status của bảng trung gian record_plan_device_xreft
            $device->recordPlans()->sync([$id => [
                'status' => config("common.status.record_plan_change_status.wrap"),
                'start_time' => null,
                'end_time' => null,
            ]]);
        }
    }

    /**
     * @param $request
     * @param $id
     * @return bool
     */
    public function checkReceivedDevice($request, $id)
    {
        $devices = $request->input();
        $record_plan_devices = RecordPlanDeviceXref::all();

        foreach ($record_plan_devices as $record_plan_device) {
            foreach ($devices as $device) {
                if ($device['id'] == $record_plan_device->device_id && $id == $record_plan_device->record_plan_id) {
                    if (
                        $record_plan_device->status == config("common.status.record_plan_change_status.pending") ||
                        $record_plan_device->status == config("common.status.record_plan_change_status.approved") ||
                        $record_plan_device->status == config("common.status.record_plan_change_status.wrap") ||
                        $record_plan_device->status == config("common.status.record_plan_change_status.cancel")
                    ) return false;
                }
            }
        }
        return true;
    }

    public function getDataHasDeliveredDevices($criteria = [])
    {
        return $this->allQuery($criteria)->whereHas("devices", function ($query) {
            $query->whereIn("record_plan_device_xrefs.status", [config("common.status.propose_device.delivered")]);
        })->get();
    }

    public function getProposalsByDepartmentCode($departmentCode, $query, $allocate_status = '')
    {
        date_default_timezone_set("Asia/Bangkok");
        $DateFilter = [];
        $generalStatus = [
            config("common.status.record_plan.pending"),
            config("common.status.record_plan.approved"),
            config("common.status.record_plan.in_progress"),
            config("common.status.record_plan.wrap"),
            config("common.status.record_plan.done")
        ];
        $query['status'] = $generalStatus;
        $group_allocate_status = $departmentCode . "_allocate_status";
        if (!empty($allocate_status)) {
            $query[$group_allocate_status] = $allocate_status;
        }

        $offer_group_number = "offer_$departmentCode" . "_number";
        $offer_group_note = "offer_$departmentCode" . "_note";
        $position = "";
        if ($departmentCode == "reporter") {
            if (auth()->user()->hasRole("super admin")) {
                $position = config("common.departments.production");
            } else {
                $position = auth()->user()->department_code;
            }
        }

        if (!empty($query["record_plan_id"])) {
            $query["id"] = $query["record_plan_id"];
        }
        if ((!empty($query["address_type"]) && $query["address_type"] == "all")) {
            unset($query["address_type"]);
        }
        $data = $this->allQuery($query);
        if (!empty($query["time_filter"])) {
            switch ($query["time_filter"]) {
                case 1:
                    array_push($DateFilter, ["start_time", '>=', date("Y:m:d 0:0:0", strtotime(date('Y-m-d 0:0:0')))]);
                    array_push($DateFilter, ["start_time", '<=', date("Y:m:d 23:59:59", strtotime(date('Y-m-d 23:59:59')))]);
                    break;

                case 2:
                    array_push($DateFilter, ["start_time", '>=', date("Y:m:d 0:0:0", strtotime(date('Y-m-d 0:0:0') . ' +1 days'))]);
                    array_push($DateFilter, ["start_time", '<=', date("Y:m:d 23:59:59", strtotime(date('Y-m-d 23:59:59') . ' +1 days'))]);
                    break;
                default:
                    break;
            }
        }
        if ($position) {
            $data = $data->where([["department_code", "like", "%$position%"]]);
        }
        // với tab chưa phân công (allocate_status = 1) chỉ list những lịch còn hạn phân bổ (issue VQLSX-625)
        if ($allocate_status == "1") {
            $data = $data->whereDate("end_time", ">=", Carbon::today()->clone()->startOfDay());
        }

        $data = $data->where(function ($query) use ($offer_group_number, $offer_group_note) {
            $query->where([[$offer_group_number, ">=", 0]])->orWhereNotNull($offer_group_note);
        })->where($DateFilter)
            ->orderByRaw("
                CASE WHEN DATEDIFF(start_time, CURRENT_TIME()) = 0 THEN DATEDIFF(start_time, CURRENT_TIME()) END DESC,
                CASE WHEN DATEDIFF(start_time, CURRENT_TIME()) > 0 THEN 1 END DESC,
                CASE WHEN DATEDIFF(start_time, CURRENT_TIME()) > 0 THEN DATEDIFF(start_time, CURRENT_TIME()) END ASC,
                DATEDIFF(start_time, CURRENT_TIME()) DESC,
                CASE
                    WHEN  status = ? THEN 1
                    WHEN  status = ? THEN 2
                    WHEN  status = ? THEN 3
                    WHEN  status = ? THEN 4
                    WHEN  status = ? THEN 5
                ELSE 6 END",
                [
                    config('common.status.record_plan.pending'),
                    config('common.status.record_plan.approved'),
                    config('common.status.record_plan.in_progress'),
                    config('common.status.record_plan.wrap'),
                    config('common.status.record_plan.done'),
                ]);

        return $data;
    }

    /**
     * @param $baseQuery
     * @param $search
     * @return mixed
     */
    protected function expandQuery($baseQuery, $search)
    {
        return $baseQuery->with(["members", "devices", "plan_category", "creator"]);
    }
}
