<?php


namespace App\Repositories;

use App\Models\CustomRole;

class RoleRepository extends BaseRepository
{
    protected $fieldSearchable = ["name"];

    protected $quickSearchField = [];

    protected $fieldFilterable = [];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomRole::class;
    }

    /**
     * Get a collection of roles, and what deparments having those roles, and what permissions those roles have
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllDepartmentRoles()
    {
        $query = $this->model->newQuery();
        return $query->with(["departments", "permissions"])->get();
    }
}
