<?php


namespace App\Repositories;

use App\Models\Stage;

class StageRepository extends BaseRepository
{
    protected $fieldSearchable = [];

    protected $quickSearchField = ["name", "address",];

    protected $fieldFilterable = [
        "type", "code", "status",
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Stage::class;
    }

     /**
     * @param $baseQuery
     * @param $search
     * @return mixed
     */
    protected function expandQuery($baseQuery, $search)
    {
        return $baseQuery->orderBy("created_at", "desc");
    }
}
