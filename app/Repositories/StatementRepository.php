<?php


namespace App\Repositories;

use App\Models\Statement;

class StatementRepository extends BaseRepository
{
    protected $fieldSearchable = [
        "code", "title",
    ];

    protected $quickSearchField = ["code", "title", "department->departments.name"];

    protected $fieldFilterable = [
        "code", "title",
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Statement::class;
    }

    protected function expandQuery($baseQuery, $search)
    {
        return $baseQuery->with(["department"])->orderBy("created_at", "desc");
    }

    public function getDataByDepartmentId($department_id, $status = 1)
    {
        return $this->allQuery()->where(["department_id" => $department_id, "status" => $status])->get();
    }
}
