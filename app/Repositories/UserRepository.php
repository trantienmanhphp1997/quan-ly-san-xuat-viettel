<?php


namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name'
    ];

    protected $quickSearchField = ['username', 'full_name', 'email', 'phone_number', 'department_code'];

    protected $fieldFilterable = [
        'id', "department_code", "uuid", "phone_number", "recieve_notice_phone_number"
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    protected function expandQuery($baseQuery, $search)
    {
        return $baseQuery->orderBy("created_at", "desc");
    }
    //    public function expandQuery($baseQuery, $search)
//    {
//        return auth()->user()->department_code == "ADMIN" ? $baseQuery : $baseQuery->where([
//            "department_code" => auth()->user()->department_code
//        ])->orderBy("created_at", "desc");
//    }
}
