<?php


namespace App\ResponseHandler;


class HandleResponse
{
    private $_code;
    private $_status;
    private $_error;
    private $_data;
    private $_path;

    public static function builder($path){
        $ins = new HandleResponse();
        $ins->_path = $path;
        $ins->_status = 200;
        return $ins;
    }
    public function code($value){
        $this->_code = $value;
        return $this;
    }
    public function status($value){
        $this->_status = $value;
        return $this;
    }
    public function error($value){
        $this->_error = $value;
        return $this;
    }
    public function data($value){
        $this->_data = $value;
        return $this;
    }
    public function path($value){
        $this->_path = $value;
        return $this;
    }
    public function build(){
        return self::handle($this->_code,$this->_status,$this->_error,$this->_data,$this->_path);
    }


    public static function handle($code, $status, $error = null, $data = null, $path = null)
    {
        if ($data === null && $error !== null) {
            return response()->json([
                "code" => $code,
                "status" => $status,
                "error" => $error,
                "path" => $path,
                "timestamp" => now()
            ], $status);
        } else if ($data !== null && $error === null) {
            return response()->json([
                "code" => $code,
                "status" => $status,
                "data" => $data,
                "path" => $path,
                "timestamp" => now()
            ], $status);
        } else if($data === null && $error === null){
            return response()->json([
                "code" => $code,
                "status" => $status,
                "path" => $path,
                "timestamp" => now()
            ], $status);
        }
        return response()->json([
            "code" => $code,
            "status" => $status,
            "error" => $error,
            "data" => $data,
            "path" => $path,
            "timestamp" => now()
        ], $status);
    }

}
