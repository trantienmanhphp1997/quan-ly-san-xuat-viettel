<?php

namespace App\Services\API;

use App\Repositories\DeviceRepository;
use App\Repositories\RecordPlanRepository;

class AssignmentService
{
    public $deviceRepository;
    public $recordPlanRepository;

    public function __construct(DeviceRepository $deviceRepository, RecordPlanRepository $recordPlanRepository)
    {
        $this->deviceRepository = $deviceRepository;
        $this->recordPlanRepository = $recordPlanRepository;
    }

    public function getSuggestByDepartmentCode($department_code, $query = [], $allocate_status = null)
    {
        $general_status = [
            config("common.status.record_plan.pending"),
            config("common.status.record_plan.approved"),
            config("common.status.record_plan.in_progress"),
            config("common.status.record_plan.wrap"),
            config("common.status.record_plan.done")
        ];
        if (empty($allocate_status)) {
            $allocate_status = config('common.status.proposals.unassigned');
        }
        $group_allocate_status = $department_code . "_allocate_status";
        $offer_group_number = "offer_$department_code" . "_number";
        $data = $this->recordPlanRepository->allQuery($query)->where([
            $group_allocate_status => $allocate_status,
        ])->whereIn("status", $general_status);

        if (!in_array($department_code, [
            config("common.departments.production"),
            config("common.departments.general_news"),
            config("common.departments.news"),
            config("common.departments.documentary"),
            config("common.departments.entertainment"),
            config("common.departments.music"),
            config("common.departments.film"),
        ])) {
            return $data = $data->where([[$offer_group_number, ">", 0]])->get();
        } else {
            return $data = $data->get();
        }
    }
}
