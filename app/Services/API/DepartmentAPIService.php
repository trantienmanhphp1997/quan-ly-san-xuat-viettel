<?php

namespace App\Services\API;

use App\Repositories\DepartmentRepository;
use App\Repositories\RecordPlanRepository;

class DepartmentAPIService
{
    public $departmentRepository;
    public $recordPlanRepository;

    public function __construct(DepartmentRepository $departmentRepository, RecordPlanRepository $recordPlanRepository)
    {
        $this->departmentRepository = $departmentRepository;
        $this->recordPlanRepository = $recordPlanRepository;
    }

    /**
     *  tìm department_code
     *
     * @param $request
     * @return mixed
     */
    public function findCodeByRecordPlanId($request)
    {
        $department_code = "";
        if ($this->departmentRepository->isValue($request->record_plan_id)) {
            $record_plan = $this->recordPlanRepository->findOrFail($request->record_plan_id);
            $department_code = $record_plan->department_code;
        }
        return $department_code;
    }
}
