<?php

namespace App\Services\API;

use App\Repositories\RecordPlanDeviceXrefRepository;
use App\Repositories\RecordPlanRepository;

class RecordPlanAPIService
{
    public $recordPlanRepository;
    public $recordPlanDeviceXrefRepository;

    public function __construct(
        RecordPlanRepository $recordPlanRepository,
        RecordPlanDeviceXrefRepository $recordPlanDeviceXrefRepository
    )
    {
        $this->recordPlanRepository = $recordPlanRepository;
        $this->recordPlanDeviceXrefRepository = $recordPlanDeviceXrefRepository;
    }

    /**
     * @param $request
     */
    public function handleSuggestMember($request)
    {
        // lưu đề xuất thiết bị vào bảng record plan
        $recordPlan = $this->recordPlanRepository->find($request->record_plan_id);
        if ($request->group_type == "reporter") {
            $recordPlan->offer_reporter_note = $request->offer_note;
            $recordPlan->offer_reporter_number = $request->offer_number;
        }
        if ($request->group_type == "camera") {
            $recordPlan->offer_camera_note = $request->offer_note;
            $recordPlan->offer_camera_number = $request->offer_number;
        }
        if ($request->group_type == "transportation") {
            $recordPlan->offer_transportation_note = $request->offer_note;
            $recordPlan->offer_transportation_number = $request->offer_number;
        }
        $recordPlan->save();
    }

    public function getRecordPlanBorrowingDevices($criteria = [])
    {
        $recordPlanIds = $this->recordPlanDeviceXrefRepository->allQuery(
            ['status' => config('common.status.propose_device.delivered')]
        )->pluck('record_plan_id');
        $recordPlans = $this->recordPlanRepository->allQuery($criteria)->whereIn('id', $recordPlanIds)->get();
        return $recordPlans;
    }

    public function handleStatusCode($id)
    {
        $code = [];
        $code['success'] = config('api_message.record_plan.create_success');
        $code['failed'] = config('api_message.record_plan.create_failed');
        if ($this->recordPlanRepository->isValue($id)) {
            $code['success'] = config('api_message.record_plan.update_success');
            $code['failed'] = config('api_message.record_plan.update_failed');
        }
        return $code;
    }

}
