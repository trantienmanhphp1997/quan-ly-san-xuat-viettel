<?php


namespace App\Services;


use App\Exceptions\ValidateException;
use App\Http\Controllers\Admin\Controller;
use App\Models\User;
use App\Repositories\MemberRepository;
use App\Repositories\NoteRepository;
use App\Repositories\RecordPlanDeviceXrefRepository;
use App\Repositories\RecordPlanMemberXrefRepository;
use App\Repositories\RecordPlanRepository;
use App\Services\Interfaces\IAssignmentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use function Matrix\trace;

class AssignmentService implements IAssignmentService
{
    public $recordPlanMemberXrefRepository;
    public $recordPlanDeviceXrefRepository;
    public $recordPlanRepository;
    public $controller;
    public $noteRepository;
    public $memberRepository;
    public $recordPlanService;

    public function __construct(
        RecordPlanMemberXrefRepository $recordPlanMemberXrefRepository,
        RecordPlanDeviceXrefRepository $recordPlanDeviceXrefRepository,
        RecordPlanRepository $recordPlanRepository,
        Controller $controller,
        NoteRepository $noteRepository,
        MemberRepository $memberRepository,
        RecordPlanService $recordPlanService
    )
    {
        $this->recordPlanMemberXrefRepository = $recordPlanMemberXrefRepository;
        $this->recordPlanDeviceXrefRepository = $recordPlanDeviceXrefRepository;
        $this->recordPlanRepository = $recordPlanRepository;
        $this->controller = $controller;
        $this->noteRepository = $noteRepository;
        $this->memberRepository = $memberRepository;
        $this->recordPlanService = $recordPlanService;
    }

    public function updateDeviceStatus($record_plan_id, $device_ids, $status, $user_id = null)
    {
        if ($status == config("common.status.propose_device.delivered")) {
            $rs = $this->checkCanDeliverDevicesToRecordPlan($record_plan_id, $device_ids);
            $result = $rs["result"];
            if (!$result) {
                return [
                    "result" => false,
                    "message" => $rs["message"]
                ];
            }
        }
        $this->recordPlanDeviceXrefRepository->updateDataByStatus($device_ids, $record_plan_id, $status, $user_id);
        return [
            "result" => true,
            "message" => ""
        ];

    }

    public function checkCanDeliverDevicesToRecordPlan($record_plan_id, $device_ids)
    {
        $record_plan = $this->recordPlanRepository->find($record_plan_id);

        $devices_not_returned = $this->recordPlanDeviceXrefRepository->allQuery(["device_id" => $device_ids])
            ->where([
                ["end_time", "<", $record_plan->start_time],
                ["status", "=", config("common.status.propose_device.delivered")]
            ])->get();
        if (count($devices_not_returned)) {
            return [
                "result" => false,
                "message" => __("notification.assignment.fail.deliver_device", ["record_plan_name" => $record_plan->name])
            ];
        }
        return [
            "result" => true,
            "message" => ""
        ];
    }

    public function deleteMembersByRecordPlanId($recordPlanId, $memberIds){
        //chỉ xóa nhân sự của phòng ban mình phân công
        $userDepartmentCode = auth()->user()->department_code;

        if(str_contains($userDepartmentCode, "production")){
            $departmentCode = config("common.departments.production");
        }else{
            $departmentCode = $userDepartmentCode;
        }
        $assignments = $this->recordPlanMemberXrefRepository->allQuery(["record_plan_id" => $recordPlanId])->whereHas("member", function ($query) use ($departmentCode){
            $query->where(["users.department_code" => $departmentCode]);
        });

        $assignedMemberIds = $assignments->pluck("member_id")->toArray();

        //gửi tin nhắn thông báo đến các nhân sự đã từng được phân công,
        if (in_array($departmentCode, [config("common.departments.transportation"), config("common.departments.camera"), config("common.departments.technical_camera")])) {
            $memberIds = array_map(function ($memberId) {
                return isset($memberId["memberId"]) ? $memberId["memberId"] : $memberId;
            }, $memberIds);
        }
        // những member trong assignedMembers nhưng k ở trong memberIds => loại khỏi phân công
        $removedMemberIds = array_filter($assignedMemberIds, function ($assignedId) use ($memberIds) {
            return !in_array($assignedId, $memberIds);
        });

        $assignments->delete();

        $removedMembers = User::whereIn("department_code", [$departmentCode])->whereIn("id", $removedMemberIds)->where([ "status" => 1])->whereNotNull("phone_number")->get();
        $label = "Xem chi tiết";
        $deeplink = "mocha://survey?ref=".urlencode(config("mocha.wap_url")."/record-plans/detail/$recordPlanId?record-plan-id=$recordPlanId");
        $oa_id = config("mocha.oa_id");
        $recordPlan = $this->recordPlanRepository->find($recordPlanId);
        MochaService::sendMessageToUser($oa_id,
            'Quản lý vừa huỷ phân công bạn trong lịch sản xuất' ."\n"
            . 'Lịch sản xuất: ' . $recordPlan->name . "\n"
            . 'Khởi hành: ' . $this->formateDate($recordPlan->start_time) ."\n"
            . 'Kết thúc: ' . $this->formateDate($recordPlan->end_time) ."\n"
            , $removedMembers, $label, $deeplink);

        return true ;
    }


    public function assignMembersToRecordPlan($recordPlanId, $departmentCode, $memberIds)
    {
        if (blank($memberIds)) {
            throw new ValidateException(__("notification.common.fail.no-data-selected"));
        }
        $record_plan = $this->recordPlanRepository->find($recordPlanId);
        if (!$record_plan)
            return true;
        //@todo kiểm tra xem lịch có còn trong thời gian được phân công thêm người hay k
        $proposalNumber = $record_plan["offer_" . $departmentCode . "_number"];

        if ($departmentCode == "reporter") {
            if (auth()->user()) {
                if (auth()->user()->hasRole("super admin")) {
                    $departmentCode = config("common.departments.production");
                } else {
                    $departmentCode = auth()->user()->department_code;
                }
            }else {
                $departmentCode = $record_plan->department_code;
            }
        }

        $assignedMember = $this->recordPlanMemberXrefRepository->allQuery(["record_plan_id" => $recordPlanId, "status" => config("common.status.proposals.assigned")])
            ->whereHas("member", function ($query) use ($departmentCode) {
                $query->where([["users.department_code", "like", "$departmentCode%"]]);
            })
            ->count();

        // kiểm tra số lượng member đã được assign so với số lượng đề xuất ngoại trừ đối với reporter và driver, camera
        if (!in_array($departmentCode, ["technical_camera", config("common.departments.transportation"), config("common.departments.camera")]) && !str_contains($departmentCode, "production")
        ) {
            if ((count($memberIds) + $assignedMember > $proposalNumber)) {
                throw new ValidateException(__("notification.assignment.fail.exceed", ["proposalNumber" => $proposalNumber, "department" => __('data_field_name.model_name.' . $departmentCode)]));
            }
        }

        // Xử lý phân bổ lái xe, quay phim (thêm loại hình thức tham gia đối vói lái xe :...)
        if (in_array($departmentCode, [config("common.departments.transportation"), config("common.departments.camera"), config("common.departments.technical_camera")])) {
            $member_ids = [];
            foreach ($memberIds as $memberInfo) {
                $member_ids[$memberInfo["memberId"]] = [
                    "record_plan_id" => $record_plan->id,
                    "member_id" => $memberInfo["memberId"],
                    "status" => 2,
                    "start_time" => $record_plan->start_time,
                    "end_time" => $record_plan->end_time,
                    "send_message_type" => config("common.sendMessageType.invite"),
                    "record_plan_status" => $record_plan->status,
                    "involve_type" => ($departmentCode == config("common.departments.transportation") ? $memberInfo["involveType"] : null)
                ];
            }

        }
        else {
            $member_ids = $this->controller->addOtherInfoToMembers($record_plan, $memberIds, 2);
        }

        $record_plan->members()->syncWithoutDetaching($member_ids);

        //Gửi tin nhắn đến nhân sự được phân công vào lịch
        $this->sendMessageToAssignedMembers($recordPlanId, $memberIds);
        return true;
    }

    private function formateDate($inputDate) {
        $date = new \DateTime($inputDate);
        return $date->format('H:i d/m/Y');
    }

    public function sendMessageToAssignedMembers($recordPlanId, $memberIds, $departmentCode = null){
        $recordPlan = $this->recordPlanRepository->find($recordPlanId);
        $label = "Xem chi tiết";
        $deeplink = "mocha://survey?ref=".urlencode(config("mocha.wap_url")."/record-plans/detail/$recordPlanId?record-plan-id=$recordPlanId");
        $oa_id = config("mocha.oa_id");

        $members = [];
        foreach ($memberIds as $memberId){
            Log::info("type of", [ gettype($memberId) ]);
            if(is_array($memberId)){
                $members[] = $memberId["memberId"];
            }else{
                $members[] = $memberId;
            }
        }

        $phoneNumbers = User::whereIn("id", $members)->get();
//        dd($phoneNumbers);
        MochaService::sendMessageToUser($oa_id,
            'Bạn vừa được phân công vào lịch sản xuất' ."\n"
            . 'Lịch sản xuất: ' . $recordPlan->name . "\n"
//            . 'Nội dung: ' . $recordPlan->description ."\n"
            . 'Khởi hành: ' . $this->formateDate($recordPlan->start_time) ."\n"
            . 'Kết thúc: ' . $this->formateDate($recordPlan->end_time) ."\n"
            . 'Địa điểm: ' . $recordPlan->actual_address ."\n"
            . 'Nội dung đề xuất: ' . $recordPlan->offer_transportation_note
            , $phoneNumbers, $label, $deeplink);
    }

    public function assignDevicesToRecordPlan($record_plan_id, $device_ids, $status)
    {

        try {
            $record_plan = $this->recordPlanRepository->find($record_plan_id);
            $assignedDevice = $this->recordPlanDeviceXrefRepository->allQuery(["record_plan_id" => $record_plan_id, "status" => config("common.status.proposals.assigned")])->count();
            $proposalNumber = $record_plan["offer_device_number"];
            // kiểm tra số lượng device đã được assign so với số lượng đề xuất
            // if ((count($device_ids) + $assignedDevice) > $proposalNumber) {
            //     session()->flash("error", __("notification.assignment.fail.exceed", ["proposalNumber" => $proposalNumber, "department" => __('data_field_name.model_name.device')]));
            //     return false;
            // }
            $device_ids = $this->controller->addOtherInfoToDevice($record_plan, $device_ids, $status);
            $record_plan->devices()->syncWithoutDetaching($device_ids);

            return true;
        } catch (\Exception $exception) {
            Log::error($exception);
            return false;
        }
    }

    public function addNoteForRecordPlan($record_plan_id, $creator_department_code, $content)
    {
        $note = $this->noteRepository->allQuery()->where(["record_plan_id" => $record_plan_id, "from" => $creator_department_code]);
        if (count($note->get())) {
            $note->update(["content" => $content]);
            return true;
        } else {
            return $this->noteRepository->create([
                "record_plan_id" => $record_plan_id,
                "from" => $creator_department_code,
                "to" => "reporter",
                "content" => $content,
                "status" => 1
            ]);
        }
    }

    public function getRecordPlansHasDeliveredDevices()
    {
        return $this->recordPlanRepository->getDataHasDeliveredDevices();
    }


    public function getRecordPlansForDevices($device_xref_status, $allParams = null, $allocate_status = null)
    {
        $general_status = [
            config("common.status.record_plan.pending"),
            config("common.status.record_plan.approved"),
            config("common.status.record_plan.in_progress"),
            config("common.status.record_plan.wrap"),
            config("common.status.record_plan.done")
        ];
        if (empty($allocate_status)) {
            $allocate_status = config('common.status.proposals.unassigned');
        }
        $group_allocate_status = "device_allocate_status";
        $search = '';
        if (isset($allParams['s'])) {
            $search = $allParams['s'];
        }
        $data = $this->recordPlanRepository->allQuery(["s" => $search])->where([
            $group_allocate_status => $allocate_status,
        ])->whereIn("status", $general_status)->whereHas("devices", function ($query) use ($device_xref_status) {
            $query->where(["record_plan_device_xrefs.status" => $device_xref_status]);
        });

        $data = $data->paginate(10);

        return $data;
    }


    public function checkNumberAssignedMembers($record_plan_id, $department_code)
    {
//        $member_ids = $this->memberRepository->getMemberIdsByDepartmentCode($department_code);
        $num_of_assigned_members = $this->recordPlanMemberXrefRepository->allQuery()
            ->where(["record_plan_id" => $record_plan_id, "status" => config("common.status.propose.approved")])
//            ->whereIn("member_id", $member_ids)
            ->count();


        if ($num_of_assigned_members < 1) {
            $this->updateAllocateStatus($department_code, $record_plan_id, 1);
        }
        return true;

    }

    public function checkNumberAssignedDevices($record_plan_id)
    {
        $num_of_assigned_devices = $this->recordPlanDeviceXrefRepository->allQuery()
            ->where(["record_plan_id" => $record_plan_id, "status" => config("common.status.propose.approved")])->count();

        if ($num_of_assigned_devices < 1) {
            $this->updateAllocateStatus(config("common.departments.device"), $record_plan_id, 1);
        }
        return true;
    }

    public function checkAllocateStatus($department_code, $record_plan_id, $saveToDB=true)
    {
        $record_plan = $this->recordPlanRepository->find($record_plan_id);
        $offer_number = "offer_$department_code" . "_number";
        $allocate_status = $department_code . "_allocate_status";
        $proposed_number = $record_plan[$offer_number];
        $allocate_status = $record_plan[$allocate_status];

        $position = $department_code;
        $isAllocatedReporter = false;
        if ($department_code == config("common.departments.device")) {
            $device_status = [
                config("common.status.propose_device.approved"),
                config("common.status.propose_device.delivered"),
                config("common.status.propose_device.returned"),
            ];
            $assigned_number = $this->recordPlanDeviceXrefRepository->getDataByRecordPlanIdAndStatus($record_plan_id, $device_status)->count();

        } else {
            if ($department_code == "reporter") {
                if (auth()->user()->hasRole("super admin")) {
                    $department_code = config("common.departments.production");
                } else {
                    $department_code = auth()->user()->department_code;
                }
            }

            $member_ids = $this->memberRepository->allQuery(["department_code" => $department_code])->where(["status" => config("common.status.member.active")])->pluck("id");
            $assigned_number = $this->recordPlanMemberXrefRepository->getDataByRecordPlanIdAndStatus($record_plan_id, $member_ids, config("common.status.propose.approved"))->count();
            if($assigned_number> 0){
                $isAllocatedReporter = true;
            }
        }
        // kiểm tra nếu department có ghi chú về nhân sự thuê ngoài => status == approved
        if(str_contains($department_code, "production")){
            $outsourceNote = $this->noteRepository->findNoteByRecordPlanIdAndDepartmentCode($record_plan_id, "reporter");
        }else{
            $outsourceNote = $this->noteRepository->findNoteByRecordPlanIdAndDepartmentCode($record_plan_id, $department_code);
        }
        $outsourceNote = $outsourceNote ? trim($outsourceNote->content) : null;
//        dd($outsourceNote);
        //kiểm tra số lượng đề xuát và số lượng phân công
        if ($assigned_number >= $proposed_number || $outsourceNote) {
            $status = config("common.status.propose.approved");
        } else {
            $status = config("common.status.propose.pending");
        }

        if(str_contains($department_code, "production")){
            if($isAllocatedReporter  || $outsourceNote){
                $status = config("common.status.propose.approved");
            }else{
                $status = config("common.status.propose.pending");
            }
        }

        if ($saveToDB && $allocate_status != $status) {
            $this->updateAllocateStatus($position, $record_plan_id, $status);
        }
        return $status;
    }

    public function updateAllocateStatus($department_code, $record_plan_id, $status)
    {
        $field = $department_code . "_allocate_status";
        $this->recordPlanRepository->updateRecordPlanStatus($field, $status, $record_plan_id);
        $this->recordPlanService->checkRecordPlanGeneralStatus($record_plan_id);
    }

    public function handleProposedDevicesOnOtherRecordPlan($record_plan_id, $device_id, $xref_status = -2)
    {
        //lấy ra recordplan đang phân thiết bị
        $record_plan = $this->recordPlanRepository->findOrFail($record_plan_id);
        //lấy ra tất cả phân công thiết bị của lịch hiện tại
        $current_record_plan_device_ids = $this->recordPlanDeviceXrefRepository->allQuery(["device_id" => $device_id, "record_plan_id" => $record_plan_id])->pluck("id")->toArray();
        // lấy ra tất cả recordplan_device_xref bị trùng lịch với recordplan đang phân thiết bị
        $duplicated_record_plan_device_ids = $this->recordPlanDeviceXrefRepository->allQuery(["device_id" => $device_id])
            ->where([
                ["end_time", ">=", $record_plan->start_time],
                ["start_time", "<=", $record_plan->end_time],
            ])->get("id")->except($current_record_plan_device_ids)->toArray();
        $duplicated_record_plan_device_ids = array_column($duplicated_record_plan_device_ids, "id");
        // cập nhật trạng thái của các $duplicated_record_plan_device_ids status = -2
        $this->recordPlanDeviceXrefRepository->allQuery()->whereIn("id", $duplicated_record_plan_device_ids)
            ->update(["status" => $xref_status]);
        return true;
    }

    public function updateWorkStatusOfMember($recordPlanId, $workStatus) {
        $memberId = auth()->user()->id;
        $member = $this->memberRepository->find($memberId);
        if (!$member) {
            throw new ValidateException("member is not valid");
        }

        if ($member->department_code != config("common.departments.transportation") && $member->department_code != config("common.departments.camera") && $member->department_code != config("common.departments.technical_camera")) {
            throw new ValidateException("member is invalid");
        }

        if (!in_array($workStatus, array_values(config("common.transportation_status")))) {
            throw new ValidateException("status is not valid");
        }
        if (!$this->recordPlanMemberXrefRepository->allQuery(["record_plan_id" => $recordPlanId, "member_id" => $memberId])->first()) {
            throw new ValidateException("member is not assigned to recordplan");
        }

        $this->recordPlanMemberXrefRepository->allQuery(["record_plan_id" => $recordPlanId, "member_id" => $memberId])->update(["transportation_status" => $workStatus]);
        return true;
    }


}
