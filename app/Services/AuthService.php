<?php


namespace App\Services;


use App\Repositories\UserRepository;
use App\ResponseHandler\HandleResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AuthService
{
    public $userRepository;
    public $mochaService;

    public function __construct(
        UserRepository $userRepository,
        MochaService $mochaService
    )
    {
        $this->userRepository = $userRepository;
        $this->mochaService = $mochaService;
    }

    /**
     * Xác thực người dùng thông qua uuid
     * 1. Lấy thông tin user thông qua uuid.
     * Nếu lấy được, sinh token, thông báo xác thực thành công
     * Nếu không lấy được, thông báo xác thực không thành công
     * @param Request $request
     * @return JsonResponse
     */
    public function authenticate(Request $request)
    {
        //lấy thông tin người dùng thông qua uuid
        $user = $this->getUserByUuid($request);
        //Trả về thông báo xác thực thất bại trong trường hợp không lấy được user
        if (blank($user)) {
            return HandleResponse::handle(
                config('api_message.auth.get_user_failed'),
                401,
                'Không lấy được user qua uuid', '',
                "api/authenticate"
            );
        }
        $token = auth("api")->login($user);

        return $this->respondWithToken($token);

    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        $data = [
            'access_token' => $token,
            'token_type' => 'bearer',
        ];
        return HandleResponse::handle(
            config('api_message.auth.check_auth_success'),
            200,
            null,
            $data,
            "api/authenticate"
        );
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }


    /**
     * Xác định người dùng trong hệ thống bằng uuid
     * 1. Tìm User theo uuid trong bảng user.
     * Nếu tìm được, trả về cho người dùng
     * Nếu không tìm được, gọi sang mocha để lấy dữ liệu số điện thoại.
     * 2. Sau khi có thông tin số điện thoại, duyệt lại trong bảng user theo sđt để tìm thông tin user tương ứng
     * Nếu tìm được cập nhât uuid cho user tương ứng
     * Nếu không tìm được, trả về null
     * @param Request $request
     * @return mixed
     */
    public function getUserByUuid(Request $request)
    {
        if(!$request->uuid && !$request->phone_number){
            return false;
        }
        try {
            //truy vấn tìm user trong bảng user theo uuid
            $user = $this->userRepository->allQuery(["uuid" => $request->uuid])->get()->first();
            if (!$user) {
                //Check lại uuid thông qua mocha để tìm được user tương ứng.
                $user =  $this->getMochaUserByUuid($request->uuid);
            }
            if (!$user && !blank($request->phone_number)){
                $user = $this->userRepository->allQuery(["phone_number" => $request->phone_number])->get()->first();
            }

            //trả về user trong trường hợp tìm được
            return $user;
        } catch (\Exception $e) {
            Log::error("Error get user by uuid or phone number");
            Log::error($e->getMessage());
            return false;
        }
    }


    /**
     * Lấy thông tin người dùng từ mocha bằng uuid
     * 1. Gọi lên mocha theo api được mocha cung cấp để lấy thông tin người dùng của mocha theo uuid
     * 2. Từ thông tin người dùng, lấy ra thông tin sđt cần thiết
     * 3. Truy vấn dữ liệu tìm user tương ứng theo
     * @param $uuid
     * @return mixed|null
     */
    public function getMochaUserByUuid($uuid)
    {
        try {
            //lấy thông tin user từ mocha
            $rs = $this->mochaService->getMochaUserByUuid($uuid);

            Log::info($rs->getStatusCode());
            Log::info(json_encode($rs->getBody()));
            if ($rs) {//nếu có rs và msisdn(sđt) => tìm user trong db theo sđt
                Log::info($rs);
                if (isset($rs["msisdn"]) && $rs["msisdn"]) {
                    $phone_number = $rs["msisdn"];
                    return $this->getUserByPhoneNumber($phone_number, $uuid);
                } else {
                    //nếu có rs và msisdn = "" => k tìm thấy user trong mocha theo uuid
                    Log::info(__("notification.phone_number_is_null"));
                    return null;
                }
            } else {
                // nếu k có rs => lỗi fail to get mocha user by uuid
                Log::info(__("notification.cannot_get_user_by_uuid"));
                return null;
            }
        } catch (\Exception $exception) {
            Log::error($exception);
            return null;
        }

    }

    /**
     * Lấy thông tin người dùng thông qua số điện thoại
     * Cập nhật lại thông tin uuid tương ứng cho user
     * @param $phone_number : user's phone number
     * @param $uuid
     * @return mixed|null
     */
    public function getUserByPhoneNumber($phone_number, $uuid)
    {
        //get user info by phone number
        $user = $this->userRepository->allQuery(["phone_number" => $phone_number])->get()->first();
        if ($user) {
            $user->uuid = $uuid;
            $user->save();
            return $user;
        } else {
            Log::info(__("notification.user_not_exists_in_db"));
            return null;
        }

    }
}
