<?php


namespace App\Services;


use App\Repositories\CarRepository;
use App\Repositories\MemberRepository;

class CarService
{
    public $memberRepository;
    public $carRepository;

    public function __construct(
        MemberRepository $memberRepository,
        CarRepository $carRepository
    )
    {
        $this->memberRepository = $memberRepository;
        $this->carRepository = $carRepository;
    }
}
