<?php


namespace App\Services;


use App\Repositories\DeviceRepository;
use App\Repositories\RecordPlanDeviceXrefRepository;
use App\Repositories\RecordPlanRepository;
use Illuminate\Support\Facades\Log;

class DeviceService
{
    public $deviceRepository;
    public $recordPlanDeviceXrefRepository;
    public $recordPlanRepository;

    public function __construct(
        DeviceRepository $deviceRepository,
        RecordPlanDeviceXrefRepository $recordPlanDeviceXrefRepository,
        RecordPlanRepository $recordPlanRepository
    )
    {
        $this->deviceRepository = $deviceRepository;
        $this->recordPlanDeviceXrefRepository = $recordPlanDeviceXrefRepository;
        $this->recordPlanRepository = $recordPlanRepository;
    }


    public function getProposalDeviceIds($record_plan_id)
    {
        try {
            $status = [
                config("common.status.propose_device.pending"),
                config("common.status.propose_device.duplicated"),
            ];
            return $this->recordPlanDeviceXrefRepository->getColumnsByRecordPlanIdAndStatus($record_plan_id, $status);

        } catch (\Exception $exception) {
            Log::error($exception);
            return [];
        }
    }

    public function getReturnedDeviceIds($record_plan_id)
    {
        try {
            $status = [
                config("common.status.propose_device.returned"),
            ];
            return $this->recordPlanDeviceXrefRepository->getColumnsByRecordPlanIdAndStatus($record_plan_id, $status);

        } catch (\Exception $exception) {
            Log::error($exception);
            return [];
        }
    }

    public function getAssignedDevices($record_plan_id)
    {
        try {
            $status = [
                config("common.status.propose_device.approved"),
                config("common.status.propose_device.duplicated"),
            ];
            $assigned_device_ids = $this->recordPlanDeviceXrefRepository->getColumnsByRecordPlanIdAndStatus($record_plan_id, $status);
            return $this->recordPlanDeviceXrefRepository->geDataByRecordPlanIdAndDeviceIds($record_plan_id, $assigned_device_ids);

        } catch (\Exception $exception) {
            Log::error($exception);
            return [];
        }
    }

    public function handleLendingDevice()
    {
        $status = [
            config("common.status.propose_device.delivered"),
        ];
        $device_ids = $this->deviceRepository->allQuery(["status" => config("common.status.device.active")])->pluck("id");
        return ["status" => $status, "device_ids" => $device_ids];
    }

    public function getLendingDevices($allParams)
    {
        return $this->recordPlanDeviceXrefRepository->getDataByStatusAnDeviceIds($this->handleLendingDevice()['device_ids'], $this->handleLendingDevice()['status'], $allParams);
    }

    public function getLendingDevicesForAPI($allParams)
    {
        return $this->recordPlanDeviceXrefRepository->getDataByStatusAnDeviceIdsNoPaginate($this->handleLendingDevice()['device_ids'], $this->handleLendingDevice()['status'], $allParams);
    }

    public function getAvailableDevicesByRecordPlanId($record_plan_id, $keyword = null, $category_id = null)
    {
        try {
            $record_plan = $this->recordPlanRepository->find($record_plan_id);
            $record_plan_start_time = $record_plan->start_time;
            $record_plan_end_time = $record_plan->end_time;

            $data = $this->getAvailableDevicesByTime($record_plan_start_time, $record_plan_end_time, $record_plan_id, $keyword, $category_id);
            return $data;
        } catch (\Exception $exception) {
            Log::error($exception);
            return [];
        }
    }

    public function handleAvailableDevicesByTime($start_time, $end_time, $record_plan_id = null, $keyword = null)
    {
        $assigned_status = [
            config("common.status.propose_device.approved"),
            config("common.status.propose_device.delivered"),
        ];
        return $this->recordPlanDeviceXrefRepository->getDataStartTimeAndEndTime($start_time, $end_time, $assigned_status)->pluck("device_id")->toArray();
    }

    public function getAvailableDevicesByTime($start_time, $end_time, $record_plan_id = null, $keyword = null, $category_id = null)
    {
        $assigned_device_ids = $this->handleAvailableDevicesByTime($start_time, $end_time, $record_plan_id = null, $keyword, $category_id);
//        $record_plan_ids = $interfered_record_plans;
//        if ((int)$record_plan_id) {
//            $record_plan_ids = array_unique(array_merge($interfered_record_plans, [(int)$record_plan_id]));
//        }
//
//        $assigned_device_ids = $this->recordPlanDeviceXrefRepository->getColumnsByRecordPlanIdAndStatus($record_plan_ids, 2);
        //@todo xử lý trường hợp có device có tính đến quantity, hiện tại quantity đang mặc định = 1
        $data = $this->deviceRepository->getAvailableDevices($assigned_device_ids, $keyword, $category_id);
        return ($data);
    }

    public function searchDevicesByMany($start_time, $end_time, $record_plan_id = null, $keyword = null, $status = null)
    {
        $assigned_device_ids = $this->handleAvailableDevicesByTime($start_time, $end_time, $record_plan_id = null, $keyword = null);
        if ($status == null) {
            $data = $this->deviceRepository->allQuery()
                ->where(["status" => config("common.status.device.active")])
                ->where([["name", "like", "%$keyword%"]])
                ->whereNotIn("id", $assigned_device_ids)->get();
        } else {
            $data = $this->deviceRepository->allQuery()
                ->where([["name", "like", "%$keyword%"]])
                ->where([["status", $status]])
                ->whereNotIn("id", $assigned_device_ids)->get();
        }
        return ($data);
    }

    public function getDuplicatedProposalDeviceIds($record_plan_id, $available_device_ids)
    {
        $record_plan = $this->recordPlanRepository->findOrFail($record_plan_id);
        $start_time = $record_plan->start_time;
        $end_time = $record_plan->end_time;

        return $duplicated_proposal_device_ids = $this->recordPlanDeviceXrefRepository->getDuplicatedDataByDeviceIds($start_time, $end_time, $available_device_ids);

    }

    public function checkIfDevicesAssignedToRecordPlan($device_ids)
    {
        // check lịch mà thiết bị được phân công vào. Nếu lịch ở trạng thái (in_progress || wrap || done) || (pending && rp_device_xref_status == delivered) || (approved && start_time - now <= 1 ngày)
        // => cảnh báo, k cho xóa thiết bị

//        trạng thái (in_progress || wrap || done)
        $status = [
            config("common.status.record_plan.in_progress"),
            config("common.status.record_plan.wrap"),
            config("common.status.record_plan.done"),
        ];
        $record_plans = $this->recordPlanRepository->getDataByStatusAndDeviceIdsAndXrefStatus($status, $device_ids);

        //(pending && rp_device_xref_status == delivered)
        $rp_device_status = [
            config("common.status.propose_device.delivered"),
        ];
        $delivered_device_rp = $this->recordPlanRepository->getDataByStatusAndDeviceIdsAndXrefStatus([config("common.status.record_plan.pending"), config("common.status.record_plan.approved")], $device_ids, $rp_device_status);
        $record_plans = $record_plans->merge($delivered_device_rp);

        //(approved && start_time - now <= 1 ngày)
        $approved_rps = $this->recordPlanRepository->getDataByStatusAndDeviceIdsAndXrefStatus([config("common.status.record_plan.approved")], $device_ids);
        $cannt_delete_rps = $approved_rps->filter(function ($item) {
            $start_time = new \DateTime($item->start_time, new \DateTimeZone('Asia/Ho_Chi_Minh'));
            $now = new \DateTime(now());
            $diff = $start_time->getTimestamp() - $now->getTimestamp();
            if ($diff <= 86400 && $start_time->getTimestamp() > $now->getTimestamp()) {
                return $item;
            }
        });
        $record_plans = $record_plans->merge($cannt_delete_rps);
        $can_delete_rps = $approved_rps->filter(function ($item) {
            $start_time = new \DateTime($item->start_time, new \DateTimeZone('Asia/Ho_Chi_Minh'));
            $now = new \DateTime(now());
            $diff = $start_time->getTimestamp() - $now->getTimestamp();
            if ($diff > 86400 && $start_time->getTimestamp() > $now->getTimestamp()) {
                return $item;
            }
        });

        //Với những lịch đang ở trạng thái pending thì được phép xóa
        $pendingRecordPlan =$this->recordPlanRepository->getDataByStatusAndDeviceIdsAndXrefStatus([config("common.status.record_plan.pending")], $device_ids);
        $can_delete_rps = $can_delete_rps->merge($pendingRecordPlan);

        return [
            "cannt_delete_rps" => $record_plans,
            "can_delete_rps" => $can_delete_rps
        ];
    }

    public function handleDeletedDevices($device_ids, $record_plan_ids)
    {
        // lấy từng record_plans ở trạng thái cancel, approved, pending để cập nhât allocate_status và general_status
        $status = [
            config("common.status.record_plan.pending"),
            config("common.status.record_plan.approved"),
            config("common.status.record_plan.cancel"),
        ];
//        dd($device_ids);
        $delete_devices = $this->recordPlanDeviceXrefRepository->getDataDeviceIdsAndRecordPlanStatus($device_ids, $record_plan_ids, $status);

        // xóa phân công thiết bị ở bảng trung gian
        $data = $this->recordPlanDeviceXrefRepository->allQuery(["device_id" => $device_ids, "record_plan_id" => $record_plan_ids])->delete();
//        try {
        if ($delete_devices) {
            $assignmentService = app(AssignmentService::class);
            foreach ($delete_devices as $item) {
                $assignmentService->checkAllocateStatus("device", $item->record_plan_id);
            }
            return true;
        }

//        } catch (\Exception $exception) {
//            Log::error($exception);
//            return false;
//        }
    }

    public function handleStatusCode($id)
    {
        $code = [];
        $code['success'] = config('api_message.devices.create_success');
        $code['failed'] = config('api_message.devices.create_failed');
        if ($this->deviceRepository->isValue($id)) {
            $code['success'] = config('api_message.devices.update_success');
            $code['failed'] = config('api_message.devices.update_failed');
        }
        return $code;
    }

    /**
     * Lấy record plan theo device
     *
     * @param $deviceId
     * @return mixed
     */
    public function getRecordPlansForDevices($deviceId)
    {
        return $this->recordPlanRepository->allQuery()->whereHas('devices', function ($query) use ($deviceId) {
            $query->where([
                ['record_plan_device_xrefs.device_id', $deviceId],
                ['record_plan_device_xrefs.status', config("common.status.record_plan.in_progress")],
            ]);
        })->first();
    }

    /**
     * Lấy deivce đang mượn theo userId
     *
     * @param $deviceId
     * @return mixed
     */
    public function getListBorrowingDeviceByUserId($userId, $keyword = null)
    {
        return $this->recordPlanDeviceXrefRepository->allQuery(['status'=>config("common.status.propose_device.delivered"), "s"=>$keyword])->where('hold_by',$userId)->with(["device", "record_plan"])->get();
    }
}
