<?php


namespace App\Services\Interfaces;


use Illuminate\Http\Request;

interface IAssignmentService
{
    function assignMembersToRecordPlan($recordPlanId, $departmentCode, $memberIds);

    /**
     * Cập nhật trạng thái công việc của quay phim | lái xe theo lịch
     *
     * @param $record_plan_id
     * @param $workStatus { status(1: bắt đầu | 2: kết thúc) }
     * @return mixed
     */
    function updateWorkStatusOfMember ($record_plan_id, $workStatus);
}
