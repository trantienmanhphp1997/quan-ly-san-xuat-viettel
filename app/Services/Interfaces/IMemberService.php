<?php


namespace App\Services\Interfaces;


use App\ViewModel\PaginateModel;
use Illuminate\Http\Request;

interface IMemberService
{

    function getAvailableMembers($departmentCode, $recordPlanId, $query = []);

    function getAssignedMembers($departmentCode, $recordPlanId, $query = []);

    function getAssignmentsByDepartmentCode(Request $request, $departmentCode, PaginateModel $paginateModel = null);

    function getAssignmentsByMember(Request $request, $memberId, PaginateModel $paginateModel = null);

    function getReportersForRecPlan(Request $request);

    function getReportersByDepartmentCode($departmentCode, $search);

    function getMembersForRecPlan(Request $request);
}
