<?php


namespace App\Services\Interfaces;


use App\ViewModel\PaginateModel;
use Illuminate\Http\Request;


interface IRecordPlanService
{
    function saveRecordPlan(Request $request);

    function searchRecordPlan($query, $isActive, PaginateModel $paginateModel = null, $creator=null);

    function getProposalsByDepartmentCode($departmentCode, $query, $allocate_status, PaginateModel $paginateModel = null);

    function countProposalsByDepartmentCode($departmentCode, $query, $allocate_status='');

    function countProposalsByLendingStatus($departmentCode, $query, $allocate_status='', $lendingDevice='');

    function detachMembersByRecordPlan($recordPlanId, $memberIds);

    function destroy($recordPlanIds);

    function getAssignComing(Request $request);

    public static function checkTimeToUpdateRecordPlan($recordPlanStartTime);

    function updateGeneralStatus($id, Request $request);

    function getDailyRecordPlans($fromDate, $toDate, $status);

    function prepareRecordPlansForExport($data, $isExportAllInfo = false);

    function addXrefsInfoToRecordPlans($recordPlans);
}
