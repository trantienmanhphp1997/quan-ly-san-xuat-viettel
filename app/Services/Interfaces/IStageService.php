<?php


namespace App\Services\Interfaces;


use Illuminate\Http\Request;

interface IStageService
{
    function getAllStages(Request $request);

}
