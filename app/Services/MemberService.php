<?php


namespace App\Services;


use App\Exceptions\ValidateException;
use App\Models\User;
use App\Repositories\DepartmentRepository;
use App\Repositories\MemberRepository;
use App\Repositories\RecordPlanMemberXrefRepository;
use App\Repositories\RecordPlanRepository;
use App\Repositories\UserRepository;
use App\Services\Interfaces\IMemberService;
use App\ViewModel\PaginateModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MemberService implements IMemberService
{
    public $memberRepository;
    public $recordPlanRepository;
    public $recordPlanMemberXrefRepository;
    public $departmentRepository;
    public $userRepository;

    public function __construct(
        MemberRepository $memberRepository,
        RecordPlanRepository $recordPlanRepository,
        RecordPlanMemberXrefRepository $recordPlanMemberXrefRepository,
        DepartmentRepository $departmentRepository,
        UserRepository $userRepository
    )
    {
        $this->memberRepository = $memberRepository;
        $this->recordPlanRepository = $recordPlanRepository;
        $this->recordPlanMemberXrefRepository = $recordPlanMemberXrefRepository;
        $this->departmentRepository = $departmentRepository;
        $this->userRepository = $userRepository;
    }

    public function handleDepartmentCode($department_code)
    {
        return $this->memberRepository->handleDepartmentCode($department_code);
    }

    /**
     * @param $departmentCode
     * @param $recordPlanId
     * @param array $relationship
     * @param array $query
     * @return array|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     * trả về danh sách member đã được phân công
     */
    public function getAssignedMembers($departmentCode, $recordPlanId, $relationship = [], $query = [])
    {
        $assigned_member_ids = $this->recordPlanMemberXrefRepository->getMemberIdsByStatus($departmentCode, $recordPlanId, [config("common.status.assignments.members.accept"), config("common.status.record_plan.done") ]);
        if($departmentCode == "reporter"){
           $departmentCode = config("common.departments.production");
        }
        return $this->memberRepository->getMemberByIdAndDepartmentCode($departmentCode, $assigned_member_ids, $relationship, $query);
    }
// lấy danh sách member sẵn sàng để phân công cho 1 lịch

    /**
     * @param $departmentCode
     * @param $recordPlanId
     * @param array $query
     * @param $start_time
     * @param $end_time
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     * trả về danh sách member sẵn sàng trong khoảng thời gian (start_time-endtime) để phần công
     */
    public function getAvailableMembers($departmentCode, $recordPlanId = null, $query = [], $start_time = null, $end_time = null)
    {
        $record_plan_start_time = null;
        $record_plan_end_time = null;
        if(!empty($recordPlanId)) {
            $record_plan = $this->recordPlanRepository->allQuery()->with(["plan"])->find($recordPlanId);
            $record_plan_start_time = $record_plan->start_time;
            $record_plan_end_time = $record_plan->end_time;
        }
        else {
            $record_plan_start_time = $start_time;
            $record_plan_end_time = $end_time;
        }


        if ($departmentCode == "reporter") {
            if (auth()->user()->hasRole("super admin")) {
                $departmentCode = config("common.departments.production");
            } else {
                $departmentCode = auth()->user()->department_code;
            }
        } else {
            $departmentCode = config("common.departments.$departmentCode");
        }

        if( ((str_contains($departmentCode, config("common.departments.production")) || $departmentCode==config("common.departments.transportation")) && $recordPlanId)){
            $assigned_member_ids = $this->recordPlanMemberXrefRepository->allQuery()->where(["status" => [config("common.status.propose.approved"),config("common.status.propose.pending")], "record_plan_id" => $recordPlanId])->pluck("member_id")->toArray();
        }else {
            $assigned_member_ids = $this->recordPlanMemberXrefRepository->getDataStartTimeAndEndTime($record_plan_start_time, $record_plan_end_time, [config("common.status.propose.approved"),config("common.status.propose.pending")])->pluck("member_id")->toArray();
        }

        $data = $this->memberRepository->getAvailableMembers($departmentCode, $assigned_member_ids, $query);
//        dd($data);
        if($departmentCode == config("common.departments.transportation")){// nếu là transportation, trong members thêm thuộc tính record_plans là những lịch sx có
            //thời gian giao với lịch hiện tại mà lái xe đó tham gia
            $record_plan_xrefs = $this->recordPlanMemberXrefRepository->getDataStartTimeAndEndTime($record_plan_start_time, $record_plan_end_time);
            foreach ($data as $member){
                $record_plans = $record_plan_xrefs->map(function ($recPlan) use($member) {
                    if($recPlan->member_id == $member->id){
                        return $recPlan->record_plan;
                    }
                });
                $member->{"record_plans"} = $record_plans->filter();
            }
        }

        return $data;
    }

    public function checkIfMembersAssignedToRecordPlan($member_ids)
    {
        // check lịch mà thiết bị được phân công vào. Nếu lịch ở trạng thái (in_progress || wrap || done) || (pending && rp_device_xref_status == delivered) || (approved && start_time - now <= 1 ngày)
        // => cảnh báo, k cho xóa thiết bị

//        trạng thái (in_progress || wrap || done)
        $status = [
            config("common.status.record_plan.in_progress"),
            config("common.status.record_plan.wrap"),
            config("common.status.record_plan.done"),
        ];
        $record_plans = $this->recordPlanRepository->getDataByStatusAndMemberIdsAndXrefStatus($status, $member_ids);

        //(approved && start_time - now <= 1 ngày)
        $approved_rps = $this->recordPlanRepository->getDataByStatusAndMemberIdsAndXrefStatus([config("common.status.record_plan.approved")], $member_ids);
        $cannt_delete_rps = $approved_rps->filter(function ($item) {
            $start_time = new \DateTime($item->start_time, new \DateTimeZone('Asia/Ho_Chi_Minh'));
            $now = new \DateTime(now());
            $diff = $start_time->getTimestamp() - $now->getTimestamp();
            if ($diff <= 86400 && $start_time->getTimestamp() > $now->getTimestamp()) {
                return $item;
            }
        });
        $record_plans = $record_plans->merge($cannt_delete_rps);
        $can_delete_rps = $approved_rps->filter(function ($item) {
            $start_time = new \DateTime($item->start_time, new \DateTimeZone('Asia/Ho_Chi_Minh'));
            $now = new \DateTime(now());
            $diff = $start_time->getTimestamp() - $now->getTimestamp();
            if ($diff > 86400 && $start_time->getTimestamp() > $now->getTimestamp()) {
                return $item;
            }
        });

        //Với những lịch đang ở trạng thái pending thì được phép xóa
        $pendingRecordPlan = $this->recordPlanRepository->getDataByStatusAndMemberIdsAndXrefStatus([config("common.status.record_plan.pending")], $member_ids);
        $can_delete_rps = $can_delete_rps->merge($pendingRecordPlan);

        return [
            "cannt_delete_rps" => $record_plans,
            "can_delete_rps" => $can_delete_rps
        ];
    }

    public function handleDeletedMembers($member_ids, $record_plan_ids, $department_code)
    {

        // lấy từng record_plans ở trạng thái cancel, pending để cập nhât allocate_status và general_status
        $status = [
            config("common.status.record_plan.pending"),
            config("common.status.record_plan.approved"),
            config("common.status.record_plan.cancel"),
        ];
        $delete_members = $this->recordPlanMemberXrefRepository->getDataByMemberIdsAndRecordPlanStatus($member_ids, $record_plan_ids, $status);
        // xóa phân công nhân sự ở bảng trung gian
        $data = $this->recordPlanMemberXrefRepository->allQuery(["member_id" => $member_ids, "record_plan_id" => $record_plan_ids])->delete();
        $assignmentService = app(AssignmentService::class);
        foreach ($delete_members as $item) {
            $assignmentService->checkAllocateStatus($department_code, $item->record_plan_id);
        }
        return true;

    }

    public function getAssignmentsByDepartmentCode(Request $request, $departmentCode, PaginateModel $paginateModel = null)
    {
        if(blank($departmentCode)){
            throw new ValidateException(__("notification.common.fail.no_department_code"));
        }
        $member_ids = $this->memberRepository->getMemberIdsByDepartmentCode($departmentCode);

        if ($departmentCode == config("common.departments.device")) {
            $status = [
                config("common.status.propose_device.pending"),
                config("common.status.propose_device.approved"),
                config("common.status.propose_device.delivered"),
                config("common.status.propose_device.returned"),
                config("common.status.propose_device.record_plan_done"),
            ];
        } else {
            $status = [
                config("common.status.propose.approved"),
                config("common.status.propose.pending"),
                config("common.status.propose.record_plan_done"),
            ];
        }
        $allParams = $request->input();

        $record_plans = $this->recordPlanMemberXrefRepository->handleDataByMemberIdsAndStatus($member_ids, $status, $allParams, $paginateModel);

        return $record_plans;

    }

    public function getAssignmentsByMember(Request $request, $memberId, PaginateModel $paginateModel = null){
        if(!$memberId){
            throw new ValidateException(__("notification.common.fail.no-data-selected"));
        }

        $status = [
            config("common.status.propose.approved"),
            config("common.status.propose.pending"),
            config("common.status.propose.record_plan_done"),
        ];

        $allParams = $request->input();

        return $this->recordPlanMemberXrefRepository->handleDataByMemberIdsAndStatus($memberId, $status, $allParams, $paginateModel);

    }

    public function getReportersForRecPlan(Request $request) {
        $department_code = config("common.departments.production");
        $criterials = $request->all();
        $criterials['department_code'] = $department_code;
        return  $data = $this->memberRepository->allQuery($criterials)->get();
    }

    public function getReportersByDepartmentCode($departmentCode, $search) {
        $department_code = $departmentCode ?? config("common.departments.production");
        $_query = $this->memberRepository->allQuery()->where([!$departmentCode ? ["department_code", "like", "%$department_code%"] : ["department_code", "=", "$department_code"]])->where(["status" => 1]);
        if ($search) {
            $_query->where("full_name", "like", "%$search%")->orWhere("phone_number", "like", "%$search%");
        }
        return $_query->get();
    }

    public function getMembersForRecPlan(Request $request) {
        $criterials = $request->all();
        $record_plan_id = $request->record_plan_id;
        $record_plan = $this->recordPlanRepository->find($record_plan_id);
        $member_status = config("common.status.member.active");
        $data = $this->memberRepository->getMembersByDepartmentCodeAndStatus($member_status, $criterials['department_code'], $request->s);
        $result = [];
        //@todo cần update logic
        // nếu là quay phim | lái xe, kỹ thuật phòng quay trong members thêm thuộc tính record_plans là những lịch sx có
        if (in_array($criterials['department_code'], [config("common.departments.transportation"), config("common.departments.camera"), config("common.departments.technical_camera")])) {
            // thời gian giao với lịch hiện tại mà lái xe | quay phim đó tham gia
            $status = [config("common.status.record_plan.pending"), config("common.status.record_plan.approved"), config("common.status.record_plan.in_progress"), config("common.status.record_plan.wrap"), ];
            $record_plan_xrefs = $this->recordPlanMemberXrefRepository->getDataStartTimeAndEndTime($record_plan->start_time, $record_plan->end_time, $status);
            if ($criterials['department_code'] === config("common.departments.transportation")) {
                foreach ($data as $member) {
                    $isAvailable = true;
                    $record_plans = $record_plan_xrefs->filter(function ($recPlan) use($member, $record_plan_id) {
                        return($recPlan->member_id == $member->id);
                    });
                    $recordPlansOnTrip = $record_plan_xrefs->filter(function ($recPlan) use($member) {
                        return($recPlan->transportation_status == config("common.transportation_status.on_trip") && $recPlan->member_id == $member->id);
                    });
                    if (count($recordPlansOnTrip)) {
                        $isAvailable = false;
                    }
                    $member = $member->toArray();
                    $member["is_available"] = $isAvailable;
                    $member["involved_record_plans"] = array_map(function ($recPlan) {
                        $item = new \stdClass();
                        $item->recPlan = $recPlan["record_plan"];
                        $item->involveType = $recPlan["involve_type"];
                        return $item;
                    }, array_values($record_plans->toArray()));
                    $result[] = $member;
                }
            } else if ($criterials['department_code'] === config("common.departments.camera") || $criterials['department_code'] === config("common.departments.technical_camera")) {
                foreach ($data as $member) {
                    $isAvailable = true;
                    $record_plans = $record_plan_xrefs->filter(function ($recPlan) use($member, $record_plan_id) {
                        return($recPlan->member_id == $member->id);
                    });
                    $recordPlansOnTrip = $record_plan_xrefs->filter(function ($recPlan) use($member) {
                        return($recPlan->transportation_status == config("common.camera_status.on_trip") && $recPlan->member_id == $member->id);
                    });
                    if (count($recordPlansOnTrip)) {
                        $isAvailable = false;
                    }
                    $member = $member->toArray();
                    $member["is_available"] = $isAvailable;
                    $member["involved_record_plans"] = array_map(function ($recPlan) {
                        $item = new \stdClass();
                        $item->recPlan = $recPlan["record_plan"];
                        return $item;
                    }, array_values($record_plans->toArray()));
                    $result[] = $member;
                }
            }
        }
        return  $result;
    }

    public function getMembersByDepartmentCode($department_code) {
        return $this->memberRepository->getMembersByDepartmentCodeAndStatus(config("common.status.member.active"), $department_code);
    }

    public function haveRecordPlanCreator ($member_ids) {
        return $this->recordPlanRepository->allQuery()->whereIn("created_by",$member_ids)->exists();
    }
}
