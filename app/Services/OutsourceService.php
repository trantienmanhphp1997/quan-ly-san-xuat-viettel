<?php

namespace App\Services;

use App\Repositories\OutsourceRepository;
use App\Repositories\OutsourceTypeRepository;

class OutsourceService
{
    public $outsourceRepository;
    public $outsourceTypeRepository;

    public function __construct(OutsourceRepository $outsourceRepository, OutsourceTypeRepository $outsourceTypeRepository)
    {
        $this->outsourceRepository = $outsourceRepository;
        $this->outsourceTypeRepository = $outsourceTypeRepository;
    }
}
