<?php


namespace App\Services;

use App\Exceptions\ValidateException;
use App\Http\Controllers\Admin\Controller;
use App\Mail\ReportDailyRecordPlansEmail;
use App\Models\RecordPlan;
use App\Models\User;
use App\Repositories\MemberRepository;
use App\Repositories\RecordPlanDeviceXrefRepository;
use App\Repositories\RecordPlanMemberXrefRepository;
use App\Repositories\RecordPlanRepository;
use App\Services\Interfaces\IMemberService;
use App\Services\Interfaces\IRecordPlanService;
use App\Utils\ExcelUtil;
use App\ViewModel\PaginateModel;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PHPUnit\Exception;

class RecordPlanService implements IRecordPlanService
{
    public static $PAGE_SIZE = 10;
    public $recordPlanMemberXrefRepository;
    public $recordPlanDeviceXrefRepository;
    public $recordPlanRepository;
    public $memberRepository;
    public $controller;
    public $memberService;
    public $excelUtil;

    public function __construct(
        RecordPlanMemberXrefRepository $recordPlanMemberXrefRepository,
        RecordPlanDeviceXrefRepository $recordPlanDeviceXrefRepository,
        RecordPlanRepository $recordPlanRepository,
        MemberRepository $memberRepository,
        Controller $controller,
        IMemberService $memberService,
        ExcelUtil $excelUtil
    )
    {
        $this->recordPlanMemberXrefRepository = $recordPlanMemberXrefRepository;
        $this->recordPlanDeviceXrefRepository = $recordPlanDeviceXrefRepository;
        $this->recordPlanRepository = $recordPlanRepository;
        $this->memberRepository = $memberRepository;
        $this->controller = $controller;
        $this->memberService = $memberService;
        $this->excelUtil = $excelUtil;
    }

    public function detachMembersByRecordPlan($recordPlanId, $memberIds)
    {
        if (blank($memberIds)) {
            throw new ValidateException(__("notification.common.fail.no-data-selected"));
        }

        $recordPlan = $this->recordPlanRepository->find($recordPlanId);
        if (blank($recordPlan)) {
            throw new ValidateException(__("notification.common.fail.no-data"));
        }
        $this->recordPlanMemberXrefRepository->allQuery()->where(["record_plan_id" => $recordPlanId])
            ->whereIn("member_id", $memberIds)->delete();
        // gửi tin nhắn đến những thành viên bị xóa khỏi lịch
        $this->sendMessageToRemovedMembers($recordPlanId, $memberIds);
        return true;
    }

    public function sendMessageToRemovedMembers($recordPlanId, $memberIds)
    {
        $recordPlan = $this->recordPlanRepository->find($recordPlanId);
        $label = "Xem chi tiết";
        $deeplink = "mocha://survey?ref=".urlencode(config("mocha.wap_url")."/record-plans/detail/$recordPlanId?record-plan-id=$recordPlanId");
        $oa_id = config("mocha.oa_id");

        $phoneNumbers = User::whereIn("id", $memberIds)->whereNotNull('phone_number')->get();
        MochaService::sendMessageToUser($oa_id, 'Bạn vừa được bỏ phân công khỏi lịch sản xuất ' . $recordPlan->name, $phoneNumbers, $label, $deeplink);
    }

    public function detachDevicesByRecordPlan($record_plan_id, $device_ids)
    {
        $record_plan = $this->recordPlanRepository->findOrFail($record_plan_id);
        if ($record_plan) {
            $data = $this->recordPlanDeviceXrefRepository->allQuery()->where(["record_plan_id" => $record_plan_id])
                ->whereIn("device_id", $device_ids)->delete();
            return true;
        }
        return null;
    }

    public function checkRecordPlanGeneralStatus($record_plan_id)
    {
        $record_plan = $this->recordPlanRepository->findOrFail($record_plan_id);

        $reporter_allocate_status = $record_plan->reporter_allocate_status;
        $camera_allocate_status = $record_plan->camera_allocate_status;
        $transportation_allocate_status = $record_plan->transportation_allocate_status;
        $device_allocate_status = $record_plan->device_allocate_status;
        $technical_camera_allocate_status = $record_plan->technical_camera_allocate_status;

        if ($reporter_allocate_status == 2 && $camera_allocate_status == 2 && $transportation_allocate_status == 2 && $device_allocate_status == 2 && $technical_camera_allocate_status == 2) {
            if ($record_plan->status == 1) {
                $record_plan->update(["status" => 2]);
            }
        } else {
            if ($record_plan->status >= 1) {
                $record_plan->update(["status" => 1]);
            } else {
                $record_plan->update(["status" => 1]);// k vào trường hợp nào cho thì đẩy lịch về trạng thái default = chờ phê duyệt
            }
        }
        return true;
    }

    public function destroy($recordPlanIds)
    {
        //kiểm tra trạng thái của lịch sản xuất
        // nếu trạng thái của lịch sản xuất  = Chờ xác nhận || Nháp || Đã hủy  thì mới được xóa !
        $status = $this->recordPlanRepository->allQuery()->whereIn('id', $recordPlanIds)->pluck('status')->toArray();
        $statusCanDels = [config("common.status.record_plan.cancel"),/*config("common.status.record_plan.inactive"),*/
            config("common.status.record_plan.pending")];

        if (count(array_diff($status, $statusCanDels)) == 0) {
            $this->recordPlanRepository->delete($recordPlanIds);
            return true;
        } else {
            throw new ValidateException(__("notification.record_plan.statusCannotDel"));
        }
    }

    public function handleOfferNumberOfResourcesToUpdate($record_plan, $department_code, Request $request)
    {
        $request = $request->all();
        $offer_number = "offer_" . $department_code . "_number";

        if ($record_plan->status == config("common.status.record_plan.approved")) {
            $assigned_number = $record_plan[$offer_number];
            $proposed_number = $request[$offer_number];
            if ($assigned_number > $proposed_number) {
                return false;
            }
            return true;
        }
        return true;
    }

    public function checkResourceAllocateStatusToUpdate($record_plan, $department_code, Request $request)
    {
        $request = $request->all();
        $offer_number = "offer_" . $department_code . "_number";

        if (!($request[$offer_number])) {
            return config("common.status.propose.approved");
        } else {
            if ((!($record_plan[$offer_number]) && $request[$offer_number]) || ($record_plan[$offer_number] < $request[$offer_number])) {
                return config("common.status.propose.pending");
            }
            if (($record_plan[$offer_number] >= $request[$offer_number])) {
                $offer_number = "offer_$department_code" . "_number";
                $proposed_number = $request[$offer_number];

                if ($department_code == config("common.departments.device")) {
                    $assigned_number = $this->recordPlanDeviceXrefRepository->getDataByRecordPlanIdAndStatus($record_plan->id,
                        [config("common.status.propose_device.approved"), config("common.status.propose_device.delivered"), config("common.status.propose_device.returned")]
                    )->count();
                } else {
                    if ($department_code == "reporter") {
                        if (auth()->user()->hasRole("super admin")) {
                            $department_code = config("common.departments.production");
                        } else {
                            $department_code = auth()->user()->department_code;
                        }
                    }
                    $member_ids = $this->memberRepository->allQuery(["department_code" => $department_code])->where(["status" => config("common.status.member.active")])->pluck("id");
                    $assigned_number = $this->recordPlanMemberXrefRepository->getDataByRecordPlanIdAndStatus($record_plan->id, $member_ids, config("common.status.propose.approved"))->count();

                }
                //kiểm tra số lượng đề xuát và số lượng phân công
                if ($assigned_number >= $proposed_number) {
                    return $status = config("common.status.propose.approved");
                } else {
                    return $status = config("common.status.propose.pending");
                }
            }
        }
//        return $record_plan[$allocate_status];

    }

    public function handleAssignedResourcesForCanceledRecordPlan($record_plan)
    {
        try {
            $record_plan_id = $record_plan->id;
            $assigned_member_ids = $this->recordPlanMemberXrefRepository->getDataByRecordPlanId($record_plan_id)->pluck("member_id");
            $assigned_device_ids = $this->recordPlanDeviceXrefRepository->getColumnsByRecordPlanIdAndStatus($record_plan_id);
            $update_allocate_status = [
                "reporter_allocate_status" => 1,
                "camera_allocate_status" => 1,
                "technical_camera_allocate_status" => 1,
                "transportation_allocate_status" => 1,
                "device_allocate_status" => 1,
            ];
            $record_plan->update($update_allocate_status);

            $record_plan->members()->detach($assigned_member_ids);
            $record_plan->devices()->detach($assigned_device_ids);
            return true;
        } catch (\Exception $exception) {
            Log::error($exception);
            return false;
        }
    }

    public function sendMessagesToAssignedResources()
    {
        //@todo
        return true;
    }

    public function handleAssignedResourcesForDoneRecordPlan($record_plan)
    {
        $record_plan_id = $record_plan->id;
        $assigned_member_ids = $this->recordPlanMemberXrefRepository->getDataByRecordPlanId($record_plan_id)->pluck("member_id");
        $assigned_device_ids = $this->recordPlanDeviceXrefRepository->getColumnsByRecordPlanIdAndStatus($record_plan_id);

//        try {
        //update status of members and devices to record_plan_done;
        $this->recordPlanMemberXrefRepository->allQuery()->where(["record_plan_id" => $record_plan_id])->whereIn("member_id", $assigned_member_ids)
            ->update(["status" => config("common.status.propose.record_plan_done")]);
        // $this->recordPlanDeviceXrefRepository->allQuery()->where(["record_plan_id" => $record_plan_id])->whereIn("device_id", $assigned_device_ids)
        //     ->update(["status" => config("common.status.propose.record_plan_done")]);

        return true;
//        } catch (\Exception $exception) {
//            Log::error($exception);
//            return false;
//        }
    }

    //Kiểm tra nguồn lực trong lịch sản xuất
    public function checkResourcesForPreparingInProgress(RecordPlan $record_plan)
    {
        $deviceInRecordPlanArr = $this->recordPlanDeviceXrefRepository->allQuery(["record_plan_id" => $record_plan->id])->pluck('status')->toArray();
        $memberInRecordPlanArr = [$record_plan->reporter_allocate_status, $record_plan->camera_allocate_status, $record_plan->technical_camera_allocate_status, $record_plan->transportation_allocate_status];
        $check = true;
        if ((count(array_keys($memberInRecordPlanArr, config('common.status.propose.approved'))) != count($memberInRecordPlanArr)) || (count(array_keys($deviceInRecordPlanArr, config('common.status.propose_device.delivered'))) != count($deviceInRecordPlanArr))) {
            $check = false;
        }
        return $check;
    }

    public function remindInComingRecordPlans()
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        //lọc những lịch có trạng thái = đã chốt (approved) mà có thời gian bắt đầu vào ngày mai
        $record_plans = $this->recordPlanRepository->allQuery()->where([
            "status" => config("common.status.record_plan.approved"),
            ["start_time", "<=", now()->addDays(1)],
            ["start_time", ">", now()],
        ])->get();

        //gửi tin nhắn nhắc lịch cho những thành viên liên quan với từng lịch
        foreach ($record_plans as $record_plan) {
            $message_content = "Lịch sản xuất '$record_plan->name' (" . date('H:i d/m/Y', strtotime($record_plan->start_time)) . " - " . date('H:i d/m/Y', strtotime($record_plan->end_time)) . " )"
                . " tại " . $record_plan->actual_address . " sắp diễn ra." . "\r\n"
                . " Bạn hãy chuẩn bị sẵn sàng để tham gia lịch đầy đủ nhé.";
            $member_ids = $record_plan->members->pluck("id")->toArray();
            $member_phone_numbers = $this->memberRepository->allQuery(["id" => $member_ids])->whereHas("record_plans", function ($query) {
                $query->where(["record_plan_member_xrefs.status" => config("common.status.propose.approved")]);
            })->get();
            Log::info("Send message to remind incoming in_progress record plans ");
            MochaService::sendMessageToUser("oa_id", $message_content, $member_phone_numbers, "", "");
        }

    }

    public function remindWrapRecordPlans()
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        //lọc những lịch có trạng thái = đang diễn ra (in_progress) mà có thời gian kết thúc vào ngày mai
        $record_plans = $this->recordPlanRepository->allQuery()->where([
            "status" => config("common.status.record_plan.in_progress"),
            ["end_time", "<=", now()->addDays(1)],
            ["end_time", ">", now()],
        ])->get();

        //gửi tin nhắn nhắc lịch cho những thành viên liên quan với từng lịch
        foreach ($record_plans as $record_plan) {
            $message_content = "Lịch sản xuất '$record_plan->name' (" . date('H:i d/m/Y', strtotime($record_plan->start_time)) . " - " . date('H:i d/m/Y', strtotime($record_plan->end_time)) . " )"
                . " tại " . $record_plan->actual_address . " sắp kết thúc." . "\r\n";
            $member_ids = $record_plan->members->pluck("id")->toArray();
            $member_phone_numbers = $this->memberRepository->allQuery(["id" => $member_ids])->whereHas("record_plans", function ($query) {
                $query->where(["record_plan_member_xrefs.status" => config("common.status.propose.in_progress")]);
            })->get();
            Log::info("Send message to remind incoming wrap record plans ");
            MochaService::sendMessageToUser("oa_id", $message_content, $member_phone_numbers, "", "");
        }
    }

    public function autoStartReportPlan()
    {
        // lấy ra tất cả các lịch có giờ bắt đầu  nằm trong h hiện tại
        date_default_timezone_set("Asia/Bangkok");
        $record_plans = $this->recordPlanRepository->allQuery()->where([
            ["start_time", "<", now()],
        ])->whereIn("status", [config("common.status.record_plan.pending"), config("common.status.record_plan.approved")])->pluck('id');
        Log::info("Auto Start RecordPlan", [now(), $record_plans]);
        //Tự động start các lịch sản xuất
        return $this->recordPlanRepository->allQuery()->whereIn('id', $record_plans)->update(['status' => config("common.status.record_plan.in_progress")]);
    }

    public function autoWrapReportPlan()
    {
        // lấy ra tất cả các lịch có giờ bắt đầu  nằm trong h hiện tại
        date_default_timezone_set("Asia/Bangkok");
        $record_plans = $this->recordPlanRepository->allQuery()->where([
            "status" => config("common.status.record_plan.in_progress"),
            ["end_time", "<", now()],
        ])->pluck('id');
        Log::info("Auto Cancel RecordPlan", [$record_plans]);
        //Tự động Cancel các lịch sản xuất
        return $this->recordPlanRepository->allQuery()->whereIn('id', $record_plans)->update(['status' => config("common.status.record_plan.wrap")]);
    }

    //checkTimeToUpdateRecordPlan
    public static function checkTimeToUpdateRecordPlan($recordPlanEndTime)
    {

        //xử lý thời gian
         date_default_timezone_set("Asia/Bangkok");
        $now = Carbon::now()->startOfDay();
        $endTime = Carbon::create($recordPlanEndTime);
        $canUpdate=  $now < $endTime;

        return $canUpdate;
    }

    //check trạng thái recordplan có thể thao tác
    public static function checkGeneralStatusCanUpdate($recordPlanStatus): bool
    {
        $rs = true;

        if (in_array($recordPlanStatus, [config("common.status.record_plan.wrap"), config("common.status.record_plan.done"), config("common.status.record_plan.cancel")])) {
            return false;
        }

        return $rs;
    }

    function saveRecordPlan(Request $request)
    {

    }

    function searchRecordPlan($query, $isActive, PaginateModel $paginateModel = null, $creator = null)
    {
        // dd($query, $isActive);
        $fromDateFilter = [];
        $toDateFilter = [];

        if ($paginateModel) {
            $paginateModel = new PaginateModel();
        }
        if (blank($query)) {
            $query = [];
        }

        //lọc theo người tạo lịch
        if (!empty($creator)) {
            $query['created_by'] = $creator;
        }
        //Lọc theo thời gian
        if (!empty($query['from_date'])) {
            array_push($fromDateFilter, ["end_time", '>=', $query['from_date']]);

        }

        if (!empty($query['to_date'])) {
            array_push($toDateFilter, ["start_time", '<=', $query['to_date']]);
        }
        if (!empty($query['recurring']) && $query['recurring'] == 1) {
            $query['is_recurring'] = 1;
        }

        if ($isActive == -1) {
            $data = $this->recordPlanRepository->allQuery($query)->onlyTrashed()->orderBy("created_at", "desc");
        } else {
            $data = $this->recordPlanRepository->allQuery($query)->where($fromDateFilter)->where($toDateFilter)
                ->orderBy("start_time", "desc")
                ->orderByRaw("
                CASE WHEN DATEDIFF(start_time, CURRENT_TIME()) = 0 THEN DATEDIFF(start_time, CURRENT_TIME()) END DESC,
                CASE WHEN DATEDIFF(start_time, CURRENT_TIME()) > 0 THEN 1 END DESC,
                CASE WHEN DATEDIFF(start_time, CURRENT_TIME()) > 0 THEN DATEDIFF(start_time, CURRENT_TIME()) END ASC,
                DATEDIFF(start_time, CURRENT_TIME()) DESC,
                CASE
                    WHEN  status = ? THEN 1
                    WHEN  status = ? THEN 2
                    WHEN  status = ? THEN 3
                    WHEN  status = ? THEN 4
                    WHEN  status = ? THEN 5
                ELSE 6 END",
                [
                    config('common.status.record_plan.pending'),
                    config('common.status.record_plan.approved'),
                    config('common.status.record_plan.in_progress'),
                    config('common.status.record_plan.wrap'),
                    config('common.status.record_plan.done'),
                ]);
        }
        if ($paginateModel) {
            return $paginateModel->toPaging($data);
        }
        return PaginateModel::All()->toPaging($data)->items() ?? [];
    }

    /**
     * @param $departmentCode
     * @param $query
     * @param $allocate_status
     * @param PaginateModel|null $paginateModel if == null => get all else => paging
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    function getProposalsByDepartmentCode($departmentCode, $query, $allocate_status, PaginateModel $paginateModel = null)
    {

        $data = $this->recordPlanRepository->getProposalsByDepartmentCode($departmentCode, $query, $allocate_status);
        if (!empty($query['lendingDevice'])) {
            switch ($query['lendingDevice']) {
                case "lendingOnTime":
                    $data = $data->whereHas('lendingDevicesOnTime');
                    break;
                case "lendingOutTime":
                    $data = $data->whereHas('lendingDevicesOutTime');
                    break;
                case "notGive":
                    $data = $data->whereHas('notGivenDevice');
                    break;
                case "lendingDevice":
                    $data = $data->whereHas('lendingDevices');
                    break;
            }
        }
        if ($paginateModel) {
            return $paginateModel->toPaging($data);
        }
        return PaginateModel::All()->toPaging($data)->items() ?? [];
    }

    function countProposalsByDepartmentCode($departmentCode, $query, $allocate_status = '')
    {
        $data = $this->recordPlanRepository->getProposalsByDepartmentCode($departmentCode, $query, $allocate_status);
        return $data->count();
    }

    function countProposalsByLendingStatus($departmentCode, $query, $allocate_status = '', $lendingDevice = '')
    {
        $data = $this->recordPlanRepository->getProposalsByDepartmentCode($departmentCode, $query, $allocate_status);
        switch ($lendingDevice) {
            case "lendingOnTime":
                $data->whereHas('lendingDevicesOnTime');
                break;
            case "lendingOutTime":
                $data->whereHas('lendingDevicesOutTime');
                break;
            case "notGive":
                $data->whereHas('notGivenDevice');
                break;
            case "lendingDevices":
                $data->whereHas('lendingDevices');
                break;
        }

        return $data->count();
    }

    public function updateGeneralStatus($id, Request $request)
    {
        $status = $request->status;
        $record_plan = $this->recordPlanRepository->find($id);
        // nếu chuyển trạng thái từ đã chốt -> đang diễn ra thì cần kiểm tra:
        // 1. Thời gian lịch sx
        // 2. Nhân sự được phân công cho lịch sx
        $current_time = time();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $record_plan_start_time = strtotime($record_plan->start_time);
        /* if ($status == config('common.status.record_plan.in_progress')) {
             if ($current_time < $record_plan_start_time) {
                 throw new ValidateException(__("notification.record_plan.notYetTime"));
             } elseif (!$this->checkResourcesForPreparingInProgress($record_plan)) {
                 throw new ValidateException(__("notification.record_plan.notEnoughPeople"));
             } else {
                 $record_plan->update(["status" => $status]);
             }
         } else {
             $record_plan->update(["status" => $status]);
         }*/
        $record_plan->update(["status" => $status]);

        if ($status == config("common.status.record_plan.cancel")) {
            $this->handleAssignedResourcesForCanceledRecordPlan($record_plan);
            $this->sendMessagesToAssignedResources();
        } elseif ($status == config("common.status.record_plan.done")) {
            $this->handleAssignedResourcesForDoneRecordPlan($record_plan);
        }
        return true;
    }


    public function getAssignComing(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $memberId = auth("api")->user()->id;
        $recPlanXrefs = $this->memberService->getAssignmentsByMember($request, $memberId);

        //time today
        $now = Carbon::now();
        $startOfToday = $now->copy()->startOfDay()->unix();
        $endOfToday = $now->copy()->endOfDay()->unix();
        //time tomorrow
        $tomorrow = Carbon::tomorrow();
        $startOfTomorrow = $tomorrow->copy()->startOfDay()->unix();
        $endOfTomorrow = $tomorrow->copy()->endOfDay()->unix();
        //time 7 ngay toi

        $data = [];
        $status = [
            config("common.status.record_plan.pending"),
            config("common.status.record_plan.approved"),
            config("common.status.record_plan.in_progress"),
        ];
        foreach ($recPlanXrefs as $recPlanXref) {
            $recPlan = $recPlanXref->record_plan;
            $jobStatus = $recPlanXref->transportation_status;
            $recPlan->{"transportation_status"} = $jobStatus;
            $startTime = strtotime($recPlanXref->start_time);

            $endTime = strtotime($recPlanXref->end_time);
            //tìm kiếm lịch của member thuộc hôm nay, ngày mai hay sắp tới
            /*
                - lịch hôm nay: Láy lịch có khoảng thời gian giao với hôm nay
                    + Kiểm tra date của start_time <= today && endTime >= today
                    + status != đóng máy, hoàn thành, hủy
                - lịch ngày mai:
                    + Kiểm tra date của start_time <= tomorrow && endTime >= tomorrow
                    + status != đóng máy, hoàn thành, hủy
                - Lịch sắp tới trong tuần:
                    + Kiểm tra startTime < today + 7, endTime > end of today
                    + status != đóng máy, hoàn thành, hủy
            */

            // hôm nay
            if ($startTime <= $endOfToday && $endTime >= $startOfToday && in_array($recPlan->status, $status)) {
                $data["today"][] = $recPlan;
            }

            // ngày mai
            if ($startTime <= $endOfTomorrow && $endTime > $endOfToday && in_array($recPlan->status, $status)) {
                $data["tomorrow"][] = $recPlan;
            }
        }
        return $data;
    }

    public function addXrefsInfoToRecordPlans($recordPlans)
    {
        foreach ($recordPlans as $item) {
            $members = $item->members;
            $devices = $item->devices;
            $reporter = [];
            $cam = [];
            $device_array = [];
            $car_array = [];
            $driver = [];
            $technical_cam = [];
            foreach ($members as $member) {
                $member_info = $member->full_name . " (" . $member->phone_number . ") " . "\n" ;
                $car_info = optional($member->car)->name . " - " . optional($member->car)->license_plate;
                if (str_contains($member->department_code, "production")) {
                    $reporter[] = $member_info;
                } elseif ($member->department_code == config("common.departments.camera")) {
                    $cam[] = $member_info;
                } elseif ($member->department_code == config("common.departments.transportation")) {
                    $driver[] = $member_info;
                    $car_array[] = $car_info;
                }elseif ($member->department_code == config("common.departments.technical_camera")){
                    $technical_cam[] = $member_info;
                }
            }

            foreach ($devices as $device) {
                $device_info = $device->name . "\n";
                $device_array[] = $device_info;
            }
            $item->reporters = join(" ", $reporter);
            $item->cams = join(" ", $cam);
            $item->cars = join(" ", $car_array);
            $item->devices = join(" ", $device_array);
            $item->drivers = join(" ", $driver);
            $item->technical_cams = join("", $technical_cam);
        }
        return $recordPlans;
    }

    public function prepareRecordPlansForExport($data, $isExportAllInfo = false)
    {
        $index = 1;
        foreach ($data as $item) {
            $item->{'no'} = $index++;
            $item->{'start_date'} = Date::PHPToExcel(date("d/m/Y", strtotime($item->start_time)));
            $item->{'start_time'} = Date::PHPToExcel(date("d/m/Y H:i", strtotime($item->start_time))) - Date::PHPToExcel(date("d/m/Y", strtotime($item->start_time)));
            $item->{'end_date'} = Date::PHPToExcel(date("d/m/Y", strtotime($item->end_time)));
            $item->{'end_time'} = Date::PHPToExcel(date("d/m/Y H:i", strtotime($item->end_time))) - Date::PHPToExcel(date("d/m/Y", strtotime($item->end_time)));
            $item->{'plan_code'} = optional($item->plan)->code;
            $item->{'offer_reporter_note'} = $item->reporters;
            $item->{'offer_transportation_note'} = $item->drivers;
            $item->{'offer_camera_note'} = $item->cams;
            $item->{'is_allDay'} = $item->is_allday ? "Có" : "Không";
            $item->{"department_code"} = __("department." . $item->department_code);
            $item->{"offer_device_note"} = $item->devices;
            $item->{"offer_technical_camera_note"} = $item->technical_cams;
            $item->{"actual_address"} = $item->actual_address;
        }

        // kiểm tra, xuất template excel theo department_code
        if ((!empty(auth()->user()) && str_contains(auth()->user()->department_code, "production") && auth()->user()->is_manager == 1) || (!empty(auth()->user()) && auth()->user()->hasRole('super admin')) || $isExportAllInfo) {
            $titles = [
                "no", "department_code", "name", "offer_reporter_note", "offer_technical_camera_number", "offer_technical_camera_note", "offer_camera_number", "offer_camera_note",
                "start_date", "start_time", "end_date", "end_time", "actual_address", "offer_transportation_number", "offer_transportation_note", "offer_device_number", "offer_device_note",
            ];
            $fileName = "record_plans-export.xlsx";
        } else {
            $offer_number = "offer_" . auth()->user()->department_code . "_number";
            $offer_note = "offer_" . auth()->user()->department_code . "_note";
            $titles = ["no", "name", $offer_number, $offer_note,
                "start_date", "start_time", "end_date", "end_time",  "address",];
            $fileName = "record_plans_part_export.xlsx";
            $result['header'] = [
                "C4" => __('data_field_name.record_plan.proposal_number.' . auth()->user()->department_code),
                "D4" => __('data_field_name.model_name.' . auth()->user()->department_code)
            ];
        }
        $result["data"] = $data;
        $result["titles"] = $titles;
//        dd($result);
        return [
            "result" => $result,
            "fileName" => $fileName
        ];

    }

    public function getDailyRecordPlans($fromDate, $toDate, $status)
    {
        $query = [
            "from_date" => $fromDate,
            "to_date" => $toDate,
            "status" => $status
        ];
        $data = $this->searchRecordPlan($query, null);
        $data = $this->addXrefsInfoToRecordPlans($data);
        $data = $this->prepareRecordPlansForExport($data, true);
//        dd($data);
//        dd($startOfDay, $endOfDay);
        $template = public_path()."/templates/record_plans-export.xlsx";
        $spreadsheet = IOFactory::load($template);
        $worksheet = $spreadsheet->getActiveSheet();

        $this->excelUtil->writeDataToSheet($data["result"], $worksheet, 5);

        //Tạo file excel
        $newFileName = $this->excelUtil->createFileName("DSLichSanXuat");

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $fileName = public_path() . '/excel_exported/' . $newFileName;
        $writer->save($fileName);
        return config("app.url"). '/excel_exported/' . $newFileName;
    }

    public function autoCreateRecordPlanRecurring()
    {
        $weekMap = [
            0 => 'on_sunday',
            1 => 'on_monday',
            2 => 'on_tuesday',
            3 => 'on_wednesday',
            4 => 'on_thursday',
            5 => 'on_friday',
            6 => 'on_saturday',
        ];
        $tomorrow = Carbon::tomorrow();
        $dayOfWeek = $tomorrow->clone()->dayOfWeek;
        Log::info("dayOfWeek: $weekMap[$dayOfWeek]");

        $recurringPlans = RecordPlan::where('is_recurring', 1)->where('status', '<>', -1)->where($weekMap[$dayOfWeek], 1)->get();
        Log::info("dayOfWeek: ", [$recurringPlans]);
        date_default_timezone_set("Asia/Bangkok");
        foreach ($recurringPlans as $recurringPlan) {
            try {
                // chỉ thiết lập lịch khi date(start_time) != date(tomorrow)
                $createRecurring = date('Y-m-d', strtotime($recurringPlan->start_time)) != date('Y-m-d', $tomorrow->clone()->unix())
                    && date('Y-m-d', $tomorrow->clone()->unix()) >  date('Y-m-d', strtotime($recurringPlan->start_time));
                if($createRecurring){
                    Log::info("Cloning RecordPlan: ", [$recurringPlan]);
                    $newPlan = $recurringPlan->replicate();
                    $startTime = Carbon::create($newPlan->start_time);
                    $startTime->setDate($tomorrow->year, $tomorrow->month, $tomorrow->day);
                    $endTime = Carbon::create($newPlan->end_time);
                    $endTime->setDate($tomorrow->year, $tomorrow->month, $tomorrow->day);
                    //Bỏ thiết lập lặp lại của lịch được clone ra
                    $newPlan->is_recurring = 0;
                    //Set lại thời gian của lịch lặp lại thành ngày kia
                    $newPlan->start_time = $startTime;
                    $newPlan->end_time = $endTime;

                    //Cập nhật trạng thái của lịch sản xuất thành chưa phân bổ
                    $newPlan->reporter_allocate_status = ($newPlan->offer_reporter_number == 0 || $newPlan->offer_reporter_number == null) ? 2 : 1;
                    $newPlan->camera_allocate_status = ($newPlan->offer_camera_number == 0 || $newPlan->offer_camera_number == null) ? 2 : 1;
                    $newPlan->transportation_allocate_status = ($newPlan->offer_transportation_number == 0 || $newPlan->offer_transportation_number == null) ? 2 : 1;
                    $newPlan->device_allocate_status = ($newPlan->offer_device_number == 0 || $newPlan->offer_device_number == null) ? 2 : 1;
                    $newPlan->technical_camera_allocate_status = ($newPlan->offer_technical_camera_number == 0 || $newPlan->offer_technical_camera_number == null) ? 2 : 1;

                    if($newPlan->technical_camera_allocate_status == 2 && $newPlan->device_allocate_status == 2 && $newPlan->transportation_allocate_status== 2
                        && $newPlan->camera_allocate_status == 2 && $newPlan->reporter_allocate_status == 2){
                        $newPlan->status = config("common.status.record_plan.approved");
                    }else{
                        $newPlan->status = config("common.status.record_plan.pending");
                    }
                    Log::info("Cloning ....: saving new plan", [$newPlan->name, $newPlan->start_time->toDateTimeString(), $newPlan->end_time->toDateTimeString()]);
                    $newPlan->save();
                    Log::info("Cloning ....: saved new plan", [$newPlan->id, $newPlan->name, $newPlan->start_time->toDateTimeString(), $newPlan->end_time->toDateTimeString()]);
                    //loại bỏ người/thiết bị đã phân vào lịch để thực hiện phân công lại trừ phóng viên;
                    $assignedReporters = $recurringPlan->members->filter(function ($member) {
                        return str_contains($member->department_code, "production");
                    });
                    $assignedReporterIds = $assignedReporters->map(function ($member){
                        return $member->id;
                    })->toArray();
                    $assignmentService = app("App\Services\AssignmentService");
                    Log::info("Cloning ....: assign member", [$assignedReporterIds]);
                    if (!blank($assignedReporterIds))
                        $assignmentService->assignMembersToRecordPlan($newPlan->id, "reporter", $assignedReporterIds);

                    Log::info("Cloned RecordPlan: ", [$recurringPlan->name]);
                }else {
                    Log::info("Ignore clone RecordPlan: ", [$recurringPlan->name]);
                }
            }catch (Exception $e) {
                Log::error("Fail to clone RecordPlan");
                Log::error($e);
            }

        }

        //Lấy tất cả các lịch có recurirng == true và có chọn vào ngày mai
    }
    public function checkRecordPlanExistsInStage($address, $startTime, $recordPlanId = null) {
        $response = [
            "status" => "0000",
            "record_plan" => "",
            "stage" => ""
        ];

        $recordPlanInStage = $this->recordPlanRepository->allQuery()->where([
            ['address_type', '=', config("common.address_type.useStage")],
            ['id', '<>', $recordPlanId],
            ['address', '=', $address],
            ['start_time', '<=', $startTime],
            ['end_time', '>=', $startTime],
        ])->first();
        // ->where(function ($query) use ($newData) {
        //     return $query->where('on_monday','=',$newData['on_monday'])
        //                 ->orWhere('on_tuesday','=',$newData['on_tuesday'])
        //                 ->orWhere('on_wednesday','=',$newData['on_wednesday'])
        //                 ->orWhere('on_thursday','=',$newData['on_thursday'])
        //                 ->orWhere('on_friday','=',$newData['on_friday'])
        //                 ->orWhere('on_saturday','=',$newData['on_saturday'])
        //                 ->orWhere('on_sunday','=',$newData['on_sunday']);
        // })

        if (!empty($recordPlanInStage)) {
            $response["status"] = "FOUND";
            $response["record_plan"] = $recordPlanInStage->name;
            $response["stage"] = $recordPlanInStage->actual_address;
        }

        return $response;
    }

}
