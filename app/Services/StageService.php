<?php


namespace App\Services;


use App\Repositories\CarRepository;
use App\Repositories\MemberRepository;
use App\Repositories\StageRepository;
use App\Services\Interfaces\IStageService;
use Illuminate\Http\Request;

class StageService implements IStageService
{
    public $stageRepository;
    public function __construct( StageRepository $stageRepository)
    {
        $this->stageRepository = $stageRepository;
    }

    public function getAllStages(Request $request)
    {
        $query = [];
        if($request->has("s")){
            $query["s"] = $request->s;
        }
        return $this->stageRepository->allQuery($query)->where(["status" => 1])->get();
    }
}
