<?php


namespace App\Utils;


use App\Services\MochaService;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class CommonUtil
{
    public function __construct()
    {}

    public static function backPage($total, $currentPage, $perPage, $lastPage, $ids,$prefix,$deleted = true){
        // Tính số bản ghi trên trang hiện tại
        $from = $total > 0 ? ($currentPage - 1) *  $perPage + 1 : 0;
        $to = $currentPage == $lastPage ? $total : $currentPage *  $perPage ;
        $countItem = ($to - $from) + 1 ;
        //set url back
        // nếu xóa toàn bộ bản ghi của trang hiện tại thì di chuyển về trang trước đó
        $url =  $countItem == count($ids) && ($currentPage == $lastPage) && $deleted == true ? '/'.$prefix.'/?page='.($currentPage - 1) : '/'.$prefix.'/?page='.$currentPage;
        return $url;
    }

    public static function Categ($category)
    {
        if ($category->parent_id == 0 && $category->parent == null) {
            return $category->name;
        }
        return $category->parent->name.' / '.$category->name;
    }
}
