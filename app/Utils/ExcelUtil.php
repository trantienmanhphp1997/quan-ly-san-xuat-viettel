<?php


namespace App\Utils;


use PhpOffice\PhpSpreadsheet\IOFactory;

class ExcelUtil
{
    public static function stripVN($str)
    {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);

        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        return $str;
    }

    public function formatChar($string)
    {
        $normalized_string = $this->stripVN($string);
        $removeSpecialCharacter = preg_replace('/\W/', '_', $normalized_string);
        $string = strtolower($removeSpecialCharacter);
        $new_string = explode(" ", $string);
        return join("_", $new_string);
    }

    public function createFileName($file_name, $other_info = null)
    {
        $date = date("YmdHis");
        $other_info = $this->formatChar($other_info);
        $newFileName = $file_name . "_" . $date . ".xlsx";
        if($other_info){
            $newFileName = $file_name . "_" . $other_info . "_" . $date . ".xlsx";
        }
        return $newFileName;
    }

    public function getColumnName($columnIndex)
    {
        $dividend = $columnIndex;
        $columnName = "";
        while ($dividend > 0) {
            $modulo = ($dividend - 1) % 26;
            $columnName = chr(65 + $modulo) . $columnName;
            $dividend = (int)(($dividend - $modulo) / 26);
        }

        return $columnName;
    }

    public function getCellName($columnIndex, $rowIndex)
    {
        return $this->getColumnName($columnIndex) . $rowIndex;
    }

    public function writeDataToSheet($data, $worksheet, $firstRowIndex, $firstDataColumn = 1)
    {
        $surveyData = $data['data'];
        $titles = $data['titles'];
        $headers = isset($data['header']) ? $data['header'] : [] ;
        if(count($headers) > 0) {
            foreach ($headers as $key => $value) {
                $worksheet->setCellValue($key, $value);
            }
        }
        $worksheet->insertNewRowBefore(($firstRowIndex + 1), count($surveyData));
        foreach ($surveyData as $rowIndex => $datum) {
            foreach ($titles as $columnIndex => $title)
                $worksheet->setCellValue($this->getCellName($columnIndex + $firstDataColumn, $rowIndex + $firstRowIndex), $datum[$title]);
        }
        // Make all cells align top
        $worksheet->getStyle('A1:'. $this->getCellName($columnIndex + $firstDataColumn, $rowIndex + $firstRowIndex))
        ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $worksheet->removeRow($firstRowIndex + count($surveyData), 2);

    }


    public function exportExcel($data, $file_name, $template, $firstRow, $other_file_name_info = null)
    {
        $spreadsheet = IOFactory::load($template);
        $worksheet = $spreadsheet->getActiveSheet();

        //Ghi dữ liệu vào sheet
        $this->writeDataToSheet($data, $worksheet, $firstRow);

        //Tạo file excel
        $newFileName = $this->createFileName($file_name, $other_file_name_info);

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save(storage_path() . '/excel_exported/' . $newFileName);
//        return $newFileName;
        response()->download(storage_path() . '/excel_exported/' . $newFileName)->setStatusCode(200);
        response()->download(storage_path() . '/excel_exported/' . $newFileName)->send();
    }

}
