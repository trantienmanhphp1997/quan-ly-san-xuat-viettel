<?php


namespace App\Utils;


use App\Services\MochaService;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class MochaUtil
{
    public $mochaService;
    public function __construct(MochaService $mochaService)
    {
        $this->mochaService = $mochaService;
    }

    public function sendMessage($service_id, $message_content, $phone_numbers, $label, $link){
        if ($service_id) {
            try {
                $rs = $this->mochaService->sendMessageToPhoneNumbers($service_id, $message_content, $phone_numbers, $label, $link);
            } catch (\Exception $exception) {
                return response()->json([
                    'status' => '422',
                    'msg' => "Failed to connect to sending messages center",
                    'errors' => 'Error'], 422);
            }

            Log::info(config("mocha.send_message_url"));
            Log::info($rs->getStatusCode());
            Log::info(json_encode($rs->getBody()));
            return $rs->json();
        } else {
            return response()->json([
                'status' => '422',
                'msg' => "No service id",
                'errors' => 'Error'], 422);
        }
    }
}
