<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AssignMemberTab extends Component
{
    public $id;
    public $labelledby;
    public $title;
    public $formName;
    public $formId;
    public $status;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id, $labelledby, $title, $formName, $formId, $status=null)
    {
        $this->id = $id;
        $this->labelledby = $labelledby;
        $this->title = $title;
        $this->formName = $formName;
        $this->formId = $formId;
        $this->status = $status;
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.assign-member-tab');
    }
}
