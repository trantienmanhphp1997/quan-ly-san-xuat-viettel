<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CardResource extends Component
{
    public $title;
    public $members;
    public $memberAssgined;
    public $recordPlan;
    public $proposals;
    public $note;
    public $departmentCode;
    public $recordPlanId;
    public $nodataMessage;
    public $outsourceNote;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $members,$memberAssgined = [], $recordPlan, $proposals = false, $note = false, $departmentCode, $recordPlanId, $nodataMessage='',  $outsourceNote="")
    {
        $this->title = $title;
        $this->members = $members;
        $this->memberAssgined = $memberAssgined;
        $this->recordPlan = $recordPlan;
        $this->proposals = $proposals;
        $this->note = $note;
        $this->departmentCode = $departmentCode;
        $this->recordPlanId = $recordPlanId;
        $this->nodataMessage = $nodataMessage;
        $this->outsourceNote = $outsourceNote;
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.card-resource');
    }
}
