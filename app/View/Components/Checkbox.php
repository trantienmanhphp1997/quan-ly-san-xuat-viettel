<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Checkbox extends Component
{
    public $formName;
    public $label;
    public $checked;
    public $disabled;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($formName, $label, $checked, $disabled)
    {
        $this->formName = $formName;
        $this->label = $label;
        $this->checked = $checked;
        $this->disabled = $disabled;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.checkbox');
    }
}
