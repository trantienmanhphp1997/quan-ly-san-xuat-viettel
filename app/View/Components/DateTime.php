<?php

namespace App\View\Components;

use Illuminate\View\Component;

class DateTime extends Component
{
    public $formName;
    public $label;
    public $value;
    public $required;
    public $disabled;
    public $horizontal;
    public $styleInput;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($formName, $label, $value, $required, $disabled=false, $horizontal=false, $styleInput=false)
    {
        $this->formName = $formName;
        $this->label = $label;
        $this->value = $value;
        $this->required = $required;
        $this->disabled = $disabled;
        $this->horizontal = $horizontal;
        $this->styleInput = $styleInput;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.date-time');
    }
}
