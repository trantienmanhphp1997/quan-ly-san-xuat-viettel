<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{
    public $type;
    public $label;
    public $formName;
    public $value;
    public $required;
    public $disabled;
    public $placeholder;
    public $maxlength;
    public $styleAll;
    public $styleLabel;
    public $styleInput;
    public $min;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($type, $label, $formName, $value, $required, $disabled=false, $placeholder, $maxlength=null, $styleAll=false, $styleLabel=false, $styleInput=false, $min = 1)
    {
        $this->type = $type;
        $this->label = $label;
        $this->formName = $formName;
        $this->value = $value;
        $this->required = $required;
        $this->disabled = $disabled;
        $this->placeholder = $placeholder;
        $this->maxlength = $maxlength;
        $this->styleAll = $styleAll;
        $this->styleLabel = $styleLabel;
        $this->styleInput = $styleInput;
        $this->min = $min;
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.input');
    }
}
