<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Textarea extends Component
{
    public $formName;
    public $label;
    public $value;
    public $required;
    public $disabled;
    public $placeholder;
    public $horizontal;
    public $row;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($formName, $label, $value, $required=false,$disabled=false, $placeholder, $horizontal=false, $row = 2)
    {
        $this->formName = $formName;
        $this->label = $label;
        $this->value = $value;
        $this->required = $required;
        $this->disabled = $disabled;
        $this->placeholder = $placeholder;
        $this->horizontal = $horizontal;
        $this->row = $row;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.textarea');
    }
}
