<?php


namespace App\ViewModel;


class PaginateModel
{
    public static $PER_PAGE = 10;
    public static $MAX_PER_PAGE = 10000;
    private $page;
    private $perPage;
    private $columns;
    private $pageName;

    public static function All()
    {
        return new PaginateModel(PaginateModel::$MAX_PER_PAGE, 1);
    }

    public function __construct($perPage = null, $page = null)
    {
        $this->page = $page;
        $this->perPage = $perPage ?? PaginateModel::$PER_PAGE;
        if ($this->perPage > PaginateModel::$MAX_PER_PAGE) {
            $this->perPage = PaginateModel::$MAX_PER_PAGE;
        }
        $this->columns = ['*'];
        $this->pageName = 'page';
    }

    public function toPaging(\Illuminate\Database\Eloquent\Builder $eloquentBuilder)
    {
        return $eloquentBuilder->paginate(
            $this->getPerPage(),
            $this->getColumns(),
            $this->getPageName(),
            $this->getPage()
        );
    }

    /**
     * @return null
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param null $page
     */
    public function setPage($page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     */
    public function setPerPage(int $perPage): void
    {
        $this->perPage = $perPage;
    }

    /**
     * @return mixed
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param mixed $columns
     */
    public function setColumns($columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @return mixed
     */
    public function getPageName()
    {
        return $this->pageName;
    }

    /**
     * @param mixed $pageName
     */
    public function setPageName($pageName): void
    {
        $this->pageName = $pageName;
    }


}
