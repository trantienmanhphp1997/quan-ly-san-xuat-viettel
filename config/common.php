<?php
return [
    "groups" => [
        "admin" => [
            "id" => 1,
            "code" => "admin",
            "display_name" => "Administrator",
        ],
        "reporter" => [
            "id" => 2,
            "code" => "reporter",
            "display_name" => "Reporter"
        ],
        "camera" => [
            "id" => 3,
            "code" => "camera",
            "display_name" => "Cameraman"
        ],
        "device" => [
            "id" => 4,
            "code" => "device",
            "display_name" => "Device"
        ],
        "transportation" => [
            "id" => 5,
            "code" => "transportation",
            "display_name" => "Driver"
        ]
    ],
    "departments" => [
        "production" => "production",
        "camera" => "camera",
        "technical_camera" => "technical_camera",
        "device" => "device",
        "transportation" => "transportation",
        "accounting" => "accounting",
        "general_news" => "production_general_news",
        "news" => "production_general_news_news",
        "documentary" => "production_general_news_documentary",
        "entertainment" => "production_entertainment",
        "music" => "production_entertainment_music",
        "film" => "production_entertainment_film",
    ],
    "status" => [
        "record_plan" => [
//            "inactive" => 0,
            "pending" => 1,
            "approved" => 2,
            "in_progress" => 3,
            "wrap" => 4, //Đã đóng máy, chưa chốt xong giấy tờ
            "done" => 5,
            "cancel" => -1
        ],
        "record_plan_change_status" =>[ //không quan tâm
            "inactive" => 0,
            "pending" => 1,
            "approved" => 2,
            "in_progress" => 3,
            "wrap" => 4, //Đã đóng máy, chưa chốt xong giấy tờ
            "done" => 5,
            "cancel" => -1
        ],
        "propose" => [
            "pending" => 1,
            "approved" => 2,
            "record_plan_done" => 5,
            "rejected" => -1,
            "cancel" => -2,
            "record_plan_cancelled" => -3,

        ],
        "propose_device" => [
            "pending" => 1,
            "approved" => 2,
            "delivered" => 3,
            "returned" => 4,
            "record_plan_done" => 5,
            "rejected" => -1,
            "duplicated" => -2,
            "record_plan_cancelled" => -3,
            "pending_return" => -4
        ],
        "member" => [
            "inactive" => 0,
            "active" => 1
        ],
        "plan" => [
            "inactive" => 0,
            "active" => 1
        ],
        "stage" => [
            "inactive" => 0,
            "active" => 1
        ],
        "proposals"=>[
            "unassigned" => 1,
            "assigned" => 2
        ],
        "car" => [
            "inactive" => 0,
            "active" => 1,
//            "busy" => 2,
            "broken" => 3,
        ],
        "device" => [
            "inactive" => 0,
            "active" => 1,
//            "borrowed" => 2,
            "broken" => 3,
        ],
        "device_category" => [
            "active" => 1,
        ],
        "assignments" => [
            "devices" => [
                "proposal" => 1,
                "accept" => 2,
                "cancel" => 3
            ],
            "members" => [
                "proposal" => 1,
                "accept" => 2,
                "cancel" => 3
            ]
        ]
    ],
    "file_template" => [
        "camera" => "Import_QuayPhim.xlsx",
        "technical_camera" => "Import_KTPQ.xlsx",
        "reporter" => "Import_PhongVien.xlsx",
        "cars" => "Import_Xe.xlsx",
        "plans" => "Import_KeHoachSX.xlsx",
        "transportation" => "Import_LaiXe.xlsx",
        "statements" => "Import_ToTrinh.xlsx",
        "contracts" => "Import_HopDong.xlsx",
        "record-plans" => "Import_LichSanXuats.xlsx",
        "devices" => "Import_ThietBi.xlsx"
    ],
    "sendMessageType" => [
        "invite" => "invited",
        "update_time" => "update_time",
        "update_place" => "update_place",
        "update_time_place" => "update_time_place",
        "update_offer" => "update_offer"
    ],
    "is_allday" => [
        "không"=> 0,
        "no" => 0,
        "có" => 1,
        "yes" => 1
    ],
    "transportation_involve_type" =>[
        "go_only" => "go_only",
        "return_only" => "return_only",
        "round_trip" => "round_trip",
        "along_with_trip" => "along_with_trip"
    ],
    "transportation_status" => [
        "on_trip" => 1,
        "finished" => 2
    ],
    "camera_status" => [
        "on_trip" => 1,
        "finished" => 2
    ],
    "address_type" => [
        "urban" => 0,
        "interMunicipal" => 1,
        "useStage" => 2
    ]
];
