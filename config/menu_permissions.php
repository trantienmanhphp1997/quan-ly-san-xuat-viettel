<?php
$featurePermissions = [
    "create",
    "view",
    "view_any",
    "update",
    "update_any",
    "delete",
    "delete_any",
    "restore",
//    "import",
//    "export"
];

$accessPermissions = [
    "access"
];

return [
    "feature_permissions" => $featurePermissions,
    "permissions" => [
        "record_plans" => array_merge($accessPermissions, $featurePermissions),
        "camera" => $accessPermissions,
        "camera_list" => array_merge($accessPermissions, $featurePermissions),
        "camera_assignment" => array_merge($accessPermissions, $featurePermissions),
        "camera_proposal" => array_merge($accessPermissions, $featurePermissions),
        "reporter" => $accessPermissions,
        "reporter_list" => array_merge($accessPermissions, $featurePermissions),
        "reporter_assignment" => array_merge($accessPermissions, $featurePermissions),
        "reporter_proposal" => array_merge($accessPermissions, $featurePermissions),
        "transportation" => $accessPermissions,
        "transportation_list" => array_merge($accessPermissions, $featurePermissions),
        "transportation_assignment" => array_merge($accessPermissions, $featurePermissions),
        "transportation_proposal" => array_merge($accessPermissions, $featurePermissions),
        "cars" => array_merge($accessPermissions, $featurePermissions),
        "devices" => $accessPermissions,
        "device_list" => array_merge($accessPermissions, $featurePermissions),
        "device_assignment" => array_merge($accessPermissions, $featurePermissions),
        "device_categories" => array_merge($accessPermissions, $featurePermissions),
        "device_proposal" => array_merge($accessPermissions, $featurePermissions),
        "device_lending" => array_merge($accessPermissions, $featurePermissions),
        "device_returning" => array_merge($accessPermissions, $featurePermissions),
        "outsources" => $accessPermissions,
        "outsource_types" => array_merge($accessPermissions, $featurePermissions),
        "outsource_list" => array_merge($accessPermissions, $featurePermissions),
        "plans" => array_merge($accessPermissions, $featurePermissions),
        "departments" => array_merge($accessPermissions, $featurePermissions),
        "managers" => array_merge($accessPermissions, $featurePermissions),
        "statements" => array_merge($accessPermissions, $featurePermissions),
        "contracts" => array_merge($accessPermissions, $featurePermissions),
    ]
];
