<?php
return [
    "send_message_url" => env("SEND_MESSAGE_URL", ""),
    "get_student_info_by_uuid" => env("GET_STUDENT_INFO_BY_UUID",""),
    "wap_url" => env("WAP_URL", "http://wapdev.quanlysanxuat.tk"),
    "oa_id" => env("OA_ID", "oa_id")
];
