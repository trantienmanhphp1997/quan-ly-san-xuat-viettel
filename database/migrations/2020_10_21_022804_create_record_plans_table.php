<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text("name");
            $table->text("description")->nullable();
            $table->unsignedBigInteger("plan_id")->nullable();
            $table->bigInteger("plan_category_id")->nullable();
            $table->bigInteger("version")->nullable();
            $table->dateTime("start_time");
            $table->dateTime("end_time");
            $table->tinyInteger("is_recurring")->nullable();
            $table->tinyInteger("on_monday")->nullable();
            $table->tinyInteger("on_tuesday")->nullable();
            $table->tinyInteger("on_wednesday")->nullable();
            $table->tinyInteger("on_thursday")->nullable();
            $table->tinyInteger("on_friday")->nullable();
            $table->tinyInteger("on_saturday")->nullable();
            $table->tinyInteger("on_sunday")->nullable();
            $table->text("address");
            $table->tinyInteger("reporter_allocate_status")->nullable()->default(1);
            $table->tinyInteger("camera_allocate_status")->nullable();
            $table->tinyInteger("transportation_allocate_status")->nullable();
            $table->tinyInteger("device_allocate_status")->nullable();
            $table->tinyInteger("status")->default(1);

            $table->text("notes")->nullable();
            $table->text("offer_reporter_note")->nullable();
            $table->text("offer_camera_note")->nullable();
            $table->text("offer_transportation_note")->nullable();
            $table->text("offer_device_note")->nullable();

            $table->integer("offer_reporter_number")->nullable()->default(0);
            $table->integer("offer_camera_number")->nullable()->default(0);
            $table->integer("offer_transportation_number")->nullable()->default(0);
            $table->integer("offer_device_number")->nullable()->default(0);

            $table->string("department_code")->nullable();

            // for send mocha notice
            $table->string("send_message_type")->nullable();
            $table->text("removing_member")->nullable();

            $table->timestamps();
            $table->bigInteger("created_by")->nullable();
            $table->bigInteger("updated_by")->nullable();
            $table->softDeletes();

//            $table->foreign('department_code')->references('code')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('record_plans');
    }
}
