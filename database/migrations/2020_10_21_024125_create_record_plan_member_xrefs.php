<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordPlanMemberXrefs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_plan_member_xrefs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("record_plan_id");
            $table->unsignedBigInteger("member_id");
            $table->dateTime("start_time")->nullable();
            $table->dateTime("end_time")->nullable();
            $table->tinyInteger("status");
            $table->text("note")->nullable();
            $table->text("description")->nullable();

            // for send mocha notice
            $table->string("send_message_type")->nullable();

            $table->timestamps();
            $table->bigInteger("created_by")->nullable();
            $table->bigInteger("updated_by")->nullable();
            $table->softDeletes();

            $table->foreign('record_plan_id')->references('id')->on('record_plans');
            $table->foreign('member_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('record_plan_member_xrefs');
    }
}
