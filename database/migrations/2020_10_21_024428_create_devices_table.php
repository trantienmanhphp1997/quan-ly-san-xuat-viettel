<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("device_category_id");
            $table->string("device_category_code");
            $table->string("name")->nullable();
            $table->tinyInteger("status")->nullable();
            $table->string("serial")->nullable();
            $table->text("description")->nullable();
            $table->integer("quantity")->default(1);
            $table->text("note")->nullable();
            $table->timestamps();
            $table->bigInteger("created_by")->nullable();
            $table->bigInteger("updated_by")->nullable();
            $table->softDeletes();

            $table->foreign('device_category_id')->references('id')->on('device_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
