<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordPlanDeviceXrefs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_plan_device_xrefs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("record_plan_id");
            $table->unsignedBigInteger("device_id");
            $table->dateTime("start_time")->nullable();
            $table->dateTime("end_time")->nullable();
            $table->text("note")->nullable();
            $table->tinyInteger("status")->nullable();
            $table->text("description")->nullable();
            $table->timestamps();
            $table->bigInteger("created_by")->nullable();
            $table->bigInteger("updated_by")->nullable();
            $table->softDeletes();

            $table->foreign('record_plan_id')->references('id')->on('record_plans');
            $table->foreign('device_id')->references('id')->on('devices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('record_plan_device_xrefs');
    }
}
