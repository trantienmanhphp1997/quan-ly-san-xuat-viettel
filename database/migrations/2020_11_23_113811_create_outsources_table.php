<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutsourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outsources', function (Blueprint $table) {
            $table->id();
            $table->foreignId("outsource_type_id")->constrained("outsource_types");
            $table->string("name")->nullable();
            $table->string("password")->nullable();
            $table->text("description")->nullable();
            $table->string("uuid")->nullable();
            $table->string("full_name")->nullable();
            $table->string("phone_number")->nullable();
            $table->string("email")->nullable();
            $table->text("address")->nullable();
            $table->tinyInteger("status")->nullable();
            $table->text("note")->nullable();

            $table->timestamps();
            $table->bigInteger("created_by")->nullable();
            $table->bigInteger("updated_by")->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outsources');
    }
}
