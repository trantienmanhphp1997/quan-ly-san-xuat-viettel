<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentaries', function (Blueprint $table) {
            $table->id();
            $table->text("original_name")->nullable();
            $table->string("file_name")->nullable();
            $table->string("mime_type")->nullable();
            $table->string("href")->nullable();
            $table->integer("related_document_type")->nullable();
            $table->bigInteger("related_document_id")->nullable();
            $table->string("sub_type")->nullable();
            $table->tinyInteger("status")->default(1);
            $table->text("note")->nullable();
            $table->text("description")->nullable();
            $table->timestamps();
            $table->bigInteger("created_by")->nullable();
            $table->bigInteger("updated_by")->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentaries');
    }
}
