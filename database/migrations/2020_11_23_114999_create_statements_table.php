<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statements', function (Blueprint $table) {
            $table->id();
            $table->string("code")->nullable()->unique();
            $table->text("title")->nullable();
            $table->text("description")->nullable();
            $table->foreignId("department_id")->constrained();
            $table->unsignedBigInteger("original_statement_id")->nullable();
            $table->integer("security_level")->nullable();
            $table->integer("urgent_level")->nullable();
            $table->tinyInteger("status")->default(1);
            $table->text("note")->nullable();
            $table->timestamps();
            $table->bigInteger("created_by")->nullable();
            $table->bigInteger("updated_by")->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statements');
    }
}
