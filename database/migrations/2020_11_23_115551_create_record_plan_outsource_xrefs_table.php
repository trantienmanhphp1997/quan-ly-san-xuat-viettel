<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordPlanOutsourceXrefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_plan_outsource_xrefs', function (Blueprint $table) {
            $table->id();
            $table->foreignId("outsource_id")->constrained("outsources");
            $table->foreignId("contract_id")->constrained("contracts");
            $table->bigInteger("price")->nullable();
            $table->dateTime("start_time")->nullable();
            $table->dateTime("end_time")->nullable();
            $table->tinyInteger("status")->nullable();
            $table->text("note")->nullable();
            $table->text("description")->nullable();
            $table->timestamps();
            $table->bigInteger("created_by")->nullable();
            $table->bigInteger("updated_by")->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('record_plan_outsource_xrefs');
    }
}
