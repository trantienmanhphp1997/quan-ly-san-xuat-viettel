<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_info', function (Blueprint $table) {
            $table->id();
            $table->string("code")->nullable()->unique();
            $table->integer("year")->nullable();
            $table->text("title")->nullable();
            $table->text("description")->nullable();
            $table->foreignId("department_id")->constrained("departments");
            $table->foreignId("plan_category_id")->nullable()->constrained("plan_categories");
            $table->tinyInteger("status")->default(1);
            $table->text("note")->nullable();
            $table->timestamps();
            $table->bigInteger("created_by")->nullable();
            $table->bigInteger("updated_by")->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_info');
    }
}
