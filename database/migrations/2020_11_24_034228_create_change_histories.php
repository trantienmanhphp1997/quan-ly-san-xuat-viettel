<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChangeHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('change_histories', function (Blueprint $table) {
            $table->id();
            $table->string("tracking_table")->nullable();
            $table->string("relation_table")->nullable();
            $table->unsignedBigInteger("tracking_id")->nullable();
            $table->unsignedBigInteger("relation_key")->nullable();
            $table->string("change_type")->nullable();
            $table->bigInteger("version")->nullable();
            $table->string("change_field")->nullable();
            $table->text("original_data")->nullable();
            $table->text("new_data")->nullable();
            $table->tinyInteger("status")->default(1);
            $table->text("note")->nullable();
            $table->text("description")->nullable();
            $table->timestamps();
            $table->bigInteger("created_by")->nullable();
            $table->bigInteger("updated_by")->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('record_plan_histories');
    }
}
