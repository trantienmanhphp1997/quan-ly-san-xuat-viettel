<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsAlldayToRecordPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('record_plans', function (Blueprint $table) {
            $table->boolean("is_allday")->nullable()->default(0)->after('is_recurring');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('record_plans', function (Blueprint $table) {
            $table->dropColumn('is_allday');
        });
    }
}
