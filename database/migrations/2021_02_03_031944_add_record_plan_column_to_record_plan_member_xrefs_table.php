<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordPlanColumnToRecordPlanMemberXrefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('record_plan_member_xrefs', function (Blueprint $table) {
            $table->tinyInteger("record_plan_status")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('record_plan_member_xrefs', function (Blueprint $table) {
            $table->dropColumn(["record_plan_status"]);
        });
    }
}
