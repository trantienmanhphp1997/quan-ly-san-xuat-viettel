<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHoldByToRecordPlanDeviceXrefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('record_plan_device_xrefs', function (Blueprint $table) {
            //
            $table->unsignedBigInteger("hold_by")->nullable()->after('device_id');
            $table->foreign('hold_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('record_plan_device_xrefs', function (Blueprint $table) {
            $table->dropForeign('hold_by');
            $table->dropColumn('hold_by');
        });
    }
}
