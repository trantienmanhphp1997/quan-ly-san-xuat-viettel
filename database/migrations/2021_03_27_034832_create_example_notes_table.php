<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExampleNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('example_notes', function (Blueprint $table) {
            $table->id();
            $table->text("name")->nullable();
            $table->text("type")->nullable();
            $table->string("content")->nullable();
            $table->string("code")->nullable();
            $table->tinyInteger("status")->default(1);
            $table->text("note")->nullable();
            $table->text("description")->nullable();
            $table->timestamps();
            $table->bigInteger("created_by")->nullable();
            $table->bigInteger("updated_by")->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('example_notes');
    }
}
