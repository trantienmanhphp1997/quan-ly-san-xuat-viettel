<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTechnicalCameraAllocateStatusToRecordPlans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('record_plans', function (Blueprint $table) {
            $table->tinyInteger("technical_camera_allocate_status")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('record_plans', function (Blueprint $table) {
            $table->dropColumn(["technical_camera_allocate_status"]);
        });
    }
}
