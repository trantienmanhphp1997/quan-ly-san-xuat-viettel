<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExcelImportedColumnToRecordPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('record_plans', function (Blueprint $table) {
            $table->bigInteger("excel_imported")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('record_plans', function (Blueprint $table) {
            $table->dropColumn(["excel_imported"]);
        });
    }
}
