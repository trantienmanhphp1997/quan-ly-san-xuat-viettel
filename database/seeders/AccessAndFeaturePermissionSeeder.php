<?php

namespace Database\Seeders;

use App\Models\Menu;
use App\Models\RoleHasPermission;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class AccessAndFeaturePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = Menu::all();

        $mapMenuId = [];
        foreach ($menus as $menu) {
            $mapMenuId[$menu->menu_code] = $menu;
        }

        $featurePermissions = [
            "create",
            "view",
            "view_any",
            "update",
            "update_any",
            "delete",
            "delete_any",
            "restore",
        ];

        $accessPermissions = [
            "access"
        ];

        $reporters = [
            "record_plans" => array_merge($accessPermissions, $featurePermissions),
            "reporter" => $accessPermissions,
            "reporter_list" => array_merge($accessPermissions, $featurePermissions),
            "reporter_assignment" => array_merge($accessPermissions, $featurePermissions),
            "reporter_proposal" => array_merge($accessPermissions),
            "device_assignment" => ["access", "create"],
            "outsources" => $accessPermissions,
            "outsource_types" => array_merge($accessPermissions, $featurePermissions),
            "outsource_list" => array_merge($accessPermissions, $featurePermissions),
            "plans" => array_merge($accessPermissions, $featurePermissions),
            "statements" => array_merge($accessPermissions, $featurePermissions),
            "contracts" => array_merge($accessPermissions, $featurePermissions),
        ];
        $reporter_roles = [1, 6, 7, 8, 9, 10, 11];
        // give permission to reporters
        $permission_name = [];
        foreach ($reporters as $menu => $permissions) {
            foreach ($permissions as $permission) {
                $permission_name[] = $permission . '_' . $mapMenuId[$menu]->menu_code;
            }
        }
        $permissions = Permission::whereIn("name", $permission_name)->get("id");
        $query = [];
        foreach ($permissions as $permission) {
            foreach ($reporter_roles as $role){
                $query[] = [
                    "permission_id" => $permission->id,
                    "role_id" => $role
                ];
            }
        }

        RoleHasPermission::insert($query);

        //Camera
        $camera = [
            "record_plans" => ["view", "access"],
            "camera" => $accessPermissions,
            "camera_list" => array_merge($accessPermissions, $featurePermissions),
            "camera_assignment" => array_merge($accessPermissions, $featurePermissions),
            "camera_proposal" => array_merge($accessPermissions, $featurePermissions),
            "outsources" => $accessPermissions,
            "outsource_types" => array_merge($accessPermissions, $featurePermissions),
            "outsource_list" => array_merge($accessPermissions, $featurePermissions),
        ];
        $roles = [2];
        // give permission to reporters
        $permission_name = [];
        foreach ($camera as $menu => $permissions) {
            foreach ($permissions as $permission) {
                $permission_name[] = $permission . '_' . $mapMenuId[$menu]->menu_code;
            }
        }
        $permissions = Permission::whereIn("name", $permission_name)->get("id");
        $query = [];
        foreach ($permissions as $permission) {
            foreach ($roles as $role){
                $query[] = [
                    "permission_id" => $permission->id,
                    "role_id" => $role
                ];
            }
        }

        RoleHasPermission::insert($query);

        //devices
        $devices = [
            "record_plans" => ["view", "access"],
            "devices" => $accessPermissions,
            "device_list" => array_merge($accessPermissions, $featurePermissions),
            "device_assignment" => array_merge($accessPermissions, $featurePermissions),
            "device_categories" => array_merge($accessPermissions, $featurePermissions),
            "device_proposal" => array_merge($accessPermissions, $featurePermissions),
            "device_lending" => array_merge($accessPermissions, $featurePermissions),
            "device_returning" => array_merge($accessPermissions, $featurePermissions),
            "outsources" => $accessPermissions,
            "outsource_types" => array_merge($accessPermissions, $featurePermissions),
            "outsource_list" => array_merge($accessPermissions, $featurePermissions),
        ];
        $roles = [3];
        // give permission to reporters
        $permission_name = [];
        foreach ($devices as $menu => $permissions) {
            foreach ($permissions as $permission) {
                $permission_name[] = $permission . '_' . $mapMenuId[$menu]->menu_code;
            }
        }
        $permissions = Permission::whereIn("name", $permission_name)->get("id");
        $query = [];
        foreach ($permissions as $permission) {
            foreach ($roles as $role){
                $query[] = [
                    "permission_id" => $permission->id,
                    "role_id" => $role
                ];
            }
        }

        RoleHasPermission::insert($query);

        //transportation
        $devices = [
            "record_plans" => ["view", "access"],
            "transportation" => $accessPermissions,
            "transportation_list" => array_merge($accessPermissions, $featurePermissions),
            "transportation_assignment" => array_merge($accessPermissions, $featurePermissions),
            "transportation_proposal" => array_merge($accessPermissions, $featurePermissions),
            "cars" => array_merge($accessPermissions, $featurePermissions),
            "outsources" => $accessPermissions,
            "outsource_types" => array_merge($accessPermissions, $featurePermissions),
            "outsource_list" => array_merge($accessPermissions, $featurePermissions),
        ];
        $roles = [4];
        // give permission to reporters
        $permission_name = [];
        foreach ($devices as $menu => $permissions) {
            foreach ($permissions as $permission) {
                $permission_name[] = $permission . '_' . $mapMenuId[$menu]->menu_code;
            }
        }
        $permissions = Permission::whereIn("name", $permission_name)->get("id");
        $query = [];
        foreach ($permissions as $permission) {
            foreach ($roles as $role){
                $query[] = [
                    "permission_id" => $permission->id,
                    "role_id" => $role
                ];
            }
        }

        RoleHasPermission::insert($query);

    }
}
