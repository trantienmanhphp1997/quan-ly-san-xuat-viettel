<?php

namespace Database\Seeders;

use App\Models\Car;
use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CarSeeder extends Seeder
{
    public function __construct()
    {
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $number_of_seats = [7, 29, 35];
        $model = ["Ferrari", "Ford", "Porsche", "Honda", "Lamborghini"];

        $driver_ids = User::where(["department_code" => config("common.departments.transportation")])->pluck("id");

        for ($i = 0; $i < 20; $i++) {
            Car::create([
                "driver_id" => null,
                "name" => $faker->streetName,
                "brand" =>  $faker->randomElement($model),
                "model" => $faker->name,
                "color" => $faker->colorName,
                "number_of_seats" => $faker->randomElement($number_of_seats),
                "license_plate" => $faker->randomLetter.$faker->randomLetter."-".rand(200, 500).$i,
                "status" => 1
            ]);
        }

    }
}
