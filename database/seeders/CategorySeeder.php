<?php

namespace Database\Seeders;

use App\Models\DeviceCategory;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $names = [
            "Camera", "Laptop", "Đèn pin", "Đèn kino", "Ống kính"
        ];
        for ($i = 0; $i < 10; $i++) {
            $uni = uniqid();
            DeviceCategory::insert([
                [
                    "name" => $faker->randomElement($names) . "_" . $i,
                    "display_code" => $uni,
                    "code" => $uni,
                    "parent_id" => 0,
                    "status" => 1,
                ],

            ]);
        }
    }
}
