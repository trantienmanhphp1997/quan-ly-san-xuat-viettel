<?php

namespace Database\Seeders;

use App\Models\Contract;
use App\Models\Department;
use App\Models\Partner;
use App\Models\Statement;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ContractSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $department_ids = Department::where(["status" => 1])->pluck("id");
        $statement_ids = Statement::where(["status" => 1])->pluck("id");
        $contact_title = ["thuê ngoài ", "mua ", "cấp chi phí"];
        $partner_ids = Partner::where(["status" => 1])->pluck("id");

        for($i = 0; $i<20; $i++){
            Contract::insert([
                "title" => "Hợp đồng ".$faker->randomElement($contact_title).$faker->name,
                "code" => $faker->creditCardNumber(),
                "status" => 1,
                "department_id" => $faker->randomElement($department_ids),
                "statement_id" => $faker->randomElement($statement_ids),
                "partner_id" => $faker->randomElement($partner_ids),
            ]);
        }
    }
}
