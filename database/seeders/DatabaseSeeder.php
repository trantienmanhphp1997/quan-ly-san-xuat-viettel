<?php

namespace Database\Seeders;

use App\Models\PlanCategory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleTableSeeder::class,
            DepartmentsSeeder::class,
            MenusAndPermissionsTableSeeder::class,
//            AccessAndFeaturePermissionSeeder::class,

            UserSeeder::class,
//            CategorySeeder::class,
//            CarSeeder::class,
//            DeviceSeeder::class,
//            RecordPlanSeeder::class,
//            OutsourceTypeSeeder::class,
            PartnerSeeder::class,
//            StatementSeeder::class,
//            ContractSeeder::class,
            PlanCategorySeeder::class,
//            PlanInfoSeeder::class
        ]);
    }
}
