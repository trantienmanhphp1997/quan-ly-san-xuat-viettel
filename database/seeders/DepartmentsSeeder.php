<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $production = Department::create([
            "id" => 1,
            "name" => "Phòng sản xuất",
            "description" => "Phòng sản xuất",
            "code" => config("common.departments.production"),
            "parent_id" => 0,
            "status" => 1,
        ]);
        $production->assignRole("department production");
        $camera = Department::create([
            "id" => 2,
            "name" => "Kỹ thuật phòng quay",
            "description" => "Kỹ thuật phòng quay",
            "code" => config("common.departments.camera"),
            "parent_id" => 0,
            "status" => 1,
        ]);
        $camera->assignRole("department camera");
        $device = Department::create([
            "id" => 3,
            "name" => "Quản lý thiết bị",
            "description" => "Quản lý thiết bị",
            "code" => config("common.departments.device"),
            "parent_id" => 0,
            "status" => 1,
        ]);
        $device->assignRole("department device");
        $transportation = Department::create([
            "id" => 4,
            "name" => "Vận tải",
            "description" => "Vận tải",
            "code" => config("common.departments.transportation"),
            "parent_id" => 0,
            "status" => 1,
        ]);
        $transportation->assignRole("department transportation");
        $accounting = Department::create([
            "id" => 5,
            "name" => "Tài chính - kế toán",
            "description" => "Tài chính - kế toán",
            "code" => config("common.departments.accounting"),
            "parent_id" => 0,
            "status" => 1,
        ]);
        $accounting->assignRole("department accounting");
        $generalNews = Department::create([
            "id" => 6,
            "name" => "Tin tức tổng hợp",
            "description" => "Tin tức tổng hợp",
            "code" => config("common.departments.general_news"),
            "parent_id" => 1,
            "status" => 1,
        ]);
        $generalNews->assignRole("department ".config("common.departments.general_news"));
        $news = Department::create([
            "id" => 7,
            "name" => "Thời sự",
            "description" => "Thời sự",
            "code" => config("common.departments.news"),
            "parent_id" => 6,
            "status" => 1,
        ]);
        $news->assignRole("department ".config("common.departments.news"));
        $documentary = Department::create([
            "id" => 8,
            "name" => "Phóng sự",
            "description" => "Phóng sự",
            "code" => config("common.departments.documentary"),
            "parent_id" => 6,
            "status" => 1,
        ]);
        $documentary->assignRole("department ".config("common.departments.documentary"));
        $entertainment = Department::create([
            "id" => 9,
            "name" => "Giải trí",
            "description" => "Giải trí",
            "code" => config("common.departments.entertainment"),
            "parent_id" => 1,
            "status" => 1,
        ]);
        $entertainment->assignRole("department ".config("common.departments.entertainment"));
        $music = Department::create([
            "id" => 10,
            "name" => "Âm nhạc",
            "description" => "Âm nhạc",
            "code" => config("common.departments.music"),
            "parent_id" => 9,
            "status" => 1,
        ]);
        $music->assignRole("department ".config("common.departments.music"));
        $film = Department::create([
            "id" => 11,
            "name" => "Phim truyền hình",
            "description" => "Phim truyền hình",
            "code" => config("common.departments.film"),
            "parent_id" => 9,
            "status" => 1,
        ]);
        $film->assignRole("department ".config("common.departments.film"));
    }
}
