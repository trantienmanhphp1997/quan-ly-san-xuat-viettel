<?php
namespace Database\Seeders;
use App\Models\Device;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $categories = DB::table("device_categories")->get()->pluck("id")->toArray();
        $category_codes = DB::table("device_categories")->get()->pluck("code")->toArray();
        $names  = [
            'AG-HPX250EN/Camera truyền hình 1/3" loại cầm tay P2 theo chuẩn HD/Panasonic',
            'AG-HPX372EN/Camera truyền hình 1/3" loại vác vai P2 theo chuẩn HD/HD 17x Zoom Lens (Includes)/Panasonic',
            'AJ-HPX3100G/Camcorder/01 x AJ-HPX3100GX Camcorder/01 x AJ-HVF21KG 2" B/W Viewfinder for Camcorder/01 x V-Mount Camera Plate/01 x Shoulder Strap/Panasonic',
            'Máy bộ đàm KenWood TK-P701',
            'Máy trạm di động 8770w - Bộ dựng xách tay HP Intel Core i7-3720QM Quad Core, Ram 8GB, HDD 180GB',
            'Máy tính xách tay ProBook 4740s, 6GB Dual Channel DDR3 1333MHz/750GB 7200 RPM SATA Hard Drive/HP',
            'Đèn kino Farseeing Fluorescent Soft light FD-S6x55DMX',
            'Ống kính Canon EF 16-35mm f/2.8L III USM Lens',
            'Pin IDX System Technology DUO-C98 Endura Duo 14.4V-6.6Ah 96Wh Lithium-Ion Battery (V-Mount)'
        ];
        $status = [0, 1,1,1, 3];

        for ($i = 0; $i < 50; $i++){
            Device::insert([
                [
                    "device_category_id" => $faker->randomElement($categories),
                    "device_category_code" => $faker->randomElement($category_codes),
                    "name" => $faker->randomElement($names).$i,
                    "status"=> $faker->randomElement($status),
                    "serial" => $faker->bankAccountNumber.$i
                ]
            ]);
        }
    }
}
