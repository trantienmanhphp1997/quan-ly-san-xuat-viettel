<?php

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class MenusAndPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $recordPlanMenu = Menu::create([
            "name" => "Record plans",
            "href" => "/record-plans",
            "menu_code" => "record_plans"
        ]);
        $cameraMenu = Menu::create([
            "name" => "Camera",
            "menu_type" => "dropdown",
            "menu_code" => "camera"
        ]);
        $cameraListMenu = Menu::create([
            "name" => "Camera list",
            "href" => "/camera-list",
            "parent_id" => $cameraMenu->id,
            "menu_code" => "camera_list"
        ]);
        $cameraAssignmentMenu = Menu::create([
            "name" => "Camera assignment",
            "href" => "/camera-assignment",
            "parent_id" => $cameraMenu->id,
            "menu_code" => "camera_assignment"
        ]);
        $cameraProposalMenu = Menu::create([
            "name" => "Camera proposal",
            "href" => "/camera-proposal",
            "parent_id" => $cameraMenu->id,
            "menu_code" => "camera_proposal"
        ]);
        $reporterMenu = Menu::create([
            "name" => "reporter",
            "menu_type" => "dropdown",
            "menu_code" => "reporter"
        ]);
        $reporterListMenu = Menu::create([
            "name" => "Reporter list",
            "href" => "/reporter-list",
            "parent_id" => $reporterMenu->id,
            "menu_code" => "reporter_list"
        ]);
        $reporterAssignmentMenu = Menu::create([
            "name" => "Reporter assignment",
            "href" => "/reporter-assignment",
            "parent_id" => $reporterMenu->id,
            "menu_code" => "reporter_assignment"
        ]);
        $reporterProposalMenu = Menu::create([
            "name" => "Reporter proposal",
            "href" => "/reporter-proposal",
            "parent_id" => $reporterMenu->id,
            "menu_code" => "reporter_proposal"
        ]);
        $transportationMenu = Menu::create([
            "name" => "transportation",
            "menu_type" => "dropdown",
            "menu_code" => "transportation"
        ]);
        $transportationListMenu = Menu::create([
            "name" => "transportation list",
            "href" => "/transportation-list",
            "parent_id" => $transportationMenu->id,
            "menu_code" => "transportation_list"
        ]);
        $transportationAssignmentMenu = Menu::create([
            "name" => "transportation assignment",
            "href" => "/transportation-assignment",
            "parent_id" => $transportationMenu->id,
            "menu_code" => "transportation_assignment"
        ]);
        $transportationProposalMenu = Menu::create([
            "name" => "transportation proposal",
            "href" => "/transportation-proposal",
            "parent_id" => $transportationMenu->id,
            "menu_code" => "transportation_proposal"
        ]);
        $carsMenu = Menu::create([
            "name" => "Cars",
            "href" => "/cars",
            "parent_id" => $transportationMenu->id,
            "menu_code" => "cars"
        ]);
        $deviceMenu = Menu::create([
            "name" => "Device",
            "menu_type" => "dropdown",
            "menu_code" => "devices"
        ]);
        $deviceListMenu = Menu::create([
            "name" => "Device list",
            "href" => "/devices",
            "parent_id" => $deviceMenu->id,
            "menu_code" => "device_list"
        ]);
        $deviceAssignmentMenu = Menu::create([
            "name" => "device assignment",
            "href" => "/device-assignment",
            "parent_id" => $deviceMenu->id,
            "menu_code" => "device_assignment"
        ]);
        $deviceCategoryMenu = Menu::create([
            "name" => "Device categories",
            "href" => "/device-categories",
            "parent_id" => $deviceMenu->id,
            "menu_code" => "device_categories"
        ]);
        $deviceProposalMenu = Menu::create([
            "name" => "Device proposal",
            "href" => "/device-proposal",
            "parent_id" => $deviceMenu->id,
            "menu_code" => "device_proposal"
        ]);
        $deviceLendingMenu = Menu::create([
            "name" => "Device lending",
            "href" => "/assignment/device/device-lending",
            "parent_id" => $deviceMenu->id,
            "menu_code" => "device_lending"
        ]);
        $deviceReturningMenu = Menu::create([
            "name" => "Device returning",
            "href" => "/assignment/device/device-returning",
            "parent_id" => $deviceMenu->id,
            "menu_code" => "device_returning"
        ]);
        $outsourcesMenu = Menu::create([
            "name" => "Outsources",
            "menu_type" => "dropdown",
            "menu_code" => "outsources"
        ]);
        $outsourceTypeMenu = Menu::create([
            "name" => "Outsource types",
            "href" => "/outsource-types",
            "parent_id" => $outsourcesMenu->id,
            "menu_code" => "outsource_types"
        ]);
        $outsourceListMenu = Menu::create([
            "name" => "Outsource list",
            "href" => "/outsources",
            "parent_id" => $outsourcesMenu->id,
            "menu_code" => "outsource_list"
        ]);
        $planMenu = Menu::create([
            "name" => "Plans",
            "href" => "/plans",
            "menu_code" => "plans"
        ]);
        $departmentMenu = Menu::create([
            "name" => "Departments",
            "href" => "/departments",
            "menu_code" => "departments"
        ]);
        $managerMenu = Menu::create([
            "name" => "Managers",
            "href" => "/managers",
            "menu_code" => "managers"
        ]);
        $statementMenu = Menu::create([
            "name" => "Statements",
            "href" => "/statements",
            "menu_code" => "statements"
        ]);
        $contractMenu = Menu::create([
            "name" => "Contracts",
            "href" => "/contracts",
            "menu_code" => "contracts"
        ]);

        $mapMenuId = [
            "record_plans" => $recordPlanMenu,
            "camera" => $cameraMenu,
            "camera_list" => $cameraListMenu,
            "camera_assignment" => $cameraAssignmentMenu,
            "camera_proposal" => $cameraProposalMenu,
            "reporter" => $reporterMenu,
            "reporter_list" => $reporterListMenu,
            "reporter_assignment" => $reporterAssignmentMenu,
            "reporter_proposal" => $reporterProposalMenu,
            "transportation" => $transportationMenu,
            "transportation_list" => $transportationListMenu,
            "transportation_assignment" => $transportationAssignmentMenu,
            "transportation_proposal" => $transportationProposalMenu,
            "cars" => $carsMenu,
            "devices" => $deviceMenu,
            "device_list" => $deviceListMenu,
            "device_assignment" =>$deviceAssignmentMenu,
            "device_categories" => $deviceCategoryMenu,
            "device_proposal" => $deviceProposalMenu,
            "device_lending" => $deviceLendingMenu,
            "device_returning" => $deviceReturningMenu,
            "outsources" => $outsourcesMenu,
            "outsource_types" => $outsourceTypeMenu,
            "outsource_list" => $outsourceListMenu,
            "plans" => $planMenu,
            "departments" => $departmentMenu,
            "managers" => $managerMenu,
            "statements" => $statementMenu,
            "contracts" => $contractMenu,
        ];
        $query = [];
        foreach (config("menu_permissions.permissions") as $menu => $permissions) {
            foreach($permissions as $permission) {
                $query[] = [
                    "name" => $permission . '_' . $mapMenuId[$menu]->menu_code,
                    "guard_name" => "web",
                    "menu_id" => $mapMenuId[$menu]->id
                ];
            }
        }
        Permission::insert($query);
    }
}
