<?php

namespace Database\Seeders;

use App\Models\OutsourceType;
use Illuminate\Database\Seeder;

class OutsourceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OutsourceType::insert([
            [
                "id" => 1,
                "name" => "Đạo diễn",
                "code" => "DD",
                "department_code" => "production",
                "status" => 1,
            ],
            [
                "id" => 2,
                "name" =>"Diễn viên chính",
                "code" => "DVC",
                "department_code" => "production",
                "status" => 1,
            ],
            [
                "id" => 3,
                "name" => "Diễn viên phụ",
                "code" => "DVP",
                "department_code" => "production",
                "status" => 1,
            ],
            [
                "id" => 4,
                "name" => "Diễn viên đóng thế",
                "code" => "DVDT",
                "department_code" => "production",
                "status" => 1,
            ],
        ]);
    }
}
