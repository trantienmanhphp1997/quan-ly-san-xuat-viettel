<?php

namespace Database\Seeders;

use App\Models\Partner;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class PartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for($i = 0; $i<20; $i++){
            Partner::insert([
                "status" => 1,
                "name" => $faker->name,
                "email" => $faker->email,
                "phone_number" => $faker->phoneNumber,
                "address" => $faker->address,
            ]);
        }
    }
}
