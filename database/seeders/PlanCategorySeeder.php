<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\PlanCategory;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class PlanCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $departments = Department::where(["status" => 1])->pluck("name");
        PlanCategory::insert([
            [
                "name" => "Kế hoạch năm phòng " .$faker->randomElement($departments),
                "code" => $faker->creditCardNumber,
                "status" => 1
            ]
        ]);
    }
}
