<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\PlanInfo;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class PlanInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $department_ids = Department::where(["status" => 1])->pluck("id");

        PlanInfo::insert([
            "id" => 1,
            "title" => "Kế hoạch năm 2021",
            "code" => $faker->creditCardNumber(),
            "status" => 1,
            "department_id" => $faker->randomElement($department_ids),
            "year" => "2021",
            "plan_category_id"=> 1
        ]);
    }
}
