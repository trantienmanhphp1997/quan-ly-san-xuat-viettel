<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\RoleHasPermission;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ProductionDepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Cập nhật tên của các phòng sản xuất cũ
        $oldProductionDepartment = Department::where("code", "like", "production_");
        Department::where("code", "production_general_news_news")->update(['name'=> "Tin tức", "description" => "Tin tức"]);
        Department::where("code", "production_general_news_documentary")->update(['name'=> "Chuyên đề", "description" => "Chuyên đề"]);
        Department::where("code", "production_entertainment_music")->update(['name'=> "Nhạc", "description" => "Nhạc"]);
        Department::where("code", "production_entertainment_film")->update(['name'=> "Phim", "description" => "Phim"]);

        //Tạo mới
        $game = Department::create([
            "code" => "production_entertainment_game",
            "parent_id" => "0",
            "name" => "Game",
            "description" => "Game",
            "status" => 1
        ]);

        $marketing = Department::create([
            "code" => "production_entertainment_marketing",
            "parent_id" => "0",
            "name" => "Marketing",
            "description" => "Marketing",
            "status" => 1
        ]);
        $ecommerce = Department::create([
            "code" => "production_e-commerce",
            "parent_id" => "0",
            "name" => "Trang tin điện tử",
            "description" => "Trang tin điện tử",
            "status" => 1
        ]);
        $individualCcustomer = Department::create([
            "code" => "production_individual_customer",
            "parent_id" => "0",
            "name" => "Khách hàng cá nhân",
            "description" => "Khách hàng cá nhân",
            "status" => 1
        ]);
        $sale_marketing = Department::create([
            "code" => "production_sale_marketing",
            "parent_id" => "0",
            "name" => "Kinh doanh quảng cáo",
            "description" => "Kinh doanh quảng cáo",
            "status" => 1
        ]);
        $enterprise_customer = Department::create([
            "code" => "production_enterprise_customer",
            "parent_id" => "0",
            "name" => "Khách hàng doanh nghiệp",
            "description" => "Khách hàng doanh nghiệp",
            "status" => 1
        ]);
        $technical_center = Department::create([
            "code" => "production_technical_center",
            "parent_id" => "0",
            "name" => "Trung tâm Kỹ thuật",
            "description" => "Trung tâm Kỹ thuật",
            "status" => 1
        ]);
        $editor = Department::create([
            "code" => "production_editor",
            "parent_id" => "0",
            "name" => "Thư ký Biên tập",
            "description" => "Thư ký Biên tập",
            "status" => 1
        ]);
        $business_plan = Department::create([
            "code" => "production_business_plan",
            "parent_id" => "0",
            "name" => "TKế hoạch kinh doanh",
            "description" => "Kế hoạch kinh doanh",
            "status" => 1
        ]);

//        $newDepartment = [$game, $marketing, $ecommerce, $individualCcustomer, $sale_marketing, $enterprise_customer, $technical_center, $editor, $business_plan];

//        $superAdminRole = Role::where("name", "super admin")->get("id")->pluck('id');



        //Bổ sung 6 phòng ban mới
//        /**
//         * seed based roles
//         */
//        $technical_camera_role = Role::create(['name' => 'department technical_camera']);
//        $superAdminRole = Role::where("name", "super admin")->get("id")->pluck('id');
//        $technical_permissons = Permission::where("name", "like", "%technical_camera%")->orWhere("name", "access_studios")->get("id")->pluck("id")->all();
//        $query = [];
//        foreach ($technical_permissons as $permission) {
//            $query[] = [
//                "permission_id" => $permission,
//                "role_id" => $technical_camera_role->id
//            ];
//            $query[] = [
//                "permission_id" => $permission,
//                "role_id" => $superAdminRole[0]
//            ];
//        }
//        RoleHasPermission::insert($query);
    }
}
