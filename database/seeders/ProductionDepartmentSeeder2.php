<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Menu;
use App\Models\RoleHasPermission;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ProductionDepartmentSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = Menu::all();

        $mapMenuId = [];
        foreach ($menus as $menu) {
            $mapMenuId[$menu->menu_code] = $menu;
        }
        Department::where("name", "Marketing")->update(['code'=> "production_entertainment_marketing"]);
        Department::where("code", "production_business_plan")->update(['name'=> "Kế hoạch kinh doanh"]);

        $production_entertainment_game = Role::create(['name' => 'department production_entertainment_game']);
        $production_entertainment_marketing = Role::create(['name' => 'department production_entertainment_marketing']);
        $production_e_commerce = Role::create(['name' => 'department production_e-commerce']);
        $production_individual_customer = Role::create(['name' => 'department production_individual_customer']);
        $production_sale_marketing = Role::create(['name' => 'department production_sale_marketing']);
        $production_enterprise_customer = Role::create(['name' => 'department production_enterprise_customer']);
        $production_technical_center = Role::create(['name' => 'department production_technical_center']);
        $production_editor = Role::create(['name' => 'department production_editor']);
        $production_business_plan = Role::create(['name' => 'department production_business_plan']);

        $reporter_roles = [
            $production_entertainment_game->id,
            $production_entertainment_marketing->id,
            $production_e_commerce->id,
            $production_individual_customer->id,
            $production_sale_marketing->id,
            $production_enterprise_customer->id,
            $production_technical_center->id,
            $production_editor->id,
            $production_business_plan->id,
        ];

        $featurePermissions = [
            "create",
            "view",
            "view_any",
            "update",
            "update_any",
            "delete",
            "delete_any",
            "restore",
        ];

        $accessPermissions = [
            "access"
        ];

        $reporters = [
            "record_plans" => array_merge($accessPermissions, $featurePermissions),
            "reporter" => $accessPermissions,
            "reporter_list" => array_merge($accessPermissions, $featurePermissions),
            "reporter_assignment" => array_merge($accessPermissions, $featurePermissions),
            "reporter_proposal" => array_merge($accessPermissions),
            "device_assignment" => ["access", "create"],
            "outsources" => $accessPermissions,
            "outsource_types" => array_merge($accessPermissions, $featurePermissions),
            "outsource_list" => array_merge($accessPermissions, $featurePermissions),
            "plans" => array_merge($accessPermissions, $featurePermissions),
            "statements" => array_merge($accessPermissions, $featurePermissions),
            "contracts" => array_merge($accessPermissions, $featurePermissions),
        ];
        $permission_name = [];
        foreach ($reporters as $menu => $permissions) {
            foreach ($permissions as $permission) {
                $permission_name[] = $permission . '_' . $mapMenuId[$menu]->menu_code;
            }
        }
        $permissions = Permission::whereIn("name", $permission_name)->get("id");
        $query = [];
        foreach ($permissions as $permission) {
            foreach ($reporter_roles as $role){
                $query[] = [
                    "permission_id" => $permission->id,
                    "role_id" => $role
                ];
            }
        }

        RoleHasPermission::insert($query);

    }
}
