<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\RecordPlan;
use App\Models\RecordPlanDeviceXref;
use App\Models\RecordPlanMemberXref;
use App\Models\RecordPlanCarXref;
use Illuminate\Support\Facades\DB;

class RecordPlanSeeder extends Seeder
{
    /*
     * @return void
     */
    public function run()
    {
        $numberOfPlans = 60;
        $faker = Faker::create();
        $sampleRecordPlanNames = [
            'Ai là triệu phú',
            'Vui khỏe có ích',
            'Người tốt việc tốt',
            'Thể thao 24h',
            'Cafe buổi sáng',
            '24h công nghệ',
            '90 phút để hiểu',
            '100 ngày chống dịch COVID-19',
            'An toàn giao thông',
            'Ẩm thực đường phố',
            'Âm nhạc & cuộc sống',
            'Báo chí toàn cảnh',
            'Bản tin Bất động sản',
            'Bản tin cuối ngày',
            'Bản tin cuối ngày',
            'Chuyện nhà nông',
            'Điểm tuần – Review',
            'Dự báo thời tiết',
            'Sự kiện và bình luận',
            'Tài chính – kinh doanh',
            'Tạp chí kinh tế cuối tuần',
            'Tiêu dùng 24h',
            'Việt Nam và thế giới',
            'Việt Nam qua con mắt người nước ngoài',
            'Vui cùng bé yêu',
            'Dọc miền đất nước',
            'Doanh nhân Việt Nam',
            'Diễn đàn văn nghệ',
            'Diễn đàn doanh nghiệp',
            'Di sản văn hóa',
            'Chuyến xe khởi nghiệp',
            'Danh ngôn và cuộc sống',
            'Danh nhân đất Việt',
            'Kỷ lục Guinness',
            'Ký sự những nẻo đường',
            'Ký sự Thăng Long',
            'Ký ức Việt Nam',
            'Ký ức Việt Nam',
            'Làm theo lời Bác',
            'Một địa chỉ văn hóa',
            'Nghĩ khác, sống khác',
            'Điểm hẹn 7 ngày',
            'Tạp chí văn hóa xã hội',
            'Công nghệ thế kỷ 21',
            'Cuộc đua số',
            'Cuộc sống xanh',
            'Âm nhạc dân gian',
            '7 ngày công nghệ',
            'Có thể bạn chưa biết',
            'Không gian IT',
            'Thế giới công nghệ',
            'Xe và đời sống',
            'Thật đơn giản',
            'Giải mã cuộc sống',
            'Đi đâu? Ăn gì?',
            'Đến với tri thức khoa học',
            'Cùng bạn chữa bệnh',
            'Cuộc đời là những chuyến đi'
        ];
        $members = DB::table("users")->where(["is_manager" => 0, "department_code" => "production"])->get()->pluck("id")->toArray();
        $managers = DB::table("users")->where(["is_manager" => 1])->get()->pluck("id")->toArray();
        $cars = DB::table("cars")->get()->pluck("id")->toArray();
        $devices = DB::table("devices")->get()->pluck("id")->toArray();
        $department_codes = ["production",];// "general_news", "news", "documentary", "entertainment", "music", "film"
        for($i = 0; $i<$numberOfPlans; $i++){
            $startPoint = rand(-14, 60);
            $midPoint = $startPoint + $faker->randomElement([0,0,0,0,1,2]);
            $endPoint = $midPoint + $faker->randomElement([0,0,0,0,1,2]);

            $start_time = $faker->dateTimeBetween($startPoint == 0 ? 'now' : $startPoint . ' days', $midPoint == 0 ? 'now' : $midPoint . ' days');
            $end_time = $faker->dateTimeBetween($midPoint == 0 ? 'now' : $midPoint . ' days', $endPoint == 0 ? 'now' : $endPoint . ' days');

            $plan = RecordPlan::create([
                'name' => $faker->randomElement($sampleRecordPlanNames) . ' số ' . ($i + 1),
                'description' => 'Lịch quay phim cho chương trình ' . $faker->randomElement($sampleRecordPlanNames) . ' số ' . (round($i/5) + 1),
                'start_time' => $start_time,
                'end_time' => $end_time,
                'address' => $faker->address,
                'created_at' => now(),
                'updated_at' => now(),
                "created_by" => $faker->randomElement($managers),
                "status" => 1,
                "reporter_allocate_status" => 1,
                "device_allocate_status" => 1,
                "camera_allocate_status" => 1,
                "transportation_allocate_status" => 1,
                "offer_camera_note" => "",
                "offer_transportation_note" => "",
                "offer_device_note" => "",
                "offer_reporter_number" => rand(1,3),
                "offer_camera_number" => rand(1,3),
                "offer_transportation_number" => rand(1,3),
                "offer_device_number" => rand(1,3),
                "department_code" => $faker->randomElement($department_codes),
                "plan_id" => 1,
            ]);
            $plan_id = $plan->id;

//            $record_plan_member_xrefs = RecordPlanMemberXref::create([
//                "record_plan_id" => $plan_id,
//                "member_id" => $faker->randomElement($members),
//                "start_time" => $start_time,
//                "end_time" => $end_time,
//                "created_at" => now(),
//                "updated_at" => now(),
//                "status" => 2
//            ]);

//            $record_plan_device_xrefs = RecordPlanDeviceXref::create([
//                "record_plan_id" => $plan_id,
//                "device_id" => $faker->randomElement($devices),
//                "start_time" => $start_time,
//                "end_time" => $end_time,
//                "created_at" => now(),
//                "updated_at" => now(),
//                "status" => 1
//            ]);
        }
    }
}
