<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * seed based roles
         */
        Role::create(['name' => 'department production']);
        Role::create(['name' => 'department camera']);
        Role::create(['name' => 'department device']);
        Role::create(['name' => 'department transportation']);
        Role::create(['name' => 'department accounting']);
        Role::create(['name' => 'department production_general_news']);
        Role::create(['name' => 'department production_general_news_news']);
        Role::create(['name' => 'department production_general_news_documentary']);
        Role::create(['name' => 'department production_entertainment']);
        Role::create(['name' => 'department production_entertainment_music']);
        Role::create(['name' => 'department production_entertainment_film']);
        Role::create(['name' => 'super admin']);
    }
}
