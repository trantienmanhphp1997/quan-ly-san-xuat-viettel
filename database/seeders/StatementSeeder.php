<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Statement;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class StatementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $department_ids = Department::where(["status" => 1])->pluck("id");
        $statement_title = ["thuê ngoài ", "mua ", "cấp chi phí"];

        for($i = 0; $i<20; $i++){
            Statement::insert([
                "title" => "Tờ trình ".$faker->randomElement($statement_title).$faker->name,
                "code" => $faker->creditCardNumber(),
                "status" => 1,
                "department_id" => $faker->randomElement($department_ids)
            ]);
        }
    }
}
