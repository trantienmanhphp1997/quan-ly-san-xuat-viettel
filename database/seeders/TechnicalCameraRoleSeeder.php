<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\RoleHasPermission;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class TechnicalCameraRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * seed based roles
         */
        $technical_camera_role = Role::create(['name' => 'department technical_camera']);
        $superAdminRole = Role::where("name", "super admin")->get("id")->pluck('id');
        $technical_permissons = Permission::where("name", "like", "%technical_camera%")->orWhere("name", "access_studios")->get("id")->pluck("id")->all();
        $query = [];
        foreach ($technical_permissons as $permission) {
            $query[] = [
                "permission_id" => $permission,
                "role_id" => $technical_camera_role->id
            ];
            $query[] = [
                "permission_id" => $permission,
                "role_id" => $superAdminRole[0]
            ];
        }
        RoleHasPermission::insert($query);
    }
}
