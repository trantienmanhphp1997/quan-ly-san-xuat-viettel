<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Menu;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class TechnicalCameraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create([
            "name" => "technical camera",
            "description" => "technical camera",
            "code" => "technical_camera",
            "display_code" => "technical_camera",
            "status" => "1",
        ]);
        $technicalCameraMenu = Menu::create([
            "name" => "Technical camera",
            "menu_type" => "dropdown",
            "menu_code" => "technical_camera"
        ]);
        $technicalCameraListMenu = Menu::create([
            "name" => "Technical camera list",
            "href" => "/camera-list",
            "parent_id" => $technicalCameraMenu->id,
            "menu_code" => "technical_camera_list"
        ]);
        $technicalCameraAssignmentMenu = Menu::create([
            "name" => "Technical camera assignment",
            "href" => "/camera-assignment",
            "parent_id" => $technicalCameraMenu->id,
            "menu_code" => "technical_camera_assignment"
        ]);
        $technicalCameraProposalMenu = Menu::create([
            "name" => "Technical camera proposal",
            "href" => "/camera-proposal",
            "parent_id" => $technicalCameraMenu->id,
            "menu_code" => "technical_camera_proposal"
        ]);

        $studio = Menu::create([
            "name" => "Studios",
            "href" => "/studios",
            "parent_id" => $technicalCameraMenu->id,
            "menu_code" => "studios"
        ]);


        $mapMenuId = [
            "technical_camera" => $technicalCameraMenu,
            "technical_camera_list" => $technicalCameraListMenu,
            "technical_camera_assignment" => $technicalCameraAssignmentMenu,
            "technical_camera_proposal" => $technicalCameraProposalMenu,
            "studio" => $studio,
        ];
        $query = [];
        $featurePermissions = [
            "create",
            "view",
            "view_any",
            "update",
            "update_any",
            "delete",
            "delete_any",
            "restore",
//    "import",
//    "export"
        ];

        $accessPermissions = [
            "access"
        ];


        $configPermison = [
            "technical_camera" => $accessPermissions,
            "technical_camera_list" => array_merge($accessPermissions, $featurePermissions),
            "technical_camera_assignment" => array_merge($accessPermissions, $featurePermissions),
            "technical_camera_proposal" => array_merge($accessPermissions, $featurePermissions),
            "studio" => array_merge($accessPermissions, $featurePermissions),
        ];

        foreach ($configPermison as $menu => $permissions) {
            foreach($permissions as $permission) {
                $query[] = [
                    "name" => $permission . '_' . $mapMenuId[$menu]->menu_code,
                    "guard_name" => "web",
                    "menu_id" => $mapMenuId[$menu]->id
                ];
            }
        }
        Permission::insert($query);
    }
}
