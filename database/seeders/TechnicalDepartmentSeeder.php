<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Menu;
use App\Models\RoleHasPermission;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class TechnicalDepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = Menu::all();

        $mapMenuId = [];
        foreach ($menus as $menu) {
            $mapMenuId[$menu->menu_code] = $menu;
        }
        Department::where("code", "technical_camera")->update(['parent_id'=> "0"]);

        //Cấp quyền xem recordplans cho technical camera
        $permission_name = ['view_record_plans', 'access_record_plans'];
        $permissions = Permission::whereIn("name", $permission_name)->get("id");
        $technicalCameraRole = Role::where('name', 'department technical_camera')->first();
        $query = [];
        foreach ($permissions as $permission) {
            $query[] = [
                "permission_id" => $permission->id,
                "role_id" => $technicalCameraRole->id
            ];
        }
        RoleHasPermission::insert($query);

        //Cập nhật lại quyền của device

        $featurePermissions = [
            "create",
            "view",
            "view_any",
            "update",
            "update_any",
            "delete",
            "delete_any",
            "restore",
        ];

        $accessPermissions = [
            "access"
        ];

        $devices = [
            "record_plans" => ["view", "access"],
            "devices" => $accessPermissions,
            "device_list" => array_merge($accessPermissions, $featurePermissions),
            "device_assignment" => array_merge($accessPermissions, $featurePermissions),
            "device_categories" => array_merge($accessPermissions, $featurePermissions),
            "device_proposal" => array_merge($accessPermissions, $featurePermissions),
            "device_lending" => array_merge($accessPermissions, $featurePermissions),
            "device_returning" => array_merge($accessPermissions, $featurePermissions),
            "outsources" => $accessPermissions,
            "outsource_types" => array_merge($accessPermissions, $featurePermissions),
            "outsource_list" => array_merge($accessPermissions, $featurePermissions),
        ];

        $deviceRole = Role::where('name', 'department device')->first();
        // give permission to reporters
        $permission_name = [];
        foreach ($devices as $menu => $permissions) {
            foreach ($permissions as $permission) {
                $permission_name[] = $permission . '_' . $mapMenuId[$menu]->menu_code;
            }
        }
        $permissions = Permission::whereIn("name", $permission_name)->get("id");
        $query = [];
        foreach ($permissions as $permission) {
            $query[] = [
                "permission_id" => $permission->id,
                "role_id" => $deviceRole->id
            ];
        }
        RoleHasPermission::where('role_id',$deviceRole->id)->delete();
        RoleHasPermission::insert($query);
    }
}
