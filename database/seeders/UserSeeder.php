<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numberOfUsers = 50;
        $usersIds = array();
        $faker = Faker::create();
        $departments = Department::with(["roles"])->get();
        $departmentIds = $departments->map(function ($department) {
            return $department->id;
        });
        $departmentRoles = $departments->map(function ($department) {
            return $department->roles[0];
        });
        $departmentRoles = $departmentIds->combine($departmentRoles);
        $departmentCodes = array_values(config("common.departments"));
        /*  insert managers   */

        $user = User::create([
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'department_code' => config("common.departments.production"),
            "is_manager" => 1
        ])->assignRole($departmentRoles[1], "super admin");
//        $user = User::create([
//            'username' => 'reporter',
//            'email' => 'reporter@reporter.com',
//            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
//            'department_code' => config("common.departments.production"),
//            "is_manager" => 1
//        ])->assignRole($departmentRoles[1]);
//        $user = User::create([
//            'username' => 'cam',
//            'email' => 'cam@cam.com',
//            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
//            'department_code' => config("common.departments.camera"),
//            "is_manager" => 1
//        ])->assignRole($departmentRoles[2]);
//        $user = User::create([
//            'username' => 'device',
//            'email' => 'device@device.com',
//            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
//            'department_code' => config("common.departments.device"),
//            "is_manager" => 1
//        ])->assignRole($departmentRoles[3]);
//        $user = User::create([
//            'username' => 'driver',
//            'email' => 'driver@driver.com',
//            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
//            'department_code' => config("common.departments.transportation"),
//            "is_manager" => 1
//        ])->assignRole($departmentRoles[4]);

        /*for ($i = 0; $i < $numberOfUsers; $i++) {
            $user = User::create([
                'username' => $faker->userName,
                "full_name" => $faker->name,
                'email' => $faker->unique()->safeEmail(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'department_code' => $departmentCodes[$i % 11],
                "is_manager" => 0,
                "phone_number" => $faker->phoneNumber
            ])->assignRole($departmentRoles[$i % 11 + 1]);
            array_push($usersIds, $user->id);
        }*/

        /*  insert managers   */
//        $user = User::create([
//            'username' => 'xuyen_reporter',
//            'email' => 'admin@admin.com',
//            'password' => '$2y$10$nlUi0Znstskj6E67BRGyb.6uph3NZ.QH/zg9HDKg5.XHz4ZDQs9NO', // 123456
//            'department_code' => config("common.departments.production"),
//            "is_manager" => 1
//        ])->assignRole($departmentRoles[1]);
//        $user = User::create([
//            'username' => 'xuyen_cam',
//            'email' => 'admin@admin.com',
//            'password' => '$2y$10$nlUi0Znstskj6E67BRGyb.6uph3NZ.QH/zg9HDKg5.XHz4ZDQs9NO', // 123456
//            'department_code' => config("common.departments.camera"),
//            "is_manager" => 1
//        ])->assignRole($departmentRoles[2]);
//        $user = User::create([
//            'username' => 'xuyen_driver',
//            'email' => 'admin@admin.com',
//            'password' => '$2y$10$nlUi0Znstskj6E67BRGyb.6uph3NZ.QH/zg9HDKg5.XHz4ZDQs9NO', // 123456
//            'department_code' => config("common.departments.transportation"),
//            "is_manager" => 1
//        ])->assignRole($departmentRoles[4]);
//        $user = User::create([
//            'username' => 'xuyen_device',
//            'email' => 'admin@admin.com',
//            'password' => '$2y$10$nlUi0Znstskj6E67BRGyb.6uph3NZ.QH/zg9HDKg5.XHz4ZDQs9NO', // 123456
//            'department_code' => config("common.departments.device"),
//            "is_manager" => 1
//        ])->assignRole($departmentRoles[3]);

        // Add all permissions to user super admin
        Role::findByName("super admin")->givePermissionTo(Permission::all()->pluck('name')->toArray());
    }
}
