$(function () {
    $.validator.addMethod('dateBefore', function (value, element, params) {
        // // return startTime < endTime
        // // if end date is valid, validate it as well
        // // document.getElementById("start_time").removeEventListener("change", checkTime());
        // // $("#start_time").one("change", checkStarTime(value));
        // var end = $("#end_time");
        // var start = $("#start_time");
        // console.log(start.val())
        // console.log(end.val())
        // console.log(new Date(start.val()) < new Date(end.val()))
        // return new Date(start.val()) < new Date(end.val());

        var end = $(params);
        if (!end.data('validation.running')) {
            $(element).data('validation.running', true);
            setTimeout($.proxy(
                function () {
                    this.element(end);
                }, this), 0);
            // Ensure clearing the 'flag' happens after the validation of 'end' to prevent endless looping
            setTimeout(function () {
                $(element).data('validation.running', false);
            }, 0);
        }
        return this.optional(element) || this.optional(end[0]) || new Date(value) < new Date(end.val());

    }, 'Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc');

    $.validator.addMethod('dateAfter', function (value, element, params) {
        // if start date is valid, validate it as well
        var start = $(params);
        if (!start.data('validation.running')) {
            $(element).data('validation.running', true);
            setTimeout($.proxy(
                function () {
                    this.element(start);
                }, this), 0);
            setTimeout(function () {
                $(element).data('validation.running', false);
            }, 0);
        }
        return this.optional(element) || this.optional(start[0]) || new Date(value) > new Date($(params).val());

    }, 'End time must be after start time');
})
