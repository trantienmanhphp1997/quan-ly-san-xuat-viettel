function customDate(selector, min_date, defaut_date, defaultHour = 12, defaultMin = 0) {
    $(`${selector}`).flatpickr({
        enableTime: true,
        allowInput: false,
        "onChange": function(selectedDates, dateStr, instance) {
            var regex_date = /^\d{2}\:\d{2}\ \d{1,2}\-\d{1,2}\-\d{4}$/;
            if(!regex_date.test(dateStr))
            {
                instance.setDate(null, false);
            }
            else {
                instance.setDate(instance.input.value,false)
            }
        },
        monthSelectorType: "static",
        wrap: true,
        minDate: min_date,
        dateFormat: "H:i d-m-Y",
        altInput: true,
        altFormat: "H:i d-m-Y",
        time_24hr: true,
        defaultDate: defaut_date,
        defaultHour: defaultHour,
        defaultMinute: defaultMin,
        locale: {
            weekdays: {
                shorthand: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"]
                // longhand: [],
            },
            months: {
                // shorthand: [],
                longhand: [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                ]
            }
        }
    });
}
