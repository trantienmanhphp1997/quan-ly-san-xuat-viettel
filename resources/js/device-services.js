function getListDeviceCategory() {
    axios.get(`/device-categories/getListCategory`, {

        })
        .then(function(response) {
            var $device_categories = response.data;
            var $select_device_cate_id = $(`#device_category`)
            if($device_categories.length){
                $device_categories.forEach($dc => {
                    var content = "";
                    if($dc){
                        content = ` <option value=${$dc.id}>${$dc.name}</option>`
                        $select_device_cate_id.append(content)
                    }
                })
            }
        })
        .catch(function(error) {
            console.log(error);
        })
}

function getListDeviceWaitConf(ids) {
    axios.post(`/devices/getListDeviceWaitConf`, {
        ids: ids
    })
    .then(function(response) {
        var $wait_conf_devices = response.data;
        var $wait_conf_devices_id = $(`#table_wait_conf_device`);
        if ($wait_conf_devices.length) {
            $wait_conf_devices_id.prop('disabled', false)
            $wait_conf_devices.forEach($device => {
                var content = "";
                if ($device) {
                        content =`<tr>
                        <td class="text-truncate text-limit">${$device.name}</td>
                        <td>
                            <input type="hidden" name="row_id" id="checkbox_${$device.id}" value="${$device.id}"/>
                        </td>
                        <td>${$device.category.name}</td>
                        </tr>`;

                    $wait_conf_devices_id.append(content)
                }
            })
        } else {
            $wait_conf_devices_id.append(
                `<option value="">Không có thiết bị phù hợp</option>`)
            $wait_conf_devices_id.prop('disabled', true)
        }
    })
    .catch(function(error) {
        console.log(error);
    })
}
function getListDeviceCategory() {
    axios.get(`/device-categories/getListCategory`, {

        })
        .then(function(response) {
            var $device_categories = response.data;
            var $select_device_cate_id = $(`#device_category`)
            if($device_categories.length){
                $device_categories.forEach($dc => {
                    var content = "";
                    if($dc){
                        content = ` <option value=${$dc.id}>${$dc.name}</option>`
                        $select_device_cate_id.append(content)
                    }
                })
            }
        })
        .catch(function(error) {
            console.log(error);
        })
}

function getListAvailableDevice(category_id,recordPlanId,search) {
    axios.post(`/assignment/device/${recordPlanId}/show`, {
            category_id: category_id,
            keyword: search
        })
        .then(function(response) {
            var $available_devices = response.data.available_devices;
            var $select_available_members_id = $(`#table_assign_device`).html("");
            var loop = 1;
            if ($available_devices.length) {
                $select_available_members_id.prop('disabled', false)
                $available_devices.forEach($device => {
                    var content = "";
                    let isChecked = "";
                    if(selected_ids.includes(String($device.id))){
                        isChecked = "checked";
                    }
                    console.log(selected_ids.includes(String($device.id)), selected_ids, isChecked)
                    if ($device) {
                            content =`<tr>
                            <td>
                                <input type="checkbox" name="row_id" id="checkbox_assign_${$device.id}" value="${$device.id}" ${isChecked}>
                            </td>
                            <td>${loop++}</td>
                            <td class="text-truncate text-limit">${$device.name}</td>
                            <td class="text-truncate text-limit">${$device.serial}</td>
                            <td>${$device.category.name}</td>
                            </tr>`;

                        $select_available_members_id.append(content);

                    }
                });
                clickHandler();
            } else {
                $select_available_members_id.append(
                    `<option value="">Không có thiết bị phù hợp</option>`)
                $select_available_members_id.prop('disabled', true)
            }

        })
        .catch(function(error) {
            console.log(error);
        })
}
const prepareDataForAssignDevice = () => {
    var ids = [];

    var $checkedBoxes = $('#assign_assign_table input[type=checkbox]:checked').not('.select_all');
    $.each($checkedBoxes, function () {
        var value = $(this).val();
        ids.push(value);
    });
    console.log("selected device ids "+ ids)
    return ids;
}

function assignDeviceToRecordPlan(recordPlanId,ids){
    axios.post(`/assignment/device/${recordPlanId}`, {
        device_ids: ids
    })
    .then(function(response) {
        location.reload();
    })
    .catch(function(error) {
        console.log(error);
        location.reload();
    })
}
function updateDeviceStatus(recordPlanId, ids, status_change, user_id=null){
    axios.put(`/assignment/device/update-status`, {
        device_ids: ids,
        status: status_change,
        user_id: user_id,
        record_plan_id: recordPlanId
    })
    .then(function(response) {
        location.reload();
    })
    .catch(function(error) {
        console.log(error);
        location.reload();
    })
}
function getListDevicesForReturning(recordPlanId) {
    axios.get(`/assignment/device/${recordPlanId}/returning-detail`, {

        })
        .then(function(response) {
            var $return_devices = response.data.return_devices.data;
            var $select_return_device_id = $(`#table_receive_device`).html("");
            var loop = 1;
            if ($return_devices.length) {
                $select_return_device_id.prop('disabled', false)
                $return_devices.forEach($data => {
                    var content = "";
                    if ($data) {
                            content =`<tr>
                            <td>
                                <input type="checkbox" name="row_id" id="checkbox_receive_${$data.device.id}" value="${$data.device.id}">
                            </td>
                            <td>${loop++}</td>
                            <td class="text-truncate text-limit">${$data.device.name}</td>
                            <td class="text-truncate text-limit">${$data.device.serial}</td>
                            <td>${$data.device.category.name}</td>
                            </tr>`;

                        $select_return_device_id.append(content)
                    }
                })
            } else {
                $select_return_device_id.append(
                    `<option value="">Không có thiết bị phù hợp</option>`)
                $select_return_device_id.prop('disabled', true)
            }
        })
        .catch(function(error) {
            console.log(error);
        })
}

function getListDevicesForLending(recordPlanId) {
    axios.get(`/assignment/device/${recordPlanId}/lending-detail`, {

        })
        .then(function(response) {
            var $delivery_devices = response.data.delivery_devices.data;
            var $select_delivery_device_id = $(`#table_delivery_device`).html("");
            var loop = 1;
            if ($delivery_devices.length) {
                $select_delivery_device_id.prop('disabled', false)
                $delivery_devices.forEach($data => {
                    var content = "";
                    if ($data) {
                            content =`<tr>
                            <td>
                                <input type="checkbox" name="row_id" id="checkbox_delivery_${$data.device.id}" value="${$data.device.id}">
                            </td>
                            <td>${loop++}</td>
                            <td class="text-truncate text-limit">${$data.device.name}</td>
                            <td class="text-truncate text-limit">${$data.device.serial}</td>
                            <td>${$data.device.category.name}</td>
                            </tr>`;

                        $select_delivery_device_id.append(content)
                    }
                })
            } else {
                $select_delivery_device_id.append(
                    `<option value="">Không có thiết bị phù hợp</option>`)
                $select_delivery_device_id.prop('disabled', true)
            }
        })
        .catch(function(error) {
            console.log(error);
        })
}

function getListMemberCanLendingDevice(recordPlanId) {
    axios.get(`/assignment/device/${recordPlanId}/memer-can-lending-device`, {

    })
    .then(function(response) {
        var $user = response.data;
        var $select_user_id = $(`#users`).html("");
        if($user.length){
            $user.forEach($dc => {
                var content = "";
                if($dc){
                    content = ` <option value=${$dc.id}>${$dc.full_name ? $dc.full_name : $dc.username }</option>`
                    $select_user_id.append(content)
                }
            })
        }
    })
    .catch(function(error) {
        console.log(error);
    })
}

const getDeviceNoteByDepartment  =  async (departmentCode,recordPlanId) => {
    let data = {
        departmentCode: departmentCode
    }
    return await axios.post(`/assignment/${recordPlanId}/note`, data);
}
const getDeviceOutsourceNote = async (recordPlanId) => {
    $("#contentMessage_device").val(``)
    let device_note = await getDeviceNoteByDepartment("device", recordPlanId);
    let note = device_note.data.note ? device_note.data.note : "";
    console.log(note);
    $("#contentMessage_device").val(`${note}`)
}

function clickHandler() {
    // if(action == "assign"){
        $("input[type=checkbox]").on("change", function(){
            let target_id = $(this).attr("value");
            if($(this).is(":checked")){
                selected_ids.push(target_id)
            }else{
                selected_ids = selected_ids.filter((item) => item != target_id )
            }
        });
    // }

}
