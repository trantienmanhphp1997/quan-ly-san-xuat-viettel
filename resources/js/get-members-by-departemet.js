function getMembersByDepartmentCode(departmentCode) {
    axios
        .post("/reporter/get-members-by-department_code", {
            department_code: departmentCode
        })
        .then(function (response) {
            if (response.data.length) {
                response.data.forEach(member => {
                    $('#getReporters').append(`<option value="${member.id}">${member.full_name + ' (sđt: ' + member.phone_number + ')'}</option>`);
                });
                $('#getReporters').select2({
                    placeholder: "Chọn phóng viên"
                });
            } else {
                $('#getReporters').select2({
                    placeholder: "Không có phóng viên thuộc phòng ban"
                });
            }
        })
        .catch(function (error) {
            console.log(error);
        });
}
