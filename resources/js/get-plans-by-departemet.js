function getPlansByDepartmentId(department_id) {
    axios
        .post("/plans/get-plans-by-department_id", {
            department_id: department_id
        })
        .then(function(response) {
            removeElementInSelectOption("plan_id");
            var $data = response.data;
            var $select_device_id = $("#plan_id");
            var $plan_id = $("#plan").val();

            if (response.data.length) {
                $select_device_id.prop("disabled", false);
                $data.forEach($device => {
                    var content = "";
                    if ($device) {
                        if ($plan_id == $device.id) {
                            content = ` <option value="${$device.id}" selected>${$device.title}</option>`;
                        } else {
                            content = ` <option value="${$device.id}" >${$device.title}</option>`;
                        }
                        $select_device_id.append(content);
                    }
                });
            } else {
                $select_device_id.append(
                    `<option value="">Không có kế hoạch phù hợp</option>`
                );
                $select_device_id.prop("disabled", true);
            }
        })
        .catch(function(error) {
            console.log(error);
        });
}
