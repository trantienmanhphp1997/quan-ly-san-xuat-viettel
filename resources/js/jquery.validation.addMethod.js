$(function() {
    $.validator.addMethod(
        "dateBefore",
        function(value, element, params) {
            var end = $(params);
            if (!end.data("validation.running")) {
                $(element).data("validation.running", true);
                setTimeout(
                    $.proxy(function() {
                        this.element(end);
                    }, this),
                    0
                );
                // Ensure clearing the 'flag' happens after the validation of 'end' to prevent endless looping
                setTimeout(function() {
                    $(element).data("validation.running", false);
                }, 0);
            }
            return (
                this.optional(element) ||
                this.optional(end[0]) ||
                new Date(value) < new Date(end.val())
            );
        },
        "Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc"
    );

    $.validator.addMethod(
        "dateCheck",
        function(value, element, params) {
            var end = $(params);

            if (!end.data("validation.running")) {
                $(element).data("validation.running", true);
                setTimeout(
                    $.proxy(function() {
                        this.element(end);
                    }, this),
                    0
                ); // Ensure clearing the 'flag' happens after the validation of 'end' to prevent endless looping

                setTimeout(function() {
                    $(element).data("validation.running", false);
                }, 0);
            }
            // console.log(new Date(value) - Date.now());
            return (
                this.optional(element) ||
                new Date(value) - new Date() > 0
            );
        },
        "Thời gian bắt đầu phải lớn hơn hiện tại"
    );

    $.validator.addMethod(
        "dateAfter",
        function(value, element, params) {
            // if start date is valid, validate it as well
            // $(`#end_time-error`).remove()
            var start = $(params);
            if (!start.data("validation.running")) {
                $(element).data("validation.running", true);
                setTimeout(
                    $.proxy(function() {
                        this.element(start);
                    }, this),
                    0
                );
                setTimeout(function() {
                    $(element).data("validation.running", false);
                }, 0);
            }
            return (
                this.optional(element) ||
                this.optional(start[0]) ||
                new Date(value) > new Date($(params).val())
            );
        },
        " "
    );
});
