<?php
return [
    'button' => [
        'view' => 'View',
        'show' => 'Show',
        'create' => 'Create',
        'delete' => 'Delete',
        'bulk_delete' => 'Delete',
        'bulk_restore' => 'Restore',
        'block' => 'Block',
        'edit' => 'Edit',
        'update' => 'Update',
        'back' => 'Back',
        'search' => 'Search',
        'upload' => 'Upload',
        'download' => 'Download',
        'export' => 'Export',
        'import' => 'Import',
        'export_excel' => 'Export excel',
        'restore' => 'Restore',
        'used' => 'Approve',
        'send' => 'Send',
        'send_all' => 'Send all',
        'resend' => 'Resend message',
        'save' => 'Save',
        'remove' => 'Remove',
        'cancel' => 'Cancel',
        'ok' => 'OK',
        'submit' => 'Submit',
        'confirm' => 'Confirm',
        'extend' => 'Extend',
        'view_tasks' => 'View Tasks',
        "assign" => "Assign",
        "assign-device" => "Assign device",
        "remove-assign" => "Remove assignment",
        "remove-assign-device" => "Remove device",
        "device-proposal" => "Device proposal",
        "assignment" => [
            "camera" => "Assign cam",
            "technical_camera" => "Assign technical cam",
            "reporter" => "Assign reporter",
            "car-transportation" => "Assign car & transportation",
            "device" => "Manage device"
        ],
        "report_broken" => "Report broken",
    ],
    'place_holder' => [
        'search' => 'Search',
        'enter_key_word' => 'Enter your key word',
    ],
    'message' => [
        'showing_entries' => 'Showing :from to :to of :all entries',
        'delete_success' => 'Your :modelName was successfully deleted.',
        'retore_success' => 'Your :modelName was successfully restored.'
    ],
    'confirm_message' => [
        'are_you_sure_delete' => 'Are you sure you want to delete',
        'confirm_to_delete' => 'Yes',
        'confirm_to_restore' => 'Yes',
        'confirm_to_report_broken' => 'Yes',
        "are_you_sure_report_broken" => 'Are you sure you want to report broken',
    ],
    'toast_message' => [
        'bulk_delete_nothing' => 'You haven\'t selected anything to delete',
        'bulk_restore_nothing' => 'You haven\'t selected anything to restore'
    ]
];
