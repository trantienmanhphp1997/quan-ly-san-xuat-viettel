<?php

return [
    'common_field' => [
        'id' => '#',
        'name' => 'Name',
        'content' => 'Content',
        'phone' => 'Phone_number',
        'email' => 'E-mail',
        'action' => 'Action',
        'create_at' => 'Create time',
        'update_at' => 'Last update time',
        'create_by' => 'Create by',
        'update_by' => 'Update by',
        'status' => 'Status'
    ],
    "record_plan" => [
        "change_status" => "Change status"
    ]
];
