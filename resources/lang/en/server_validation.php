<?php
return [
    "record_plan" => [
        "name" => [
            "required" => "Record plan's name is required",
            "max" => "Record plan's name is too long",
            "unique" => "Record plan's name is already exist"
        ],
        "address" => [
             "required" => "Record plan's address is required",
        ],
        "start_time" => [
            "required" => "Record plan's start time is required",
        ],
        "end_time" => [
             "required" => "Record plan's end time is required",
        ]
    ],

    "car" => [
        "license_plate" => [
            "unique" => "License plate 's already existed",
        ]
    ]

];
