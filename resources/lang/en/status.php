<?php
return [
    "record_plan" => [
        "inactive" => "Inactive",
        "pending" => "Pending",
        "approved" => "Approved",
        "in_progress" => "In progress",
        "wrap" => "Wrap", //Đã đóng máy, chưa chốt xong giấy tờ
        "done" => "Done",
        "cancel" => "Cancel"
    ],
    "propose" => [
        "pending" => "Pending",
        "approved" => "Approved",
        "rejected" => "Rejected",
        "cancel" => "Cancel"
    ],
    "propose_device" => [
        "pending" => "Pending",
        "approved" => "Approved",
        "delivered" => "Delivered",
        "returned" => "Returned",
        "rejected" => "Rejected",
    ],
    "member" => [
        "inactive" => "Inactive",
        "active" => "Active",
    ],
    "car" => [
        "all" => "All",
        "inactive" => "Inactive",
        "active" => "Active",
        "borrowed" => "Borrowed",
        "broken" => "Broken",
        "busy" => "Busy"
    ],
    "device" => [
        "all" => "All",
        "inactive" => "Inactive",
        "active" => "Active",
        "borrowed" => "Borrowed",
        "instock" => "In Stock",
        "broken" => "Broken",
    ],
    "device_category" => [
        "ready" => "Ready",
    ],
    "assignments" => [
        "devices" => [
            "proposal" => "Proposal",
            "accept" => "Accept",
            "cancel" => "Cancel"
        ],
        "members" => [
            "proposal" => "Proposal",
            "accept" => "Accept",
            "cancel" => "Cancel"
        ]
    ],
    "stage" => [
        "active" => "Active",
        "inactive"=> "Inactive"
    ]
];
