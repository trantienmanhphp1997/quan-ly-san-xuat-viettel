<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Tài khoản hoặc mật khẩu không hợp lệ.',
    'throttle' => 'Bạn đã thử quá nhiều lần. Vui lòng thử lại sau :seconds giây.',
    "error" => [
        "username" => [
            "required" => "Tên đăng nhập là bắt buộc",
            "max" => "Tên đăng nhập tối đa 191 ký tự"
        ],
        "password" => [
            "required" => "Mật khẩu là bắt buộc",
            "max" => "c"
        ]
    ]
];
