<?php
return [
    "form" => [
        "required" => [
            "common" => [
                "abstract" => "Bạn chưa nhập :fieldName",
                "user_name" => "Bạn chưa nhập tên đăng nhập",
                "password" => "Bạn chưa nhập mật khẩu",
                "phone_number" => "Bạn chưa điền mật khẩu",
                "full_name" => "Tên đầy đủ"
            ],
            "contract" => [
                "department_id" => "Bạn chưa chọn phòng ban lập hợp đồng",
                "statement_id" => "Bạn chưa chọn tờ trình tương ứng của hợp đồng",
            ],
            "plan" => [
                "department" => "Bạn chưa chọn phòng ban",
            ],
            "manager" => [
                "department_code" => "Bạn chưa chọn nhóm cho quản lý",
                "password" => "Bạn chưa nhập mật khẩu"
            ],
            "outsource" => [
                "type" => "Bạn chưa chọn loại nguồn lực",
            ],
            "record_plan" => [
                "plan" => "Bạn chưa chọn kế hoạch",
                "address" => "Bạn chưa nhập thông tin địa chỉ",
                "address2" => "Bạn chưa chọn phòng quay",
                "offer_technical_camera_number" => "Bạn chưa chọn đề xuất số lượng kỹ thuật phòng quay",
            ],
            "phone_number" => "Bạn chưa nhập số điện thoại",
            "device" => [
                "device_category_id" => "Bạn chưa chọn danh mục thiết bị"
            ],
            "user" => [
                "current_password" => "Bạn chưa nhập mật khẩu hiện tại",
                "new_password" => "Bạn chưa nhập mật khẩu mới",
                "confirm_password" => "Bạn chưa xác nhận mật khẩu mới"
            ]
        ],
        "max_length" => [
            "abstract" => "Thông tin :fieldName không dài quá :maxlength ký tự",
        ],
        "format"=>[
            "email" => "Email không đúng định dạng",
            "phone_number" => "Số điện thoại không đúng định dạng",
            "file" => "Định dạng file không đúng"
        ],
        "number" => [
            "min" => ":fieldName phải lớn hơn :min",
            "number" => "Số không hợp lệ"
        ],
        "equal" => [
            "confirm_password" => "Xác nhận mật khẩu mới chưa chính xác"
        ]
    ]
];
