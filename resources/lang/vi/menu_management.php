<?php
return [
    "subheader" => "Quản lý danh mục",
    "table_column" => [
        "name" => "Tên danh mục",
        "href" => "Đường dẫn",
        "parent" => "Danh mục cha",
        "actions" => "Thao tác"
    ],
    "menu_name" =>[
            "record_plans" => "Lịch sản xuất",
            "reporter" => "Quản lý phóng viên",
            "reporter_list" => "Danh sách phóng viên",
            "reporter_assignment" => "Phân công phóng viên",
            "reporter_proposal" => "Đề xuất phóng vien",
            "outsources" => "Quản lý nguồn lực ngoài",
            "outsource_types" => "DS loại nguồn lực ngoài",
            "outsource_list" => "DS nguồn lực ngoài",
            "plans" => "Kế hoạch",
            "statements" => "Tờ trình",
            "contracts" => "Hợp đồng",
            "camera" => "Quản lý quay phim",
            "camera_list" => "Danh sách quay phim",
            "camera_assignment" => "Phân công quay phim",
            "camera_proposal" => "Đề xuất quay phim",
            "cars" => "Quản lý xe",
            "departments" => "Quản lý phòng ban",
            "device_assignment" => "Phân công thiết bị",
            "device_categories" => "Danh mục thiết bị",
            "device_lending" => "Cấp thiết bị",
            "device_list" => "Danh sách thiết bị",
            "device_proposal" => "Đề xuất thiết bị",
            "device_returning" => "Nhận lại thiết bị",
            "devices" => "Quản lý thiết bị",
            "managers" => "Danh sách quản lý",
            "transportation_list" => "Danh sách lái xe",
            "transportation" => "Quản lý lái xe",
            "transportation_assignment" => "Phân công lái xe",
            "transportation_proposal" => "Đề xuất lái xe",
            "technical_camera" => "Kỹ thuật phòng quay",
            "technical_camera_list"=> "Danh sách kỹ thuật phòng quay",
            "technical_camera_assignment" => "Phân công kỹ thuật phòng quay",
            "technical_camera_proposal"=> "Đề xuất kỹ thuật phòng quay",
            "studios" => "Danh sách phòng quay"

    ]
];
