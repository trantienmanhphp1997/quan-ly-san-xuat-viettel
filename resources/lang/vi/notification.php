<?php
return [
    "common" => [
        "success" => [
            "add" => "Thêm mới thành công",
            "update" => "Cập nhật thành công",
            "destroy" => "Hủy thành công",
            "delete" => "Xóa thành công",
            "report-broken" => "Báo hỏng thành công",
            "restore" => "Khôi phục thành công",
            "update-status" => "Cập nhật trạng thái thành công",
            "import" => "Import thành công",
        ],
        "fail" => [
            "no-data" => "Không tìm thấy bản ghi nào",
            "no-data-selected" => "Chưa có bản ghi nào được chọn",
            "no-device-selected" => "Bạn chưa chọn thiết bị nào",
            "add" => "Thêm mới thất bại",
            "update" => "Cập nhật thất bại",
            "report_broken" => "Báo hỏng thất bại",
            "restore" => "Khôi phục thất bại",
            "delete" => "Xóa thất bại",
            "delete_parent" => "Không thể xóa Danh mục có chứa danh mục con",
            "update-status" => "Cập nhật trạng thái thất bại",
            "import" => "Import thất bại",
            "no_menu" => "Bạn chưa chọn menu nào",
            "propose_number" => "Số lượng đề xuất hiện tại đang nhỏ hơn số lượng thực tế đã phân công",
            "no_department_code" =>  "Chưa chọn phòng ban"
        ],
    ],
    "member" => [
        "warning" => [
            "delete_member" => "Bạn không thể xóa nhân sự :member_name do :member_name đang tham gia lịch sản xuất :record_plan_name",
            "delete_list_members" => "Có nhân sự đang tham gia lịch sản xuất. Vui lòng kiểm tra lại",
            "delete_device" => "Bạn không thể xóa thiết bị :device_name do thiết bị đang tham gia lịch sản xuất :record_plan_name",
            "delete_list_devices" => "Có thiết bị đang tham gia lịch sản xuất. Vui lòng kiểm tra lại",
            "reallocate_device" => "Lịch sản xuất :record_plan_name cần phân công lại thiết bị.",
            "reallocate_member" => "Lịch sản xuất :record_plan_name cần phân công lại nhân sự.",
        ]
    ],
    "assignment" => [
        "success" => [
            "add-note" => "Thêm ghi chú thành công",
            'add' => [
                "member" => "Phân công nhân sự thành công",
                "device" => "Phân công thiết bị thành công"
            ],
            "delete" => [
                "member" => "Xóa phân công nhân sự thành công",
                "device" => "Xoá phân công thiết bị thành công"
            ],
            "accept" => [
                "device" =>  "Phê duyệt thiết bị thành công"
            ],
            "propose_device" => [
                "delivered" => "Giao thiết bị thành công",
                "returned" => "Nhận lại thiết bị thành công"
            ]
        ],
        "fail" => [
            "add-note" => "Thêm ghi chú thất bại",
            'add' => [
                "member" => "Phân công nhân sự thất bại",
                "device" => "Phân công thiết bị thất bại"
            ],
            "delete" => [
                "member" => "Xóa phân công nhân sự thất bại",
                "device" => "Xoá phân công thiết bị thất bại"
            ],
            "accept" => [
                "device" =>  "Phê duyệt thiết bị thất bại"
            ],
            "exceed" => "Lịch sản xuất chỉ cần :proposalNumber :department",
            "shortage" => "Lịch sản xuất cần :proposalNumber :department",
            "deliver_device" => "Thiết bị chưa sẵn sàng trong kho, bạn không thể cấp cho lịch :record_plan_name"
        ]
    ],
    "device_category" => [
        "hasItem" => "Không thể xóa danh mục có chứa thiết bị"
    ],
    "record_plan" => [
        "statusCannotDel" => "Không thể xóa lịch sản xuất đang ở trạng thái đã chốt/đang diễn ra/đóng máy/hoàn thành",
        "notYetTime" => "Chưa đến giờ bắt đầu lịch sản xuất",
        "notEnoughPeople" => "Lịch sản xuất chưa đầy đủ nguồn lực"
    ],
    "update_status_record_plan" => [
        1 => [
            "success" => "Phục hồi lịch sản xất thành công",
            "fail" => "Phục hồi lịch sản xuất thất bại"
        ],
        3 => [
            "success" => "Bắt đầu lịch sản xất thành công",
            "fail" => "Bắt đầu lịch sản xuất thất bại"
        ],

        4 => [
            "success" => "Đóng máy thành cônng",
            "fail" => "Đóng máy thất bại"
        ],

        5 => [
            "success" => "Đã hoàn thành lịch sản xuất",
            "fail" => "Lịch sản xuất chưa hoàn thành"
        ],

        -1 => [
            "success" => "Hủy lịch thành công",
            "fail" => "Hủy lịch thất bại"
        ]
    ]
];
