<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Đặt lại mật khẩu thành công thành công!',
    'sent' => 'Chúng tôi đã gửi link khôi phục mật khẩu vào email cho bạn!',
    'throttled' => 'Thao tác thử quá nhanh',
    'token' => 'Token khôi phục mật khẩu không hợp lệ',
    'user' => "e-mail không tồn tại",
    'forgot' => "Bạn quên mật khẩu"

];
