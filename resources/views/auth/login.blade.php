@extends('layouts.master-mini')
@section('content')

    <div class="content-wrapper d-flex align-items-center justify-content-center auth theme-one" style="background-image: url({{ url('assets/images/auth/login_1.jpg') }}); background-size: cover;">
        <div class="row w-100">
            <div class="col-lg-4 mx-auto">
                <div class="auto-form-wrapper">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label class="label">{{ __('data_field_name.user.user_name') }}</label>
                                <div class="input-group @error('username') is-invalid @enderror">
                                    <input id="username" type="text" class="form-control @error('username') invalid @enderror"  name="username" placeholder="{{ __('data_field_name.user.user_name') }}">
                                </div>
                                @error('username')
                                <span class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>
                        <div class="form-group">
                            <label class="label">{{ __('data_field_name.user.password') }}</label>
                                <div class="input-group @error('password') is-invalid @enderror">
                                    <input id="password" type="password"  placeholder="*********" class="form-control @error('password') invalid @enderror" name="password" required autocomplete="current-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                        </div>
{{--                        <div class="form-group d-flex justify-content-end">--}}
{{--                            @if (Route::has('password.request'))--}}
{{--                                <a class="text-small forgot-password text-black" href="{{ route('password.request') }}">--}}
{{--                                    {{ __('passwords.forgot') }}--}}
{{--                                </a>--}}
{{--                            @endif--}}
{{--                        </div>--}}

                        <div class="form-group">
                            <button class="btn btn-primary submit-btn btn-block">{{ __('common.button.login') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection




