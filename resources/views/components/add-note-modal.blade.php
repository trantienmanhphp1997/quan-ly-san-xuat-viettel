<div class="modal" id="add-note-to-assign-{{$type}}" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Xác nhận</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div id="confirmMessageContent_{{$type}}"></div>
                <label for="">Ghi chú</label>
                <textarea class="form-control" name="content" id="contentMessage_{{$type}}" cols="20" rows="5">{{$message}}</textarea>
                <input type="hidden" id="fromDepartment_{{$type}}" name="department">
            </div>
            <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn btn-sm" title="{{ __('common.button.cancel') }}">{{ __('common.button.cancel') }}</a>
                <a href="#" class="btn btn-primary btn-sm" title="{{ __('common.button.confirm') }}" id="confirmForOutSource_{{$type}}">{{ __('common.button.confirm') }}</a>
            </div>
        </div>
    </div>
</div>
