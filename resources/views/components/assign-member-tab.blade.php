<div class="tab-pane fade {{$status}} show" id="{{$id}}" role="tabpanel" aria-labelledby="{{$labelledby}}" style="">
    <div class="row">
        <div class="table-responsive">
            <div class="form-group">
                <div class="d-flex justify-content-between mb-1">
                    <label>{{$title}} <span class="text-danger error_{{$formName}}"></span></label>
                </div>

                <div>
                    <div class="table-responsive tableFixHead">
                        <table class="table table-hover table-striped table-head-fixed {{$formName}}" id="assignMemberTable">
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" class="{{$labelledby}}_select_all">
                                    </th>
                                    <th>{{__('data_field_name.common_field.stt')}}</th>
                                    <th>{{__('data_field_name.user.full_name')}}</th>
                                    <th>{{__('data_field_name.common_field.phone')}}</th>
                                    <th>{{__('data_field_name.common_field.email')}}</th>
                                    <th>{{__('data_field_name.common_field.department')}}</th>
                                    @if($labelledby == config("common.departments.transportation"))
                                        <th>Hình thức tham gia</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody id="{{$formId}}"></tbody>
                        </table>
                    </div>
                </div>
                <div class="assigned_{{$formName}}"></div>
            </div>
        </div>
    </div>
</div>
