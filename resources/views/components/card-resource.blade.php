<div class="mb-2 w-100 float-left mt-3">
    <div class="float-left">
        <h6>{{$title}} <small class="text-secondary">{!! $proposals !!}</small></h6>
        <div class="text-secondary">
            <div class="label-note">
                <small>* Ghi chú: </small>
            </div>
            <div class="content ml-3">
                <small>{!! nl2br($note) !!}</small>
            </div>
        </div>
        @if(isset($outsourceNote) && $outsourceNote)
            <div class="text-secondary">
                <div class="label-note">
                    <small>* Ghi chú nhân sự thuê ngoài: </small>
                </div>
                <div class="content ml-3">
                    <small>{!! nl2br($outsourceNote) !!}</small>
                </div>
            </div>
        @endif
    </div>
</div>
<div class="w-100 table-responsive-xl tableFixHead">
    <!-- <p class="ml-0 mb-0">(Ghi chú)</p> -->
    @if(count($memberAssgined) > 0)
        <table class="table w-100 border-0 table-striped table-head-fixed">
            <thead>
            <tr class="row p-2">
                <th class="col-sm-4">{{__('data_field_name.user.full_name')}}</th>
                <th class="col-sm-2">{{__('data_field_name.common_field.phone')}}</th>
                <th class="col-sm-2">{{__('data_field_name.common_field.department')}}</th>
                @if($departmentCode == config("common.departments.transportation"))
                    <th class="col-sm-2">Hình thức</th>
                    @else
                    <th class="col-sm-2"></th>
                @endif
                <th class="col-sm-2">{{__("data_field_name.common_field.action")}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($memberAssgined as $member)
                <tr class="row p-2">
                    <td class="col-sm-4">{{$member->full_name}}</td>
                    <td class="col-sm-2">{{$member->phone_number}}</td>
                    <td class="col-sm-2">{{ __("department.$member->department_code") }}</td>
                    @if($departmentCode == config("common.departments.transportation"))
                        <td class="col-sm-2">{{isset($member->record_plan_member_xref[0]->involve_type) ? __('common.transportation_involve_type.'.$member->record_plan_member_xref[0]->involve_type) : ''}}</td>
                    @else
                        <td class="col-sm-2"></td>
                    @endif
                    @can("removeAssignedMember",  $departmentCode)
                        <td class="col-sm-2">
                            @can("assignResource", $recordPlan)
                                <button class="btn btn-danger btn-sm unassign-btn" id="{{$member}}"
                                        title="{{__('common.button.remove-assign')}}">
                                    {{__('common.button.remove-assign')}}
                                </button>
                            @endcan
                        </td>
                    @endcan
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div class="container">
            <p style="text-align: center">{{$nodataMessage}}</p>
        </div>
    @endif
</div>
