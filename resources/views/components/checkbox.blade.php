<div class="form-group row">
    <label class="col-md-3 col-form-label">{{$label}}</label>
    <div class="col-md-9 col-form-label">
        <div class="form-check checkbox">
            <input type="checkbox" class="form-check-input" name="{{$formName}}" {{(isset($checked) && $checked) ? "checked" : ""}} {{isset($disabled) && $disabled == true ? "disabled" : "" }} >
        </div>
    </div>
</div>
