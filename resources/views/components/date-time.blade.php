<div class="form-group {{$horizontal ? "" : "row"}}"">
    <label class=" {{$horizontal ? "" : "col-md-3 col-form-label"}}"> {{$label}}
    @if(isset($required) && $required == "true")
    <span class="text-danger">(*)</span>
    @endif
    </label>
    <div class="{{$styleInput ? $styleInput : "col-md-9"}}">
        <div class="input-group {{$formName}}">
            <input class="form-control " type="text" id="{{$formName}}" data-input class=""
                value="{{ $value ? date("Y-m-d H:i" , strtotime($value)) : "" }}"
                {{isset($disabled) && $disabled == true ? "disabled" : "" }} placeholder="Chọn thời gian..."
                name="{{$formName}}">
            <div class="input-group-append" data-toggle>
                <button type="button" class="btn btn-primary"><i class="fa fa-calendar" aria-hidden="true"></i></button>
            </div>
        </div>
        <label id="{{$formName}}-error" class="error" for="{{$formName}}"></label>
        @error($formName)
        <span class="text-danger">
            {{ $message }}
        </span>
        @enderror
    </div>
    @if(isset($disabled) && $disabled == true)
    <input type="hidden" name="{{$formName}}" value="{{$value}}">
    @endif
</div>
@if((isset($required) && $required == "true"))
    @push('validation-rules')
        {{$formName}}: {
            @if(isset($required) && $required == "true")
                "required" : true,
            @endif
            },
    @endpush

    @push('validation-messages')
        {{$formName}}: {
            @if(isset($required) && $required == "true")
                required: "{{__('client_validation.form.required.common.abstract', ["fieldName" => $label])}}",
            @endif
        },
    @endpush
@endif
