{{--<li @if($level==0)class="init-arrow-down divbutton i-12"@endif>--}}
{{--    <a href="javascript:void(0)"> <span class="gw-menu-text">{{$item->name}}</span> <b class="gw-arrow icon-arrow-up8"></b>--}}
{{--        <span class="float-right btn-hidden">--}}
{{--                          <button class="btn btn-default btn-sm mr-1" data-toggle="modal" data-target="#createnew"><i class="fas fa-plus"></i></button>--}}
{{--                          <button class="btn btn-default btn-sm mr-1" data-toggle="modal" data-target="#createnew"><i class="fas fa-pen"></i></button>--}}
{{--                          <button class="btn btn-default btn-sm mr-1"  data-toggle="modal" data-target="#history"><i class="fas fa-history"></i></button>--}}
{{--                          <button class="btn btn-default btn-sm mr-1" data-toggle="modal" data-target="#delete"><i class="fas fa-trash"></i></button>--}}
{{--                        </span>--}}
{{--    </a>--}}
{{--    @if(isset($item->children) && count($item->children) > 0)--}}
{{--        <ul class="{{$level == 0 ? 'gw-submenu' : 'child-menu'}}">--}}
{{--            @foreach($item->children as $child)--}}
{{--                <x-department-item :item=$child level="{{$level+1}}"></x-department-item>--}}
{{--            @endforeach--}}
{{--        </ul>--}}
{{--    @endif--}}
{{--</li>--}}

@if($level==0)
    <li @if($level==0)class="init-arrow-down divbutton i-12"@endif>
        <a href="javascript:void(0)"> <span class="gw-menu-text">{{$item->name}}</span> <b
                class="gw-arrow icon-arrow-up8"></b>
            <div class="float-right btn-hidden">
                <!-- <button class="btn btn-default btn-sm mr-1" data-action="access-permission" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.access')}}"
                        data-department-id="{{$item->id}}">
                    <i class="far fa-eye"></i>
                </button>
                <button class="btn btn-default btn-sm mr-1" data-action="feature-permission" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.function')}}"
                        data-department-id="{{$item->id}}">
                    <i class="fas fa-user-tag"></i>
                </button> -->
                <!-- <button class="btn btn-default btn-sm mr-1" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.plus')}}" data-target="#createnew"><i
                        class="fas fa-plus"></i></button>
                <button class="btn btn-default btn-sm mr-1" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.edit')}}" data-target="#createnew"><i
                        class="fas fa-pen"></i></button> -->
                <!-- <button class="btn btn-default btn-sm mr-1" data-target="#history"><i
                        class="fas fa-history"></i></button> -->
                <!-- <button class="btn btn-default btn-sm mr-1" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.delete')}}" data-target="#delete"><i
                        class="fas fa-trash"></i></button> -->
            </div>
        </a>
        @if(isset($item->children) && count($item->children) > 0)
            <ul class="{{$level == 0 ? 'gw-submenu' : 'child-menu'}}">
                @foreach($item->children as $child)
                    <x-department-item :item=$child level="{{$level+1}}"></x-department-item>
                @endforeach
            </ul>
        @endif
    </li>
@elseif($level==1)
    <li>
        <a href="javascript:void(0)" class="menu-parent divbutton i-12"><p class="mb-0"><i
                    class="more-less fas fa-arrow-down mr-1"></i>{{$item->name}}</p>
            <div class="float-right btn-hidden" style="margin-top: -23px;">
                <!-- <button class="btn btn-default btn-sm mr-1" data-action="access-permission" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.access')}}"
                        data-department-id="{{$item->id}}">
                    <i class="far fa-eye"></i>
                </button>
                <button class="btn btn-default btn-sm mr-1" data-action="feature-permission" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.function')}}"
                        data-department-id="{{$item->id}}">
                    <i class="fas fa-user-tag"></i>
                </button> -->
               {{-- <button class="btn btn-default btn-sm mr-1" data-toggle="modal" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.plus')}}"
                        data-target="#createnew"><i class="fas fa-plus"></i></button>
                <button class="btn btn-default btn-sm mr-1" data-toggle="modal" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.edit')}}"
                        data-target="#createnew"><i class="fas fa-pen"></i></button>
                <!-- <button class="btn btn-default btn-sm mr-1" data-toggle="modal"
                        data-target="#history"><i class="fas fa-history"></i></button> -->
                <button class="btn btn-default btn-sm mr-1" data-toggle="modal" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.delete')}}" data-target="#delete"><i
                        class="fas fa-trash"></i></button>--}}
            </div>
        </a>
        @if(isset($item->children) && count($item->children) > 0)
            <div class="child-menu">
                @foreach($item->children as $child)
                    <x-department-item :item=$child level=2></x-department-item>
                    {{--                    <a href="#" class="divbutton i-12"><p class="mb-0">{{$child->name}}</p>--}}
                    {{--                        <span class="float-right btn-hidden"  style="margin-top: -23px;">--}}
                    {{--                                  <button class="btn btn-default btn-sm mr-1" data-toggle="modal" data-target="#createnew"><i class="fas fa-plus"></i></button>--}}
                    {{--                                  <button class="btn btn-default btn-sm mr-1" data-toggle="modal" data-target="#createnew"><i class="fas fa-pen"></i></button>--}}
                    {{--                                  <button class="btn btn-default btn-sm mr-1"  data-toggle="modal" data-target="#history"><i class="fas fa-history"></i></button>--}}
                    {{--                                  <button class="btn btn-default btn-sm mr-1" data-toggle="modal" data-target="#delete"><i class="fas fa-trash"></i></button>--}}
                    {{--                                </span>--}}
                    {{--                    </a>--}}
                @endforeach
            </div>
        @endif
    </li>
@elseif($level==2)
    <a href="#" class="divbutton i-12"><p class="mb-0">{{$item->name}}</p>
        <div class="float-right btn-hidden" style="margin-top: -23px;">
            <!-- <button class="btn btn-default btn-sm mr-1" data-action="access-permission" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.access')}}"
                    data-department-id="{{$item->id}}">
                <i class="far fa-eye"></i>
            </button>
            <button class="btn btn-default btn-sm mr-1" data-action="feature-permission" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.function')}}"
                    data-department-id="{{$item->id}}">
                <i class="fas fa-user-tag"></i>
            </button> -->
            {{--<button class="btn btn-default btn-sm mr-1" data-toggle="modal" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.plus')}}"
                    data-target="#createnew"><i class="fas fa-plus"></i></button>
            <button class="btn btn-default btn-sm mr-1" data-toggle="modal" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.edit')}}"
                    data-target="#createnew"><i class="fas fa-pen"></i></button>
            <!-- <button class="btn btn-default btn-sm mr-1" data-toggle="modal"
                    data-target="#history"><i class="fas fa-history"></i></button> -->
            <button class="btn btn-default btn-sm mr-1" data-toggle="modal" data-toggle="modal" data-toggle="tooltip" data-placement="top" title="{{__('common.tooltip.menu.delete')}}" data-target="#delete"><i
                    class="fas fa-trash"></i></button>--}}
        </div>
    </a>
@endif
