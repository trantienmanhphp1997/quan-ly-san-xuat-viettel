<div class="form-group {{$styleAll ? $styleAll : "row"}}">
    <label class="{{$styleLabel ? $styleLabel : "col-md-3 col-form-label"}}">{{$label}}
        @if(isset($required) && $required == "true")
            <span class="text-danger">(*)</span>
        @endif
    </label>
    <div class="{{$styleInput ? $styleInput : "col-md-9"}}">
        <input type="{{$type ?? "text"}}" class="form-control" name="{{$formName}}" autocomplete="off"
               id="{{$formName}}" {{isset($required) && $required == "true" ? "required" : "" }} value="{{(old($formName)) ? html_entity_decode(old($formName)) : html_entity_decode($value)}}"
               placeholder="{{$placeholder? $placeholder : (empty($disabled) ?  __('data_field_name.action.enter', ["dataName" => $label]) : '')}}"
            {{isset($disabled) && $disabled == true ? "disabled" : "" }}
        >
        @error($formName)
        <label class="error">
                {{ $message }}
        </label>
        @enderror

        {{ $slot }}
    </div>
</div>

@php
    $typeArr = ["text","number","email"]
@endphp
@if((isset($required) && $required == "true") || isset($maxlength) || (isset($type) && in_array($type,$typeArr)))
    @push('validation-rules')
        {{$formName}}: {
            @if(isset($required) && $required == "true")
                "required" : true,
                normalizer: function( value ) {
                    // Trim the value of the `field` element before
                    // validating. this trims only the value passed
                    // to the attached validators, not the value of
                    // the element itself.
                    return $.trim( value );
                },
            @endif
        @if((isset($type) && $type == "text") || isset($maxlength))
            maxlength: {{isset($maxlength) ? $maxlength : 100}},
        @endif
        @if((isset($type) && $type == "number") )
            min: "{{isset($min) ? $min : 1}}",
        @endif
        @if((isset($type) && $type == "email"))
            email: true,
        @endif
        },
    @endpush

    @push('validation-messages')
        {{$formName}}: {
        @if(isset($required) && $required == "true")
            required: "{{__('client_validation.form.required.common.abstract', ["fieldName" => $label])}}",
        @endif
        @if((isset($type) && $type == "text") || isset($maxlength))
            maxlength: "{{__('client_validation.form.max_length.abstract', ["fieldName" => $label, "maxlength" => isset($maxlength) ? $maxlength : 100])}}",
        @endif
        @if((isset($type) && $type == "number") )
            min: "{{__('client_validation.form.number.min', ["fieldName" => $label, "min" => isset($min) ? $min : 0])}}",
            number: "{{__('client_validation.form.number.number')}}",
        @endif
        @if((isset($type) && $type == "email"))
            email: "{{__('client_validation.form.format.email')}}",
        @endif
        },
    @endpush
@endif
