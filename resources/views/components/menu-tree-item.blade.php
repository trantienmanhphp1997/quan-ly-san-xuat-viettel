<li class="nav-item has-treeview {{ active_class([$activeRoute], 'menu-open', isset($parameterName)  ? $parameterName : "" , isset($parameterValue)  ? $parameterValue : "") }}">
    <a class="nav-link {{ active_class([$activeRoute], 'active', isset($parameterName)  ? $parameterName : "" , isset($parameterValue)  ? $parameterValue : "") }}" href="#">
        <i class="{{$iconClass}}"></i>
        <p class="menu-title">{{$label}}<i class="right fas fa-angle-left"></i></p>
    </a>
    <ul class="nav nav-treeview ml-3">
        {{ $slot }}
    </ul>
</li>
