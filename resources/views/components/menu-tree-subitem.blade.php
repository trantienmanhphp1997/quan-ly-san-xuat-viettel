<li class="nav-item">
    <a href={{$href}} class="nav-link {{ active_class([$activeRoute], 'active', isset($parameterName)  ? $parameterName : "" , isset($parameterValue)  ? $parameterValue : "") }}">
        <i class="{{isset($iconClass)  ? $iconClass : '' }}"></i>
        <p>{{$label}}</p>
    </a>
</li>
