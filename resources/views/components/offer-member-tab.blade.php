<div class="tab-pane fade {{$status}} show" id="offer-member-{{$id}}" role="tabpanel" aria-labelledby="{{"offer-member-".$labelledby}}">
    <div class="row">
        <div class="table-responsive">
            <div class="form-group">
                @if(!in_array($labelledby, ["reporter"]))
                <div>
                    <label for="">Số lượng</label>
                    <input type="number" class="form-control number-offer" id="{{$formInputId}}" name="{{"offer_".$labelledby."_number"}}" value="{{$recordPlan["offer_".$labelledby."_number"]}}">
                    <span class="error" id="error_{{$formInputId}}"></span>
                </div>
                @endif
                <div>
                    <label for="">Ghi chú</label>
                    <textarea class="form-control" name="{{"offer_".$labelledby."_note"}}" id="{{$formTextareaId}}" cols="10" rows="5">{{$recordPlan["offer_".$labelledby."_note"]}}</textarea>
                    <span class="error" id="error_{{$formTextareaId}}"></span>
                </div>

            </div>
        </div>
    </div>
</div>
@push('detail-record-plan')
    <script>
        $('#'+'{{$formInputId}}').on('keyup change',function() {
            if ($(this).val() < 0) {
                if ($('#error_'+'{{$formInputId}}').text() === "") {
                    $('#error_'+'{{$formInputId}}').append("Số lượng không hợp lệ");
                }
                $('#update-offer').prop("disabled",true);
            } else if ($(this).val() === 0 ) {
                if ($('#error_'+'{{$formInputId}}').text() === "") {
                    $('#error_'+'{{$formInputId}}').append("Số lượng phải lớn hơn 0");
                }
                $('#update-offer').prop("disabled",true);
            }else{
                $('#error_'+'{{$formInputId}}').empty();
                $('#update-offer').prop("disabled",false);
            }
        });

        $('#'+'{{$formTextareaId}}').on('keyup change',function() {
            if ($(this).val() !== "" && $(this).val().length > 100) {
                if ($('#error_'+'{{$formTextareaId}}').text() === "") {
                    $('#error_'+'{{$formTextareaId}}').append("Ghi chú đề xuất không dài quá 100 ký tự");
                }
                $('#update-offer').prop("disabled", true);
            } else {
                $('#edit_{{$formTextareaId}}').prop("disabled", false);
                $('#error_'+'{{$formTextareaId}}').empty();
            }
        });
    </script>
@endpush
