{{--<button class="btn btn-sm rounded-0--}}
{{--{{ $allParams && array_key_exists($name, $allParams)--}}
{{--    ? ($allParams[$name] == $value ? 'btn-primary' : 'btn-default')--}}
{{--    : ( ($value === "" || isset($defaultValue) && $value == $defaultValue)? 'btn-primary' : 'btn-default')}}"--}}
{{--        name={{$name}} value={{$value}}>{{$label}}</button>--}}
<button class="btn btn-link  mr-4 redhover p-0 quick-filter {{ $allParams && array_key_exists($name, $allParams)
    ? ($allParams[$name] == $value ? 'quick-filter-active ' : '')
    : ( (($value === "" && !isset($defaultValue)) || isset($defaultValue) && $value == $defaultValue)? 'quick-filter-active' : '')}}"

        name={{$name}} value={{$value}}>{{$label}}</button>
