    <select id="status-dropdown-filter" name="status" class="form-control form-control-sm rounded-0 custom-select-filter" data-dropdown-css-class="select2-blue">
        <option value="" @if(isset($allParams) == false || array_key_exists('status', $allParams) == false  || $allParams['status'] == "") selected @endif>Chọn trạng thái</option>
        @foreach(config('common.status.'. $dataType) as $key => $value)
            <option value="{{ $value}}"
                    @if(isset($allParams) && array_key_exists('status', $allParams)  && $allParams['status'] != "" && $allParams['status'] == $value) selected @endif
            >{{__('status.'. $dataType . '.' . $key)}}</option>
        @endforeach
    </select>
