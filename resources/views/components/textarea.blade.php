<div class="form-group {{$horizontal ? "" : "row"}}">
    <label class="{{$horizontal ? "" : "col-md-3 col-form-label"}}">{{$label}}
    @if(isset($required) && $required == "true")
            <span class="text-danger">(*)</span>
    @endif
    </label>
    <div class="{{$horizontal ? "" : "col-md-9"}}">
        <textarea class="form-control" name="{{$formName}}" {{isset($required) && $required == "true" ? "required" : ""}} autocomplete="off" placeholder="{{$placeholder? $placeholder : (empty($disabled) ?  __('data_field_name.action.enter', ["dataName" => $label]) : '')}}" {{isset($disabled) && $disabled == true ? "disabled" : "" }} rows="{{$row}}">{{$value ? $value : old($formName)}}</textarea>
    </div>
</div>

@if(((isset($required) && $required == "true") || isset($maxlength)))
    @push('validation-rules')
        {{$formName}}: {
            @if(isset($required) && $required == "true")
                "required" : true,
                normalizer: function( value ) {
                    // Trim the value of the `field` element before
                    // validating. this trims only the value passed
                    // to the attached validators, not the value of
                    // the element itself.
                    return $.trim( value );
                },
            @endif
            @if(isset($maxlength))
                maxlength: {{$maxlength}},
            @endif
        },
    @endpush

    @push('validation-messages')
        {{$formName}}: {
        @if(isset($required) && $required == "true")
            required: "{{__('client_validation.form.required.common.abstract', ["fieldName" => $label])}}",
        @endif
        @if(isset($maxlength))
            maxlength: "{{__('client_validation.form.max_length.abstract', ["fieldName" => $label, "maxlength" => $maxlength])}}",
        @endif
        },
    @endpush
@endif
