@extends('layouts.master')

@section('page_header')

@section('content')
    <div class="page-content browse container-fluid">
        {{--        @if(empty($filterOptions) == false)--}}
        {{--            @include('partials.filter', ['allParams' => $allParams, 'filterOptions' => $filterOptions])--}}
        {{--        @endif--}}

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row m-0 pb-2">
                            <div class="float-left">
                                @yield("buttons")
                                @can('deleteAny', app($modelClass))
                                    @if(isset($allParams) && array_key_exists("is_active", $allParams) && $allParams["is_active"] == -1)
                                        @include("partials.text-btn.bulk-restore")
                                    @else
                                        @include('partials.text-btn.bulk-delete')
                                    @endif
                                @endcan
                                @yield('custom-buttons')
                            </div>

                            @include('partials.search', ['allParams' => $allParams, 'filterOptions' => $filterOptions])
                        </div>

                        @if ($data ->isEmpty())
                            <p class="text-center">No data avaiable</p>
                        @else
                            @yield('table-list')
                        @endif

                        {{--                         ==============================Pagination==============================--}}
                        <div style="padding-top: 10px">
                            <div class="float-left">
                                <div role="status" class="show-res" aria-live="polite">{{ __(
                                                            'common.message.showing_entries', [
                                                                'from' => $data->total() > 0 ? ($data->currentPage() - 1) *  $data->perPage() + 1 : 0,
                                                                'to' => $data->currentPage() == $data->lastPage() ? $data->total() : $data->currentPage() *  $data->perPage() ,
                                                                'all' =>$data->total()
                                                            ])}}</div>
                            </div>
                            <div class="float-right">
                                {{ $allParams && array_key_exists("s", $allParams)  ? $data->appends([
                                    's' => $allParams['s'],
                                ])->links() : $data->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('javascript')
    @include("partials.message")
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            $('.select_all').on('click', function (e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
            });
        });

        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = ''.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        $('input[name="row_id"]').on('change', function () {
            var ids = [];
            $('input[name="row_id"]').each(function () {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            $('.selected_ids').val(ids);
        });
    </script>
@endsection

