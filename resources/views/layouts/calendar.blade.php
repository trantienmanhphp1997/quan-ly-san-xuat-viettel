@extends('layouts.master')
@section('css')
    <link href="{{ asset('css/calendar.min.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <div class="container">
        <div class="response"></div>
        <div id='calendar'></div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/calendar.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            var SITEURL = "{{url('/')}}";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


        });
    </script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                headerToolbar: {
                    left: 'prevYear,prev,next,nextYear today',
                    center: 'title',
                    right: 'dayGridMonth,dayGridWeek,dayGridDay'
                },
                navLinks: true, // can click day/week names to navigate views
                editable: false,
                dayMaxEvents: true, // allow "more" link when too many events
                events: [
                    @foreach($data as $recordPlan)
                    {
                        title : '{{ $recordPlan->name }}',
                        start : '{{ $recordPlan->start_time }}',
                        end: '{{ $recordPlan->end_time }}',
                    },
                    @endforeach
                ]
            });

            calendar.render();
        });
    </script>
@endsection
