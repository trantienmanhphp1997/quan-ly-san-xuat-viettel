@extends('layouts.master')

@push('validation-scripts')
    <script>
        //add method validation
        jQuery.validator.addMethod("phoneNumber", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            if(phone_number.indexOf("+") == 0){
                phone_number = phone_number.replace("+84","0");
            }
            return this.optional(element) || phone_number.length == 10 &&
            phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
        }, "Please specify a valid phone number")
        // validate form
        $("#detail-form").validate({
            rules: {
                @stack('validation-rules')
            },
            messages: {
                @stack('validation-messages')
            },
        });

        // khi đã validate xong trên form -> nhấn btn submit, hiển thị spinner btn
        $(".btn-action").click(function() {
            if ($('#detail-form').valid()) {
                // Kiểm tra trạng thái btn submit
                // - enabled: sunmit form , disabled btn,
                if(!$(".btn-action").prop('disabled')){
                     //submit form
                    $('#detail-form').submit();
                     //disabled btn submit
                    $(".btn-action").prop("disabled", true);
                     //add spinner loading...
                    $(".btn-action").html(
                        '<i class="fa fa-circle-o-notch fa-spin"></i> loading...'
                    );
                }
                return true;
            }else{
                // alert("Validation failed");
                return false;
            }
        });
    </script>
@endpush
@push('custom-scripts')
    <script>
    $( document ).ready(function() {
        $('#link').val(localStorage.getItem("prevUrl"));
    });
        function goBackUrlTest()
        {
            document.getElementById("go-back-url-test").href=localStorage.getItem("prevUrl");
        }
    </script>
@endpush
