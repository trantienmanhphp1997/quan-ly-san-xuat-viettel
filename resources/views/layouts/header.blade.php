<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <!-- SEARCH FORM -->
    <div class="form-group mb-0">
        <p class="mb-1 font-weight-bold text-uppercase text-primary">Quản lý sản xuất</p>

    </div>
    <!-- Right navbar links -->
{{--    <ul class="navbar-nav ml-auto">--}}
{{--        <li class="nav-item form-inline">--}}
{{--            <div class="form-group">--}}
{{--                <select class="select2 rounded-0 w-100" data-dropdown-css-class="select2-blue">--}}
{{--                    <option selected="selected">Bệnh viện Yên Bái</option>--}}
{{--                    <option>Bệnh viện Bạch Mai</option>--}}
{{--                </select>--}}
{{--            </div>--}}
{{--        </li>--}}
{{--    </ul>--}}

    <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell fa-2"></i>
                <span class="badge badge-danger navbar-badge">3</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                        <img src="../../assets/images/faces/face10.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                        <div class="media-body">
                            <h3 class="dropdown-item-title">
                                Brad Diesel
                                <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                            </h3>
                            <p class="text-sm">Call me whenever you can...</p>
                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                        <img src="../../assets/images/faces/face12.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                        <div class="media-body">
                            <h3 class="dropdown-item-title">
                                John Pierce
                                <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                            </h3>
                            <p class="text-sm">I got your message bro</p>
                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                        <img src="../../assets/images/faces/face1.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                        <div class="media-body">
                            <h3 class="dropdown-item-title">
                                Nora Silvester
                                <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                            </h3>
                            <p class="text-sm">The subject goes here</p>
                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <ul class="navbar-nav ml-auto d-flex align-items-center">
                <li class="nav-item dropdown">
                    <p class="dropdown-toggle mr-2 p-2" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-bottom: 0px;">
                        {{__('Hello')}}, <span><strong>{{ auth()->user()->username }}</strong></span>
                    </p>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" style="left:-60px !important;">
                        <a class="btn btn-sm action" href="{{route('change-password.index')}}">{{__('Change password')}}</a>
                    </div>
                </li>
                <li>|</li>
                <li class="nav-item dropdown">
                    <form action="{{route('logout')}}" method="POST">
                        @csrf
                        <button type="submit" class="btn "><span class="text-danger "><strong>{{__('common.button.logout')}}</strong></span></button>
                    </form>
                </li>
            </ul>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
