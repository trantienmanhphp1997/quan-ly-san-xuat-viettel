@extends('layouts.master')

@section('content')
    <div class="card">
        @if (isset($failures))
            <div class="alert alert-danger" role="alert">
                <strong>{{__("common.error")}}:</strong>
                <ul>
                    @foreach ($failures as $failure)
                    @if($failure->attribute())
                        <li> {{ "---- ".__("common.excel.row"). ' ' . $failure->row(). ' ' .__("common.excel.column").' ' .($failure->attribute() + 1) }}
                    @endif
                            @foreach($failure->errors() as $error)
                                {{$error}}
                                {{-- {{dd($failure->attribute())}} --}}
                            @endforeach
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        @php
            $memberArr = ['camera','technical_camera','transportation','reporter'];
            $name = in_array($routePrefix,$memberArr) ? 'members' : $routePrefix;
        @endphp
        <div class="card-body">
            <div class="w-50 m-auto">
                <form class="box" method="post" action="{{route($routePrefix.".store-import")}}" enctype="multipart/form-data">
                    @csrf
                    <div class="box__input text-center">
                        <input class="box__file" type="file" name="{{$name}}" id="file" data-multiple-caption="{count} files selected" multiple />
                        <label for="file">
                            <div class="upload-icon">
                                <i class="fas fa-cloud-upload-alt"></i>
                            </div>
                            <strong>{{__("common.button.choosefile")}}</strong>
                            <span class="box__dragndrop">{{__("common.button.drag")}}</span>
                        </label>
                        <div class="label">
                            <span class="file-name"></span>
                        </div>
                        @error($name)
                        <div class="show-error">
                            <label class= "error">
                                {{$message}}
                            </label>
                        </div>
                        @enderror
                        <div class="box_error"></div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-success mr-1" id="btn-upload">{{__("common.button.upload")}}</button>
                            <a href="{{route($routePrefix.".dowloadFile", ["fileName" => config("common.file_template.".$routePrefix)])}}" class="btn btn-info"><i class="fas fa-download mr-2"></i>{{__("common.button.download_template")}}</a>
                            @include("partials.back-btn", ["route" => "$routePrefix.index", "params" => []])
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('custom-scripts')
@include("partials.message")
<script>
	'use strict';

    ( function ( document, window, index )
    {
        $('#btn-upload').prop("disabled",true);
        // feature detection for drag&drop upload
        var isAdvancedUpload = function()
            {
                var div = document.createElement( 'div' );
                return ( ( 'draggable' in div ) || ( 'ondragstart' in div && 'ondrop' in div ) ) && 'FormData' in window && 'FileReader' in window;
            }();


        // applying the effect for every form
        var forms = document.querySelectorAll( '.box' );
        Array.prototype.forEach.call( forms, function( form )
        {
            var input		 = form.querySelector( 'input[type="file"]' ),
                label		 = form.querySelector( '.file-name' ),
                // errorMsg	 = form.querySelector( '.box__error span' ),
                restart		 = form.querySelectorAll( '.box__restart' ),
                droppedFiles = false,
                showFiles	 = function( files )
                {
                    label.textContent = files.length > 1 ? ( input.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', files.length ) : files[ 0 ].name;
                },
                triggerFormSubmit = function()
                {
                    var event = document.createEvent( 'HTMLEvents' );
                    event.initEvent( 'submit', true, false );
                    form.dispatchEvent( event );
                };

            // letting the server side to know we are going to make an Ajax request
            var ajaxFlag = document.createElement( 'input' );
            ajaxFlag.setAttribute( 'type', 'hidden' );
            ajaxFlag.setAttribute( 'name', 'ajax' );
            ajaxFlag.setAttribute( 'value', 1 );
            form.appendChild( ajaxFlag );
            var regex = /(.*?)\.(xlsx|xls)$/i ;
            // automatically submit the form on file select
            input.addEventListener( 'change', function( e )
            {
                showFiles(e.target.files);
                if (!(regex.exec(e.target.files[0].name))) {
                    $(".box_error").append('<label class= "error validate-file">{{__("client_validation.form.format.file")}}</label>');
                    $('#btn-upload').prop("disabled",true);
                }
                else{
                    $('#btn-upload').prop("disabled",false);
                    $('.validate-file').remove();
                }
            });

            // drag&drop files if the feature is available
            if( isAdvancedUpload )
            {
                form.classList.add( 'has-advanced-upload' ); // letting the CSS part to know drag&drop is supported by the browser

                [ 'drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop' ].forEach( function( event )
                {
                    form.addEventListener( event, function( e )
                    {
                        // preventing the unwanted behaviours
                        e.preventDefault();
                        e.stopPropagation();
                    });
                });
                [ 'dragover', 'dragenter' ].forEach( function( event )
                {
                    form.addEventListener( event, function()
                    {
                        form.classList.add( 'is-dragover' );
                    });
                });
                [ 'dragleave', 'dragend', 'drop' ].forEach( function( event )
                {
                    form.addEventListener( event, function()
                    {
                        form.classList.remove( 'is-dragover' );
                    });
                });
                form.addEventListener( 'drop', function( e )
                {
                    droppedFiles = e.dataTransfer.files; // the files that were dropped
                    showFiles( droppedFiles );
                    $("input[type='file']").prop("files", droppedFiles);
                    if (!(regex.exec(droppedFiles[0].name))) {
                        $(".box_error").append('<label class= "error validate-file">{{__("client_validation.form.format.file")}}</label>');
                    }
                    else{
                        $('#btn-upload').prop("disabled",false);
                        $('.validate-file').remove();
                    }
                });
            }

            // restart the form if has a state of error/success
            Array.prototype.forEach.call( restart, function( entry )
            {
                entry.addEventListener( 'click', function( e )
                {
                    e.preventDefault();
                    form.classList.remove( 'is-error', 'is-success' );
                    input.click();
                });
            });

            // Firefox focus bug fix for file input
            input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
            input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });

        });
    }( document, window, 0 ));

</script>
@endpush

