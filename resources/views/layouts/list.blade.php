@extends('layouts.master')


@section('content')
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Top content -->
        @yield('content-top')
        <!-- /.top content -->
            <div class="row">
                <div class="col-12">

                    <div class="card card-defalut">
                        <div class="card-header">
                            <h6>
                                @yield("sub-header-content")
                            </h6>
                        </div>
                        <div class="card-body pt-2">
                            <div class="d-flex flex-wrap justify-content-between mb-2 position-relative">
                                <form class="form-inline w-100" name="quick-search-form" id="quick-search-form">
                                    <div class="d-flex justify-content-between w-100">
                                        <div class="d-flex">
                                            <div class="form-group has-search ">
                                                @hasSection('time-filter')
                                                    @yield('time-filter')
                                                @endif
                                                <input type="search" class="form-control form-control-sm rounded-0" maxlength="100" style="height: 28px; width: 220px; padding-left: 10px;" placeholder="{{__('common.place_holder.search')}}" name="s"
                                                       value="{{ $allParams && array_key_exists("s", $allParams)  ? $allParams['s'] : "" }}">
                                                <button class="btn btn-secondary rounded-0 mr-2" style="height: 28px; width: 28px; padding: 1px" data-toggle="tooltip" data-placement="top" title="{{ __('common.button.search') }}"><i class="fa fa-search"></i></button>
                                            </div>
                                            @yield("inline-filter")
                                        </div>

                                    </div>
                                    @hasSection('quick-filter')
                                        <div class="w-100 mt-2 d-flex align-items-center">
                                            @yield('quick-filter')
                                        </div>
                                    @endif
                                </form>

                                <div class="d-flex position-absolute" style="right: 0">
                                    @yield("buttons")
                                    @if(!isset($allParams['requestReturn']))
                                    @can('deleteAny', app($modelClass))
                                        @if(isset($allParams) && array_key_exists("is_active", $allParams) && $allParams["is_active"] == -1)
                                            @include("partials.text-btn.bulk-restore")
                                        @elseif(@$isPivotData)
                                        @else
                                            @include("partials.text-btn.bulk-delete",
                                            [
                                                'disabled' => true,
                                                'total' => $data->total(),
                                                'perPage' => $data->perPage(),
                                                'currentPage' => $data->currentPage(),
                                                'lastPage' => $data->lastPage()
                                            ])
                                        @endif
                                    @endcan
                                    @endif
                                </div>
                            </div>

                                @yield('table-list')
                                @if(count($data) > 0)
                                @if($data instanceof \Illuminate\Pagination\LengthAwarePaginator)
                                    <div style="padding-top: 10px">
                                        <div class="float-left">
                                            <div role="status" class="show-res" aria-live="polite">
                                                {{ __('common.message.showing_entries', [
                                                       'from' => $data->total() > 0 ? ($data->currentPage() - 1) *  $data->perPage() + 1 : 0,
                                                       'to' => $data->currentPage() == $data->lastPage() ? $data->total() : $data->currentPage() *  $data->perPage() ,
                                                        'all' =>$data->total()
                                               ])}}</div>
                                        </div>
                                        <div class="float-right">
                                            {{ $allParams ? $data->appends($allParams)->links() : $data->links() }}
                                        </div>
                                    </div>
                                @endif
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascript')
    @include("partials.message")
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            localStorage.removeItem('prevUrl');
            $('.action').on('click', function (e) {
                localStorage.setItem('prevUrl',window.location.href);
            });
            $('.select_all').on('click', function (e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
            });
            $('.select2').select2({
                placeholder: "Select a state",
                allowClear: true
            });
            //Xử lý check box trên list
            $('input[name="row_id"]').on('change', function () {
                var ids = [];
                var deleteIds = [];
                var restoreIds = [];
                var unCheckedCount = 0;
                var objRecord = null;
                $('input[name="row_id"]').each(function () {
                    if ($(this).is(':checked')) {
                        objRecord = $(this).val().split(',').map(Number);
                        if (objRecord[1] == brokenStatus && routeArr.includes(currRoute)) {
                            restoreIds.push(objRecord[0]);
                            deleteIds.push(objRecord[0]);
                        }
                        else{
                            deleteIds.push(objRecord[0]);
                            ids.push(objRecord[0]);
                        }
                    } else {
                        unCheckedCount++;
                    }
                });
                $('.select_all').prop('checked', unCheckedCount == 0);
                $('#bulk_delete_btn').prop("disabled", deleteIds.length == 0);
                $('#bulk_restore_btn').prop("disabled", restoreIds.length == 0);
                $('#report_broken_btn').prop("disabled", ids.length == 0);
                $('.selected_ids').val(ids);
            });
        });

        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = ''.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        var routeArr = ['cars','devices'];
        var currRoute = '{{$routePrefix}}';
        var brokenStatus = '{{config("common.status.car.broken")}}'



        //xử lý dropdown status gọi submit form
        $('#status-dropdown-filter').on('change', function() {
            document.forms['quick-search-form'].submit();
        });

        function removeElementInSelectOption(el) {
            $(`#${el}`).children().remove();
            $(`#${el}`).append(
                // `<option value=""></option>`
            )
        }
    </script>
    @stack('list-record-plan')
    @stack('detail-record-plan')
@endsection

