@extends('layouts.master')

@section('javascript')
    @include("partials.message")
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            localStorage.removeItem('prevUrl');
            $('.action').on('click', function (e) {
                localStorage.setItem('prevUrl',window.location.href);
            });
            $('.select_all').on('click', function (e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
            });
            $('.select2').select2({
                placeholder: "Select a state",
                allowClear: true
            });
            //Xử lý check box trên list
            $('input[name="row_id"]').on('change', function () {
                var ids = [];
                var deleteIds = [];
                var restoreIds = [];
                var unCheckedCount = 0;
                var objRecord = null;
                $('input[name="row_id"]').each(function () {
                    if ($(this).is(':checked')) {
                        objRecord = $(this).val().split(',').map(Number);
                        if (objRecord[1] == brokenStatus && routeArr.includes(currRoute)) {
                            restoreIds.push(objRecord[0]);
                            deleteIds.push(objRecord[0]);
                        }
                        else{
                            deleteIds.push(objRecord[0]);
                            ids.push(objRecord[0]);
                        }
                    } else {
                        unCheckedCount++;
                    }
                });
                $('.select_all').prop('checked', unCheckedCount == 0);
                $('#bulk_delete_btn').prop("disabled", deleteIds.length == 0);
                $('#bulk_restore_btn').prop("disabled", restoreIds.length == 0);
                $('#report_broken_btn').prop("disabled", ids.length == 0);
                $('.selected_ids').val(ids);
            });
        });

        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = ''.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        var routeArr = ['cars','devices'];
        var currRoute = '{{$routePrefix}}';
        var brokenStatus = '{{config("common.status.car.broken")}}'



        //xử lý dropdown status gọi submit form
        $('#status-dropdown-filter').on('change', function() {
            document.forms['quick-search-form'].submit();
        });

        function removeElementInSelectOption(el) {
            $(`#${el}`).children().remove();
            $(`#${el}`).append(
                // `<option value=""></option>`
            )
        }
    </script>
    @stack('list-record-plan')
    @stack('detail-record-plan')
@endsection

