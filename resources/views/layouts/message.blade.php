<!-- Hiển thị thông báo thành công  -->
@if ( Session::has('success'))
    <div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom: 10px;">
        <strong>{{ Session::get('success') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
    </div>
    <?php session()->forget('success');?>
@endif
<!-- Hiển thị thông báo lỗi  -->
@if ( Session::has('error'))
    <div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom: 10px;">
        <strong>{{ Session::get('error') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
    </div>
    <?php session()->forget('error');?>
@endif
@if ( Session::has('create_error'))
    <div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom: 10px;">
        <strong>{{ Session::get('create_error') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
    </div>
    <?php session()->forget('error');?>
@endif
<!-- Hiển thị cảnh báo -->
@if ( Session::has('warning'))
    <div class="alert alert-warning alert-dismissible" role="alert" style="margin-bottom: 10px;">
        <strong>{{ Session::get('warning') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
    </div>
    <?php session()->forget('warning');?>
@endif
