<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route("home")}}" class="brand-link">
        <img src="{{ asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image">
        <span class="brand-text">QLSX</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header mt-3">QUẢN LÝ SẢN XUẤT</li>
                {{--                <x-menu-tree-subitem href="{{ url('/calendars') }}"--}}
                {{--                                     label="Calendar" icon-class="nav-icon fa fa-calendar" :active-route="['calendars']" ></x-menu-tree-subitem>--}}
                @can("access_record_plans")
                    <x-menu-tree-subitem href="{{ url('/record-plans') }}"
                                         label="Lịch sản xuất" icon-class="nav-icon fa fa-calendar-check-o"
                                         :active-route="['record-plans']"></x-menu-tree-subitem>
                @endcan

                @can("access_reporter")
                    <x-menu-tree-item label="Quản lý phóng viên" icon-class="nav-icon fa fa-microphone"
                                      :active-route="['reporter','reporter/assignments/production', 'assignment/reporter/proposals']"
                                      parameter-name="department_code" parameter-value="reporter">
                        @can("access_reporter_list")
                            <x-menu-tree-subitem
                                href="{{ route('reporter.index') }}"
                                label="Danh sách phóng viên" :active-route="['reporter']"
                                parameter-name="department_code"
                                parameter-value="reporter"></x-menu-tree-subitem>
                        @endcan

                        @can("access_reporter_assignment")
                            <x-menu-tree-subitem
                                href="{{ route('reporter.list-assignments', ['department_code' => config('common.departments.production')]) }}"
                                label="Danh sách công việc" :active-route="['reporter/assignments/production']"></x-menu-tree-subitem>
                        @endcan

                        @can("access_reporter_proposal")
                            <x-menu-tree-subitem href="{{ route('assignment.reporter.proposals') }}"
                                                 label="Danh sách đề xuất" :active-route="['assignment/reporter/proposals']"></x-menu-tree-subitem>
                        @endcan

                    </x-menu-tree-item>
                @endcan

                @can("access_camera")
                    <x-menu-tree-item label="Quản lý quay phim" icon-class="nav-icon fa fa-video-camera"
                                      :active-route="['camera','camera/assignments', 'assignment/camera/proposals']">
                        @can("access_camera_list")
                            <x-menu-tree-subitem
                                href="{{ route('camera.index') }}"
                                label="Danh sách quay phim" :active-route="['camera']" parameter-name="department_code"
                                parameter-value="cam"></x-menu-tree-subitem>
                        @endcan

                        @can("access_camera_assignment")
                            <x-menu-tree-subitem
                                href="{{ route('camera.list-assignments') }}"
                                label="Danh sách công việc" :active-route="['camera/assignments']"></x-menu-tree-subitem>
                        @endcan

                        @can("access_camera_proposal")
                            <x-menu-tree-subitem href="{{ route('assignment.camera.proposals') }}"
                                                 label="Danh sách đề xuất"
                                                 :active-route="['assignment/camera/proposals']"></x-menu-tree-subitem>
                        @endcan
{{--                            @can("access_camera_list")--}}
{{--                                <x-menu-tree-subitem--}}
{{--                                    href="{{ route('stages.index') }}"--}}
{{--                                    label="Danh sách trường quay" :active-route="['stages']" ></x-menu-tree-subitem>--}}
{{--                            @endcan--}}

                    </x-menu-tree-item>
                @endcan

                @can("access_technical_camera")
                    <x-menu-tree-item label="Kĩ thuật phòng quay" icon-class="nav-icon fa fa-video-camera"
                                      :active-route="['technical_camera','technical_camera/assignments', 'assignment/technical_camera/proposals', 'stages']">
                        @can("access_technical_camera_list")
                            <x-menu-tree-subitem
                                href="{{ route('technical_camera.index') }}"
                                label="Danh sách nhân viên" :active-route="['technical_camera']" parameter-name="department_code"
                                parameter-value="cam"></x-menu-tree-subitem>
                        @endcan

                        @can("access_technical_camera_assignment")
                            <x-menu-tree-subitem
                                href="{{ route('technical_camera.list-assignments') }}"
                                label="Danh sách công việc" :active-route="['technical_camera/assignments']"></x-menu-tree-subitem>
                        @endcan

                        @can("access_technical_camera_proposal")
                            <x-menu-tree-subitem href="{{ route('assignment.technical_camera.proposals') }}"
                                                 label="Danh sách đề xuất"
                                                 :active-route="['assignment/technical_camera/proposals']"></x-menu-tree-subitem>
                        @endcan
                        @can("access_studios")
                            <x-menu-tree-subitem
                                href="{{ route('stages.index') }}"
                                label="Danh sách trường quay" :active-route="['stages']" ></x-menu-tree-subitem>
                        @endcan

                    </x-menu-tree-item>
                @endcan


                @can("access_transportation")
                    <x-menu-tree-item label="Quản lý lái xe" icon-class="nav-icon fa fa-cab"
                                      :active-route="['transportation','cars','transportation/assignments','assignment/transportation/proposals']">
                        @can("access_transportation_list")
                            <x-menu-tree-subitem
                                href="{{ route('transportation.index') }}"
                                label="Danh sách lái xe" :active-route="['transportation']"></x-menu-tree-subitem>
                        @endcan

                        @can("access_cars")
                            <x-menu-tree-subitem href="{{ route('cars.index') }}"
                                                 label="Danh sách xe" :active-route="['cars']"></x-menu-tree-subitem>
                        @endcan

                        @can("access_transportation_assignment")
                            <x-menu-tree-subitem
                                href="{{ route('transportation.list-assignments') }}"
                                label="Danh sách công việc" :active-route="['transportation/assignments']"></x-menu-tree-subitem>
                        @endcan

                        @can("access_transportation_proposal")
                            <x-menu-tree-subitem href="{{ route('assignment.transportation.proposals') }}"
                                                 label="Danh sách đề xuất"
                                                 :active-route="['assignment/transportation/proposals']"></x-menu-tree-subitem>
                        @endcan
                    </x-menu-tree-item>
                @endcan

                @if((auth()->user()->hasPermissionTo("access_devices") && auth()->user()->hasAnyPermission(["access_device_returning", "access_device_list", "access_device_categories", "access_device_proposal",
                                                                                            "access_device_lending"])))
                    <x-menu-tree-item label="Quản lý thiết bị" icon-class="nav-icon fas fa-warehouse"
                                      :active-route="['devices','device-categories', 'assignment/device/proposals','assignment/device/lending','assignment/device/returning', 'devices/lending']">
                        @can("access_device_list")
                            <x-menu-tree-subitem href="{{ route('devices.index') }}"
                                                 label="Danh sách thiết bị"
                                                 :active-route="['devices']"></x-menu-tree-subitem>
                        @endcan

                        @can("access_device_categories")
                            <x-menu-tree-subitem href="{{ route('device-categories.index') }}"
                                                 class="nav-link {{ active_class(['device-categories']) }}"
                                                 label="Danh mục thiết bị"
                                                 :active-route="['device-categories']"></x-menu-tree-subitem>
                        @endcan

                        @can("access_device_proposal")
                            <x-menu-tree-subitem href="{{ route('assignment.device.proposals') }}"
                                                 label="Danh sách đề xuất"
                                                 :active-route="['assignment/device/proposals']"></x-menu-tree-subitem>
                        @endcan

{{--                        @can("access_device_proposal")--}}
{{--                            <x-menu-tree-subitem href="{{ route('devices.lending') }}"--}}
{{--                                                 label="Danh sách thiết bị đang cho mượn"--}}
{{--                                                :active-route="['devices/lending']"></x-menu-tree-subitem>--}}
{{--                        @endcan--}}
{{--                        @can("access_device_lending")--}}
{{--                            <x-menu-tree-subitem href="{{ route('assignment.device.lending') }}"--}}
{{--                                                 label="Cấp thiết bị"--}}
{{--                                                 :active-route="['assignment/device/lending']"></x-menu-tree-subitem>--}}
{{--                        @endcan--}}
{{--                        @can("access_device_returning")--}}
{{--                            <x-menu-tree-subitem href="{{ route('assignment.device.returning') }}"--}}
{{--                                                 label="Nhận lại thiết bị"--}}
{{--                                                 :active-route="['assignment/device/returning']"></x-menu-tree-subitem>--}}
{{--                        @endcan--}}
                    </x-menu-tree-item>
                @endif

                @can("access_outsources")
                    <x-menu-tree-item label="QL nguồn lực ngoài" icon-class="nav-icon fas fa-users"
                                      :active-route="['outsources','outsource-types']">
                        @can("access_outsource_types")
                            <x-menu-tree-subitem href="{{ route('outsource-types.index') }}"
                                                 label="Loại nguồn lực thuê ngoài"
                                                 :active-route="['outsource-types']"></x-menu-tree-subitem>
                        @endcan

                        @can("access_outsource_list")
                            <x-menu-tree-subitem href="{{ route('outsources.index') }}"
                                                 label="Danh sách nguồn lực"
                                                 :active-route="['outsources']"></x-menu-tree-subitem>
                        @endcan
                    </x-menu-tree-item>
                @endcan
                @if(auth()->user()->hasAnyPermission(["access_plans", "access_statements", "access_contracts"]))
                    <li class="nav-header mt-2">QUẢN LÝ TÀI CHÍNH</li>
                    @can("access_plans")
                        <x-menu-tree-subitem href="{{ url('/plans') }}"
                                             label="Kế hoạch" icon-class="nav-icon fas fa-clipboard-list"
                                             :active-route="['plans']"></x-menu-tree-subitem>
                    @endcan

                    @can("access_statements")
                        <x-menu-tree-subitem href="{{ url('/statements') }}"
                                             label="Tờ trình" icon-class="nav-icon fas fa-file-contract"
                                             :active-route="['statements']"></x-menu-tree-subitem>
                    @endcan

                    @can("access_contracts")
                        <x-menu-tree-subitem href="{{ url('/contracts') }}"
                                             label="Hợp đồng" icon-class="nav-icon fas fa-file-signature"
                                             :active-route="['contracts']"></x-menu-tree-subitem>
                    @endcan
                @endif
                @if((auth()->user()->hasPermissionTo("create_device_assignment")) || auth()->user()->hasAnyPermission(["access_managers", "access_departments"]))
                    <li class="nav-header mt-2">QUẢN TRỊ HỆ THỐNG</li>
                    @can("access_managers")
                        <x-menu-tree-subitem href="{{ url('/managers') }}"
                                             label="Danh sách quản lý" icon-class="nav-icon fas  fa-user-check"
                                             :active-route="['managers']"></x-menu-tree-subitem>
                    @endcan

                    {{--                    <x-menu-tree-subitem href="{{ url('/groups') }}"--}}
                    {{--                                         label="Nhóm thành viên" icon-class="nav-icon fas  fa-user-friends"--}}
                    {{--                                         :active-route="['groups']"></x-menu-tree-subitem>--}}
                    @can("access_departments")
                        <x-menu-tree-subitem href="{{ url('/departments') }}"
                                             label="Phòng ban" icon-class="nav-icon fas  fa-user-friends"
                                             :active-route="['departments']"></x-menu-tree-subitem>
                    @endcan
                    @can("create_device_assignment")
                        <x-menu-tree-subitem href="{{ route('example-notes.index') }}"
                                             label="Đề xuất thiết bị" icon-class="nav-icon far fa-comment-alt"
                                           :active-route="['assignment/device/proposals']"></x-menu-tree-subitem>
                    @endcan
                @endif
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
