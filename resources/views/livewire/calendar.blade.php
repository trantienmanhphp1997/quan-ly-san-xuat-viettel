<div>
    <div id='calendar-container' wire:ignore>
        <div id='calendar'></div>
    </div>
</div>

@push('scripts')
    <script src="{{ asset('assets/plugins/fullcalendar/main.min.js') }}"></script>
    <script>
        document.addEventListener('livewire:load', function() {
            var Calendar = FullCalendar.Calendar;
            var calendarEl = document.getElementById('calendar');

            // initialize the calendar
            // -----------------------------------------------------------------

            var calendar = new Calendar(calendarEl, {
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay'
                },
                editable: false,
                droppable: false, // this allows things to be dropped onto the calendar
                loading: function(isLoading) {
                    if (!isLoading) {
                        // Reset custom events
                        this.getEvents().forEach(function(e){
                            if (e.source === null) {
                                e.remove();
                            }
                        });
                    }
                }
            });

            calendar.addEventSource( {
                url: '/api/calendars/record-plans',
                extraParams: function() {
                    return {
                        name: @this.name
                    };
                }
            });

            calendar.render();

        @this.on(`refreshCalendar`, () => {
            calendar.refetchEvents()
        });
        });

    </script>

    <style>
        .fc-daygrid-day {
            background-color: white;
        }

        #calendar-container {
            align-items: center;
            margin: auto;
        }

        #calendar {
            width: 90%;
            margin: auto;
        }
    </style>

    <link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.3.1/main.min.css' rel='stylesheet' />
@endpush
