<div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Top content -->
        <div class="row">
            <div class="col-md-3">
                <div class="card mini-stats-wid">
                    <div class="card-body pt-2 pb-2" style="cursor: pointer;" wire:click='setSearchStatus(1)'>
                        <div class="media">
                            <div class="media-body">
                                <p class="text-muted font-weight-medium">Chờ xác nhận</p>
                                <h4 class="mb-0 text-primary">{{$num_of_pendings}}</h4>
                            </div>
                            <div class="mini-stat-icon avatar-sm rounded-circle bg-primary align-self-center">
                        <span class="avatar-title">
                          <i class="fas fa-check"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card mini-stats-wid">
                    <div class="card-body pt-2 pb-2" style="cursor: pointer;" wire:click='setSearchStatus(2)'>
                        <div class="media">
                            <div class="media-body">
                                <p class="text-muted font-weight-medium">Đã chốt</p>
                                <h4 class="mb-0 text-success">{{$num_of_approved}}</h4>
                            </div>
                            <div class="mini-stat-icon avatar-sm rounded-circle bg-success align-self-center">
                        <span class="avatar-title">
                          <i class="fas fa-hourglass-end"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card mini-stats-wid">
                    <div class="card-body pt-2 pb-2" style="cursor: pointer;" wire:click='setSearchStatus(3)'>
                        <div class="media">
                            <div class="media-body">
                                <p class="text-muted font-weight-medium">Đang diễn ra</p>
                                <h4 class="mb-0 text-secondary">{{$num_of_in_progress}}</h4>
                            </div>
                            <div class="mini-stat-icon avatar-sm rounded-circle bg-secondary align-self-center">
                        <span class="avatar-title">
                          <i class="fas fa-industry"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card mini-stats-wid">
                    <div class="card-body pt-2 pb-2" style="cursor: pointer;" wire:click='setSearchStatus(4)'>
                        <div class="media">
                            <div class="media-body">
                                <p class="text-muted font-weight-medium">Đã đóng máy</p>
                                <h4 class="mb-0 text-warning">{{$num_of_wrap}}</h4>
                            </div>
                            <div class="mini-stat-icon avatar-sm rounded-circle bg-warning align-self-center">
                                <span class="avatar-title">
                                  <i class="fa fa-flag-checkered" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.top content -->
            <div class="row">
                <div class="col-12">

                    <div class="card card-defalut">
                        <div class="card-header">
                            <h6>
                                {{__('data_field_name.action.manage', ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                            </h6>
                        </div>
                        <div class="card-body pt-2">
                            <div class="d-flex flex-wrap justify-content-between mb-2 position-relative">
                                <form class="form-inline w-100" name="quick-search-form" id="quick-search-form">
                                    <div class="d-flex justify-content-between w-100">
                                        <div class="d-flex">
                                            <div class="form-group has-search ">
                                                <div class="input-group time-filter from_date mr-2" wire:ignore>
                                                    <input class="form-control form-control-sm rounded-0" type="text" id="from_date" data-input placeholder="Từ ngày" name="from_date" wire:model='fromDate'>
                                                    <div class="input-group-append" data-toggle>
                                                        <button type="button" class="btn btn-primary rounded-0" style="height: 28px; width: 28px; padding: 1px"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                                                    </div>
                                                </div>

                                                <div class="input-group time-filter to_date mr-2" wire:ignore>
                                                    <input class="form-control form-control-sm rounded-0" type="text" id="to_date" data-input placeholder="Đến ngày" name="to_date" wire:model='toDate'>
                                                    <div class="input-group-append" data-toggle>
                                                        <button type="button" class="btn btn-primary rounded-0" style="height: 28px; width: 28px; padding: 1px"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control form-control-sm rounded-0" maxlength="100" style="height: 28px; width: 220px; padding-left: 10px;" placeholder="{{__('common.place_holder.search')}}"wire:model.debounce.1000ms="searchTerm">
                                                <button class="btn btn-secondary rounded-0 mr-2" style="height: 28px; width: 28px; padding: 1px" data-toggle="tooltip" data-placement="top" title="{{ __('common.button.search') }}"><i class="fa fa-search"></i></button>
                                            </div>
                                            <select class="form-control form-control-sm rounded-0 custom-select-filter" data-dropdown-css-class="select2-blue" wire:model='searchStatus'>
                                                <option value=''>Chọn trạng thái</option>
                                                <option value="1">Chờ xác nhận</option>
                                                <option value="2">Đã chốt</option>
                                                <option value="3">Đang diễn ra</option>
                                                <option value="4">Đã đóng máy</option>
                                                <option value="5">Đã hoàn thành</option>
                                                <option value="-1">Đã hủy</option>
                                            </select>

                                            <select wire:model='searchRecurring' name="recurring" class="form-control form-control-sm rounded-0 custom-select-filter" data-dropdown-css-class="select2-blue" style="margin-left: 10px">
                                                <option value="0">Tất cả</option>
                                                <option value="1">Lịch lặp lại</option>
                                            </select>
                                        </div>

                                    </div>
                                </form>

                                <div class="d-flex position-absolute" style="right: 0">
                                    <div class="dropdown">
                                        <button class="btn btn-primary btn-sm rounded-0 dropdown-toggle mr-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-plus mr-2"></i>Tạo mới
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" style="left:-60px !important;">
                                            <a class="btn btn-sm" data-toggle="modal" data-target="#create_modal">Tạo mới</a>
                                            <a class="btn btn-sm action" href="{{isset($route) ? $route : route($routePrefix. '.import', isset($importParams) ? $importParams : [])}}">Tải lên từ Excel</a> 
                                        </div>
                                    </div>
                                    @include("partials.text-btn.export-excel-btn")
                                    @if(!isset($allParams['requestReturn']))
                                        @if(isset($allParams) && array_key_exists("is_active", $allParams) && $allParams["is_active"] == -1)
                                            @include("partials.text-btn.bulk-restore")
                                        @elseif(@$isPivotData)
                                        @else
                                            @include("partials.text-btn.bulk-delete",
                                            [
                                                'disabled' => true,
                                                'total' => $data->total(),
                                                'perPage' => $data->perPage(),
                                                'currentPage' => $data->currentPage(),
                                                'lastPage' => $data->lastPage()
                                            ])
                                        @endif
                                    @endif
                                </div>
                            </div>
                           <div class="table-responsive height_auto">
                                <table class="table table-hover table-striped table-head-fixed" id="dataTable">
                                    <thead>
                                    <tr class="bg-light">
                                        <th>
                                            <input type="checkbox" class="select_all">
                                        </th>
                                        <th>{{__('data_field_name.common_field.stt')}}</th>
                                        <th style="width: 17%">{{__('data_field_name.common_field.name')}}</th>
                                        <th>{{__('data_field_name.common_field.start_date')}}</th>
                                        <th>{{__('data_field_name.common_field.end_date')}}</th>
                                        <th>{{__('data_field_name.common_field.creator')}}</th>
                                        <th>{{__('data_field_name.common_field.status')}}</th>
                                        <th class="no-wrap text-left">{{__('data_field_name.record_plan.change_status')}}</th>
                                        <th class="text-left">{{__('data_field_name.common_field.action')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $datum)
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="row_id" id="checkbox_{{ $datum->id }}"
                                                       value="{{ $datum->id }},{{$datum->status}}">
                                            </td>
                                            <td>{{ $loop->index + ($data->currentPage() - 1) * $data->perPage() +1 }}</td>
                                            <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top"
                                                title="{{ $datum->name }}">{{ $datum->name }}</td>
                                            <td>{{ date("d-m-Y H:i" , strtotime($datum->start_time)) }}</td>
                                            <td>{{ date("d-m-Y H:i" , strtotime($datum->end_time)) }}</td>
                                            <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{$datum->creator ? $datum->creator->full_name.' - '.$datum->creator->phone_number : ''}}">{{ $datum->creator ? ($datum->creator->full_name ? $datum->creator->full_name : $datum->creator->phone_number) : "" }}</td>
                                            <td>
                                                <x-status-span status="{{$datum->status}}" data-type="record_plan" :danger="[-1]"
                                                               :secondary="[0]" :warning="[1,3]" :success="[2,4]"
                                                               :primary="[5]"></x-status-span>
                                            </td>
                                            <td>
                                                @if($datum->status != config("common.status.record_plan.cancel"))
                                                    <div class="h-100 d-flex no-wrap justify-content-left">
                                                        <!---Cancel button--->
                                                    @include("partials.text-btn.submit-btn", [
                                                        "status" => -1, "dataType" => "record_plan",
                                                        "danger" => [-1], "secondary" => [0], "warning" => [1, 3], "success" => [2, 4], "primary" => [5],
                                                        "route" => "$routePrefix.update-status", "method" => "post", "params" => ["id" => $datum->id],
                                                        "name" => "status", "value" => -1, "btnLabel" => __("common.button.cancel")
                                                    ])

                                                    <!---Update status button--->

                                                        @if($datum->status >= config("common.status.record_plan.approved")  && $datum->status < config("common.status.record_plan.done"))
                                                            @php
                                                                $current_status = $datum->status;
                                                                if($current_status == config("common.status.record_plan.approved")){
                                                                    $nextStatus = config("common.status.record_plan.in_progress");
                                                                }elseif($current_status == config("common.status.record_plan.in_progress")){
                                                                    $nextStatus = config("common.status.record_plan.wrap");
                                                                }elseif ($current_status == config("common.status.record_plan.wrap")){
                                                                    $nextStatus =  config("common.status.record_plan.done");
                                                                }else{
                                                                    $nextStatus = $current_status;
                                                                }
                                                            @endphp
                                                            @include("partials.text-btn.submit-btn", [
                                                                "status" => $nextStatus, "dataType" => "record_plan_change_status",
                                                                "danger" => [-1], "secondary" => [0], "warning" => [1, 3], "success" => [2, 4], "primary" => [5],
                                                                "route" => "$routePrefix.update-status", "method" => "post", "params" => ["id" => $datum->id],
                                                                "name" => "status", "value" => $nextStatus
                                                            ])
                                                        @endif

                                                    </div>
                                                @else
                                                <!---Restore button--->
                                                    @include("partials.text-btn.submit-btn", [
                                                                "status" => 5, "dataType" => "record_plan",
                                                                "danger" => [-1], "secondary" => [0], "warning" => [1, 3], "success" => [2, 4], "primary" => [5],
                                                                "route" => "$routePrefix.update-status", "method" => "post", "params" => ["id" => $datum->id],
                                                                "name" => "status", "value" => 1, "btnLabel" => __("common.button.restore")
                                                            ])

                                                @endif
                                            </td>
                                            <td>
                                                <div class="h-100 d-flex no-wrap justify-content-left m-1">
                                                    <button class="btn btn-primary btn-sm mx-3" data-toggle="modal"
                                                            data-target="#source-detail-modal-{{$datum->id}}">{{__('common.button.source')}}
                                                    </button>
                                                @include("pages.record-plans.source-detail-modal", ["data" => isset($datum) ? $datum: null])
                                                <!---Common actions--->
                                                    @php
                                                        $titleBtn = __('common.button.view');
                                                        $icon = "fa-eye";

                                                            $condition = ((auth()->user()->is_manager == 1 && str_contains(auth()->user()->department_code, "production") && auth()->user()->id == $datum->created_by) || auth()->user()->hasRole('super admin'))

                                                                          && ($datum->is_recurring == 1 || \App\Services\RecordPlanService::checkTimeToUpdateRecordPlan($datum->end_time));
                                                            if($condition){
                                                                $titleBtn = __('common.button.edit');
                                                                $icon = "fa-edit";
                                                            }
                                                    @endphp

                                                    <a href='{{route($routePrefix. '.show',['id' => $datum->id])}}' type="button"
                                                       class="btn btn-default btn-sm action rounded-1 mr-1" data-toggle="tooltip"
                                                       data-placement="top"
                                                       title="{{ $titleBtn }}"><i class="fas {{$icon}}" title="{{ $titleBtn }}"></i></a>

                                                    @if(!$datum->deleted_at)
                                                        @include("partials.small-btn.delete-btn")
                                                    @endif

                                                    @if($datum->deleted_at)
                                                        <form action="{{route($routePrefix. '.restore',['id' => $datum->id])}}"
                                                              method="post"
                                                              enctype="multipart/form-data" class="form-horizontal">
                                                            @csrf
                                                            <div>
                                                                <button class="btn btn-sm btn-warning" type="submit"
                                                                        title="{{ __('common.button.restore') }}"
                                                                        onclick="return confirm('Are you sure?')"><i
                                                                        title="{{ __('common.button.restore') }}"
                                                                        class="far fa-check"></i> {{__('common.button.restore')}}</button>
                                                            </div>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @if(count($data) == 0)
                                    <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
                                @endif
                            </div>
                            @if(count($data) > 0)
                                @if($data instanceof \Illuminate\Pagination\LengthAwarePaginator)
                                    <div style="padding-top: 10px">
                                        <div class="float-left">
                                            <div role="status" class="show-res" aria-live="polite">
                                                {{ __('common.message.showing_entries', [
                                                       'from' => $data->total() > 0 ? ($data->currentPage() - 1) *  $data->perPage() + 1 : 0,
                                                       'to' => $data->currentPage() == $data->lastPage() ? $data->total() : $data->currentPage() *  $data->perPage() ,
                                                        'all' =>$data->total()
                                               ])}}</div>
                                        </div>
                                        <div class="float-right">
                                            {{ $data->links() }}
                                        </div>
                                    </div>
                                @endif
                            @endif
{{--                             <div wire:ignore>
                                @include('partials.modal.create-record-plan')
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


@push('validation-scripts')
    <script>
        // validate form
        $("#detail-form").validate({
            rules: {
                @stack('validation-rules')
            },
            messages: {
                @stack('validation-messages')
            },
        });

        // khi đã validate xong trên form -> nhấn btn submit, hiển thị spinner btn
        $(".btn-action").click(function () {
            if ($('#detail-form').valid()) {
                // Kiểm tra trạng thái btn submit
                // - enabled: sunmit form , disabled btn,
                if (!$(".btn-action").prop('disabled')) {
                    //submit form
                    $('#detail-form').submit();
                    //disabled btn submit
                    $(".btn-action").prop("disabled", true);
                    //add spinner loading...
                    $(".btn-action").html(
                        '<i class="fa fa-circle-o-notch fa-spin"></i> loading...'
                    );
                }
                return true;
            } else {
                return false;
            }
        });
    </script>
@endpush

@push('list-record-plan')
    <script src="{{ asset('js/get-plans-by-departemet.js') }}"></script>
    <script src="{{ asset('js/get-members-by-departemet.js') }}"></script>
    <script src="{{ asset('js/custom-flatpickr.js') }}"></script>
    <script>
    $(document).ready(function () {
        customDate('.from_date', null, null, 0, 0)
        customDate('.to_date', null, null, 23, 59)
    })
        $('#recurring-dropdown-filter').on('change', function () {
            document.forms['quick-search-form'].submit();
        });
    </script>
@endpush