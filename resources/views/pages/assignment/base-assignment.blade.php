@extends('layouts.master')
@section("css")
    <link href="{{ asset('css/calenderInput.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class=" col-12">
                    <div class="card">
                        <div class="card-header" style="font-weight: bold;">
                            <h3>
                                @yield("title")
                            </h3>
                        </div>
                        <div class="card-body pt-0 ">
                            <div class="w-100">
                                <div>
                                    <div>
                                        <div class="form-group mb-0">
                                            <strong class="">
                                                <i class="fa fa-clock-o text-primary" style="font-size: 16px "></i> {{date('H:i d/m/Y', strtotime($record_plan->start_time))}} - {{date('H:i d/m/Y ', strtotime($record_plan->end_time))}}
                                            </strong>
                                        </div>
                                        <div class="form-group mb-0">
                                            <strong class="">
                                                <i class="fa fa-compass text-primary" style="font-size: 16px"></i> {{$record_plan->address}}
                                            </strong>
                                        </div>
                                        @if($department_code == "reporter")
                                            <div class="form-group mb-0">
                                                <strong class="">
                                                    <i class="fa fa-building text-primary" style="font-size: 16px"></i>  {{__("department.$record_plan->department_code") }}
                                                </strong>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="d-flex">
                                        <div class="mr-1"><label class="mt-3">{{__('data_field_name.record_plan.proposaled_number.'.$department_code)}}:</label></div>
                                        <div class="text-red">
                                            <label class="mt-3">
                                                @if(isset($assigned_members) && count($assigned_members) >= 0)
                                                    {{$record_plan["offer_$department_code"."_number"] ? count($assigned_members) .'/'.$record_plan["offer_$department_code"."_number"] : 0}}
                                                @elseif(isset($assigned_devices) && count($assigned_devices) >= 0)
                                                    {{$record_plan["offer_$department_code"."_number"] ? count($assigned_devices) .'/'.$record_plan["offer_$department_code"."_number"] : 0}}
                                                @else
                                                    {{$record_plan["offer_$department_code"."_number"] ? count($delivered_devices) .'/'.$record_plan["offer_$department_code"."_number"] : 0}}
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="mr-1"><label class="mt-1">{{__('data_field_name.record_plan.proposal.'.$department_code)}}:</label></div>
                                        <div class="text-red">
                                            <label class="mt-1">{{$record_plan["offer_$department_code"."_note"] ? $record_plan["offer_$department_code"."_note"] : 0}} </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @yield("assigment-table")
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    @include("partials.message")
    @stack("assignment-js")
@endsection

