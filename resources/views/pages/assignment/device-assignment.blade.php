@if(!empty($record_plan["offer_$department_code"."_number"]))
<div class="w-100 d-flex pt-3">
    <div style="width: 49%;  padding-right: 2%;">
        <div>
            <div class="form-group">
                <div class="d-flex justify-content-between flex-wrap">
                    <label class="col-form-label pb-0">
                        <strong>{{__('data_field_name.assignment.available_device')}}</strong>
                    </label>
                    <div class="d-flex flex-wrap">
                        <form class="form-inline" name="quick-search-form">
                            <div class="form-group has-search ">
                                <input type="search" class="form-control form-control-sm rounded-0" style="height: 28px; width: 220px; padding-left: 10px;" placeholder="{{__('common.place_holder.search')}}" name="s"
                                    value="{{ $allParams && array_key_exists("s", $allParams)  ? $allParams['s'] : "" }}">
                                <button class="btn btn-secondary rounded-0 mr-2" style="height: 28px; width: 28px; padding: 1px" data-toggle="tooltip" data-placement="top" title="{{ __('common.button.search') }}"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                        <button id="assignMemberButton" class="btn btn-primary btn-sm" disabled>
                        @if(in_array(auth()->user()->department_code, [
                                config("common.departments.production"),
                                config("common.departments.general_news"),
                                config("common.departments.news"),
                                config("common.departments.documentary"),
                                config("common.departments.entertainment"),
                                config("common.departments.music"),
                                config("common.departments.film"),
                            ]) && auth()->user()->is_manager == 1 && !auth()->user()->hasRole('super admin'))
                            {{__('common.button.proposal')}}
                        @else
                            {{__('common.button.assign')}}
                        @endif
                        </button>
                    </div>
                </div>
            </div>
            <div>
                <form action="{{route("assignment.$department_code.store", ["record_plan_id" => $record_plan_id])}}" method="post" id="assignMember">
                    @csrf
                    <input type="hidden" name="department_code" id="department_code" value="{{$department_code}}">
                    <input type="hidden" name="devices" id="devices" value="{{count($devices)}}">
                    <input type="hidden" name="proposal_number" id="proposal_number" value="{{$record_plan["offer_$department_code"."_number"]}}">
                    <input type="hidden" name="device_ids" value="" id="member_ids">
                </form>

                <div class="table-responsive">
                    <table class="table table-hover table-striped table-head-fixed" id="assignMemberTable">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" class="select_all">
                            </th>
                            <th>{{__('data_field_name.common_field.stt')}}</th>
                            <th>{{__('data_field_name.common_field.name')}}</th>
                            <th>{{__('data_field_name.device.serial')}}</th>
                            <th>{{__('data_field_name.device.category')}}</th>
                            <th class="text-center">{{__('data_field_name.common_field.status')}}</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($available_devices as $index => $device)
                            <tr>
                                <td>
                                    <input type="checkbox" name="row_id" id="checkbox_{{ $device->id }}" value="{{ $device->id }}">
                                </td>
                                <td>{{$index + 1}}</td>
                                <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $device->name }}">{{ $device->name }}</td>
                                <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $device->serial }}">{{ $device->serial }}</td>
                                <td>{{ optional($device->category)->name }}</td>
                                <td>
                                    @if($duplicated_proposal_device_ids->contains($device->id))
                                        @php
                                            $status = config("common.status.propose_device.duplicated")
                                        @endphp
                                        <x-status-span status="{{$status}}" data-type="propose_device" :danger="[-1]" :secondary="[0]" :warning="[-2,4]" :success="[2]" :primary="[1,3]"></x-status-span>
                                    @else
                                        <x-status-span status="{{$device->status}}" data-type="device" :danger="[3]" :secondary="[0]" :warning="[]" :success="[1]" :primary="[]"></x-status-span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if(count($available_devices) == 0)
                        <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
                    @endif
                    @php
                        $query =  \Illuminate\Support\Arr::except(request()->query(), 'availPage');
                    @endphp
                    {{$available_devices->appends($query)->links()}}
                </div>
            </div>
            <div class="mt-3">
                <a href="{{ route(isset($redirect_back_route) &&  $redirect_back_route ? $redirect_back_route : 'assignment.device.proposals', isset($redirect_back_params) && $redirect_back_params ? $redirect_back_params : []) }}"
                   class="btn btn-default btn-sm" title="{{__('common.button.back')}}">
                    {{__('common.button.back')}}
                </a>
            </div>
        </div>
    </div>

    <div style="width: 49%; padding-left: 2%; border-left: solid 1px lightgrey">
        <div>
            <div class="form-group d-flex justify-content-between">
                <div style="width: 100%; display: flex; justify-content: space-between">
                    <label class="col-form-label pb-0">
                        <strong>{{__('data_field_name.assignment.assigned_device')}}</strong> </label>
                    <button id="removeAssignButton" class="btn btn-danger btn-sm" disabled>
                    @if(in_array(auth()->user()->department_code, [
                        config("common.departments.production"),
                        config("common.departments.general_news"),
                        config("common.departments.news"),
                        config("common.departments.documentary"),
                        config("common.departments.entertainment"),
                        config("common.departments.music"),
                        config("common.departments.film"),
                    ]) && auth()->user()->is_manager == 1 && !auth()->user()->hasRole('super admin'))
                        {{__('common.button.remove-proposal')}}
                    @else
                        {{__('common.button.remove-assign')}}
                    @endif
                    </button>
                </div>

            </div>
            <div>
                <form action="{{route("assignment.$department_code.destroy", ["record_plan_id" => $record_plan_id])}}" method="post" id="removeAssignMemberForm">
                    @method("delete")
                    @csrf
                    <input type="hidden" name="device_ids" value="" id="remove_member_ids">
                </form>
                <div class="table-responsive">
                    <table class="table table-hover table-striped table-head-fixed" id="removeAssignMemberTable">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" class="remove_member_select_all">
                            </th>
                            <th class="text-center">{{__('data_field_name.common_field.stt')}}</th>
                            <th>{{__('data_field_name.common_field.name')}}</th>
                            <th>{{__('data_field_name.device.serial')}}</th>
                            <th>{{__('data_field_name.device.category')}}</th>
                            <th>{{__('data_field_name.common_field.status')}}</th>
                            <th class="text-center">{{__('data_field_name.common_field.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($devices as $index => $device)
                            <tr>
                                <td><input type="checkbox" name="row_id_member" id="checkbox_{{ optional($device->device)->id }}" value="{{ optional($device->device)->id }}"></td>
                                <td>{{$index + 1}}</td>
                                <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ optional($device->device)->name }}">{{ optional($device->device)->name }}</td>
                                <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ optional($device->device)->serial }}">{{ optional($device->device)->serial }}</td>
                                <td>{{ optional(optional($device->device)->category)->name }}</td>
                                <td>
                                    <x-status-span status="{{$device->status}}" data-type="propose_device" :danger="[-1]" :secondary="[0]" :warning="[-2,4]" :success="[2]" :primary="[1,3]"></x-status-span>
                                </td>
                                <td class="d-flex text-center">
                                    @if(optional($device->device)->id && $proposal_device_ids->contains($device->device->id) )
                                        @can("delete_device_assignment")
                                            @if($device->status == config("common.status.propose_device.pending"))
                                                <div class="mr-1">
                                                    <form action="{{route("assignment.$department_code". '.accept',['record_plan_id' => $record_plan_id, "device_ids" => $device->device_id])}}" method="post"
                                                          enctype="multipart/form-data" class="d-flex">
                                                        @csrf
                                                        @method("delete")
                                                        <button class="btn btn-sm btn-info h-100" type="submit" title="Accept"
                                                                onclick="return confirm('Are you sure?')"><i class="fa fa-check text-white" title="Accept"></i></button>
                                                    </form>
                                                </div>
                                            @endif
                                        @endcan

                                        <div>
                                            <form action="{{route($routePrefix. '.deny',['record_plan_id' => $record_plan_id, "device_ids" => $device->device_id] )}}" method="post"
                                                  enctype="multipart/form-data" class="d-flex">
                                                @csrf
                                                @method("delete")
                                                <button class="btn btn-sm btn-danger h-100" type="submit"
                                                        title="{{ (auth()->user()->department_code == "admin" || (auth()->user()->department_code == "device" && auth()->user()->is_manager == 1)) ? "Deny" : "Delete proposal"}}"
                                                        onclick="return confirm('Are you sure?')">
                                                    <i class="fa fa-times text-white"
                                                       title="{{ (auth()->user()->department_code == "admin" || (auth()->user()->department_code == "device" && auth()->user()->is_manager == 1)) ? "Deny" : "Delete proposal"}}">
                                                    </i>
                                                </button>
                                            </form>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @php
                        $queryParams = \Illuminate\Support\Arr::except(request()->query(), 'assignedPage');
                    @endphp
                    {{$devices->appends($queryParams)->links()}}
                </div>
            </div>

        </div>

        {{--<form action="{{route("$routePrefix.add-note", ["record_plan_id" => $record_plan_id])}}" method="post">
            @csrf
            <input type="hidden" name="department_code" value="{{$department_code}}">
            <label class="col-form-label pb-1"><strong>Note of {{config("common.departments.$department_code")}} manager</strong> </label>
            <div class=" row d-flex">
                <div class="col-md-12">
                    <textarea class="form-control" autocomplete="off" name="note" rows="10" >{{isset($note) ? $note : ""}}</textarea>
                </div>
                <div class="col-md-12 mt-3">
                    <button class="btn btn-info btn-md " type="submit" title="{{__("common.button.submit")}}">{{__("common.button.submit")}}</button>
                </div>
            </div>
        </form>--}}
    </div>
</div>
@else
<div class="mt-3">
    <a href="{{ route(isset($redirect_back_route) && $redirect_back_route ? $redirect_back_route : "assignment.$department_code.proposals" , isset($redirect_back_params) && $redirect_back_params ? $redirect_back_params : []) }}"
        class="btn btn-default btn-sm " title="{{__('common.button.back')}}">
        {{__('common.button.back')}}
    </a>
</div>
@endif
