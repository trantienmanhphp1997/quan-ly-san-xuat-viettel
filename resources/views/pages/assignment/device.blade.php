@extends('pages.assignment.base-assignment')
@section("title")
    {{__('data_field_name.record_plan.proposal_device').": ".$record_plan->name }}
@endsection
@section("assigment-table")
    @include("pages.assignment.device-assignment")
@endsection

@push('assignment-js')

    <script>
            $(document).ready(function () {

                $('#device_list').select2({
                    tags: false,
                    tokenSeparators: [],
                    placeholder: "{{__('common.place_holder.select_device')}}",
                    multiple: true,
                    closeOnSelect: false
                });
                $("#member-assignment-form").validate({});
            })

        </script>
        <script>
            $(document).ready(function () {
                $(document).ready(function () {
                    $('.select_all').on('click', function (e) {
                        $('input[name="row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
                    });

                });

                //Xử lý check box trên list
                $('input[name="row_id"]').on('change', function () {
                    var ids = [];
                    var unCheckedCount = 0;
                    $('input[name="row_id"]').each(function () {
                        if ($(this).is(':checked')) {
                            ids.push($(this).val());
                        } else {
                            unCheckedCount++;
                        }
                    });
                    $('.select_all').prop('checked', unCheckedCount == 0);
                    $('#assignMemberButton').prop("disabled", ids.length == 0);
                    $('.selected_ids').val(ids);
                });

                $('.remove_member_select_all').on('click', function (e) {
                    $('input[name="row_id_member"]').prop('checked', $(this).prop('checked')).trigger('change');
                });

                //Xử lý check box trên list
                $('input[name="row_id_member"]').on('change', function () {
                    var ids = [];
                    var unCheckedCount = 0;
                    $('input[name="row_id_member"]').each(function () {
                        if ($(this).is(':checked')) {
                            ids.push($(this).val());
                        } else {
                            unCheckedCount++;
                        }
                    });
                    $('.remove_member_select_all').prop('checked', unCheckedCount == 0);
                    $('#removeAssignButton').prop("disabled", ids.length == 0);
                    $('.selected_ids').val(ids);
                });


                var $assignMemberButton = $('#assignMemberButton');
                var $member_ids = $('#member_ids');
                var $assgin_member_form = $("#assignMember");
                $assignMemberButton.click(function (event) {
                    var ids = [];
                    var ajaxurl = '{{route("assignment.$department_code.store", ["record_plan_id" => $record_plan_id])}}'
                    var $checkedBoxes = $('#assignMemberTable input[type=checkbox]:checked').not('.select_all');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        ids.push(value);
                    });
                    $member_ids.val(ids);
                    var formData = {
                        department_code : $('#department_code').val(),
                        devices : $('#devices').val(),
                        proposal_number : $('#proposal_number').val(),
                        device_ids : $('#member_ids').val()
                    }
                    $.ajax({
                        type: "POST",
                        url: ajaxurl,
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            location.reload();
                        },
                        error: function (data) {
                            location.reload();
                        }
                    });
                    // $assgin_member_form.submit();
                })

                var $removeAssignMemberButton = $('#removeAssignButton');
                var $remove_member_ids = $('#remove_member_ids');
                var $remove_assgin_member_form = $("#removeAssignMemberForm");
                $removeAssignMemberButton.click(function (event) {
                    var ids = [];
                    var ajaxurl = '{{route("assignment.$department_code.destroy", ["record_plan_id" => $record_plan_id])}}'
                    var $checkedBoxes = $('#removeAssignMemberTable input[type=checkbox]:checked').not('.remove_member_select_all');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        ids.push(value);
                    });
                    $remove_member_ids.val(ids);
                    var formData = {
                        // department_code : $('#department_code').val(),
                        device_ids : $('#remove_member_ids').val()
                    }
                    $.ajax({
                        type: "DELETE",
                        url: ajaxurl,
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            location.reload();
                        },
                        error: function (data) {
                            location.reload();
                        }
                    });
                    // $remove_assgin_member_form.submit();
                })
            })

        </script>

@endpush

