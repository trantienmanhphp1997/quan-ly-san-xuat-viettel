@extends('pages.assignment.base-assignment')
@section("title")
    {{__('data_field_name.assignment.approved_devices')." <".$record_plan->name .">" }}
@endsection
@section("assigment-table")
    <div class="w-100 d-flex pt-3">
        <div style="width: 49%;  padding-right: 2%;">
            <div>
                <div class="form-group">
                    <div class="d-flex justify-content-between flex-wrap">
                        <label class="col-form-label pb-0">
                            <strong>{{__('data_field_name.assignment.available_device')}}</strong>
                        </label>
                        <div class="d-flex">
                            <form class="form-inline" name="quick-search-form">
                                <div class="form-group has-search ">
                                    <input type="search" class="form-control form-control-sm rounded-0" style="height: 28px; width: 200px; padding-left: 10px;" placeholder="{{__('common.place_holder.search')}}" name="s"
                                        value="{{ $allParams && array_key_exists("s", $allParams)  ? $allParams['s'] : "" }}">
                                    <button class="btn btn-secondary rounded-0 mr-2" style="height: 28px; width: 28px; padding: 1px" data-toggle="tooltip" data-placement="top" title="{{ __('common.button.search') }}"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                            <button id="updateDeviceStatusButton" class="btn btn-success btn-sm" title="{{__('common.button.lending-device')}}" disabled>{{__('common.button.lending-device')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <form action="{{route("assignment.device.update-status")}}" method="post" id="updateDeviceStatusForm">
                    @method("put")
                    @csrf
                    <input type="hidden" value="" id="device_ids" name="device_ids">
                    <input type="hidden" value="{{$record_plan_id}}" name="record_plan_id">
                    <input type="hidden" value="{{config("common.status.propose_device.delivered")}}" name="status">
                </form>

                <div class="table-responsive">
                    <table class="table table-hover table-striped table-head-fixed" id="updateDeviceTable">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" class="select_all">
                            </th>
                            <th>{{__('data_field_name.common_field.stt')}}</th>
                            <th>{{__('data_field_name.common_field.name')}}</th>
                            <th>{{__('data_field_name.device.category')}}</th>
                            <th>{{__('data_field_name.device.serial')}}</th>
                            <th>{{__('data_field_name.common_field.descriptions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $datum)
                            <tr>
                                <td>
                                    <input type="checkbox" name="row_id" id="checkbox_{{ $datum->device_id }}" value="{{ $datum->device_id }}">
                                </td>
                                <td>{{ $loop->index + /*($data->currentPage() - 1) * $data->perPage()*/ +1 }}</td>
                                <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $datum->device->name }}">{{ $datum->device->name }}</td>
                                <td>{{ optional($datum->device->category)->name }}</td>
                                <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $datum->device->serial }}">{{ $datum->device->serial }}</td>
                                <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $datum->device->description }}">{{ $datum->device->description }}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                    @php
                        $query = \Illuminate\Support\Arr::except(request()->query(), 'approved');
                    @endphp
                    {{$data->appends($query)->links()}}
                </div>
            </div>
            <div class="mt-3">
                <a href="{{ route(isset($is_returning) && $is_returning == 1 ? 'assignment.device.returning' :'assignment.device.lending') }}"
                   class="btn btn-default btn-sm" title="{{__('common.button.back')}}">
                    {{__('common.button.back')}}
                </a>
            </div>
        </div>
        <div style="width: 49%;  padding-left: 2%; border-left: solid 1px lightgrey">
            <div>
                <div class="form-group d-flex justify-content-between">
                    <div style="width: 100%; display: flex; justify-content: space-between ">
                        <label class="col-form-label pb-0">
                            <strong>{{__("data_field_name.assignment.delivered_devices")}}</strong> </label>
                        <button id="removeUpdateDeviceStatusButton" class="btn btn-warning btn-sm" title="{{__('common.button.retrieve-device')}}" disabled>
                            {{__('common.button.retrieve-device')}}
                        </button>
                        {{--                            <div style="border-bottom: 1px solid #9b9b9b; flex-grow: 1; margin-left: 12px;"></div>--}}
                    </div>

                </div>
            </div>
            <div>
                <form action="{{route("assignment.device.update-status")}}" method="post" id="removeUpdateDeviceStatusForm">
                    @csrf
                    @method("put")
                    <input type="hidden" value="" id="remove_device_ids" name="device_ids">
                    <input type="hidden" value="{{$record_plan_id}}" name="record_plan_id">
                    <input type="hidden" value="{{config("common.status.propose_device.returned")}}" name="status">
                </form>

                <div class="table-responsive">
                    <table class="table table-hover table-striped table-head-fixed" id="removeUpdateDeviceTable">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" class="remove_select_all">
                            </th>
                            <th>{{__('data_field_name.common_field.stt')}}</th>
                            <th>{{__('data_field_name.common_field.name')}}</th>
                            <th>{{__('data_field_name.device.category')}}</th>
                            <th>{{__('data_field_name.device.serial')}}</th>
                            <th>{{__('data_field_name.common_field.descriptions')}}</th>
                            {{--                                                        <th>{{__('data_field_name.common_field.status')}}</th>--}}
                            <th>{{__('data_field_name.common_field.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($delivered_devices as $datum)
                            <tr>
                                <td>
                                    <input type="checkbox" name="remove_row_id" id="checkbox_{{ $datum->device_id }}" value="{{ $datum->device_id }}">
                                </td>
                                <td>{{ $loop->index + /*($data->currentPage() - 1) * $data->perPage()*/ +1 }}</td>
                                <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $datum->device->name }}">{{ $datum->device->name }}</td>
                                <td>{{ optional($datum->device->category)->name }}</td>
                                <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $datum->device->serial }}">{{ $datum->device->serial }}</td>
                                <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $datum->device->description }}">{{ $datum->device->description }}</td>
                                <td>
                                    <div class="d-flex">
                                        @if( $datum->status == config("common.status.propose_device.delivered"))
                                            <form action="{{route("$routePrefix.device.update-status")}}" method="post">
                                                @method("put")
                                                @csrf
                                                <input type="hidden" value="{{$datum->device_id}}" name="device_ids">
                                                <input type="hidden" value="{{$record_plan_id}}" name="record_plan_id">
                                                <input type="hidden" value="{{config("common.status.propose_device.returned")}}" name="status">
                                                <button class="btn btn-sm no-wrap view text-decoration-none mr-1 btn-warning btn-sm">{{ __("common.button.retrieve-device")}}</button>
                                            </form>
                                        @endif

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @php
                        $queryParams = \Illuminate\Support\Arr::except(request()->query(), 'delivered');
                    @endphp
                    {{$delivered_devices->appends($queryParams)->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('assignment-js')
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            $('.select_all').on('click', function (e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
            });
            $('.remove_select_all').on('click', function (e) {
                $('input[name="remove_row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
            });
        });

        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = ''.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        //Xử lý check box trên list
        $('input[name="row_id"]').on('change', function () {
            var ids = [];
            var unCheckedCount = 0;
            $('input[name="row_id"]').each(function () {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                } else {
                    unCheckedCount++;
                }
            });
            $('.select_all').prop('checked', unCheckedCount == 0);
            $('#updateDeviceStatusButton').prop("disabled", ids.length == 0);
            $('.selected_ids').val(ids);
        });

        $('input[name="remove_row_id"]').on('change', function () {
            var ids = [];
            var unCheckedCount = 0;
            $('input[name="remove_row_id"]').each(function () {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                } else {
                    unCheckedCount++;
                }
            });
            $('.remove_select_all').prop('checked', unCheckedCount == 0);
            $('#removeUpdateDeviceStatusButton').prop("disabled", ids.length == 0);
            $('.selected_ids').val(ids);
        });

        $(document).ready(function () {
            var $updateDeviceStatusButton = $('#updateDeviceStatusButton');
            var $device_ids = $('#device_ids');
            var $updateDeviceStatusForm = $("#updateDeviceStatusForm");
            $updateDeviceStatusButton.click(function (event) {
                var ids = [];
                var $checkedBoxes = $('#updateDeviceTable input[type=checkbox]:checked').not('.select_all');
                $.each($checkedBoxes, function () {
                    var value = $(this).val();
                    ids.push(value);
                });
                console.log(ids)
                $device_ids.val(ids)
                $updateDeviceStatusForm.submit();
            })
        })

        $(document).ready(function () {
            var $removeUpdateDeviceStatusButton = $('#removeUpdateDeviceStatusButton');
            var $remove_device_ids = $('#remove_device_ids');
            var $removeUpdateDeviceStatusForm = $("#removeUpdateDeviceStatusForm");
            $removeUpdateDeviceStatusButton.click(function (event) {
                var ids = [];
                var $checkedBoxes = $('#removeUpdateDeviceTable input[type=checkbox]:checked').not('.remove_select_all');
                $.each($checkedBoxes, function () {
                    var value = $(this).val();
                    ids.push(value);
                });
                console.log(ids)
                $remove_device_ids.val(ids)
                $removeUpdateDeviceStatusForm.submit();
            })
        })


        //xử lý dropdown status gọi submit form
        $('#status-dropdown-filter').on('change', function () {
            document.forms['quick-search-form'].submit();
        });
    </script>
@endpush
