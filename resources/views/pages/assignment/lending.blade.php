@extends('layouts.list')

@section("sub-header-content")
    {{"Quản lý cho mượn thiết bị"}}
@endsection

@section("quick-filter")
    @if(isset($is_lending) && $is_lending )
        <x-quick-filter-btn :allParams="$allParams" name="status" value="2" label="Chưa cấp thiết bị " default-value="2"></x-quick-filter-btn>
        <x-quick-filter-btn :allParams="$allParams" name="status" value="3" label="Đã cấp thiết bị"></x-quick-filter-btn>
    @elseif(isset($is_returning) && $is_returning)
        <x-quick-filter-btn :allParams="$allParams" name="status" value="3" label="Chưa nhận lại thiết bị " default-value="3"></x-quick-filter-btn>
        <x-quick-filter-btn :allParams="$allParams" name="status" value="4" label="Đã nhận lại thiết bị"></x-quick-filter-btn>
    @endif
@endsection

@section('table-list')
    <div class="table-responsive height_auto">
        <table class="table table-hover table-striped table-head-fixed" id="dataTable">
            <thead>
            <tr class="bg-light">
                {{--                <th>--}}
                {{--                    <input type="checkbox" class="select_all">--}}
                {{--                </th>--}}
                <th>{{__('data_field_name.common_field.stt')}}</th>
                <th colspan="2">{{__('data_field_name.common_field.name')}}</th>
                <th>{{__('data_field_name.common_field.start_time')}}</th>
                <th>{{__('data_field_name.common_field.end_time')}}</th>
                <th>{{__('data_field_name.common_field.address')}}</th>

{{--                @if($department_code == config("common.departments.device"))--}}
{{--                    <th colspan="2">{{__('data_field_name.assignment.device')}}</th>--}}
{{--                @endif--}}

                <th>{{__('data_field_name.common_field.status')}}</th>
                <th>{{__('data_field_name.common_field.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $datum)
                <tr>
                    {{--                    <td>--}}
                    {{--                        <input type="checkbox" name="row_id" id="checkbox_{{ $datum->id }}" value="{{ $datum->id }}">--}}
                    {{--                    </td>--}}
                    <td>{{ $loop->index + ($data->currentPage() - 1) * $data->perPage() + 1 }}</td>
                    <td colspan="2">{{ $datum->name }}</td>
                    <td>{{ $datum->start_time }}</td>
                    <td>{{ $datum->end_time }}</td>
                    <td class="text-truncate" style="max-width: 150px">{{ $datum->address }}</td>
{{--                    @php--}}
{{--                        $devices = $datum->devices->map(function ($device) use($status){--}}
{{--                            dd($device);--}}
{{--                            if($device->device_xref->status == $status &&  is_null($device->deleted_at)){--}}
{{--                                return $device->name. "</br>";--}}
{{--                            }--}}
{{--                        });--}}
{{--                        $devices = $devices->filter(function ($value) { return !is_null($value); });--}}
{{--                        $devices = join(", ", $devices->toArray());--}}
{{--                    @endphp--}}
{{--                    <td colspan="2" class="text-truncate" style="max-width: 200px">{!!  $devices !!}</td>--}}
                    <td>
                        @php
                            $status_name = $department_code."_allocate_status";
                        @endphp
                        <x-status-span status='{{$datum[$status_name]}}' data-type="propose" :warning="[1]" :primary="[2]"></x-status-span>
                    </td>
                    <td>
                        @php
                            $params = ["record_plan_id" => $datum->id];
                            $route = "lending-detail";
                            if(isset($is_lending) && $is_lending){
                                $params["is_lending"] = 1;
                            }
                            if(isset($is_returning) && $is_returning){
                                $params["is_returning"] = 1;
                                $route = "returning-detail";
                            }
                            if(!isset($data_return)){
                                $data_return = [];
                            }
                        @endphp
                        @if(!isset($allParams['status']) || $allParams['status']==config('common.status.propose_device.delivered') || in_array($datum->id, $data_return) && isset($is_returning) || !isset($is_returning))
                        <div class="h-100 d-flex no-wrap align-items-center">
                            @include("partials.small-btn.assign-device-btn", ["route" => "assignment.device.$route", "params" => $params])
                        </div>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(count($data) == 0)
        <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
        @endif
    </div>
@endsection
