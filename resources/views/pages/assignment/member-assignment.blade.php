@if(!empty($record_plan["offer_$department_code"."_number"]))
<div class="w-100 d-flex pt-3">
    <div style="width: 49%;  padding-right: 2%;">
        <div>
            <div class="form-group d-flex justify-content-between">
                <div style="width: 100%; display: flex; justify-content: space-between ">
                    <label class="col-form-label pb-0">
                        <strong>{{__('data_field_name.assignment.available_member')}}</strong> </label>
                    <button id="assignMemberButton" class="btn btn-primary btn-sm" title="{{__('common.button.assign')}}" disabled>
                        {{__('common.button.assign')}}
                    </button>
                </div>
            </div>

            <div>
                <form action="{{route("assignment.$department_code.store", ["record_plan_id" => $record_plan_id])}}" method="post" id="assignMember">
                    @csrf
                    <input type="hidden" name="department_code" id="department_code" value="{{$department_code}}">
                    <input type="hidden" name="assigned_member" id="assigned_member" value="{{count($assigned_members)}}">
                    <input type="hidden" name="proposal_number" id="proposal_number" value="{{$record_plan["offer_$department_code"."_number"]}}">
                    <input type="hidden" name="member_ids" value="" id="member_ids">
                </form>

                <div class="table-responsive">
                    <table class="table table-hover table-striped table-head-fixed" id="assignMemberTable">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="select_all">
                                </th>
                                <th>{{__('data_field_name.common_field.stt')}}</th>
                                <th>{{__('data_field_name.user.full_name')}}</th>
                                <th>{{__('data_field_name.common_field.phone')}}</th>
                                @if(auth()->user()->hasRole("super admin") || strpos(auth()->user()->department_code, "production") !== false)
                                    <th>{{__('data_field_name.common_field.department')}}</th>
                                @endif
                                <th>{{__('data_field_name.common_field.position')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($available_members as $index => $member)
                            <tr>
                                <td>
                                    <input type="checkbox" name="row_id" id="checkbox_{{ $member->id }}" value="{{ $member->id }}">
                                </td>
                                <td>{{$index + 1}}</td>
                                <td>{{ $member->full_name }}</td>
                                <td>{{ $member->phone_number }}</td>
                                @if(auth()->user()->hasRole("super admin") || strpos(auth()->user()->department_code, "production") !== false)
                                    <td>{{ __("department.$member->department_code") }}</td>
                                @endif
                                <td>{{ __("data_field_name.model_name.$department_code") }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="mt-3">
                <a href="{{ route(isset($redirect_back_route) && $redirect_back_route ? $redirect_back_route : "assignment.$department_code.proposals" , isset($redirect_back_params) && $redirect_back_params ? $redirect_back_params : []) }}"
                   class="btn btn-default btn-sm " title="{{__('common.button.back')}}">
                    {{__('common.button.back')}}
                </a>
            </div>
        </div>
    </div>

    <div style="width: 49%; padding-left: 2%; border-left: solid 1px lightgrey">
        <div>
            <div class="form-group d-flex justify-content-between">
                <div style="width: 100%; display: flex; justify-content: space-between">
                    <label class="col-form-label pb-0">
                        <strong>{{__('data_field_name.assignment.assigned_member')}}</strong> </label>
                    <button id="removeAssignButton" class="btn btn-danger btn-sm" title="{{__('common.button.remove-assign')}}" disabled>
                        {{__('common.button.remove-assign')}}
                    </button>
                </div>
            </div>
            <div>
                <form action="{{route("assignment.$department_code.destroy", ["record_plan_id" => $record_plan_id])}}" method="post" id="removeAssignMemberForm">
                    @method("delete")
                    @csrf
                    <input type="hidden" name="department_code" id="department_code" value="{{$department_code}}">
                    <input type="hidden" name="member_ids" value="" id="remove_member_ids">
                </form>
                <div class="table-responsive">
                    <table class="table table-hover table-striped table-head-fixed" id="removeAssignMemberTable">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="remove_member_select_all">
                                </th>
                                <th>{{__('data_field_name.common_field.stt')}}</th>
                                <th>{{__('data_field_name.user.full_name')}}</th>
                                <th>{{__('data_field_name.common_field.phone')}}</th>
                                @if(auth()->user()->hasRole("super admin") || strpos(auth()->user()->department_code, "production") !== false)
                                    <th>{{__('data_field_name.common_field.department')}}</th>
                                @endif
                                <th>{{__('data_field_name.common_field.position')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($assigned_members as $index => $member)
                            <tr>
                                <td><input type="checkbox" name="row_id_member" id="checkbox_{{ $member->id }}" value="{{ $member->id }}"></td>
                                <td>{{$index + 1}}</td>
                                <td>{{ $member->full_name }}</td>
                                <td>{{ $member->phone_number }}</td>
                                @if(auth()->user()->hasRole("super admin") || strpos(auth()->user()->department_code, "production") !== false)
                                    <td>{{ __("department.$member->department_code") }}</td>
                                @endif
                                <td>{{ __("data_field_name.model_name.$department_code") }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        {{--<form action="{{route("$routePrefix.add-note", ["record_plan_id" => $record_plan_id])}}" method="post">
            @csrf
            <input type="hidden" name="department_code" value="{{$department_code}}">
            <label class="col-form-label pb-1"><strong>Note of {{config("common.departments.$department_code")}} manager</strong> </label>
            <div class=" row d-flex">
                <div class="col-md-12">
                    <textarea class="form-control" autocomplete="off" name="note" rows="10" >{{isset($note) ? $note : ""}}</textarea>
                </div>
                <div class="col-md-12 mt-3">
                    <button class="btn btn-info btn-md " type="submit" title="{{__("common.button.submit")}}">{{__("common.button.submit")}}</button>
                </div>
            </div>
        </form>--}}
    </div>
</div>
@else
<div class="mt-3">
    <a href="{{ route(isset($redirect_back_route) && $redirect_back_route ? $redirect_back_route : "assignment.$department_code.proposals" , isset($redirect_back_params) && $redirect_back_params ? $redirect_back_params : []) }}"
        class="btn btn-default btn-sm " title="{{__('common.button.back')}}">
        {{__('common.button.back')}}
    </a>
</div>
@endif
