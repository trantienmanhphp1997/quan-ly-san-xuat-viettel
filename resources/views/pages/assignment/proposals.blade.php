@extends('layouts.list')

@section("sub-header-content")
    {{__('data_field_name.action.view_proposal', ["dataName" => __('data_field_name.model_name.'.$department_code)])}}
@endsection
@section("time-filter")
    <div class="btn btn-outline-secondary rounded-0" style="height: 28px; width: 28px; padding: 1px;" data-toggle="tooltip" data-placement="top" title="Thời gian"><i class="far fa-clock"></i></div>
    <select name="time_filter" id="" class="form-control form-control-sm rounded-0" style="height: 28px; width: 120px; padding-left: 10px;">
        <option value="0" {isset($allParams) == false || array_key_exists('time_filter', $allParams) == false  || $allParams['time_filter'] == 0) ? selected : ""}>Tất cả</option>
        <option value="1" {@if(isset($allParams) && array_key_exists('time_filter', $allParams)  && $allParams['time_filter'] != "" && $allParams['time_filter'] == 1) selected @endif}>Hôm nay</option>
        <option value="2" {@if(isset($allParams) && array_key_exists('time_filter', $allParams)  && $allParams['time_filter'] != "" && $allParams['time_filter'] == 2) selected @endif}>Ngày mai</option>
    </select>
    <input type="hidden" name="status" value="{{$allParams['status']}}">
@endsection
@section("quick-filter")
    <x-quick-filter-btn :allParams="$allParams" name="status" value=" " label="Tất cả ({{$total}})" default-value="all"></x-quick-filter-btn>
    <x-quick-filter-btn :allParams="$allParams" name="status" value="1" label="Chưa phân công ({{$unassigned}})"></x-quick-filter-btn>
    <x-quick-filter-btn :allParams="$allParams" name="status" value="2" label="Đã phân công ({{$assigned}})"></x-quick-filter-btn>

    @if($department_code == config("common.departments.device"))
        <x-quick-filter-btn :allParams="$allParams" name="lendingDevice" value="notGive" label="Chưa giao thiết bị ({{$notGive}})"></x-quick-filter-btn>
        <x-quick-filter-btn :allParams="$allParams" name="lendingDevice" value="lendingDevice" label="Đang mượn thiết bị ({{$lendingDevice}})"></x-quick-filter-btn>
        <x-quick-filter-btn :allParams="$allParams" name="lendingDevice" value="lendingOutTime" label="Mượn quá hạn ({{$lendingOutTime}})"></x-quick-filter-btn>
    @endif
@endsection
@if($department_code == config("common.departments.transportation"))
    @section("inline-filter")
    <select id="status-dropdown-filter" name="address_type" class="form-control form-control-sm rounded-0 custom-select-filter" data-dropdown-css-class="select2-blue">
        <option value="all" @if(isset($allParams) == false || array_key_exists('address_type', $allParams) == false  || $allParams['address_type'] == "all" || $allParams['address_type'] == "") selected @endif>Tất cả</option>
        @foreach(config('common.address_type') as $key => $value)
            @if($key != "useStage")
            <option value="{{ $value}}"
                    @if(isset($allParams) && array_key_exists('address_type', $allParams)  && $allParams['address_type'] != "all" && $allParams['address_type'] != "" && $allParams['address_type'] == $value) selected @endif
            >{{__('common.address_type.' . $key)}}</option>
            @endif
        @endforeach
    </select>
    @endsection
@endif
@section("buttons")

    @if($department_code == config("common.departments.device") && (isset($allParams) && array_key_exists("status", $allParams) && $allParams["status"] == 2))
        @include("partials.text-btn.export-excel-btn", ["label" => "DS lịch sử dụng thiết bị", "route" => "devices.export-assignments", "params" => $allParams])
    @endif

@endsection

@section('table-list')
{{--    {{dd($allParams)}}--}}

    <div class="table-responsive">
        <table class="table table-hover table-striped table-head-fixed" id="dataTable">
            <thead>
            <tr class="bg-light">
                {{--                <th>--}}
                {{--                    <input type="checkbox" class="select_all">--}}
                {{--                </th>--}}
                <th>{{__('data_field_name.common_field.stt')}}</th>
                <th colspan="2">{{__('data_field_name.common_field.name')}}</th>
                <th>{{__('data_field_name.common_field.start_time')}}</th>
                <th>{{__('data_field_name.common_field.end_time')}}</th>
                @if($department_code != config("common.departments.device"))
                    <th>{{__('data_field_name.common_field.address')}}</th>
                @endif
                @if(!in_array($department_code, [ config("common.departments.device"), config("common.departments.production")] ))
                    <th class="text-center">{{__('data_field_name.common_field.proposal_number')}}</th>
                @endif

                {{--                @if($department_code != config("common.departments.production"))--}}
                <th>{{__('data_field_name.common_field.proposal')}}</th>
                {{--                @endif--}}
                @if($department_code == config("common.departments.production"))
                    <th>{{__('data_field_name.assignment.reporter')}}</th>
                @endif
                @if($department_code == config("common.departments.camera"))
                    <th>{{__('data_field_name.assignment.cameramen')}}</th>
                @endif
                @if($department_code == config("common.departments.technical_camera"))
                    <th>{{__('data_field_name.assignment.cameramen')}}</th>
                @endif
                @if($department_code == config("common.departments.device"))
                    <th colspan="2">{{__('data_field_name.assignment.device')}}</th>
                @endif
                @if($department_code == config("common.departments.transportation"))
                    <th>{{__('data_field_name.assignment.transportation')}}</th>
                @endif
                <th>{{__('data_field_name.common_field.creator')}}</th>
                <th>{{__('data_field_name.common_field.status')}}</th>
                <th>{{__('data_field_name.common_field.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $datum)
                <tr>
                    {{--                    <td>--}}
                    {{--                        <input type="checkbox" name="row_id" id="checkbox_{{ $datum->id }}" value="{{ $datum->id }}">--}}
                    {{--                    </td>--}}
                    <td>{{ $loop->index + ($data->currentPage() - 1) * $data->perPage() +1 }}</td>
                    <td colspan="2" class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $datum->name }}">{{ $datum->name }}</td>
                    <td>{{  date("d-m-Y H:i" , strtotime($datum->start_time)) }}</td>
                    <td>{{  date("d-m-Y H:i" , strtotime($datum->end_time)) }}</td>
                    @if($department_code != config("common.departments.device"))
                        <td class="text-truncate" style="max-width: 150px" data-toggle="tooltip" data-placement="top" title="{{ $datum->actual_address }}">{{ $datum->actual_address }}</td>
                    @endif
                    {{--                    Nội dung đề xuất--}}
                    @if($department_code == config("common.departments.production"))
{{--                        <td class="text-center">{{$datum->offer_reporter_number}}</td>--}}
                        <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{$datum->offer_reporter_note}}">{{$datum->offer_reporter_note}}</td>
                    @endif
                    @if($department_code == config("common.departments.camera"))
                        <td class="text-center">{{$datum->offer_camera_number}}</td>
                        <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{$datum->offer_camera_note}}">{{$datum->offer_camera_note}}</td>
                    @endif
                    @if($department_code == config("common.departments.technical_camera"))
                        <td class="text-center">{{$datum->offer_technical_camera_number}}</td>
                        <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{$datum->offer_technical_camera_note}}">{{$datum->offer_technical_camera_note}}</td>
                    @endif
                    @if($department_code == config("common.departments.device"))
{{--                        <td class="text-center">{{$datum->offer_device_number}}</td>--}}
                        <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{$datum->offer_device_note}}">{{$datum->offer_device_note}}</td>
                    @endif
                    @if($department_code == config("common.departments.transportation"))
                        <td class="text-center">{{$datum->offer_transportation_number}}</td>
                        <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{$datum->offer_transportation_note}}">{{$datum->offer_transportation_note}}</td>
                    @endif
                    {{--                    Danh sách nhân sự/thiết bị đã phân bổ--}}
                    @if($department_code == config("common.departments.production"))
                        @php
                            $reporters = $datum->members->map(function ($member){
                                if(strpos($member->department_code, config("common.departments.production")) !== false ){
                                    return $member->full_name. "</br>";
                                }
                            });
                            $reporters = $reporters->filter(function ($value) { return !is_null($value); });
                            $reporters = join("", $reporters->toArray());
                        @endphp
                        <td>{!! $reporters !!}</td>
                    @endif

                    @if($department_code == config("common.departments.camera"))
                        @php
                            $cams = $datum->members->map(function ($member){
                                if($member->department_code ==  config("common.departments.camera")){
                                    return $member->full_name. "</br>";
                                }
                            });
                            $cams = $cams->filter(function ($value) { return !is_null($value); });
                            $cams = join("", $cams->toArray());
                        @endphp
                        <td>{!! $cams !!}</td>
                    @endif

                    @if($department_code == config("common.departments.technical_camera"))
                        @php
                            $cams = $datum->members->map(function ($member){
                                if($member->department_code ==  config("common.departments.technical_camera")){
                                    return $member->full_name. "</br>";
                                }
                            });
                            $cams = $cams->filter(function ($value) { return !is_null($value); });
                            $cams = join("", $cams->toArray());
                        @endphp
                        <td>{!! $cams !!}</td>
                    @endif
                    @if($department_code == config("common.departments.device"))
                        @php
                            $devices = $datum->devices->map(function ($device){
                                if($device->status == 1 &&  is_null($device->deleted_at)){
                                    return $device->name. "</br>";
                                }
                            });
                            $devices = $devices->filter(function ($value) { return !is_null($value); });
                            $devices = join("", $devices->toArray());
                        @endphp
                        <td colspan="2" class="text-truncate" style="max-width: 150px" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ $devices }}">{!!  $devices !!}</td>
                    @endif
                    @if($department_code == config("common.departments.transportation"))
                        @php
                            $drivers = $datum->members->map(function ($member){
                                if($member->department_code ==  config("common.departments.transportation")){
                                    return $member->full_name. "</br>";
                                }
                            });
                            $drivers = $drivers->filter(function ($value) { return !is_null($value); });
                            $drivers = join("", $drivers->toArray());
                        @endphp
                        {{--                        <td>{!!  $datum->cars !!}</td>--}}
                        <td>{!!  $drivers !!}</td>
                    @endif
                    <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{$datum->creator ? $datum->creator->full_name.' - '.$datum->creator->phone_number : ''}}">{{ $datum->creator ? ($datum->creator->full_name ? $datum->creator->full_name : $datum->creator->phone_number) : "" }}</td>
                    <td>
                        @php
                            $status_name = $department_code."_allocate_status";
                               if($department_code == config("common.departments.production")){
                                $status_name = "reporter_allocate_status";
                               }
                        @endphp
                        <x-status-span status='{{$datum[$status_name]}}' data-type="propose" :warning="[1]" :primary="[2]"></x-status-span>
                    </td>
                    <td class="d-flex">
                        @php
                            $titleBtn = __('common.button.view');
                            $icon = "fa-eye";

                                $condition = ((auth()->user()->is_manager == 1 && str_contains(auth()->user()->department_code, "production") && auth()->user()->id == $datum->created_by) || auth()->user()->hasRole('super admin'))
                                               && \App\Services\RecordPlanService::checkTimeToUpdateRecordPlan($datum->end_time);
                                if($condition){
                                    $titleBtn = __('common.button.edit');
                                    $icon = "fa-edit";
                                }
                        @endphp

                        @can("view", $datum)
                            <a href='{{route( 'record-plans.show',['id' => $datum->id])}}' type="button"
                               class="btn btn-default btn-sm action rounded-1 mr-1" data-toggle="tooltip"
                               data-placement="top"
                               title="{{ $titleBtn }}"><i class="fas {{$icon}}" title="{{ $titleBtn }}"></i></a>
                        @endcan
                        @can("canUpdateApprovedRecordPlan", $datum)
                            @if(($datum->status != config("common.status.record_plan.in_progress") && $datum->status != config("common.status.record_plan.wrap") && $datum->status != config("common.status.record_plan.done")))
                                <div class="h-100 d-flex no-wrap align-items-center">
                                    @if($department_code == config("common.departments.camera"))
                                        @include("partials.small-btn.assign-cam-btn", ["route" => "assignment.camera.show", "params" => ["record_plan_id" => $datum]])
                                    @elseif($department_code == config("common.departments.technical_camera"))
                                        @include("partials.small-btn.assign-technical-cam-btn", ["route" => "assignment.technical_camera.show", "params" => ["record_plan_id" => $datum]])
                                    @elseif($department_code == config("common.departments.transportation"))
                                        @include("partials.small-btn.assign-car-driver-btn", ["route" => "assignment.transportation.show", "params" => ["record_plan_id" => $datum]])
                                    @elseif($department_code == config("common.departments.device"))
                                        @include("partials.small-btn.assign-device-btn", ["route" => "assignment.device.show", "params" => ["record_plan_id" => $datum->id]])
                                    @else
    {{--                                    @if($datum->created_by == auth()->user()->id)--}}
                                        @include("partials.small-btn.assign-reporter-btn", ["route" => "assignment.reporter.show", "params" => ["record_plan_id" => $datum]])
    {{--                                    @endif--}}
                                    @endif
                                </div>
                            @endif
                        @endcan
                        @if($department_code == config("common.departments.device"))
                            @if(count($datum->notGivenDevice) > 0)
                                <a  class='btn btn-sm btn-outline-success pull-right view text-decoration-none ml-1 no-wrap delivery-btn' id="{{$datum->id}}">{{__('common.button.lending')}}</a>
                            @endif
                            @if(count($datum->lendingDevices) > 0)
                                <a  class='btn btn-sm btn-outline-success pull-right view text-decoration-none ml-1 no-wrap receive-btn' id="{{$datum->id}}">{{__('common.button.retrieve-device')}}</a>
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(count($data) == 0)
            <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
        @endif
    </div>
{{--    @if(count($data) > 0)--}}
    @if($department_code == config("common.departments.device"))
        @include('partials.modal.edit-assign-devices',['title'=>'Danh sách thiết bị sẵn có', 'action'=>'assign', 'btn_action'=>'assign-device', 'submit_btn'=>__('common.button.assign')])
        @include('partials.modal.edit-assign-devices',['title'=>'Danh sách thiết bị đã cấp', 'action'=>'receive', 'btn_action'=>'receive-device', 'submit_btn'=>'Nhận lại'])
        @include('partials.modal.edit-assign-devices',['title'=>'Danh sách thiết bị sẽ cấp', 'action'=>'delivery', 'btn_action'=>'delivery-device', 'submit_btn'=>'Cấp thiết bị'])
    @else
        @include('partials.modal.edit-assign-members',['type'=>$department_code])
    @endif
{{--    @endif--}}

@endsection
@push('list-record-plan')
    <script src="{{ asset('js/device-services.js') }}"></script>
    <script>
        $(document).ready(function () {
            var selected_ids = [];
            $(".assign-btn").click(function () {
                var department_code = '{{$department_code}}'
                recordPlanId = JSON.parse($(this).attr('id')).id
                switch (department_code) {
                    case 'production':
                        $('.offer_reporter_number').html(JSON.parse($(this).attr('id')).offer_reporter_number)
                        break;
                        case 'camera':
                        $('.offer_camera_number').html(JSON.parse($(this).attr('id')).offer_camera_number)
                        break;
                         case 'transportation':
                        $('.offer_transportation_number').html(JSON.parse($(this).attr('id')).offer_transportation_number)
                        break;
                    default:
                        $('.offer_number').html(0)
                        break;
                }
                $('#assign_member_modal').modal('show');
                getMembers();
                getDeviceOutsourceNote(recordPlanId);
            });
            $('.assign-device-btn').click(function() {
                recordPlanId = $(this).attr('id');
                getListAvailableDevice(null, recordPlanId, null)
                getListDeviceCategory();
                $('#assign_device_modal').modal('show');
            })
            $('.delivery-btn').click(function() {
                recordPlanId = $(this).attr('id');
                getListDevicesForLending(recordPlanId);
                getListMemberCanLendingDevice(recordPlanId);
                $('#delivery_device_modal').modal('show');
            });

            $('.receive-btn').click(function() {
                recordPlanId = $(this).attr('id')
                getListDevicesForReturning(recordPlanId);
                $('#receive_device_modal').modal('show');
            })

        })
    </script>
@endpush
