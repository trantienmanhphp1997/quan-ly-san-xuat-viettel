@extends('layouts.detail')

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex" style="font-weight: bold;">
                            <div>
                                <i class="fa fa-align-justify mr-2"></i>
                            </div>
                            <strong>
                                @if(!isset($data))
                                    {{__("data_field_name.action.create", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @elseif(isset($editable) && $editable == true)
                                    {{__("data_field_name.action.edit", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @else
                                    {{__("data_field_name.action.show", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @endif
                            </strong>
                        </div>
                        <div class="card-body">
                            <br>
                            <form method="POST" novalidate
                                  action=" {{ (!isset($data)) ? route($routePrefix.".store") : route($routePrefix.".update", ["id" => $data->id])}}"
                                  id="detail-form">
                                @csrf
                                @if(isset($editable) && $editable == true && isset($data))
                                    @method('PUT')
                                @endif
                                <x-input type="text" label="{{__('data_field_name.car.brand')}}" form-name="brand" value="{{isset($data) ? $data->brand : ''}}" required=true disabled="{{!$editable}}" placeholder="" />
                                <x-input type="text" label="{{__('data_field_name.car.model')}}" form-name="model" value="{{isset($data) ? $data->model : ''}}" required=true disabled="{{!$editable}}" placeholder="" />
                                <x-input type="text" label="{{__('data_field_name.car.color')}}" form-name="color" value="{{isset($data) ? $data->color : ''}}" required=true disabled="{{!$editable}}" placeholder="" />

                                @if(isset($editable) && $editable == true)
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">{{__('data_field_name.car.transportation')}}</label>
                                        <div class="col-md-9">
                                            <select name="driver_id" id="driver_list" class="form-control" >
                                                <option value="">{{__("data_field_name.action.select", ["dataName" => __('data_field_name.car.transportation')])}}</option>
                                                @if(isset($drivers))
                                                    @foreach($drivers as $driver)
                                                        <option value="{{ $driver->id }}" {{(isset($data) && $driver->id == $data->driver_id) || (old("driver_id") == $driver->id) ? "selected" : "" }}>{{ $driver->full_name }}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                            <label id="driver_list-error"></label>
                                        </div>
                                    </div>
                                @else
                                    <x-input type="text" label="{{__('data_field_name.car.transportation')}}" form-name="department_code" value="{{isset($data) && $data->transportation ? $data->transportation->full_name : ''}}" required=true disabled="true" placeholder=""/>
                                @endif

                                <x-input type="number" label="{{__('data_field_name.car.num_of_seat')}}" form-name="number_of_seats" value="{{isset($data) ? $data->number_of_seats : ''}}" required=true disabled="{{!$editable}}" placeholder=""/>
                                <x-input type="text" label="{{__('data_field_name.car.license_plate')}}" form-name="license_plate" value="{{isset($data) ? $data->license_plate : ''}}" required=true disabled="{{!$editable}}" placeholder="" />
                                <x-textarea label="{{__('data_field_name.common_field.descriptions')}}" form-name="description" value="{{isset($data) ? $data->description : ''}}" disabled="{{!$editable}}" placeholder=""/>
                                <x-checkbox label="{{__('data_field_name.common_field.active')}}" form-name="status"
                                            checked="{{ (isset($editable) && !isset($data))? 'checked' : (isset($data) && $data->status == 1 ? 'checked' : '') }}"
                                            disabled="{{!$editable}}"/>
                                <input type="hidden" name="link" id="link">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        @include("partials.common-detail-action", ["back_params" =>
                                        ["is_active" => isset($data) ? ($data->deleted_at ? -1 : 1) : (isset($is_active) ? $is_active : 1)]
                                        ])
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
    @include("partials.message")
    <script>
        $("#driver_list").on("select2:close", function (e) {
            $(this).valid();
        });
        $('#driver_list-error').hide();
    </script>
@endsection
