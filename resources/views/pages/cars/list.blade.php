@extends('layouts.list')

@section("buttons")
    @can('create', app($modelClass))
        @include("partials.text-btn.create-group-btn")
    @endcan
    @can('export', app($modelClass))
        @include("partials.text-btn.export-excel-btn",["params" => $allParams])
    @endcan

    @include("partials.text-btn.export-excel-btn", ["label" => "DS lịch sử dụng xe", "route" => "$routePrefix.export-assignments", "params" => $allParams])

    @can("deleteAny", app($modelClass))
        @include("partials.text-btn.report-broken",['disabled' => true])
    @endcan
    @can("restore", app($modelClass))
        @include("partials.text-btn.bulk-restore",['disabled' => true])
    @endcan
@endsection

@section("sub-header-content")
    {{__('data_field_name.action.manage', ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
@endsection

@section("quick-filter")
    <div class="btn-group mr-1" role="group" aria-label="Button group"  >
        <x-quick-filter-btn :allParams="$allParams" name="status" value="" label="{{__('status.car.all')}} ({{$countInfo->has('total') ? $countInfo['total'] : 0}})"></x-quick-filter-btn>
        <x-quick-filter-btn :allParams="$allParams" name="status" value="0" label="{{__('status.car.inactive')}} ({{$countInfo->has(0) ? $countInfo[0] : 0}})"></x-quick-filter-btn>
        <x-quick-filter-btn :allParams="$allParams" name="status" value="1" label="{{__('status.car.active')}} ({{$countInfo->has(1) ? $countInfo[1] : 0}})"></x-quick-filter-btn>
        {{--        <x-quick-filter-btn :allParams="$allParams" name="status" value="2" label="Busy"></x-quick-filter-btn>--}}
        <x-quick-filter-btn :allParams="$allParams" name="status" value="3" label="{{__('status.car.broken')}} ({{$countInfo->has(3) ? $countInfo[3] : 0}})"></x-quick-filter-btn>
    </div>
@endsection

@section('table-list')
    @include("partials.warning")
    <div class="table-responsive">
        <table class="table table-hover table-striped" id="dataTable">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" class="select_all">
                    </th>
                    <th>{{__('data_field_name.common_field.stt')}}</th>
                    <th>{{__('data_field_name.car.brand')}}</th>
                    <th>{{__('data_field_name.device.model')}}</th>
                    <th>{{__('data_field_name.car.num_of_seat')}}</th>
                    <th>{{__('data_field_name.car.license_plate')}}</th>
                    <th>{{__('data_field_name.common_field.status')}}</th>
                    <th>{{__('data_field_name.car.driver_name')}}</th>
                    <th>{{__('data_field_name.common_field.phone')}}</th>
                    <th>{{__('data_field_name.common_field.action')}}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($data as $datum)
                <tr>
                    <td>
                        <input type="checkbox" name="row_id" id="checkbox_{{ $datum->id }}" value="{{ $datum->id }},{{$datum->status}}">
                    </td>
                    <td>{{ $loop->index + ($data->currentPage() - 1) * $data->perPage() +1 }}</td>
                    <td>{{ $datum->brand }}</td>
                    <td>{{ $datum->model }}</td>
                    <td>{{ $datum->number_of_seats }}</td>
                    <td>{{ $datum->license_plate }}</td>
                    <td> @if($datum->status == 1)
                            <span class="h100 badge badge-success">{{__('status.car.active')}}</span>
                        @elseif($datum->status == 3)
                            <span class="h100 badge badge-danger">{{__('status.car.broken')}}</span>
                        @else
                            <span class="h100 badge badge-secondary">{{__('status.car.inactive')}}</span>
                        @endif
                    </td>
                    <td>{{ isset($datum->transportation) ? $datum->transportation->full_name : "" }}</td>
                    <td>{{ isset($datum->transportation) ? $datum->transportation->phone_number : "" }}</td>
                    <td>
                        <div class="d-flex">
                            @include('partials.small-btn.common-action')
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(count($data) == 0)
        <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
        @endif
    </div>
@endsection
