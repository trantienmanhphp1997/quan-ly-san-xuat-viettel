@extends('layouts.detail')

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex" style="font-weight: bold;">
                            <div>
                                <i class="fa fa-align-justify mr-2"></i>
                            </div>

                            <strong>
                               Đổi mật khẩu
                            </strong>
                        </div>
                        <div class="card-body">
                            <form method="POST" novalidate
                                  action="{{route('change-password.store')}}"
                                  id="detail-form">
                                @csrf

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">
                                        {{__('data_field_name.user.current_password')}}
                                        <span class="text-danger">(*)</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="current_password" value="" required=true  placeholder="{{__('common.place_holder.password')}}" id="current-password">
                                        <span toggle="#current-password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                          @error('current_password')
                                        <label class="error">
                                            {{ $message }}
                                        </label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">
                                        {{__('data_field_name.user.new_password')}}
                                        <span class="text-danger">(*)</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="new_password" value="" required=true  placeholder="{{__('common.place_holder.password')}}" id="new-password">
                                        <span toggle="#new-password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        @error('new_password')
                                        <label class="error">
                                            {{ $message }}
                                        </label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">
                                        {{__('data_field_name.user.confirm_password')}}
                                        <span class="text-danger">(*)</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="confirm_password" value="" required=true  placeholder="{{__('common.place_holder.password')}}" id="confirm-password">
                                        <span toggle="#confirm-password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        @error('confirm_password')
                                        <label class="error">
                                            {{ $message }}
                                        </label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                       <button type="submit" class="btn btn-success btn-action">Cập nhật</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('validation-rules')
    current_password : "required",
    new_password : "required",
    confirm_password : {
        required: true,
        equalTo: '[name="new_password"]'
    }
@endpush

@push('validation-messages')
    current_password:  {
        required: "{{__('client_validation.form.required.user.current_password')}}",
    },
    new_password:  {
        required: "{{__('client_validation.form.required.user.new_password')}}",
    },
    confirm_password:  {
        required: "{{__('client_validation.form.required.user.confirm_password')}}",
        equalTo : "{{__('client_validation.form.equal.confirm_password')}}"
    },
@endpush
@section('javascript')
    @include("partials.message")
    <script>
        $(".toggle-password").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>

@endsection
