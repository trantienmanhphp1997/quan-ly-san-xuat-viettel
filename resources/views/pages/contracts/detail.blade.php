@extends('layouts.detail')

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex" style="font-weight: bold;">
                            <div>
                                <i class="fa fa-align-justify mr-2"></i>
                            </div>
                            <strong>
                                @if(!isset($data))
                                    {{__("data_field_name.action.create", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @elseif(isset($editable) && $editable == true)
                                    {{__("data_field_name.action.edit", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @else
                                    {{__("data_field_name.action.show", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @endif
                            </strong>
                        </div>
                        <div class="card-body">
                            <br>
                            <form method="POST" novalidate
                                  action=" {{ (!isset($data)) ? route($routePrefix.".store") : route($routePrefix.".update", ["id" => $data->id])}}"
                                  id="detail-form">
                                @csrf
                                @if(isset($editable) && $editable == true && isset($data))
                                    @method('PUT')
                                @endif

                                <x-input type="text" label="{{__('data_field_name.contract.title')}}" form-name="title" value="{{isset($data) ? $data->title : ''}}" required=true disabled="{{!$editable}}" placeholder="" />
                                <x-input type="text" label="{{__('data_field_name.contract.code')}}" form-name="code" value="{{isset($data) ? $data->code : ''}}" required=true disabled="{{isset($data) ? true :false}}" placeholder="" />
                                @if(isset($data))
                                    <input type="hidden" name="code" value="{{isset($data) ? $data->code : ''}}">
                                @endif

                                @if(isset($editable) && $editable == true)
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">{{__('data_field_name.model_name.department')}}<span class="text-danger">(*)</span></label>
                                        <div class="col-md-9">
                                            <select name="department_id" id="department_list" class="form-control" required>
                                                <option value="">{{__('data_field_name.action.select', ["dataName" => __('data_field_name.model_name.department')])}}</option>
                                                @if(isset($departments))
                                                    @foreach($departments as $department)
                                                        <option value="{{ $department->id }}" {{isset($data) && $department->id == $data->department_id ? "selected" : "" }}>{{ $department->name }}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                        </div>
                                    </div>
                                @else
                                    <x-input type="text" label="Departments" form-name="department_id" value="{{isset($data) ? $data->department->name : ''}}" required=true disabled="true" placeholder="" />
                                @endif

                                @if(isset($editable) && $editable == true)
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">{{__('data_field_name.model_name.statements')}}<span class="text-danger">(*)</span></label>
                                        <div class="col-md-9">
                                            <select name="statement_id" id="statement_list" class="form-control" required>
                                                <option value="">{{__('data_field_name.action.select', ["dataName" => __('data_field_name.model_name.statements')])}}</option>
                                                @if(isset($statements))
                                                    @foreach($statements as $statement)
                                                        <option value="{{ $statement->id }}" {{isset($data) && $statement->id == $data->statement_id ? "selected" : "" }}>{{ $statement->title }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                @else
                                    <x-input type="text" label="Statement" form-name="statement_id" value="{{isset($data) ? $data->statement->title ." - ". $data->statement->code : ''}}" required=true disabled="true" placeholder="" />
                                @endif


                                <x-textarea label="{{__('data_field_name.common_field.descriptions')}}" form-name="description" value="{{isset($data) ? $data->description : ''}}" disabled="{{!$editable}}" placeholder=""/>
                                <input type="hidden" name="link" id="link">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        @include("partials.common-detail-action", ["back_params" =>
                                        ["is_active" => isset($data) ? ($data->deleted_at ? -1 : 1) : (isset($is_active) ? $is_active : 1)]
                                        ])
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('validation-rules')
    department_id: "required",
    statement_id: "required",
@endpush

@push('validation-messages')
    department_id: {
        required: "{{__('client_validation.form.required.contract.department_id')}}",
    },
    statement_id: {
        required:"{{__('client_validation.form.required.contract.statement_id')}}",
    }
@endpush

@section('javascript')
    @include("partials.message")
    <script>
        $(document).ready(function (){
            $("#department_list").change(function (){
                var department_id = $("#department_list").val();
                if(department_id){
                    removeStatements("statement_list")
                    getStatementsByDepartmentId(department_id)
                }else{
                    removeStatements("statement_list")
                }
            });

            function getStatementsByDepartmentId(department_id){
                axios.post("/statements/get-data-by-department-id", {
                    department_id: department_id
                }).then(res => {
                    var $data = res.data;
                    var $select_statement_id = $("#statement_list")
                    $data.forEach($statement => {
                        $select_statement_id.append(`
                        <option value="${$statement.id}">${$statement.title}</option>
                        `)
                    })
                }).catch(function (error) {
                    console.log(error);
                })
            }

            function removeStatements(el){
                $(`#${el}`).children().remove();
                $(`#${el}`).append(
                    `<option value="">{{__('data_field_name.action.select', ["dataName" => __('data_field_name.model_name.statements')])}}</option>`
                )
            }
        })
    </script>
@endsection
