@extends('layouts.list')

@section("buttons")
    @can('create', app($modelClass))
        @include("partials.text-btn.create-group-btn")
    @endcan
@endsection

@section("sub-header-content")
    {{__('data_field_name.action.manage', ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
@endsection

@section('table-list')
    <div class="row">
        <div class="gw-sidebar">
            <div id="gw-sidebar" class="gw-sidebar">
                <div class="nano-content">
                    <ul class="gw-nav gw-nav-list">
                        <input type="hidden" name="access-permission-route" value="{{route("permissions.access")}}" />
                        <input type="hidden" name="feature-permission-route" value="{{route("permissions.features")}}" />
                        <!-- <li class="init-un-active"> <a href="javascript:void(0)"><span class="gw-menu-text">GĐ - Giám đốc</span> </a> </li> -->
                        @foreach($tree as $department)
                        <x-department-item :item=$department level=0></x-department-item>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function toggleIcon(e) {
            $(e.target)
                .prev(".panel-heading")
                .find(".more-less")
                .toggleClass("fa-arrow-up fa-arrow-down");
        }
        $(".panel-group").on("hidden.bs.collapse", toggleIcon);
        $(".panel-group").on("shown.bs.collapse", toggleIcon);

    </script>
    <script>
        var toggler = document.getElementsByClassName("caret");
        var i;

        for (i = 0; i < toggler.length; i++) {
            toggler[i].addEventListener("click", function() {
                this.parentElement.querySelector(".nested").classList.toggle("active");
                this.classList.toggle("caret-down");
            });
        }
    </script>
    <script>
        function toggleIcon(e) {
            $(e.target)
                .prev(".menu-parent")
                .find(".more-less")
                .toggleClass("fa-arrow-up fa-arrow-down");
        }
        $(".menu-parent").on("hidden.bs.collapse", toggleIcon);
        $(".menu-parent").on("shown.bs.collapse", toggleIcon);

    </script>
    <script>
        let accessRoute = document.getElementsByName("access-permission-route")[0].value
        let featureRoute = document.getElementsByName("feature-permission-route")[0].value
        console.log(accessRoute,featureRoute)
        document
            .querySelectorAll("button[data-action='access-permission']")
            .forEach(el => el.addEventListener("click", (e) => {
                e.stopImmediatePropagation()
                e.stopPropagation()
                const deparmentId = el.dataset.departmentId

                window.location.href = `${accessRoute}?department=${deparmentId}`
            }))
        document
            .querySelectorAll("button[data-action='feature-permission']")
            .forEach(el => el.addEventListener("click", (e) => {
                e.stopImmediatePropagation()
                e.stopPropagation()
                const deparmentId = el.dataset.departmentId

                window.location.href =`${featureRoute}?department=${deparmentId}`
            }))
    </script>
@endpush
