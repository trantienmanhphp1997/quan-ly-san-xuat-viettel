@extends('layouts.detail')
@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex" style="font-weight: bold;">
                            <div>
                                <i class="fa fa-align-justify mr-2"></i>
                            </div>

                            <strong>
                                @if(!isset($data))
                                    {{__("data_field_name.action.create", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @elseif(isset($editable) && $editable == true)
                                    {{__("data_field_name.action.edit", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @else
                                    {{__("data_field_name.action.show", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @endif
                            </strong>
                        </div>
                        <div class="card-body">
                            <br>
                            <form method="POST" novalidate
                                  action=" {{ (!isset($data)) ? route($routePrefix.".store") : route($routePrefix.".update", ["id" => $data->id])}}"
                                  id="detail-form">
                                @csrf
                                @if(isset($editable) && $editable == true && isset($data))
                                    @method('PUT')
                                @endif
                                <x-input type="text" label="{{__('data_field_name.device_category.name')}}" form-name="name" value="{{@$data->name}}"
                                         required=true disabled="{{!$editable}}" placeholder=""></x-input>

                                @if(isset($editable) && $editable == true && $hideCategory == false)
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">{{__('data_field_name.device_category.parent')}}</label>
                                        <div class="col-md-9">
                                            @if(isset($categories))
                                            <select name="parent_id" id="group_list" class="form-control">
                                                <option value="0">{{__('data_field_name.action.select', ["dataName" => __('data_field_name.common_field.parent_category')])}}</option>
                                                @foreach($categories as $categorie)
                                                    <option value="{{ $categorie->id }}"
                                                        {{isset($data) && $categorie->id == $data->parent_id ? "selected" : "" }}
                                                    >{{ $categorie->name }}</option>
                                                @endforeach
                                            </select>
                                            @endif
                                            <label id="category_list_error"></label>
                                        </div>
                                    </div>
                                @endif
                                <x-input type="text" label="{{__('data_field_name.device_category.code')}}" form-name="display_code"
                                         value="{{@$data->display_code}}"
                                         required=true disabled="{{$hideCategory}}" placeholder=""></x-input>
                                <!-- <x-input type="hidden" label="" form-name="parent_id" id="parent_id" value="0"
                                         required=false disabled="" placeholder=""></x-input> -->
                                <x-textarea label="{{__('data_field_name.common_field.descriptions')}}" form-name="description" value="{{@$data->description}}"
                                            disabled="{{!$editable}}" placeholder=""></x-textarea>
                                <input type="hidden" name="link" id="link">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        @include("partials.common-detail-action", ["back_params" =>
                                        ["is_active" => isset($data) ? ($data->deleted_at ? -1 : 1) : (isset($is_active) ? $is_active : 1)]
                                        ])
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    @include("partials.message")
    <script>
        //handle tree
        $(function () {
            var nodeSelected = null;

            // Kiểm tra parent id khi cập nhật danh mục, gán = node đã chọn
            @if(isset($data))
                nodeSelected ='#'+ "{{$data->parent_id}}";
            @endif

            // 6 create an instance when the DOM is ready
            $('#jstree').jstree();

            // hiển thị node danh mục cha đã chọn
            $('#jstree').jstree("select_node", nodeSelected, true);

            // 7 bind to events triggered on the tree
            // cập nhật danh mục cha mới
            $('#jstree').on("changed.jstree", function (e, data) {
                $('#parent_id').val(data.node.id);
            });

            // 8 interact with the tree - either way is OK
            $('button').on('click', function () {
                $('#jstree').jstree(true).select_node('child_node_1');
                $('#jstree').jstree('select_node', 'child_node_1');
                $.jstree.reference('#jstree').select_node('child_node_1');
            });
        });

        $("#category_list").on("select2:close", function (e) {
            $(this).valid();
        });
        $('#category_list_error').hide();
    </script>
@endsection

