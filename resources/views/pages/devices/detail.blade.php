@extends('layouts.detail')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex" style="font-weight: bold;">
                            <div>
                                <i class="fa fa-align-justify mr-2"></i>
                            </div>
                            <strong>
                                @if(!isset($data))
                                    {{__("data_field_name.action.create", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @elseif(isset($editable) && $editable == true)
                                    {{__("data_field_name.action.edit", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @else
                                    {{__("data_field_name.action.show", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @endif
                            </strong>
                        </div>
                        <div class="card-body">
                            <br>
                            <form method="POST" novalidate
                                  action=" {{ (!isset($data)) ? route($routePrefix.".store") : route($routePrefix.".update", ["id" => $data->id])}}"
                                  id="detail-form">
                                @csrf
                                @if(isset($editable) && $editable == true && isset($data))
                                    @method('PUT')
                                @endif
                                <x-input type="text" label="{{__('data_field_name.device.name')}}" form-name="name" value="{{@$data->name}}"
                                         required=true disabled="{{!$editable}}" placeholder=""></x-input>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">{{__('data_field_name.device.category')}} <span
                                                class="text-danger">(*)</span></label>
                                        <div class="col-md-9">
                                            <select id="category_list"
                                                    class="form-control" required = true name="device_category_id">
                                                <option value="">{{__('data_field_name.action.select', ["dataName" => __('data_field_name.model_name.device-categories')])}}</option>
                                                @if(isset($categories))
                                                    @foreach($categories as $category)
                                                        <option
                                                            value="{{ $category->id }}" {{(isset($data) && $category->id == $data->device_category_id) || (old("device_category_id") == $category->id ) ? "selected" : "" }}>
                                                            {{$category->name}}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <label id="category_list_error"></label>
                                        </div>
                                    </div>
                                <x-input type="text" label="{{__('data_field_name.device.serial')}}" form-name="serial" value="{{@$data->serial}}"
                                         required=true disabled="{{!$editable}}" placeholder=""></x-input>
                                <x-textarea label="{{__('data_field_name.common_field.descriptions')}}" form-name="description" value="{{@$data->description}}"
                                            disabled="{{!$editable}}" placeholder=""></x-textarea>
                                <x-checkbox label="{{__('data_field_name.common_field.active')}}" form-name="status"
                                            checked="{{ (isset($editable) && !isset($data))? 'checked' : (isset($data) && $data->status == 1 ? 'checked' : '') }}"
                                            disabled="{{!$editable}}"/>
                                <input type="hidden" name="link" id="link">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        @include("partials.common-detail-action", ["back_params" =>
                                        ["is_active" => isset($data) ? ($data->deleted_at ? -1 : 1) : (isset($is_active) ? $is_active : 1)]
                                        ])
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('validation-rules')
    device_category_id: "required",
@endpush

@push('validation-messages')
    device_category_id: {
    required: "{{__('client_validation.form.required.device.device_category_id')}}",
    },
@endpush

@section('javascript')
    @include("partials.message")
    <script>
        $("#category_list").on("select2:close", function (e) {
            $(this).valid();
        });
        $('#category_list_error').hide();

        $editable = '{{$editable}}';
        if($editable == 0) {
            $("#category_list").prop('disabled',true);
        }
    </script>
@endsection

