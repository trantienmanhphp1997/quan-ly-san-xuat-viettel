@extends('layouts.list')

@section("buttons")

@endsection
@section("sub-header-content")
    {{__('data_field_name.action.lending_device_list')}}
@endsection
@section("quick-filter")
    <x-quick-filter-btn :allParams="$allParams" name="status" value="{{config('common.status.proposals.unassigned')}}" label="Còn hạn" default-value="1"></x-quick-filter-btn>
    <x-quick-filter-btn :allParams="$allParams" name="status" value="{{config('common.status.proposals.assigned')}}" label="Quá hạn"></x-quick-filter-btn>
@endsection
@section('table-list')
    @include("partials.warning")
    <div class="table-responsive table-hover table-striped">
        <table class="table" id="dataTable">
            <thead>
                <tr>
                    {{--                <th><input type="checkbox" class="select_all"></th>--}}
                    <th>{{__('data_field_name.common_field.stt')}}</th>
                    <th>{{__('data_field_name.assignment.device')}}</th>
                    <th>{{__('data_field_name.common_field.category')}}</th>
                    <th>{{__('data_field_name.assignment.task_name')}}</th>
                    <th>{{__('data_field_name.common_field.start_date')}}</th>
                    <th>{{__('data_field_name.common_field.end_date')}}</th>
                    <th>{{__('data_field_name.common_field.address')}}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($data as $datum )
                <tr>
                    <td>{{ $loop->index + ($data->currentPage() - 1) * $data->perPage() +1 }}</td>
                    <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ optional($datum->device)->name }}">{{ optional($datum->device)->name }}</td>
                    <td>{{ optional(optional($datum->device)->category)->name }}</td>
                    <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ optional($datum->record_plan)->name }}">{{ optional($datum->record_plan)->name }}</td>
                    <td>{{ date("d-m-Y" , strtotime(optional($datum->record_plan)->start_time)) }}</td>
                    <td>{{ date("d-m-Y" , strtotime(optional($datum->record_plan)->end_time)) }}</td>
                    <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ optional($datum->record_plan)->address }}">{{ optional($datum->record_plan)->address }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(count($data) == 0)
            <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
        @endif
    </div>
@endsection
