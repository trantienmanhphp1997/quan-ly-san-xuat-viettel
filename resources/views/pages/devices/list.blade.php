@extends('layouts.list')

@section("buttons")

    @can('create', app($modelClass))
        @include("partials.text-btn.create-group-btn")
    @endcan
    @can("exportDataExpiredLendingDayDevices", app($modelClass))
        @if(isset($allParams['borrowed']) && $allParams['borrowed'] == 'borrowed')
            @include("partials.text-btn.export-excel-btn",["route"=> $routePrefix.".exportDataExpiredLendingDayDevices","params" => $allParams,"label" =>__('common.button.expiredLendingDayDevices')])
        @endif
    @endcan
    @can('export', app($modelClass))
        @if(!isset($allParams['borrowed']))
            @include("partials.text-btn.export-excel-btn",["route"=> $routePrefix.".exportdevice","params" => $allParams])
        @endif
    @endcan
    @if(!isset($allParams['requestReturn']))
    @can("deleteAny", app($modelClass))
        @include("partials.text-btn.report-broken",['disabled' => true])
    @endcan
    @can("restore", app($modelClass))
        @include("partials.text-btn.bulk-restore",['disabled' => true])
    @endcan
    @endif
@endsection

@section("sub-header-content")
    {{__('data_field_name.action.manage', ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
@endsection

@section("quick-filter")
    <div class="btn-group mr-1" role="group" aria-label="Button group"  >
        <x-quick-filter-btn :allParams="$allParams" name="status" value="" default-value="all" label="{{__('status.device.all')}} ({{$countInfo->has('total') ? $countInfo['total'] : 0}})"></x-quick-filter-btn>
        <x-quick-filter-btn :allParams="$allParams" name="status" value="1" label="{{__('status.device.active')}} ({{$countInfo->has(1) ? $countInfo[1] : 0}})"></x-quick-filter-btn>
        <x-quick-filter-btn :allParams="$allParams" name="borrowed" value="instock" label="{{__('status.device.instock')}} ({{$countInfo->has('total') && $lendingCount ? $countInfo['total'] - $lendingCount : 0}})"></x-quick-filter-btn>
        <x-quick-filter-btn :allParams="$allParams" name="borrowed" value="borrowed" label="{{__('status.device.borrowed')}} ({{$lendingCount ? $lendingCount : 0}})"></x-quick-filter-btn>
        <x-quick-filter-btn :allParams="$allParams" name="status" value="0" label="{{__('status.device.inactive')}} ({{$countInfo->has(0) ? $countInfo[0] : 0}})"></x-quick-filter-btn>


        {{--        <x-quick-filter-btn :allParams="$allParams" name="status" value="2" label="Busy"></x-quick-filter-btn>--}}
        <x-quick-filter-btn :allParams="$allParams" name="status" value="3" label="{{__('status.device.broken')}} ({{$countInfo->has(3) ? $countInfo[3] : 0}})"></x-quick-filter-btn>
        <x-quick-filter-btn :allParams="$allParams" name="requestReturn" value="requestReturn" label="{{__('status.device.request')}} ({{$requestReturn ? $requestReturn : 0}})"></x-quick-filter-btn>
        <input type="hidden" name="device_category_id" id="device_category_id" value="">
    </div>
@endsection

@section('table-list')
    @include("partials.warning")
    <?php
        //HÀM ĐỆ QUY HIỂN THỊ CATEGORIES
        function showCategories($categories, $parent_id = 0, $char = '')
        {
            //LẤY DANH SÁCH CATE CON
            $cateChild = [];
            foreach ($categories as $key => $category) {
                // Nếu là chuyên mục con thì hiển thị
                if ($category->parent_id == $parent_id) {
                    $cateChild[] = $category;
                    // Xóa chuyên mục đã lặp
                    unset($categories->key);
                }
            }
            if ($cateChild) {
                echo '<ul>';
                // dd($cateChild[1]);
                foreach ($cateChild as $key => $item) {
                    // Hiển thị tiêu đề chuyên mục - kèm theo id để xác định các node
                    $deviceInChild = 0;

                    array_walk_recursive($item->child, function($i) use (&$deviceInChild) {
                        $deviceInChild += count($i->devices);
                    });
                    
                    echo '<li data-jstree=\'{"icon":"glyphicon glyphicon-leaf"}\' id = "'.$item->id.'">' . $item->name. ' (<span class="text-primary">'.( $deviceInChild+count($item->devices)).'</span>)';
                    // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
                    showCategories($categories, $item->id, $char . '|---');
                    echo '</li>';
                }
                echo '</ul>';
            }
        }
        
    ?>
    <div class="d-flex">
        <div class="category-tree w-25">
            <h6>Danh mục thiết bị</h6>
            <div class="form-group row">
                <div class="col-md-11">
                    @if(isset($categories))
                    <div id="jstree">
                        <ul>
                            <li data-jstree='{"icon":"glyphicon glyphicon-leaf", "opened":true}'>Tất cả {{showCategories($categories)}}</li>
                        </ul>
                    </div>
                    @endif
                    <label id="category_list_error"></label>
                </div>
            </div>
        </div>
        <div class="list-device w-75">
            <div class="table-responsive">
            <table class="table table-hover table-striped" id="dataTable">
                <thead>
                    <tr>
                        <th>
                            <input type="checkbox" class="select_all">
                        </th>
                        <th>{{__('data_field_name.common_field.stt')}}</th>
                        <th colspan="2">{{__('data_field_name.common_field.name')}}</th>
                        <th>{{__('data_field_name.device.category')}}</th>
                        <th>{{__('data_field_name.common_field.status')}}</th>
                        <th>{{__('data_field_name.device.serial')}}</th>
                        @if(isset($allParams['borrowed']) && $allParams['borrowed'] == 'borrowed')
                        <th>{{__('data_field_name.assignment.task_name')}}</th>
                        <th>{{__('data_field_name.common_field.start_date')}}</th>
                        <th>{{__('data_field_name.common_field.end_date')}}</th>
                        @endif
                        <th>{{__('data_field_name.common_field.descriptions')}}</th>
                        <th>{{__('data_field_name.common_field.action')}}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($data as $datum)
                    <tr>
                        <td>
                            <input type="checkbox" name="row_id" id="checkbox_{{ $datum->id }}" value="{{ $datum->id }},{{$datum->status}}">
                        </td>
                        <td>{{ $loop->index + ($data->currentPage() - 1) * $data->perPage() +1 }}</td>
                        <td colspan="2" class="text-truncate" data-toggle="tooltip" data-placement="top" title="{{ $datum->name }}" style="max-width: 200px">{{$datum->name}}</td>
                        <td>{{@$datum->category->name}}</td>
                        <td>
                            @if($datum->status == 1)
                                <span class="h100 badge badge-success">{{__('status.device.active')}}</span>
                            @elseif($datum->status == 3)
                                <span class="h100 badge badge-danger">{{__('status.device.broken')}}</span>
                            @else
                                <span class="h100 badge badge-secondary">{{__('status.device.inactive')}}</span>
                            @endif
                        </td>
                        <td>{{$datum->serial}}</td>
                        @if(isset($allParams['borrowed']) && $allParams['borrowed'] == 'borrowed')
                            <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ count($datum->lendingPlans) > 0  ? $datum->lendingPlans[0]->name :'' }}">{{ count($datum->lendingPlans) > 0  ? $datum->lendingPlans[0]->name : ''}}</td>
                            <td>{{  date("d-m-Y " , strtotime(count($datum->lendingPlans) > 0  ? $datum->lendingPlans[0]->start_time : '')) }}</td>
                            <td>{{  date("d-m-Y " , strtotime(count($datum->lendingPlans) > 0  ?  $datum->lendingPlans[0]->end_time : '')) }}</td>
                        @endif
                        <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $datum->description }}">{{$datum->description}}</td>
                        <td>
                            <div class="d-flex">
                            @if(isset($allParams['requestReturn']))
                                @can("update", $datum)
                                    <a type="button" class="btn btn-sm btn-warning pull-right view text-decoration-none ml-1 no-wrap confirm-retrieve-btn" id="{{$datum}}">{{__('common.button.confirm')}}</a>
                                @endcan
                            @else
                                @include('partials.small-btn.common-action')
                            @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @if(count($data) == 0)
            <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
            @endif
            </div>
        </div>
    </div>
    @include('partials.modal.confirm-retrieve-device')
@endsection
@push('list-record-plan')
    <script src="{{ asset('js/device-services.js') }}"></script>
    <script>
        $('.confirm-retrieve-btn').click(function() {
                recordPlanId = JSON.parse($(this).attr('id')).request_return[0].id
                // console.log(recordPlanId);
                deviceId = JSON.parse($(this).attr('id')).id;
                // console.log(deviceId);
                $('#confirm-retrieve').modal('show');
            });

        //handle tree
        $(function () {
            var nodeSelected = null;

            //config jstree
            $('#jstree').jstree({
                "checkbox" : {
                    "keep_selected_style" : false,
                    "three_state": true,
                },
                "themes" : { "stripes" : true },
                "plugins":["checkbox","wholerow","themes"]
            });

            //set checked node after load data
            var selected = "{{isset($allParams['device_category_id']) ? $allParams['device_category_id'] : ''}}";
            $('#jstree').jstree("check_node", selected.split(','), true);

            //change state note
            $('#jstree').on("changed.jstree", function (e, data) {
                var selected = [];
                for (i = 0; i < data.selected.length; i++) {
                    selected = selected.concat($('#jstree').jstree(true).get_selected(data.selected[i].id));
                    //first false=I want an array; second true=I want only IDs
                }
                selected = selected.unique();

                $('#device_category_id').val(selected);
                // $('#quick-search-form').submit();
            });

            // submit form
            $('#jstree').on('activate_node.jstree', function (e, data) {
                $('#quick-search-form').submit()
            });

            Array.prototype.unique = function () {
                return Array.from(new Set(this));
            }
        });

    </script>
@endpush
