@extends('layouts.detail')

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex" style="font-weight: bold;">
                            <div>
                                <i class="fa fa-align-justify mr-2"></i>
                            </div>
                            <strong>
                                @if(!isset($data))
                                    {{__("data_field_name.action.create", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @elseif(isset($editable) && $editable == true)
                                    {{__("data_field_name.action.edit", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @else
                                    {{__("data_field_name.action.show", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @endif
                            </strong>
                        </div>
                        <div class="card-body">
                            <br>
                            <form method="POST" novalidate
                                  action=" {{ (!isset($data)) ? route($routePrefix.".store") : route($routePrefix.".update", ["id" => $data->id])}}"
                                  id="detail-form">
                                @csrf
                                @if(isset($editable) && $editable == true && isset($data))
                                    @method('PUT')
                                @endif

                                <x-input type="text" label="{{__('data_field_name.common_field.title')}}" form-name="name" value="{{isset($data) ? $data->name : ''}}" required=true disabled="{{!$editable}}" placeholder="" />
                                <x-input type="text" label="{{__('data_field_name.common_field.content')}}" form-name="content" value="{{isset($data) ? $data->content : ''}}" required=true disabled="{{!$editable}}" placeholder="" />



                                <x-textarea label="{{__('data_field_name.common_field.descriptions')}}" form-name="description" value="{{isset($data) ? $data->description : ''}}" disabled="{{!$editable}}" placeholder=""/>
                                <input type="hidden" name="link" id="link">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        @include("partials.common-detail-action", ["back_params" =>
                                        ["is_active" => isset($data) ? ($data->deleted_at ? -1 : 1) : (isset($is_active) ? $is_active : 1)]
                                        ])
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('validation-rules')
    department_id: "required",
    statement_id: "required",
@endpush

@push('validation-messages')
    department_id: {
        required: "{{__('client_validation.form.required.contract.department_id')}}",
    },
    statement_id: {
        required:"{{__('client_validation.form.required.contract.statement_id')}}",
    }
@endpush

@section('javascript')
    @include("partials.message")
    <script>
        $(document).ready(function (){
            $("#department_list").change(function (){
                var department_id = $("#department_list").val();
                if(department_id){
                    removeStatements("statement_list")
                    getStatementsByDepartmentId(department_id)
                }else{
                    removeStatements("statement_list")
                }
            });

            function getStatementsByDepartmentId(department_id){
                axios.post("/statements/get-data-by-department-id", {
                    department_id: department_id
                }).then(res => {
                    var $data = res.data;
                    var $select_statement_id = $("#statement_list")
                    $data.forEach($statement => {
                        $select_statement_id.append(`
                        <option value="${$statement.id}">${$statement.title}</option>
                        `)
                    })
                }).catch(function (error) {
                    console.log(error);
                })
            }

            function removeStatements(el){
                $(`#${el}`).children().remove();
                $(`#${el}`).append(
                    `<option value="">{{__('data_field_name.action.select', ["dataName" => __('data_field_name.model_name.statements')])}}</option>`
                )
            }
        })
    </script>
@endsection
