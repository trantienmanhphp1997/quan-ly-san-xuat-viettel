@extends('layouts.list')

@section("buttons")
    @can('create', app($modelClass))
        @include("partials.text-btn.create-group-btn")
    @endcan
@endsection

@section("sub-header-content")
    {{__('data_field_name.action.manage', ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
@endsection

@section('table-list')
    <div class="table-responsive">
        <table class="table" id="dataTable">
            <thead>
            <tr>
                <th>
                    <input type="checkbox" class="select_all">
                </th>
                <th>{{__('data_field_name.common_field.stt')}}</th>
                <th>{{__('data_field_name.common_field.name')}}</th>
                <th>{{__('data_field_name.common_field.code')}}</th>
                <th>{{__('data_field_name.common_field.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $datum)
                <tr>
                    <td>
                        <input type="checkbox" name="row_id" id="checkbox_{{ $datum->id }}" value="{{ $datum->id }}">
                    </td>
                    <td>{{ $loop->index + ($data->currentPage() - 1) * $data->perPage() +1 }}</td>
                    <td><a href="{{route("managers.index", ["department_code" => $datum->code])}}" class="text-decoration-none">{{ $datum->name }}</a></td>
                    <td>{{ $datum->code }}</td>
                    <td>
                        <div class="d-flex">
                            @include('partials.small-btn.common-action')
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
