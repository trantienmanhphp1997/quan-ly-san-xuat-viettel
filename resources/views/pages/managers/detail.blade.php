@extends('layouts.detail')

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex" style="font-weight: bold;">
                            <div>
                                <i class="fa fa-align-justify mr-2"></i>
                            </div>

                            <strong>
                                @if(!isset($data))
                                    {{__("data_field_name.action.create", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @elseif(isset($editable) && $editable == true)
                                    {{__("data_field_name.action.edit", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @else
                                    {{__("data_field_name.action.show", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @endif
                            </strong>
                        </div>
                        <div class="card-body">
                            <br>
                            <form method="POST" novalidate
                                  action=" {{ (!isset($data)) ? route("managers.store") : route("managers.update", ["id" => $data->id])}}"
                                  id="detail-form">
                                @csrf
                                @if(isset($editable) && $editable == true && isset($data))
                                    @method('PUT')
                                @endif

                                <x-input type="text"  label="{{__('data_field_name.user.user_name')}}" form-name="username" value="{{isset($data) ? $data->username : ''}}" required=true disabled="{{!$editable}}" placeholder=""/>
                                @if(isset($editable) && $editable == true && !isset($data))
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">
                                        {{__('data_field_name.user.password')}}
                                        <span class="text-danger">(*)</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="password" value="" required=true  placeholder="{{__('common.place_holder.password')}}" id="password-field">
                                        <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        @error("password")
                                        <label class="error">
                                            {{ $message }}
                                        </label>
                                        @enderror
                                    </div>
                                </div>
                                @endif

                                @if(isset($editable) && $editable == true)
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">{{__('data_field_name.user.group')}}<span
                                                class="text-danger">(*)</span></label>
                                        <div class="col-md-9">
                                            <select name="department_code" id="group_list" class="form-control">
                                                @foreach($departments as $department)
                                                    <option value="{{ $department->code }}"
                                                        {{isset($data) && $department->code == $data->department_code ? "selected" : "" }}
                                                    >{{ $department->name }}</option>
                                                @endforeach
                                            </select>
                                            <label id="group_list-error" class="error"></label>
                                        </div>
                                    </div>
                                @else
                                    <x-input type="text"  label="{{__('data_field_name.user.group')}}" form-name="department_code" value="{{isset($data) ? $data->department->name : ''}}" required=true disabled="true" placeholder=""/>
                                @endif

                                <x-input type="text"  label="{{__('data_field_name.user.full_name')}}" form-name="full_name" value="{{isset($data) ? $data->full_name : ''}}" required=true disabled="{{!$editable}}" placeholder=""/>
                                <x-input type="text"  label="{{__('data_field_name.common_field.phone_full')}}" form-name="phone_number" value="{{isset($data) ? $data->phone_number : ''}}" required=true disabled="{{!$editable}}" placeholder=""/>
                                <x-input type="text"  label="{{__('data_field_name.common_field.email')}}" form-name="email" value="{{isset($data) ? $data->email : ''}}" required=false disabled="{{!$editable}}" placeholder=""/>
                                <x-textarea label="{{__('data_field_name.common_field.note')}}" form-name="note" value="{{isset($data) ? $data->note : ''}}"  disabled="{{!$editable}}" placeholder=""/>

                                @if(isset($editable) && $editable == true && isset($data))
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label"></label>
                                    <div class="col-md-9">
                                        <a class="btn btn-seconary" id="btn-update-pass"><i class="fas fa-key mr-2"></i>{{__('data_field_name.user.update-password')}}</a>
                                    </div>
                                </div>

                                <div class="form-group row" id="password-area">
                                    <label class="col-md-3 col-form-label">
                                        {{__('data_field_name.user.password')}}
                                        <span class="text-danger">(*)</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="password" value="" required=true  placeholder="{{__('common.place_holder.password')}}" id="password-field">
                                        <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                    </div>
                                </div>
                                @endif

                                <input type="hidden" name="is_manager" value="1">
                                <input type="hidden" name="link" id="link">

                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        @include("partials.common-detail-action", ["back_params" =>
                                        [/*"department_code" => isset($data) ? $data->department_code : (isset($department_code) ? $department_code : auth()->user()->department_code) ,*/ "is_active" => isset($data) ? ($data->deleted_at ? -1 : 1) : 1]
                                        ])
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('validation-rules')
    department_code: "required",
    password : "required",
    phone_number: {
    phoneNumber: true,
    required: true
    },
@endpush

@push('validation-messages')
    department_code: {
        required: "{{__('client_validation.form.required.manager.department_code')}}",
    },
    password:  {
        required: "{{__('client_validation.form.required.manager.password')}}",
    },
    phone_number: {
    phoneNumber:'{{__("client_validation.form.format.phone_number")}}',
    required: '{{__("client_validation.form.required.phone_number")}}'
    }
@endpush

@section('javascript')
    @include("partials.message")
    <script>
        $('#group_list-error').hide();

        $('#password-area').hide();
        //show/hide password
        $("#btn-update-pass").click(function() {
            $('#password-area').toggle();
        });
        $(".toggle-password").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>

@endsection
