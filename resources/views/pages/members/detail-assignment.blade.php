@extends('layouts.list')

@section("buttons")
    @include("partials.text-btn.export-excel-btn",[
        "route" => "$prefix". '.export-assignments',
        "params" =>  array_merge([ "member_id" =>  $member->id], isset($allParams) ? $allParams : [] )
    ])
@endsection

@section("sub-header-content")
    {{"Danh sách công việc của $member->full_name "}}
@endsection

@section('table-list')
    <div class="table-responsive table-hover">
        <table class="table" id="dataTable">
            <thead>
            <tr>
                {{--                <th>--}}
                {{--                    <input type="checkbox" class="select_all">--}}
                {{--                </th>--}}
                <th>{{__('data_field_name.common_field.stt')}}</th>
                <th>{{__('data_field_name.common_field.name')}}</th>
                <th>{{__('data_field_name.common_field.start_time')}}</th>
                <th>{{__('data_field_name.common_field.end_time')}}</th>
                <th>{{__('data_field_name.common_field.address')}}</th>
                <th>{{__('data_field_name.common_field.status')}}</th>
                <!-- <th>{{__('data_field_name.common_field.action')}}</th> -->
            </tr>
            </thead>
            <tbody>
            @foreach($data as $datum )
                <tr>
                    {{--                    <td>--}}
                    {{--                        <input type="checkbox" name="row_id" id="checkbox_{{ $datum->id }}" value="{{ $datum->id }}">--}}
                    {{--                    </td>--}}
                    <td>{{ $loop->index + ($data->currentPage() - 1) * $data->perPage() +1 }}</td>
                    <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $datum->record_plan->name }}">{{ $datum->record_plan->name }}</td>
                    <td>{{ date("d-m-Y H:i" , strtotime($datum->record_plan->start_time)) }}</td>
                    <td>{{ date("d-m-Y H:i" , strtotime($datum->record_plan->end_time)) }}</td>
                    <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $datum->record_plan->address }}">{{ $datum->record_plan->actual_address }}</td>
                    <td>
                        <x-status-span status="{{$datum->record_plan->status}}" data-type="record_plan" :danger="[-1]" :secondary="[0]" :warning="[1,3]" :success="[2,4]" :primary="[5]"></x-status-span>
                    </td>
                    <!-- <td>
                    @can("canUpdateApprovedRecordPlan", $datum)
                        @include("partials.small-btn.delete-btn", ["route" => "assignment.$prefix". '.destroy', "params" => ['record_plan_id' => $datum->record_plan->id, "member_ids" => $datum->member->id, "department_code" => $prefix] ])
                    @endcan
                    </td> -->
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(count($data) == 0)
        <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
        @endif
    </div>
@endsection
