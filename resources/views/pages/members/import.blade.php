@extends('layouts.master')

@section('content')
    <div class="card">
        @if (isset($failures))
            <div class="alert alert-danger" role="alert">
                <strong>{{__("common.error")}}:</strong>

                <ul>
                    @foreach ($failures as $failure)
                        <li> {{ "---- ".__("common.excel.row"). ' ' . $failure->row(). ' ' .__("common.excel.column").' ' .($failure->attribute() + 1) }}
                            @foreach($failure->errors() as $error)
                                {{$error}}
                            @endforeach
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card-body">
            <div class="w-50 m-auto">
                <form action="{{route($routePrefix.".store-import")}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">File <span class="text-danger">(*)</span></label>
                        <div class="col-md-9">
                            <input type="file" name="members">
                            <div class="error">
                                @error("members")
                                {{$message}}
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class=" offset-3 col-md-9 d-flex">
                            <button type="submit" class="btn btn-success">{{__("common.button.import")}}</button>
                            @include("partials.back-btn", ["route" => "$routePrefix.index", "params" => []])
                        </div>
                    </div>
                </form>


            </div>

        </div>

    </div>
@endsection
@include("partials.message")
