@extends('layouts.list')

@section("buttons")
    {{--        @can('export', app($modelClass))--}}
    @include("partials.text-btn.export-excel-btn",[
        "route" => "$prefix". '.export-assignments',
        "params" => isset($department_code) ? array_merge([ "department_code" =>  $department_code], isset($allParams) ? $allParams : [] ) : []
    ])
    {{--        @endcan--}}
@endsection
@section("sub-header-content")
    {{__('data_field_name.action.view_assignment', ["dataName" => __('data_field_name.model_name.'.$department_code)])}}
@endsection

@section('table-list')
    <div class="table-responsive table-hover table-striped">
        <table class="table" id="dataTable">
            <thead>
            <tr>
                {{--                <th><input type="checkbox" class="select_all"></th>--}}
                <th>{{__('data_field_name.common_field.stt')}}</th>
                <th>{{__('data_field_name.assignment.member_name')}}</th>
                <th>{{__('data_field_name.common_field.phone')}}</th>
                <th>{{__('data_field_name.assignment.task_name')}}</th>
                <th>{{__('data_field_name.common_field.start_time')}}</th>
                <th>{{__('data_field_name.common_field.end_time')}}</th>
                <th>{{__('data_field_name.common_field.address')}}</th>
                <th>{{__('data_field_name.common_field.status')}}</th>
                <!-- <th>{{__('data_field_name.common_field.action')}}</th> -->
            </tr>
            </thead>
            <tbody>
            @foreach($data as $datum )

                <tr>
                    {{--                    <td>--}}
                    {{--                        <input type="checkbox" name="row_id" id="checkbox_{{ $datum->id }}"--}}
                    {{--                               value="{{ $datum->id }}">--}}
                    {{--                    </td>--}}
                    <td>{{ $loop->index + ($data->currentPage() - 1) * $data->perPage() +1 }}</td>
                    <td>{{ optional($datum->member)->full_name }}</td>
                    <td>{{ optional($datum->member)->phone_number }}</td>
                    <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ optional($datum->record_plan)->name }}">{{ optional($datum->record_plan)->name }}</td>
                    <td>{{  date("d-m-Y H:i" , strtotime($datum->start_time)) }}</td>
                    <td>{{  date("d-m-Y H:i" , strtotime($datum->end_time)) }}</td>
                    <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ optional($datum->record_plan)->address }}">{{ optional($datum->record_plan)->actual_address }}</td>
                    <td>
                        <x-status-span status="{{$datum->record_plan->status}}" data-type="record_plan" :danger="[-1]" :secondary="[0]" :warning="[1,3]" :success="[2,4]" :primary="[5]"></x-status-span>
                    </td>
                    <!-- <td>
                    @can("canUpdateApprovedRecordPlan", $datum)
                        @if(($datum->record_plan) && $datum->member &&
                            ($datum->record_plan->status != config("common.status.record_plan.in_progress") && $datum->record_plan->status != config("common.status.record_plan.wrap") && $datum->record_plan->status != config("common.status.record_plan.done")))
                            @include("partials.small-btn.delete-btn", ["route" => "assignment.$prefix". '.destroy', "params" => ['record_plan_id' => $datum->record_plan->id, "member_ids" => $datum->member->id, "department_code" => $prefix] ])
                        @endif
                    @endcan
                    </td> -->
                </tr>

            @endforeach
            </tbody>
        </table>
        @if(count($data) == 0)
            <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
        @endif
    </div>
@endsection
