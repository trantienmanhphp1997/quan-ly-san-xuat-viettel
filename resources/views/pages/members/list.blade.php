@extends('layouts.list')

@section("buttons")
    @can('create', app($modelClass))
        @include("partials.text-btn.create-group-btn")
    @endcan
    @can('export', app($modelClass))
        @include("partials.text-btn.export-excel-btn",["params" => $allParams])
    @endcan
@endsection

@section("sub-header-content")
    {{__('data_field_name.action.manage', ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
@endsection

@section('table-list')
    @include("partials.warning")
    <div class="table-responsive table-hover table-striped">
        <table class="table" id="dataTable">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" class="select_all">
                    </th>
                    <th>{{__('data_field_name.common_field.stt')}}</th>
                    <th>{{__('data_field_name.user.full_name')}}</th>
                    <th>{{__('data_field_name.common_field.phone')}}</th>
                    <th>{{__('data_field_name.common_field.email')}}</th>
                    @if(auth()->user()->hasRole("super admin") || strpos(auth()->user()->department_code, "production") !== false)
                        <th>{{__('data_field_name.department.name')}}</th>
                    @endif
                    @if($department_code == config("common.departments.transportation"))
                        @can("manager_driver")
                            <th>{{__('data_field_name.car.license_plate')}}</th>
                            <th>{{__('data_field_name.car.num_of_seat')}}</th>
                        @endcan
                    @endif
                    <th>{{__('data_field_name.common_field.action')}}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($data as $datum)
                <tr>
                    <td>
                        @if($datum->is_manager ==0)
                        <input type="checkbox" name="row_id" id="checkbox_{{ $datum->id }}"
                               value="{{ $datum->id }},{{$datum->status}}">
                        @endif
                    </td>
                    <td>{{ $loop->index + ($data->currentPage() - 1) * $data->perPage() +1 }}</td>
                    <td>{{ $datum->full_name }}</td>
                    <td>{{ $datum->phone_number }}</td>
                    <td>{{ $datum->email }}</td>
                    @if(auth()->user()->hasRole("super admin") || strpos(auth()->user()->department_code, "production") !== false)
                        <td>{{ $datum->department->name }}</td>
                    @endif
                    @if($department_code == "transportation")
                        @can("manager_driver")
                            @if($datum->car)
                                <td><a class="action" href="{{route("cars.show", ["id" => optional($datum->car)->id])}}">{{ optional($datum->car)->license_plate }}</a></td>
                                <td><a class="action" href="{{route("cars.show", ["id" => optional($datum->car)->id])}}">{{ optional($datum->car)->number_of_seats }}</a></td>
                            @else
                                <td></td>
                                <td></td>
                            @endif
                        @endcan
                    @endif
                    <td>
                        <div class="d-flex">
                            @if($datum->is_manager == 0)
                                @include('partials.small-btn.common-action')
                            @else
                            <a href='{{route($routePrefix. '.show',['id' => $datum->id])}}' type="button" class="btn btn-default btn-sm action rounded-1 mr-72" data-toggle="tooltip" data-placement="top" title="{{ __('common.button.view') }}"><i class="fas fa-eye"></i></a>
                            @endif
                            @include('partials.small-btn.member-view-assignments-btn')
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(count($data) == 0)
        <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
        @endif
    </div>
@endsection
