@extends('layouts.list')

@section("buttons")
    @can('create', app($modelClass))
        @include("partials.text-btn.create-group-btn")
    @endcan
@endsection

@section("sub-header-content")
    {{__("menu_management.subheader")}}
@endsection

@section('table-list')
    <div class="table-responsive">
        <table class="table" id="dataTable">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" class="select_all">
                    </th>
                    <th>{{__("menu_management.table_column.name")}}</th>
                    <th>{{__("menu_management.table_column.href")}}</th>
                    <th>{{__("menu_management.table_column.parent")}}</th>
                    <th>{{__("menu_management.table_column.actions")}}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($data as $datum)
                <tr>
                    <td>
                        <input type="checkbox" name="row_id" id="checkbox_{{ $datum->id }}" value="{{ $datum->id }}">
                    </td>
                    <td><a href="{{route("menus.index", ["department_code" => $datum->id])}}" class="text-decoration-none">{{ $datum->name }}</a></td>
                    <td>{{ $datum->href }}</td>
                    <td>{{ isset($datum->parent) ? $datum->parent->name : "" }}</td>
                    <td>
                        <div class="d-flex">
                            @include('partials.small-btn.common-action')
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(count($data) == 0)
        <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
        @endif
    </div>
@endsection
