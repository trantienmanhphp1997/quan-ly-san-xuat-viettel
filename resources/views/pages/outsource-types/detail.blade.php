@extends('layouts.detail')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex" style="font-weight: bold;">
                            <div>
                                <i class="fa fa-align-justify mr-2"></i>
                            </div>

                            <strong>
                                @if(!isset($data))
                                    {{__("data_field_name.action.create", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @elseif(isset($editable) && $editable == true)
                                    {{__("data_field_name.action.edit", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @else
                                    {{__("data_field_name.action.show", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @endif
                            </strong>
                        </div>
                        <div class="card-body">
                            <br>
                            <form method="POST" novalidate
                                  action=" {{ (!isset($data)) ? route($routePrefix.".store") : route($routePrefix.".update", ["id" => $data->id])}}"
                                  id="detail-form">
                                @csrf
                                @if(isset($editable) && $editable == true && isset($data))
                                    @method('PUT')
                                @endif

                                <x-input type="text" label="{{__('data_field_name.common_field.name')}}" form-name="name" value="{{isset($data) ? $data->name : ''}}" required=true disabled="{{!$editable}}" placeholder=""/>
                                <x-input type="text" label="{{__('data_field_name.common_field.code')}}" form-name="code" value="{{isset($data) ? $data->code : ''}}" required=true disabled="{{!$editable}}" placeholder=""/>

                                @if(!auth()->user()->hasRole('super admin') && strpos(auth()->user()->department_code, "production") === false)
                                    <input type="hidden" value="{{auth()->user()->department_code}}" name="department_code">
                                @else
                                    @if(isset($editable) && $editable == true)
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">{{__('data_field_name.model_name.department')}}<span
                                                    class="text-danger">(*)</span></label>
                                            <div class="col-md-9">
                                                <select name="department_code" id="group_list" class="form-control">
                                                    @foreach($departments as $department)
                                                        <option value="{{ $department->code }}"
                                                            {{(isset($data) && $department->code == $data->department_code ) || (auth()->user()->department_code  == $department->code)? "selected" : "" }}
                                                        >{{ $department->name }}</option>
                                                    @endforeach
                                                </select>
                                                <label id="group_list-error" class="error"></label>
                                            </div>
                                        </div>
                                    @else
                                        <x-input type="text" label="{{__('data_field_name.user.group')}}" form-name="department_code" value="{{isset($data) ? $data->department->name : ''}}" required=true disabled="true" placeholder=""/>
                                    @endif
                                @endif

                               <x-textarea label="{{__('data_field_name.common_field.descriptions')}}" form-name="description" value="{{isset($data) ? $data->description : ''}}" disabled="{{!$editable}}" placeholder=""/>
                               <input type="hidden" name="link" id="link">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        @include("partials.common-detail-action", ["back_params" =>
                                        ["is_active" => isset($data) ? ($data->deleted_at ? -1 : 1) : (isset($is_active) ? $is_active : 1)]
                                        ])
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
    @include("partials.message")
@endsection
