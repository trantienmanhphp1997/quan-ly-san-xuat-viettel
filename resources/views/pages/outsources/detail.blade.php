@extends('layouts.detail')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex" style="font-weight: bold;">
                            <div>
                                <i class="fa fa-align-justify mr-2"></i>
                            </div>

                            <strong>
                                @if(!isset($data))
                                    {{__("data_field_name.action.create", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @elseif(isset($editable) && $editable == true)
                                    {{__("data_field_name.action.edit", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @else
                                    {{__("data_field_name.action.show", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @endif
                            </strong>
                        </div>
                        <div class="card-body">
                            <br>
                            <form method="POST" novalidate
                                  action=" {{ (!isset($data)) ? route($routePrefix.".store") : route($routePrefix.".update", ["id" => $data->id])}}"
                                  id="detail-form">
                                @csrf
                                @if(isset($editable) && $editable == true && isset($data))
                                    @method('PUT')
                                @endif

                                <x-input type="text" label="{{__('data_field_name.user.full_name')}}" form-name="full_name" value="{{isset($data) ? $data->full_name : ''}}" required=true disabled="{{!$editable}}" placeholder=""/>
                                <x-input type="text" label="{{__('data_field_name.common_field.phone_full')}}" form-name="phone_number" value="{{isset($data) ? $data->phone_number : ''}}" required=true disabled="{{!$editable}}" placeholder=""/>

                                @if(isset($editable) && $editable == true)
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">{{__('data_field_name.outsource.type')}}<span class="text-danger">(*)</span></label>
                                        <div class="col-md-9">
                                            <select name="outsource_type_id" id="outsource_type_list" class="form-control" required>
                                                <option value="">{{__('data_field_name.action.select', ["dataName" => __('data_field_name.model_name.outsource-types')])}}</option>
                                                @if(isset($outsource_types))
                                                    @foreach($outsource_types as $outsource_type)
                                                        <option value="{{ $outsource_type->id }}" {{ (isset($data) && $outsource_type->id == $data->outsource_type_id) || (old("outsource_type_id") == $outsource_type->id ) ? "selected" : "" }}>{{ $outsource_type->name }}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                            <label id="outsource_type_list-error"></label>
                                        </div>
                                    </div>
                                @else
                                    <x-input type="text" label="{{__('data_field_name.outsource.type')}}" form-name="outsource_type_id" value="{{isset($data) ? $data->outsource_type->name : ''}}" required=true disabled="true" placeholder=""/>
                                @endif

                                <x-input type="email" label="{{__('data_field_name.common_field.email')}}" form-name="email" value="{{isset($data) ? $data->email : ''}}" required=false disabled="{{!$editable}}" placeholder=""/>
                                <x-input type="text" label="{{__('data_field_name.common_field.address')}}" form-name="address" value="{{isset($data) ? $data->address : ''}}" required=false disabled="{{!$editable}}" placeholder=""/>
                                <x-textarea label="{{__('data_field_name.common_field.descriptions')}}" form-name="description" value="{{isset($data) ? $data->description : ''}}" disabled="{{!$editable}}" placeholder=""/>
                                <input type="hidden" name="link" id="link">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        @include("partials.common-detail-action", ["back_params" =>
                                        ["is_active" => isset($data) ? ($data->deleted_at ? -1 : 1) : (isset($is_active) ? $is_active : 1)]
                                        ])
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('validation-rules')
    outsource_type_id: "required",
    phone_number: {
    phoneNumber: true,
    required: true
    },
@endpush

@push('validation-messages')
    outsource_type_id: {
    required: "{{__('client_validation.form.required.outsource.type')}}",
    },
    phone_number: {
    phoneNumber:'{{__("client_validation.form.format.phone_number")}}',
    required: '{{__("client_validation.form.required.phone_number")}}'
    }
@endpush

@section('javascript')
    @include("partials.message")
    <script>
        $("#outsource_type_list").on("select2:close", function (e) {
            $(this).valid();
        });
        $('#outsource_type_list-error').hide();


    </script>

@endsection
