@extends('layouts.master')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <form class="card mt-4" action="{{route("permissions.access.save")}}" method="POST">
                        @csrf
                        @method("PUT")
                        <div class="card-header">
                            <strong>
                                {{__("permission.access.header")}}
                            </strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6">
                                    <form action="{{route("permissions.access.save")}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <strong>{{__('permission.access.menus.available')}}</strong>
                                        <input type="hidden" value="{{$role->id}}" name="roleId"/>
                                        <input type="hidden" value="{{$departmentId}}" name="departmentId"/>
                                        <ul>
                                            @foreach($availableMenus as $menu)
                                                <li class>
                                                    <input type="checkbox" class="form-check-input" name="menu[]"
                                                           value={{ $menu->id }} data-menu-type="used"
                                                           data-menu-level="parent">
                                                    <label for="" class="form-check-label">{{__("menu_management.menu_name.".$menu->menu_code) }}</label>
                                                    <ul>
                                                        @isset($menu->children)
                                                            @foreach($menu->children as $subMenu)
                                                                <li>
                                                                    <input type="checkbox" class="form-check-input"
                                                                           name="menu[]"
                                                                           value={{ $subMenu->id }} data-menu-type="used"
                                                                           data-menu-level="child"
                                                                           data-parent-id={{ $menu->id }}>
                                                                    <label for=""
                                                                           class="form-check-label">{{ __("menu_management.menu_name.".$subMenu->menu_code) }}</label>
                                                                </li>
                                                            @endforeach
                                                        @endisset
                                                    </ul>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <button class="btn btn-success"
                                                id="add-menus">{{__('permission.access.menus.add')}}</button>
                                    </form>
                                </div>

                                <div class="col-6">
                                    <form action="{{route("permissions.access.remove")}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <strong>{{__('permission.access.menus.selected')}}</strong>
                                        <input type="hidden" value="{{$role->id}}" name="roleId"/>
                                        <input type="hidden" value="{{$departmentId}}" name="departmentId"/>
                                        <ul>
                                            @foreach($selectedMenus as $menu)
                                                <li>
                                                    <input type="checkbox" class="form-check-input" name="menu[]"
                                                           value={{ $menu->id }} data-menu-type="unused"
                                                           data-menu-level="parent">
                                                    <label for=""
                                                           class="form-check-label">{{ __("menu_management.menu_name.".$menu->menu_code) }}</label>
                                                    <ul>
                                                        @isset($menu->children)

                                                            @foreach($menu->children as $subMenu)
                                                                <li>
                                                                    <input type="checkbox" class="form-check-input"
                                                                           name="menu[{{ $menu->id }}][]"
                                                                           value={{ $subMenu->id }} data-menu-type="unused"
                                                                           data-menu-level="child"
                                                                           data-parent-id={{ $menu->id }}>
                                                                    <label for=""
                                                                           class="form-check-label">{{ __("menu_management.menu_name.".$subMenu->menu_code) }}</label>
                                                                </li>
                                                            @endforeach
                                                        @endisset
                                                    </ul>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <button class="btn btn-danger"
                                                id="remove-menus">{{__('permission.access.menus.remove')}}</button>
                                    </form>
                                </div>
                            </div>

                            <div class=" mt-2">
                                @include("partials.back-btn", ["route" => "departments.index", "params" => []])
                                {{--                                <button class="btn btn-primary">{{__("common.button.back")}}</button>--}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
    @include("partials.message")
@endsection
@push("custom-scripts")
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            // Add onChange event for checkboxes
            const elements = getNodeArr('.card-body input[type=checkbox]');
            elements.forEach(el => el.addEventListener('change', (evt) => {

                if (evt.target.dataset['menuLevel'] === "parent") {

                    const parentCheckStatus = evt.target.checked;
                    const children = getNodeArr(`[data-parent-id="${evt.target.value}"]`, evt.target.closest('ul'));
                    children.forEach(child => child.checked = parentCheckStatus);

                } else {

                    const parent = evt.target.closest('ul').parentNode.querySelector('input[type=checkbox]');
                    const siblings = getNodeArr(`[data-parent-id="${evt.target.dataset['parentId']}"]`, evt.target.closest('ul'));
                    const checkStatus = siblings.map(check => check.checked);
                    const every = checkStatus.every(Boolean);
                    const some = checkStatus.some(Boolean);
                    parent.checked = every;
                    parent.indeterminate = !every && every !== some;

                }
            }))

            // On submit form
            // document.getElementById('add-menus').addEventListener('click', (evt) => {
            //   evt.preventDefault();
            //   const button = evt.target;
            //   console.log(button.parentNode)
            // })
        })

        const getNodeArr = (selector, parent = document) => {
            return [].slice.call(parent.querySelectorAll(selector));
        }
    </script>
@endpush
