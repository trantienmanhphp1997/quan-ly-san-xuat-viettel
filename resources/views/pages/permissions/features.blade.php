@extends('layouts.master')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <form class="card mt-4" action="{{route("permissions.features.save")}}" method="POST">
                        @csrf
                        @method("PUT")
                        <div class="card-header">
                            <strong>
                                {{__("permission.features.header")}}
                            </strong>
                        </div>
                        <div class="card-body">
                            <form action="{{route("permissions.features.save")}}" method="POST">
                                @method("PUT")
                                @csrf
                                <div class="row">
                                    <input type="hidden" name="departmentId" value="{{$departmentId}}">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th rowspan="2">{{__("permission.features.table.menus")}}</th>
                                            <th colspan="{{count(config("menu_permissions.feature_permissions")) + 1}}"
                                                class="text-center">
                                                {{__("permission.features.table.feature_permissions")}}
                                            </th>
                                        </tr>
                                        <tr>
                                            @foreach(config("menu_permissions.feature_permissions") as $permission)
                                                <th class="text-center">
                                                    {{__("permission.features.table.".$permission)}}
                                                </th>
                                            @endforeach
                                            <th class="text-center">
                                                {{__("permission.features.table.all")}}
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($menus as $menu)
                                            <tr>
                                                <td>{{__("menu_management.menu_name.".$menu->menu_code)}}</td>
                                                @foreach(config("menu_permissions.feature_permissions") as $permission)
                                                    <td class="text-center">
                                                        <input type="checkbox"
                                                               name="permissions[{{$menu->id}}][]"
                                                               data-permission="menu-{{$menu->id}}"
                                                               value="{{$permission . '_' . $menu->menu_code}}"
                                                            {{in_array($permission . '_' . $menu->menu_code, $currentPermissions) ? "checked" : ""}}
                                                        />
                                                    </td>
                                                @endforeach
                                                <td class="text-center">
                                                    <input type="checkbox"
                                                           class="select-all-btn"
                                                           data-menu="menu-{{$menu->id}}"
                                                    />
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr></tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-left mt-2">
                                    @include("partials.back-btn", ["route" => "departments.index", "params" => []])
                                    <button class="btn btn-success">{{__("common.button.save")}}</button>
                                </div>
                            </form>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push("custom-scripts")
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            // Add onChange event for checkboxes
            const elements = getNodeArr('.card-body input[type=checkbox]');
            $.each(elements, function(index, item){
                if (item.hasAttribute('data-menu')) {
                    const siblings = $("input[data-permission="+$(item).data('menu')+"]");
                    const siblingschecked = $("input[data-permission="+$(item).data('menu')+"]:checked");
                    item.checked = (siblings.length == siblingschecked.length);
                }
            });
            elements.forEach(el => el.addEventListener('change', (evt) => {

                if (evt.target.dataset['menu']) {
                    const parentCheckStatus = evt.target.checked;
                    const children = getNodeArr(`[data-permission="${evt.target.dataset['menu']}"]`, evt.target.closest('tr'));
                    children.forEach(child => child.checked = parentCheckStatus);
                } else if (evt.target.dataset['permission']) {
                    const parent = evt.target.closest('tr').querySelector(`[data-menu="${evt.target.dataset['permission']}"]`);
                    const siblings = getNodeArr(`[data-permission="${evt.target.dataset['permission']}"]`, evt.target.closest('tr'));
                    const checkStatus = siblings.map(check => check.checked);
                    const every = checkStatus.every(Boolean);
                    const some = checkStatus.some(Boolean);
                    parent.checked = every;
                    parent.indeterminate = !every && every !== some;

                }
            }))

        })

        const getNodeArr = (selector, parent = document) => {
            return [].slice.call(parent.querySelectorAll(selector));
        }
    </script>
@endpush
