@extends('layouts.detail')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex" style="font-weight: bold;">
                            <div>
                                <i class="fa fa-align-justify mr-2"></i>
                            </div>

                            <strong>
                                @if(!isset($data))
                                    {{__("data_field_name.action.create", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @elseif(isset($editable) && $editable == true)
                                    {{__("data_field_name.action.edit", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @else
                                    {{__("data_field_name.action.show", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @endif
                            </strong>
                        </div>
                        <div class="card-body">
                            <br>
                            <form method="POST" novalidate
                                  action=" {{ (!isset($data)) ? route($routePrefix.".store") : route($routePrefix.".update", ["id" => $data->id])}}"
                                  id="detail-form">
                                @csrf
                                @if(isset($editable) && $editable == true && isset($data))
                                    @method('PUT')
                                @endif

                                <x-input type="text" label="{{__('data_field_name.plan.title')}}" form-name="title" value="{{isset($data) ? $data->title : ''}}" required=true disabled="{{!$editable}}" placeholder="" />
                                <x-input type="text" label="{{__('data_field_name.plan.code')}}" form-name="code" value="{{isset($data) ? $data->code : ''}}" required=true disabled="{{isset($data) ? true :false}}" placeholder=""/>
                                <x-input type="number" label="{{__('data_field_name.plan.year')}}" form-name="year" value="{{isset($data) ? $data->year : ''}}" required=true disabled="{{!$editable}}" placeholder=""/>
                                @if(isset($data))
                                    <input type="hidden" name="code" value="{{isset($data) ? $data->code : ''}}">
                                @endif

                                @if(isset($editable) && $editable == true)
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">{{__('data_field_name.common_field.department')}} <span class="text-danger">(*)</span></label>
                                        <div class="col-md-9">
                                            <select name="department_id" id="department_list" class="form-control" required>
                                                <option value="">{{__('data_field_name.action.select', ["dataName" => __('data_field_name.model_name.department')])}}</option>
                                                @if(isset($departments))
{{--                                                    <option value="{{ $department['id'] }}" {{isset($data) && $department['id'] == $data->department_id ? "selected" : "" }}>{{ $department['name'] }}</option>--}}
                                                    @foreach($departments as $department)
                                                        <option value="{{ $department['id'] }}" {{isset($data) && $department['id'] == $data->department_id ? "selected" : "" }}>{{ $department['name'] }}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                        </div>
                                    </div>
                                @else
                                    <x-input type="text" label="{{__('data_field_name.common_field.department')}}" form-name="department_id" value="{{isset($data) ? $data->department->name : ''}}" required=true disabled="true" placeholder=""/>
                                @endif

                                <x-textarea label="{{__('data_field_name.common_field.descriptions')}}" form-name="description" value="{{isset($data) ? $data->description : ''}}" disabled="{{!$editable}}" placeholder=""/>
                                <input type="hidden" name="link" id="link">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        @include("partials.common-detail-action", ["back_params" =>
                                        ["is_active" => isset($data) ? ($data->deleted_at ? -1 : 1) : (isset($is_active) ? $is_active : 1)]
                                        ])
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('validation-rules')
    department_id: "required",
@endpush

@push('validation-messages')
    department_id: {
        required: "{{__('client_validation.form.required.plan.department')}}",
    },
@endpush
@include("partials.message")
