<div class="">
    <div class="card-body">
        <br>
        <form method="POST" novalidate
              action=" {{ (is_null($data)) ? route("record-plans.store") : route("record-plans.update", ["id" => $data->id])}}"
              id="detail-form">
            @csrf
            @if(isset($editable) && $editable == true && isset($data))
                @method('PUT')
            @endif

            @php
                $today = date('Y-m-d');
                $tomorrow = date('Y-m-d',strtotime($today . "+1 days"));
                $editableStartTime = $editable;
                $editableEndTime = $editable;
                if(isset($data)){
                    if($data->start_time < $today){
                        $editableStartTime = false;
                        $editableEndTime = false;
                    }
                }
            @endphp
            <div class="row record-plan-info">
                <div class="col-lg-4 col-md-6 mb-2 mt-2">
                    <x-input type="text" label="{{__('data_field_name.record_plan.name')}}" form-name="name" value="{{isset($data) ? $data->name : ''}}" required=true disabled="{{!$editable}}" placeholder="" horizontal="name"/>
                </div>
                <div class="col-lg-4 col-md-6 mb-2 mt-2">
                    @if($editable)
                        <div class="form-group">
                            <label class=""> {{__('data_field_name.model_name.department')}}<span class="text-danger">(*)</span></label>
                            <div class="">
                                <select name="department_id" id="department_id" class="form-control" required
                                    {{isset($data) && !$editable ? "disabled" : ""}}>
                                    @foreach($departments as $department)
                                        <option value="{{$department->id}}" {{isset($data) && $department->code == $data->department_code ? "selected" : ""}}>{{$department->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @else
                        <x-input type="text" label="{{__('data_field_name.model_name.department')}}" form-name="department_code" value="{{isset($data) ? optional($data->department)->name : ''}}" required=true disabled="true" placeholder=""
                                 horizontal="department_code"/>
                    @endif
                </div>
                <div class="col-lg-4 col-md-6 mb-2 mt-2">
                    @if($editable)
                        <div class="form-group">
                            <label> {{__('data_field_name.model_name.plans')}}{{--<span class="text-danger">(*)</span>--}}</label>
                            <div>
                                <select name="plan_id" id="plan_id" class="form-control"
                                    {{isset($data) && !$editable ? "disabled" : ""}}>
                                </select>
                            </div>
                            @error("plan_id")
                            <span class="text-danger">
                        {{ $message }}
                    </span>
                            @enderror
                        </div>
                    @else
                        <x-input type="text" label="{{__('data_field_name.model_name.plans')}}" form-name="plan_id" value="{{isset($data) ? optional($data->plan)->title : ''}}" required=false disabled="true" placeholder=""
                                 horizontal="plan_id"/>
                    @endif
                </div>
                <div class="col-lg-4 col-md-4 mb-2 mt-2">
                    <label for="">
                        Thời gian <span class="text-danger">(*)</span>
                    </label>
                    <div class="d-flex mb-2">
                        <div class="">
                            <div class="form-check checkbox">
                                <input type="checkbox" class="form-check-input" id="is_allday"
                                       name="is_allday" {{(isset($editable) && !isset($data)) ? '' : (isset($data) && $data->is_allday < 1 ? '' : 'checked')}} {{!$editable ? "disabled" : "" }} >
                            </div>
                        </div>
                        <label class="">{{__('data_field_name.record_plan.is_allday')}}</label>
                    </div>
                    <div class="d-flex">
                        <div class="mr-2">
                            <x-date-time form-name="start_time" label="Bắt đầu" value="{{isset($data) ? $data->start_time : $today}}" required="true" disabled="{{!$editableStartTime}}" horizontal="start_time"/>
                        </div>
                        <div>
                            <x-date-time form-name="end_time" label="Kết thúc" value="{{isset($data) ? $data->end_time : $tomorrow }}" required="true" disabled="{{!$editableEndTime}}" horizontal="end_time"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-2 mt-2">
                    <x-textarea label="{{__('data_field_name.common_field.address')}}" form-name="address" value="{{isset($data) ? $data->address : ''}}" required=true disabled="{{!$editable}}" placeholder="" horizontal="address" row=5/>
                </div>
                <div class="col-lg-4 col-md-6 mb-2 mt-2">
                    <x-textarea label="{{__('data_field_name.common_field.note')}}" form-name="notes" value="{{isset($data) ? $data->notes : ''}}" disabled="{{!$editable}}" placeholder="" horizontal="notes" row=5/>
                </div>
                <div class="col-12 mb-2 mt-2">
                    <x-textarea label="{{__('data_field_name.common_field.descriptions')}}" form-name="description" value="{{isset($data) ? $data->description : ''}}" disabled="{{!$editable}}" placeholder="" required="false"
                                horizontal="description"/>
                </div>
            </div>

            <div class="row mt-1">
                <div class="col-lg-4 col-md-6 mb-2 mt-2">
                    <x-input type="number" label="{{__('data_field_name.record_plan.proposal_number.reporter')}}" form-name="offer_reporter_number" value="{{isset($data) ? $data->offer_reporter_number : ''}}" required=true
                             disabled="{{!$editable}}"
                             placeholder="" horizontal="offer_reporter_number"/>
                    <x-input type="text" label="{{__('data_field_name.record_plan.proposal.reporter')}}" form-name="offer_reporter_note" value="{{isset($data) ? $data->offer_reporter_note : ''}}" required=false disabled="{{!$editable}}"
                             placeholder="" horizontal="offer_reporter_note"/>
                </div>
                <div class="col-lg-4 col-md-6 mb-2 mt-2">
                    <x-input type="number" label="{{__('data_field_name.record_plan.proposal_number.camera')}}" form-name="offer_camera_number" value="{{isset($data) ? $data->offer_camera_number : ''}}" required=false
                             disabled="{{!$editable}}"
                             placeholder="" horizontal="offer_camera_number"/>
                    <x-input type="text" label="{{__('data_field_name.record_plan.proposal.camera')}}" form-name="offer_camera_note" value="{{isset($data) ? $data->offer_camera_note : ''}}" required=false disabled="{{!$editable}}"
                             placeholder="" horizontal="offer_camera_note"/>
                </div>
                <div class="col-lg-4 col-md-6 mb-2 mt-2">
                    <x-input type="number" label="{{__('data_field_name.record_plan.proposal_number.transportation')}}" form-name="offer_transportation_number" value="{{isset($data) ? $data->offer_transportation_number : ''}}"
                             required=false
                             disabled="{{!$editable}}" placeholder="" horizontal="offer_transportation_number"/>
                    <x-input type="text" label="{{__('data_field_name.record_plan.proposal.transportation')}}" form-name="offer_transportation_note" value="{{isset($data) ? $data->offer_transportation_note : ''}}" required=false
                             disabled="{{!$editable}}"
                             placeholder="" horizontal="offer_transportation_note"/>
                </div>
                <div class="col-lg-4 col-md-6 mb-2 mt-2">
                    <x-input type="number" label="{{__('data_field_name.record_plan.proposal_number.device')}}" form-name="offer_device_number" value="{{isset($data) ? $data->offer_device_number : ''}}" required=false
                             disabled="{{!$editable}}"
                             placeholder="" horizontal="offer_device_number"/>
                </div>
                <div class="col-lg-8 col-md-6 mb-2 mt-2">
                    <x-input type="text" label="{{__('data_field_name.record_plan.proposal.device')}}" form-name="offer_device_note" value="{{isset($data) ? $data->offer_device_note : ''}}" required=false disabled="{{!$editable}}"
                             placeholder="" horizontal="offer_device_note"/>
                </div>
            </div>
            @if(isset($data))
                <input type="hidden" id="plan" value="{{$data->plan_id}}">
            @endif
            @if(isset($data))
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">{{__("data_field_name.record_plan.current_status")}}</label>
                    <div class="col-md-9">
                        <x-status-span status="{{$data->status}}" data-type="record_plan" :danger="[-1]" :secondary="[0]" :warning="[1,3]" :success="[2,4]" :primary="[5]"></x-status-span>
                    </div>
                </div>
            @endif

            <div class="form-group row float-right">
                <div class="col-md-9 d-flex">
                    {{--Nếu không có data ==> create--}}
                    @if(!isset($data))
                        <button class="btn btn-action btn-success no-wrap view text-decoration-none ml-1" type="submit"
                                title="{{__('common.button.create')}}">
                            {{__('common.button.create')}}
                        </button>
                    @endif
                    {{--Nếu có data, không cho update ==> show.  Nếu loginUser được quyền cập nhật ==> show nút edit--}}
                    @can('update', app($modelClass))
                        @if(isset($data) && $editable == false)
                            @if(!in_array($data->status, [config("common.status.record_plan.cancel"), config("common.status.record_plan.wrap"), config("common.status.record_plan.done") ]))
                                <a href="{{ route($routePrefix. '.edit', ['id' => $data->id])}}" class="btn btn-success no-wrap view text-decoration-none ml-1"
                                   title="{{__('common.button.edit')}}">
                                    {{__('common.button.edit')}}
                                </a>
                            @endif
                        @endif
                    @endcan
                    {{--Nếu có data, cho update ==> edit--}}
                    @if(isset($data) && $editable == true)
                        <button class="btn btn-action btn-success no-wrap view text-decoration-none ml-1" type="submit"
                                title="{{__('common.button.update')}}">
                            {{__('common.button.update')}}
                        </button>
                    @endif
                    {{--Back để quay về màn hình danh sách--}}
                    @include("partials.back-btn", ["route" => $routePrefix. '.index', "params" => isset($back_params) ? $back_params : []])
                    {{--                    End common detail actions--}}

                    @if(isset($data) && ($data->status == config("common.status.record_plan.pending") || $data->status == config("common.status.record_plan.approved") || $data->status == config("common.status.record_plan.in_progress")))
                        @can("is_author", $data)
                            @php
                                $redirect_back_route = $routePrefix;
                                if(isset($editable) && $editable){
                                    $redirect_back_route .= ".edit";
                                }else{
                                    $redirect_back_route .= ".show";
                                }
                                $redirect_back_params = $data->id;
                            @endphp

                            @can("update", $data)
                                @can("create_reporter_assignment")
                                    @include("partials.small-btn.assign-reporter-btn", ["route" => "assignment.reporter.show",
                                                "params" => ["record_plan_id" => $data->id, "redirect_back_route" => $redirect_back_route, "redirect_back_params" => $redirect_back_params]])
                                @endcan

                                @can("create_device_assignment")
                                    @include("partials.small-btn.assign-device-btn", ["route" => "assignment.device.show",
                                                 "params" => ["record_plan_id" => $data->id, "redirect_back_route" => $redirect_back_route, "redirect_back_params" => $redirect_back_params]])
                                @endcan
                            @endcan


                        @endcan
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>

