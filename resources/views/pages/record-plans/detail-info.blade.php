<div class="card-body pt-0">
    <div class="row">
        <div class="col-12 mt-4  mb-4">
            <div class="d-flex">
                <h5>
                    {!! isset($data) ? $data->name : '' !!}
                    <x-status-span status="{{$data->status}}" data-type="record_plan" :danger="[-1]" :secondary="[0]"
                                   :warning="[1,3]" :success="[2,4]" :primary="[5]"></x-status-span>
                </h5>
                @can("update", $data)
                    <div class="ml-2"><a href="" data-toggle="modal" data-target="#edit_name_modal"
                                         class="font-weight-bold"><i class="fas fa-edit"></i></a></div>
                @endcan
            </div>
            <small><i class="fa fa-clock-o text-primary mr-2"
                      style="font-size: 16px "></i>{{date('H:i d/m/Y', strtotime($data->start_time))}} -
                {{date('H:i d/m/Y ', strtotime($data->end_time))}}</small>
            <br>
            <small><i class="fa fa-compass text-primary mr-2" style="font-size: 16px"></i>{!! $data->actual_address !!}</small>
            <br>
            @if($data->is_recurring != 1 && \App\Services\RecordPlanService::checkTimeToUpdateRecordPlan($data->end_time) == false)
                <span class="badge badge-danger">Đã hết thời hạn chỉnh sửa thông tin, phân bổ nguồn lực cho lịch sản xuất</span>
            @endif
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-lg-6 col-md-12 form-inline mb-2">
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-lg-9 col-md-6">
                <div class="border w-100 rounded p-3 mb-3">
                    <div class="d-flex justify-content-between">
                        <h5><i class="fas fa-clipboard-list mr-2"></i>Nhân sự tham gia</h5>
                        <div class="savce" id="save-6">
                            @can("assignResource", $data)
                                <button class="btn btn-warning btn-sm btn-save" id="assign_member_btn"><i class="fas fa-edit mr-2"></i>Phân bổ nhân sự</button>
                            @endcan
                            @if($data->is_recurring == 1 || (\App\Services\RecordPlanService::checkTimeToUpdateRecordPlan($data->end_time) && \App\Services\RecordPlanService::checkGeneralStatusCanUpdate($data->status)))
                                @can("is_author", $data)
                                    <button class="btn btn-primary btn-sm btn-save" id="save-offer" data-toggle="modal"
                                            data-target="#offer_member_modal"><i class="fas fa-edit mr-2"></i>Cập nhật đề xuất nhân sự
                                    </button>
                                @endcan
                            @endif
                        </div>
                    </div>
                            <x-card-resource :memberAssgined="$assigned_reporter" :recordPlan="$data" title="Phóng viên" members="pending-reporter" nodata_message="Chưa phân bổ phóng viên nào"
                                             proposals=" "
                                             note="{{isset($data->offer_reporter_note) ? $data->offer_reporter_note : ''}}" departmentCode="reporter"
                                             recordPlanId="{{$data->id}}"
                                             outsourceNote="{{$reporter_note}}"
                            >
                            </x-card-resource>
                            <hr>
                            <x-card-resource :memberAssgined="$assigned_technical_camera" :recordPlan="$data" title="Kĩ thuật phòng quay" members="pending-technical-camera" nodata_message="Chưa phân bổ kĩ thuật phòng quay nào"
                                             proposals="(Cần {{isset($data->offer_technical_camera_number)? $data->offer_technical_camera_number : '0'}} nhân sự)"
                                             note="{{isset($data->offer_technical_camera_note) ? $data->offer_technical_camera_note : ''}}" departmentCode="technical_camera"
                                             recordPlanId="{{$data->id}}"
                                             outsourceNote=""
                            >
                            </x-card-resource>
                            <hr>
                            <x-card-resource :memberAssgined="$assigned_camera" :recordPlan="$data" title="Quay phim" members="pending-camera" nodata_message="Chưa phân bổ quay phim nào"
                                             proposals="(Cần {{isset($data->offer_camera_number)? $data->offer_camera_number : '0'}} nhân sự)"
                                             note="{{isset($data->offer_camera_note) ? $data->offer_camera_note : ''}}" departmentCode="camera"
                                             recordPlanId="{{$data->id}}"
                                             outsourceNote="{{$camera_note}}"
                            >
                            </x-card-resource>
                            <hr>
                            <x-card-resource :memberAssgined="$assigned_transportation" :recordPlan="$data" title="Lái Xe"
                                             members="pending-transportation"
                                             nodata_message="Chưa phân bổ lái xe nào"
                                             proposals="(Cần {{isset($data->offer_transportation_number)? $data->offer_transportation_number : '0'}} nhân sự)"
                                             note="{{isset($data->offer_transportation_note) ? $data->offer_transportation_note : ''}}"
                                             departmentCode="transportation" recordPlanId="{{$data->id}}"
                                             outsourceNote="{{$transportation_note}}"
                            >
                            </x-card-resource>
                            <hr>
                    <!-- @if((!isset($data->offer_camera_number) ||  $data->offer_camera_number == 0) && (!isset($data->offer_transportation_number) ||  $data->offer_transportation_number == 0))
                        <div class="container">
                            <p style="text-align: center">Không có nhân sự nào</p>
                        </div>
                    @endif -->
                </div>
                <div class="border w-100 rounded p-3 mb-3">
                    <div class="d-flex justify-content-between">
                        <h5><i class="fas fa-check-circle text-green mr-2"></i>Thiết bị</h5>
                        <div class="savce" id="save-6">
                            @if((auth()->user()->hasRole("super admin") || (auth()->user()->department_code == "device" && auth()->user()->is_manager == 1 )) && !empty($data->offer_device_note)
                            &&  ($data->is_recurring == 1 || (\App\Services\RecordPlanService::checkTimeToUpdateRecordPlan($data->end_time))
                            && \App\Services\RecordPlanService::checkGeneralStatusCanUpdate($data->status)))
                                <button class="btn btn-warning btn-sm btn-save" id="update-assign-device"><i class="fas fa-edit mr-2"></i>Phân bổ thiết bị
                                </button>
                            @endif
                            @if($data->is_recurring == 1 || (\App\Services\RecordPlanService::checkTimeToUpdateRecordPlan($data->end_time) && \App\Services\RecordPlanService::checkGeneralStatusCanUpdate($data->status)))
                                @can("proposalDevice", $data)
                                    <button class="btn btn-primary btn-sm btn-save" id="offer_device-device" data-toggle="modal"
                                            data-target="#offer_device_modal"><i class="fas fa-edit mr-2"></i>Cập nhật đề xuất thiết bị
                                    </button>
                                @endcan
                            @endif
                        </div>
                    </div>
                    <div class="text-secondary">
                        <div class="label-note">
                            <small>* Ghi chú: </small>
                        </div>
                        <div class="content ml-3">
                            <small>{!! isset($data->offer_device_note) && trim($data->offer_device_note) != '' ? nl2br($data->offer_device_note) : '' !!}</small>
                        </div>
                    </div>
                    @if(isset($device_note) && $device_note)
                        <div class="text-secondary">
                            <div class="label-note">
                                <small>* Ghi chú thiết bị thuê ngoài: </small>
                            </div>
                            <div class="content ml-3">
                                <small>{!! nl2br($device_note) !!}</small>
                            </div>
                        </div>
                    @endif
                        @if(count($assigned_devices) > 0)
                        <div class="table-responsive-xl tableFixHead">
                            <table class="table border-0 table-striped">
                                <thead>
                                <tr class="row p-2">
                                    <th class="col-sm-4">{{__('data_field_name.common_field.name')}}</th>
                                    <th class="col-sm-3">{{__('data_field_name.device.serial')}}</th>
                                    <th class="col-sm-3">{{__('data_field_name.device.category')}}</th>
                                    <th class="col-sm-2">{{__('data_field_name.common_field.action')}}</th>
                                </tr>
                                </thead>
                                <tbody id="table_wait_conf_device">
                                @foreach($assigned_devices as $device)
                                    <tr class="row p-2">
                                        <td class="text-truncate overflow col-sm-4" data-toggle="tooltip" data-placement="top" title="{{ optional($device->device)->name }}">{{ optional($device->device)->name }}</td>
                                        <td class="text-truncate col-sm-3" data-toggle="tooltip" data-placement="top" title="{{ optional($device->device)->serial }}">{{ optional($device->device)->serial }}</td>
                                        <td class="col-sm-3">{{ optional(optional($device->device)->category)->name }}</td>
                                        <td class="col-sm-2">
                                        <!-- <form action="{{route("assignment.device.destroy", ["record_plan_id" => $data->id])}}" method="post">
                                        @method("delete")
                                        @csrf
                                            <input type="hidden" name="device_ids" value="{{ optional($device->device)->id }}">
                                        <button  class="btn btn-danger btn-sm" title="{{__('common.button.remove-assign')}}" >
                                            {{__('common.button.remove-assign')}}
                                            </button>
                                        </form> -->
                                        @can("assignDevice", $data)
                                            <button class="btn btn-danger btn-sm unassign-btn" id="{{$device}}" title="{{__('common.button.remove-assign')}}">
                                                {{__('common.button.remove-assign')}}
                                            </button>
                                        @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                            <div class="container">
                                <p style="text-align: center">Chưa phân bổ thiết bị nào</p>
                            </div>
                        @endif
                </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="border w-100 rounded p-3">
                <div class="mb-2 w-100 float-left">
                    <div class="float-left">
                        <h6>Mô tả</h6>
                    </div>
                    @can("update", $data)
                        <div class="float-right"><a href="" id="description" data-toggle="modal"
                                                    data-target="#edit_description_modal" class="font-weight-bold"><i
                                    class="fas fa-edit"></i></a></div>
                    @endcan
                </div>
                <div class="w-100">
                    <p class="ml-0">{{$data->description}}</p>
                </div>
            </div>
            <div class="border w-100 rounded p-3 mt-3">
                <div class="mb-2 w-100 float-left">
                    <div class="float-left">
                        <h6>Thông tin lịch sản xuất</h6>
                    </div>
                    @can("update", $data)
                        <div class="float-right"><a href="" id="infomation" data-toggle="modal"
                                                    data-target="#edit_infomation_modal" class="font-weight-bold"><i
                                    class="fas fa-edit"></i></a></div>
                    @endcan
                </div>
                <div class="">
                    <div class="flex-column">
                        <p>Thời gian bắt đầu: <span class="text-primary">{{date('H:i d/m/Y', strtotime($data->start_time))}}<span></p>
                    </div>
                    <div class="flex-column">
                        <p>Thời gian kết thúc: <span class="text-primary">{{date('H:i d/m/Y', strtotime($data->end_time))}}<span></p>
                    </div>
{{--                    <div class="flex-column">--}}
{{--                        <p>Diễn ra cả ngày: <span class="text-primary">{{$data->is_allday ? 'Có' : 'Không'}}<span></p>--}}
{{--                    </div>--}}
                    @if($data->is_recurring == 1)
                        <p>Lặp lại:
                            <span class="text-primary">
                                @php
                                    $recurring = array();
                                    if ($data->on_monday == 1) $recurring[] = 'Thứ 2';
                                    if ($data->on_tuesday == 1) $recurring[] = 'Thứ 3';
                                    if ($data->on_wednesday == 1) $recurring[] = 'Thứ 4';
                                    if ($data->on_thursday == 1) $recurring[] = 'Thứ 5';
                                    if ($data->on_friday == 1) $recurring[] = 'Thứ 6';
                                    if ($data->on_saturday == 1) $recurring[] = 'Thứ 7';
                                    if ($data->on_sunday == 1) $recurring[] = 'Chủ nhật';
                                    echo implode(", ",$recurring)
                                @endphp
                            <span>
                        </p>
                    @endif
                    <div class="flex-column">
                        <p>Địa chỉ: <span class="text-primary">{{$data->actual_address}}<span></p>
                    </div>
                    <hr>
                </div>
            </div>
            <div class="border w-100 rounded p-3 mt-3">
                <div class="mb-2 w-100">
                    <h5>Người tạo lịch</h5>
                </div>
                <div class="">
                    <img src="{{ asset('dist/img/avatar5.png')}}" style="width:50px; height:50px;" class="img-circle" alt="avatar">
                    <div class="d-flex flex-column ml-2">
                        <a>Họ tên: <span class="text-primary">{{$data->creator ? $data->creator->full_name : 'Chưa cập nhật'}}<span></a>
                        <a>SĐT: <span class="text-primary">{{$data->creator ? $data->creator->phone_number : 'Chưa cập nhật'}}<span></a>
                        <a>Email: <span class="text-primary"> {{$data->creator ? $data->creator->email : 'Chưa cập nhật'}}<span></a>
                    </div>
                    <hr>
                </div>
            </div>
            <div class="border w-100 rounded p-3 mt-3">
                <div class="mb-2 w-100 float-left">
                    <div class="float-left">
                        <h6>Kế hoạch sản xuất</h6>
                    </div>
                    @can("update", $data)
                        <div class="float-right"><a href="" id="plan" data-toggle="modal" data-target="#edit_plan_modal"
                                                    class="font-weight-bold"><i class="fas fa-edit"></i></a></div>
                    @endcan
                </div>
                <div class="">
                    <div class="flex-column">
                        <a>Phòng ban: <span class="text-primary">{{isset($data) ? optional($data->department)->name : ''}}<span></a>
                        <p>Kế hoạch: <span class="text-primary">{{isset($data) ? optional($data->plan)->title : ''}}</span></p>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
    <div class="mb-2">
        @include("partials.back-btn", ["route" => $routePrefix. '.index', "params" => isset($back_params) ? $back_params :[]])
    </div>
</div>
<form method="POST" novalidate action="{{route("record-plans.update", ["id" => $data->id])}}" id="detail-form"
      enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="name">
        @include('partials.modal.edit-record-plan',["field"=>"name","data" => $data])
    </div>
    <div class="description">
        @include('partials.modal.edit-record-plan',["field"=>"description","data" => $data])
    </div>
    <div class="infomation">
        @include('partials.modal.edit-record-plan',["field"=>"infomation","data" => $data])
    </div>
    <div class="reporter">
        @include('partials.modal.edit-record-plan',["field"=>"reporter","data" => $data])
    </div>
    <div class="plan">
        @include('partials.modal.edit-record-plan',["field"=>"plan","data" => $data])
    </div>
</form>

@include('partials.modal.edit-assign-members',['type'=>'all'])
@include("partials.modal.update-offer-member")
@include('partials.modal.edit-proposal-devices')
@include('partials.modal.edit-assign-devices',['title'=>'Danh sách thiết bị sẵn có', 'action'=>'assign', 'btn_action'=>'assign-device', 'submit_btn'=> __('common.button.assign')])
@include("partials.modal.unassign-resource")


@push('detail-record-plan')
    <script>
        var recordPlanId = '{{$data->id}}';
        $('#update-assign-device').click(function () {
            recordPlanId = '{{$data->id}}';
            getListDeviceCategory();
            getListAvailableDevice(null, recordPlanId, null)
            $('#assign_device_modal').modal('show');
        })
        $('#assign_member_btn').click(function () {
            recordPlanId = '{{$data->id}}';
            getMembers();
            $('#assign_member_modal').modal('show');
        })
        $('.unassign-btn').click(function () {
            var $row = $(this).closest("tr");
            var id = $(this).attr('id');
            var recordPlan = '{{$data->id}}';
            if (JSON.parse(id).department_code !== undefined) {
                //unassign member
                $("#resource_ids").val(JSON.parse(id).id)
                $("#resource_ids").attr("name", "member_ids");
                console.log(JSON.parse(id).department_code.search('production'));
                if (JSON.parse(id).department_code.search('production') >= 0) {
                    $("#department_code").val('reporter');
                } else {
                    $("#department_code").val(JSON.parse(id).department_code);
                }
                $('#unassign-form').attr('action', `/assignment/${$("#department_code").val()}/${recordPlan}/destroy`);
            } else {
                //unassign device
                $("#resource_ids").val(JSON.parse(id).device.id);
                $("#resource_ids").attr("name", "device_ids");
                $('#unassign-form').attr('action', '{{route("assignment.device.destroy", ["record_plan_id" => $data->id])}}');
            }
            $('#unassign_modal').modal('show');
        });

        const stage=`<div class="form-group row">
                <label class="col-md-4 col-form-label"><span
                                        class="text-danger"></span></label>
                <div class="col-md-8 d-flex">
                    <select id="studio" name="address" class="form-control">
                        <option value="">Chọn phòng quay</option>
                        @foreach($stages as $stage)
                            <option value="{{$stage->code}}" {{isset($data) && $stage->code == $data->address ? "selected" : ""}}>{{$stage->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label"><span class="text-danger"></span>Đề xuất số lượng kỹ thuật phòng quay</label>
                <div class="col-md-8">
                    <input type="hidden" value="2" name="address_type">
                    <input type="number" min="0" class="form-control mb-1 w-100" name="offer_technical_camera_number" placeholder="Nhập ghi chú đề xuất số lượng kỹ thuật phòng quay" value="{{$data->offer_technical_camera_number}}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label"><span class="text-danger"></span>Ghi chú đề xuất kỹ thuật phòng quay</label>
                <div class="col-md-8">
                    <textarea class="form-control" name="offer_technical_camera_note" require  autocomplete="off" placeholder="Nội dung..." rows=4>{{$data->offer_technical_camera_note}}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="recurring">
                    <div class="row">
                        <label class="col-md-4 col-form-label"><span class="text-danger"></span></label>
                        <div class="col-md-8">
                            <div class="checkbox">
                                <label id="div_recurring" onchange="toggleCheckbox(this)">
                                    <input id="is_recurring" name="is_recurring" {{(isset($data->is_recurring) && $data->is_recurring == 1) ? 'checked' : ''}} type="checkbox" /> Thiết lập lịch lặp lại
                                 </label>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="display: {{(isset($data->is_recurring) && $data->is_recurring == 1) ? 'flex' : 'none'}}" id="dayOfWeek">
                        <label class="col-md-4 col-form-label"><span class="text-danger"></span></label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-4">
                                    <small>
                                        Ngày lặp lại <span class="text-danger">(*)</span>:
                                     </small>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_monday" name="on_monday" {{(isset($data->on_monday) && $data->on_monday == 1) ? 'checked' : ''}} type="checkbox" /> Thứ 2
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_tuesday" name="on_tuesday" {{(isset($data->on_tuesday) && $data->on_tuesday == 1) ? 'checked' : ''}} type="checkbox" /> Thứ 3
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_wednesday" name="on_wednesday" {{(isset($data->on_wednesday) && $data->on_wednesday == 1) ? 'checked' : ''}} type="checkbox" /> Thứ 4
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_thursday" name="on_thursday" {{(isset($data->on_thursday) && $data->on_thursday == 1) ? 'checked' : ''}} type="checkbox" /> Thứ 5
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_friday" name="on_friday" {{(isset($data->on_friday) && $data->on_friday == 1) ? 'checked' : ''}} type="checkbox" /> Thứ 6
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_saturday" name="on_saturday" {{(isset($data->on_saturday) && $data->on_saturday == 1) ? 'checked' : ''}} type="checkbox" /> Thứ 7
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_sunday" name="on_sunday" {{(isset($data->on_sunday) && $data->on_sunday == 1) ? 'checked' : ''}} type="checkbox" /> Chủ nhật
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div>`;
        const addressInput = `<div class="form-group row">
                            <label class="col-md-4 col-form-label"><span class="text-danger"></span></label>
                            <div class="col-md-8">
                                <textarea class="form-control" name="address" require autocomplete="off" placeholder="Nhập thông tin địa chỉ" rows=4>{{$data->address_type != 2 ? $data->actual_address: ""}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label"><span class="text-danger"></span>Đề xuất số lượng kỹ thuật phòng quay</label>
                            <div class="col-md-8">
                                <input type="hidden" value="2" name="address_type">
                                <input type="number" min="0" class="form-control mb-1 w-100" name="offer_technical_camera_number" placeholder="Nhập ghi chú đề xuất số lượng kỹ thuật phòng quay" value="{{$data->offer_technical_camera_number}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label"><span class="text-danger"></span>Ghi chú đề xuất kỹ thuật phòng quay</label>
                            <div class="col-md-8">
                                <textarea class="form-control" name="offer_technical_camera_note" require  autocomplete="off" placeholder="Nội dung..." rows=4>{{$data->offer_technical_camera_note}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label"><span class="text-danger"></span></label>
                            <input name="offer_technical_camera_number" value="0" type="hidden">
                            <input name="offer_technical_camera_note" value="" type="hidden">
                            <div class="col-md-8 d-flex">
                                <div class="form-check mr-2">
                                    <input class="form-check-input" type="radio" name="address_type"
                                        id="flexRadioDefault1" value="0" {{$data->address_type > 1 || $data->address_type == 0 ? 'checked' : "" }}>
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        Nội thành
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="address_type"
                                        id="flexRadioDefault2" value="1" {{$data->address_type == 1 ? 'checked' : "" }}>
                                    <label class="form-check-label" for="flexRadioDefault2">
                                        Liên tỉnh
                                    </label>
                                </div>
                            </div>
                        </div>
                        `;
        function toggleCheckbox(element) {
            if ($(element).children('#is_recurring').is(':checked')) {
                $(element).parents('.recurring').children('#dayOfWeek').css('display', 'flex');
            } else {
                $(element).parents('.recurring').children('#dayOfWeek').css('display', 'none');
            }
        }

    </script>
@endpush
