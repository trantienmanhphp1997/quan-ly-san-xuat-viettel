@extends('layouts.detail')
@section("css")
    <link href="{{ asset('css/calenderInput.css') }}" rel="stylesheet">
    @stack("detail-record-plan-css")
@endsection

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex" style="font-weight: bold;">
                            <div>
                                <i class="fa fa-align-justify mr-2"></i>
                            </div>

                            <strong>
                                @if($data->is_recurring != 1 && \App\Services\RecordPlanService::checkTimeToUpdateRecordPlan($data->end_time) == false)
                                {{__("data_field_name.action.show", ["dataName" => strtolower(__('data_field_name.model_name.'.$modelDisplayName))])}}
                                @else
                                {{__("data_field_name.action.edit", ["dataName" => strtolower(__('data_field_name.model_name.'.$modelDisplayName))])}}
                                @endif
                            </strong>
                        </div>
{{--                        <div class="card-body">--}}
                            @include("pages.record-plans.detail-info", ["data" => isset($data) ? $data : null, "editable" => $editable ])
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('validation-rules')
    start_time: {
    dateBefore: '#end_time',
    dateCheck:true,
    required: true
    },
    end_time: {
    dateAfter: '#start_time',
    required: true
    },

@endpush

@push('validation-messages')
    plan_id: {
    required: '{{__("client_validation.form.required.record_plan.plan")}}'
    }
@endpush

@section('javascript')
    @include("partials.message")
    <script src="{{ asset('js/get-plans-by-departemet.js') }}"></script>
    <script src="{{ asset('js/device-services.js') }}"></script>
    <script src="{{ asset('js/custom-flatpickr.js') }}"></script>
    <script>
        $(document).ready(function () {
            customDate(
                '.start_time',
                "{{ date('d-m-Y',strtotime(date('d-m-Y')))}}",
                "{{ isset($data) ? date('H:i d-m-Y',strtotime($data->start_time)) : date('H:i d-m-Y',strtotime(date('H:i d-m-Y') .'+2 hours +1 days')) }}");
            customDate(
                '.end_time',
                "{{ date('d-m-Y',strtotime(date('d-m-Y')))}}",
                "{{ isset($data) ? date('H:i d-m-Y',strtotime($data->end_time)) : date('H:i d-m-Y',strtotime(date('H:i d-m-Y') .'+7 hours +1 days')) }}");

            $('.select2').select2({
                placeholder: "Select a state",
                allowClear: true
            });
            getPlans();
            // getMembers();
        });

        $("#department_id").on("change", function (e){
            getPlans();
        })

        $('#start_time-error').hide();
        $('#end_time-error').hide();

        function getPlans(){
            var $department_id = $("#department_id").val();
            if($department_id){
                getPlansByDepartmentId($department_id)
            }
        }

        function removeElementInSelectOption(el) {
            $(`#${el}`).children().remove();
            $(`#${el}`).append(
                // `<option value=""></option>`
            )
        }
    </script>
    @stack('detail-record-plan')
@endsection
