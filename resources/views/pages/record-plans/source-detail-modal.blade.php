@php
    $reporters = $data->members->filter(function($member){
        return strpos($member->department_code, "production" ) !== false;
    }) ?? [];
    $cameras = $data->members->filter(function($member){
        return $member->department_code == config("common.departments.camera");
    }) ?? [];
    $technical_cameras = $data->members->filter(function($member){
        return $member->department_code == config("common.departments.technical_camera");
    }) ?? [];
    $transportations = $data->members->filter(function($member){
        return $member->department_code == config("common.departments.transportation");
    }) ?? [];
    $devices = $data->devices ?? [];
@endphp
<div class="modal" tabindex="-1" id="source-detail-modal-{{$data->id}}" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="card card-primary card-outline card-outline-tabs p-3">
                    <div class="card-header p-0 pt-1 border-bottom-0 flex-row">
                        <ul class="nav nav-tabs" id="nav-source" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="custom-tabs-two-home-tab-{{$data->id}}" data-toggle="pill"
                                   href="#custom-tabs-two-home-{{$data->id}}" role="tab" aria-controls="custom-tabs-two-home-{{$data->id}}"
                                   aria-selected="false">{{__('data_field_name.model_name.reporter')}} ({{$reporters->count()}})</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-two-you-tab-{{$data->id}}" data-toggle="pill"
                                   href="#custom-tabs-two-you-{{$data->id}}" role="tab"
                                   aria-controls="custom-tabs-two-you-{{$data->id}}" aria-selected="false">{{__('data_field_name.model_name.technical_camera')}} ({{isset($data->offer_technical_camera_number)? $data->offer_technical_camera_number : '0'}})</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-two-profile-tab-{{$data->id}}" data-toggle="pill"
                                   href="#custom-tabs-two-profile-{{$data->id}}" role="tab"
                                   aria-controls="custom-tabs-two-profile-{{$data->id}}" aria-selected="false">{{__('data_field_name.model_name.camera')}} ({{isset($data->offer_camera_number)? $data->offer_camera_number : '0'}})</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-two-messages-tab-{{$data->id}}" data-toggle="pill"
                                   href="#custom-tabs-two-messages-{{$data->id}}" role="tab"
                                   aria-controls="custom-tabs-two-messages-{{$data->id}}" aria-selected="false">{{__('data_field_name.model_name.transportation')}} ({{isset($data->offer_transportation_number)? $data->offer_transportation_number : '0'}})</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-two-settings-tab-{{$data->id}}" data-toggle="pill"
                                   href="#custom-tabs-two-settings-{{$data->id}}" role="tab"
                                   aria-controls="custom-tabs-two-settings-{{$data->id}}" aria-selected="true">{{__('data_field_name.model_name.device')}} ({{isset($data->offer_device_number)? $data->offer_device_number : '0'}})</a>
                            </li>
                        </ul>
                        <div class="position-absolute" style="top: 0;right: 0;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-two-tabContent-{{$data->id}}">
                            {{--                        phóng viên--}}
                            <div class="tab-pane fade active show" id="custom-tabs-two-home-{{$data->id}}" role="tabpanel"
                                 aria-labelledby="custom-tabs-two-home-tab-{{$data->id}}">
                                <label class="font-weight-bold">{{__('data_field_name.record_plan.proposaled_number.reporter')}} :</label>
                                @if($reporters->count() > 0)
                                    <span class="text-danger">{{$reporters->count()}}</span>
                                @else
                                    <span>{{__('common.message.no_assign')}}</span>
                                @endif
                                <br>
                                <label class="font-weight-bold">{{__('data_field_name.common_field.note')}} :</label>
                                <span>{{$data->offer_reporter_note ?? ''}}</span>
                                <br>
                                <label class="font-weight-bold">Danh sách nguồn lực</label>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped table-head-fixed">
                                            <thead>
                                                <tr>
                                                    <th>{{__('data_field_name.common_field.stt')}}</th>
                                                    <th>{{__('data_field_name.user.full_name')}}</th>
                                                    <th>{{__('data_field_name.common_field.phone')}}</th>
                                                    @if(auth()->user()->hasRole("super admin") || strpos(auth()->user()->department_code, "production") !== false)
                                                        <th>{{__('data_field_name.common_field.department')}}</th>
                                                    @endif
                                                    <th>{{__('data_field_name.common_field.position')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($reporters as $reporter)
                                                <tr>
                                                    <td>{{$loop->index + 1}}</td>
                                                    <td>{{$reporter->full_name ?? ''}}</td>
                                                    <td>{{$reporter->phone_number ?? ''}}</td>
                                                    <td>{{$reporter->department->name ?? ''}}</td>
                                                    <td>{{$reporter->is_manager != 0 ? 'Quản lý' : 'Nhân viên'}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        @if(count($reporters) == 0)
                                            <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{--                            Kỹ thuật phòng quay --}}
                            <div class="tab-pane fade" id="custom-tabs-two-you-{{$data->id}}" role="tabpanel"
                                 aria-labelledby="custom-tabs-you-profile-tab-{{$data->id}}">
                                <label class="font-weight-bold">{{__('data_field_name.record_plan.proposaled_number.technical_camera')}} :</label>
                                @if(isset($data->offer_technical_camera_number))
                                    <span class="text-danger">{{$technical_cameras->count()}}/{{$data->offer_technical_camera_number}}</span>
                                @else
                                    <span>{{__('common.message.no_assign')}}</span>
                                @endif
                                <br>
                                <label class="font-weight-bold">{{__('data_field_name.common_field.note')}} :</label>
                                <span>{{$data->offer_technical_camera_note ?? ''}}</span>
                                <br>
                                <label class="font-weight-bold">Danh sách nguồn lực</label>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped table-head-fixed">
                                            <thead>
                                                <tr>
                                                    <th>{{__('data_field_name.common_field.stt')}}</th>
                                                    <th>{{__('data_field_name.user.full_name')}}</th>
                                                    <th>{{__('data_field_name.common_field.phone')}}</th>
                                                    @if(auth()->user()->hasRole("super admin") || strpos(auth()->user()->department_code, "production") !== false)
                                                        <th>{{__('data_field_name.common_field.department')}}</th>
                                                    @endif
                                                    <th>{{__('data_field_name.common_field.position')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($technical_cameras as $camera)
                                                <tr>
                                                    <td>{{$loop->index + 1}}</td>
                                                    <td>{{$camera->full_name ?? ''}}</td>
                                                    <td>{{$camera->phone_number ?? ''}}</td>
                                                    <td>{{$camera->department->name ?? ''}}</td>
                                                    <td>{{$camera->is_manager != 0 ? 'Quản lý' : 'Nhân viên'}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        @if(count($technical_cameras) == 0)
                                            <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            {{--                            Quay phim--}}
                            <div class="tab-pane fade" id="custom-tabs-two-profile-{{$data->id}}" role="tabpanel"
                                 aria-labelledby="custom-tabs-two-profile-tab-{{$data->id}}">
                                <label class="font-weight-bold">{{__('data_field_name.record_plan.proposaled_number.camera')}} :</label>
                                @if(isset($data->offer_camera_number))
                                    <span class="text-danger">{{$cameras->count()}}/{{$data->offer_camera_number}}</span>
                                @else
                                    <span>{{__('common.message.no_assign')}}</span>
                                @endif
                                <br>
                                <label class="font-weight-bold">{{__('data_field_name.common_field.note')}} :</label>
                                <span>{{$data->offer_camera_note ?? ''}}</span>
                                <br>
                                <label class="font-weight-bold">Danh sách nguồn lực</label>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped table-head-fixed">
                                            <thead>
                                                <tr>
                                                    <th>{{__('data_field_name.common_field.stt')}}</th>
                                                    <th>{{__('data_field_name.user.full_name')}}</th>
                                                    <th>{{__('data_field_name.common_field.phone')}}</th>
                                                    @if(auth()->user()->hasRole("super admin") || strpos(auth()->user()->department_code, "production") !== false)
                                                        <th>{{__('data_field_name.common_field.department')}}</th>
                                                    @endif
                                                    <th>{{__('data_field_name.common_field.position')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($cameras as $camera)
                                                <tr>
                                                    <td>{{$loop->index + 1}}</td>
                                                    <td>{{$camera->full_name ?? ''}}</td>
                                                    <td>{{$camera->phone_number ?? ''}}</td>
                                                    <td>{{$camera->department->name ?? ''}}</td>
                                                    <td>{{$camera->is_manager != 0 ? 'Quản lý' : 'Nhân viên'}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        @if(count($cameras) == 0)
                                            <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            {{--                            Lái xe--}}
                            <div class="tab-pane fade" id="custom-tabs-two-messages-{{$data->id}}" role="tabpanel"
                                 aria-labelledby="custom-tabs-two-messages-tab-{{$data->id}}">
                                <label class="font-weight-bold">{{__('data_field_name.record_plan.proposaled_number.transportation')}} :</label>
                                @if(isset($data->offer_transportation_number))
                                    <span class="text-danger">{{$transportations->count()}}/{{$data->offer_transportation_number}}</span>
                                @else
                                    <span>{{__('common.message.no_assign')}}</span>
                                @endif
                                <br>
                                <label class="font-weight-bold">{{__('data_field_name.common_field.note')}} :</label>
                                <span>{{$data->offer_transportation_note ?? ''}}</span>
                                <br>
                                <label class="font-weight-bold">Danh sách nguồn lực</label>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped table-head-fixed">
                                            <thead>
                                                <tr>
                                                    <th>{{__('data_field_name.common_field.stt')}}</th>
                                                    <th>{{__('data_field_name.user.full_name')}}</th>
                                                    <th>{{__('data_field_name.common_field.phone')}}</th>
                                                    @if(auth()->user()->hasRole("super admin") || strpos(auth()->user()->department_code, "production") !== false)
                                                        <th>{{__('data_field_name.common_field.department')}}</th>
                                                    @endif
                                                    <th>{{__('data_field_name.common_field.position')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($transportations as $transportation)
                                                <tr>
                                                    <td>{{$loop->index + 1}}</td>
                                                    <td>{{$transportation->full_name ?? ''}}</td>
                                                    <td>{{$transportation->phone_number ?? ''}}</td>
                                                    <td>{{$transportation->department->name ?? ''}}</td>
                                                    <td>{{$transportation->is_manager != 0 ? 'Quản lý' : 'Nhân viên'}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        @if(count($transportations) == 0)
                                            <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{--                            Thiết bị--}}
                            <div class="tab-pane fade" id="custom-tabs-two-settings-{{$data->id}}" role="tabpanel"
                                 aria-labelledby="custom-tabs-two-settings-tab-{{$data->id}}">
                                <label class="font-weight-bold">{{__('data_field_name.record_plan.proposaled_number.device')}} :</label>
                                @if(isset($data->offer_device_number))
                                    <span class="text-danger">{{$devices->count()}}/{{$data->offer_device_number}}</span>
                                @else
                                    <span>{{__('common.message.no_assign')}}</span>
                                @endif
                                <br>
                                <label class="font-weight-bold">{{__('data_field_name.common_field.note')}} :</label>
                                <span>{{$data->offer_device_note ?? ''}}</span>
                                <br>
                                <label class="font-weight-bold">Danh sách nguồn lực</label>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped table-head-fixed">
                                            <thead>
                                                <tr>
                                                    <th>{{__('data_field_name.common_field.stt')}}</th>
                                                    <th>{{__('data_field_name.device.name')}}</th>
                                                    <th>{{__('data_field_name.device.category')}}</th>
                                                    <th>{{__('data_field_name.device.serial')}}</th>
                                                    <th>{{__('data_field_name.common_field.descriptions')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($devices as $device)
                                                <tr>
                                                    <td>{{$loop->index + 1}}</td>
                                                    <td>{{$device->name}}</td>
                                                    <td>{{$device->device_category_code}}</td>
                                                    <td>{{$device->serial}}</td>
                                                    <td>{{$device->note}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        @if(count($devices) == 0)
                                            <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
