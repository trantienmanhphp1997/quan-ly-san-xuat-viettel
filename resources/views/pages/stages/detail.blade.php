@extends('layouts.detail')

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex" style="font-weight: bold;">
                            <div>
                                <i class="fa fa-align-justify mr-2"></i>
                            </div>
                            <strong>
                                @if(!isset($data))
                                    {{__("data_field_name.action.create", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @elseif(isset($editable) && $editable == true)
                                    {{__("data_field_name.action.edit", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @else
                                    {{__("data_field_name.action.show", ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
                                @endif
                            </strong>
                        </div>
                        <div class="card-body">
                            <br>
                            <form method="POST" novalidate
                                  action=" {{ (!isset($data)) ? route($routePrefix.".store") : route($routePrefix.".update", ["id" => $data->id])}}"
                                  id="detail-form">
                                @csrf
                                @if(isset($editable) && $editable == true && isset($data))
                                    @method('PUT')
                                @endif
                                <x-input type="text" label="{{__('data_field_name.common_field.name')}}" form-name="name" value="{{isset($data) ? $data->name : ''}}" required=true disabled="{{!$editable}}" placeholder="" />
{{--                                {{dd(!$editable || isset($hideCode) ? $hideCode : false)}}--}}

                                @if(isset($hideCode) && $hideCode )
                                    <input type="hidden" name="code" value="{{isset($data) ? $data->code : ''}}">
                                @endif
                                <x-input type="text" label="{{__('data_field_name.common_field.code')}}" form-name="code" value="{{isset($data) ? $data->code : ''}}" required=true disabled="{{!$editable || isset($hideCode) ? $hideCode : false}}" placeholder="" />
                                <x-input type="text" label="{{__('data_field_name.common_field.address')}}" form-name="address" value="{{isset($data) ? $data->address : ''}}" required=true disabled="{{!$editable}}" placeholder="" />


                                <x-textarea label="{{__('data_field_name.common_field.descriptions')}}" form-name="description" value="{{isset($data) ? $data->description : ''}}" disabled="{{!$editable}}" placeholder=""/>
                                <x-checkbox label="{{__('data_field_name.common_field.active')}}" form-name="status"
                                            checked="{{ (isset($editable) && !isset($data))? 'checked' : (isset($data) && $data->status == 1 ? 'checked' : '') }}"
                                            disabled="{{!$editable}}"/>
                                <input type="hidden" name="link" id="link">
                                <div class="form-group row">
                                    <div class="offset-md-3 col-md-9">
                                        @include("partials.common-detail-action", ["back_params" =>
                                        ["is_active" => isset($data) ? ($data->deleted_at ? -1 : 1) : (isset($is_active) ? $is_active : 1)]
                                        ])
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
    @include("partials.message")
    <script>
    </script>
@endsection
