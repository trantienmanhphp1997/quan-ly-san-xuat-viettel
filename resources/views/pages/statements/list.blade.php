@extends('layouts.list')

@section("buttons")
    @can('create', app($modelClass))
        @include("partials.text-btn.create-group-btn")
    @endcan
    @can('export', app($modelClass))
        @include("partials.text-btn.export-excel-btn",["params" => $allParams])
    @endcan
@endsection

@section("sub-header-content")
    {{__('data_field_name.action.manage', ["dataName" => __('data_field_name.model_name.'.$modelDisplayName)])}}
@endsection


@section('table-list')
    <div class="table-responsive">
        <table class="table table-hover table-striped" id="dataTable">
            <thead>
            <tr>
                <th>
                    <input type="checkbox" class="select_all">
                </th>
                <th>{{__('data_field_name.common_field.stt')}}</th>
                <th>{{__('data_field_name.contract.title')}}</th>
                <th>{{__('data_field_name.common_field.code')}}</th>
                <th>{{__('data_field_name.department.name')}}</th>
                <th>{{__('data_field_name.common_field.descriptions')}}</th>
                <th>{{__('data_field_name.common_field.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $datum)
                <tr>
                    <td>
                        <input type="checkbox" name="row_id" id="checkbox_{{ $datum->id }}" value="{{ $datum->id }},{{$datum->status}}">
                    </td>
                    <td>{{ $loop->index + ($data->currentPage() - 1) * $data->perPage() +1 }}</td>
                    <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $datum->title }}">{{ $datum->title }}</td>
                    <td>{{ $datum->code }}</td>
                    <td>{{ $datum->department->name }}</td>
                    <td class="text-truncate text-limit" data-toggle="tooltip" data-placement="top" title="{{ $datum->description }}">{{ $datum->description }}</td>

                    <td>
                        <div class="d-flex">
                            @include('partials.small-btn.common-action')
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(count($data) == 0)
        <div class="d-flex justify-content-center"><b>{{__('notification.common.fail.no-data')}}</b></div>
        @endif
    </div>
@endsection
