<a href="{{ route($route, $params)}}" class="btn btn-primary no-wrap view text-decoration-none ml-1" id="go-back-url-test" onclick="goBackUrlTest()"
   title="{{__('common.button.back')}}">
    {{__('common.button.back')}}
</a>
