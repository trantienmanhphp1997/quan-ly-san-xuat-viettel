@can("create_reporter_assignment")
    @include("partials.small-btn.assign-reporter-btn", ["route" => "assignment.production.show", "params" => ["record_plan_id" => $datum->id]])
@endcan
@can("create_camera_assignment")
    @include("partials.small-btn.assign-cam-btn", ["route" => "assignment.camera.show", "params" => ["record_plan_id" => $datum->id]])
@endcan
@can("create_driver_assignment")
    @include("partials.small-btn.assign-car-driver-btn", ["route" => "assignment.transportation.show", "params" => ["record_plan_id" => $datum->id]])
@endcan
{{--@can("is_device")--}}
{{--    @include("partials.small-btn.assign-device-btn", ["route" => "assignment.device.show", "params" => ["record_plan_id" => $datum->id]])--}}
{{--@endcan--}}
@if("create_device_assignment")
    @include("partials.small-btn.assign-device-btn", ["route" => "assignment.device.show", "params" => ["record_plan_id" => $datum->id]])
@endif
