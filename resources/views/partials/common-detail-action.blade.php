{{--Nếu không có data ==> create--}}
@if(!isset($data))
    <button class="btn btn-action btn-success no-wrap view text-decoration-none ml-1"  type="submit"
            title="{{__('common.button.create')}}">
        {{__('common.button.create')}}
    </button>
@endif
{{--Nếu có data, không cho update ==> show.  Nếu loginUser được quyền cập nhật ==> show nút edit--}}
<!-- @can('update', app($modelClass))
    @if(isset($data) && $editable == false)
        <a href="{{ route($routePrefix. '.edit', ['id' => $data->id])}}" class="btn btn-success no-wrap view text-decoration-none ml-1"
           title="{{__('common.button.edit')}}">
            {{__('common.button.edit')}}
        </a>
    @endif
@endcan -->
{{--Nếu có data, cho update ==> edit--}}
@if(isset($data) && $editable == true)
<button class="btn btn-action btn-success no-wrap view text-decoration-none ml-1" type="submit"
        title="{{__('common.button.update')}}">
    {{__('common.button.update')}}
</button>
@endif
{{--Back để quay về màn hình danh sách--}}
@include("partials.back-btn", ["route" => $routePrefix. '.index', "params" => isset($back_params) ? $back_params : []])
{{--@include("partials.previous-route-btn");--}}
