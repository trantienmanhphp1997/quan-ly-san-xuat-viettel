<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading active"  role="tab" id="headingOne">
            <h4 class="panel-title">
                <div>
                    @include('partials.search', ['allParams' => $allParams, 'filterOptions' => $filterOptions])
                    @if(empty($filterOptions) == false)
                        <button class="btn btn-link float-right" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Nâng cao
                        </button>
                    @endif
                </div>
            </h4>
        </div>

        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body clearfix" style="padding: 30px 20px 20px;">
                <form method="get">
                    <div class="clearfix row">
                        @foreach($filterOptions as $filterKey => $filterValue)
                            <div class="col-md-3 form-group">
                                <label for="order_column">{{$filterValue['label']}}</label>
                                @if(array_key_exists('tooltip', $filterValue))
                                    <span
                                          aria-hidden="true"
                                          data-toggle="tooltip"
                                          data-placement="right"
                                          title="{{ $filterValue['tooltip'] }}"></span>
                                @endif

                                <select name="{{$filterKey}}" class="select2 form-control">
                                    @foreach($filterValue['options'] as $optionKey => $optionValue)
                                        <option value="{{ $optionKey }}"
                                                @if(isset($allParams) && array_key_exists($filterKey, $allParams)  && $allParams[$filterKey] == $optionKey) selected @endif
                                        >{{ $optionValue }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endforeach
                    </div>
                    <button type="submit" class="btn float-right btn-primary">Apply</button>
                </form>
            </div>
        </div>
    </div>
</div>
