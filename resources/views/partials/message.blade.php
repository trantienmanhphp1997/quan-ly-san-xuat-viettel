<!-- custom toastr -->
<script>
   toastr.options = {
    "closeButton": false,
    "debug": false,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "3000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
</script>
<!-- Hiển thị thông báo thành công  -->
@if ( Session::has('success'))
    <script>
        toastr.success("{{ Session::get('success') }}");
    </script>
    <?php
        session()->forget('success');
    ?>
@endif
<!-- Hiển thị thông báo lỗi  -->
@if ( Session::has('error'))
    <script>
        toastr.error("{{ Session::get('error') }}");
    </script>
    <?php
    session()->forget('error');
    ?>
@endif
@if ( Session::has('create_error'))
    <script>
        toastr.error("{{ Session::get('create_error') }}");
    </script>
    <?php
    session()->forget('create_error');
    ?>
@endif
