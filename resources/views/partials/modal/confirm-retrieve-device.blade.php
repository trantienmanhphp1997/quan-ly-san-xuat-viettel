<div class="modal modal-danger fade" tabindex="-1" id="confirm-retrieve" role="dialog" aria-labelledby="confirm_retrieve_modal_body">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
           <div class="modal-body text-center" id="confirm_retrieve_modal_body">
                <div class="icon-default icon-report mb-3">
                    <i class="fas fa-tools"></i>
                </div>
                <h4 class="modal-title mb-4">
                    {{ __('common.confirm_message.confirm_title') }}
                </h4>
                <h5 class="noti-text">
                    {{ __('common.confirm_message.are_you_sure_retrieve_device') }}?
                </h5>
           </div>
            <div class="modal-footer justify-content-center">
                    <button id="bulk_broken" type="button" class="btn btn-default pull-right" data-dismiss="modal">
                        {{ __('common.button.cancel') }}
                    </button>
                    <button type="button" class="btn btn-warning pull-right" id="confirm-receive-device">{{ __('common.confirm_message.confirm_to_delete') }}</button>
            </div>
        </div>
    </div>
</div>
@push('detail-record-plan')
<script>
    var recordPlanId = ""
    var deviceId = "";
        $('#confirm-receive-device').click(function (event) {
        var ids = [];
            ids.push(deviceId);
        updateDeviceStatus(recordPlanId,ids,"{{config('common.status.propose_device.returned')}}");
    })
</script>
@endpush
