<div class="modal modal-danger fade" tabindex="-1" id="create_modal" role="dialog" aria-labelledby="create_modal_body">
    <div class="modal-dialog modal-dialog-centered modal-xl ">
        <div class="modal-content">
            <div class="modal-header">
                <h4> {{__("data_field_name.action.create", ["dataName" => __('data_field_name.model_name.record-plans')])}}
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body" id="create_modal_body">
                <form method="POST" novalidate action="{{route('record-plans.store')}}" id="detail-form" class="p-3">
                    @csrf
                    <div class="record-plan-info mb-3">
                        <h6>Thông tin lịch Sản xuất</h6>
                        <x-input type="text" label="{{__('data_field_name.record_plan.name')}}" form-name="name"
                                 value="" required=true placeholder="" styleInput="col-md-7"/>
                    <!-- <div class="row">
                            <div class="col-3"></div>
                            <div class="col-9 d-flex">
                                <div class="form-check checkbox">
                                    <input type="checkbox" class="form-check-input" id="is_allday" name="is_allday">
                                </div>
                                <label class="">{{__('data_field_name.record_plan.is_allday')}}</label>
                            </div>
                        </div> -->
                        <x-date-time form-name="start_time" label="Thời gian bắt đầu" value="{{old('start_time')}}" required="true"
                                     styleInput="col-md-4"/>
                        <x-date-time form-name="end_time" label="Thời gian kết thúc" value="{{old('end_time')}}" required="true"
                                     styleInput="col-md-4"/>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Địa chỉ<span
                                    class="text-danger">(*)</span></label>
                            <div class="col-md-8 d-flex">
                                <div class="form-check mr-2">
                                    <input class="form-check-input" type="radio" name="address_type_check"
                                           id="flexRadioDefault4" value="4" checked>
                                    <label class="form-check-label" for="flexRadioDefault4">
                                        Địa chỉ ngoài
                                    </label>
                                </div>
                                <div class="form-check mr-2">
                                    <input class="form-check-input" type="radio" name="address_type_check"
                                           id="flexRadioDefault3" value="3" @if(old('address_type_check')) checked @endif>
                                    <label class="form-check-label" for="flexRadioDefault3">
                                        Trường quay
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="address-option">

                        </div>

                        <div class="input-address">

                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label"> {{__('data_field_name.model_name.department')}}<span
                                    class="text-danger">(*)</span></label>
                            <div class="col-md-4">
                                <select name="department_id" id="department_id" class="form-control" required>
                                    @foreach($departments as $department)
                                        <option value="{{$department->id}}" code="{{$department->code}}"
                                            {{ old('department_id') == $department->id ? 'selected' : '' }}>
                                            {{$department->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">
                                {{__('data_field_name.model_name.plans')}}{{--<span class="text-danger">(*)</span>--}}</label>
                            <div class="col-md-4">
                                <select name="plan_id" id="plan_id" class="form-control">
                                </select>
                            </div>
                            @error("plan_id")
                            <span class="text-danger error">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>
                        <x-textarea label="{{__('data_field_name.common_field.descriptions')}}" form-name="description"
                                    value="" placeholder="" required="false" row=5/>
                    </div>

                    <div class="prosal-resources">
                        <h6>Đề xuất nguồn lực</h6>
                        <div class="form-group d-flex">
                            <label class="col-md-3 col-form-label">Chọn phóng viên<span
                                    class="text-danger">(*)</span></label>
                            <div class="col-md-9">
                                <select id="getReporters" name="reporter_ids[]" multiple="multiple"
                                        class="form-control">
                                    <option value=""></option>
                                    @foreach($reporters as $reporter)
                                        @php
                                            $name = "$reporter->full_name (sđt: $reporter->phone_number)"
                                        @endphp
                                        <option value="{{$reporter->id}}"
                                            {{(collect(old('reporter_ids'))->contains($reporter->id)) ? "selected" : (auth()->user()->id == $reporter->id ? "selected" : "")}}>
                                            {{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        {{--                        <x-input type="number" label="{{__('data_field_name.record_plan.proposal_number.reporter')}}"--}}
                        {{--                            form-name="offer_reporter_number" value="" required=true placeholder=""--}}
                        {{--                            styleInput="col-md-4" />--}}

                        <x-input type="text" label="{{__('data_field_name.record_plan.proposal.reporter')}}"
                                 form-name="offer_reporter_note" value="" required=false placeholder=""
                                 styleInput="col-md-7"/>

                        <x-input type="number" label="{{__('data_field_name.record_plan.proposal_number.camera')}}"
                                 form-name="offer_camera_number" value="" required=false placeholder=""
                                 styleInput="col-md-4"/>
                        <x-input type="text" label="{{__('data_field_name.record_plan.proposal.camera')}}"
                                 form-name="offer_camera_note" value="" required=false placeholder=""
                                 styleInput="col-md-7"/>

                        <x-input type="number"
                                 label="{{__('data_field_name.record_plan.proposal_number.transportation')}}"
                                 form-name="offer_transportation_number" value="" max="1000" required=false placeholder=""
                                 styleInput="col-md-4"/>
                        <x-input type="text" label="{{__('data_field_name.record_plan.proposal.transportation')}}"
                                 form-name="offer_transportation_note" value="" required=false placeholder=""
                                 styleInput="col-md-7"/>

                    <!-- <x-input type="number" label="{{__('data_field_name.record_plan.proposal_number.device')}}"
                            form-name="offer_device_number" value="" required=false placeholder=""
                            styleInput="col-md-4" />
                        <x-input type="text" label="{{__('data_field_name.record_plan.proposal.device')}}"
                            form-name="offer_device_note" value="" required=false placeholder=""
                            styleInput="col-md-7" /> -->
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Đề xuất thiết bị<span
                                    class="text-danger"></span></label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-9 d-flex">
                                        <div class="dropdown col-form-label">
                                            <label>Mẫu đề xuất </label>
                                            <button class="btn btn-default dropdown-toggle btn-sm" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                <i class="fas fa-list-ol"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                @foreach($exampleNotes as $note)
                                                    <a class="dropdown-item" content="{{$note->content}}"
                                                       href="#">{!! $note->name !!}</a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12 d-flex">
                                        <textarea class="form-control" name="offer_device_note" id="text"
                                                  autocomplete="off" placeholder="Ghi chú đề xuất thiết bị"
                                                  rows=4>{{old("offer_device_note")}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" title="{{__('common.button.create')}}"
                                class="btn btn-primary pull-right btn-action action" id="create">
                            {{ __('common.button.create') }}
                        </button>
                        <button type="button" class="btn btn-default pull-right" id="cancel" data-dismiss="modal">
                            {{ __('common.button.cancel') }}
                        </button>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-center">
            </div>
        </div>
    </div>
</div>
@push('validation-rules')
    start_time: {
        dateBefore: '#end_time',
        required: true
    },
    end_time: {
        dateAfter: '#start_time',
        required: true
    },
    address: {
        required: true,
        maxlength: 500
    },
    address2: {
        required: true
    },
    "reporter_ids[]": "required",
    description: {
        maxlength: 3000
    },
    offer_device_note: {
        maxlength: 3000
    },
    offer_technical_camera_note: {
        maxlength: 100
    },
@endpush

@push('validation-messages')
plan_id: {
    required: '{{__("client_validation.form.required.record_plan.plan")}}'
},
address: {
    required: '{{__("client_validation.form.required.record_plan.address")}}',
    maxlength: '{{__("client_validation.form.max_length.abstract", ["fieldName" => "địa chỉ", "maxlength" => 500])}}'
},
address2: {
    required: '{{__("client_validation.form.required.record_plan.address2")}}'
},
"reporter_ids[]": 'Bạn chưa chọn phóng viên cho lịch sản xuất',
offer_technical_camera_number: {
    min: 'Đề xuất số lượng kỹ thuật phòng quay cần lớn hơn 0',
    number: 'Số lượng không hợp lệ',
    required: '{{__("client_validation.form.required.record_plan.offer_technical_camera_number")}}'
},
description: {
    maxlength: '{{__("client_validation.form.max_length.abstract", ["fieldName" => "mô tả", "maxlength" => 3000])}}'
},
offer_device_note: {
    maxlength: '{{__("client_validation.form.max_length.abstract", ["fieldName" => "ghi chú đề xuất thiết bị", "maxlength" => 3000])}}'
},
offer_technical_camera_note: {
    maxlength: '{{__("client_validation.form.max_length.abstract", ["fieldName" => "ghi chú đề xuất kỹ thuật phòng quay", "maxlength" => 100])}}'
},
@endpush
@push('list-record-plan')
    <script>
        const stage = `<div class="form-group row">
                <label class="col-md-3 col-form-label"><span
                                        class="text-danger"></span></label>
                <div class="col-md-6 d-flex">
                    <select id="studio" name="address2" class="form-control">
                        <option value="">Chọn phòng quay</option>
                        @foreach($stages as $stage)
                        <option value="{{$stage->code}}" {{old('address2') == $stage->code ? "selected" : ""}}>{{$stage->name}}</option>
                        @endforeach
                    </select>
                </div>
</div>
<x-input type="number" label="{{__('data_field_name.record_plan.proposal_number.technical_camera')}}"
                            form-name="offer_technical_camera_number" min=0 value="" required=false placeholder=""
                            styleInput="col-md-4" />
            <div class="form-group row">
                <label class="col-md-3 col-form-label"><span class="text-danger"></span></label>
                <div class="col-md-6">
                    <textarea class="form-control" name="offer_technical_camera_note" require  autocomplete="off" placeholder="Ghi chú đề xuất kỹ thuật phòng quay" rows=4>{{old("offer_technical_camera_note")}}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="recurring">
                    <div class="row">
                        <label class="col-md-3 col-form-label"><span class="text-danger"></span></label>
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label id="div_recurring" onchange="toggleCheckbox(this)">
                                    <input id="is_recurring" name="is_recurring" type="checkbox" @if(old('is_recurring')) checked @endif/> Thiết lập lịch lặp lại
                                 </label>
                            </div>
                        </div>
                    </div>
                </div>
            <div>
            `;

        const divRecurring = `<div class="row" id="dayOfWeek">
                        <label class="col-md-3 col-form-label"><span class="text-danger"></span></label>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <small>
                                        Ngày lặp lại <span class="text-danger">(*)</span>:
                                     </small>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_monday" name="on_monday" type="checkbox" @if(old('on_monday')) checked @endif /> Thứ 2
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_tuesday" name="on_tuesday" type="checkbox" @if(old('on_tuesday')) checked @endif /> Thứ 3
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_wednesday" name="on_wednesday" type="checkbox" @if(old('on_wednesday')) checked @endif /> Thứ 4
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_thursday" name="on_thursday" type="checkbox" @if(old('on_thursday')) checked @endif /> Thứ 5
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_friday" name="on_friday" type="checkbox" @if(old('on_friday')) checked @endif /> Thứ 6
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_saturday" name="on_saturday" type="checkbox" @if(old('on_saturday')) checked @endif /> Thứ 7
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="on_sunday" name="on_sunday" type="checkbox" @if(old('on_sunday')) checked @endif /> Chủ nhật
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;

        const addressInput = `<div class="form-group row">
                                <label class="col-md-3 col-form-label"><span class="text-danger"></span></label>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="address" require autocomplete="off" placeholder="Nhập thông tin địa chỉ" rows=4>{{old("address")}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label"><span class="text-danger"></span></label>
                                <div class="col-md-8 d-flex">
                                    <div class="form-check mr-2">
                                        <input class="form-check-input" type="radio" name="address_type"
                                            id="flexRadioDefault1" value="0" checked>
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            Nội thành
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="address_type"
                                            id="flexRadioDefault2" value="1" @if(old('address_type')) checked @endif>
                                        <label class="form-check-label" for="flexRadioDefault2">
                                            Liên tỉnh
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <x-input type="number" label="{{__('data_field_name.record_plan.proposal_number.technical_camera')}}"
                                form-name="offer_technical_camera_number" min=0 value="" required=false placeholder=""
                                styleInput="col-md-4" />
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label"><span class="text-danger"></span></label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="offer_technical_camera_note" require  autocomplete="off" placeholder="Ghi chú đề xuất kỹ thuật phòng quay" rows=4>{{old("offer_technical_camera_note")}}</textarea>
                                </div>
                            </div>`;

        function toggleCheckbox(element) {
            if ($(element).children('#is_recurring').is(':checked')) {
                $(element).parents('.recurring').append(divRecurring);
            } else {
                $(element).parents('.recurring').children('#dayOfWeek').remove();
            }
        }

        $(document).ready(function () {
            $('#getReporters').select2({
                placeholder: "Chọn phóng viên"
            });
            // $('#detail-form').on("submit", function (e) {
            //     if (document.getElementById('is_recurring').checked && (!document.getElementById('on_monday').checked && !document.getElementById('on_tuesday').checked &&
            //         !document.getElementById('on_wednesday').checked && !document.getElementById('on_thursday').checked && !document.getElementById('on_friday').checked
            //         && !document.getElementById('on_saturday').checked && !document.getElementById('on_sunday').checked)) {
            //         debugger
            //         e.preventDefault();
            //         e.stopPropagation();
            //         alert('xxx')
            //         return;
            //     }
            // });
            $("#create_modal").on('shown.bs.modal', function () {
                const $now = new Date();
                let $startString = moment($now).add(moment.duration(1, 'hours')).format('H:00 DD-MM-YYYY');
                let $endString = moment($now).format('23:59 DD-MM-YYYY');
                let $startDay = moment($now).format('00:00 DD-MM-YYYY');
                if ($("#start_time").val()) {
                    console.log($("#start_time").val(), $("#end_time").val());
                    const startTime = new Date($("#start_time").val());
                    const endTime = new Date($("#end_time").val());
                    $startString = moment(startTime).format('H:m DD-MM-YYYY');
                    $endString = moment(endTime).format('H:m DD-MM-YYYY');
                    $startDay = moment(startTime).format('00:00 DD-MM-YYYY');
                }

                customDate(
                    '.start_time',
                    $startDay,
                    $startString);

                customDate(
                    '.end_time',
                    $startDay,
                    $endString);
            });

            $('.dropdown-item').on('click', function () {
                $('#text').val($(this).attr('content'));
            });

            getPlans();
            $(addressInput).appendTo($(".address-option"));

            switch ($("input[name='address_type_check']:checked").val()) {
            case "3":
                $(".address-option").empty();
                $(stage).appendTo($(".address-option"));
                // $(".input-address").hide('slow');
                if($('input[name="is_recurring"]:checked')) {
                console.log('a');
                $(divRecurring).appendTo($(".recurring"));
            }
                break;
            case "4":
                $(".address-option").empty();
                $(addressInput).appendTo($(".address-option"));
                break;
            default:
                break;
    }
            $('input[type="radio"]').click(function () {
                switch ($(this).attr("value")) {
                    case "3":
                        $(".address-option").empty();
                        $(stage).appendTo($(".address-option"));
                        // $(".input-address").hide('slow');
                        break;
                    case "4":
                        $(".address-option").empty();
                        $(addressInput).appendTo($(".address-option"));
                        break;
                    // case "1":
                    //     $(".select-devices").show('slow');
                    //     break;
                    // case "0":
                    //     $(".select-devices").hide('slow');
                    //     break;
                    default:
                        break;
                }
            });
            // $('input[type="radio"]').trigger('click');
        });


        $('#department_id').on("change", function (e) {
            getPlans();
            // empty select2 chose reporters
            $('#getReporters').val(null).trigger('change');
            $('#getReporters option').remove();
            $('#getReporters').select2({
                placeholder: "Loading..."
            });
            getMembersByDepartmentCode($(this).find(":selected").attr('code'));
        });

        $('#start_time-error').hide();
        $('#end_time-error').hide();

        function getPlans() {
            const $department_id = $("#department_id").val();
            if ($department_id) {
                getPlansByDepartmentId($department_id)
            }
        }

        function removeElementInSelectOption(el) {
            $(`#${el}`).children().remove();
            $(`#${el}`).append(
                // `<option value=""></option>`
            )
        }

        if ($('.error').text() !== '' || (eval('<?php echo Session::has('create_error')?>') === 1)) {
            $('#create_modal').modal('show');
        }

        $('#is_allday').click(function () {
            var start_time = "{{ date('H:i:s d-m-Y',strtotime(date('H:i d-m-Y')))}}";
            if ($('#is_allday').is(':checked')) {
                start_time = "{{ date('H:i:s d-m-Y',strtotime(date('00:00 d-m-Y')))}}";
            }
            customDate(
                '.start_time',
                "{{ date('d-m-Y',strtotime(date('d-m-Y')))}}",
                start_time);
            customDate(
                '.end_time',
                "{{ date('d-m-Y',strtotime(date('d-m-Y')))}}",
                "{{ date('H:i d-m-Y',strtotime(date('23:59 d-m-Y')))}}");
        });
        $('#start_time').on("change",function(e) {
            console.log("diff time");
            let diffTime = moment($('#end_time').val(), 'HH:mm DD-MM-YYYY').diff($('#end_time').val() !== '' && moment($('#start_time').val(), 'HH:mm DD-MM-YYYY'), 'days');
            if ($('#start_time').val() == '') {
                $('#start_time-error').html('Bạn chưa chọn thời gian bắt đầu');
                $('#start_time-error').show();
                $('#create').attr('disabled', true);
            } else {
                if ($('#flexRadioDefault3').is(':checked') && (moment($('#start_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD') != moment($('#end_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD'))) {
                    $('#start_time-error').html('Lịch quay trong trường quay chỉ diễn ra trong ngày, vui lòng chọn thời gian hợp lệ');
                    $('#start_time-error').show();
                    $('#create').attr('disabled', true);
                    return;
                }
                if ($('#end_time').val() !== '' && moment($('#start_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD HH:mm') > moment($('#end_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD HH:mm')) {
                    $('#start_time-error').html('Thời gian bắt đầu lớn hơn thời gian kết thúc');
                    $('#start_time-error').show();
                    $('#create').attr('disabled', true);
                    return;
                }
                if (moment($('#start_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD HH:mm') < moment().format('YYYY-MM-DD HH:mm')) {
                    $('#start_time-error').html('Thời gian bắt đầu không được nhỏ hơn thời gian hiện tại');
                    $('#start_time-error').show();
                    $('#create').attr('disabled', true);
                    return;
                }
                if(Math.abs(diffTime) > 1 &&  $('input[name="address_type"]').val() == 2){
                    $('#start_time-error').html('Lịch trường quay chỉ diễn ra trong 1 ngày')
                    $('#start_time-error').show()
                    $('#create').attr('disabled', true)
                    return;
                }
                $('#start_time-error').hide();
                $('#create').attr('disabled', false);
            }
        });
        $('#end_time').on("change", function(e) {
            console.log("diff time, end_time")
            let diffTime = moment($('#end_time').val(), 'HH:mm DD-MM-YYYY').diff($('#end_time').val() !== '' && moment($('#start_time').val(), 'HH:mm DD-MM-YYYY'), 'days');
            if ($('#end_time').val() == '') {
                $('#end_time-error').html('Bạn chưa chọn thời gian kết thúc');
                $('#end_time-error').show();
                $('#create').attr('disabled', true);
            } else {
                if ($('#flexRadioDefault3').is(':checked') && (moment($('#start_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD') != moment($('#end_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD'))) {
                    $('#start_time-error').html('Lịch quay trong trường quay chỉ diễn ra trong ngày, vui lòng chọn thời gian hợp lệ');
                    $('#start_time-error').show();
                    $('#create').attr('disabled', true);
                    return;
                }
                if ($('#start_time').val() !== '' && moment($('#start_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD HH:mm') > moment($('#end_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD HH:mm')) {
                    $('#start_time-error').html('Thời gian bắt đầu lớn hơn thời gian kết thúc');
                    $('#start_time-error').show();
                    $('#create').attr('disabled', true);
                } else {
                    $('#start_time').trigger( "change" );
                }
                if(Math.abs(diffTime) > 1 && $('input[name="address_type"]').val() == 2){
                    $('#end_time-error').html('Lịch trường quay chỉ diễn ra trong 1 ngày')
                    $('#end_time-error').show()
                    $('#create').attr('disabled', true)
                    return;
                }
            }
        });

        $('#flexRadioDefault3').on("change", function(e) {
            if ($(this).is(':checked') && (moment($('#start_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD') != moment($('#end_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD'))) {
                $('#start_time').trigger("change");
                $('#start_time-error').html('Lịch quay trong trường quay chỉ diễn ra trong ngày, vui lòng chọn thời gian hợp lệ');
                $('#start_time-error').show();
                $('#create').attr('disabled', true);
                return;
            }
        });

        $('#flexRadioDefault4').on("change", function(e) {
            $('#start_time').trigger( "change" );
        });

        $('#cancel').on("click", function (e) {
            location.reload();
        })

        $('.close').on("click", function (e) {
            location.reload();
        })
    </script>
@endpush
