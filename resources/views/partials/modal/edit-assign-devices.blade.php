<div class="modal modal-danger fade" tabindex="-1" id="{{$action}}_device_modal" role="dialog"
     aria-labelledby="{{$action}}_device_modal_body">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-body" id="{{$action}}_device_modal_body">
                <div><h5>{{$title}}</h5></div>
                @if(isset($action) && $action == 'assign')
                    <div class="options d-flex justify-content-end mb-3">
                        <div class="select-category">
                            <select name="" id="device_category" class="form-control form-control-sm rounded-0 custom-select-filter">
                                <option value="">Tất cả</option>
                            </select>
                        </div>
                        <div class="form-inline" name="quick-search-form">
                            <div class="form-group has-search ">
                                <input type="search" class="form-control rounded-0"
                                       style="height: 28px; width: 220px; padding-left: 10px;"
                                       placeholder="{{__('common.place_holder.search')}}" name="s" id="keyword"
                                       value="">
                                <button class="btn btn-secondary rounded-0 mr-2" id="search"
                                        style="height: 28px; width: 28px; padding: 1px" data-toggle="tooltip"
                                        data-placement="top" title="{{ __('common.button.search') }}"><i
                                        class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                @endif
                @if(isset($action) && $action == 'delivery')
                    <div class="options d-flex justify-content-start mb-3">
                        <div class="mr-2">
                            <span>Người nhận thiết bị: </span>
                        </div>
                        <div class="select-category">
                            <select name="" id="users" class="form-control form-control-sm rounded-0 custom-select-filter">
                            </select>
                        </div>
                    </div>
                @endif
                <div class="table-responsive tableFixHead">
                    <table class="table table-hover table-striped table-head-fixed" id="assign_{{$action}}_table">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" class="select_all">
                            </th>
                            <th>{{__('data_field_name.common_field.stt')}}</th>
                            <th>{{__('data_field_name.common_field.name')}}</th>
                            <th>{{__('data_field_name.device.serial')}}</th>
                            <th>{{__('data_field_name.device.category')}}</th>
                        <!-- <th class="text-center">{{__('data_field_name.common_field.status')}}</th> -->
                        </tr>
                        </thead>
                        <tbody id="table_{{$action}}_device">
                        </tbody>
                    </table>
                </div>
                <input type="hidden" id="assign_{{$action}}_input">
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal" id="{{$btn_action}}">
                    {{$submit_btn}}
                </button>
                @if($action == "assign")
                    <button type="button" class="btn btn-primary pull-right" id="outsourceDevice">Thuê ngoài thiết bị</button>
                @endif
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('common.button.cancel') }}
                </button>
            </div>
        </div>
    </div>
</div>

@php
    $outsourceNote = "";
@endphp
@include("components.add-note-modal", ["message" => "", "type" => "device"])

@push('detail-record-plan')
    <script>
        var recordPlanId = "";
        var action = "{{$action}}"
        var selected_ids = [];
        // $('#assign_device_modal').on('hidden.bs.modal', function () {
        //     selected_ids = [];
        // })

        $("#search").on("click", function (e) {
            getListAvailableDevice($('#device_category').val(), recordPlanId, $('#keyword').val())
        })

        if (action == 'assign') {

            $('#assign-device').click(function (event) {
                let unique_selected_ids = [...new Set(selected_ids)]
                console.log("unique_selected_ids "+unique_selected_ids)
                assignDeviceToRecordPlan(recordPlanId, unique_selected_ids);
                //sau khi phân công xong, xóa tất cả data trong selected ids
                selected_ids = [];
            })

            $("#outsourceDevice").on("click", function () {
                getDeviceOutsourceNote(recordPlanId)
                $("#confirmMessageContent_device").html("Bạn có muốn thuê ngoài thiết bị?");
                let departmentCode = "device";
                $("#fromDepartment_device").val(departmentCode);

                $("#add-note-to-assign-device").modal("show");
            })

            $("#confirmForOutSource_device").on("click", function () {
                let ids = prepareDataForAssignDevice();
                let formData = {
                    departmentCode: $("#fromDepartment_device").val(),
                    content: $("#contentMessage_device").val(),
                    device_ids: ids
                }
                // add note
                $.ajax({
                    type: "POST",
                    url: `/assignment/${recordPlanId}/add-note`,
                    data: formData,
                    dataType: 'json',
                    success: function (data) {
                        location.reload();
                    },
                    error: function (data) {
                        location.reload();
                    }
                });

            })

        }


        if (action == 'delivery') {
            $('#delivery-device').click(function (event) {
                var ids = [];

                var $checkedBoxes = $('#assign_delivery_table input[type=checkbox]:checked').not('.select_all');
                $.each($checkedBoxes, function () {
                    var value = $(this).val();
                    ids.push(value);
                });
                updateDeviceStatus(recordPlanId, ids, "{{config('common.status.propose_device.delivered')}}", $('#users').val());
            })
        }

        if (action == 'receive') {
            $('#receive-device').click(function (event) {
                var ids = [];

                var $checkedBoxes = $('#assign_receive_table input[type=checkbox]:checked').not('.select_all');
                $.each($checkedBoxes, function () {
                    var value = $(this).val();
                    ids.push(value);
                });
                updateDeviceStatus(recordPlanId, ids, "{{config('common.status.propose_device.returned')}}");
            })
        }

    </script>
@endpush

