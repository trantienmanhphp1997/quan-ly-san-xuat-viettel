<div class="modal modal-danger fade" tabindex="-1" id="assign_member_modal" role="dialog" aria-labelledby="assign_member_modal_body">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom: 0">
                <h4>Cập nhật phân bổ nhân sự</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body" id="assign_member_modal_body" style="padding-top: 0">
                <div class="card card-primary card-outline card-outline-tabs p-3">
                    <div class="card-header p-0 pt-1 border-bottom-0 flex-row">
                        <ul class="nav nav-tabs" id="nav-source" role="tablist">
                            @if((isset($type) &&  $type == config("common.departments.production")) || ($type == 'all'))
                                @can("removeAssignedMember",  "reporter")
                                    <li class="nav-item">
                                        <a class="nav-link active members" id="reporter" data-toggle="pill"
                                           href="#reporters" role="tab" aria-controls="reporters"
                                           aria-selected="false">{{__('data_field_name.model_name.reporter')}}{{-- (<span
                                                class="offer_number offer_reporter_number">{{isset($data->offer_reporter_number)? $data->offer_reporter_number : '0'}}</span>)--}}</a>
                                    </li>
                                @endcan
                            @endif
                            @if((isset($type) &&  $type == config("common.departments.technical_camera")) || ($type == 'all'))
                                @can("removeAssignedMember",  "technical_camera")
                                    <li class="nav-item">
                                        <a class="nav-link members" id="technical_camera" data-toggle="pill"
                                           href="#technical_cameras" role="tab"
                                           aria-controls="technical_cameras" aria-selected="false">{{__('data_field_name.model_name.technical_camera')}}</a>
                                    </li>
                                @endcan
                            @endif
                            @if((isset($type) &&  $type == config("common.departments.camera")) || ($type == 'all' && (isset($data->offer_camera_number) && $data->offer_camera_number > 0)))
                                @can("removeAssignedMember",  "camera")
                                    <li class="nav-item">
                                        <a class="nav-link members" id="camera" data-toggle="pill"
                                           href="#cameras" role="tab"
                                           aria-controls="cameras" aria-selected="false">{{__('data_field_name.model_name.camera')}} (<span
                                                class="offer_number offer_camera_number">{{isset($data->offer_camera_number)? $data->offer_camera_number : '0'}}</span>)</a>
                                    </li>
                                @endcan
                            @endif
                            @if((isset($type) &&  $type == config("common.departments.transportation")) || ($type == 'all' && (isset($data->offer_transportation_number) && $data->offer_transportation_number > 0)))
                                @can("removeAssignedMember",  "transportation")
                                    <li class="nav-item">
                                        <a class="nav-link members" id="transportation" data-toggle="pill"
                                           href="#transportations" role="tab"
                                           aria-controls="transportations" aria-selected="false">{{__('data_field_name.model_name.transportation')}} (<span
                                                class="offer_number offer_transportation_number">{{isset($data->offer_transportation_number)? $data->offer_transportation_number : '0'}}</span>)</a>
                                    </li>
                                @endcan
                            @endif
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-two-tabContent">
                            @if(isset($type) &&  $type == config("common.departments.production")  || $type == 'all')
                                @can("removeAssignedMember",  "reporter")
                                    <x-assign-member-tab id="reporters" labelledby="reporter" title="Chọn phóng viên" formName="table_reporter" formId="table_reporter" status='active'></x-assign-member-tab>
                                @endcan
                            @endif
                            @if(isset($type) &&  $type == config("common.departments.technical_camera")  || $type == 'all')
                                @can("removeAssignedMember",  "technical_camera")
                                    <x-assign-member-tab id="technical_cameras" labelledby="technical_camera" title="Chọn Kỹ thuật phòng quay" formName="table_technical_camera" formId="table_technical_camera"
                                                         status="{{auth()->user()->hasRole('super admin') && $type != config('common.departments.technical_camera') ? '' : 'active'}}"></x-assign-member-tab>
                                @endcan
                            @endif
                            @if(isset($type) &&  $type == config("common.departments.camera")  || $type == 'all')
                                @can("removeAssignedMember",  "camera")
                                    <x-assign-member-tab id="cameras" labelledby="camera" title="Chọn quay phim" formName="table_camera" formId="table_camera"
                                                         status="{{auth()->user()->hasRole('super admin') && $type != config('common.departments.camera') ? '' : 'active'}}"></x-assign-member-tab>
                                @endcan
                            @endif
                            @if(isset($type) &&  $type == config("common.departments.transportation") || $type == 'all')
                                @can("removeAssignedMember",  "transportation")
                                    <x-assign-member-tab id="transportations" labelledby="transportation" title="Chọn lái xe" formName="table_transportation" formId="table_transportation"
                                                         status="{{auth()->user()->hasRole('super admin') && $type != config('common.departments.transportation') ? '' : 'active'}}"></x-assign-member-tab>
                                @endcan
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-primary pull-right" id="assign-member">
                    {{ __('common.button.assign') }}
                </button>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('common.button.cancel') }}
                </button>
            </div>
        </div>
    </div>
</div>
@php
    $outsourceNote = "";
@endphp
@include("components.add-note-modal", ["message" => $outsourceNote, "type" => "member"])

@push('detail-record-plan')
    <script>
        var recordPlanId = "";
        $(".members").on("click", function (e) {
            var $department_id = $(this).attr('id');
            getOutsourceNote($department_id);
            if ($department_id && $(`#table_${$department_id}`).text() === '') {
                getMemberByDepartmentId($department_id);
            }
        })

        function getMembers() {
            var $department_id = $(".members").attr('id');
            getOutsourceNote($department_id);
            if ($department_id) {
                getMemberByDepartmentId($department_id)
            }
        }

        function getMemberByDepartmentId(department_id, recPlanId = null) {
            if (!recPlanId) {
                recPlanId = recordPlanId;
            }
            axios.get(`/assignment/${department_id}/${recPlanId}/show`, {
                department_id: department_id,
            })
                .then(function (response) {
                    $('.error').html("");
                    removeElementInSelectOption(`table_${department_id}`);
                    var $available_members = response.data.available_members;
                    var $select_available_members_id = $(`#table_${department_id}`)
                    var $member_id = $('#member').val();
                    var loop = 1;
                    if ($available_members.length > 0) {
                        $available_members.forEach($member => {
                            const template = `<td>
                                <select id="${$member.id}_select" class="form-control">
                                    <option value="go_only">Chỉ đưa đi</option>
                                    <option value="return_only">Chỉ đón về</option>
                                    <option value="round_trip">Chỉ đưa đi & đón về</option>
                                    <option value="along_with_trip">Đi cùng đoàn</option>
                                </select>
                            </td>`
                            content = `<tr>
                            <td>
                                <input type="checkbox" name="${department_id}_row_id" id="checkbox_assign_${$member.id}" value="${$member.id}">
                            </td>
                            <td>${loop++}</td>
                            <td class="text-truncate text-limit">${$member.full_name ? $member.full_name : ''}</td>
                            <td class="text-truncate text-limit">${$member.phone_number ? $member.phone_number : ''}</td>
                            <td>${$member.email ? $member.email : ''}</td>
                            <td>${$member.department.name}</td>
                            ${ $member.department_code == "transportation" && template}
                            </tr>`;
                            $select_available_members_id.append(content);
                        })

                        //Xử lý nút check all
                        $('.' + department_id + '_select_all').on('click', function (e) {
                            console.log("clicked");
                            $("input[name='" + department_id + "_row_id']").prop('checked', $(this).prop('checked')).trigger('change');
                        });
                        //Xử lý check box trên list
                        $("input[name='" + department_id + "_row_id']").on('change', function () {
                            var ids = [];
                            var unCheckedCount = 0;
                            $("input[name='" + department_id + "_row_id']").each(function () {
                                if ($(this).is(':checked')) {
                                    ids.push($(this).val());
                                } else {
                                    unCheckedCount++;
                                }
                            });
                            $('.'+ department_id + '_select_all').prop('checked', unCheckedCount == 0);
                            $('.selected_ids').val(ids);
                        });
                    } else {
                        $select_available_members_id.append(`<p class="text-center">Không có nhân sự phù hợp</p>`)
                    }
                    $(`.assigned_table_${department_id}`).append(`<input type="hidden" id="assigned_${department_id}" value="${response.data.assigned_members.length}"/>`)
                })
                .catch(function (error) {
                    console.log(error);
                })
        }

        $('#assign-member').click(function () {

            var formData = prepareDataForAssignMembers();
            let assign_reporter_ids = formData.assign_reporter.ids;
            let assign_camera_ids = formData.assign_camera.ids;
            let assign_transportation_ids = formData.assign_transportation.ids;

            //kiểm tra xem số lượng assign có thiếu có với số lượng đề xuất hay k
            let active_tab = $(".members.active").attr("id");
            if(!active_tab){
                active_tab =  $(".members").attr("id");
            }
            // console.log("active_tab " + active_tab)
            // console.log()
            let is_assign_shortage_members = false;
            if(active_tab == "reporter"){
                is_assign_shortage_members = checkIfAssignShortageMember(assign_reporter_ids.length, $('#assigned_reporter').val(), $('.offer_reporter_number').text());
            }else if(active_tab == "camera"){
                is_assign_shortage_members = checkIfAssignShortageMember(assign_camera_ids.length, $('#assigned_camera').val(), $('.offer_camera_number').text());
            }else if(active_tab == "transportation"){
                is_assign_shortage_members = checkIfAssignShortageMember(assign_transportation_ids.length, $('#assigned_transportation').val(), $('.offer_transportation_number').text());
            }

            // console.log("phân công thiếu nhân sự" + is_assign_shortage_members);
            if(is_assign_shortage_members){
                openAddNoteModal();
                    return false;
            }
            //checkOfferNumber(assign_reporter_ids.length, $('#assigned_reporter').val(), $('.offer_reporter_number').text(), '.error_table_reporter', 'đã vượt quá số lượng đề xuất') &&
            //checkOfferNumber(assign_transportation_ids.length, $('#assigned_transportation').val(), $('.offer_transportation_number').text(), '.error_table_transportation', 'đã vượt quá số lượng đề xuất')
            if (
                checkOfferNumber(assign_camera_ids.length, $('#assigned_camera').val(), $('.offer_camera_number').text(), '.error_table_camera', 'đã vượt quá số lượng đề xuất')
            ) {
                $('.error').html("");
                $.ajax({
                    type: "POST",
                    url: `/assignment/${recordPlanId}/assignMembers`,
                    data: formData,
                    dataType: 'json',
                    success: function (data) {
                        location.reload();
                    },
                    error: function (data) {
                        location.reload();
                    }
                });
            }
        })

        const prepareDataForAssignMembers = () => {
            let assign_reporter_ids = [];
            let assign_camera_ids = [];
            let assign_transportation_ids = [];
            let assign_technical_camera_ids = [];

            let $checkedReporters = $('#table_reporter input[type=checkbox]:checked').not('.select_all');
            let $checkedCams = $('#table_camera input[type=checkbox]:checked').not('.select_all');
            let $checkedTransportations = $('#table_transportation input[type=checkbox]:checked').not('.select_all');
            let $checkedTechnicalCams = $('#table_technical_camera input[type=checkbox]:checked').not('.select_all');
            $.each($checkedReporters, function () {
                let value = $(this).val();
                assign_reporter_ids.push(value);
            });

            $.each($checkedCams, function () {
                let value = $(this).val();
                assign_camera_ids.push({
                    memberId: value,
                    involveType:  ""
                });
            });

            $.each($checkedTechnicalCams, function () {
                let value = $(this).val();
                assign_technical_camera_ids.push({
                    memberId: value,
                    involveType:  ""
                });
            });

            $.each($checkedTransportations, function () {
                let value = $(this).val();
                assign_transportation_ids.push({
                    memberId: value,
                    involveType:  $(`#${value}_select`).val()
                });
            });

            return formData = {
                assign_reporter: {
                    ids: assign_reporter_ids,
                    department_code: 'reporter'
                },
                assign_camera: {
                    ids: assign_camera_ids,
                    department_code: 'camera'
                },
                assign_transportation: {
                    ids: assign_transportation_ids,
                    department_code: 'transportation',

                },
                assign_technical_camera:{
                    ids: assign_technical_camera_ids,
                    department_code: 'technical_camera',

                }
            }
        }

        function checkOfferNumber(assign_ids, assigned_ids, offer_number, error, msg) {
            $(`${error}`).html("");
            if ((parseInt(assign_ids) + parseInt(assigned_ids)) > parseInt(offer_number)) {
                $(`${error}`).append(msg);
                return false;
            }

            return true
        }

        const checkIfAssignShortageMember = (assign_ids, assigned_ids, offer_number) => {
            if ((parseInt(assign_ids) + parseInt(assigned_ids)) < parseInt(offer_number)) {
                openAddNoteModal();
                return true;
            }
            return false;
        }

        const getNoteByDepartment = async (departmentCode, recPlanId) => {
            let data = {
                departmentCode: departmentCode
            }
            return await axios.post(`/assignment/${recPlanId}/note`, data);
        }

        const getOutsourceNote = async ($department_id = null, recPlanId = null) => {
            console.log(recordPlanId)
            if (!$department_id) {
                $department_id = $(".members.active").attr("id");
                console.log($department_id)
            }
            if (!recPlanId) {
                recPlanId = recordPlanId;
            }
            //clear data cũ
            if ($department_id == "reporter") {
                $("#contentMessage_member").val(``)
            } else if ($department_id == "camera") {
                $("#contentMessage_member").val(``)
            } else if ($department_id == "transportation") {
                $("#contentMessage_member").val(``)
            }

            let reporter_note = await getNoteByDepartment("reporter", recPlanId);
            let camera_note = await getNoteByDepartment("camera", recPlanId);
            let transportation_note = await getNoteByDepartment("transportation", recPlanId);
            let reporterNote = reporter_note.data.note ? reporter_note.data.note : "";
            let cameraNote = camera_note.data.note ? camera_note.data.note : "";
            let transportationNote = transportation_note.data.note ? transportation_note.data.note : "";
            if ($department_id == "reporter") {
                $("#contentMessage_member").val(`${reporterNote} `)
            } else if ($department_id == "camera") {
                $("#contentMessage_member").val(`${cameraNote}`)
            } else if ($department_id == "transportation") {
                $("#contentMessage_member").val(`${transportationNote}`)
            }
        }

        const openAddNoteModal = () => {
            $("#confirmMessageContent_member").html("Chưa phân công đủ nhân sự cần thiết cho lịch sản xuất. Bạn có muốn thuê ngoài nhân sự?");
            let departmentCode = $(".members.active").attr("id");

            if (!departmentCode) {
                departmentCode = $(".members").attr('id');
            }

            $("#fromDepartment_member").val(departmentCode);

            $("#add-note-to-assign-member").modal("show");
        }

        $("#confirmForOutSource_member").on("click", function () {
            let data = {
                departmentCode: $("#fromDepartment_member").val(),
                content: $("#contentMessage_member").val(),
            }
            let assignMembers = prepareDataForAssignMembers();
            let formData = {...assignMembers, ...data}

            $.ajax({
                type: "POST",
                url: `/assignment/${recordPlanId}/add-note`,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    location.reload();
                },
                error: function (data) {
                    location.reload();
                }
            });
        })
    </script>
@endpush
