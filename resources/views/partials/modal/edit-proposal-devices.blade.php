<div class="modal modal-danger fade" tabindex="-1" id="offer_device_modal" role="dialog"
     aria-labelledby="offer_device_modal_body">
    <div class="modal-dialog modal-dialog-centered modal">
        <div class="modal-content">
            <div class="modal-header">
                Cập nhật đề xuất thiết bị
            </div>
            <div class="modal-body" id="offer_device_modal_body">
                <div class="prosol-device">
                    <div class="form-group">
{{--                        <div>--}}
{{--                            <label for="">Số lượng</label>--}}
{{--                            <input type="text" class="form-control" id="offer_device_number" name="offer_device_number" value="{{$data["offer_device_number"]}}">--}}
{{--                        </div>--}}
                        <div>

                            <!-- <textarea class="form-control" name="offer_device_note" id="offer_device_note" cols="10" rows="5">{{$data["offer_device_note"]}}</textarea> -->
                            <div class="row">
                                <div class="col-md-9 d-flex">
                                    <div class="dropdown col-form-label">
                                        <label>Mẫu đề xuất </label>
                                        <button class="btn btn-default dropdown-toggle btn-sm" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-list-ol"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            @foreach($exampleNotes as $note)
                                                <a class="dropdown-item" content="{{$note->content}}" href="#">{!! $note->name !!}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <label for="">Ghi chú</label>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <textarea class="form-control" name="offer_device_note" id="offer_device_note"
                                        autocomplete="off" placeholder="Ghi chú đề xuất thiết bị"
                                        rows=5>{{$data["offer_device_note"] ? $data["offer_device_note"] : old("technical_camera_note")}}</textarea>
                                    <span class="error" id="error_offer_device_note"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal" id="submit_offer_device">
                    {{ __('common.button.update') }}
                </button>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal" id="cancel_offer_device">
                    {{ __('common.button.cancel') }}
                </button>
            </div>
        </div>

    </div>
</div>

@push('detail-record-plan')
    <script>
        $('#offer_device_note').on('keyup change',function() {
            if ($(this).val() !== "" && $(this).val().length > 3000) {
                if ($('#error_offer_device_note').text() === "") {
                    $('#error_offer_device_note').append("Ghi chú đề xuất thiết bị không dài quá 3000 ký tự");
                }
                $('#submit_offer_device').prop("disabled", true);
            } else {
                $('#submit_offer_device').prop("disabled", false);
                $('#error_offer_device_note').empty();
            }
        });
        $('.dropdown-item').on('click', function () {
            $('#offer_device_note').val($(this).attr('content'));
        });
        $('#cancel_offer_device').on('click', function () {
            $('#text').val('{{$data["offer_device_note"] }}');
        });
        $('#submit_offer_device').click(function () {
            const recordPlanId = "{{$data->id}}";
            const device_offer_number = $("#offer_device_number").val();
            const device_offer_note = $("#offer_device_note").val();
            const formData = {
                device : {
                    number : device_offer_number,
                    note : device_offer_note
                },
            };
            $.ajax({
                type: "POST",
                url: `/record-plans/${recordPlanId}/update-offer-member`,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    location.reload();
                },
                error: function (data) {
                    location.reload();
                }
            });
        })
    </script>
@endpush

