<div class="modal modal-danger fade" tabindex="-1" id="edit_{{$field}}_modal" role="dialog" aria-labelledby="edit_{{$field}}_modal_body">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom: 0">
                <h4>Cập nhật {{isset($field) ? (($field == 'name') ? 'tên lịch sản xuất': (($field == 'description') ? 'mô tả': (($field == 'infomation') ? 'thông tin lịch sản xuất' : (($field == 'plan') ? 'kế hoạch sản xuất' : '')))) : '' }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
                <div class="modal-body" id="edit_{{$field}}_modal_body" style="padding-top: 0">
                    @if(isset($field) && $field == 'name')
                    <x-input type="text" label="{{__('data_field_name.record_plan.name')}}" form-name="name" value="{{isset($data) ? $data->name : ''}}" required=true placeholder="" styleAll="name" styleLabel="nameLable" styleInput="nameInput"/>
                    @endif
                    @if(isset($field) && $field == 'description')
                        <x-textarea label="{{__('data_field_name.common_field.descriptions')}}" form-name="description" value="{!! isset($data) ? $data->description : ''!!}" placeholder="" required="false" horizontal="description" row=5 maxlength=5/>
                    @endif
                    @if(isset($field) && $field == 'infomation')
                            <input type="text" name="field" id="field" hidden value="{{$field}}">
                            <div class="row position-allday">
                                <div class="col-3"></div>
{{--                                <div class="col-9 d-flex">--}}
{{--                                    <div class="form-check checkbox">--}}
{{--                                        <input type="checkbox" class="form-check-input" id="is_allday" name="is_allday" {{(!isset($data)) ? '' : (isset($data) && $data->is_allday < 1 ? '' : 'checked')}}>--}}
{{--                                    </div>--}}
{{--                                    <label class="">{{__('data_field_name.record_plan.is_allday')}}</label>--}}
{{--                                </div>--}}
                            </div>
                            <x-date-time form-name="start_time" label="Bắt đầu" value="{{isset($data) ? $data->start_time : $today}}" required="true" horizontal="start_time_class" styleInput="start_time_class"/>
                            <x-date-time form-name="end_time" label="Kết thúc" value="{{isset($data) ? $data->end_time : $tomorrow }}" required="true" horizontal="end_time_class" styleInput="end_time_class"/>
                            <!-- <x-textarea label="{{__('data_field_name.common_field.address')}}" form-name="address" value="{!! isset($data) ? $data->actual_address : ''!!}" required=true placeholder="" horizontal="address" row=5/> -->
                            <div class="form-group row">
                                <input type="hidden" id="addressType" value="{{isset($data) ? $data->address_type : null}}">
                            <label class="col-md-4 col-form-label">Địa chỉ<span
                                    class="text-danger">(*)</span></label>
                            <div class="col-md-8 d-flex">
                                <div class="form-check mr-2">
                                    <input class="form-check-input" type="radio" name="address_type_check"
                                        id="flexRadioDefault4" value="4" {{$data->address_type == 0 || $data->address_type == 1  ? 'checked' : "" }}>
                                    <label class="form-check-label" for="flexRadioDefault4">
                                        Địa chỉ ngoài
                                    </label>
                                </div>
                                <div class="form-check mr-2">
                                    <input class="form-check-input" type="radio" name="address_type_check"
                                        id="flexRadioDefault3" value ="3" {{$data->address_type == 2 ? 'checked' : "" }}>
                                    <label class="form-check-label" for="flexRadioDefault3">
                                        Trường quay
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="address-option">

                        </div>

                        <div class="input-address">

                        </div>
                    @endif
                    @if(isset($field) && $field == 'reporter')
                            <p>c</p>
                    @endif
                    @if(isset($field) && $field == 'plan')
                            <div class="form-group">
                                <label class=""> {{__('data_field_name.model_name.department')}}<span class="text-danger">(*)</span></label>
                                <div class="">
                                    <select name="department_id" id="department_id" class="form-control" required>
                                        @foreach($departments as $department)
                                            <option value="{{$department->id}}" {{isset($data) && $department->code == $data->department_code ? "selected" : ""}}>{{$department->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label> {{__('data_field_name.model_name.plans')}}{{--<span class="text-danger">(*)</span>--}}</label>
                                <div>
                                    <select name="plan_id" id="plan_id" class="form-control">
                                    </select>
                                </div>
                                @error("plan_id")
                                <span class="text-danger">
                                {{ $message }}
                            </span>
                                @enderror
                            </div>
                    @endif
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="submit" title="{{__('common.button.update')}}" class="btn btn-primary pull-right btn-action" id="edit_{{$field}}">
                        {{ __('common.button.update') }}
                    </button>
                    <button type="button" class="btn btn-default pull-right cancel" data-dismiss="modal" >
                        {{ __('common.button.cancel') }}
                    </button>
                </div>
        </div>
    </div>
</div>
@push('validation-rules')
    address: {
        required: true,
        maxlength: 500
    },
    description: {
        maxlength: 3000
    },
    offer_device_note: {
        maxlength: 3000
    },
    offer_technical_camera_note: {
        maxlength: 100
    },
@endpush

@push('validation-messages')
address: {
    required: '{{__("client_validation.form.required.record_plan.address")}}',
    maxlength: '{{__("client_validation.form.max_length.abstract", ["fieldName" => "địa chỉ", "maxlength" => 500])}}'
},

description: {
    maxlength: '{{__("client_validation.form.max_length.abstract", ["fieldName" => "mô tả", "maxlength" => 3000])}}'
},
offer_device_note: {
    maxlength: '{{__("client_validation.form.max_length.abstract", ["fieldName" => "ghi chú đề xuất thiết bị", "maxlength" => 3000])}}'
},
offer_technical_camera_note: {
    maxlength: '{{__("client_validation.form.max_length.abstract", ["fieldName" => "ghi chú đề xuất kỹ thuật phòng quay", "maxlength" => 100])}}'
},
@endpush
@push('detail-record-plan')
    <script>
         if($('.error').text() !== '') {
            $('#edit_name_modal').modal('show');
        }

$(document).ready(function() {

    if ($('.error').text() !== '' || (eval('<?php echo Session::has('error')?>') === 1)) {
        $('#edit_{{Session::get('field')}}_modal').modal('show');
    }

    $(".select-devices").hide();
    customDate(
        '.start_time',
        "{{ date('d-m-Y',strtotime(date('d-m-Y')))}}",
        "{{ date('H:i d-m-Y',strtotime($data->start_time))}}");
    customDate(
        '.end_time',
        "{{ date('d-m-Y',strtotime(date('d-m-Y')))}}",
        "{{ date('H:i d-m-Y',strtotime($data->end_time))}}");

    getPlans();
    $('#getReporters').select2({
        placeholder: "Chọn phóng viên"
    });

    switch ($("input[name='address_type_check']:checked").val()) {
            case "3":
                $(".address-option").empty();
                $(stage).appendTo($(".address-option"));
                // $(".input-address").hide('slow');
                break;
            case "4":
                $(".address-option").empty();
                $(addressInput).appendTo($(".address-option"));
                break;
            default:
                break;
    }
    // $(addressInput).appendTo($(".address-option"));
    $('input[type="radio"]').click(function(){
        switch ($(this).attr("value")) {
            case "3":
                $(".address-option").empty();
                $(stage).appendTo($(".address-option"));
                // $(".input-address").hide('slow');
                break;
            case "4":
                $(".address-option").empty();
                $(addressInput).appendTo($(".address-option"));
                break;
            case "1":
                $(".select-devices").show('slow');
                break;
            case "0":
                $(".select-devices").hide('slow');
                break;
            default:
                break;
        }
        });
    // $('input[type="radio"]').trigger('click');
});
       
        console.log( $("#addressType").val());
         $('#start_time').on("change",function(e){
             console.log("diff time");
             let diffTime = moment($('#end_time').val(), 'HH:mm DD-MM-YYYY').diff($('#end_time').val() !== '' && moment($('#start_time').val(), 'HH:mm DD-MM-YYYY'), 'days') +1;
             if($('#start_time').val() == ''){
                 $('#start_time-error').html('Bạn chưa chọn thời gian bắt đầu')
                 $('#start_time-error').show()
                 $('#edit_infomation').attr('disabled', true)
             }else{
                 if($('#end_time').val() !== '' && moment($('#start_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD HH:mm') > moment($('#end_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD HH:mm')){
                     $('#start_time-error').html('Thời gian bắt đầu lớn hơn thời gian kết thúc')
                     $('#start_time-error').show()
                     $('#edit_infomation').attr('disabled', true)
                     return;
                 }
                 if(moment($('#start_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD HH:mm') < moment().format('YYYY-MM-DD HH:mm')){
                     $('#start_time-error').html('Thời gian bắt đầu không được nhỏ hơn thời gian hiện tại')
                     $('#start_time-error').show()
                     $('#edit_infomation').attr('disabled', true)
                     return;
                 }
                 if(Math.abs(diffTime) > 1 &&  $("#addressType").val() == 2){
                     $('#start_time-error').html('Lịch trường quay chỉ diễn ra trong 1 ngày')
                     $('#start_time-error').show()
                     $('#end_time-error').hide()
                     $('#edit_infomation').attr('disabled', true)
                     return;
                 }
                 $('#start_time-error').hide()
                 $('#edit_infomation').attr('disabled', false)
             }
         });
         $('#end_time').on("change", function(e){
             console.log("diff time, end_time")
             let diffTime = moment($('#end_time').val(), 'HH:mm DD-MM-YYYY').diff($('#end_time').val() !== '' && moment($('#start_time').val(), 'HH:mm DD-MM-YYYY'), 'days') +1;
             console.log(diffTime, {{$data->address_type}})
             if($('#end_time').val() == ''){
                 $('#end_time-error').html('Bạn chưa chọn thời gian kết thúc')
                 $('#end_time-error').show()
                 $('#edit_infomation').attr('disabled', true)
             }else{
                 if($('#start_time').val() !== '' && moment($('#start_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD HH:mm') > moment($('#end_time').val(), 'HH:mm DD-MM-YYYY').format('YYYY-MM-DD HH:mm')){
                     $('#start_time-error').html('Thời gian bắt đầu lớn hơn thời gian kết thúc')
                     $('#start_time-error').show()
                     $('#edit_infomation').attr('disabled', true)
                 }else{
                     $('#start_time-error').hide()
                     $('#edit_infomation').attr('disabled', false)
                 }
                 if(Math.abs(diffTime) > 1 &&   $("#addressType").val() == 2){
                     $('#end_time-error').html('Lịch trường quay chỉ diễn ra trong 1 ngày')
                     $('#end_time-error').show()
                     $('#start_time-error').hide()
                     $('#edit_infomation').attr('disabled', true)
                     return;
                 }
                 $('#end_time-error').hide()
                 $('#edit_infomation').attr('disabled', false)
             }
         });

         $('.cancel').on("click", function (e) {
            location.reload();
         })

         $('.close').on("click", function (e) {
            location.reload();
        })
    </script>
@endpush
