<div class="modal modal-danger fade" tabindex="-1" id="unassign_modal" role="dialog"
    aria-labelledby="unassign_modal_body">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body text-center" id="unassign_modal_body">
                <div class="icon-default icon-delete mb-3">
                    <i class="fas fa-times"></i>
                </div>
                <h4 class="modal-title mb-4">
                    {{ __('common.confirm_message.confirm_title') }}
                </h4>
                <h5 class="noti-text">
                    {{ __('common.confirm_message.are_you_sure_unassign') }}?
                </h5>
            </div>
            <div class="modal-footer justify-content-center">
                <button id="bulk_delete" type="button" class="btn btn-default pull-right"
                    data-dismiss="modal">
                    {{ __('common.button.cancel') }}
                </button>
                <form action="" method="post" id="unassign-form">
                                        @method("delete")
                                        @csrf
                    <input type="hidden" name="department_code" id="department_code" value="">
                    <input type="hidden" name="" id="resource_ids" value="">
                    <button class="btn btn-danger" id="confirm_unassign" title="{{__('common.button.remove-assign')}}">
                        {{__('common.confirm_message.confirm_to_delete')}}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
