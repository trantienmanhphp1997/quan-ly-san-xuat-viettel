<div class="modal modal-danger fade" tabindex="-1" id="offer_member_modal" role="dialog" aria-labelledby="offer_member_modal_body">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom: 0">
                <h4>Cập nhật phân bổ nhân sự</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body" id="offer_member_modal_body" style="padding-top: 0">
                <div class="card card-primary card-outline card-outline-tabs p-3">
                    <div class="card-header p-0 pt-1 border-bottom-0 flex-row">
                        <ul class="nav nav-tabs" id="nav-source" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="reporter" data-toggle="pill"
                                       href="#offer-member-reporters" role="tab" aria-controls="offer-member-reporters"
                                       aria-selected="false">{{__('data_field_name.model_name.reporter')}} </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="technical-camera" data-toggle="pill"
                                       href="#offer-member-technical-cameras" role="tab"
                                       aria-controls="offer-member-reporters" aria-selected="false">{{__('data_field_name.model_name.technical_camera')}} </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="camera" data-toggle="pill"
                                       href="#offer-member-cameras" role="tab"
                                       aria-controls="offer-member-reporters" aria-selected="false">{{__('data_field_name.model_name.camera')}} </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="transportation" data-toggle="pill"
                                       href="#offer-member-transportations" role="tab"
                                       aria-controls="offer-member-reporters" aria-selected="false">{{__('data_field_name.model_name.transportation')}} </a>
                                </li>

                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-two-tabContent">
                            <x-offer-member-tab id="reporters" labelledby="reporter" title="Đề xuất phóng viên" :recordPlan="$data" formInputId="input_offer_reporter" formTextareaId="textarea_offer_reporter" status='active'></x-offer-member-tab>
                            <x-offer-member-tab id="technical-cameras" labelledby="technical_camera" title="Đề xuất kỹ thuật phòng quay" :recordPlan="$data"  formInputId="input_offer_technical_camera" formTextareaId="textarea_offer_technical_camera" status=""></x-offer-member-tab>
                            <x-offer-member-tab id="cameras" labelledby="camera" title="Đề xuất quay phim" :recordPlan="$data"  formInputId="input_offer_camera" formTextareaId="textarea_offer_camera" status=""></x-offer-member-tab>
                            <x-offer-member-tab id="transportations" labelledby="transportation" title="Đề xuất lái xe" :recordPlan="$data" formInputId="input_offer_transportation" formTextareaId="textarea_offer_transportation" status=""></x-offer-member-tab>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal" id="update-offer">
                    {{ __('common.button.update') }}
                </button>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('common.button.cancel') }}
                </button>
            </div>
        </div>
    </div>
</div>

@push('detail-record-plan')
    <script>
        $('#update-offer').click(function () {
            var recordPlanId = "{{$data->id}}";
            let reporter_offer_number = $("#input_offer_reporter").val();
            let reporter_offer_note = $("#textarea_offer_reporter").val();
            let camera_offer_number = $("#input_offer_camera").val();
            let camera_offer_note = $("#textarea_offer_camera").val();
            let transportation_offer_number = $("#input_offer_transportation").val();
            let transportation_offer_note = $("#textarea_offer_transportation").val();
            let technical_camera_offer_note = $("#textarea_offer_technical_camera").val();
            let technical_camera_offer_number = $("#input_offer_technical_camera").val();
            var formData = {
                reporter : {
                    number : reporter_offer_number,
                    note : reporter_offer_note
                },
                camera : {
                    number : camera_offer_number,
                    note : camera_offer_note
                },
                transportation : {
                    number : transportation_offer_number,
                    note : transportation_offer_note
                },
                technical_camera:{
                    note: technical_camera_offer_note,
                    number: technical_camera_offer_number
                }
            }
            $.ajax({
                type: "POST",
                url: `/record-plans/${recordPlanId}/update-offer-member`,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    location.reload();
                },
                error: function (data) {
                    location.reload();
                }
            });
        })
    </script>
@endpush
