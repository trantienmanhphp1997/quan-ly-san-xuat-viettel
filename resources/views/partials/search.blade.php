<form method="get" class="ml-auto search-form ">
        <input type="search" class="form-control" placeholder="{{__('common.place_holder.search')}}" name="s" value="{{ $allParams && array_key_exists("s", $allParams)  ? $allParams['s'] : "" }}">

        @if (Request::has('sort_order') && Request::has('order_by'))
            <input type="hidden" name="sort_order" value="{{ Request::get('sort_order') }}">
            <input type="hidden" name="order_by" value="{{ Request::get('order_by') }}">
        @endif
        @foreach($allParams as $key => $value)
            @if ($key !== 's')
                <input type="hidden" name={{$key}} value="{{ $value }}">
            @endif

        @endforeach
</form>
