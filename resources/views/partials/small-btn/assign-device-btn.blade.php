<a  class='btn btn-sm btn-outline-warning pull-right view text-decoration-none ml-1 no-wrap assign-device-btn' id="{{$params['record_plan_id']}}">
    {{--        <i class="far fa-eye"></i>--}}
    <span class="hidden-xs hidden-sm">
        @if(in_array(auth()->user()->department_code, [
                config("common.departments.production"),
                config("common.departments.general_news"),
                config("common.departments.news"),
                config("common.departments.documentary"),
                config("common.departments.entertainment"),
                config("common.departments.music"),
                config("common.departments.film"),
            ]) && auth()->user()->is_manager == 1 && !auth()->user()->hasRole('super admin'))
            {{__('common.button.device-proposal')}}
        @else
            @if(isset($params["is_returning"]) && $params["is_returning"] == 1)
            {{__('common.button.retrieve-device')}}
            @elseif(isset($params["is_lending"]) && $params["is_lending"] == 1)
            {{__('common.button.lending')}}
            @else
            {{__('common.button.assignment.device')}}
            @endif
        @endif
    </span>
</a>
