<button class="btn btn-warning btn-sm  rounded-0 mr-1" data-toggle="modal" data-target="#broken_modal{{$datum->id}}" {{$datum->status == 3 ? 'disabled' : '' }} data-placement="top" title="{{ __('common.button.report_broken') }}"><i class="fas fa fa-wrench"></i></button>

{{-- Bulk delete modal --}}
<div class="modal modal-danger fade" tabindex="-1" id="broken_modal{{$datum->id}}" role="dialog" aria-labelledby="broken_modal_body{{$datum->id}}">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
           <div class="modal-body text-center" id="broken_modal_body{{$datum->id}}">
                <div class="icon-default icon-report mb-3">
                    <i class="fas fa-tools"></i>
                </div>
                <h4 class="modal-title mb-4">
                    {{ __('common.confirm_message.confirm_title') }}
                </h4>
                <h5 class="noti-text">
                    {{ __('common.confirm_message.are_you_sure_report_broken') }}?
                </h5>
           </div>
            <div class="modal-footer justify-content-center">

                <form action="{{route(isset($route) ? $route : $routePrefix. '.report-broken', isset($params) ? $params :  ['id' => $datum->id])}}" method="post" enctype="multipart/form-data" class="w-100 p-3">
                    {{ csrf_field() }}
                    <div class="form-group text-center">
                        <textarea type="text" name="reason_report_broken" class="form-control" value="" placeholder="{{__('common.place_holder.reason_report_broken')}}"></textarea>
                    </div>
                    <div class="form-group text-center">
                    <button id="bulk_broken{{$datum->id}}" type="button" class="btn btn-default pull-right" data-dismiss="modal">
                        {{ __('common.button.cancel') }}
                    </button>
                    <input type="submit" class="btn btn-warning pull-right broken-confirm"
                             value="{{ __('common.confirm_message.confirm_to_delete') }} {{--{{ $modelDisplayName }}--}}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
