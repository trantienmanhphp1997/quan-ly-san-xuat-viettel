    @can("view", $datum)
            <a href='{{route($routePrefix. '.show',['id' => $datum->id])}}' type="button" class="btn btn-default btn-sm action  rounded-1 mr-1" data-toggle="tooltip" data-placement="top" title="{{ __('common.button.view') }}"><i class="fas fa-eye"></i></a>
    @endcan
    @can("update", $datum)
            <a href='{{route($routePrefix. '.edit',['id' => $datum->id])}}' type="button" class="btn btn-default btn-sm action rounded-0 mr-1" data-toggle="tooltip" data-placement="top" title="{{ __('common.button.update') }}"><i class="fas fa-edit"></i></a>
    @endcan

    @if(in_array($routePrefix,['devices','cars']))
        @can("deleteAny", $datum)
            @include("partials.small-btn.broken-btn")
        @endcan
    @endif

    @if(!$datum->deleted_at)
        @can("delete", $datum)
            @include("partials.small-btn.delete-btn")
        @endcan
    @endif

    @if($datum->deleted_at)
        @can("restore", $datum)

                <form action="{{route($routePrefix. '.restore',['id' => $datum->id])}}" method="post"
                      enctype="multipart/form-data" class="form-horizontal">
                    @csrf
                    <div>
                        <button class="btn btn-sm btn-warning" type="submit" title="{{ __('common.button.restore') }}"
                                onclick="return confirm('Are you sure?')"><i title="{{ __('common.button.restore') }}"
                                class="far fa-check"></i> {{__('common.button.restore')}}</button>
                    </div>
                </form>
        @endcan
    @endif
