<button class="btn btn-danger btn-sm  rounded-0 mr-1" data-toggle="modal" data-target="#delete_modal{{$datum->id}}" data-placement="top" title="{{ __('common.button.bulk_delete') }}"><i class="far fa-trash-alt"></i></button>

{{-- Bulk delete modal --}}
<div class="modal modal-danger fade" tabindex="-1" id="delete_modal{{$datum->id}}" role="dialog" aria-labelledby="delete_modal_body{{$datum->id}}">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
           <div class="modal-body text-center" id="delete_modal_body{{$datum->id}}">
               <div class="icon-default icon-delete mb-3">
                    <i class="fas fa-times"></i>
               </div>
               <h4 class="modal-title mb-4">
                    {{ __('common.confirm_message.confirm_title') }}
                </h4>
                <h5 class="noti-text">
                     {{ __('common.confirm_message.are_you_sure_delete') }}?
                </h5>
           </div>
            <div class="modal-footer justify-content-center">
                <button id="bulk_delete{{$datum->id}}" type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('common.button.cancel') }}
                </button>
                <form action="{{route(isset($route) ? $route : $routePrefix. '.destroy', isset($params) ? $params :  ['id' => $datum->id])}}" method="post" enctype="multipart/form-data" class="d-flex">
                    {{ method_field("DELETE") }}
                    {{ csrf_field() }}
                    <input type="hidden" name="ids" id="bulk_delete_input{{$datum->id}}" value="">
                    <input type="hidden" name="currentPage" value="{{$data->currentPage()}}">
                    <input type="hidden" name="total" value="{{$data->total()}}">
                    <input type="hidden" name="perPage" value="{{$data->perPage()}}">
                    <input type="hidden" name="lastPage" value="{{$data->lastPage()}}">
                    <input type="submit" class="btn btn-danger pull-right delete-confirm"
                             value="{{ __('common.confirm_message.confirm_to_delete') }} {{--{{ $modelDisplayName }}--}}">
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
