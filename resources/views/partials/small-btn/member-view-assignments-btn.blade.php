<div style="margin: 0 1%" class="d-flex">
    <a href="{{route($routePrefix. '.assignments',['id' => $datum->id])}}"
       class='btn btn-sm btn-success pull-right view text-decoration-none'>
        <i class="far fa-edit"></i>
        <span class="hidden-xs hidden-sm">{{__('common.button.view_tasks')}}</span>
    </a>
</div>
