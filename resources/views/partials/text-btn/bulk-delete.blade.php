<button class="btn btn-danger btn-sm  rounded-0 mr-1" id="bulk_delete_btn" {{isset($disabled) && $disabled ? "disabled" : ""}} data-toggle="tooltip" data-placement="top" title="{{ __('common.button.bulk_delete') }}"><i class="far fa-trash-alt mr-2"></i> <span>{{ __('common.button.bulk_delete') }}</span></button>

{{-- Bulk delete modal --}}
<div class="modal modal-danger fade" tabindex="-1" id="bulk_delete_modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
           <div class="modal-body text-center" id="bulk_delete_modal_body">
               <div class="icon-default icon-delete mb-3">
                    <i class="fas fa-times"></i>
               </div>
               <h4 class="modal-title mb-4">
                    {{ __('common.confirm_message.confirm_title') }}
                </h4>
                <h5 class="noti-text">
                    {{ __('common.confirm_message.are_you_sure_delete') }}
                    <span class="attention" id="bulk_delete_count"></span>
                    <span class="attention" id="bulk_delete_display_name"></span>?
                </h5>
           </div>
            <div class="modal-footer justify-content-center">
                <button id="bulk_delete" type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('common.button.cancel') }}
                </button>
                <form action="{{route(isset($route) ? $route : $routePrefix.".destroy", isset($params) ? $params : ["id" => -1])}}" id="bulk_delete_form" method="POST">
                    {{ method_field("DELETE") }}
                    {{ csrf_field() }}
                    <input type="hidden" name="ids" id="bulk_delete_input" value="">
                    <input type="hidden" name="currentPage" value="{{$currentPage}}">
                    <input type="hidden" name="total" value="{{$total}}">
                    <input type="hidden" name="perPage" value="{{$perPage}}">
                    <input type="hidden" name="lastPage" value="{{$lastPage}}">
                    <input type="submit" class="btn btn-danger pull-right delete-confirm"
                             value="{{ __('common.confirm_message.confirm_to_delete') }} {{--{{ $modelDisplayName }}--}}">
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@push('custom-scripts')
    <script>

        $(document).ready(function () {
            // Bulk delete selectors
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkDeleteModal = $('#bulk_delete_modal');
            var $bulkDeleteCount = $('#bulk_delete_count');
            var $bulkDeleteDisplayName = $('#bulk_delete_display_name');
            var $bulkDeleteInput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            $bulkDeleteModal.appendTo('body');
            // Bulk delete listener
            $bulkDeleteBtn.click(function () {
                var ids = [];
                var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
                // var count = $checkedBoxes.length;
                if ($checkedBoxes.length) {
                    // Reset input value
                    $bulkDeleteInput.val('');
                    // Deletion info
                    var displayName = "{{__('data_field_name.model_name.'.$modelDisplayName)}}";

                    $bulkDeleteDisplayName.html(displayName);
                    // Gather IDs
                    $.each($checkedBoxes, function () {
                        var objRecord = $(this).val().split(',').map(Number);
                            ids.push(objRecord[0]);
                    });
                    $bulkDeleteCount.html(ids.length);
                    // Set input value
                    $bulkDeleteInput.val(ids);
                    // Show modal
                    $bulkDeleteModal.modal('show');
                } else {
                    // No row selected
                    toastr.warning('{{ __('common.toast_message.bulk_delete_nothing') }}');
                }
            });
        })
    </script>

@endpush
