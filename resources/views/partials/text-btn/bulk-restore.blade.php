<button class="btn btn-info btn-sm  rounded-0 mr-1" data-toggle="tooltip" data-placement="top"  {{isset($disabled) && $disabled ? "disabled" : ""}} title="{{ __('common.button.bulk_restore') }}" id="bulk_restore_btn"> <i class="fas fa-redo mr-2"></i> <span>{{ __('common.button.bulk_restore') }}</span></button>

{{-- Bulk delete modal --}}
<div class="modal modal-info fade" tabindex="-1" id="bulk_restore_modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body text-center" id="bulk_restore_modal_body">
                <div class="icon-default icon-restore mb-3">
                    <i class="fas fa-redo"></i>
                </div>
                <h4 class="modal-title mb-4">
                    {{ __('common.confirm_message.confirm_title') }}
                </h4>
                <h5 class="noti-text">
                    {{ __('common.confirm_message.are_you_sure_restore') }}
                    <span class="attention" id="bulk_restore_count"></span> <span class="attention" id="bulk_restore_display_name"></span>
                    ?
                </h5>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('common.button.cancel') }}
                </button>
                <form action="{{isset($route) ? $route : route($routePrefix.".restore", isset($params) ? $params : ["id" => -1])}}" id="bulk_restore_form" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="ids" id="bulk_restore_input" value="">
                    <input type="submit" class="btn btn-info pull-right delete-confirm"
                           value="{{ __('common.confirm_message.confirm_to_restore') }}">
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@push('scripts')
    <script>
        $(document).ready(function (){
            var $bulkRestoreBtn = $('#bulk_restore_btn');
            var $bulkRestoreModal = $('#bulk_restore_modal');
            var $bulkRestoreCount = $('#bulk_restore_count');
            var $bulkRestoreDisplayName = $('#bulk_restore_display_name');
            var $bulkRestoreInput = $('#bulk_restore_input');
            var brokenStatus = '{{config("common.status.car.broken")}}';
            // Reposition modal to prevent z-index issues
            $bulkRestoreModal.appendTo('body');
            // Bulk delete listener
            $bulkRestoreBtn.click(function () {
                var ids = [];
                var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
                // var count = $checkedBoxes.length;
                if ($checkedBoxes.length) {
                    // Reset input value
                    $bulkRestoreInput.val('');
                    // Deletion info
                    var displayName = "{{__('data_field_name.model_name.'.$modelDisplayName)}}";

                    $bulkRestoreDisplayName.html(displayName);
                    // Gather IDs
                    $.each($checkedBoxes, function () {
                        var objRecord = $(this).val().split(',').map(Number);
                        if (objRecord[1] == brokenStatus) {
                            ids.push(objRecord[0]);
                        }
                    })
                    $bulkRestoreCount.html(ids.length);
                    // Set input value
                    $bulkRestoreInput.val(ids);
                    // Show modal
                    $bulkRestoreModal.modal('show');
                } else {
                    // No row selected
                    toastr.warning('{{ __('common.toast_message.bulk_restore_nothing') }}');
                }
            });
        })
        window.onload = function () {
            // Bulk delete selectors

        }
    </script>
@endpush


