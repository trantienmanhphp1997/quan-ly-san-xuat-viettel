<a href="{{isset($route) ? $route : route($routePrefix. '.create')}}" class="btn btn-success btn-add-new action">
    <i class="mdi mdi-plus"></i> <span>{{ __('common.button.create') }}</span>
</a>
