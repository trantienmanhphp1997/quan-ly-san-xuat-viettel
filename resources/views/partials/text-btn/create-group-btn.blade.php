<!-- <div class="dropdown">
    <button class="btn btn-primary btn-sm rounded-0 dropdown-toggle mr-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-plus mr-2"></i>Tạo mới
    </button>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" style="left:-60px !important;">
        <a class="btn btn-sm" href="{{isset($route) ? $route : route($routePrefix.".create", isset($createParams) ? $createParams : [])}}">Tạo mới</a>
        @can("import", app($modelClass))
            <a class="btn btn-sm"  href="{{isset($route) ? $route : route($routePrefix. '.import', isset($importParams) ? $importParams : [])}}">Tải lên từ Excel</a>
        @endcan
    </div>
</div> -->
@if(Gate::check("import", app($modelClass)) )
<div class="dropdown">
    <button class="btn btn-primary btn-sm rounded-0 dropdown-toggle mr-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-plus mr-2"></i>Tạo mới
    </button>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" style="left:-60px !important;">
        <a class="btn btn-sm action" href="{{isset($route) ? $route : route($routePrefix.".create", isset($createParams) ? $createParams : [])}}">Tạo mới</a>
        @can("import", app($modelClass))
            <a class="btn btn-sm action"  href="{{isset($route) ? $route : route($routePrefix. '.import', isset($importParams) ? $importParams : [])}}">Tải lên từ Excel</a>
        @endcan
    </div>
</div>
@else
    <a class="btn btn-primary action btn-sm rounded-0 mr-1" href="{{isset($route) ? $route : route($routePrefix.".create", isset($createParams) ? $createParams : [])}}"><i class="fas fa-plus mr-2"></i>Tạo mới</a>
@endif
