<button class="btn btn-warning btn-sm  rounded-0 mr-1"  data-toggle="tooltip" data-placement="top" {{isset($disabled) && $disabled ? "disabled" : ""}} title="{{ __('common.button.report_broken') }}" id="report_broken_btn"> <i class="fas fa fa-wrench mr-2"></i> <span>{{ __('common.button.report_broken') }}</span></button>

{{-- Bulk delete modal --}}
<div class="modal modal-warning fade" tabindex="-1" id="report_broken_modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
          <div class="modal-body text-center" id="bulk_restore_modal_body">
            <div class="icon-default icon-report mb-3">
                <i class="fas fa-tools"></i>
            </div>
            <h4 class="modal-title mb-4">
                {{ __('common.confirm_message.confirm_title') }}
            </h4>
            <h5 class="noti-text">
                {{ __('common.confirm_message.are_you_sure_report_broken') }}
                <span class="attention" id="report_broken_count"></span> <span class="attention" id="report_broken_display_name"></span>
                ?
            </h5>
          </div>
            <div class="modal-footer justify-content-center">
                <form action="{{isset($route) ? $route : route($routePrefix.".report-broken", isset($params) ? $params : ["id" => -1])}}" id="report_broken_form" method="POST" class="w-100 p-3">
                    {{ csrf_field() }}
                    <div class="form-group text-center">
                        <input type="hidden" name="ids" id="report_broken_input" value="">
                        <textarea type="text" name="reason_report_broken" class="form-control" value="" placeholder="{{__('common.place_holder.reason_report_broken')}}"></textarea>
                    </div>
                    <div class="form-group text-center">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                        {{ __('common.button.cancel') }}
                        </button>
                        <input type="submit" class="btn btn-warning pull-right delete-confirm"
                            value="{{ __('common.confirm_message.confirm_to_report_broken') }}">
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@push('scripts')
    <script>
        $(document).ready(function (){
            var $report_broken_btn = $('#report_broken_btn');
            var $report_broken_modal = $('#report_broken_modal');
            var $report_broken_count = $('#report_broken_count');
            var $report_broken_display_name = $('#report_broken_display_name');
            var $report_broken_input = $('#report_broken_input');
            var brokenStatus = '{{config("common.status.car.broken")}}';
            // Reposition modal to prevent z-index issues
            $report_broken_modal.appendTo('body');
            // Bulk delete listener
            $report_broken_btn.click(function () {
                var ids = [];
                var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
                // var count = $checkedBoxes.length;
                if ($checkedBoxes.length) {
                    // Reset input value
                    $report_broken_input.val('');
                    // Deletion info
                    var displayName = "{{__('data_field_name.model_name.'.$modelDisplayName)}}";

                    $report_broken_display_name.html(displayName);
                    // Gather IDs
                    $.each($checkedBoxes, function () {
                        var objRecord = $(this).val().split(',').map(Number);
                        if (objRecord[1] != brokenStatus) {
                            ids.push(objRecord[0]);
                        }
                    })
                    $report_broken_count.html(ids.length);
                    // Set input value
                    $report_broken_input.val(ids);
                    // Show modal
                    $report_broken_modal.modal('show');
                } else {
                    // No row selected
                    toastr.warning('{{ __('common.toast_message.report_broken_nothing') }}');
                }
            });
        })
        window.onload = function () {
            // Bulk delete selectors

        }
    </script>
@endpush


