<form action="{{route($route, isset($params) ? $params : [])}}" method="{{$method}}">
    @csrf
    <input type="hidden" name="{{$name}}" value="{{$value}}">
    <button class="btn btn-sm no-wrap view text-decoration-none mr-1
    @if(isset($primary) && in_array($status, $primary))
        badge-primary
    @elseif(isset($secondary) && in_array($status, $secondary))
        badge-secondary
    @elseif(isset($success) && in_array($status, $success))
        badge-success
    @elseif(isset($danger) && in_array($status, $danger))
        badge-danger
    @elseif(isset($warning) && in_array($status, $warning))
        badge-warning
    @elseif(isset($info) && in_array($status, $info))
        badge-info
    @elseif(isset($light) && in_array($status, $light))
        badge-light
    @elseif(isset($dark) && in_array($status, $dark))
        badge-dark
    @endif">
        @if(isset($btnLabel))
            {{$btnLabel}}
        @else
            @foreach(config('common.status.'. $dataType) as $key => $value)
                @if($value == $status)
                    {{__('status.'. $dataType . '.' . $key)}}
                @endif
            @endforeach
        @endif
    </button>
</form>
