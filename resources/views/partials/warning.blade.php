<!-- Hiển thị thông báo thành công  -->
@if ( session()->has('warning'))
    @push("custom-scripts")
    <script>
        console.log("ok");
        $("#warning_modal").modal('show');
    </script>
    @endpush
@endif

{{-- Bulk delete modal --}}
<div class="modal modal-warning fade" tabindex="-1" id="warning_modal" role="dialog" aria-labelledby="warning_modal_body">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
           <div class="modal-body text-center" id="warning_modal_body">
               <div class="icon-default icon-report mb-3">
                    <i class="fas fa-exclamation-triangle"></i>
               </div>
               <h4 class="modal-title mb-4"></h4>
                <h5 class="noti-text">
                     {{session()->get('warning')}}
                </h5>
           </div>
            <div class="modal-footer justify-content-center">
                <button id="bulk_delete" type="button" class="btn btn-warning pull-right" data-dismiss="modal">
                    {{ __('common.button.ok') }}
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php
    session()->forget('warning');
?>
