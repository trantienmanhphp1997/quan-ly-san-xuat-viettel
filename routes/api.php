<?php

use App\Http\Controllers\API\AssignmentAPIController;
use App\Http\Controllers\API\AuthAPIController;
use App\Http\Controllers\API\CarAPIController;
use App\Http\Controllers\API\DepartmentAPIController;
use App\Http\Controllers\API\DeviceAPIController;
use App\Http\Controllers\API\DeviceCategoryAPIController;
use App\Http\Controllers\API\MemberAPIController;
use App\Http\Controllers\API\OutSourceAPIController;
use App\Http\Controllers\API\PlanInfoAPIController;
use App\Http\Controllers\API\RecordPlanAPIController;
use App\Http\Controllers\API\SendMessageApiController;
use App\Http\Controllers\API\StageAPIController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\ExampleNoteAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ["cors"]], function(){
    Route::post('authenticate', [AuthAPIController::class, "authenticateUser"])->name("authenticate");
});
//sendMessage
Route::post("send-mocha-message", [SendMessageApiController::class, "sendMessage"]);

Route::group(['middleware' => ["cors", "jwt.verify"]], function () {
    Route::get("auth-user", [AuthAPIController::class, "getUser"]);
    //Assignments

    Route::group(["prefix" => "assignments"], function () {
        Route::get("/", [RecordPlanAPIController::class, "getAssignmentList"]);
        Route::get("/my-assignments", [RecordPlanAPIController::class, "getMyAssignmentList"]);
//
        //Lấy ds đề xuất
        Route::get("get-proposals", [AssignmentAPIController::class, "getProposals"]);
        Route::get("get-assigned-proposals", [AssignmentAPIController::class, "getAssignedProposals"]);

        //lấy ds recordplan có thiết bị đã được giao
        Route::get("get-record-plans-have-delivered-devices", [AssignmentAPIController::class, "getRecordPlansHasDeliveredDevices"]);
        //members
        Route::get("get-available-members", [AssignmentAPIController::class, "getAvailableMemberForRecordPlanByDepartmentCode"]);
        Route::get("{record_plan_id}/get-assigned-members", [AssignmentAPIController::class, "getAssignedMemberForRecordPlanByDepartmentCode"]);
        Route::post("{record_plan_id}/assign-members", [AssignmentAPIController::class, "assignMembersToRecordPlan"]);
        Route::post("{record_plan_id}/cancel-members", [AssignmentAPIController::class, "cancelMembersForRecordPlan"]);

        //devices
        Route::get("{record_plan_id}/get-available-devices", [AssignmentAPIController::class, "getAvailableDevicesForRecordPlan"]);
        Route::post("{record_plan_id}/assign-devices", [AssignmentAPIController::class, "assignDeviceForRecordPlan"]);
        Route::post("{record_plan_id}/cancel-devices", [AssignmentAPIController::class, "cancelDeviceForRecordPlan"]);
        Route::post("{record_plan_id}/update-status-devices", [AssignmentAPIController::class, "updateDeviceStatus"]);

        // update trạng thái công việc của quay phim | lái xe theo lịch (bắt đầu | kết thúc)
        Route::post("{record_plan_id}/update-work-status", [AssignmentAPIController::class, "updateWorkStatusOfMember"]);

    });

    //Record plans
    Route::group(["prefix" => "record-plans"], function () {
        Route::get("/", [RecordPlanAPIController::class, "index"]);
        Route::get("assign-coming", [RecordPlanAPIController::class, "getAssignComing"]);
        Route::post("update-or-create/{id}", [RecordPlanAPIController::class, "updateOrCreate"]);
        Route::get("find-by-id/{id}", [RecordPlanAPIController::class, "findById"]);
        Route::delete("delete/{id}", [RecordPlanAPIController::class, "destroy"]);
        Route::post("suggest-member", [RecordPlanAPIController::class, "suggestMember"]);
        Route::post("confirm-borrow-all/{id}", [RecordPlanAPIController::class, "confirmBorrowAll"]);
        Route::post("confirm-return-all/{id}", [RecordPlanAPIController::class, "confirmReturnAll"]);
        Route::post("check-received-device/{id}", [RecordPlanAPIController::class, "checkReceivedDevice"]);
        Route::post('{id}/update-status', [RecordPlanAPIController::class, 'updateGeneralStatus']);
    });
    //Members
    Route::group(["prefix" => "members"], function () {
        Route::get("/", [MemberAPIController::class, "getMember"]);
        Route::post("/find-by-uuid", [MemberAPIController::class, "findByUuid"]);
        Route::get("/find-by-id", [MemberAPIController::class, "findById"]);
        Route::get('/find-busy-by-department', [MemberAPIController::class, 'findBusyMembers']);
        Route::get("/get-reporters-for-record-plan", [MemberAPIController::class, "getReportersForRecordPlan"]);
        Route::get("/get-members-for-record-plan", [MemberAPIController::class, "getMembersForRecordPlan"]);
    });
    //Category
    Route::group(["prefix" => "categories"], function () {
        Route::get("/", [DeviceCategoryAPIController::class, "index"]);
    });
    //Device
    Route::group(["prefix" => "devices"], function () {
        Route::get("/", [DeviceAPIController::class, "index"]);
        Route::post("search", [DeviceAPIController::class, "search"]);
        Route::get("find-by-id/{id}", [DeviceAPIController::class, "findById"]);
        Route::get("detail-have-record-plan/{id}", [DeviceAPIController::class, "getDetailHaveRecordPlan"]);
        Route::post("suggest", [DeviceAPIController::class, "suggest"]);
        Route::post("confirm-borrow-one", [DeviceAPIController::class, "confirmBorrowOne"]);
        Route::post("confirm-return-one", [DeviceAPIController::class, "confirmReturnOne"]);
        Route::post("report-broken", [DeviceAPIController::class, "reportBroken"]);
        Route::post("restore-device", [DeviceAPIController::class, "restoreDevice"]);
        Route::post("update-or-create/{id}", [DeviceAPIController::class, "updateOrCreateDevice"]);
        Route::delete("delete/{id}", [DeviceAPIController::class, "destroy"]);
        Route::get("list-record-plan-suggest", [DeviceAPIController::class, "listRecordPlanSuggest"]);
        Route::get('borrowing', [DeviceAPIController::class, 'findBorrowingDevicesByRecPlan']);
        Route::get('one-borrowing/{id}', [DeviceAPIController::class, 'getOneDeviceHaveBorrowingOrNot']);
        Route::get('list-borrowing', [DeviceAPIController::class, 'getListDeviceBorrowing']);
        Route::get('record-plan-borrowing', [DeviceAPIController::class, 'findRecPlansBorrowingDevice']);
        Route::get("borrowing-device", [DeviceAPIController::class, "getListBorrowingDeviceByUserId"]);
        Route::post("request-return-device", [DeviceAPIController::class, "requestReturnDevice"]);
    });
    //Car
    Route::group(["prefix" => "cars"], function () {
        Route::get("/", [CarAPIController::class, "index"]);
        Route::get("find-by-id/{id}", [CarAPIController::class, "findById"]);
        Route::post("store/{id}", [CarAPIController::class, "store"]);
    });
    //stages
    Route::group(["prefix" => "stages"], function (){
        Route::get("/all", [StageAPIController::class, "index"]);
    });

    //Plan info
    Route::group(["prefix" => "plan-info"], function () {
        Route::get("/", [PlanInfoAPIController::class, "index"]);
        Route::get("/find-by-department", [PlanInfoAPIController::class, "findByDepartment"]);
    });
    //Department
    Route::group(["prefix" => "departments"], function () {
        Route::get("/", [DepartmentAPIController::class, "index"]);
        Route::get("/include-child", [DepartmentAPIController::class, "getIncludeChild"]);
        Route::get("/find-by-code", [DepartmentAPIController::class, "findByCode"]);
        Route::get("/find-by-record-plan-id", [DepartmentAPIController::class, "findByRecordPlanId"]);
    });

    //Outsource
    Route::group(["prefix" => "outsources"], function () {
        Route::get("/", [OutSourceAPIController::class, "index"]);
    });

    Route::group([
        'prefix' => 'example-notes',
    ], function () {
        Route::get('', [ExampleNoteAPIController::class, 'index'])->name('index');
    });
});



