<?php

use App\Http\Controllers\Admin\DeviceCategoryController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\ManagerController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\DepartmentController;
use App\Http\Controllers\Admin\RecordPlanController;
use App\Http\Controllers\Admin\RecordPlan\RecordPlanController2;
use App\Http\Controllers\Admin\AssignController;
use App\Http\Controllers\Admin\TechnicalCameraController;
use App\Http\Controllers\Admin\ReporterController;
use App\Http\Controllers\Admin\CameraController;
use App\Http\Controllers\Admin\DriverController;
use App\Http\Controllers\Admin\CarController;
use App\Http\Controllers\Admin\DeviceController;
use App\Http\Controllers\Admin\OutsourceTypeController;
use App\Http\Controllers\Admin\OutsourceController;
use App\Http\Controllers\Admin\PlanController;
use App\Http\Controllers\Admin\StatementController;
use App\Http\Controllers\Admin\ContractController;
use App\Http\Controllers\Admin\ChangePasswordController;
use App\Http\Livewire\Counter;
use App\Http\Livewire\Greeting;
use App\Http\Livewire\Base;
use App\Http\Livewire\Calendar;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\StageController;
use App\Http\Controllers\Admin\ExampleNoteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/','DashboardController@index');

Route::group(['prefix' => 'basic-ui'], function(){
    Route::get('accordions', function () { return view('pages.basic-ui.accordions'); });
    Route::get('buttons', function () { return view('pages.basic-ui.buttons'); });
    Route::get('badges', function () { return view('pages.basic-ui.badges'); });
    Route::get('breadcrumbs', function () { return view('pages.basic-ui.breadcrumbs'); });
    Route::get('dropdowns', function () { return view('pages.basic-ui.dropdowns'); });
    Route::get('modals', function () { return view('pages.basic-ui.modals'); });
    Route::get('progress-bar', function () { return view('pages.basic-ui.progress-bar'); });
    Route::get('pagination', function () { return view('pages.basic-ui.pagination'); });
    Route::get('tabs', function () { return view('pages.basic-ui.tabs'); });
    Route::get('typography', function () { return view('pages.basic-ui.typography'); });
    Route::get('tooltips', function () { return view('pages.basic-ui.tooltips'); });
});

Route::group(['prefix' => 'charts'], function(){
    Route::get('chartjs', function () { return view('pages.charts.chartjs'); });
    Route::get('morris', function () { return view('pages.charts.morris'); });
    Route::get('flot', function () { return view('pages.charts.flot'); });
    Route::get('google-charts', function () { return view('pages.charts.google-charts'); });
    Route::get('sparklinejs', function () { return view('pages.charts.sparklinejs'); });
    Route::get('c3-charts', function () { return view('pages.charts.c3-charts'); });
    Route::get('chartist', function () { return view('pages.charts.chartist'); });
    Route::get('justgage', function () { return view('pages.charts.justgage'); });
});

Route::group(['prefix' => 'tables'], function(){
    Route::get('basic-table', function () { return view('pages.tables.basic-table'); });
    Route::get('data-table', function () { return view('pages.tables.data-table'); });
    Route::get('js-grid', function () { return view('pages.tables.js-grid'); });
    Route::get('sortable-table', function () { return view('pages.tables.sortable-table'); });
});

Route::group(['prefix' => 'icons'], function(){
    Route::get('material', function () { return view('pages.icons.material'); });
    Route::get('flag-icons', function () { return view('pages.icons.flag-icons'); });
    Route::get('font-awesome', function () { return view('pages.icons.font-awesome'); });
    Route::get('simple-line-icons', function () { return view('pages.icons.simple-line-icons'); });
    Route::get('themify', function () { return view('pages.icons.themify'); });
});

//-------------------------------------------------------------------------------------------------------------------------------------------------------



Auth::routes([
    'register' => false, // Registration Routes...
    'verify' => false, // Email Verification Routes...
]);

//Route::get('/', function () {
//    return view('welcome');
//});

Route::group(['middleware' => ['auth', 'get.menu']], function () {
    Route::get('/calendars', Calendar::class)->name('calendar');
//    Route::get('/', Calendar::class)->name('home');
    Route::get('/', [RecordPlanController::class, 'index'])->name('home');

    //change-password'
    Route::group([
        'prefix' => 'change-password',
        'as' => 'change-password.',
    ], function(){
        Route::get('', [ChangePasswordController::class, 'index'])->name('index');
        Route::post('store', [ChangePasswordController::class, 'store'])->name('store');
    });
    Route::get("planned_mail", [\App\Http\Controllers\Admin\MailController::class, "sendDailyPlannedReportRecordPlans"]);
    Route::get("executed_mail", [\App\Http\Controllers\Admin\MailController::class, "sendDailyExecutedReportRecordPlans"]);

    //Record plans
    Route::group([
        'prefix' => 'record-plans2',
        'as' => 'record-plans2.',
    ], function(){
        Route::get('', [RecordPlanController2::class, 'index'])->name('index');
    });

    Route::group([
        'prefix' => 'record-plans',
        'as' => 'record-plans.',
    ], function(){
        Route::get('', [RecordPlanController::class, 'index'])->name('index');
        Route::get('{id}/show', [RecordPlanController::class, 'showRecordPlan'])->name('show');
        Route::get('create', [RecordPlanController::class, 'create'])->name('create');
        Route::post('', [RecordPlanController::class, 'store'])->name('store');
        Route::get('{id}/edit', [RecordPlanController::class, 'editRecordPlan'])->name('edit');
        Route::put('{id}/update', [RecordPlanController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [RecordPlanController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [RecordPlanController::class, 'restore'])->name('restore');
        Route::post('{id}/update-status', [RecordPlanController::class, 'updateGeneralStatus'])->name('update-status');
        Route::get('import', [RecordPlanController::class, 'import'])->name('import');
        Route::post('import', [RecordPlanController::class, 'importStore'])->name('store-import');
        Route::get('export', [RecordPlanController::class, 'export'])->name('export');
        Route::get("downloadFile/{fileName}", [RecordPlanController::class, "downloadFile"])->name('dowloadFile');

        Route::post("{id}/update-offer-member", [RecordPlanController::class, "updateOfferMember"]);

        Route::get("test", [RecordPlanController::class, "autoCreateRecordPlanRecurring"]);
    });

    //Calendar
    Route::middleware([])->group(function () {
        Route::group(['prefix' => 'calendars'], function () {
            Route::get('/record-plans', [RecordPlanController::class, 'getEvents']);
        });
    });

    // Group
    Route::group([
        "middleware" => "checkRole:superadmin",
        'prefix' => 'menus',
        'as' => 'menus.',
//        "middleware" =>  ['can:is_admin']
    ], function () {
        Route::get('', [MenuController::class, 'index'])->name('index');
        Route::get('{id}/show', [MenuController::class, 'show'])->name('show');
        Route::get('create', [MenuController::class, 'create'])->name('create');
        Route::post('', [MenuController::class, 'store'])->name('store');
        Route::get('{id}/edit', [MenuController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [MenuController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [MenuController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [MenuController::class, 'restore'])->name('restore');
    });

    // Departments
    Route::group([
        "middleware" => "checkRole:superadmin",
        'prefix' => 'departments',
        'as' => 'departments.',
//        "middleware" =>  ['can:is_admin']
    ], function () {
        Route::get('', [DepartmentController::class, 'index'])->name('index');
        Route::get('{id}/show', [DepartmentController::class, 'show'])->name('show');
        Route::get('create', [DepartmentController::class, 'create'])->name('create');
        Route::post('', [DepartmentController::class, 'store'])->name('store');
        Route::get('{id}/edit', [DepartmentController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [DepartmentController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [DepartmentController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [DepartmentController::class, 'restore'])->name('restore');
    });

    // Permission management - access and features
    Route::group([
        'as' => 'permissions.'
    ], function () {
        Route::get('access-permissions', [PermissionController::class, 'access'])->name('access');
        Route::put('access-permissions/save', [PermissionController::class, 'saveAccessPermissions'])->name('access.save');
        Route::put('access-permissions/remove', [PermissionController::class, 'removeAccessPermissions'])->name('access.remove');
        Route::get("feature-permissions", [PermissionController::class, 'features'])->name('features');
        Route::put('feature-permissions/save', [PermissionController::class, 'saveFeaturePermissions'])->name('features.save');
    });

    // Manager
    Route::group([
        "middleware" => "checkRole:superadmin",
        'prefix' => 'managers',
        'as' => 'managers.',
    ], function () {
        Route::get('', [ManagerController::class, 'index'])->name('index');
        Route::get('{id}/show', [ManagerController::class, 'show'])->name('show');
        Route::get('create', [ManagerController::class, 'create'])->name('create');
        Route::post('', [ManagerController::class, 'store'])->name('store');
        Route::get('{id}/edit', [ManagerController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [ManagerController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [ManagerController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [ManagerController::class, 'restore'])->name('restore');
    });
    // Car
    Route::group([
        "middleware" => "checkRole:".config('common.departments.transportation'),
        'prefix' => 'cars',
        'as' => 'cars.',
    ], function () {
        Route::get('', [CarController::class, 'index'])->name('index');
        Route::get('{id}/show', [CarController::class, 'show'])->name('show');
        Route::get('create', [CarController::class, 'create'])->name('create');
        Route::post('', [CarController::class, 'store'])->name('store');
        Route::get('{id}/edit', [CarController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [CarController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [CarController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [CarController::class, 'restore'])->name('restore');
        Route::get('export', [CarController::class, 'export'])->name('export');
        Route::get('import', [CarController::class, 'import'])->name('import');
        Route::post('importStore', [CarController::class, 'importStore'])->name('store-import');
        Route::get("downloadFile/{fileName}", [CarController::class, "downloadFile"])->name('dowloadFile');
        Route::post('{id}/report-broken', [CarController::class, 'reportBrokenCars'])->name('report-broken');
        Route::get("export-assignments", [CarController::class, 'exportAssignment'])->name("export-assignments");
    });
    // Device
    Route::group([
        "middleware" => "checkRole:".config('common.departments.device'),
        'prefix' => 'devices',
        'as' => 'devices.',
    ], function () {
        Route::get('/', [DeviceController::class, 'index'])->name('index');
        Route::get('{id}/show', [DeviceController::class, 'show'])->name('show');
        Route::get('create', [DeviceController::class, 'create'])->name('create');
        Route::post('', [DeviceController::class, 'store'])->name('store');
        Route::get('{id}/edit', [DeviceController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [DeviceController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [DeviceController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [DeviceController::class, 'restore'])->name('restore');
        Route::get('exportdevice', [DeviceController::class, 'export'])->name('exportdevice');
        Route::get('exportDataExpiredLendingDayDevices', [DeviceController::class, 'exportDataExpiredLendingDayDevices'])->name('exportDataExpiredLendingDayDevices');
        Route::post("get-available-devices", [DeviceController::class, "getAvailableDevicesByTime"])->name("get-available-devices");
        Route::post("{id}/report-broken", [DeviceController::class, "reportBrokenDevices"])->name("report-broken");
        Route::get("import", [DeviceController::class, "import"])->name("import");
        Route::post("import", [DeviceController::class, "storeImport"])->name("store-import");
        Route::get("downloadFile/{fileName}", [DeviceController::class, "downloadFile"])->name('dowloadFile');
        Route::get("export", [AssignController::class, "exportMembers"])->name("export");
        Route::get("lending", [DeviceController::class, "getLendingDevices"])->name("lending");

        Route::get("export-assignments", [DeviceController::class, "exportDeviceAssignment"])->name("export-assignments");
        Route::post("getListDeviceWaitConf", [DeviceController::class, "getListDeviceWaitConf"])->name("getListDeviceWaitConf");
//        Route::get("")->name("expired-lending");
    });

    //Device Category
    Route::group([
        "middleware" => "checkRole:".config('common.departments.device'),
        'prefix' => 'device-categories',
        'as' => 'device-categories.',
    ], function () {
        Route::get('', [DeviceCategoryController::class, 'index'])->name('index');
        Route::get('{id}/show', [DeviceCategoryController::class, 'show'])->name('show');
        Route::get('create', [DeviceCategoryController::class, 'create'])->name('create');
        Route::post('', [DeviceCategoryController::class, 'store'])->name('store');
        Route::get('{id}/edit', [DeviceCategoryController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [DeviceCategoryController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [DeviceCategoryController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [DeviceCategoryController::class, 'restore'])->name('restore');
        Route::get('getListCategory', [DeviceCategoryController::class, 'getListCategory'])->name('getListCategory');
    });

    //Members
    Route::group([
        "middleware" => "checkRole:".config('common.departments.production'),
        "prefix" => "reporter",
        "as" => "reporter."
    ], function() {
        Route::get('', [ReporterController::class, 'index'])->name('index');
        Route::get('{id}/show', [ReporterController::class, 'show'])->name('show');
        Route::get('create', [ReporterController::class, 'create'])->name('create');
        Route::post('', [ReporterController::class, 'store'])->name('store');
        Route::get('{id}/edit', [ReporterController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [ReporterController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [ReporterController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [ReporterController::class, 'restore'])->name('restore');
        Route::get("import", [ReporterController::class, "import"])->name("import");
        Route::post("import", [ReporterController::class, "storeImport"])->name("store-import");
        Route::get("export", [ReporterController::class, "exportMembers"])->name("export");
        Route::get("downloadFile/{fileName}", [ReporterController::class, "downloadFile"])->name('dowloadFile');
        Route::get("{id}/assignments", [ReporterController::class, "assignments"])->name("assignments");
        Route::get("assignments/{department_code}", [ReporterController::class, "getAssignmentsByDepartmentCode"])->name("list-assignments");
        Route::get("export-assignments", [ReporterController::class, "exportAssignments"])->name("export-assignments");
        Route::post("get-members-by-department_code", [ReporterController::class, "getMembersByDepartmentCode"]);
    });

    //Members
    Route::group([
        "middleware" => "checkRole:".config('common.departments.camera'),
        "prefix" => "camera",
        "as" => "camera."
    ], function() {
        Route::get('', [CameraController::class, 'index'])->name('index');
        Route::get('{id}/show', [CameraController::class, 'show'])->name('show');
        Route::get('create', [CameraController::class, 'create'])->name('create');
        Route::post('', [CameraController::class, 'store'])->name('store');
        Route::get('{id}/edit', [CameraController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [CameraController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [CameraController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [CameraController::class, 'restore'])->name('restore');
        Route::get("import", [CameraController::class, "import"])->name("import");
        Route::post("importStore", [CameraController::class, "storeImport"])->name("store-import");
        Route::get("export", [CameraController::class, "exportMembers"])->name("export");
        Route::get("downloadFile/{fileName}", [CameraController::class, "downloadFile"])->name('dowloadFile');
        Route::get("{id}/assignments", [CameraController::class, "assignments"])->name("assignments");
        Route::get("assignments", [CameraController::class, "getAssignmentsByDepartmentCode"])->name("list-assignments");
        Route::get("export-assignments", [CameraController::class, "exportAssignments"])->name("export-assignments");

    });

    //Members
    Route::group([
        "middleware" => "checkRole:".config('common.departments.technical_camera'),
        "prefix" => "technical_camera",
        "as" => "technical_camera."
    ], function() {
        Route::get('', [TechnicalCameraController::class, 'index'])->name('index');
        Route::get('{id}/show', [TechnicalCameraController::class, 'show'])->name('show');
        Route::get('create', [TechnicalCameraController::class, 'create'])->name('create');
        Route::post('', [TechnicalCameraController::class, 'store'])->name('store');
        Route::get('{id}/edit', [TechnicalCameraController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [TechnicalCameraController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [TechnicalCameraController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [TechnicalCameraController::class, 'restore'])->name('restore');
        Route::get("import", [TechnicalCameraController::class, "import"])->name("import");
        Route::post("importStore", [TechnicalCameraController::class, "storeImport"])->name("store-import");
        Route::get("export", [TechnicalCameraController::class, "exportMembers"])->name("export");
        Route::get("downloadFile/{fileName}", [TechnicalCameraController::class, "downloadFile"])->name('dowloadFile');
        Route::get("{id}/assignments", [TechnicalCameraController::class, "assignments"])->name("assignments");
        Route::get("assignments", [TechnicalCameraController::class, "getAssignmentsByDepartmentCode"])->name("list-assignments");
        Route::get("export-assignments", [TechnicalCameraController::class, "exportAssignments"])->name("export-assignments");

    });
    //quản lý trường quay
    Route::group([
        "middleware" => "checkRole:".config('common.departments.camera'),
        "prefix" => "stages",
        "as" => "stages."
    ], function() {
        Route::get('', [StageController::class, 'index'])->name('index');
        Route::get('{id}/show', [StageController::class, 'show'])->name('show');
        Route::get('create', [StageController::class, 'create'])->name('create');
        Route::post('', [StageController::class, 'store'])->name('store');
        Route::get('{id}/edit', [StageController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [StageController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [StageController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [StageController::class, 'restore'])->name('restore');
//        Route::get("import", [CameraController::class, "import"])->name("import");
//        Route::post("importStore", [CameraController::class, "storeImport"])->name("store-import");
//        Route::get("export", [CameraController::class, "exportMembers"])->name("export");
//        Route::get("downloadFile/{fileName}", [CameraController::class, "downloadFile"])->name('dowloadFile');

    });

    //Members
    Route::group([
        "middleware" => "checkRole:".config('common.departments.transportation'),
        "prefix" => "transportation",
        "as" => "transportation."
    ], function() {
        Route::get('', [DriverController::class, 'index'])->name('index');
        Route::get('{id}/show', [DriverController::class, 'show'])->name('show');
        Route::get('create', [DriverController::class, 'create'])->name('create');
        Route::post('', [DriverController::class, 'store'])->name('store');
        Route::get('{id}/edit', [DriverController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [DriverController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [DriverController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [DriverController::class, 'restore'])->name('restore');
        Route::get("import", [DriverController::class, "import"])->name("import");
        Route::post("import", [DriverController::class, "storeImport"])->name("store-import");
        Route::get("export", [DriverController::class, "exportMembers"])->name("export");
        Route::get("downloadFile/{fileName}", [DriverController::class, "downloadFile"])->name('dowloadFile');
        Route::get("{id}/assignments", [DriverController::class, "assignments"])->name("assignments");
        Route::get("assignments", [DriverController::class, "getAssignmentsByDepartmentCode"])->name("list-assignments");
        Route::get("export-assignments", [DriverController::class, "exportAssignments"])->name("export-assignments");
    });

    // Outsource types
    Route::group([
        'prefix' => 'outsource-types',
        'as' => 'outsource-types.',
    ], function () {
        Route::get('', [OutsourceTypeController::class, 'index'])->name('index');
        Route::get('{id}/show', [OutsourceTypeController::class, 'show'])->name('show');
        Route::get('create', [OutsourceTypeController::class, 'create'])->name('create');
        Route::post('', [OutsourceTypeController::class, 'store'])->name('store');
        Route::get('{id}/edit', [OutsourceTypeController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [OutsourceTypeController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [OutsourceTypeController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [OutsourceTypeController::class, 'restore'])->name('restore');
    });

    // Outsources
    Route::group([
        'prefix' => 'outsources',
        'as' => 'outsources.',
    ], function () {
        Route::get('', [OutsourceController::class, 'index'])->name('index');
        Route::get('{id}/show', [OutsourceController::class, 'show'])->name('show');
        Route::get('create', [OutsourceController::class, 'create'])->name('create');
        Route::post('', [OutsourceController::class, 'store'])->name('store');
        Route::get('{id}/edit', [OutsourceController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [OutsourceController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [OutsourceController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [OutsourceController::class, 'restore'])->name('restore');
    });


    //Assignment
    Route::group([
        'prefix' => 'assignment',
        'as' => 'assignment.',
    ], function (){
        Route::post('{record_plan_id}/assignMembers', [AssignController::class, 'assignMembers'])->name('assignMembers');
        Route::post("{record_plan_id}/add-note", [AssignController::class, "addNote"])->name("add-note");
        Route::post("{record_plan_id}/note", [AssignController::class, "getNoteByDepartmentCode"]);
        //Phân việc cho reporter
        Route::group([
            "middleware" => "checkRole:".config('common.departments.production'),
            'prefix' => "reporter",
            'as' => 'reporter.',
        ], function (){
            Route::get("{record_plan_id}/show", [AssignController::class, "getAssignmentByDepartmentCode"])->name("show");
            Route::post("{record_plan_id}", [AssignController::class, "assignMembersForRecordPlan"])->name("store");
            Route::delete("{record_plan_id}/destroy", [AssignController::class, "deleteMembersForRecordPlan"])->name("destroy");
            Route::get("proposals",[AssignController::class, "getReporterProposals"])->name("proposals");
        });
        //Phân việc cho cameramen
        Route::group([
            "middleware" => "checkRole:".config('common.departments.camera'),
            'prefix' =>  config("common.departments.camera"),
            'as' => config('common.departments.camera').".",
        ], function (){
            Route::get("{record_plan_id}/show", [AssignController::class, "getAssignmentByDepartmentCode"])->name("show");
            Route::post("{record_plan_id}", [AssignController::class, "assignMembersForRecordPlan"])->name("store");
            Route::delete("{record_plan_id}/destroy", [AssignController::class, "deleteMembersForRecordPlan"])->name("destroy");
            Route::get("proposals", [AssignController::class, "getCamProposals"])->name("proposals");
        });

        //Phân việc cho technical_camera
        Route::group([
            "middleware" => "checkRole:".config('common.departments.technical_camera'),
            'prefix' =>  config("common.departments.technical_camera"),
            'as' => config('common.departments.technical_camera').".",
        ], function (){
            Route::get("{record_plan_id}/show", [AssignController::class, "getAssignmentByDepartmentCode"])->name("show");
            Route::post("{record_plan_id}", [AssignController::class, "assignMembersForRecordPlan"])->name("store");
            Route::delete("{record_plan_id}/destroy", [AssignController::class, "deleteMembersForRecordPlan"])->name("destroy");
            Route::get("proposals", [AssignController::class, "getTechnicalCamProposals"])->name("proposals");
        });

        //Phân công transportation
        Route::group([
            "middleware" => "checkRole:".config('common.departments.transportation'),
            'prefix' =>  config("common.departments.transportation"),
            'as' => config('common.departments.transportation').".",
        ], function (){
            Route::get("{record_plan_id}/show", [AssignController::class, "getAssignmentByDepartmentCode"])->name("show");
            Route::post("{record_plan_id}", [AssignController::class, "assignMembersForRecordPlan"])->name("store");
            Route::delete("{record_plan_id}/destroy", [AssignController::class, "deleteMembersForRecordPlan"])->name("destroy");
            Route::get("proposals", [AssignController::class, "getDriverProposals"])->name("proposals");
        });

        //Phân công cho device
        Route::group([
            "middleware" => "checkRole:".config('common.departments.device'),
            'prefix' => config("common.departments.device"),
            'as' => config('common.departments.device').".",
        ], function (){
            Route::post("{record_plan_id}/show", [AssignController::class, "getDeviceAssignment"])->name("show");
            Route::post("{record_plan_id}", [AssignController::class, "assignDeviceForRecordPlan"])->name("store");
            Route::delete("{record_plan_id}/destroy", [AssignController::class, "deleteDeviceForRecordPlan"])->name("destroy");
            Route::delete("{record_plan_id}/accept", [AssignController::class, "acceptDeviceForRecordPlan"])->name("accept");
            Route::delete("{record_plan_id}/deny", [AssignController::class, "deleteDeviceForRecordPlan"])->name("deny");
            Route::get("proposals", [AssignController::class, "getDeviceProposals"])->name("proposals");
            Route::get("lending", [AssignController::class, "lendingDevices"])->name("lending");
            Route::get("returning", [AssignController::class, "returningDevices"])->name("returning");
            Route::get("{record_plan_id}/lending-detail", [AssignController::class, "getDevicesForLending"])->name("lending-detail");
            Route::get("{record_plan_id}/returning-detail", [AssignController::class, "getDevicesForReturning"])->name("returning-detail");
            Route::put("update-status", [AssignController::class, "updateDeviceStatus"])->name("update-status");
            Route::get("{record_plan_id}/memer-can-lending-device", [AssignController::class, "getMemberCanLending"])->name("memer-can-lending-device");

        });

        //Phân người cho record-plan
        Route::group([
            'prefix' => 'record-plan',
            'as' => 'record-plan.',
        ], function (){
//            Route::get("{record_plan_id}/show", [AssignController::class, "getCameramenassignment"])->name("cam.show");
//            Route::post("{record_plan_id}", [AssignController::class, "assignCamsForRecordPlan"])->name("cam.store");
//            Route::delete("{record_plan_id}/destroy", [AssignController::class, "deleteCamsForRecordPlan"])->name("cam.destroy");
        });
    });

    // plans
    Route::group([
        "middleware" => "checkRole:".config('common.departments.production'),
        'prefix' => 'plans',
        'as' => 'plans.',
    ], function () {
        Route::get('', [PlanController::class, 'index'])->name('index');
        Route::get('{id}/show', [PlanController::class, 'show'])->name('show');
        Route::get('create', [PlanController::class, 'create'])->name('create');
        Route::post('', [PlanController::class, 'store'])->name('store');
        Route::get('{id}/edit', [PlanController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [PlanController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [PlanController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [PlanController::class, 'restore'])->name('restore');
        Route::get('import', [PlanController::class, 'import'])->name('import');
        Route::post('importStore', [PlanController::class, 'importStore'])->name('store-import');
        Route::get("downloadFile/{fileName}", [PlanController::class, "downloadFile"])->name('dowloadFile');
        Route::post("get-plans-by-department_id", [PlanController::class, "getPlansByDepartmentId"]);
        Route::get('export', [PlanController::class, 'export'])->name('export');
    });

    // Statement
    Route::group([
        "middleware" => "checkRole:".config('common.departments.production'),
        'prefix' => 'statements',
        'as' => 'statements.',
    ], function () {
        Route::get('', [StatementController::class, 'index'])->name('index');
        Route::get('{id}/show', [StatementController::class, 'show'])->name('show');
        Route::get('create', [StatementController::class, 'create'])->name('create');
        Route::post('', [StatementController::class, 'store'])->name('store');
        Route::get('{id}/edit', [StatementController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [StatementController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [StatementController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [StatementController::class, 'restore'])->name('restore');
        Route::post("get-data-by-department-id", [StatementController::class, "getStatementsByDepartmentId"]);
        Route::get('import', [StatementController::class, 'import'])->name('import');
        Route::post('importStore', [StatementController::class, 'importStore'])->name('store-import');
        Route::get('export', [StatementController::class, 'export'])->name('export');
        Route::get("downloadFile/{fileName}", [StatementController::class, "downloadFile"])->name('dowloadFile');
    });

    // Contract
    Route::group([
        "middleware" => "checkRole:".config('common.departments.production'),
        'prefix' => 'contracts',
        'as' => 'contracts.',
    ], function () {
        Route::get('', [ContractController::class, 'index'])->name('index');
        Route::get('{id}/show', [ContractController::class, 'show'])->name('show');
        Route::get('create', [ContractController::class, 'create'])->name('create');
        Route::post('', [ContractController::class, 'store'])->name('store');
        Route::get('{id}/edit', [ContractController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [ContractController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [ContractController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [ContractController::class, 'restore'])->name('restore');
        Route::get('import', [ContractController::class, 'import'])->name('import');
        Route::post('importStore', [ContractController::class, 'importStore'])->name('store-import');
        Route::get("downloadFile/{fileName}", [ContractController::class, "downloadFile"])->name('dowloadFile');
        Route::get('export', [ContractController::class, 'export'])->name('export');
    });

    Route::group([
        'prefix' => 'example-notes',
        'as' => 'example-notes.',
    ], function () {
        Route::get('', [ExampleNoteController::class, 'index'])->name('index');
        Route::get('{id}/show', [ExampleNoteController::class, 'show'])->name('show');
        Route::get('create', [ExampleNoteController::class, 'create'])->name('create');
        Route::post('', [ExampleNoteController::class, 'store'])->name('store');
        Route::get('{id}/edit', [ExampleNoteController::class, 'edit'])->name('edit');
        Route::put('{id}/update', [ExampleNoteController::class, 'update'])->name('update');
        Route::delete('{id}/destroy', [ExampleNoteController::class, 'destroy'])->name('destroy');
        Route::post('{id}/restore', [ExampleNoteController::class, 'restore'])->name('restore');
    });


});


Route::group([
    'prefix' => 'schedule',
    'as' => 'schedule.',
], function () {
    Route::get('cloneplan', [RecordPlanController::class, 'autoCreateRecordPlanRecurring'])->name('cloneplan');
});
Route::get('/livewire-base', Base::class);
Route::get('/counter', Counter::class);
Route::get('/greeting', Greeting::class);
