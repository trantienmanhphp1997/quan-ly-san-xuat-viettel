const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/jquery.validation.addMethod.js', 'public/js')
    .sass("resources/sass/app.scss", "public/css")
    .sass("resources/css/app.scss", "public/css");

mix.copy(
    "resources/js/custom-flatpickr.js",
    "public/js/custom-flatpickr.js"
);
mix.copy(
    "resources/js/get-plans-by-departemet.js",
    "public/js/get-plans-by-departemet.js"
);
mix.copy(
    "resources/js/get-members-by-departemet.js",
    "public/js/get-members-by-departemet.js"
);
mix.copy(
    "resources/js/device-services.js",
    "public/js/device-services.js"
);


mix.copyDirectory("resources/js/plugins", "public/plugins")
mix.copyDirectory("resources/css/dist", "public/dist")

mix.copyDirectory("resources/assets", "public/assets")
    // jQuery
    .copyDirectory(
        "node_modules/jquery/dist",
        "public/plugins/jquery"
    )
    // Popper
    .copyDirectory(
        "node_modules/popper.js/dist",
        "public/plugins/popper"
    )
    // Bootstrap
    .copyDirectory(
        "node_modules/bootstrap/dist/js",
        "public/plugins/bootstrap/js"
    )
    // Font Awesome
    .copyDirectory(
        "node_modules/@fortawesome/fontawesome-free/css",
        "public/plugins/fontawesome-free/css"
    )
    .copyDirectory(
        "node_modules/@fortawesome/fontawesome-free/webfonts",
        "public/plugins/fontawesome-free/webfonts"
    )
    // overlayScrollbars
    .copyDirectory(
        "node_modules/overlayscrollbars/js",
        "public/plugins/overlayScrollbars/js"
    )
    .copyDirectory(
        "node_modules/overlayscrollbars/css",
        "public/plugins/overlayScrollbars/css"
    )
    // Chart.js
    .copyDirectory(
        "node_modules/chart.js/dist/",
        "public/plugins/chart.js"
    )
    // jQuery UI
    .copyDirectory(
        "node_modules/jquery-ui-dist/",
        "public/plugins/jquery-ui"
    )
    // Flot
    .copyDirectory(
        "node_modules/flot/dist/es5/",
        "public/plugins/flot"
    )
    // Summernote
    .copyDirectory(
        "node_modules/summernote/dist/",
        "public/plugins/summernote"
    )
    // Bootstrap Slider
    .copyDirectory(
        "node_modules/bootstrap-slider/dist/",
        "public/plugins/bootstrap-slider"
    )
    // Bootstrap Colorpicker
    .copyDirectory(
        "node_modules/bootstrap-colorpicker/dist/js",
        "public/plugins/bootstrap-colorpicker/js"
    )
    .copyDirectory(
        "node_modules/bootstrap-colorpicker/dist/css",
        "public/plugins/bootstrap-colorpicker/css"
    )
    // Tempusdominus Bootstrap 4
    .copyDirectory(
        "node_modules/tempusdominus-bootstrap-4/build/js",
        "public/plugins/tempusdominus-bootstrap-4/js"
    )
    .copyDirectory(
        "node_modules/tempusdominus-bootstrap-4/build/css",
        "public/plugins/tempusdominus-bootstrap-4/css"
    )
    // Moment
    .copyDirectory(
        "node_modules/moment/min",
        "public/plugins/moment"
    )
    .copyDirectory(
        "node_modules/moment/locale",
        "public/plugins/moment/locale"
    )
    // FastClick
    .copyDirectory(
        "node_modules/fastclick/lib",
        "public/plugins/fastclick"
    )
    // Date Range Picker
    .copyDirectory(
        "node_modules/daterangepicker",
        "public/plugins/daterangepicker"
    )
    // DataTables
    .copyDirectory(
        "node_modules/pdfmake/build",
        "public/plugins/pdfmake"
    )
    .copyDirectory(
        "node_modules/jszip/dist",
        "public/plugins/jszip"
    )
    .copyDirectory(
        "node_modules/datatables.net/js",
        "public/plugins/datatables"
    )
    .copyDirectory(
        "node_modules/datatables.net-bs4/js",
        "public/plugins/datatables-bs4/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-bs4/css",
        "public/plugins/datatables-bs4/css"
    )
    .copyDirectory(
        "node_modules/datatables.net-autofill/js",
        "public/plugins/datatables-autofill/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-autofill-bs4/js",
        "public/plugins/datatables-autofill/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-autofill-bs4/css",
        "public/plugins/datatables-autofill/css"
    )
    .copyDirectory(
        "node_modules/datatables.net-buttons/js",
        "public/plugins/datatables-buttons/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-buttons-bs4/js",
        "public/plugins/datatables-buttons/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-buttons-bs4/css",
        "public/plugins/datatables-buttons/css"
    )
    .copyDirectory(
        "node_modules/datatables.net-colreorder/js",
        "public/plugins/datatables-colreorder/js"
    )

    .copyDirectory(
        "node_modules/datatables.net-colreorder-bs4/js",
        "public/plugins/datatables-colreorder/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-colreorder-bs4/css",
        "public/plugins/datatables-colreorder/css"
    )
    .copyDirectory(
        "node_modules/datatables.net-fixedcolumns/js",
        "public/plugins/datatables-fixedcolumns/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-fixedcolumns-bs4/js",
        "public/plugins/datatables-fixedcolumns/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-fixedcolumns-bs4/css",
        "public/plugins/datatables-fixedcolumns/css"
    )
    .copyDirectory(
        "node_modules/datatables.net-fixedheader/js",
        "public/plugins/datatables-fixedheader/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-fixedheader-bs4/js",
        "public/plugins/datatables-fixedheader/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-fixedheader-bs4/css",
        "public/plugins/datatables-fixedheader/css"
    )
    .copyDirectory(
        "node_modules/datatables.net-keytable/js",
        "public/plugins/datatables-keytable/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-keytable-bs4/js",
        "public/plugins/datatables-keytable/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-keytable-bs4/css",
        "public/plugins/datatables-keytable/css"
    )
    .copyDirectory(
        "node_modules/datatables.net-responsive/js",
        "public/plugins/datatables-responsive/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-responsive-bs4/js",
        "public/plugins/datatables-responsive/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-responsive-bs4/css",
        "public/plugins/datatables-responsive/css"
    )
    .copyDirectory(
        "node_modules/datatables.net-rowgroup/js",
        "public/plugins/datatables-rowgroup/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-rowgroup-bs4/js",
        "public/plugins/datatables-rowgroup/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-rowgroup-bs4/css",
        "public/plugins/datatables-rowgroup/css"
    )
    .copyDirectory(
        "node_modules/datatables.net-rowreorder/js",
        "public/plugins/datatables-rowreorder/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-rowreorder-bs4/js",
        "public/plugins/datatables-rowreorder/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-rowreorder-bs4/css",
        "public/plugins/datatables-rowreorder/css"
    )
    .copyDirectory(
        "node_modules/datatables.net-scroller/js",
        "public/plugins/datatables-scroller/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-scroller-bs4/js",
        "public/plugins/datatables-scroller/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-scroller-bs4/css",
        "public/plugins/datatables-scroller/css"
    )
    .copyDirectory(
        "node_modules/datatables.net-select/js",
        "public/plugins/datatables-select/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-select-bs4/js",
        "public/plugins/datatables-select/js"
    )
    .copyDirectory(
        "node_modules/datatables.net-select-bs4/css",
        "public/plugins/datatables-select/css"
    )

    // Fullcalendar
    .copyDirectory(
        "node_modules/@fullcalendar/core/",
        "public/plugins/fullcalendar"
    )
    .copyDirectory(
        "node_modules/@fullcalendar/bootstrap/",
        "public/plugins/fullcalendar-bootstrap"
    )
    .copyDirectory(
        "node_modules/@fullcalendar/daygrid/",
        "public/plugins/fullcalendar-daygrid"
    )
    .copyDirectory(
        "node_modules/@fullcalendar/timegrid/",
        "public/plugins/fullcalendar-timegrid"
    )
    .copyDirectory(
        "node_modules/@fullcalendar/interaction/",
        "public/plugins/fullcalendar-interaction"
    )
    // icheck bootstrap
    .copyDirectory(
        "node_modules/icheck-bootstrap/",
        "public/plugins/icheck-bootstrap"
    )
    // inputmask
    .copyDirectory(
        "node_modules/inputmask/dist/",
        "public/plugins/inputmask"
    )
    // ion-rangeslider
    .copyDirectory(
        "node_modules/ion-rangeslider/",
        "public/plugins/ion-rangeslider"
    )
    // JQVMap (jqvmap-novulnerability)
    .copyDirectory(
        "node_modules/jqvmap-novulnerability/dist/",
        "public/plugins/jqvmap"
    )
    // jQuery Mapael
    .copyDirectory(
        "node_modules/jquery-mapael/js/",
        "public/plugins/jquery-mapael"
    )
    // Raphael
    .copyDirectory(
        "node_modules/raphael/",
        "public/plugins/raphael"
    )
    // jQuery Mousewheel
    .copyDirectory(
        "node_modules/jquery-mousewheel/",
        "public/plugins/jquery-mousewheel"
    )
    // jQuery Knob
    .copyDirectory(
        "node_modules/jquery-knob-chif/dist/",
        "public/plugins/jquery-knob"
    )
    // pace-progress
    .copyDirectory(
        "node_modules/@lgaitan/pace-progress/dist/",
        "public/plugins/pace-progress"
    )
    // Select2
    .copyDirectory(
        "node_modules/select2/dist/",
        "public/plugins/select2"
    )
    .copyDirectory(
        "node_modules/@ttskch/select2-bootstrap4-theme/dist/",
        "public/plugins/select2-bootstrap4-theme"
    )
    // Sparklines
    .copyDirectory(
        "node_modules/sparklines/source/",
        "public/plugins/sparklines"
    )
    // SweetAlert2
    .copyDirectory(
        "node_modules/sweetalert2/dist/",
        "public/plugins/sweetalert2"
    )
    .copyDirectory(
        "node_modules/@sweetalert2/theme-bootstrap-4/",
        "public/plugins/sweetalert2-theme-bootstrap-4"
    )
    // Toastr
    .copyDirectory(
        "node_modules/toastr/build/",
        "public/plugins/toastr"
    )
    // jsGrid
    .copyDirectory(
        "node_modules/jsgrid/dist",
        "public/plugins/jsgrid"
    )
    .copyDirectory(
        "node_modules/jsgrid/demos/",
        "public/plugins/jsgrid/demos"
    )
    // flag-icon-css
    .copyDirectory(
        "node_modules/flag-icon-css/css",
        "public/plugins/flag-icon-css/css"
    )
    .copyDirectory(
        "node_modules/flag-icon-css/flags",
        "public/plugins/flag-icon-css/flags"
    )
    // bootstrap4-duallistbox
    .copyDirectory(
        "node_modules/bootstrap4-duallistbox/dist",
        "public/plugins/bootstrap4-duallistbox/"
    )
    // filterizr
    .copyDirectory(
        "node_modules/filterizr/dist",
        "public/plugins/filterizr/"
    )
    // ekko-lightbox
    .copyDirectory(
        "node_modules/ekko-lightbox/dist",
        "public/plugins/ekko-lightbox/"
    )
    // bootstrap-switch
    .copyDirectory(
        "node_modules/bootstrap-switch/dist",
        "public/plugins/bootstrap-switch/"
    )
    // bs-custom-file-input
    .copyDirectory(
        "node_modules/bs-custom-file-input/dist/",
        "public/plugins/bs-custom-file-input"
    )

    .copyDirectory(
        "node_modules/bootstrap-datepicker/dist",
        "public/plugins/bootstrap-datepicker"
    )

    .copyDirectory(
        "node_modules/bootstrap-datepicker/dist",
        "public/plugins/bootstrap-datepicker"
    )
    .copyDirectory(
        "node_modules/bootstrap/dist/js/bootstrap.min.js",
        "public/plugins/bootstrap/bootstrap.min.js"
    )
    .copyDirectory(
        "node_modules/bootstrap/dist/js/bootstrap.min.js.map",
        "public/plugins/bootstrap/bootstrap.min.js.map"
    )
    .copyDirectory(
        "node_modules/select2/dist/js/select2.min.js",
        "public/plugins/select2/select2.min.js"
    )
    .copyDirectory(
        "node_modules/flatpickr/dist/flatpickr.min.js",
        "public/plugins/flatpickr/flatpickr.min.js"
    )

    .copyDirectory(
        "node_modules/flatpickr/dist/flatpickr.min.css",
        "public/css/flatpickr.min.css"
    )
    .copy(
        "resources/assets/js/jquery.validation.addMethod.js",
        "public/assets/js/jquery.validation.addMethod.js"
    )
    .copy(
        "resources/css/pagelayout.css",
        "public/css/pagelayout.css"
    )
    .copy(
        "resources/css/staradminstyle.css",
        "public/css/staradminstyle.css"
    )
;
